# Props-ios

## Workflow

### Branches

* <b>master</b> - Current Development version
* <b>XXXXXXX-some-branch</b> - branch where we are doing task with id XXXXXXX

### Schemes

* Nicely - for submitting to the App Store
* NicelyStaging - for debugging, QA and sending builds configurated for Staging Server

### Work on tasks
* The latest version of the app is stored in **master** branch. **master** branch should always reflect live build.
* When a new story is started in a new branch, it should be created from the latest **master** branch. It should be called **XXXXXXX-some-branch**.
* Once story is finished a **Pull Request**  should be opened back to **master** branch.
* Once it gets merged story branch should be deleted.

### Submission to AppStore

1. When we are ready to submit to the AppStore we should create new branch based on **master** with all changes we needed. It should be called **release\v1.2.3**.
2. We should create pull request to **master** from **release\v1.2.3**.
3. Before build has been sent to testers for regression tests, build should be send to AppStoreConnect (build should be archived using "Nicely" scheme), to ensure that build can be uploaded without any troubles(exceptions, etc). If error has been occurred during uploading to the AppStoreConnect, build should be fixed and pushed to the release branch.
4. After successful build uploading to the AppStoreConnect, Testing Team can start regression testing.
5. If build is approved by Testing Team, then build should be deliver to AppStore and get its approval. Othercase, fix issues, and send back to Testing Team for approval.
6. If build isn't approved by AppStore, fix issues and send new build to AppStoreConnect and then to the Testing Team.
7. When build is approved by AppStore, create release tag on the release candidate branch, merge all changes from release candidate branch to **master** and then remove the release candidate branch.
