//
//  ContainerKeyboardHandler.swift
//  FriendstaTools
//
//  Created by Elina Batyrova on 22/10/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

public protocol ContainerKeyboardHandler: KeyboardHandler { }

// MARK: -

public extension ContainerKeyboardHandler where Self: UIViewController {
    
    // MARK: - Instance Methods
    
    func onKeyboardWillShow(with notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        
        guard let keyboardFrame = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
            
        self.handle(keyboardHeight: keyboardFrame.height,
                    view: self.view)
    }
}
