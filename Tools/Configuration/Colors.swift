//
//  Colors.swift
//  Tools
//
//  Created by Elina Batyrova on 01/10/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

enum Colors {
    
    // MARK: - Type Properties
    
    static let darkBackground = UIColor(white: 0 / 255.0, alpha: 0.5)
    static let lightBackground = UIColor(white: 248 / 255.0, alpha: 0.82)
    static let darkActivityIndicator = UIColor(white: 128 / 255.0, alpha: 1.0)
}
