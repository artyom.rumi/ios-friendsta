//
//  EventListenerConnection.swift
//  Tools
//
//  Created by Almaz Ibragimov on 12.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public final class EventListenerConnection<T>: EventConnection {
    
    // MARK: - Instance Properties
    
    let event: Event<T>
    
    // MARK: - EventConnection
    
    public var isPaused = false
    
    // MARK: - Initializers
    
    init(event: Event<T>) {
        self.event = event
    }
    
    // MARK: - Instance Methods
    
    public func unpause() {
        self.isPaused = false
    }
    
    public func pause() {
        self.isPaused = true
    }
    
    public func dispose() {
        self.event.dispose(connection: self)
    }
}
