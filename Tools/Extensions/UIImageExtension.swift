//
//  UIImageExtension.swift
//  FriendstaTools
//
//  Created by Elina Batyrova on 14.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

public extension UIImage {
    
    // MARK: - Instance Methods
    
    func fixedOrientation() -> UIImage? {
        guard self.imageOrientation != UIImage.Orientation.up else {
            return self.copy() as? UIImage
        }
        
        guard let cgImage = self.cgImage else {
            return nil
        }
        
        guard let cgColorSpace = cgImage.colorSpace else {
            return nil
        }
        
        guard let cgContext = CGContext(data: nil,
                                        width: Int(self.size.width),
                                        height: Int(self.size.height),
                                        bitsPerComponent: cgImage.bitsPerComponent,
                                        bytesPerRow: 0,
                                        space: cgColorSpace,
                                        bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue) else {
            return nil
        }
        
        var transform: CGAffineTransform = .identity
        
        switch self.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: .pi)
        
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: .pi / 2)
            
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: self.size.height)
            transform = transform.rotated(by: .pi / -2)
            
        case .up, .upMirrored:
            break
        
        @unknown default:
            break
        }
        
        switch self.imageOrientation {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
            
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: self.size.height, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
    
        case .up, .down, .left, .right:
            break
            
        @unknown default:
            break
        }
        
        cgContext.concatenate(transform)
        
        switch self.imageOrientation {
        case .left, .leftMirrored, .right, .rightMirrored:
            cgContext.draw(cgImage, in: CGRect(x: 0, y: 0, width: self.size.height, height: self.size.width))
        
        default:
            cgContext.draw(cgImage, in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        }
        
        guard let resultCGImage = cgContext.makeImage() else {
            return nil
        }
        
        return UIImage.init(cgImage: resultCGImage, scale: 1, orientation: .up)
    }
}
