//
//  UIViewExtension.swift
//  Friendsta
//
//  Created by Nikita Asabin on 1/3/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

public extension UIView {
    
    // MARK: - Instance Methods
    
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}
