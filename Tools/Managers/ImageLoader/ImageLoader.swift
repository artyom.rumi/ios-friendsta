//
//  ImageLoader.swift
//  YouMakeUp
//
//  Created by Timur Shafigullin on 19/04/2019.
//  Copyright © 2019 Flatstack. All rights reserved.
//

import UIKit

public protocol ImageLoader {

    // MARK: - Instance Methods

    func loadImage(for url: URL, in imageView: UIImageView, placeholder: UIImage?, completionHandler: ((UIImage?) -> Void)?)
    func loadImage(for url: URL, in button: UIButton)
    func loadImage(for url: URL, completionHandler: @escaping (UIImage?) -> Void)
    func loadImageProgressive(for url: URL, in imageView: UIImageView)

    func saveImage(for url: URL, image: UIImage)

    func cancelLoading(in imageView: UIImageView)
}

// MARK: -

extension ImageLoader {

    // MARK: - Instance Methods

    public func loadImage(for url: URL, in imageView: UIImageView, placeholder: UIImage? = nil, completionHandler: ((UIImage?) -> Void)? = nil) {
        return self.loadImage(for: url, in: imageView, placeholder: placeholder, completionHandler: completionHandler)
    }
}
