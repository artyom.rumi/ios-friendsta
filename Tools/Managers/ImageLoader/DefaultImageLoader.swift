//
//  DefaultImageLoader.swift
//  YouMakeUp
//
//  Created by Timur Shafigullin on 03/03/2019.
//  Copyright © 2019 Flatstack. All rights reserved.
//

import Foundation
import Nuke

public class DefaultImageLoader: ImageLoader {

    // MARK: - Instance Properties

    private var progressiveImagesCache: [URL: UIImage] = [:]
    
    // MARK: - Initializers
    
    public init() {}

    // MARK: - Instance Methods

    public func loadImage(for url: URL, in imageView: UIImageView, placeholder: UIImage? = nil, completionHandler: ((UIImage?) -> Void)? = nil) {
        let options = ImageLoadingOptions(placeholder: placeholder)

        Nuke.loadImage(with: url, options: options, into: imageView, completion: { imageTask in
            switch imageTask {
            case .success(let imageResponse):
                completionHandler?(imageResponse.image)

            case .failure:
                completionHandler?(nil)
            }
        })
    }

    public func cancelLoading(in imageView: UIImageView) {
        Nuke.cancelRequest(for: imageView)
    }

    public func loadImage(for url: URL, in button: UIButton) {
        ImagePipeline.shared.loadImage(with: url, completion: { imageTask in
            switch imageTask {
            case .success(let imageResponse):
                button.setImage(imageResponse.image, for: .normal)

            case .failure(let error):
                Log.low("loadImage(for: URL, in: UIButton) error: \(error)", from: self)
            }
        })
    }

    public func loadImage(for url: URL, completionHandler: @escaping (UIImage?) -> Void) {
        ImagePipeline.shared.loadImage(with: url, completion: { imageTask in
            switch imageTask {
            case .success(let imageResponse):
                completionHandler(imageResponse.image)

            case .failure:
                completionHandler(nil)
            }
        })
    }

    public func loadImageProgressive(for url: URL, in imageView: UIImageView) {
        
//        print("------------- ------------------")
//        print(url)
        
        if let cachedImage = self.progressiveImagesCache[url] {
            imageView.image = cachedImage
        }

        var options = ImageLoadingOptions()

        options.pipeline = ImagePipeline {
            $0.isProgressiveDecodingEnabled = true
        }

        options.isPrepareForReuseEnabled = false

        Nuke.loadImage(with: url, options: options, into: imageView, progress: { intermediateResponse, completedUnitCount, totalUnitCount in
            if let response = intermediateResponse {
                self.progressiveImagesCache[url] = response.image
            }
        }, completion: { imageImask in
            self.progressiveImagesCache.removeValue(forKey: url)
        })
    }
  
    public func saveImage(for url: URL, image: UIImage) {
        let request = ImageRequest(url: url)
        ImageCache.shared[request] = image
    }
}
