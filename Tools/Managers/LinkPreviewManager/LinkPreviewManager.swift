//
//  LinkPreviewManager.swift
//  Tools
//
//  Created by Elina Batyrova on 02/10/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

public protocol LinkPreviewManager {
    
    // MARK: - Instance Methods
    
    func getInformationFrom(link: URL, completion: @escaping ((_ urlString: String?, _ descriptionString: String?, _ imageURLString: String?, Error?) -> Void))
}
