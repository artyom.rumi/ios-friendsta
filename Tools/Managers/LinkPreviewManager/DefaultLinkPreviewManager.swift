//
//  DefaultLinkPreviewManager.swift
//  Tools
//
//  Created by Elina Batyrova on 02/10/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import SwiftLinkPreview

public class DefaultLinkPreviewManager: LinkPreviewManager {
    
    // MARK: - Instance Properties
    
    private let previewer: SwiftLinkPreview!
    
    // MARK: - Initializers
    
    public init() {
        let session = URLSession.shared
        let workQueue = SwiftLinkPreview.defaultWorkQueue
        let responseQueue = DispatchQueue.main
        let cache = InMemoryCache()
        
        self.previewer = SwiftLinkPreview(session: session,
                                          workQueue: workQueue,
                                          responseQueue: responseQueue,
                                          cache: cache)
    }
    
    // MARK: - Instance Methods
    
    public func getInformationFrom(link: URL, completion: @escaping ((_ urlString: String?, _ descriptionString: String?, _ imageURLString: String?, Error?) -> Void)) {
        let linkString = link.absoluteString
        
        if let cachedData = self.previewer.cache.slp_getCachedResponse(url: linkString) {
            if let mainImageURLString = cachedData.image {
                completion(cachedData.canonicalUrl, cachedData.description, mainImageURLString, nil)
            } else {
                completion(cachedData.canonicalUrl, cachedData.description, cachedData.images?.first, nil)
            }
        } else {
            previewer.preview(linkString, onSuccess: { response in
                if let mainImageURLString = response.image {
                    completion(response.canonicalUrl, response.description, mainImageURLString, nil)
                } else {
                    completion(response.canonicalUrl, response.description, response.images?.first, nil)
                }
            }, onError: { error in
                completion(nil, nil, nil, error)
            })
        }
    }
}
