//
//  LoadingViewController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 13.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

public class LoadingViewController: LoggedViewController {
    
    // MARK: - Instance Properties
    
    fileprivate weak var boardView: UIView!
    
    fileprivate weak var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: - Initializers
    
    override init(nibName nibNameOrNil: String? = nil, bundle nibBundleOrNil: Bundle? = nil) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.modalPresentationStyle = .overFullScreen
        self.modalTransitionStyle = .crossDissolve
    }
    
    fileprivate func createBoardView() {
        let boardView = UIView()
        
        boardView.backgroundColor = Colors.lightBackground
        boardView.clipsToBounds = true
        
        boardView.layer.cornerRadius = 14.0
        boardView.layer.masksToBounds = true
        
        boardView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(boardView)
        
        NSLayoutConstraint.activate([boardView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0.0),
                                     boardView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0.0)])
        
        self.boardView = boardView
    }
    
    fileprivate func createActivityIndicatorView() {
        let activityIndicatorView = UIActivityIndicatorView(style: .whiteLarge)
        
        activityIndicatorView.startAnimating()
        
        activityIndicatorView.color = Colors.darkActivityIndicator
        activityIndicatorView.isUserInteractionEnabled = false
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(activityIndicatorView)
        
        NSLayoutConstraint.activate([activityIndicatorView.topAnchor.constraint(equalTo: self.boardView.topAnchor,
                                                                                constant: 36.0),
                                     activityIndicatorView.bottomAnchor.constraint(equalTo: self.boardView.bottomAnchor,
                                                                                   constant: -36.0),
                                     activityIndicatorView.leadingAnchor.constraint(equalTo: self.boardView.leadingAnchor,
                                                                                    constant: 36.0),
                                     activityIndicatorView.trailingAnchor.constraint(equalTo: self.boardView.trailingAnchor,
                                                                                     constant: -36.0)])
        
        self.activityIndicatorView = activityIndicatorView
    }
    
    // MARK: - UIViewController
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.createBoardView()
        self.createActivityIndicatorView()
        
        self.view.backgroundColor = Colors.darkBackground
    }
}
