//
//  NotificationService.swift
//  FriendstaNotificationServiceExtension
//
//  Created by Oleg Gorelov on 28/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {
    
    // MARK: - Instance Methods
    
    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        guard let requestContent = request.content.mutableCopy() as? UNMutableNotificationContent else {
            return contentHandler(request.content)
        }
        
        guard let userDefaults = UserDefaults(suiteName: Keys.applicationGroupID) else {
            return contentHandler(request.content)
        }
        
        var applicationBadgeNumber = userDefaults.integer(forKey: Keys.storageApplicationBadgeNumberKey)
        
        if let notificationBadgeNumber = requestContent.badge?.intValue {
            applicationBadgeNumber += notificationBadgeNumber
            
            userDefaults.set(applicationBadgeNumber, forKey: Keys.storageApplicationBadgeNumberKey)
        }
            
        requestContent.badge = NSNumber(value: applicationBadgeNumber)
            
        contentHandler(requestContent)
    }
}
