//
//  CommentDateFormatter.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

struct CommentDateFormatter {

    // MARK: - Instance Methods

    static func string(from date: Date) -> String? {
        let pastTime = Date().timeIntervalSince(date)

        guard pastTime > 0.0 else {
            return nil
        }

        let count: Int
        let suffix: String

        let weeks = pastTime / 604800.0

        if weeks >= 1.0 - Double.leastNonzeroMagnitude {
            count = max(Int(ceil(weeks - 1.0 / 7.0)), 1)
            suffix = "w".localized()
        } else {
            let days = pastTime / 86400.0

            if days >= 1.0 - Double.leastNonzeroMagnitude {
                count = max(Int(days), 1)
                suffix = "d".localized()
            } else {
                let hours = pastTime / 3600.0

                if hours >= 1.0 - Double.leastNonzeroMagnitude {
                    count = max(Int(hours), 1)
                    suffix = "h".localized()
                } else {
                    let minutes = pastTime / 60.0

                    if minutes >= 1.0 - Double.leastNonzeroMagnitude {
                        count = max(Int(minutes), 1)
                        suffix = "m".localized()
                    } else {
                        count = max(Int(pastTime), 1)
                        suffix = "s".localized()
                    }
                }
            }
        }

        return String(format: "%d%@", count, suffix.localized())
    }
}
