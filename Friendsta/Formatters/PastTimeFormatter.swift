//
//  PastTimeFormatter.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 12.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

class PastTimeFormatter {
    
    // MARK: - Type Properties
    
    static let shared = PastTimeFormatter()
    
    // MARK: - Instance Methods
    
    func string(fromDate date: Date) -> String? {
        let pastTime = Date().timeIntervalSince(date)
        
        guard pastTime > 0.0 else {
            return nil
        }
        
        let count: Int
        let suffix: String
        
        let weeks = pastTime / 604800.0
        
        if weeks >= 1.0 - Double.leastNonzeroMagnitude {
            count = max(Int(ceil(weeks - 1.0 / 7.0)), 1)
            suffix = "W"
        } else {
            let days = pastTime / 86400.0
            
            if days >= 1.0 - Double.leastNonzeroMagnitude {
                count = max(Int(days), 1)
                suffix = "D"
            } else {
                let hours = pastTime / 3600.0
                
                if hours >= 1.0 - Double.leastNonzeroMagnitude {
                    count = max(Int(hours), 1)
                    suffix = "H"
                } else {
                    let minutes = pastTime / 60.0
                    
                    if minutes >= 1.0 - Double.leastNonzeroMagnitude {
                        count = max(Int(minutes), 1)
                        suffix = "M"
                    } else {
                        count = max(Int(pastTime), 1)
                        suffix = "S"
                    }
                }
            }
        }
        
        return String(format: "%d%@ %@", count, suffix.localized(), "AGO".localized())
    }
}
