//
//  ChatSectionDateFormatter.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 26/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

class ChatSectionDateFormatter {
    
    // MARK: - Type Properties
    
    fileprivate static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "MMM d"
        
        return dateFormatter
    }()
    
    // MARK: -
    
    static let shared = ChatSectionDateFormatter()
    
    // MARK: - Instance Methods
    
    func string(from date: Date) -> String {
        let calendar = Calendar.current
        
        if calendar.isDateInYesterday(date) {
            return "Yesterday".localized()
        } else if calendar.isDateInToday(date) {
            return "Today".localized()
        } else {
            return ChatSectionDateFormatter.dateFormatter.string(from: date)
        }
    }
}
