//
//  PhoneNumberFormatter.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 10/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

class PhoneNumberFormatter {
    
    // MARK: - Nested Types
    
    fileprivate enum DomesticNumberFormat: String {
        
        // MARK: - Enumeration Cases
        
        case short = "XXX-XXXX"
        case long = "(XXX) XXX-XXXX"
        
        // MARK: - Instance Properties
        
        var minCount: Int {
            switch self {
            case .short:
                return 4
                
            case .long:
                return 8
            }
        }
        
        var maxCount: Int {
            switch self {
            case .short:
                return 7
                
            case .long:
                return 10
            }
        }
    }
    
    // MARK: - Type Properties
    
    static let shared = PhoneNumberFormatter()
    
    // MARK: - Instance Methods
    
    func countryCode(from phoneNumber: PhoneNumber) -> String {
        return phoneNumber.countryCode
    }
    
    func domesticNumber(from phoneNumber: PhoneNumber) -> String {
        let format: DomesticNumberFormat
        
        switch phoneNumber.domesticNumber.count {
        case DomesticNumberFormat.short.minCount...DomesticNumberFormat.short.maxCount:
            format = .short
            
        case DomesticNumberFormat.long.minCount...DomesticNumberFormat.long.maxCount:
            format = .long
            
        default:
            return phoneNumber.domesticNumber
        }
        
        var domesticNumber = ""
        
        var iteratorIndex = phoneNumber.domesticNumber.startIndex
        
        for character in format.rawValue {
            if iteratorIndex == phoneNumber.domesticNumber.endIndex {
                break
            }

            if character == "X" {
                domesticNumber.append(phoneNumber.domesticNumber[iteratorIndex])
                
                iteratorIndex = phoneNumber.domesticNumber.index(after: iteratorIndex)
            } else {
                domesticNumber.append(character)
            }
        }
        
        return domesticNumber
    }
    
    func string(from phoneNumber: PhoneNumber) -> String {
        return "\(self.countryCode(from: phoneNumber)) \(self.domesticNumber(from: phoneNumber))"
    }
}
