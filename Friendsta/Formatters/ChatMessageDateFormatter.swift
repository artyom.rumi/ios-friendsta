//
//  ChatMessageDateFormatter.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 22/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

class ChatMessageDateFormatter {
    
    // MARK: - Type Properties
    
    fileprivate static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.amSymbol = "AM"
        dateFormatter.pmSymbol = "PM"
        
        return dateFormatter
    }()
    
    // MARK: -
    
    static let shared = ChatMessageDateFormatter()
    
    // MARK: - Instance Methods
    
    func string(from date: Date) -> String {
        return ChatMessageDateFormatter.dateFormatter.string(from: date)
    }
}
