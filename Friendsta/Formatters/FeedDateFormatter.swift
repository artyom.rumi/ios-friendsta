//
//  FeedDateFormatter.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 13/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

class FeedDateFormatter {

    // MARK: - Type Properties

    static let shared = FeedDateFormatter()

    // MARK: - Instance Methods

    func string(from date: Date) -> String? {
        let calendar = Calendar.current

        if calendar.isDateInToday(date) {
            let dateComponents = calendar.dateComponents([.minute, .hour, .second], from: date, to: Date())

            guard let minute = dateComponents.minute, let hour = dateComponents.hour, let second = dateComponents.second else {
                return nil
            }

            switch minute {
            case 0:
                if second == 0 {
                    return "Now".localized()
                } else {
                    return String(format: "%ds".localized(), second)
                }

            case let minute where minute < 60:
                return String(format: "%dm".localized(), minute)

            default:
                return String(format: "%dh".localized(), hour)
            }
        } else {
            let dateComponents = calendar.dateComponents([.day], from: date, to: Date())

            guard let day = dateComponents.day else {
                return nil
            }

            if day == 0 && calendar.isDateInYesterday(date) {
                return String(format: "%dd".localized(), 1)
            }

            return String(format: "%dd".localized(), day)
        }
    }
}
