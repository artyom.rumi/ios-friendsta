//
//  MenuOptionView.swift
//  Friendsta
//
//  Created by Elina Batyrova on 19.11.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class MenuOptionControl: RoundControl {
    
    // MARK: - Nested Types
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let optionControlHeight = 30
    }
    
    // MARK: - Instance Properties
    
    private var imageView = UIImageView()
    
    // MARK: -

    var image: UIImage? {
        set {
            self.imageView.image = newValue
        }

        get {
            return self.imageView.image
        }
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect = CGRect.zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    private func initialize() {
        self.addSubview(self.imageView)
    }
    
    private func updateContent() {
        self.contentMode = .scaleAspectFit
        
        self.imageView.frame = CGRect(x: 0, y: 0, width: Constants.optionControlHeight, height: Constants.optionControlHeight)
        self.imageView.center = CGPoint(x: self.bounds.width / 2, y: self.bounds.height / 2)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.updateContent()
    }
}
