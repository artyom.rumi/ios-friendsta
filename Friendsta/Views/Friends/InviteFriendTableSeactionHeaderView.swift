//
//  InviteFriendTableViewHeader.swift
//  Friendsta
//
//  Created by Damir Zaripov on 18/11/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class InviteFriendTableSeactionHeaderView: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.bold(ofSize: 17.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
