//
//  AddFriendTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 26/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class AddFriendTableViewCell: UITableViewCell {

    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var avatarImageView: RoundImageView!
    
    @IBOutlet fileprivate weak var fullNameLabel: UILabel!
    @IBOutlet fileprivate weak var schoolInfoLabel: UILabel!
    
    @IBOutlet fileprivate weak var addButton: Button!
    @IBOutlet fileprivate weak var removeButton: Button!
    
    @IBOutlet fileprivate weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet fileprivate weak var separatorView: UIView!
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.fullNameLabel.font = Fonts.medium(ofSize: 17.0)
        self.schoolInfoLabel.font = Fonts.regular(ofSize: 13.0)
    }
    
    var onAddButtonClicked: (() -> Void)!
    var onRemoveButtonClicked: (() -> Void)!
    
    // MARK: -
    
    var avatarImage: UIImage? {
        get {
            return self.avatarImageView.image
        }
        
        set {
            self.avatarImageView.image = newValue
        }
    }
    
    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }
        
        set {
            self.fullNameLabel.text = newValue
        }
    }
    
    var schoolInfo: String? {
        get {
            return self.schoolInfoLabel.text
        }
        
        set {
            self.schoolInfoLabel.text = newValue
        }
    }
    
    var isCommunity = false {
        didSet {
            self.addButton.isHidden = self.isCommunity
            self.removeButton.isHidden = !self.isCommunity
        }
    }
    
    var isActivityIndicatorHidden: Bool {
        return self.activityIndicatorView.isHidden
    }
    
    var isLastRow: Bool {
        get {
            return self.separatorView.isHidden
        }
        
        set {
            self.separatorView.isHidden = newValue
        }
    }
    
    // MARK: -
    
    var avatarImageTarget: AnyObject {
        return self.avatarImageView
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onAddButtonTouchUpInside(sender: UIButton) {
        self.onAddButtonClicked()
    }
    
    @IBAction fileprivate func onRemoveButtonTouchUpInside(sender: UIButton) {
        self.onRemoveButtonClicked()
    }
    
    // MARK: -
    
    func showActivityIndicator() {
        self.addButton.isHidden = true
        self.removeButton.isHidden = true
        
        self.activityIndicatorView.startAnimating()
    }
    
    func hideActivityIndicator() {
        self.addButton.isHidden = self.isCommunity
        self.removeButton.isHidden = !self.isCommunity
        
        self.activityIndicatorView.stopAnimating()
    }
}
