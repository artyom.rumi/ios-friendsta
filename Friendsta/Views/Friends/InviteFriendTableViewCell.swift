//
//  InviteFriendTableViewCell.swift
//  Friendsta
//
//  Created by Marat Galeev on 05.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class InviteFriendTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var checkmarkView: CheckmarkView!
    @IBOutlet fileprivate weak var avatarImageView: RoundImageView!
    
    @IBOutlet fileprivate weak var fullNameLabel: UILabel!
    @IBOutlet fileprivate weak var phoneNumberLabel: UILabel!
    
    // MARK: -
    
    var avatarImage: UIImage? {
        get {
            return self.avatarImageView.image
        }
        
        set {
            self.avatarImageView.image = newValue
        }
    }
    
    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }
        
        set {
            self.fullNameLabel.text = newValue
        }
    }
    
    var phoneNumber: String? {
        get {
            return self.phoneNumberLabel.text
        }
        
        set {
            self.phoneNumberLabel.text = newValue
        }
    }
    
    var isCheckmarked: Bool {
        get {
            return self.checkmarkView.isSelected
        }
        
        set {
            self.checkmarkView.isSelected = newValue
        }
    }
    
    // MARK: -
    
    var avatarImageTarget: AnyObject {
        return self.avatarImageView
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.fullNameLabel.font = Fonts.medium(ofSize: 17.0)
        self.phoneNumberLabel.font = Fonts.regular(ofSize: 13.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
