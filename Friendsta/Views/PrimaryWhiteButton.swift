//
//  PrimaryWhiteButton.swift
//  Friendsta
//
//  Created by Nikita Asabin on 12/21/18.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class PrimaryWhiteButton: UIButton {
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.setTitleColor( Colors.blackText, for: .normal)
        self.backgroundColor = Colors.whiteBackground
        
        self.setTitleColor(Colors.blackText.withAlphaComponent(0.75), for: .highlighted)
        self.setBackgroundImage(Colors.whiteBackground.withAlphaComponent(0.75).image(), for: .highlighted)
        
        self.titleLabel?.font = Fonts.semiBold(ofSize: 17.0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.bounds.height/2
        self.layer.masksToBounds = true
    }

}
