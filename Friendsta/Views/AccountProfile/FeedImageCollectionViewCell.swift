//
//  FeedImageCollectionViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 13.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class FeedImageCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Instance Properties

    @IBOutlet private weak var feedImageView: RoundedImageView!
    
    // MARK: -
    
    var feedImageViewTarget: UIImageView {
        return self.feedImageView
    }
    
    var placeholder: UIImage? {
        didSet {
            self.setupPlaceholder()
        }
    }
    
    // MARK: - Instance Methods
    
    private func setupPlaceholder() {
        self.feedImageView.image = self.placeholder
    }
    
    // MARK: - UICollectionViewCell
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        let attribute = UICollectionViewLayoutAttributes()
        
        attribute.frame = self.frame
        
        return attribute
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()

        self.feedImageView.image = nil
    }
}
