//
//  AccountProfileInfoCollectionViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 06.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class AccountProfileInfoCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var avatarImageView: RoundImageView!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
     
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var schoolNameLabel: UILabel!
     
    @IBOutlet private weak var friendsCountLabel: UILabel!
     
    @IBOutlet private weak var bioInfoLabel: UILabel!
    
    // MARK: -
    
    private lazy var constraintWidth: NSLayoutConstraint = {
        let consraint = self.contentView.widthAnchor.constraint(equalToConstant: self.bounds.size.width)
        
        consraint.isActive = true
        
        return consraint
    }()
    
    // MARK: -
    
    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }
        
        set {
            self.fullNameLabel.text = newValue
        }
    }
    
    var schoolInfo: String? {
        get {
            return self.schoolNameLabel.text
        }
        
        set {
            self.schoolNameLabel.text = newValue
        }
    }
    
    var bioInfo: String? {
        get {
            return self.bioInfoLabel.text
        }
        
        set {
            self.bioInfoLabel.text = newValue
        }
    }
    
    var friendsCountAttributedText: NSAttributedString? {
        get {
            return self.friendsCountLabel.attributedText 
        }
        
        set {
            self.friendsCountLabel.attributedText = newValue
        }
    }
    
    var avatarImageViewTarget: UIImageView {
        return self.avatarImageView
    }
    
    // MARK: -
    
    var onEditButtonTapped: (() -> Void)?
    var onFriendsCountTapped: (() -> Void)?

    // MARK: - Instance Methods
    
    @IBAction private func onEditButtonTouchUpInside(_ sender: Any) {
        Log.high("onEditButtonTouchUpInside()", from: self)
        
        self.onEditButtonTapped?()
    }
    
    @IBAction func onFriendsCountTouchUpInside(_ sender: Any) {
        Log.high("onFriendsCountTouchUpInside()", from: self)
        
        self.onFriendsCountTapped?()
    }
    
    // MARK: -
    
    func showActivityIndicator() {
        self.activityIndicatorView.startAnimating()
    }
    
    func hideActivityIndicator() {
        self.activityIndicatorView.stopAnimating()
    }
    
    // MARK: - UICollectionViewCell
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        self.constraintWidth.constant = self.bounds.width

        return self.contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }
}
