//
//  SegmentedControlCollectionReusableView.swift
//  Friendsta
//
//  Created by Elina Batyrova on 11.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class SegmentedControlCollectionReusableView: UICollectionReusableView {
    
    // MARK: - Nested Types
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let timelineSegmentIndex = 0
        static let anonymousSegmentIndex = 1
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var customSegmentedControl: CustomSegmentedControl!
    
    // MARK: -
    
    var onTimelineSelected: (() -> Void)?
    var onAnonymousSelected: (() -> Void)?
    
    // MARK: - Instance Methods
    
    private func configureCustomSegmentedControl() {
        self.customSegmentedControl.onSegmentSelected = { index in
            if index == Constants.timelineSegmentIndex {
                self.onTimelineSelected?()
            } else if index == Constants.anonymousSegmentIndex {
                self.onAnonymousSelected?()
            }
        }
        
        self.customSegmentedControl.buttonImages = [nil, Images.anonymousSegmentIcon]
    }
    
    // MARK: - UICollectionReusableView
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.configureCustomSegmentedControl()
    }
}
