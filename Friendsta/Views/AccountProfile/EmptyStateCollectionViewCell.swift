//
//  EmptyStateCollectionViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 17.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class EmptyStateCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var messageLabel: UILabel!
    
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: -
    
    private lazy var constraintWidth: NSLayoutConstraint = {
        let consraint = self.contentView.widthAnchor.constraint(equalToConstant: self.bounds.size.width)
        
        consraint.isActive = true
        
        return consraint
    }()
    
    // MARK: -
    
    var message: String? {
        get {
            return self.messageLabel.text
        }
        
        set {
            self.messageLabel.text = newValue
        }
    }
    
    var isActivityIndicatorHidden: Bool {
        get {
            return self.activityIndicatorView.isHidden
        }
        
        set {
            self.activityIndicatorView.isHidden = newValue
        }
    }
    
    // MARK: - Instance Methods
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        self.constraintWidth.constant = self.bounds.width

        return self.contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }
}
