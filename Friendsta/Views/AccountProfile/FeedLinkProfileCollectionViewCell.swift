//
//  FeedLinkProfileCollectionViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 24.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class FeedLinkProfileCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var linkDescriptionLabel: UILabel!
    
    // MARK: -
    
    var linkDescription: String? {
        get {
            return self.linkDescriptionLabel.text
        }
        
        set {
            self.linkDescriptionLabel.text = newValue
        }
    }
    
    // MARK: - Instance Methods
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.linkDescriptionLabel.text = nil
    }
}
