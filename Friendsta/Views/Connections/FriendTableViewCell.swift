//
//  FriendTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 04/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class FriendTableViewCell: UITableViewCell {

    // MARK: - Instance Properties
    
    @IBOutlet private weak var avatarImageView: RoundImageView!
    
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var infoLabel: UILabel!
    @IBOutlet private weak var friendCountLabel: UILabel!
    @IBOutlet private weak var keepControl: KeepControl!
    @IBOutlet private weak var keepLoadingActivityIndicator: UIActivityIndicatorView!
    
    // MARK: -
    
    var onKeepTapped: (() -> Void)?
    var onAddTapped: (() -> Void)?
    
    // MARK: -
    
    var avatarImage: UIImage? {
        get {
            return self.avatarImageView.image
        }
        
        set {
            self.avatarImageView.image = newValue
        }
    }
    
    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }
        
        set {
            self.fullNameLabel.text = newValue
        }
    }
    
    var info: String? {
        get {
            return self.infoLabel.text
        }
        
        set {
            self.infoLabel.text = newValue
        }
    }

    var infoTextColor: UIColor {
        get {
            return self.infoLabel.textColor
        }

        set {
            self.infoLabel.textColor = newValue
        }
    }

    var friendCount: String? {
        get {
            return self.friendCountLabel.text
        }

        set {
            self.friendCountLabel.text = newValue
        }
    }

    var isFriendCountHidden: Bool {
        get {
            return self.friendCountLabel.isHidden
        }

        set {
            self.friendCountLabel.isHidden = newValue
        }
    }
    
    var friendshipStatus: FriendshipStatusType? {
        get {
            return self.keepControl.friendshipStatusType
        }
        
        set {
            self.keepControl.friendshipStatusType = newValue
        }
    }
    
    var isKeepLoadingActivityIndicatorAnimating: Bool? {
        get {
            return self.keepLoadingActivityIndicator.isAnimating
        }
        
        set {
            if newValue ?? false {
                self.keepLoadingActivityIndicator.startAnimating()
            } else {
                self.keepLoadingActivityIndicator.stopAnimating()
            }
        }
    }
    
    var isKeepControlHidden: Bool {
        get {
            return self.keepControl.isHidden
        }
        
        set {
            self.keepControl.isHidden = newValue
        }
    }
    
    // MARK: -
    
    var avatarImageViewTarget: UIImageView {
        return self.avatarImageView
    }

    var pokeButtonCenterTrailing: CGFloat {
        return self.layoutMargins.right + self.keepControl.frame.width / 2
    }
    
    // MARK: - Instance Properties
    
    @IBAction private func onKeepControlTouchUpInside(_ sender: Any) {
        Log.high("onKeepControlTouchUpInside()", from: self)
        
        if self.friendshipStatus == .some(.friend) {
            self.onKeepTapped?()
        } else {
            self.onAddTapped?()
        }
    }
}
