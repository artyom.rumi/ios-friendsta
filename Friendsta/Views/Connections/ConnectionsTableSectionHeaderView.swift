//
//  ConnectionsTableSeactionHeaderView.swift
//  Friendsta
//
//  Created by Дамир Зарипов on 04/06/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class ConnectionsTableSectionHeaderView: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    @IBOutlet private weak var showAllButton: UIButton!
    
    // MARK: -

    var onShowAllButtonClicked: (() -> Void)?
    
    // MARK: -
    
    var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }

    var subtitle: String? {
        get {
            return self.subtitleLabel.text
        }

        set {
            self.subtitleLabel.text = newValue
        }
    }
    
    var isShowAllButtonHidden: Bool {
        get {
            return self.showAllButton.isHidden
        }
        
        set {
            self.showAllButton.isHidden = newValue
        }
    }

    // MARK: - Instance Methods

    @IBAction private func onShowAllButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onShowAllButtonTouchUpInside()", from: self)

        self.onShowAllButtonClicked?()
    }
}
