//
//  CommunityTableViewCell.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 13/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class CommunityTableViewCell: UITableViewCell {

    // MARK: - Nested Types

    enum Constants {

        // MARK: - Type Properties

        static let addButtonTrailing: CGFloat = 13
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var avatarImageView: RoundImageView!

    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var schoolInfoLabel: UILabel!
    @IBOutlet private weak var addButton: UIButton!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!

    // MARK: -

    var onAddButtonTapped: (() -> Void)?

    // MARK: -

    var avatarImage: UIImage? {
        get {
            return self.avatarImageView.image
        }

        set {
            self.avatarImageView.image = newValue
        }
    }

    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }

        set {
            self.fullNameLabel.text = newValue
        }
    }

    var schoolInfo: String? {
        get {
            return self.schoolInfoLabel.text
        }

        set {
            self.schoolInfoLabel.text = newValue
        }
    }

    var addButtonImage: UIImage? {
        get {
            return self.addButton.image(for: .normal)
        }

        set {
            self.addButton.setImage(newValue, for: .normal)
        }
    }

    var isAddButtonHidden: Bool {
        get {
            return self.addButton.isHidden
        }

        set {
            self.addButton.isHidden = newValue
        }
    }

    // MARK: -

    var avatarImageViewTarget: UIImageView {
        return self.avatarImageView
    }

    var addButtonCenterTrailing: CGFloat {
        return self.addButton.frame.width / 2 + Constants.addButtonTrailing
    }

    // MARK: - Instance Properties

    @IBAction private func onAddButtonTouchUpInside(_ sender: Any) {
        self.onAddButtonTapped?()
    }

    // MARK: -

    func showActivityIndicator() {
        self.activityIndicator.startAnimating()
    }

    func hideActivityIndicator() {
        self.activityIndicator.stopAnimating()
    }
}
