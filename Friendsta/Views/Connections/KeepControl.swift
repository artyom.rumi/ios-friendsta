//
//  KeepControl.swift
//  Friendsta
//
//  Created by Elina Batyrova on 14.02.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

class KeepControl: RoundedControl {
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var iconImageView: UIImageView!
    
    // MARK: -
    
    var friendshipStatusType: FriendshipStatusType? {
        didSet {
            self.configureControl()
        }
    }
    
    // MARK: - Instance Methods
    
    private func configureControl() {
        guard let status = self.friendshipStatusType else {
            return
        }
        
        switch status {
        case .uninvited:
            self.iconImageView.image = nil
            self.titleLabel.text = "Add".localized()
            self.backgroundColor = Colors.keepControl
            self.isUserInteractionEnabled = true
            
        case .invited:
            self.iconImageView.image = UIImage(named: "SandhourIcon")
            self.titleLabel.text = "Added".localized()
            self.backgroundColor = Colors.lightGray
            self.isUserInteractionEnabled = false
            
        case .friend:
            self.iconImageView.image = nil
            self.titleLabel.text = "Keep".localized()
            self.backgroundColor = Colors.keepControl
            self.isUserInteractionEnabled = true
            
        case .affirmed:
            self.iconImageView.image = UIImage(named: "CheckmarkIcon")
            self.titleLabel.text = "Kept".localized()
            self.backgroundColor = Colors.lightGray
            self.isUserInteractionEnabled = false
        }
    }
}
