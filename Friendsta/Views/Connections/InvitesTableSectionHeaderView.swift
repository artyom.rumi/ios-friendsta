//
//  InvitesTableSectionHeaderView.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 17/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class InvitesTableSectionHeaderView: UITableViewCell {

    // MARK: - Nested Types

    private enum Constants {

        // MARK: - Type Properties

        static let text = "Hang on a moment… You are waiting for their friend requests!".localized()
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var titleLabel: UILabel!

    // MARK: - Instance Methods

    private func conigureTitleLabel() {
        let attributedString = NSMutableAttributedString(string: Constants.text)

        guard let hangMomentRange = Constants.text.nsRange(of: "Hang on a moment…".localized()) else {
            return
        }

        guard let friendRequestsRange = Constants.text.nsRange(of: "You are waiting for their friend requests!".localized()) else {
            return
        }

        attributedString.addAttribute(.font, value: Fonts.regular(ofSize: 16), range: hangMomentRange)
        attributedString.addAttribute(.font, value: Fonts.bold(ofSize: 16), range: friendRequestsRange)

        self.titleLabel.attributedText = attributedString
    }

    // MARK: - UITableViewCell

    override func awakeFromNib() {
        super.awakeFromNib()

        self.conigureTitleLabel()
    }
}
