//
//  ShowMoreTableViewCell.swift
//  Friendsta
//
//  Created by Nikita on 08/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class ShowMoreTableViewCell: UITableViewCell {

    // MARK: - Instance Properties

    @IBOutlet private weak var showMoreButton: UIButton!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!

    // MARK: -

    var showMoreButtonTitle: String? {
        get {
            return self.showMoreButton.titleLabel?.text
        }
        set {
            self.showMoreButton.setTitle(newValue, for: .normal)
        }
    }

    var onShowMoreTapped: (() -> Void)?

    // MARK: - Instance Methods

    @IBAction private func onShowMoreButtonTapped(_ sender: Any) {
        self.onShowMoreTapped?()
    }

    // MARK: -

    func showActivityIndicator() {
        self.showMoreButton.setTitleColor(Colors.clear, for: .normal)

        self.activityIndicatorView.startAnimating()
    }

    func hideActivityIndicator() {
        self.showMoreButton.setTitleColor(Colors.blackText, for: .normal)

        self.activityIndicatorView.stopAnimating()
    }
}
