//
//  OutlineContactTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 08/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

enum InvitationState {
    
    // MARK: - Type Properties
    
    case uninvited
    case recentlyInvited
    case invited
}

class OutlineContactTableViewCell: UITableViewCell {
    
    // MARK: - Nested Types
    
    enum Constants {
        
        // MARK: - Type Properties
        
        static let inviteOrResendStateButtonBackgroundImage = UIImage(named: "InviteOrResendButton")
        static let sentStateButtonBackgroundImage = UIImage(named: "SentButton")
        
        static let uninvitedStateButtonTitle = "Invite"
        static let recentlyInvitedStateButtonTitle = "Sent"
        static let invitedStateButtonTitle = "Resend"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var phoneNumberLabel: UILabel!
    @IBOutlet private weak var fullNameLabel: UILabel!
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet private weak var actionButton: UIButton!
    
    // MARK: -
    
    var phoneNumber: String? {
        get {
            return self.phoneNumberLabel.text
        }
        
        set {
            self.phoneNumberLabel.text = newValue
        }
    }
    
    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }
        
        set {
            self.fullNameLabel.text = newValue
        }
    }
    
    var invitationState: InvitationState = .uninvited {
        didSet {
            self.setUpButtonState()
        }
    }
    
    var onActionButtonTouchUpInside: (() -> Void)?
    
    // MARK: - Instance Methods
    
    @IBAction private func onActionButtonTouchUpInside(_ sender: Any) {
        Log.high("onActionButtonTouchUpInside()", from: self)
        
        self.onActionButtonTouchUpInside?()
    }
    
    // MARK: -
    
    private func setUpButtonState() {
        switch self.invitationState {
        case .invited:
            
            self.actionButton.setBackgroundImage(Constants.sentStateButtonBackgroundImage, for: .normal)
            self.actionButton.setTitle(Constants.recentlyInvitedStateButtonTitle, for: .normal)
            self.actionButton.setTitleColor(UIColor.black, for: .normal)
            self.actionButton.isUserInteractionEnabled = false
            /*
            self.actionButton.setBackgroundImage(Constants.inviteOrResendStateButtonBackgroundImage, for: .normal)
            self.actionButton.setTitle(Constants.invitedStateButtonTitle, for: .normal)
            self.actionButton.setTitleColor(UIColor.white, for: .normal)
            self.actionButton.isUserInteractionEnabled = true
            */
        case .recentlyInvited:
            self.actionButton.setBackgroundImage(Constants.sentStateButtonBackgroundImage, for: .normal)
            self.actionButton.setTitle(Constants.recentlyInvitedStateButtonTitle, for: .normal)
            self.actionButton.setTitleColor(UIColor.black, for: .normal)
            self.actionButton.isUserInteractionEnabled = false
            
        case .uninvited:
            self.actionButton.setBackgroundImage(Constants.inviteOrResendStateButtonBackgroundImage, for: .normal)
            self.actionButton.setTitle(Constants.uninvitedStateButtonTitle, for: .normal)
            self.actionButton.setTitleColor(UIColor.white, for: .normal)
            self.actionButton.isUserInteractionEnabled = true
        }
    }
    
    // MARK: -
    
    func showActivityIndicator() {
        self.activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        self.activityIndicator.stopAnimating()
    }
}
