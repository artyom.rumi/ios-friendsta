//
//  AllowContactTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 02/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class AllowContactTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    var onAllowContactButtonTapped: (() -> Void)?
    
    // MARK: - Instance Methods
    
    @IBAction func onAllowContactButtonTouchUpInside(_ sender: Any) {
        Log.high("onAllowContactButtonTouchUpInside", from: self)
        
        self.onAllowContactButtonTapped?()
    }
}
