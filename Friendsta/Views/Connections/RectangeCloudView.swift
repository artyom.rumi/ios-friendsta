//
//  RectangeCloudView.swift
//  Friendsta
//
//  Created by Elina Batyrova on 18/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

@IBDesignable class RectangeCloudView: UIView {
    
    // MARK: - Nested Types
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let pointerBottomWidth: CGFloat = 30.0
    }
    
    // MARK: - Instance Properties
    
    private let path = UIBezierPath()
    private let gradient = CAGradientLayer()
    
    private let actionButton = UIButton()
    private let titleLabel = UILabel()
    
    // MARK: -
    
    var gradientColors: [CGColor]? {
        set {
            self.gradient.colors = newValue
        }
        
        get {
            return self.gradient.colors as? [CGColor]
        }
    }
    
    var title: String? {
        set {
            self.titleLabel.text = newValue
        }
        
        get {
            return self.titleLabel.text
        }
    }
    
    var buttonTitle: String? {
        set {
            self.actionButton.setTitle(newValue, for: .normal)
        }
        
        get {
            return self.actionButton.title(for: .normal)
        }
    }
    
    var onActionButtonClicked: (() -> Void)?
    
    var pointerTrailingLength: CGFloat = 50.0
    
    var isGradientHorizontal: Bool = true
    
    ///// ********* Lance's Code ********* /////
       var shouldFlipVertically: Bool = false {
           didSet {
               
               if shouldFlipVertically {
                   
                   self.rectangeCloudVewFlipY()
                   
                   titleLabel.flipY()
                   actionButton.flipY()
               }
           }
       }
       ///// ********* Lance's Code ********* /////
    
    // MARK: - Instance Methods
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.gradient.removeFromSuperlayer()
        self.path.removeAllPoints()
        
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: self.frame.size.height / 1.5))
        path.addLine(to: CGPoint(x: self.frame.size.width - self.pointerTrailingLength - Constants.pointerBottomWidth / 2,
                                 y: self.frame.size.height / 1.5))
        path.addLine(to: CGPoint(x: self.frame.size.width - self.pointerTrailingLength,
                                 y: self.frame.size.height))
        path.addLine(to: CGPoint(x: self.frame.size.width - self.pointerTrailingLength + Constants.pointerBottomWidth / 2,
                                 y: self.frame.size.height / 1.5))
        path.addLine(to: CGPoint(x: self.frame.size.width, y: self.frame.size.height / 1.5))
        path.addLine(to: CGPoint(x: self.frame.size.width, y: 0.0))
        path.close()

        if self.isGradientHorizontal {
            self.gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            self.gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        } else {
            self.gradient.startPoint = CGPoint(x: 0.5, y: 0.5)
            self.gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        }

        let shapeMask = CAShapeLayer()
        shapeMask.path = path.cgPath

        self.gradient.frame = path.bounds
        self.gradient.mask = shapeMask

        self.layer.addSublayer(self.gradient)
        
        self.configureTitleLabel()
        self.configureActionButton()
    }
    
    // MARK: -
    
    private func configureTitleLabel() {
        self.titleLabel.removeFromSuperview()
        
        self.titleLabel.font = Fonts.medium(ofSize: 16.0)
        self.titleLabel.textColor = UIColor.white
        self.titleLabel.numberOfLines = 0
        self.titleLabel.textAlignment = .center
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.titleLabel)
        
        NSLayoutConstraint.activate([self.titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8.0),
                                     self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 0.0),
                                     self.titleLabel.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 2 / 3),
                                     self.titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -70)])
    }
    
    private func configureActionButton() {
        self.actionButton.removeFromSuperview()
        
        self.actionButton.titleLabel?.font = Fonts.bold(ofSize: 16.0)
        self.actionButton.titleLabel?.textColor = UIColor.white
        self.actionButton.translatesAutoresizingMaskIntoConstraints = false
        self.actionButton.addTarget(self, action: #selector(self.onActionButtonTouchUpInside(sender:)), for: .touchUpInside)
        
        self.addSubview(self.actionButton)
        
        NSLayoutConstraint.activate([self.actionButton.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0),
                                     self.actionButton.topAnchor.constraint(equalTo: self.topAnchor, constant: 0.0),
                                     self.actionButton.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 2 / 3),
                                     self.actionButton.leadingAnchor.constraint(equalTo: self.titleLabel.trailingAnchor, constant: 6.0)])
    }
    
    @objc private func onActionButtonTouchUpInside(sender: UIButton) {
        Log.high("onActionButtonTouchUpInside()", from: self)
        
        self.onActionButtonClicked?()
    }
    
    ///// ********* Lance's Code ********* /////

    /// Flip view horizontally.
    func rectangeCloudVewFlipX() {
        transform = CGAffineTransform(scaleX: -transform.a, y: transform.d)
    }

    /// Flip view vertically.
    func rectangeCloudVewFlipY() {
        transform = CGAffineTransform(scaleX: transform.a, y: -transform.d)
    }
    
    ///// ********* Lance's Code ********* /////
}
