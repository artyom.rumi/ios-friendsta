//
//  PhotoPreviewView.swift
//  Friendsta
//
//  Created by Elina Batyrova on 20.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class PhotoPreviewView: NibLoadingView {
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var menuStackView: UIStackView!
    @IBOutlet private weak var bottomRightButton: UIButton!
    @IBOutlet private weak var topRightButton: UIButton!
    
    // MARK: -
    
    var imageViewTarget: UIImageView {
        return self.imageView
    }
    
    var bottomRightButtonImage: UIImage? {
        get {
            return self.bottomRightButton.image(for: .normal)
        }
        
        set {
            self.bottomRightButton.setImage(newValue, for: .normal)
        }
    }
    
    var topRightButtonImage: UIImage? {
        get {
            return self.topRightButton.image(for: .normal)
        }
        
        set {
            self.topRightButton.setImage(newValue, for: .normal)
        }
    }
    
    var isBottomRightButtonHidden: Bool = true {
        didSet {
            self.configureButtons()
        }
    }
    
    var isTopRightButtonHidden: Bool = true {
        didSet {
            self.configureButtons()
        }
    }
    
    var menuItems: [MenuItemView] = [] {
        didSet {
            self.configureMenu()
        }
    }
    
    var onBottomRightButtonTapped: (() -> Void)?
    var onTopRightButtonTapped: (() -> Void)?
        
    // MARK: - Instance Methods
    
    @IBAction private func onBottomRightButtonTouchUpInside(_ sender: Any) {
        Log.high("onBottomRightButtonTouchUpInside()", from: self)
        
        self.onBottomRightButtonTapped?()
    }
    
    @IBAction private func onTopRightButtonTouchUpInside(_ sender: Any) {
        Log.high("onTopRightButtonTouchUpInside()", from: self)
        
        self.onTopRightButtonTapped?()
    }
    
    // MARK: -
    
    private func configureMenu() {
        self.menuStackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        
        for menuItem in self.menuItems {
            self.menuStackView.addArrangedSubview(menuItem)
        }
    }
    
    private func configureButtons() {
        self.topRightButton.isHidden = self.isTopRightButtonHidden
        self.bottomRightButton.isHidden = self.isBottomRightButtonHidden
        
        self.topRightButton.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
        self.bottomRightButton.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.configureButtons()
        
    }
}
