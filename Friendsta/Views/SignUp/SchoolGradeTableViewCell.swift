//
//  SchoolGradeTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 16/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class SchoolGradeTableViewCell: UITableViewCell {

    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var numberLabel: UILabel!
    @IBOutlet fileprivate weak var classYearLabel: UILabel!
    
    // MARK: -
    
    var number: Int? {
        get {
            guard let text = self.numberLabel.text else {
                return nil
            }
            
            return Int(text)
        }
        
        set {
            if let number = newValue {
                self.numberLabel.text = String(format: "%d th grade".localized(), number)
            } else {
                self.numberLabel.text = nil
            }
        }
    }
    
    var classYear: Int? {
        get {
            guard let text = self.classYearLabel.text else {
                return nil
            }
            
            return Int(text)
        }
        
        set {
            if let classYear = newValue {
                self.classYearLabel.text = String(format: "Class of %d".localized(), classYear)
            } else {
                self.classYearLabel.text = nil
            }
        }
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.numberLabel.font = Fonts.regular(ofSize: 17.0)
        self.classYearLabel.font = Fonts.regular(ofSize: 17.0)
    }
}
