//
//  SchoolGradeTableSectionHeaderView.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 16/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class SchoolGradeTableSectionHeaderView: UITableViewCell {

    // MARK: - Instance Properties

    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    // MARK: -
    
    var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        titleLabel.font = Fonts.bold(ofSize: 17.0)
    }
}
