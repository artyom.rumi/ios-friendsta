//
//  SharePropsButton.swift
//  Friendsta
//
//  Created by Marat Galeev on 27.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable final class SharePropsButton: CellButton {
    
    // MARK: - Initializers
    
    override init(frame: CGRect = CGRect.zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.image = UIImage(named: "ShareIcon",
                             in: Bundle(for: self.classForCoder),
                             compatibleWith: nil)
        
        self.title = "Share Props".localized()
    }
}
