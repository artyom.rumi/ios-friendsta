//
//  CheckmarkView.swift
//  Friendsta
//
//  Created by Marat Galeev on 27.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class CheckmarkView: UIView {
    
    // MARK: - Instance Properties
    
    fileprivate var imageView: UIImageView = UIImageView()
    
    // MARK: -
    
    @IBInspectable var onStateImage: UIImage? {
        return UIImage(named: "CheckmarkOn",
                       in: Bundle(for: self.classForCoder),
                       compatibleWith: nil)
    }
    
    @IBInspectable var offStateImage: UIImage? {
        return UIImage(named: "CheckmarkOff",
                       in: Bundle(for: self.classForCoder),
                       compatibleWith: nil)
    }
    
    @IBInspectable var isSelected = false {
        didSet {
            self.applyState()
        }
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.imageView.contentMode = .scaleAspectFit
        
        self.addSubview(self.imageView)
        
        self.applyState()
    }
    
    fileprivate func applyState() {
        if self.isSelected {
            self.imageView.image = self.onStateImage
        } else {
            self.imageView.image = self.offStateImage
        }
        
        self.setNeedsLayout()
    }
    
    // MARK: - UIView
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let imageViewSize = self.imageView.image?.size ?? .zero
        
        return CGSize(width: min(imageViewSize.width, size.width),
                      height: min(imageViewSize.height, size.width))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let imageViewSize = self.imageView.image?.size ?? .zero
        
        self.imageView.frame = CGRect(x: (self.bounds.width - imageViewSize.width) * 0.5,
                                      y: (self.bounds.height - imageViewSize.height) * 0.5,
                                      size: imageViewSize).adjusted
    }
}
