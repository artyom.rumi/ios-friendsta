//
//  CellButton.swift
//  Friendsta
//
//  Created by Marat Galeev on 27.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class CellButton: UIControl {
    
    // MARK: - Instance Properties
    
    fileprivate var imageView = UIImageView()
    fileprivate let titleLabel = UILabel()
    
    // MARK: -
    
    @IBInspectable var image: UIImage? {
        get {
            return self.imageView.image
        }

        set {
            self.imageView.image = newValue
        }
    }
    
    @IBInspectable var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    // MARK: - UIControl
    
    override var isHighlighted: Bool {
        didSet {
            self.applyState()
        }
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.imageView.contentMode = .scaleAspectFit
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.imageView)
        
        self.titleLabel.font = Fonts.medium(ofSize: 17.0)
        self.titleLabel.textColor = Colors.primary
        self.titleLabel.numberOfLines = 1
        self.titleLabel.textAlignment = .left
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.titleLabel)
    }
    
    fileprivate func applyState() {
        if self.isHighlighted {
            self.backgroundColor = Colors.grayBackground
        } else {
            self.backgroundColor = Colors.whiteText
        }
    }
    
    // MARK: - UIView
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let imageViewHeight = self.imageView.bounds.size.height
        let imageViewWidth = imageViewHeight
        
        self.imageView.frame = CGRect(x: self.layoutMargins.left,
                                      y: (self.bounds.size.height - imageViewHeight) * 0.5,
                                      width: imageViewWidth,
                                      height: imageViewHeight).adjusted
        
        let titleLabelSize = self.titleLabel.sizeThatFits(self.bounds.size)
        
        self.titleLabel.frame = CGRect(x: self.imageView.frame.size.width + 12.0,
                                       y: (self.bounds.size.height - self.titleLabel.frame.height) * 0.5,
                                       width: titleLabelSize.width,
                                       height: titleLabelSize.height).adjusted
    }
}
