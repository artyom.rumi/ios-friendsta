//
//  GraduatedTableViewCell.swift
//  Friendsta
//
//  Created by Marat on 27.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class GraduatedTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var pickedTextView: PickedTextView!
    @IBOutlet fileprivate weak var classYearLabel: UILabel!
    @IBOutlet fileprivate weak var alreadyGraduatedLabel: UILabel!
    
    // MARK: -
    
    var classYear: String? {
        get {
            return self.classYearLabel.text
        }
        
        set {
            self.classYearLabel.text = newValue
        }
    }
    
    var classYears: [Int] = [] {
        didSet {
            self.pickedTextView.pickerValues = self.classYears.compactMap({ year in
                return String(year)
            })
        }
    }
    
    var onPickerValueChanged: ((_ classYear: String) -> Void)?
    var onDoneButtonClicked: (() -> Void)?
    var onCancelButtonClicked: (() -> Void)?
    
    // MARK: - UIView
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.initialize()
        self.setupFont()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.pickedTextView.hasInputToolbar = true
        
        self.pickedTextView.onPickerValueChanged = { [unowned self] value in
            self.onPickerValueChanged?(value)
        }
        
        self.pickedTextView.onInputDoneButtonClicked = {
            self.onDoneButtonClicked?()
        }
        
        self.pickedTextView.onInputCancelButtonClicked = {
            self.onCancelButtonClicked?()
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.pickedTextView.font = Fonts.regular(ofSize: 17.0)
        self.alreadyGraduatedLabel.font = Fonts.regular(ofSize: 17.0)
        self.classYearLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: - UIView
    
    override func becomeFirstResponder() -> Bool {
        if !super.becomeFirstResponder() {
            return self.pickedTextView.becomeFirstResponder()
        }
        
        return true
    }
}
