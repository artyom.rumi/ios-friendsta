//
//  BoyGenderButton.swift
//  Friendsta
//
//  Created by Marat Galeev on 19.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable final class BoyGenderButton: GenderButton {
    
    // MARK: - Initializers
    
    override init(frame: CGRect = CGRect.zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.image = UIImage(named: "GenderBoy",
                             in: Bundle(for: self.classForCoder),
                             compatibleWith: nil)
        
        self.title = "Boy".localized()
    }
}
