//
//  GenderButton.swift
//  Friendsta
//
//  Created by Marat Galeev on 19.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class GenderButton: UIControl {
    
    // MARK: - Instance Properties
    
    fileprivate var imageView = UIImageView()
    fileprivate let titleLabel = UILabel()
    
    // MARK: -
    
    @IBInspectable var image: UIImage? {
        get {
            return self.imageView.image
        }
        
        set {
            self.imageView.image = newValue
        }
    }
    
    @IBInspectable var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    // MARK: - UIControl
    
    override var isHighlighted: Bool {
        didSet {
            self.applyState()
        }
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.layer.borderWidth = 2.0
        self.layer.borderColor = Colors.grayBackground.cgColor
        self.layer.cornerRadius = 9.0
        
        self.imageView.contentMode = .scaleAspectFit
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.imageView)
        
        self.titleLabel.font = Fonts.regular(ofSize: 17.0)
        self.titleLabel.textColor = Colors.grayText
        self.titleLabel.numberOfLines = 1
        self.titleLabel.textAlignment = .center
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.titleLabel)
        
        self.applyState()
    }
    
    fileprivate func applyState() {
        if self.isHighlighted {
            self.backgroundColor = Colors.grayBackground
            self.layer.borderWidth = 0.0
        } else {
            self.backgroundColor = Colors.whiteText
            self.layer.borderWidth = 2.0
        }
    }
    
    // MARK: - UIView
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let titleLabelSize = self.titleLabel.sizeThatFits(self.bounds.size)
        let titleLabelBottom = self.bounds.height - self.bounds.height * 20.0 / 170.0
        
        self.titleLabel.frame = CGRect(x: (self.bounds.width - titleLabelSize.width) * 0.5,
                                       y: titleLabelBottom - titleLabelSize.height,
                                       width: titleLabelSize.width,
                                       height: titleLabelSize.height).adjusted
        
        let imageViewSize = self.imageView.image?.size ?? .zero
        
        self.imageView.frame = CGRect(x: (self.bounds.width - imageViewSize.width) * 0.5,
                                      y: (self.titleLabel.frame.top - imageViewSize.height) * 0.5,
                                      size: imageViewSize).adjusted
    }
}
