//
//  SchoolTableViewCell.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class SchoolTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var locationLabel: UILabel!
    @IBOutlet fileprivate weak var memberCountLabel: UILabel!
    @IBOutlet fileprivate weak var membersLabel: UILabel!
    
    var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    var location: String? {
        get {
            return self.locationLabel.text
        }
        
        set {
            self.locationLabel.text = newValue
        }
    }
    
    var memberCount: Int? {
        get {
            guard let text = self.memberCountLabel.text else {
                return nil
            }
            
            return Int(text)
        }
        
        set {
            if let newValue = newValue {
                self.memberCountLabel.text = String(newValue)
            } else {
                self.memberCountLabel.text = nil
            }
        }
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.regular(ofSize: 17.0)
        self.locationLabel.font = Fonts.regular(ofSize: 13.0)
        self.memberCountLabel.font = Fonts.semiBold(ofSize: 17.0)
        self.membersLabel.font = Fonts.regular(ofSize: 13.0)
    }
}
