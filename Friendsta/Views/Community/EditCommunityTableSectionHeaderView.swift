//
//  EditCommunityTableSectionHeaderView.swift
//  Friendsta
//
//  Created by Rinat Mukhammetzyanov on 06/09/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class EditCommunityTableSectionHeaderView: UITableViewCell {

    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var editButton: UIButton!
    
    // MARK: -
    
    var onEditButtonClicked: (() -> Void)!
    
    // MARK: - Instance Methods

    @IBAction fileprivate func onEditButtonTouchUpInside(_ sender: UIButton) {
        self.onEditButtonClicked()
    }
    
    // MARK: -
    
    func showEditButton() {
        self.editButton.isHidden = false
    }
    
    func hideEditButton() {
        self.editButton.isHidden = true
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.editButton.titleLabel?.font = Fonts.semiBold(ofSize: 13.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
