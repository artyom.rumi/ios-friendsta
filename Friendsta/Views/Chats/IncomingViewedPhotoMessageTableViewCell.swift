//
//  IncomingViewedPhotoMessageTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 17/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class IncomingViewedPhotoMessageTableViewCell: UITableViewCell {

    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var dateLabel: UILabel!
    @IBOutlet fileprivate weak var photoLabel: UILabel!
    
    // MARK: -

    var date: String? {
        get {
            return self.dateLabel.text
        }
        
        set {
            self.dateLabel.text = newValue
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.dateLabel.font = Fonts.regular(ofSize: 11.0)
        self.photoLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
