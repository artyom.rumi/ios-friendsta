//
//  WelcomeInfoMessageTableViewCell.swift
//  Friendsta
//
//  Created by Damir Zaripov on 16/11/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class WelcomeInfoMessageTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var explanationLabel: UILabel!
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.heavy(ofSize: 22.0)
        self.explanationLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
