//
//  ChatmateTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 15.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

class ChatmateTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet private(set) weak var avatarImageView: UIImageView!
    
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var schoolInfoLabel: UILabel!
    
    @IBOutlet private weak var checkmarkView: RoundView!
    
    // MARK: -
    
    var fullName: String? {
        set {
            self.fullNameLabel.text = newValue
        }
        
        get {
            return self.fullNameLabel.text
        }
    }
    
    var schoolInfo: String? {
        set {
            self.schoolInfoLabel.text = newValue
        }
        
        get {
            return self.schoolInfoLabel.text
        }
    }
    
    var isChoosen: Bool {
        get {
            return !self.checkmarkView.isHidden
        }
        
        set {
            self.checkmarkView.isHidden = !newValue
        }
    }
    
    // MARK: - UITableViewCell
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.avatarImageView.image = nil
    }
}
