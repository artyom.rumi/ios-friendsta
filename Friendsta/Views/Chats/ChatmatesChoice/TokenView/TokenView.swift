//
//  TokenView.swift
//  TokenView
//
//  Created by James Hickman on 8/11/15.
/*
Copyright (c) 2015 Appmazo, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.//
*/

import UIKit

class TokenView: RoundedView {
    
    // MARK: - Nested Types
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let tokenDeleteKey = "NWSTokenDeleteKey"
    }
    
    // MARK: - Instance Properties
    
    var hiddenTextView = UITextView()
    var isSelected: Bool = false

    // MARK: - Instance Methods
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.hiddenTextView.isHidden = true
        self.hiddenTextView.text = Constants.tokenDeleteKey
        self.hiddenTextView.autocorrectionType = UITextAutocorrectionType.no
        
        self.addSubview(hiddenTextView)
    }
}
