//
//  UserTokenView.swift
//  Nicely
//
//  Created by Nikita Asabin on 11/23/18.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class UserTokenView: TokenView {

    // MARK: - Instance properties
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    // MARK: - Initializers
    
    class func initWithTitle(_ title: String?, imageURL: URL? = nil) -> UserTokenView? {
        if let tokenView = UINib(nibName: "UserTokenView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? UserTokenView {
            let oldTextWidth = tokenView.titleLabel.bounds.width
            
            tokenView.titleLabel.text = title
            tokenView.titleLabel.sizeToFit()
            
            let newTextWidth = tokenView.titleLabel.bounds.width
            
            tokenView.imageView.image = Images.avatarSmallPlaceholder
            
            if let imageURL = imageURL {
                Managers.imageLoader.loadImage(for: imageURL, completionHandler: { image in
                    tokenView.imageView.image = image
                })
            }
            
            tokenView.frame.size = CGSize(width: tokenView.frame.size.width + (newTextWidth - oldTextWidth), height: tokenView.frame.height)
            tokenView.setNeedsLayout()
            tokenView.frame = tokenView.frame
            
            return tokenView
        }
        
        return nil
    }
}
