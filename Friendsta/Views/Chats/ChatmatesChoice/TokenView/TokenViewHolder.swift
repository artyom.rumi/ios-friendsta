//
//  TokenViewHolder.swift
//  TokenView
//
//  Created by James Hickman on 8/11/15.
/*
Copyright (c) 2015 Appmazo, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.//
*/

import UIKit

open class TokenViewHolder: UIView, UIScrollViewDelegate {
    
    // MARK: - Nested Types
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let textViewMinimumWidth: CGFloat = 30.0
        static let textViewMinimumHeight: CGFloat = 36.0
        
        static let tokenDeleteKey = "NWSTokenDeleteKey"
    }
    
    // MARK: - Instance Properties
    
    private var tokens: [TokenView] = []
    private var selectedToken: TokenView?
    private var tokenViewInsets: UIEdgeInsets = UIEdgeInsets.init(top: 0, left: 0, bottom: 5, right: 5)
    
    private var shouldBecomeFirstResponder: Bool = false
    private var scrollView = UIScrollView()
    private var lastTokenCount = 0
    private var lastText = ""
    
    // MARK: -
    
    var dataSource: TokenViewDataSource?
    var delegate: TokenViewDelegate?
    
    var textView = UITextView()
    var tokenHeight: CGFloat = 30.0
    
    // MARK: - Instance Methods
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        let contentSize = self.scrollView.contentSize
        
        self.scrollView.contentSize = CGSize(width: self.scrollView.bounds.width, height: contentSize.height)
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()

        self.scrollView.backgroundColor = UIColor.clear
        self.scrollView.isScrollEnabled = true
        self.scrollView.isUserInteractionEnabled = true
        self.scrollView.autoresizesSubviews = false
        
        self.addSubview(self.scrollView)

        self.textView.backgroundColor = UIColor.clear
        self.textView.textColor = UIColor.black
        self.textView.delegate = self
        self.textView.isScrollEnabled = false
        self.textView.autocorrectionType = UITextAutocorrectionType.no
        
        self.scrollView.addSubview(self.textView)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint(item: self.scrollView, attribute: NSLayoutConstraint.Attribute.left, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.left, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self.scrollView, attribute: NSLayoutConstraint.Attribute.right, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.right, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self.scrollView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0).isActive = true
        NSLayoutConstraint(item: self.scrollView, attribute: NSLayoutConstraint.Attribute.bottom, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1.0, constant: 0).isActive = true
    }
    
    // MARK: -
    
    private func resetTokenView() {
        for token in self.tokens {
            token.removeFromSuperview()
        }
        
        self.tokens = []
        self.tokenHeight = 0
        
        if self.textView.text != self.dataSource?.titleForTokenViewPlaceholder(self) {
            self.lastText = self.textView.text
        }
    }
    
    private func setupTextView(offsetX x: inout CGFloat, offsetY y: inout CGFloat, remainingWidth: inout CGFloat) {
        self.textView.tintColor = self.dataSource?.tintColorForTokenView(self)
        self.textView.font = self.dataSource?.fontForTokenView(self)
        
        if self.tokens.isEmpty && self.lastText.isEmpty && !self.textView.isFirstResponder {
            if let placeholderText = self.dataSource?.titleForTokenViewPlaceholder(self) {
                self.textView.text = placeholderText
                self.textView.textColor = self.dataSource?.fontColorForTokenViewPlaceholder(self)
            }
        } else {
            self.textView.textColor = UIColor.black
            self.textView.text = ""
        }
        
        if remainingWidth >= Constants.textViewMinimumWidth {
            
            self.textView.frame = CGRect(x: x + self.tokenViewInsets.left, y: y, width: remainingWidth - self.tokenViewInsets.left - self.tokenViewInsets.right, height: max(Constants.textViewMinimumHeight, self.scrollView.frame.height))
            remainingWidth = self.scrollView.bounds.width - x - self.textView.frame.width
        } else {
            remainingWidth = self.scrollView.bounds.width
            
            x = 0
            y += max(Constants.textViewMinimumHeight, self.tokenHeight)
            
            self.textView.frame = CGRect(x: x + self.tokenViewInsets.left, y: y - self.tokenViewInsets.top, width: remainingWidth - self.tokenViewInsets.left - self.tokenViewInsets.right, height: max(Constants.textViewMinimumHeight, self.scrollView.frame.height))
        }
        
        self.textView.returnKeyType = UIReturnKeyType.next
    }
    
    private func setupToken(_ token: inout TokenView, atIndex index: Int, withOffsetX x: inout CGFloat, withOffsetY y: inout CGFloat, remainingWidth: inout CGFloat) {

        self.tokens.append(token)

        token.hiddenTextView.delegate = self

        token.tag = index
        token.hiddenTextView.tag = index
        
        self.tokenHeight = token.frame.height

        if remainingWidth <= self.tokenViewInsets.left + token.frame.width + self.tokenViewInsets.right && self.tokens.count > 1 {
            x = 0
            y += token.frame.height + self.tokenViewInsets.top
        }
        token.frame = CGRect(x: x+self.tokenViewInsets.left, y: y + 3, width: min(token.bounds.width, self.scrollView.bounds.width-x-self.tokenViewInsets.left-self.tokenViewInsets.right), height: token.bounds.height)
        
        self.scrollView.addSubview(token)

        x += self.tokenViewInsets.left + token.frame.width
        remainingWidth = self.scrollView.bounds.width - x
    }
    
     // MARK: -
    
    func reloadData() {
        UIView.setAnimationsEnabled(false)
        
        self.resetTokenView()
        
        if let insets = self.dataSource?.insetsForTokenView(self) {
            self.tokenViewInsets = insets
        }
        
        var scrollViewOriginX: CGFloat = self.tokenViewInsets.left
        var scrollViewOriginY: CGFloat = self.tokenViewInsets.top
        
        var remainingWidth = self.scrollView.bounds.width
        
        let numOfTokens: Int = (dataSource?.numberOfTokensForTokenView(self)) ?? 0
        for index in 0..<numOfTokens {
            if var token = dataSource?.tokenView(self, viewForTokenAtIndex: index) as? TokenView {
                self.setupToken(&token, atIndex: index, withOffsetX: &scrollViewOriginX, withOffsetY: &scrollViewOriginY, remainingWidth: &remainingWidth)
            }
        }
        
        self.setupTextView(offsetX: &scrollViewOriginX, offsetY: &scrollViewOriginY, remainingWidth: &remainingWidth)
        
        if !self.tokens.isEmpty {
            self.scrollView.contentSize = CGSize(width: self.scrollView.bounds.width, height: scrollViewOriginY + max(Constants.textViewMinimumHeight, self.tokenHeight))
        } else {
            self.scrollView.contentSize = CGSize(width: self.scrollView.bounds.width, height: scrollViewOriginY + Constants.textViewMinimumHeight)
        }
        
        if self.tokens.count > self.lastTokenCount {
            self.shouldBecomeFirstResponder = true
            self.scrollToBottom(animated: false)
        }
        
        self.lastTokenCount = self.tokens.count
        
        if !self.lastText.isEmpty {
            self.textView.text = self.lastText
            self.lastText = ""
        }
        
        if self.shouldBecomeFirstResponder {
            self.textView.becomeFirstResponder()
            self.shouldBecomeFirstResponder = false
        }
        
        self.delegate?.tokenView(self, contentSizeChanged: self.scrollView.contentSize)
        self.delegate?.tokenView(self, didFinishLoadingTokens: self.tokens.count)
        
        UIView.setAnimationsEnabled(true)
    }
    
    func tokenForIndex(_ index: Int) -> TokenView {
        return self.tokens[index]
    }
    
    func selectToken(_ token: TokenView) {
        if self.selectedToken != nil && self.selectedToken != token {
            self.selectedToken?.isSelected = false
            token.hiddenTextView.delegate = nil
            token.hiddenTextView.resignFirstResponder()
            self.delegate?.tokenView(self, didDeselectTokenAtIndex: self.selectedToken!.tag)
        }
        
        token.isSelected = !token.isSelected
        
        if token.isSelected {
            token.hiddenTextView.delegate = self
            token.hiddenTextView.becomeFirstResponder()
            self.selectedToken = token
            self.delegate?.tokenView(self, didSelectTokenAtIndex: token.tag)
        } else {
            self.selectedToken = nil
            token.hiddenTextView.delegate = nil
            token.hiddenTextView.resignFirstResponder()
            self.delegate?.tokenView(self, didDeselectTokenAtIndex: token.tag)
        }
    }
}

// MARK: - UITextViewDelegate

extension TokenViewHolder: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    open func textViewDidBeginEditing(_ textView: UITextView) {
        if self.selectedToken != nil {
            self.selectedToken?.isSelected = false
            self.selectedToken?.hiddenTextView.delegate = nil
            self.selectedToken?.hiddenTextView.resignFirstResponder()
            self.delegate?.tokenView(self, didDeselectTokenAtIndex: self.selectedToken!.tag)
            self.selectedToken = nil
        }

        if textView.superview is TokenView == false {
            textView.text = ""
            textView.textColor = UIColor.black
        }

        self.delegate?.tokenView(self)
    }

    open func textViewDidEndEditing(_ textView: UITextView) {
        if textView.superview is TokenView {
            for (index, token) in self.tokens.enumerated() {
                if textView.superview == token {
                    self.delegate?.tokenView(self, didDeselectTokenAtIndex: index)
                    self.selectToken(token)
                    
                    break
                }
            }
        } else {
            if self.tokens.isEmpty && textView.text.isEmpty && !self.textView.isFirstResponder {
                if let placeholderText = self.dataSource?.titleForTokenViewPlaceholder(self) {
                    textView.text = placeholderText
                    textView.textColor = self.dataSource?.fontColorForTokenViewPlaceholder(self)
                }
            }
        }
        
        self.delegate?.tokenViewDidEndEditing(self)
    }
    
    open func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView.superview is TokenView {
            if textView.text == Constants.tokenDeleteKey {
                if !text.isEmpty && text != " " && text != "\n" {
                    self.textView.text = text
                }
                
                self.delegate?.tokenView(self, didDeleteTokenAtIndex: textView.tag)
            }
            
            return false
            
        } else {
            if textView.text.isEmpty && text == "\n" {
                self.delegate?.tokenView(self, didEnterText: textView.text)
                return false
            }
            
            if !textView.text.isEmpty && text == "\n" {
                self.shouldBecomeFirstResponder = true
                self.delegate?.tokenView(self, didEnterText: textView.text)
                return false
            }
            
            if textView.text.isEmpty && text.isEmpty {
                if let token = self.tokens.last {
                    self.selectToken(token)
                }
                return false
            }
        }
        
        return true
    }
    
    open func textViewDidChange(_ textView: UITextView) {
        if textView.superview is TokenView == false {
            var scrollViewOriginY = textView.frame.origin.y
            let availableWidth = textView.bounds.width
            let maxWidth = self.scrollView.bounds.width - self.tokenViewInsets.left - self.tokenViewInsets.right
            
            let textWidth = self.tokenViewInsets.left + textView.attributedText.size().width + self.tokenViewInsets.right
            
            if textWidth > availableWidth {
                var height = textView.frame.height

                if textView.frame.origin.x != self.tokenViewInsets.left {
                    scrollViewOriginY += textView.bounds.height
                } else {
                    self.textView.sizeToFit()
                    height = self.textView.frame.height
                }
                
                self.textView.frame = CGRect(x: self.tokenViewInsets.left, y: scrollViewOriginY, width: maxWidth, height: height)
                self.scrollView.contentSize = CGSize(width: self.scrollView.bounds.width, height: textView.frame.origin.y+height+self.tokenViewInsets.bottom)
                
                self.layoutIfNeeded()
                
                self.scrollToBottom(animated: true)
            }
            
            self.textView.layoutIfNeeded()
            self.delegate?.tokenView(self, didChangeText: textView.text)
        }
    }
    
    func dismissTokenView() {
        self.resignFirstResponder()
        self.endEditing(true)
    }
    
    private func scrollToBottom(animated: Bool) {
        let bottomY = self.scrollView.contentSize.height > self.scrollView.bounds.height ? self.scrollView.contentSize.height-self.scrollView.bounds.height : 0
        let bottomPoint = CGPoint(x: 0, y: bottomY)
        self.scrollView.setContentOffset(bottomPoint, animated: animated)
    }
}
