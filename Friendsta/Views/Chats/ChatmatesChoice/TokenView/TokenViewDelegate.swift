//
//  TokenViewDelegate.swift
//  Friendsta
//
//  Created by Elina Batyrova on 20.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

protocol TokenViewDelegate: AnyObject {
    
    // MARK: - Instance Methods
    
    func tokenView(_ tokenView: TokenViewHolder, didSelectTokenAtIndex index: Int)
    func tokenView(_ tokenView: TokenViewHolder, didDeselectTokenAtIndex index: Int)
    func tokenView(_ tokenView: TokenViewHolder, didDeleteTokenAtIndex index: Int)
    func tokenView(_ tokenViewDidBeginEditing: TokenViewHolder)
    func tokenViewDidEndEditing(_ tokenView: TokenViewHolder)
    func tokenView(_ tokenView: TokenViewHolder, didChangeText text: String)
    func tokenView(_ tokenView: TokenViewHolder, didEnterText text: String)
    func tokenView(_ tokenView: TokenViewHolder, contentSizeChanged size: CGSize)
    func tokenView(_ tokenView: TokenViewHolder, didFinishLoadingTokens tokenCount: Int)
}

// MARK: -

extension TokenViewDelegate {
    
    // MARK: - Instance Methods
    
    func tokenView(_ tokenView: TokenViewHolder, didSelectTokenAtIndex index: Int) {}
    func tokenView(_ tokenView: TokenViewHolder, didDeselectTokenAtIndex index: Int) {}
    func tokenView(_ tokenView: TokenViewHolder, didDeleteTokenAtIndex index: Int) {}
    func tokenView(_ tokenViewDidBeginEditing: TokenViewHolder) {}
    func tokenViewDidEndEditing(_ tokenView: TokenViewHolder) {}
    func tokenView(_ tokenView: TokenViewHolder, didChangeText text: String) {}
    func tokenView(_ tokenView: TokenViewHolder, didEnterText text: String) {}
    func tokenView(_ tokenView: TokenViewHolder, contentSizeChanged size: CGSize) {}
    func tokenView(_ tokenView: TokenViewHolder, didFinishLoadingTokens tokenCount: Int) {}
}
