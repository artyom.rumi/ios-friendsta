//
//  TokenViewDataSource.swift
//  Friendsta
//
//  Created by Elina Batyrova on 20.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

protocol TokenViewDataSource {
    
    // MARK: - Instance Methods
    
    func insetsForTokenView(_ tokenView: TokenViewHolder) -> UIEdgeInsets?
    func numberOfTokensForTokenView(_ tokenView: TokenViewHolder) -> Int
    func titleForTokenViewPlaceholder(_ tokenView: TokenViewHolder) -> String?
    func fontForTokenView(_ tokenView: TokenViewHolder) -> UIFont?
    func fontColorForTokenViewPlaceholder(_ tokenView: TokenViewHolder) -> UIColor?
    func tokenView(_ tokenView: TokenViewHolder, viewForTokenAtIndex index: Int) -> UIView?
    func tintColorForTokenView(_ tokenView: TokenViewHolder) -> UIColor?
}
