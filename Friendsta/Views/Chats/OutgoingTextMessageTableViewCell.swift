//
//  OutgoingTextMessageTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 17/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class OutgoingTextMessageTableViewCell: UITableViewCell {

    // MARK: - Instance Properties
    
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var statusImageView: UIImageView!
    
    @IBOutlet private weak var errorButton: UIButton!
    
    @IBOutlet private weak var cloudView: UIView!
    
    // MARK: -
    
    var onErrorButtonClicked: (() -> Void)?
    
    // MARK: -
    
    var message: String? {
        get {
            return self.messageLabel.text
        }
        
        set {
            self.messageLabel.text = newValue
        }
    }
    
    var date: String? {
        get {
            return self.dateLabel.text
        }
        
        set {
            self.dateLabel.text = newValue
        }
    }
    
    var isSent: Bool = false {
        didSet {
            self.updateStatusImageView()
        }
    }
    
    var isViewed: Bool = false {
        didSet {
            self.updateStatusImageView()
        }
    }
    
    var isFailed: Bool {
        get {
            return !self.errorButton.isHidden
        }
        
        set {
            self.errorButton.isHidden = !newValue
        }
    }
    
    var cloudViewColor: UIColor? {
        get {
            return self.cloudView.backgroundColor
        }
        
        set {
            self.cloudView.backgroundColor = newValue
        }
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onErrorButtonTouchUpInside(_ sender: Any) {
        self.onErrorButtonClicked?()
    }
    
    // MARK: -
    
    private func updateStatusImageView() {
        if self.isViewed {
            self.statusImageView.image = #imageLiteral(resourceName: "ChatViewedMessageIcon")
        } else if self.isSent {
            self.statusImageView.image = #imageLiteral(resourceName: "ChatSentMessageIcon")
        } else {
            self.statusImageView.image = nil
        }
    }
    
    // MARK: -
    
    private func setupFont() {
        self.messageLabel.font = Fonts.regular(ofSize: 14.0)
        self.dateLabel.font = Fonts.medium(ofSize: 9.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
