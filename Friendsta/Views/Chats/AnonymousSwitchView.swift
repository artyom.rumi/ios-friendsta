//
//  AnonymousSwitchView.swift
//  Friendsta
//
//  Created by Elina Batyrova on 22.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class AnonymousSwitchView: UIView {
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var changeRoleButton: UIButton!
    @IBOutlet private weak var currentRoleLabel: UILabel!
    
    // MARK: -
    
    var currentRole: String? {
        get {
            return self.currentRoleLabel.text
        }

        set {
            self.currentRoleLabel.text = newValue
        }
    }
    
    var changeRoleButtonTitle: String? {
        get {
            return self.changeRoleButton.titleLabel?.text
        }
        
        set {
            self.changeRoleButton.setTitle(newValue, for: .normal)
        }
    }
    
    var onChangeRoleButtonTapped: (() -> Void)?
    
    // MARK: - Instance Methods

    @IBAction private func onChangeRoleButtonTouchUpInside(_ sender: Any) {
        Log.high("onChangeRoleButtonTouchUpInside()", from: self)
        
        self.onChangeRoleButtonTapped?()
    }
    
    // MARK: -
    
    private func configureView() {
        self.layer.shadowColor = Colors.blackShadow.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
    }
    
    private func configureChangeRoleButton() {
        self.changeRoleButton.layer.cornerRadius = 14
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.configureView()
        self.configureChangeRoleButton()
    }
}
