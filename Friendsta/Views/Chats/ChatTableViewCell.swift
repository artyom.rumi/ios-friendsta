//
//  ChatTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 15.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//
// swiftlint:disable private_outlet

import Foundation
import UIKit

class ChatTableViewCell: UITableViewCell {
    
    // MARK: Instance Properties
    
    @IBOutlet weak var avatarImageView: RoundImageView!
    
    @IBOutlet fileprivate weak var fullNameLabel: UILabel!
    @IBOutlet fileprivate weak var lastMessageLabel: UILabel!
    
    @IBOutlet fileprivate weak var lastMessageDateLabel: UILabel!
    
    @IBOutlet fileprivate weak var newMessageImageView: UIImageView!
    @IBOutlet fileprivate weak var contentHolderView: UIView!
    
    @IBOutlet fileprivate weak var separatorView: UIView!
    // MARK: -
    
    var avatarImage: UIImage? {
        get {
            return self.avatarImageView.image
        }
        
        set {
            self.avatarImageView.image = newValue
        }
    }
    
    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }
        
        set {
            self.fullNameLabel.text = newValue
        }
    }
    
    var fullNameTextColor: UIColor? {
        get {
            return self.fullNameLabel.textColor
        }
        
        set {
            self.fullNameLabel.textColor = newValue
        }
    }
    
    var lastMessage: String? {
        get {
            return self.lastMessageLabel.text
        }
        
        set {
            self.lastMessageLabel.text = newValue
        }
    }
    
    var lastMessageDate: String? {
        get {
            return self.lastMessageDateLabel.text
        }
        
        set {
            self.lastMessageDateLabel.text = newValue
        }
    }
    
    var isViewed: Bool {
        get {
            return self.lastMessageLabel.textColor == Colors.grayText
        }
        
        set {
            self.lastMessageLabel.textColor = newValue ? Colors.grayText : Colors.orangeGuideView
        }
    }
    
    // MARK: -
    
    var avatarImageViewTarget: UIImageView {
        return self.avatarImageView
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.fullNameLabel.font = Fonts.medium(ofSize: 16.0)
        self.lastMessageLabel.font = Fonts.medium(ofSize: 14.0)
        self.lastMessageDateLabel.font = Fonts.regular(ofSize: 14.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if highlighted {
            self.contentHolderView.layer.cornerRadius = 3.0
            self.contentHolderView.layer.shadowColor = UIColor.lightGray.cgColor
            self.contentHolderView.layer.shadowRadius = 3.0
            self.contentHolderView.layer.shadowOpacity = 0.4
            self.contentHolderView.layer.shadowOffset = CGSize.zero
            self.contentHolderView.layer.shadowPath = UIBezierPath(roundedRect: CGRect(x: -3, y: -3, width: self.contentHolderView.frame.width + 6, height: self.contentHolderView.frame.height + 6), cornerRadius: 3.0).cgPath
            self.separatorView.isHidden = true
        } else {
            self.contentHolderView.layer.cornerRadius = 0.0
            self.contentHolderView.layer.shadowColor = UIColor.clear.cgColor
            self.contentHolderView.layer.shadowRadius = 0.0
            self.contentHolderView.layer.shadowOffset = CGSize.zero
            self.separatorView.isHidden = false
        }
    }
}
