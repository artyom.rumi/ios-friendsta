//
//  ChatMessageTableSectionHeaderView.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 23/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class ChatMessageTableSectionHeaderView: UITableViewCell {

    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    // MARK: -
    
    var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.regular(ofSize: 12.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
