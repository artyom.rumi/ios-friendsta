//
//  ChatmatesListTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 28.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

class ChatmatesListTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
        
    @IBOutlet private weak var leftStackView: UIStackView!
    @IBOutlet private weak var rightStackView: UIStackView!
    
    // MARK: -
    
    var userTokenViews: [UserTokenView]? {
        didSet {
            self.configureStackViews()
        }
    }
    
    // MARK: - Instance Methods
    
    private func configureStackViews() {
        self.removeAllViews(from: leftStackView)
        self.removeAllViews(from: rightStackView)
        
        guard let tokenViews = self.userTokenViews else {
            return
        }
        
        let splittedTokens = tokenViews.split()
        
        let leftTokens = splittedTokens.right
        let rightTokens = splittedTokens.left
        
        for token in leftTokens {
            self.leftStackView.addArrangedSubview(token)
        }
        
        for token in rightTokens {
            self.rightStackView.addArrangedSubview(token)
        }
    }
    
    private func removeAllViews(from stackView: UIStackView) {
        for view in stackView.arrangedSubviews {
            stackView.removeArrangedSubview(view)
        }
    }
}
