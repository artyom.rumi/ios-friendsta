//
//  OutgoingPhotoMessageTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 17/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class OutgoingPhotoMessageTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var statusImageView: UIImageView!
    
    @IBOutlet fileprivate weak var errorButton: UIButton!
    
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var dateLabel: UILabel!
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    // MARK: -
    
    var onViewPhotoButtonClicked: (() -> Void)?
    var onErrorButtonClicked: (() -> Void)?
    
    // MARK: -
    
    var photoImage: UIImage? {
        get {
            return self.photoImageView.image
        }
        
        set {
            self.photoImageView.image = newValue
        }
    }
    
    var date: String? {
        get {
            return self.dateLabel.text
        }
        
        set {
            self.dateLabel.text = newValue
        }
    }
    
    var isSent: Bool = false {
        didSet {
            self.updateStatusImageView()
        }
    }
    
    var isViewed: Bool = false {
        didSet {
            self.updateStatusImageView()
        }
    }
    
    var isFailed: Bool {
        get {
            return !self.errorButton.isHidden
        }
        
        set {
            self.errorButton.isHidden = !newValue
        }
    }
    
    var photoImageViewTarget: UIImageView {
        return self.photoImageView
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onViewPhotoButtonTouchUpInside(_ sender: Any) {
        self.onViewPhotoButtonClicked?()
    }
    
    // MARK: -
    
    private func setupFont() {
        self.dateLabel.font = Fonts.medium(ofSize: 9.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onErrorButtonTouchUpInside(_ sender: Any) {
        self.onErrorButtonClicked?()
    }
    
    // MARK: -
    
    fileprivate func updateStatusImageView() {
        if self.isViewed {
            self.statusImageView.image = #imageLiteral(resourceName: "ChatViewedMessageIcon")
        } else if self.isSent {
            self.statusImageView.image = #imageLiteral(resourceName: "ChatSentMessageIcon")
        } else {
            self.statusImageView.image = nil
        }
    }
    
    // MARK: -
    
    func showLoadingState() {
        self.activityIndicator.startAnimating()
    }
    
    func hideLoadingState() {
        self.activityIndicator.stopAnimating()
    }
    
    // MARK: -
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.photoImageView.image = nil
    }
}
