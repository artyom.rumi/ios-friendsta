//
//  IncomingTextMessageTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 17/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class IncomingTextMessageTableViewCell: UITableViewCell {

    // MARK: - Instance Properties
    
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var authorLabel: UILabel!
    
    @IBOutlet private weak var cloudView: UIView!
    
    // MARK: -
    
    var message: String? {
        get {
            return self.messageLabel.text
        }
        
        set {
            self.messageLabel.text = newValue
        }
    }
    
    var date: String? {
        get {
            return self.dateLabel.text
        }
        
        set {
            self.dateLabel.text = newValue
        }
    }

    var author: String? {
        get {
            return self.authorLabel.text
        }

        set {
            self.authorLabel.text = newValue
        }
    }
    
    var isViewed: Bool = false {
        didSet {
            self.updateMessageLabel()
        }
    }
    
    var cloudViewColor: UIColor? {
        get {
            return cloudView.backgroundColor
        }
        
        set {
            self.cloudView.backgroundColor = newValue
        }
    }

    // MARK: -
    
    private func updateMessageLabel() {
        if self.isViewed {
            self.messageLabel.font = Fonts.regular(ofSize: 14.0)
        } else {
            self.messageLabel.font = Fonts.bold(ofSize: 14.0)
        }
    }
    
    private func setupFont() {
        self.messageLabel.font = Fonts.regular(ofSize: 14.0)
        self.dateLabel.font = Fonts.medium(ofSize: 9.0)
        self.authorLabel.font = Fonts.medium(ofSize: 9.0)
    }
    
    override func awakeFromNib() {
         super.awakeFromNib()
        
        self.setupFont() 
    }
}
