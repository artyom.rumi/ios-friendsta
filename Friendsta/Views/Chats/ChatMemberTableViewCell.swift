//
//  ChatMaskUserTableViewCell.swift
//  Friendsta
//
//  Created by Marat Galeev on 21.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class ChatMemberTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var avatarImageView: RoundImageView!
    
    @IBOutlet fileprivate weak var fullNameLabel: UILabel!
    @IBOutlet fileprivate weak var revealStatusLabel: UILabel!
    
    @IBOutlet fileprivate weak var separatorView: UIView!
    
    // MARK: -
    
    var avatarImage: UIImage? {
        get {
            return self.avatarImageView.image
        }
        
        set {
            self.avatarImageView.image = newValue
        }
    }
    
    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }
        
        set {
            self.fullNameLabel.text = newValue
        }
    }
    
    var revealStatus: String? {
        get {
            return self.revealStatusLabel.text
        }
        
        set {
            self.revealStatusLabel.text = newValue
        }
    }
    
    var isLastRow: Bool {
        get {
            return self.separatorView.isHidden
        }
        
        set {
            self.separatorView.isHidden = newValue
        }
    }
    
    // MARK: -
    
    var avatarImageViewTarget: UIImageView {
        return self.avatarImageView
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.fullNameLabel.font = Fonts.regular(ofSize: 17.0)
        self.revealStatusLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
