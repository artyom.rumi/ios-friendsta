//
//  RevealView.swift
//  Friendsta
//
//  Created by Nikita on 25.10.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class RevealView: UIView {

    // MARK: - Nested Types

    enum State {
        case none
        case revealByMe
        case revealByOpponent
        case revealByBoth
        case revealByBothAnimation
        case revealByBothAndShowAddFriend
        case revealByBothAndShowAddFriendAnimation
    }

    enum RevealColors {
        static let gray = UIColor(red: 216/255, green: 216/255, blue: 216/255)
        static let yellow = UIColor(red: 255/255, green: 228/255, blue: 98/255)
        static let white = UIColor(white: 1.0)
    }

    private enum Constants {
        static let revealedImageViewHeight: CGFloat = 140
        static let leftIconCenterX: CGFloat = -73
        static let rightIconCenterX: CGFloat = 73
    }

    // MARK: - Instance Properties

    var onRevealStateChanged: ((Bool) -> Void)?
    var onAddAsAFriendTapped: (() -> Void)?

    // MARK: -

    @IBOutlet private var leftIconImageView: UIImageView!
    @IBOutlet private var leftIconCenterXConstraint: NSLayoutConstraint!

    @IBOutlet private var rightIconImageView: UIImageView!
    @IBOutlet private var rightIconCenterXConstraint: NSLayoutConstraint!

    @IBOutlet private var leftProgressView: UIView!
    @IBOutlet private var leftProgressViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet private var leftEnabledProgressView: UIView!
    @IBOutlet private var rightProgressView: UIView!

    @IBOutlet private var rightEnabledProgressView: UIView!
    @IBOutlet private var centerCircleView: UIView!

    @IBOutlet private var leftMessageLabel: UILabel!
    @IBOutlet private var rightMessageLabel: UILabel!

    @IBOutlet private var sliderView: UIView!
    @IBOutlet private var revealedView: UIView!
    @IBOutlet private var revealedImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var addAsAFriendButton: UIButton!
    @IBOutlet private var starIconImageView: UIImageView!
    @IBOutlet private var revealedImageView: UIImageView!

    // MARK: -

    private(set) var revealState: Bool = false

    // MARK: - Instance Methods

    func set(state: State, animate: Bool = false, completion: (() -> Void)? = nil) {
        if animate {
            UIView.animate(withDuration: 0.25, animations: {
                self.set(state: state)
            }, completion: { _ in
                completion?()
            })
        } else {
            self.set(state: state, completion: completion)
        }
    }

    func set(state: State, completion: (() -> Void)? = nil) {
        switch state {
        case .none:
            self.configureForNone()

        case .revealByMe:
            self.configureForRevealByMe()

        case .revealByOpponent:
            self.configureForRevealByOpponent()

        case .revealByBoth:
            self.configureForRevealByBoth()

        case .revealByBothAnimation:
            self.configureForRevealByBothAnimation(completion: completion)

        case .revealByBothAndShowAddFriend:
            self.configureForRevealByBothAndShowAddFriend()

        case .revealByBothAndShowAddFriendAnimation:
            self.configureForRevealByBothAndShowAddFriendAnimation(completion: completion)
        }
    }

    override func awakeFromNib() {
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.onGestureRecognized(sender:)))
        self.rightIconImageView.addGestureRecognizer(gesture)
        
        self.centerCircleView.layer.cornerRadius = self.centerCircleView.frame.size.width/2
        self.rightEnabledProgressView.layer.cornerRadius = 2
        self.leftEnabledProgressView.layer.cornerRadius = 2

        self.leftMessageLabel.text = ""
        self.rightMessageLabel.text = ""
        self.addAsAFriendButton.layer.cornerRadius = 16
    }

    // MARK: -

    private func configureForNone() {
        self.leftMessageLabel.text = ""
        self.rightMessageLabel.text = "Swipe for a mutual reveal".localized()
        self.leftIconCenterXConstraint.constant = Constants.leftIconCenterX
        self.rightIconCenterXConstraint.constant = Constants.rightIconCenterX

        self.revealedView.isHidden = true
        self.revealedView.alpha = 0
        self.revealedImageViewHeightConstraint.constant = Constants.revealedImageViewHeight/2
        self.addAsAFriendButton.isHidden = true
        self.starIconImageView.isHidden = true

        self.revealState = false

        self.layoutIfNeeded()
    }

    private func configureForRevealByMe() {
        self.leftMessageLabel.text = ""
        self.rightMessageLabel.text = "Waiting for a friend to swipe...".localized()
        self.leftIconCenterXConstraint.constant = Constants.leftIconCenterX
        self.rightIconCenterXConstraint.constant = 0

        self.revealedView.isHidden = true
        self.revealedView.alpha = 0
        self.revealedImageViewHeightConstraint.constant = Constants.revealedImageViewHeight/2
        self.addAsAFriendButton.isHidden = true
        self.starIconImageView.isHidden = true

        self.revealState = true

        self.layoutIfNeeded()
    }

    private func configureForRevealByOpponent() {
        self.leftMessageLabel.text = "A friend agreed to reveal!".localized()
        self.rightMessageLabel.text = "Swipe to be revealed!".localized()
        self.leftIconCenterXConstraint.constant = 0
        self.rightIconCenterXConstraint.constant = Constants.rightIconCenterX

        self.revealedView.isHidden = true
        self.revealedView.alpha = 0
        self.revealedImageViewHeightConstraint.constant = Constants.revealedImageViewHeight/2
        self.addAsAFriendButton.isHidden = true
        self.starIconImageView.isHidden = true

        self.revealState = false

        self.layoutIfNeeded()
    }

    private func configureForRevealByBoth() {
        /*
        self.revealedView.isHidden = false
        self.revealedImageViewHeightConstraint.constant = Constants.revealedImageViewHeight/2
        self.revealedImageView.isHidden = true
        self.revealedView.alpha = 1

        self.revealState = true

        self.layoutIfNeeded()
        */
        
        self.revealState = true
        self.starIconImageView.alpha = 0
        self.addAsAFriendButton.alpha = 0
            
        self.revealedView.isHidden = false
        self.revealedView.alpha = 1
        self.revealedImageView.alpha = 1
        self.revealedImageView.isHidden = false
        self.revealedImageViewHeightConstraint.constant = Constants.revealedImageViewHeight
        self.layoutIfNeeded()
    }

    private func configureForRevealByBothAnimation(completion: (() -> Void)?) {
        self.revealState = true
        self.revealedImageView.alpha = 0
        self.starIconImageView.alpha = 0
        self.addAsAFriendButton.alpha = 0

        UIView.animate(withDuration: 1, animations: {
            self.revealedView.isHidden = false
            self.revealedView.alpha = 1
            self.revealedImageView.alpha = 1
            self.revealedImageView.isHidden = false
            self.revealedImageViewHeightConstraint.constant = Constants.revealedImageViewHeight
            self.layoutIfNeeded()
        }, completion: { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                completion?()
            }
        })
    }

    private func configureForRevealByBothAndShowAddFriendAnimation(completion: (() -> Void)?) {
        self.revealState = true
        self.revealedImageView.alpha = 0.4
        self.starIconImageView.alpha = 0
        self.addAsAFriendButton.alpha = 0

        UIView.animate(withDuration: 1, animations: {
            self.revealedView.isHidden = false
            self.revealedView.alpha = 1
            self.revealedImageView.alpha = 1
            self.revealedImageView.isHidden = false
            self.revealedImageViewHeightConstraint.constant = Constants.revealedImageViewHeight
            self.layoutIfNeeded()
        }, completion: { _ in
            UIView.animate(withDuration: 1, delay: 2, animations: {
                self.revealedImageView.alpha = 0
                self.starIconImageView.alpha = 1
                self.starIconImageView.isHidden = false
                self.addAsAFriendButton.alpha = 1
                self.addAsAFriendButton.isHidden = false
            }, completion: { _ in
                self.revealedImageView.isHidden = true
                completion?()
            })
        })
    }

    private func configureForRevealByBothAndShowAddFriend() {
        self.revealState = false

        self.revealedView.isHidden = false
        self.revealedView.alpha = 1
        self.starIconImageView.alpha = 1
        self.starIconImageView.isHidden = false
        self.addAsAFriendButton.alpha = 1
        self.addAsAFriendButton.isHidden = false
        self.revealedImageView.isHidden = true
    }

    // MARK: -

    @IBAction private func onAddAsAFriendTouchUpInside(sender: UIButton) {
        self.onAddAsAFriendTapped?()
    }

    @objc private func onGestureRecognized(sender: UIPanGestureRecognizer) {
        let location = sender.location(in: self.sliderView)

        var center = location.x - self.sliderView.frame.origin.x
        if center < 0 {
            center = 0
        }

        if center > Constants.rightIconCenterX {
            center = Constants.rightIconCenterX
        }

        switch sender.state {
        case .changed:
            self.rightIconCenterXConstraint.constant = center
            self.layoutIfNeeded()
        case .ended:
            var newRevealState = false
            var finalPosition: CGFloat = Constants.rightIconCenterX

            if center < Constants.rightIconCenterX/2 {
                finalPosition = 0
                newRevealState = true
            }

            UIView.animate(withDuration: 0.25, animations: {
                self.rightIconCenterXConstraint.constant = finalPosition
                self.layoutIfNeeded()
            }, completion: { _ in
                if self.revealState != newRevealState {
                    self.onRevealStateChanged?(newRevealState)
                    self.revealState = newRevealState
                }
            })

        default:
            break
        }
    }
}
