//
//  CreateChatTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 25.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class CreateChatTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var avatarImageView: RoundImageView!
    
    @IBOutlet fileprivate weak var fullNameLabel: UILabel!
    @IBOutlet fileprivate weak var schoolInfoLabel: UILabel!
    
    // MARK: -
    
    var avatarImage: UIImage? {
        get {
            return self.avatarImageView.image
        }
        
        set {
            self.avatarImageView.image = newValue
        }
    }
    
    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }
        
        set {
            self.fullNameLabel.text = newValue
        }
    }
    
    var schoolInfo: String? {
        get {
            return self.schoolInfoLabel.text
        }
        
        set {
            self.schoolInfoLabel.text = newValue
        }
    }
    
    // MARK: -
    
    var avatarImageTarget: AnyObject {
        return self.avatarImageView
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupFont()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func setupFont() {
        self.fullNameLabel.font = Fonts.medium(ofSize: 17.0)
        self.schoolInfoLabel.font = Fonts.regular(ofSize: 13.0)
    }
    
}
