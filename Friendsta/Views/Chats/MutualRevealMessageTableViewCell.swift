//
//  MutualRevealMessageTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 22/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class MutualRevealMessageTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties

    @IBOutlet fileprivate weak var frontAvatarImageView: RoundImageView!
    @IBOutlet fileprivate weak var backAvatarImageView: RoundImageView!

    @IBOutlet fileprivate weak var fullNameLabel: UILabel!
    @IBOutlet fileprivate weak var schoolInfoLabel: UILabel!
    @IBOutlet fileprivate weak var explanationMutualRevealMessageLabel: UILabel!
    
    // MARK: -

    var frontAvatarImage: UIImage? {
        get {
            return self.frontAvatarImageView.image
        }
        
        set {
            self.frontAvatarImageView.image = newValue
        }
    }
    
    var backAvatarImage: UIImage? {
        get {
            return self.backAvatarImageView.image
        }
        
        set {
            self.backAvatarImageView.image = newValue
        }
    }
    
    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }
        
        set {
            self.fullNameLabel.text = newValue
        }
    }
    
    var schoolInfo: String? {
        get {
            return self.schoolInfoLabel.text
        }
        
        set {
            self.schoolInfoLabel.text = newValue
        }
    }

    var onFrontAvatarTapped: (() -> Void)?
    
    // MARK: -
    
    var frontAvatarImageViewTarget: UIImageView {
        return self.frontAvatarImageView
    }
    
    var backAvatarImageViewTarget: UIImageView {
        return self.backAvatarImageView
    }

    // MARK: - UIView Methods

    override func awakeFromNib() {
        super.awakeFromNib()

        self.setupFont()
    }

    // MARK: - Instance Methods

    fileprivate func setupFont() {
        self.fullNameLabel.font = Fonts.bold(ofSize: 17.0)
        self.schoolInfoLabel.font = Fonts.regular(ofSize: 17.0)
        self.explanationMutualRevealMessageLabel.font = Fonts.regular(ofSize: 15.0)
    }

    // MARK: -

    @IBAction private func onFrontAvatarTouchUpInside(_ sender: Any) {
        self.onFrontAvatarTapped?()
    }
}
