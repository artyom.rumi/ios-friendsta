//
//  IncomingPhotoMessageTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 17/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class IncomingPhotoMessageTableViewCell: UITableViewCell {

    // MARK: - Instance Properties
    
    @IBOutlet private weak var photoImageView: UIImageView!
    @IBOutlet private weak var dateLabel: UILabel!
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet private weak var authorLabel: UILabel!
    
    @IBOutlet private weak var cloudView: UIView!
    
    // MARK: -
    
    var onViewPhotoButtonClicked: (() -> Void)?
    
    // MARK: -
    
    var photoImage: UIImage? {
        get {
            return self.photoImageView.image
        }
        
        set {
            self.photoImageView.image = newValue
        }
    }
    
    var date: String? {
        get {
            return self.dateLabel.text
        }
        
        set {
            self.dateLabel.text = newValue
        }
    }

    var author: String? {
        get {
            return self.authorLabel.text
        }

        set {
            self.authorLabel.text = newValue
        }
    }
    
    var cloudViewColor: UIColor? {
        get {
            return cloudView.backgroundColor
        }
        
        set {
            self.cloudView.backgroundColor = newValue
        }
    }
    
    // MARK: -
    
    var photoImageViewTarget: UIImageView {
        return self.photoImageView
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onViewPhotoButtonTouchUpInside(_ sender: Any) {
        self.onViewPhotoButtonClicked?()
    }
    
    // MARK: -
    
    private func setupFont() {
        self.dateLabel.font = Fonts.medium(ofSize: 9.0)
        self.authorLabel.font = Fonts.medium(ofSize: 9.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.photoImageView.image = nil
    }
    
    // MARK: -
    
    func showLoadingState() {
        self.activityIndicator.startAnimating()
    }
    
    func hideLoadingState() {
        self.activityIndicator.stopAnimating()
    }
}
