//
//  IncomingTextMessageWithAvatarCell.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/11/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
////////////////>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<
protocol IncomingTextMessageWithAvatarTableViewCellDelegate: class {
    func retrieveUserIdFromTextMessageAvatarTableViewCell(userId: Int64)
    
}

class IncomingTextMessageWithAvatarTableViewCell: IncomingTextMessageTableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var avatarImageView: RoundImageView!
    @IBOutlet private weak var avatarImageViewWidthConstraint: NSLayoutConstraint!
    
    weak var delegate: IncomingTextMessageWithAvatarTableViewCellDelegate?
    public var user: User? {
        didSet {
            configureGestures()
        }
    }
    
    var avatarImage: UIImage? {
        get {
            return self.avatarImageView.image
        }
        
        set {
            self.avatarImageView.image = newValue
        }
    }
    
    // MARK: -
    
    var avatarImageViewTarget: UIImageView {
        return self.avatarImageView
    }
}

extension IncomingTextMessageWithAvatarTableViewCell {
    
    public func configureGestures() {
        
        avatarImageView.isUserInteractionEnabled = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(avatarImageViewTapped))
        avatarImageView.addGestureRecognizer(tapGesture)
    }
    
    @objc fileprivate func avatarImageViewTapped() {
        
        guard let user = user else { return }
        
        delegate?.retrieveUserIdFromTextMessageAvatarTableViewCell(userId: user.uid)
    }
}
