//
//  IncomingCommentMessageTableViewCell.swift
//  Friendsta
//
//  Created by Nikita on 17.10.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class IncomingCommentMessageTableViewCell: IncomingPhotoMessageTableViewCell {

    // MARK: - Instance Properties

    @IBOutlet private weak var messageTextLabel: UILabel!

    // MARK: -

    var messageText: String? {
        get {
            return self.messageTextLabel.text
        }

        set {
            self.messageTextLabel.text = newValue
        }
    }
    
    var isViewed: Bool = false {
        didSet {
            self.updateMessageLabel()
        }
    }

    // MARK: - Instance Methods
    
    private func updateMessageLabel() {
        if self.isViewed {
            self.messageTextLabel.font = Fonts.regular(ofSize: 14)
        } else {
            self.messageTextLabel.font = Fonts.bold(ofSize: 14)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
