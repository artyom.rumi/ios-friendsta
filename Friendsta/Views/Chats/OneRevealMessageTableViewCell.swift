//
//  OneRevealMessageTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 22/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class OneRevealMessageTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var messageLabel: UILabel!
    
    // MARK: -
    
    var chatMaskName: String? {
        didSet {
            self.messageLabel.text = String(format: "Your chatmate is ready to reveal, but revealing is a mutual action. If you are ready to reveal yourself, you can both see each other.".localized(), self.chatMaskName ?? "Incognito User".localized())
        }
    }
    
    // MARK: - Instance Methods
    
    fileprivate func setupFont() {
        self.messageLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
