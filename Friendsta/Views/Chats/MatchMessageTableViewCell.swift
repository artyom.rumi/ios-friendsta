//
//  MatchMessageTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 22/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class MatchMessageTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var explanationLabel: UILabel!
    @IBOutlet fileprivate weak var aboutRevealingButton: UIButton!
    
    var onRevealButtonClicked: (() -> Void)?
    
    // MARK: - Instance Methods

    @IBAction fileprivate func onRevealButtonTouchUpInside(_ sender: Any) {
        Log.high("onRevealButtonTouchUpInside()", from: self)
        
        self.onRevealButtonClicked?()
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.heavy(ofSize: 22.0)
        self.explanationLabel.font = Fonts.regular(ofSize: 17.0)
        self.aboutRevealingButton.titleLabel?.font = Fonts.semiBold(ofSize: 17.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
