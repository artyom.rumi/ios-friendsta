//
//  RectangularImageView.swift
//  Friendsta
//
//  Created by Nikita Asabin on 1/23/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class RectangularImageView: UIView {
    
    fileprivate let imageView = UIImageView()
    fileprivate let backView = UIView()
    
    @IBInspectable var image: UIImage? {
        get {
            return self.imageView.image
        }
        set {
            self.imageView.image = newValue
        }
    }
    
    var imageTarget: UIImageView {
        get {
            return self.imageView
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
        self.initialize()
    }
    
    func initialize() {
        self.imageView.layer.cornerRadius = 3.0
        self.imageView.clipsToBounds = true
        
        self.backView.layer.cornerRadius = 3.0
        self.backView.backgroundColor = Colors.grayBackground
        
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        self.backView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.backView)
        self.addSubview(self.imageView)
        
        NSLayoutConstraint.activate([self.imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                                     self.imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                                     self.imageView.heightAnchor.constraint(equalToConstant: 44.0),
                                     self.imageView.widthAnchor.constraint(equalToConstant: 26.0)])
        
        NSLayoutConstraint.activate([self.backView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 3.0),
                                     self.backView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                                     self.backView.heightAnchor.constraint(equalToConstant: 38.0),
                                     self.backView.widthAnchor.constraint(equalToConstant: 26.0)])
    }
}
