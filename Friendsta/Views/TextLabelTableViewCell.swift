//
//  TextLabelTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 12.11.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class TextLabelTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    // MARK: - Instance Methods
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
