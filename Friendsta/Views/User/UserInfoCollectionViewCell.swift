//
//  UserInfoCollectionViewCell.swift
//  Friendsta
//
//  Created by Nikita Asabin on 1/13/20.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import Foundation
import UIKit
import FriendstaTools

class UserInfoCollectionViewCell: UICollectionViewCell {

    // MARK: - Instance Properties

    @IBOutlet private weak var avatarImageView: RoundImageView!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!

    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var schoolNameLabel: UILabel!

    @IBOutlet private weak var friendsCountLabel: UILabel!

    @IBOutlet private weak var bioInfoLabel: UILabel!

    @IBOutlet private weak var addButton: UIButton!
    @IBOutlet private weak var addButtonActivityIndicatorView: UIActivityIndicatorView!

    // MARK: -

    private lazy var constraintWidth: NSLayoutConstraint = {
        let consraint = self.contentView.widthAnchor.constraint(equalToConstant: self.bounds.size.width)

        consraint.isActive = true

        return consraint
    }()

    // MARK: -

    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }

        set {
            self.fullNameLabel.text = newValue
        }
    }

    var schoolInfo: String? {
        get {
            return self.schoolNameLabel.text
        }

        set {
            self.schoolNameLabel.text = newValue
        }
    }

    var bioInfo: String? {
        get {
            return self.bioInfoLabel.text
        }

        set {
            self.bioInfoLabel.text = newValue
        }
    }

    var friendsCountAttributedText: NSAttributedString? {
        get {
            return self.friendsCountLabel.attributedText
        }

        set {
            self.friendsCountLabel.attributedText = newValue
        }
    }

    var avatarImageViewTarget: UIImageView {
        return self.avatarImageView
    }

    var addButtonActivityIndicatorAnimating: Bool {
        get {
            return self.addButtonActivityIndicatorView.isAnimating
        }
        set {
            if newValue {
                self.addButtonActivityIndicatorView.startAnimating()
            } else {
                self.addButtonActivityIndicatorView.stopAnimating()
            }
        }
    }

    // MARK: -

    var onAddButtonTapped: (() -> Void)?
    var onKeepButtonTapped: (() -> Void)?

    // MARK: - Instance Methods

    func showActivityIndicator() {
        self.activityIndicatorView.startAnimating()
    }

    func hideActivityIndicator() {
        self.activityIndicatorView.stopAnimating()
    }

    func set(friendship status: FriendshipStatusType?) {
        self.addButton.removeTarget(self, action: #selector(onAddButtonTouchUpInside(_:)), for: .touchUpInside)
        self.addButton.removeTarget(self, action: #selector(onKeepButtonTouchUpInside(_:)), for: .touchUpInside)

        if let status = status {
            self.addButton.isHidden = false

            switch status {
            case .uninvited:
                self.addButton.setImage(nil, for: .normal)
                self.addButton.setBackgroundImage(UIImage(named: "KeepButton"), for: .normal)
                self.addButton.setTitle("Add", for: .normal)
                self.addButton.isUserInteractionEnabled = true
                self.addButton.addTarget(self, action: #selector(onAddButtonTouchUpInside(_:)), for: .touchUpInside)

            case .invited:
                self.addButton.setImage(UIImage(named:  "SandhourIcon"), for: .normal)
                self.addButton.setBackgroundImage(UIImage(named: "AddedButton"), for: .normal)
                self.addButton.setTitle("Added", for: .normal)
                self.addButton.isUserInteractionEnabled = false

            case .friend:
                self.addButton.setImage(nil, for: .normal)
                self.addButton.setBackgroundImage(UIImage(named: "KeepButton"), for: .normal)
                self.addButton.setTitle("Keep", for: .normal)
                self.addButton.isUserInteractionEnabled = true
                self.addButton.addTarget(self, action: #selector(onKeepButtonTouchUpInside(_:)), for: .touchUpInside)

            case .affirmed:
                self.addButton.setImage(UIImage(named: "CheckmarkIcon"), for: .normal)
                self.addButton.setBackgroundImage(UIImage(named: "AddedButton"), for: .normal)
                self.addButton.setTitle("Kept", for: .normal)
                self.addButton.isUserInteractionEnabled = false
            }
        } else {
            self.addButton.isHidden = true
        }
    }

    // MARK: -

    @objc private func onAddButtonTouchUpInside(_ sender: Any) {
        Log.high("onAddButtonTouchUpInside()", from: self)

        self.onAddButtonTapped?()
    }

    @objc private func onKeepButtonTouchUpInside(_ sender: Any) {
        Log.high("onKeepButtonTouchUpInside()", from: self)

        self.onKeepButtonTapped?()
    }

    // MARK: - UICollectionViewCell

    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        self.constraintWidth.constant = self.bounds.width

        return self.contentView.systemLayoutSizeFitting(CGSize(width: targetSize.width, height: 1))
    }
}
