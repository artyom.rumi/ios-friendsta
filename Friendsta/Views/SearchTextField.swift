//
//  SearchTextField.swift
//  Friendsta
//
//  Created by Marat Galeev on 27.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable final class SearchTextField: IconTextField {
    
    // MARK: - Instance Properties
    
    override var placeholder: String? {
        didSet {
            self.updatePlaceholder()
        }
    }
    
    // MARK: -
    
    @IBInspectable var placeholderTintColor: UIColor? {
        didSet {
            self.updatePlaceholder()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        
        set {
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = (newValue > 0.0)
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        
        set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable public var defaultBorderColor: UIColor? {
        didSet {
            self.applyState()
        }
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.placeholder = "Search".localized()
        
        self.clearButtonMode = .always
        self.tintColor = Colors.secondary
        self.font = Fonts.regular(ofSize: 17.0)
        self.returnKeyType = .search
        
        self.icon = UIImage(named: "SearchGrayIcon",
                            in: Bundle(for: self.classForCoder),
                            compatibleWith: nil)
        
        self.iconLeftInset = 8.0
        
        self.leftInset = 12.0
        self.rightInset = 26.0
    }
    
    fileprivate func updatePlaceholder() {
        if let placeholderTintColor = self.placeholderTintColor {
            self.attributedPlaceholder = NSAttributedString(string: self.placeholder!,
                                                            attributes: [.foregroundColor: placeholderTintColor])
        }
    }
    
    fileprivate func applyState() {
        self.layer.borderColor = self.defaultBorderColor?.cgColor
    }
}
