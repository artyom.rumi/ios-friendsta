//
//  SwitchableTableViewCell.swift
//  Friendsta
//
//  Created by Marat Galeev on 25.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class SwitchableTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var stateSwitch: UISwitch!
    @IBOutlet fileprivate weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet fileprivate weak var textInCellLabel: UILabel!
    
    // MARK: -
    
    var onStateChanged: ((_ isOn: Bool) -> Void)!
    
    // MARK: -
    
    var isOn: Bool {
        get {
            return self.stateSwitch.isOn
        }
        
        set {
            self.stateSwitch.isOn = newValue
        }
    }
    
    var isActivityIndicatorHidden: Bool {
        return self.activityIndicatorView.isHidden
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onSwitchValueChanged(_ sender: UISwitch) {
        self.onStateChanged(sender.isOn)
    }
    
    // MARK: -
    
    func showActivityIndicator() {
        self.stateSwitch.isHidden = true
        
        self.activityIndicatorView.startAnimating()
    }
    
    func hideActivityIndicator() {
        self.stateSwitch.isHidden = false
        
        self.activityIndicatorView.stopAnimating()
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        if self.textInCellLabel != nil {
            self.textInCellLabel.font = Fonts.regular(ofSize: 15.0)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
