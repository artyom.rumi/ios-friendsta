//
//  PrimaryButton.swift
//  Friendsta
//
//  Created by Marat Galeev on 06.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable final class PrimaryButton: UIButton {
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        let backgroundImage = UIImage(named: "PrimaryButton",
                                      in: Bundle(for: self.classForCoder),
                                      compatibleWith: nil)
        
        self.setBackgroundImage(backgroundImage, for: .normal)
        self.setTitleColor(Colors.whiteText, for: .normal)
        
        self.titleLabel?.font = Fonts.semiBold(ofSize: 17.0)
    }
}
