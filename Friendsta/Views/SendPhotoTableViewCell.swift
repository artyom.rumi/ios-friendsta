//
//  SendPhotoTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 02.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class SendPhotoTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var checkmarkView: CheckmarkView!
    @IBOutlet fileprivate weak var avatarImageView: UIImageView!

    @IBOutlet fileprivate weak var fullNameLabel: UILabel!
    
    // MARK: -
    
    var avatarImage: UIImage? {
        get {
            return self.avatarImageView.image
        }
        
        set {
            self.avatarImageView.image = newValue
        }
    }
    
    var fullName: String? {
        get {
            return self.fullNameLabel.text
        }
        
        set {
            self.fullNameLabel.text = newValue
        }
    }
    
    var isCheckmarked: Bool {
        get {
            return self.checkmarkView.isSelected
        }
        
        set {
            self.checkmarkView.isSelected = newValue
        }
    }
    
    // MARK: -
    
    var avatarImageTarget: AnyObject {
        return self.avatarImageView
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.fullNameLabel.font = Fonts.medium(ofSize: 17.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
    
}
