//
//  SettingsTableSectionFooterView.swift
//  Friendsta
//
//  Created by Marat Galeev on 08.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class SettingsTableSectionFooterView: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    // MARK: -
    
    var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
}
