//
//  EditAccountUserBioTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 24/04/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class EditAccountUserBioTableViewCell: UITableViewCell {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let characterMaxCount = 160
    }

    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var aboutMeLabel: UILabel!
    @IBOutlet fileprivate weak var counterLabel: UILabel!
    @IBOutlet fileprivate weak var bioTextView: UITextView!
    @IBOutlet fileprivate weak var placeholderLabel: UILabel!
    
    // MARK: - 
    
    var onBioChanged: ((_ text: String?) -> Void)?
    var onPreferredHeightChanged: (() -> Void)?
    
    // MARK: -
    
    @IBInspectable var bio: String? {
        get {
            return self.bioTextView.text
        }
        
        set {
            self.bioTextView.text = newValue
            
            self.updatePlaceholderLabel()
            self.updateCounterLabel()
            self.updatePreferredHeight()
        }
    }

    // MARK: - Instance Methods
    
    fileprivate func updatePlaceholderLabel() {
        self.placeholderLabel.isHidden = self.bioTextView.hasText
    }
    
    fileprivate func updateCounterLabel() {
        self.counterLabel.text = "\(Constants.characterMaxCount - (self.bioTextView.text?.count ?? 0))"
    }
    
    fileprivate func updatePreferredHeight() {
        let preferredHeight = self.bioTextView.sizeThatFits(self.bounds.size).height
        
        if self.bioTextView.frame.size.height != preferredHeight {
            self.onPreferredHeightChanged?()
        }
    }
    
    // MARK: - UIView
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.bioTextView.textContainerInset = .zero
        self.bioTextView.textContainer.lineFragmentPadding = 0
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.aboutMeLabel.font = Fonts.bold(ofSize: 17.0)
        self.counterLabel.font = Fonts.regular(ofSize: 17.0)
        self.bioTextView.font = Fonts.regular(ofSize: 17.0)
        self.placeholderLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}

// MARK: - UITextViewDelegate

extension EditAccountUserBioTableViewCell: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        let color = textView.tintColor

        textView.tintColor = .clear
        textView.tintColor = color
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard text != "\n" else {
            textView.resignFirstResponder()
            
            return false
        }
        
        let replacedText: String
        
        if !textView.text.isEmpty {
            replacedText = textView.text.replacingCharacters(in: Range(range, in: textView.text)!, with: text)
        } else {
            replacedText = text
        }
        
        return (replacedText.count <= Constants.characterMaxCount)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.updatePlaceholderLabel()
        self.updateCounterLabel()
        self.updatePreferredHeight()
        
        self.onBioChanged?(textView.text)
    }
}
