//
//  EditAccountUserTableViewCell.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 26/04/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class EditAccountUserTableViewCell: UITableViewCell {

    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var categoryNameLabel: UILabel!
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    // MARK: -
    
    var title: String? {
        get {
            return titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.categoryNameLabel.font = Fonts.regular(ofSize: 17.0)
        self.titleLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        self.setupFont()
    }
}
