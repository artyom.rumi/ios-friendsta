//
//  SettingsTableSectionHeaderView.swift
//  Friendsta
//
//  Created by Marat Galeev on 08.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class SettingsTableSectionHeaderView: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    // MARK: -
    
    var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.bold(ofSize: 17.0)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
