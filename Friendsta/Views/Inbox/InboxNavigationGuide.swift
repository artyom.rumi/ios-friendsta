//
//  InboxNavigationGuide.swift
//  Friendsta
//
//  Created by Nikita Asabin on 2/5/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class InboxNavigationGuide: NibLoadingView {
    
    // MARK: - Instance properties
    
    @IBOutlet fileprivate weak var leftView: UIView!
    @IBOutlet fileprivate weak var rightView: UIView!
    
     // MARK: - Initializers
    
    class func showInView(_ view: UIView) {
        let isExist = view.subviews.contains(where: { (object) -> Bool in
            return object is InboxNavigationGuide
        })
        
        if !isExist {
            let guide = InboxNavigationGuide(frame: view.bounds)
            view.addSubview(guide)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Instance methods
    
    private func initialize() {
        let tapGestureLeft = UITapGestureRecognizer(target: self, action: #selector(didTapOnView))
        let tapGestureRight = UITapGestureRecognizer(target: self, action: #selector(didTapOnView))
        
        self.leftView.addGestureRecognizer(tapGestureLeft)
        self.rightView.addGestureRecognizer(tapGestureRight)
    }
    
    @objc private func didTapOnView() {
        self.hideAnimated()
        self.isUserInteractionEnabled = false
    }
    
    private func hideAnimated() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { (completed) in
            self.removeFromSuperview()
        }
    }
}
