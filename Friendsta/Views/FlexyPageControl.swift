//
//  FlexyPageControl.swift
//  Friendsta
//
//  Created by Marat Galeev on 31.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class FlexyPageControl: UIControl {
    
    // MARK: - Instance Properties
    
    fileprivate let limit = 5
    fileprivate var fullScaleIndex = [0, 1, 2]
    fileprivate var dotLayers: [CALayer] = []
    
    fileprivate var diameter: CGFloat {
        return self.radius * 2
    }
    
    fileprivate var centerIndex: Int {
        return self.fullScaleIndex[1]
    }
    
    // MARK: -
    
    var currentPage = 0 {
        didSet {
            if self.numberOfPages > self.currentPage {
                self.update()
            }
        }
    }
    
    // MARK: -
    
    @IBInspectable var inactiveTintColor: UIColor = Colors.darkPageIndicator {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var currentPageTintColor: UIColor = Colors.lightPageIndicator {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var radius: CGFloat = 4.0 {
        didSet {
            self.updateDotLayersLayout()
        }
    }
    
    @IBInspectable var padding: CGFloat = 5.0 {
        didSet {
            self.updateDotLayersLayout()
        }
    }
    
    @IBInspectable var minScaleValue: CGFloat = 0.4 {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var middleScaleValue: CGFloat = 0.7 {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var numberOfPages: Int = 0 {
        didSet {
            self.setupDotLayers()
            self.isHidden = self.hideForSinglePage && self.numberOfPages <= 1
        }
    }
    
    @IBInspectable var hideForSinglePage: Bool = true {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var inactiveTransparency: CGFloat = 0.4 {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    // MARK: - UIView
    
    override var intrinsicContentSize: CGSize {
        return sizeThatFits(CGSize.zero)
    }
    
    // MARK: - Initializers
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    required public init(frame: CGRect, numberOfPages: Int) {
        super.init(frame: frame)
        
        self.numberOfPages = numberOfPages
        self.setupDotLayers()
    }
    
    // MARK: - Instance Methods
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let minValue = min(7, self.numberOfPages)
        
        return CGSize(width: CGFloat(minValue) * self.diameter + CGFloat(minValue - 1) * self.padding,
                      height: self.diameter)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        for dotLayer in self.dotLayers {
            if self.borderWidth > 0 {
                dotLayer.borderWidth = self.borderWidth
                dotLayer.borderColor = self.borderColor.cgColor
            }
        }
        
        self.update()
    }
}

// MARK: -

extension FlexyPageControl {
    
    // MARK: - Instance Methods
    
    fileprivate func setupDotLayers() {
        for dotLayer in self.dotLayers {
            dotLayer.removeFromSuperlayer()
        }
        
        self.dotLayers.removeAll()
        
        for _ in 0..<self.numberOfPages {
            let dotLayer = CALayer()
            
            self.layer.addSublayer(dotLayer)
            self.dotLayers.append(dotLayer)
        }
        
        self.updateDotLayersLayout()
        
        self.setNeedsLayout()
        self.invalidateIntrinsicContentSize()
    }
    
    fileprivate func updateDotLayersLayout() {
        let floatCount = CGFloat(self.numberOfPages)
        
        var frame = CGRect(x: (self.bounds.size.width - self.diameter * floatCount - self.padding * (floatCount - 1)) * 0.5,
                           y: (self.bounds.size.height - self.diameter) * 0.5,
                           width: self.diameter,
                           height: self.diameter)
        
        for dotLayer in self.dotLayers {
            dotLayer.cornerRadius = self.radius
            dotLayer.frame = frame
            
            frame.origin.x += self.diameter + self.padding
        }
    }
    
    fileprivate func setupDotLayersPosition() {
        let centerLayer = self.dotLayers[self.centerIndex]
        
        centerLayer.position = CGPoint(x: frame.width * 0.5,
                                       y: frame.height * 0.5)
        
        self.dotLayers.enumerated().filter { dotLayerItem in
            dotLayerItem.offset != self.centerIndex
        }.forEach { dotLayerItem in
            let index = abs(dotLayerItem.offset - self.centerIndex)
            let interval = dotLayerItem.offset > self.centerIndex ? self.diameter + self.padding : -(self.diameter + self.padding)
            
            dotLayerItem.element.position = CGPoint(x: centerLayer.position.x + interval * CGFloat(index),
                                                    y: dotLayerItem.element.position.y)
        }
    }
    
    fileprivate func setupDotLayersScale() {
        self.dotLayers.enumerated().forEach { dotLayerItem in
            guard let first = self.fullScaleIndex.first, let last = self.fullScaleIndex.last else {
                return
            }
            
            var transform = CGAffineTransform.identity
            
            if !self.fullScaleIndex.contains(dotLayerItem.offset) {
                var scaleValue: CGFloat = 0
                
                if abs(dotLayerItem.offset - first) == 1 || abs(dotLayerItem.offset - last) == 1 {
                    scaleValue = min(self.middleScaleValue, 1)
                } else if abs(dotLayerItem.offset - first) == 2 || abs(dotLayerItem.offset - last) == 2 {
                    scaleValue = min(self.minScaleValue, 1)
                } else {
                    scaleValue = 0
                }
                
                transform = transform.scaledBy(x: scaleValue, y: scaleValue)
            }
            
            dotLayerItem.element.setAffineTransform(transform)
        }
    }
    
    fileprivate func update() {
        self.dotLayers.enumerated().forEach { dotLayerItem in
            if dotLayerItem.offset == self.currentPage {
                dotLayerItem.element.backgroundColor = self.currentPageTintColor.cgColor
            } else {
                dotLayerItem.element.backgroundColor = self.inactiveTintColor.withAlphaComponent(self.inactiveTransparency).cgColor
            }
        }
        
        guard self.numberOfPages > self.limit else {
            return
        }
        
        self.changeFullScaleIndexsIfNeeded()
        self.setupDotLayersPosition()
        self.setupDotLayersScale()
    }
    
    fileprivate func changeFullScaleIndexsIfNeeded() {
        guard !self.fullScaleIndex.contains(self.currentPage) else {
            return
        }
        
        if (self.fullScaleIndex.last ?? 0) < self.currentPage {
            self.fullScaleIndex[0] = self.currentPage - 2
            self.fullScaleIndex[1] = self.currentPage - 1
            self.fullScaleIndex[2] = self.currentPage
        } else {
            self.fullScaleIndex[0] = self.currentPage
            self.fullScaleIndex[1] = self.currentPage + 1
            self.fullScaleIndex[2] = self.currentPage + 2
        }
    }
}
