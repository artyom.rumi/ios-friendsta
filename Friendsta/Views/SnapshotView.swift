//
//  SnapshotView.swift
//  Friendsta
//
//  Created by Nikita Asabin on 1/3/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class SnapshotView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize(image: nil)
    }
    
    init(frame: CGRect, image: UIImage?) {
        super.init(frame: frame)
        self.initialize(image: image)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize(image: nil)
    }
    
    fileprivate func initialize(image: UIImage?) {
        let backgroundImageView = UIImageView(image: UIImage(named: "InboxBackground"))
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(backgroundImageView)
        
        let imageView = UIImageView(image: image)
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(imageView)
        
        let logoImageView = UIImageView(image: UIImage(named: "AppLogo"))
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(logoImageView)
        
        let FriendstaLabel = UILabel()
        FriendstaLabel.text = "FriendstaSocial.com"
        FriendstaLabel.font = Fonts.bold(ofSize: 61.0)
        FriendstaLabel.minimumScaleFactor = 0.1
        FriendstaLabel.adjustsFontSizeToFitWidth = true
        FriendstaLabel.textColor = UIColor.white.withAlphaComponent(0.55)
        FriendstaLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(FriendstaLabel)
        
        NSLayoutConstraint.activate([backgroundImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0.0),
                                      backgroundImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0.0),
                                      backgroundImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0.0),
                                      backgroundImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0.0)])
        
        NSLayoutConstraint.activate([imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0.0),
                                     imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                                     imageView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                                     imageView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0.0),
                                     imageView.heightAnchor.constraint(equalToConstant: imageView.frame.height * 3),
                                     logoImageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                                     logoImageView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 0.0),
                                     logoImageView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.09),
                                     FriendstaLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
                                     FriendstaLabel.topAnchor.constraint(equalTo: logoImageView.bottomAnchor, constant: -self.bounds.height * 0.01),
                                     FriendstaLabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1)])
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
    }
}
