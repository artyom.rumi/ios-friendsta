//
//  TextBubbleView.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 18.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable final class TextBubbleView: RoundView {
    
    // MARK: - Instance Properties
    
    fileprivate let titleLabel = UILabel()
    fileprivate let messageLabel = UILabel()
    
    // MARK: -
    
    @IBInspectable var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    @IBInspectable var message: String? {
        get {
            return self.messageLabel.text
        }
        
        set {
            self.messageLabel.text = newValue
        }
    }
    
    @IBInspectable var textColor: UIColor = Colors.whiteText {
        didSet {
            self.titleLabel.textColor = self.textColor
            self.messageLabel.textColor = self.textColor
        }
    }
    
    @IBInspectable var rotationAngle: CGFloat = 0.0 {
        didSet {
            let rotationAngleRadians = self.rotationAngle.degreesToRadians
            
            self.titleLabel.transform = CGAffineTransform(rotationAngle: rotationAngleRadians)
            self.messageLabel.transform = CGAffineTransform(rotationAngle: rotationAngleRadians)
        }
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.isUserInteractionEnabled = false
        self.backgroundColor = Colors.blackBackground
        
        self.titleLabel.font = Fonts.heavy(ofSize: 20.0)
        self.titleLabel.textColor = Colors.whiteText
        self.titleLabel.textAlignment = .center
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.titleLabel)
        
        self.messageLabel.font = Fonts.regular(ofSize: 16.0)
        self.messageLabel.numberOfLines = 0
        self.messageLabel.textColor = Colors.whiteText
        self.messageLabel.textAlignment = .center
        self.messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.messageLabel)
        
        NSLayoutConstraint.activate([self.titleLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
                                     self.titleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor)])
        
        NSLayoutConstraint.activate([self.messageLabel.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 10.0),
                                     self.messageLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor)])
    }
}
