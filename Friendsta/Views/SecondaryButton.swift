//
//  SecondaryButton.swift
//  Friendsta
//
//  Created by Marat Galeev on 06.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable final class SecondaryButton: Button {
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.defaultTitleColor = Colors.primary
        self.defaultBackgroundColor = Colors.whiteBackground
        
        self.highlightedTitleColor = Colors.primary.withAlphaComponent(0.75)
        self.highlightedBackgroundColor = Colors.whiteBackground.withAlphaComponent(0.75)
        
        self.titleLabel?.font = Fonts.semiBold(ofSize: 17.0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = min(self.frame.width, self.frame.height) * 0.5
    }
}
