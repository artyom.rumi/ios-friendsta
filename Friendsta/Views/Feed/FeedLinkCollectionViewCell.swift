//
//  FeedLinkCollectionViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 09/10/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class FeedLinkCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Nested Types
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let cornerRadius: CGFloat = 12
        static let friendsEngagedLabelLeadingNearConstant: CGFloat = 16
        static let friendsEngagedLabelLeadingFarConstant: CGFloat = 47
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var linkPreviewView: LinkPreviewView!
    
    @IBOutlet private weak var createdTimeLabel: UILabel!

    @IBOutlet private weak var textLabel: UILabel!
        
    @IBOutlet private weak var viewAllCommentsLabel: UILabel!
    @IBOutlet private weak var viewAllCommentsButton: UIButton!

    @IBOutlet private weak var friendsEngagedLabel: UILabel!
    @IBOutlet private weak var friendsEngagedLabelLeadingConstraint: NSLayoutConstraint!

    @IBOutlet private weak var repostIconImageView: UIImageView!

    @IBOutlet private weak var commentActionLabel: UILabel!
    
    @IBOutlet weak var roundedControl: RoundedControl!
    
    // MARK: -

    var onAddCommentControlTapped: (() -> Void)?
    var onViewAllCommentsButtonTapped: (() -> Void)?
        
    // MARK: -

    var commentAction: String? {
        get {
            return self.commentActionLabel.text
        }

        set {
            self.commentActionLabel.text = newValue
        }
    }
    
    var text: String? {
        get {
            return self.textLabel.text
        }
        
        set {
            self.textLabel.text = newValue
        }
    }
    
    var isTextLabelHidden: Bool {
        get {
            return self.textLabel.isHidden
        }
        
        set {
            self.textLabel.isHidden = newValue
        }
    }
    
    var viewAllComments: String? {
        get {
            return self.viewAllCommentsLabel.text
        }

        set {
            self.viewAllCommentsLabel.text = newValue
        }
    }
    
    var isViewAllCommentsLabelHidden: Bool {
        get {
            return self.viewAllCommentsLabel.isHidden
        }
        
        set {
            self.viewAllCommentsLabel.isHidden = newValue
        }
    }
    
    var isViewAllCommentsButtonHidden: Bool {
        get {
            return self.viewAllCommentsButton.isHidden
        }
        
        set {
            self.viewAllCommentsButton.isHidden = newValue
        }
    }

    var engagedFriends: String? {
        get {
            return self.friendsEngagedLabel.text
        }

        set {
            self.friendsEngagedLabel.text = newValue
        }
    }

    var isEngagedFriendsHidden: Bool {
        get {
            return self.friendsEngagedLabel.isHidden
        }

        set {
            self.friendsEngagedLabel.isHidden = newValue
        }
    }

    var isRepostIconHidden: Bool {
        get {
            return self.repostIconImageView.isHidden
        }

        set {
            self.repostIconImageView.isHidden = newValue
            self.friendsEngagedLabelLeadingConstraint.constant = newValue ? Constants.friendsEngagedLabelLeadingNearConstant : Constants.friendsEngagedLabelLeadingFarConstant
        }
    }
    
    var createdTime: String? {
        get {
            return self.createdTimeLabel.text
        }
        
        set {
            self.createdTimeLabel.text = newValue
        }
    }
    
    var linkPreview: LinkPreviewView {
        return self.linkPreviewView
    }
    
    // MARK: - Instance Methods

    @IBAction private func onAddCommentControlTouchUpInside(_ sender: RoundedControl) {
        Log.high("onAddCommentControlTouchUpInside()", from: self)

        self.onAddCommentControlTapped?()
    }

    @IBAction private func onViewAllCommentsButtonTouchUpInside(_ sender: Any) {
        Log.high("onViewAllCommentsButtonTouchUpInside", from: self)
        
        self.onViewAllCommentsButtonTapped?()
    }

    // MARK: -
    
    private func applyShadow(views: [UIView]) {
        for view in views {
            view.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
        }
    }
    
    // MARK: - UICollectionViewCell

    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.linkPreviewView.linkImageViewTarget.image = nil
    }
}
