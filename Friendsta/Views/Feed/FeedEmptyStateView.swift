//
//  FeedEmptyStateView.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/06/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class FeedEmptyStateView: UIView {

    // MARK: - Instance Properties

    private let gradientView = GradientView()
    private let backgroundImageView = UIImageView()
    private let imageView = UIImageView()

    private let titleLabel = UILabel()
    private let messageLabel = UILabel()
    private let stackView = UIStackView()

    private let arrowImageView = UIImageView()

    // MARK: -

    @IBInspectable var image: UIImage? {
        get {
            return self.imageView.image
        }

        set {
            self.imageView.image = newValue
        }
    }

    @IBInspectable var title: String? {
        get {
            return self.titleLabel.text
        }

        set {
            if let newValue = newValue {
                self.titleLabel.text = newValue
                self.titleLabel.isHidden = false
            } else {
                self.titleLabel.text = nil
                self.titleLabel.isHidden = true
            }
        }
    }

    @IBInspectable var message: String? {
        get {
            return self.messageLabel.text
        }

        set {
            if let newValue = newValue {
                self.messageLabel.text = newValue
                self.messageLabel.isHidden = false
            } else {
                self.messageLabel.text = nil
                self.messageLabel.isHidden = true
            }
        }
    }

    @IBInspectable var isArrowImageHidden: Bool {
        get {
            return self.arrowImageView.isHidden
        }

        set {
            self.arrowImageView.isHidden = newValue
        }
    }

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.initialize()
    }

    // MARK: - Instance Methods

    private func initialize() {
        self.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(self.gradientView)
        self.addSubview(self.backgroundImageView)
        self.addSubview(self.imageView)
        self.addSubview(self.stackView)
        self.addSubview(self.arrowImageView)

        self.configureGradientView()
        self.configureBackgroundImageView()
        self.configureImageView()
        self.configureTitleLabel()
        self.configureMessageLabel()
        self.configureStackView()
        self.configureArrowImageView()
    }

    // MARK: -

    private func configureGradientView() {
        self.gradientView.translatesAutoresizingMaskIntoConstraints = false

        self.gradientView.firstColor = Colors.blackBackground.withAlphaComponent(0.5)
        self.gradientView.secondColor = Colors.clear
        self.gradientView.isVertical = true
        self.gradientView.backgroundColor = Colors.whiteBackground

        NSLayoutConstraint.activate([
            self.gradientView.topAnchor.constraint(equalTo: self.topAnchor),
            self.gradientView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            self.gradientView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            self.gradientView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.29)
        ])
    }

    private func configureBackgroundImageView() {
        self.backgroundImageView.translatesAutoresizingMaskIntoConstraints = false

        self.backgroundImageView.image = UIImage(named: "EmptyStateBackgroundShapeImage")
        self.backgroundImageView.contentMode = .scaleAspectFit
        self.backgroundImageView.clipsToBounds = true

        NSLayoutConstraint.activate([
            self.backgroundImageView.topAnchor.constraint(equalTo: self.imageView.topAnchor, constant: 16),
            self.backgroundImageView.centerXAnchor.constraint(equalTo: self.imageView.centerXAnchor),
            self.backgroundImageView.heightAnchor.constraint(equalToConstant: 122),
            self.backgroundImageView.widthAnchor.constraint(equalToConstant: 207)
        ])
    }

    private func configureImageView() {
        self.imageView.translatesAutoresizingMaskIntoConstraints = false

        self.imageView.contentMode = .scaleAspectFit
        self.imageView.clipsToBounds = true

        NSLayoutConstraint.activate([
            self.imageView.topAnchor.constraint(equalTo: self.gradientView.bottomAnchor, constant: 8),
            self.imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.imageView.heightAnchor.constraint(equalToConstant: 123),
            self.imageView.widthAnchor.constraint(equalToConstant: 123)
        ])
    }

    private func configureTitleLabel() {
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false

        self.titleLabel.textColor = Colors.blackText
        self.titleLabel.font = Fonts.bold(ofSize: 24)
        self.titleLabel.numberOfLines = 0
        self.titleLabel.isHidden = true
        self.titleLabel.textAlignment = .center
    }

    private func configureMessageLabel() {
        self.messageLabel.translatesAutoresizingMaskIntoConstraints = false

        self.messageLabel.textColor = Colors.grayText
        self.messageLabel.font = Fonts.medium(ofSize: 16)
        self.messageLabel.numberOfLines = 0
        self.messageLabel.isHidden = true
        self.messageLabel.textAlignment = .center
    }

    private func configureStackView() {
        self.stackView.translatesAutoresizingMaskIntoConstraints = false

        self.stackView.addArrangedSubview(self.titleLabel)
        self.stackView.addArrangedSubview(self.messageLabel)

        self.stackView.alignment = .fill
        self.stackView.distribution = .fill
        self.stackView.spacing = 16

        NSLayoutConstraint.activate([
            self.stackView.topAnchor.constraint(equalTo: self.backgroundImageView.bottomAnchor, constant: 31),
            self.stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 16),
            self.stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16),
            self.stackView.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
    }

    private func configureArrowImageView() {
        self.arrowImageView.translatesAutoresizingMaskIntoConstraints = false

        self.arrowImageView.image = UIImage(named: "EmptyStateArrowImage")
        self.arrowImageView.contentMode = .scaleAspectFit
        self.arrowImageView.clipsToBounds = true

        NSLayoutConstraint.activate([
            self.arrowImageView.topAnchor.constraint(greaterThanOrEqualTo: self.stackView.bottomAnchor, constant: 15),
            self.arrowImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 55),
            self.arrowImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -14),
            self.arrowImageView.widthAnchor.constraint(equalToConstant: 54)
        ])
    }
}
