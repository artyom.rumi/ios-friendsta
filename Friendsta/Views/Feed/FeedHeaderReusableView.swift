//
//  FeedHeaderReusableView.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 20/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class FeedHeaderReusableView: UICollectionReusableView {

    // MARK: - Instance Properties

    @IBOutlet private weak var anonymousPostControl: UIControl!
    
    @IBOutlet private(set) weak var contentView: UIView!

    // MARK: - UICollectionReusableView

    override func awakeFromNib() {
        super.awakeFromNib()

        self.anonymousPostControl.layer.applyShadow(color: Colors.blackShadow, alpha: 0.15, x: 0, y: 2, blur: 15)
    }
}
