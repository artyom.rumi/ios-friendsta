//
//  FeedEmptyStateCollectionViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 16/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class FeedEmptyStateCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Instance Properties
    
    var addFriendsButtonPressed: (() -> Void)?
    
    // MARK: - Instance Methods
    
    @IBAction private func onAddFriendsButtonTouchUpInside(_ sender: Any) {
        Log.high("onAddFriendsButtonTouchUpInside()", from: self)
        
        self.addFriendsButtonPressed?()
    }
}
