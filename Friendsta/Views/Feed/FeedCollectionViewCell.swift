//
//  FeedCollectionViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 13/06/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class FeedCollectionViewCell: UICollectionViewCell {

    // MARK: - Nested Types

    enum Constants {

        // MARK: - Type Properties

        static let friendsEngagedLabelLeadingNearConstant: CGFloat = 16
        static let friendsEngagedLabelLeadingFarConstant: CGFloat = 47
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var photoPreviewView: PhotoPreviewView!
    
    @IBOutlet private weak var viewAllCommentsLabel: UILabel!
    @IBOutlet private weak var viewAllCommentsButton: UIButton!

    @IBOutlet private weak var textLabel: UILabel!
    
    @IBOutlet private weak var friendsEngagedLabel: UILabel!
    @IBOutlet private weak var friendsEngagedLabelLeadingConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var repostIconImageView: UIImageView!
    
    @IBOutlet private weak var createdTimeLabel: UILabel!
    
    @IBOutlet private weak var commentActionLabel: UILabel!
    
    @IBOutlet weak var roundedControl: RoundedControl!
    
    @IBOutlet weak var RateView: UIView!
    
    @IBOutlet weak var noneRateView: UIView!
    
    @IBOutlet weak var rate1: UIButton!
    @IBOutlet weak var rate2: UIButton!
    @IBOutlet weak var rate3: UIButton!
    @IBOutlet weak var rate4: UIButton!
    @IBOutlet weak var rate5: UIButton!
    @IBOutlet weak var rate6: UIButton!
    @IBOutlet weak var rate7: UIButton!
    @IBOutlet weak var rate8: UIButton!
    @IBOutlet weak var rate9: UIButton!
    @IBOutlet weak var rate10: UIButton!
    
    @IBOutlet weak var rateScoreView: UIView!
    @IBOutlet weak var rateScoreLabel: UILabel!
    
    @IBOutlet weak var rateTenTipView: UIView!
    @IBOutlet weak var rateTenTipImage: UIImageView!
    @IBOutlet weak var rateTenThumb: UIImageView!
    
    @objc func rateTenTipViewPressed() {
        UIView.animate(withDuration: 2.0) {
            self.rateTenTipView.isHidden = true
            Managers.userDefaultsManager.isUserHasSeenTenRateToRequestFriend = true
        }
    }
    // MARK: -
    var onAddCommentControlTapped: (() -> Void)?
    var onViewAllCommentsButtonTapped: (() -> Void)?

    // MARK: -
    
    var text: String? {
        get {
            return self.textLabel.text
        }
        
        set {
            self.textLabel.text = newValue
        }
    }
    
    var isTextLabelHidden: Bool {
        get {
            return self.textLabel.isHidden
        }
        
        set {
            self.textLabel.isHidden = newValue
        }
    }

    var viewAllComments: String? {
        get {
            return self.viewAllCommentsLabel.text
        }

        set {
            self.viewAllCommentsLabel.text = newValue
        }
    }

    var isViewAllCommentsLabelHidden: Bool {
           get {
               return self.viewAllCommentsLabel.isHidden
           }

           set {
               self.viewAllCommentsLabel.isHidden = newValue
           }
    }
    
    var isViewAllCommentsButtonHidden: Bool {
        get {
            return self.viewAllCommentsButton.isHidden
        }
        
        set {
            self.viewAllCommentsButton.isHidden = newValue
        }
    }

    var engagedFriends: String? {
        get {
            return self.friendsEngagedLabel.text
        }

        set {
            self.friendsEngagedLabel.text = newValue
        }
    }

    var isEngagedFriendsHidden: Bool {
        get {
            return self.friendsEngagedLabel.isHidden
        }

        set {
            self.friendsEngagedLabel.isHidden = newValue
        }
    }
    
    var createdTime: String? {
        get {
            return self.createdTimeLabel.text
        }
        
        set {
            self.createdTimeLabel.text = newValue
        }
    }
    
    var isRepostIconHidden: Bool {
        get {
            return self.repostIconImageView.isHidden
        }
        
        set {
            self.repostIconImageView.isHidden = newValue
            self.friendsEngagedLabelLeadingConstraint.constant = newValue ? Constants.friendsEngagedLabelLeadingNearConstant : Constants.friendsEngagedLabelLeadingFarConstant
        }
    }
    
    var isCommentActionHidden: Bool {
        get {
            return self.commentActionLabel.isHidden
        }
        
        set {
            self.commentActionLabel.isHidden = newValue
            //self.friendsEngagedLabelLeadingConstraint.constant = newValue ? Constants.friendsEngagedLabelLeadingNearConstant : Constants.friendsEngagedLabelLeadingFarConstant
        }
    }
    
    var commentAction: String? {
        get {
            return self.commentActionLabel.text
        }
        
        set {
            self.commentActionLabel.text = newValue
        }
    }
    
    var photoPreview: PhotoPreviewView {
        return self.photoPreviewView
    }

    // MARK: - Instance Methods

    @IBAction private func onAddCommentControlTouchUpInside(_ sender: RoundedControl) {
        Log.high("onAddCommentControlTouchUpInside()---------", from: self)

        self.onAddCommentControlTapped?()
    }
    
    @IBAction private func onViewAllCommentsButtonTouchUpInside(_ sender: Any) {
        Log.high("onViewAllCommentsButtonTouchUpInside()", from: self)
        
        self.onViewAllCommentsButtonTapped?()
    }

    // MARK: - UICollectionViewCell

    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(target: self, action: #selector(rateTenTipViewPressed))
        rateTenTipImage.addGestureRecognizer(tap)
        rateTenTipImage.isUserInteractionEnabled = true
        rateTenThumb.addGestureRecognizer(tap)
        rateTenThumb.isUserInteractionEnabled = true
        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.photoPreviewView.imageViewTarget.image = nil
    }
}
