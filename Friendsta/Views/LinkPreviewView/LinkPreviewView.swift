//
//  LinkPreviewView.swift
//  Friendsta
//
//  Created by Elina Batyrova on 25.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class LinkPreviewView: NibLoadingView {
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var loadingView: RoundedView!
     
    @IBOutlet private weak var linkURLLabel: UILabel!
    @IBOutlet private weak var linkDescriptionLabel: UILabel!
    @IBOutlet private weak var linkImageView: UIImageView!
     
    @IBOutlet private weak var menuStackView: UIStackView!
     
    @IBOutlet private weak var bottomRightButton: UIButton!
    @IBOutlet private weak var topRightButton: UIButton!
    
    // MARK: -
    
    var linkImageViewTarget: UIImageView {
        return self.linkImageView
    }
    
    var linkURL: String? {
        get {
            return self.linkURLLabel.text
        }
        
        set {
            self.linkURLLabel.text = newValue
        }
    }
    
    var linkDescription: String? {
        get {
            return self.linkDescriptionLabel.text
        }
        
        set {
            self.linkDescriptionLabel.text = newValue
        }
    }
    
    var isLoadingViewHidden: Bool {
        get {
            return self.loadingView.isHidden
        }
        
        set {
            self.loadingView.isHidden = newValue
        }
    }
    
    var bottomRightButtonImage: UIImage? {
        get {
            return self.bottomRightButton.image(for: .normal)
        }
        
        set {
            self.bottomRightButton.setImage(newValue, for: .normal)
        }
    }
    
    var topRightButtonImage: UIImage? {
        get {
            return self.topRightButton.image(for: .normal)
        }
        
        set {
            self.topRightButton.setImage(newValue, for: .normal)
        }
    }
    
    var isBottomRightButtonHidden: Bool = true {
        didSet {
            self.configureButtons()
        }
    }
    
    var isTopRightButtonHidden: Bool = true {
        didSet {
            self.configureButtons()
        }
    }
    
    var menuItems: [MenuItemView] = [] {
        didSet {
            self.configureMenu()
        }
    }
    
    var onBottomRightButtonTapped: (() -> Void)?
    var onTopRightButtonTapped: (() -> Void)?
    var onLinkTapped: (() -> Void)?
    
    // MARK: - Instance Methods

    @IBAction private func onLinkTouchUpInside(_ sender: Any) {
        Log.high("onLinkTouchUpInside()", from: self)
        
        self.onLinkTapped?()
    }
    
    @IBAction private func onBottomRightButtonTouchUpInside(_ sender: Any) {
        Log.high("onBottomRightButtonTouchUpInside()", from: self)
        
        self.onBottomRightButtonTapped?()
    }
    
    @IBAction private func onTopRightButtonTouchUpInside(_ sender: Any) {
        Log.high("onTopRightButtonTouchUpInside()", from: self)
        
        self.onTopRightButtonTapped?()
    }
    
    // MARK: -
    
    private func configureMenu() {
        self.menuStackView.arrangedSubviews.forEach({ $0.removeFromSuperview() })
        
        for menuItem in self.menuItems {
            self.menuStackView.addArrangedSubview(menuItem)
        }
    }
    
    private func configureButtons() {
        self.topRightButton.isHidden = self.isTopRightButtonHidden
        self.bottomRightButton.isHidden = self.isBottomRightButtonHidden
        
        self.topRightButton.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
        self.bottomRightButton.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.configureButtons()
    }
}
