//
//  ActivityIndicatedButton.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 09/04/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class ActivityIndicatedButton: UIControl {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let transitionOptions: UIView.AnimationOptions = [.transitionCrossDissolve,
                                                                 .allowAnimatedContent,
                                                                 .allowUserInteraction]
    }
    
    // MARK: - Instance Properties
    
    fileprivate var titleLabel = UILabel()
    
    fileprivate var activityIndicatorView: UIActivityIndicatorView?
    
    // MARK: -
    
    @IBInspectable var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    @IBInspectable var titleAlignment: NSTextAlignment {
        get {
            return self.titleLabel.textAlignment
        }
        
        set {
            self.titleLabel.textAlignment = newValue
        }
    }
    
    @IBInspectable var activityIndicatorAlignment: CGFloat = 0.5 {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    var isActivityIndicatorHidden: Bool {
        return self.activityIndicatorView?.isHidden ?? true
    }
    
    // MARK: - UIControl
    
    override var isHighlighted: Bool {
        didSet {
            if self.isHighlighted {
                UIView.transition(with: self, duration: 0.05, options: Constants.transitionOptions, animations: {
                    self.titleLabel.textColor = Colors.secondary.withAlphaComponent(0.75)
                })
            } else {
                UIView.transition(with: self, duration: 0.25, options: Constants.transitionOptions, animations: {
                    self.titleLabel.textColor = Colors.secondary
                })
            }
        }
    }
    
    // MARK: - Initializers

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.titleLabel.font = Fonts.medium(ofSize: 17.0)
        self.titleLabel.textColor = Colors.secondary
        self.titleLabel.numberOfLines = 1
        self.titleLabel.textAlignment = .center
        self.titleLabel.isUserInteractionEnabled = false
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.titleLabel)
        
        NSLayoutConstraint.activate([self.titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                                     self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor),
                                     self.titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                                     self.titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor)])
    }
    
    // MARK: -
    
    func showActivityIndicator() {
        if self.activityIndicatorView == nil {
            let activityIndicatorView = UIActivityIndicatorView()
            
            activityIndicatorView.color = Colors.lightActivityIndicator
            activityIndicatorView.hidesWhenStopped = true
            
            self.addSubview(activityIndicatorView)
            self.activityIndicatorView = activityIndicatorView
        }

        self.activityIndicatorView!.startAnimating()
        
        self.titleLabel.isHidden = true
    }
    
    func hideActivityIndicator() {
        self.activityIndicatorView?.stopAnimating()
        
        self.titleLabel.isHidden = false
    }
    
    // MARK: - UIView
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let activityIndicatorView = self.activityIndicatorView {
            let activityIndicatorAlignment = max(min(self.activityIndicatorAlignment, 1.0), 0.0)
            let activityIndicatorAlignmentWidth = self.bounds.width - activityIndicatorView.frame.width
            
            self.activityIndicatorView?.frame = CGRect(x: activityIndicatorAlignmentWidth * activityIndicatorAlignment,
                                                       y: (self.bounds.height - activityIndicatorView.frame.height) * 0.5,
                                                       size: activityIndicatorView.frame.size).adjusted
        }
    }
}
