//
//  ActivityTableViewCell.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/22/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class ActivityTableViewCell: UITableViewCell {

    // MARK: - Nested Types

    enum CellType {

        // MARK: - Enumeration Cases

        case like
        case repost
        case comment
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subTitleLabel: UILabel!
    @IBOutlet private weak var feedRoundImageView: RectangularImageView!
    @IBOutlet private weak var badgeImageView: UIImageView!
    @IBOutlet private weak var dateLabel: UILabel!

    // MARK: -

    var feedImage: UIImage? {
        get {
            return self.feedRoundImageView.image
        }
        set {
            self.feedRoundImageView.image = newValue
        }
    }

    var cellType: CellType {
        get {
            return self.currentCellType
        }
        set {
            switch newValue {
            case .like:
                self.badgeImageView.image = UIImage(named: "LikeBadge")

            case .repost:
                self.badgeImageView.image = UIImage(named: "RepostBadge")

            case .comment:
                self.badgeImageView.image = UIImage(named: "CommentBadge")
            }
        }
    }

    var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }

    var subTitle: String? {
        get {
            return self.subTitleLabel.text
        }
        set {
            self.subTitleLabel.text = newValue
        }
    }

    var date: String? {
        get {
            return self.dateLabel.text
        }
        set {
            self.dateLabel.text = newValue
        }
    }

    var onCellSelected: (() -> Void)?

    // MARK: -

    private var currentCellType: CellType = .like

    // MARK: - UITableViewCell Methods

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.onCellSelected?()
        }
    }

    // MARK: - Instance Methods

    func setIsUnread(_ isUnread: Bool) {
        if isUnread {
            self.titleLabel.font = Fonts.bold(ofSize: 16)
        } else {
            self.titleLabel.font = Fonts.regular(ofSize: 16)
        }
    }
}
