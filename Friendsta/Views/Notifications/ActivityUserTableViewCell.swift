//
//  ActivityUserTableViewCell.swift
//  Friendsta
//
//  Created by Nikita Asabin on 12/25/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import UIKit

class ActivityUserTableViewCell: UITableViewCell {

    // MARK: - Nested Types

    enum CellType {

        // MARK: - Enumeration Cases

        case newFriend
        case expireFriend72h
        case expireFriend12h
        case expiredFriendship
        case rateFriend
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var badgeImageView: UIImageView!
    @IBOutlet private weak var avatarImageView: RoundImageView!
    @IBOutlet private weak var subTitleLabel: UILabel!
    @IBOutlet private weak var keepButton: UIButton!
    @IBOutlet private weak var keepActivityIndicator: UIActivityIndicatorView!

    // MARK: -

    var avatarImage: UIImage? {
        get {
            return self.avatarImageView?.image
        }
        set {
            self.avatarImageView?.image = newValue
        }
    }

    var cellType: CellType {
        get {
            return self.currentCellType
        }
        set {
            switch newValue {
            case .newFriend:
                self.badgeImageView.image = UIImage(named: "PersonBadge")
                self.keepButton.isHidden = true

            case .expireFriend12h:
                self.badgeImageView.image = UIImage(named: "ExpireRedBadge")
                self.keepButton.isHidden = false

            case .expireFriend72h:
                self.badgeImageView.image = UIImage(named: "ExpireYellowBadge")
                self.keepButton.isHidden = false

            case .expiredFriendship:
                self.badgeImageView.image = UIImage(named: "ExpireRedBadge")
                self.keepButton.isHidden = true
                
            case .rateFriend:
                self.badgeImageView.image = UIImage(named: "PersonBadge")
                self.keepButton.isHidden = false
                self.keepButton.setTitle("Add", for: .normal)
            }
            
        }
    }

    var title: String? {
        get {
            return self.titleLabel.text
        }
        set {
            self.titleLabel.text = newValue
        }
    }

    var subTitle: String? {
        get {
            return self.subTitleLabel.text
        }
        set {
            self.subTitleLabel.text = newValue
        }
    }

    var keepActivityIndicatorAnimating: Bool {
        get {
            return self.keepActivityIndicator.isAnimating
        }
        set {
            if newValue {
                self.keepActivityIndicator.startAnimating()
            } else {
                self.keepActivityIndicator.stopAnimating()
            }
        }
    }

    var onKeepTapped: (() -> Void)?
    var onCellSelected: (() -> Void)?

    // MARK: -

    private var currentCellType: CellType = .newFriend

    // MARK: - UITableViewCell Methods

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.onCellSelected?()
        }
    }

    // MARK: - Instance Methods

    func setIsUnread(_ isUnread: Bool) {
        if isUnread {
            self.titleLabel.font = Fonts.bold(ofSize: 16)
        } else {
            self.titleLabel.font = Fonts.regular(ofSize: 16)
        }
    }
    @IBAction private func onKeepTapped(_ sender: Any) {
        self.onKeepTapped?()
    }
}
