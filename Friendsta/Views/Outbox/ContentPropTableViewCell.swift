//
//  ContentPropTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 11.01.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class ContentPropTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var messageLabel: UILabel!
    @IBOutlet fileprivate weak var emojiLabel: UILabel!
    @IBOutlet fileprivate weak var propButton: Button!
    
    // MARK: -
    
    var emoji: String? {
        get {
            return self.emojiLabel.text
        }
        
        set {
            self.emojiLabel.text = newValue
        }
    }
    
    var message: String? {
        get {
            return self.messageLabel.text
        }
        
        set {
            self.messageLabel.text = newValue
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.messageLabel.font = Fonts.regular(ofSize: 16.0)
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
