//
//  CustomPropTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 09.10.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class CustomPropTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var composeBoostLabel: UILabel!
    
    // MARK: -
    
    var onCustomPropButtonClicked: (() -> Void)?
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onCustomPropButtonTouchUpInside(_ sender: Button) {
        self.onCustomPropButtonClicked?()
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.composeBoostLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
