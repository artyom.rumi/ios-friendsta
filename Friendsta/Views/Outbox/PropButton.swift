//
//  PropButton.swift
//  Friendsta
//
//  Created by Marat Galeev on 19.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class PropButton: UIControl {
    
    // MARK: - Nested Types
    
    enum ExclusionPosition {
        case left
        case right
    }
    
    // MARK: - Instance Properties
    
    fileprivate let messageLabel = UITextView()
    fileprivate let emojiLabel = UILabel()
    
    // MARK: -
    
    @IBInspectable var message: String? {
        get {
            return self.messageLabel.text
        }
        
        set {
            self.messageLabel.text = newValue
        }
    }
    
    @IBInspectable var emoji: String? {
        get {
            return self.emojiLabel.text
        }
        
        set {
            self.emojiLabel.text = newValue
        }
    }
    
    var exclusionPaths: [UIBezierPath] {
        get {
            return self.messageLabel.textContainer.exclusionPaths
        }
        
        set {
            self.messageLabel.textContainer.exclusionPaths = newValue
        }
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.backgroundColor = .white
        
        self.layer.cornerRadius = 9.0
        self.layer.masksToBounds = true
        
        if UIScreen.main.bounds.height < 667 {
            self.messageLabel.font = Fonts.regular(ofSize: 13.0)
        } else {
            self.messageLabel.font = Fonts.regular(ofSize: 17.0)
        }
        
        self.messageLabel.isScrollEnabled = false
        self.messageLabel.textAlignment = .left
        self.messageLabel.isUserInteractionEnabled = false
        self.messageLabel.translatesAutoresizingMaskIntoConstraints = false
        self.messageLabel.contentInset = UIEdgeInsets(top: -6, left: 0, bottom: 0, right: 0)
        
        self.addSubview(self.messageLabel)
        
        if UIScreen.main.bounds.height < 667 {
            self.emojiLabel.font = Fonts.regular(ofSize: 20.0)
        } else {
            self.emojiLabel.font = Fonts.regular(ofSize: 24.0)
        }
        
        self.emojiLabel.numberOfLines = 1
        self.emojiLabel.textAlignment = .left
        self.emojiLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.emojiLabel)
    }
    
    func setExclusionPath(position: ExclusionPosition, size: CGSize, radius: CGFloat) {
        let contentLeft = self.bounds.width * 24.0 / 375.0
        let contentRight = self.bounds.width - contentLeft
        let contentTop = self.bounds.width * 20.0 / 375.0
        let contentWidth = contentRight - contentLeft
        
        switch position {
        case .left:
            self.exclusionPaths = [UIBezierPath(roundedRect: CGRect(x: -contentLeft, y: -contentTop, width: size.width, height: size.height), cornerRadius: radius)]
            
            break
        case .right:
            self.exclusionPaths = [UIBezierPath(roundedRect: CGRect(x: contentWidth - (size.width - contentLeft), y: -contentTop, size: size), cornerRadius: radius)]
            break
        }
    }
    
    // MARK: - UIView
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let contentLeft = self.bounds.width * 24.0 / 375.0
        let contentRight = self.bounds.width - contentLeft
        let contentWidth = contentRight - contentLeft
        
        let contentTop = self.bounds.width * 20.0 / 375.0
        let contentBottom = self.bounds.height - contentTop
        
        let emojiLabelSize = self.emojiLabel.sizeThatFits(CGSize(width: contentWidth,
                                                                   height: self.bounds.height))
        
        self.emojiLabel.frame = CGRect(x: contentLeft,
                                       y: contentBottom - emojiLabelSize.height,
                                       width: emojiLabelSize.width,
                                       height: emojiLabelSize.height).adjusted
        
        let messageLabelHeight = self.messageLabel.sizeThatFits(CGSize(width: contentWidth,
                                                                       height: self.bounds.height)).height
        
        self.messageLabel.frame = CGRect(x: contentLeft,
                                         y: contentTop,
                                         width: contentWidth,
                                         height: messageLabelHeight).adjusted
    }
}

// MARK: - NSCopying

extension PropButton: NSCopying {
    
    // MARK: - Instance Methods
    
    func copy(with zone: NSZone? = nil) -> Any {
        let propButton = PropButton()
        
        propButton.message = self.message
        propButton.emoji = self.emoji
        propButton.frame = self.frame
        propButton.center = self.center
        
        return propButton
    }
}
