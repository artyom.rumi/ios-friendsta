//
//  PropTableViewCell.swift
//  Friendsta
//
//  Created by Marat Galeev on 03.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class PropTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var emojiLabel: UILabel!
    @IBOutlet fileprivate weak var messageLabel: UILabel!
    @IBOutlet fileprivate weak var propButton: Button!
    
    // MARK: -
    
    var onPropButtonClicked: (() -> Void)?
    
    // MARK: -
    
    var emoji: String? {
        get {
            return self.emojiLabel.text
        }
        
        set {
            self.emojiLabel.text = newValue
        }
    }
    
    var message: String? {
        get {
            return self.messageLabel.text
        }
        
        set {
            self.messageLabel.text = newValue
        }
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onPropButtonTouchUpInside(_ sender: Button) {
        self.onPropButtonClicked?()
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.messageLabel.font = Fonts.regular(ofSize: 16.0)
    }
    
    // MARK: -
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}

// MARK: - NSCopying

extension PropTableViewCell: NSCopying {
    
    // MARK: - Instance Methods
    
    func copy(with zone: NSZone? = nil) -> Any {
        let cell = ContentPropTableViewCell().loadNib() as! ContentPropTableViewCell
        
        let newFrame = CGRect(x: self.propButton.frame.origin.x,
                              y: self.propButton.frame.origin.y,
                              width: self.propButton.frame.width,
                              height: self.frame.height + 0.5)
        
        cell.frame = newFrame
        cell.emoji = self.emoji
        cell.message = self.message
        
        return cell
    }
}
