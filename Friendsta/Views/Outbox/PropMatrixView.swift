//
//  PropMatrixView.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class PropMatrixView: UIView {
    
    // MARK: -
    
    fileprivate enum Constants {
        static let overlapViewSize = CGSize(width: 22, height: 22)
    }
    
    // MARK: - Instance Properties
    
    fileprivate let topLeftPropButton = PropButton()
    fileprivate let topRightPropButton = PropButton()
    fileprivate let bottomLeftPropButton = PropButton()
    fileprivate let bottomRightPropButton = PropButton()
    
    fileprivate let customPropButton = CustomPropButton()
    
    // MARK: -
    
    @IBInspectable var topLeftPropMessage: String? {
        get {
            return self.topLeftPropButton.message
        }
        
        set {
            self.topLeftPropButton.message = newValue
        }
    }
    
    @IBInspectable var topLeftPropEmoji: String? {
        get {
            return self.topLeftPropButton.emoji
        }
        
        set {
            self.topLeftPropButton.emoji = newValue
        }
    }
    
    @IBInspectable var isTopLeftPropEnabled: Bool {
        get {
            return self.topLeftPropButton.isEnabled
        }
        
        set {
            self.topLeftPropButton.isEnabled = newValue
            
            self.updateTopLeftPropButton()
        }
    }
    
    @IBInspectable var topRightPropMessage: String? {
        get {
            return self.topRightPropButton.message
        }
        
        set {
            self.topRightPropButton.message = newValue
        }
    }
    
    @IBInspectable var topRightPropEmoji: String? {
        get {
            return self.topRightPropButton.emoji
        }
        
        set {
            self.topRightPropButton.emoji = newValue
        }
    }
    
    @IBInspectable var isTopRightPropEnabled: Bool {
        get {
            return self.topRightPropButton.isEnabled
        }
        
        set {
            self.topRightPropButton.isEnabled = newValue
            
            self.updateTopRightPropButton()
        }
    }
    
    @IBInspectable var bottomLeftPropMessage: String? {
        get {
            return self.bottomLeftPropButton.message
        }
        
        set {
            self.bottomLeftPropButton.message = newValue
        }
    }
    
    @IBInspectable var bottomLeftPropEmoji: String? {
        get {
            return self.bottomLeftPropButton.emoji
        }
        
        set {
            self.bottomLeftPropButton.emoji = newValue
        }
    }
    
    @IBInspectable var isBottomLeftPropEnabled: Bool {
        get {
            return self.bottomLeftPropButton.isEnabled
        }
        
        set {
            self.bottomLeftPropButton.isEnabled = newValue
            
            self.updateBottomLeftPropButton()
        }
    }
    
    @IBInspectable var bottomRightPropMessage: String? {
        get {
            return self.bottomRightPropButton.message
        }
        
        set {
            self.bottomRightPropButton.message = newValue
        }
    }
    
    @IBInspectable var bottomRightPropEmoji: String? {
        get {
            return self.bottomRightPropButton.emoji
        }
        
        set {
            self.bottomRightPropButton.emoji = newValue
        }
    }
    
    @IBInspectable var isBottomRightPropEnabled: Bool {
        get {
            return self.bottomRightPropButton.isEnabled
        }
        
        set {
            self.bottomRightPropButton.isEnabled = newValue
            
            self.updateBottomRightPropButton()
        }
    }
    
    @IBInspectable var isCustomPropHidden: Bool {
        get {
            return self.customPropButton.isHidden
        }
        
        set {
            self.customPropButton.isHidden = newValue
            self.topLeftPropButton.isHidden = !newValue
        }
    }
    
    var topLeftView: UIView {
        return self.topLeftPropButton
    }
    
    var topRightView: UIView {
        return self.topRightPropButton
    }
    
    var bottomLeftView: UIView {
        return self.bottomLeftPropButton
    }
    
    var bottomRightView: UIView {
        return self.bottomRightPropButton
    }

    var onTopLeftPropButtonClicked: (() -> Void)?
    var onTopRightPropButtonClicked: (() -> Void)?
    
    var onBottomLeftPropButtonClicked: (() -> Void)?
    var onBottomRightPropButtonClicked: (() -> Void)?
    
    var onCustomPropButtonClicked: (() -> Void)?
    
    var isTopLeftPropSelected: Bool {
        return self.topLeftPropButton.isSelected
    }
    
    var isTopRightPropSelected: Bool {
        return self.topRightPropButton.isSelected
    }
    
    var isBottomLeftPropSelected: Bool {
        return self.bottomLeftPropButton.isSelected
    }
    
    var isBottomRightPropSelected: Bool {
        return self.bottomRightPropButton.isSelected
    }
    
    fileprivate(set) var isWaiting = false
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    @objc fileprivate func onTopLeftPropButtonTouchUpInside(_ sender: PropButton) {
        self.onTopLeftPropButtonClicked?()
    }
    
    @objc fileprivate func onTopRightPropButtonTouchUpInside(_ sender: PropButton) {
        self.onTopRightPropButtonClicked?()
    }
    
    @objc fileprivate func onBottomLeftPropButtonTouchUpInside(_ sender: PropButton) {
        self.onBottomLeftPropButtonClicked?()
    }
    
    @objc fileprivate func onBottomRightPropButtonTouchUpInside(_ sender: PropButton) {
        self.onBottomRightPropButtonClicked?()
    }
    
    @objc fileprivate func onCustomPropButtonTouchUpInside(_ sender: PropButton) {
        self.onCustomPropButtonClicked?()
    }
    
    // MARK: -
    
    fileprivate func initialize() {
        self.topLeftPropButton.addTarget(self,
                                         action: #selector(self.onTopLeftPropButtonTouchUpInside(_:)),
                                         for: .touchUpInside)
        
        self.topRightPropButton.addTarget(self,
                                          action: #selector(self.onTopRightPropButtonTouchUpInside(_:)),
                                          for: .touchUpInside)
        
        self.bottomLeftPropButton.addTarget(self,
                                            action: #selector(self.onBottomLeftPropButtonTouchUpInside(_:)),
                                            for: .touchUpInside)
        
        self.bottomRightPropButton.addTarget(self,
                                             action: #selector(self.onBottomRightPropButtonTouchUpInside(_:)),
                                             for: .touchUpInside)
        
        self.customPropButton.addTarget(self,
                                        action: #selector(self.onCustomPropButtonTouchUpInside(_:)),
                                        for: .touchUpInside)
        
        self.customPropButton.isHidden = true
        
        self.addSubview(self.topLeftPropButton)
        self.addSubview(self.topRightPropButton)
        
        self.addSubview(self.bottomLeftPropButton)
        self.addSubview(self.bottomRightPropButton)
        
        self.addSubview(self.customPropButton)
    }
    
    fileprivate func updateTopLeftPropButton() {
        self.topLeftPropButton.alpha = self.topLeftPropButton.isEnabled ? 1.0 : 0.4
    }
    
    fileprivate func updateTopRightPropButton() {
        self.topRightPropButton.alpha = self.topRightPropButton.isEnabled ? 1.0 : 0.4
    }
    
    fileprivate func updateBottomLeftPropButton() {
        self.bottomLeftPropButton.alpha = self.bottomLeftPropButton.isEnabled ? 1.0 : 0.4
    }
    
    fileprivate func updateBottomRightPropButton() {
        self.bottomRightPropButton.alpha = self.bottomRightPropButton.isEnabled ? 1.0 : 0.4
    }
    
    fileprivate func updateCustomPropButton() {
        self.customPropButton.alpha = 1.0
    }
    
    fileprivate func enablePropButtons() {
        self.topLeftPropButton.isUserInteractionEnabled = true
        self.topRightPropButton.isUserInteractionEnabled = true
        self.bottomLeftPropButton.isUserInteractionEnabled = true
        self.bottomRightPropButton.isUserInteractionEnabled = true
        
        self.customPropButton.isUserInteractionEnabled = true
    }
    
    fileprivate func disablePropButtons() {
        self.topLeftPropButton.isUserInteractionEnabled = false
        self.topRightPropButton.isUserInteractionEnabled = false
        self.bottomLeftPropButton.isUserInteractionEnabled = false
        self.bottomRightPropButton.isUserInteractionEnabled = false
        
        self.customPropButton.isUserInteractionEnabled = false
    }
    
    // MARK: -
    
    func hidePropSelectionState() {
        self.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.25, animations: {
            self.topLeftPropButton.transform = .identity
            self.topRightPropButton.transform = .identity
            self.bottomLeftPropButton.transform = .identity
            self.bottomRightPropButton.transform = .identity
        })
        
        self.topLeftPropButton.isSelected = false
        self.topRightPropButton.isSelected = false
        self.bottomLeftPropButton.isSelected = false
        self.bottomRightPropButton.isSelected = false
        
        self.enablePropButtons()
    }
    
    func showWaitingState() {
        self.isWaiting = true
        
        UIView.animate(withDuration: 0.25, animations: {
            self.topLeftPropButton.alpha = self.topLeftPropButton.isSelected ? 1.0 : 0.25
            self.topRightPropButton.alpha = self.topRightPropButton.isSelected ? 1.0 : 0.25
            self.bottomLeftPropButton.alpha = self.bottomLeftPropButton.isSelected ? 1.0 : 0.25
            self.bottomRightPropButton.alpha = self.bottomRightPropButton.isSelected ? 1.0 : 0.25
            
            self.customPropButton.alpha = 0.25
        }, completion: { finished in
            if self.isWaiting && finished {
                UIView.animate(withDuration: 0.7, delay: 0.0, options: [.autoreverse, .repeat], animations: {
                    self.topLeftPropButton.alpha = self.topLeftPropButton.isSelected ? 1.0 : 0.4
                    self.topRightPropButton.alpha = self.topRightPropButton.isSelected ? 1.0 : 0.4
                    self.bottomLeftPropButton.alpha = self.bottomLeftPropButton.isSelected ? 1.0 : 0.4
                    self.bottomRightPropButton.alpha = self.bottomRightPropButton.isSelected ? 1.0 : 0.4
                    
                    self.customPropButton.alpha = 0.4
                })
            }
        })
    }
    
    func hideWaitingState() {
        self.isWaiting = false
        
        self.topLeftPropButton.layer.removeAllAnimations()
        self.topRightPropButton.layer.removeAllAnimations()
        self.bottomLeftPropButton.layer.removeAllAnimations()
        self.bottomRightPropButton.layer.removeAllAnimations()
        
        self.topLeftPropButton.transform = .identity
        self.topRightPropButton.transform = .identity
        self.bottomLeftPropButton.transform = .identity
        self.bottomRightPropButton.transform = .identity
        
        UIView.animate(withDuration: 0.25, animations: {
            self.updateTopLeftPropButton()
            self.updateTopRightPropButton()
            self.updateBottomLeftPropButton()
            self.updateBottomRightPropButton()
            
            self.updateCustomPropButton()
        })
    }
    
    // MARK: - UIView
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let contentLeft = self.bounds.width * 16.0 / 375.0
        let contentRight = self.bounds.width - contentLeft
        
        let contentTop = self.bounds.height * 16.0 / 375.0
        let contentBottom = self.bounds.height - contentTop
        
        let propButtonWidth = (contentRight - contentLeft - 6.0) * 0.5
        let propButtonHeight = (contentBottom - contentTop - 6.0) * 0.5
        
        self.topLeftPropButton.center = CGPoint(x: contentLeft + propButtonWidth * 0.5,
                                                y: contentTop + propButtonHeight * 0.5).adjusted
        
        self.topLeftPropButton.bounds = CGRect(x: 0.0,
                                               y: 0.0,
                                               width: propButtonWidth,
                                               height: propButtonHeight).adjusted
        
        self.topRightPropButton.center = CGPoint(x: contentRight - propButtonWidth * 0.5,
                                                 y: contentTop + propButtonHeight * 0.5).adjusted
        
        self.topRightPropButton.bounds = CGRect(x: 0.0,
                                                y: 0.0,
                                                width: propButtonWidth,
                                                height: propButtonHeight).adjusted
        
        self.bottomLeftPropButton.center = CGPoint(x: contentLeft + propButtonWidth * 0.5,
                                                   y: contentBottom - propButtonHeight * 0.5).adjusted
        
        self.bottomLeftPropButton.bounds = CGRect(x: 0.0,
                                                  y: 0.0,
                                                  width: propButtonWidth,
                                                  height: propButtonHeight).adjusted
        
        self.bottomLeftPropButton.setExclusionPath(position: .left, size: Constants.overlapViewSize, radius: 22)
        
        self.bottomRightPropButton.center = CGPoint(x: contentRight - propButtonWidth * 0.5,
                                                    y: contentBottom - propButtonHeight * 0.5).adjusted
        
        self.bottomRightPropButton.bounds = CGRect(x: 0.0,
                                                   y: 0.0,
                                                   width: propButtonWidth,
                                                   height: propButtonHeight).adjusted
        
        self.bottomRightPropButton.setExclusionPath(position: .right, size: Constants.overlapViewSize, radius: 22)
        
        self.customPropButton.center = self.topLeftPropButton.center
        self.customPropButton.bounds = self.topLeftPropButton.bounds
    }
}
