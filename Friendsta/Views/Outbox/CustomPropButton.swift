//
//  CustomPropButton.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 08/10/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class CustomPropButton: UIControl {
    
    // MARK: - Instance Properties
    
    fileprivate let imageView = UIImageView(image: #imageLiteral(resourceName: "OutboxCustomPropIcon"))
    fileprivate let titleLabel = UILabel()

    // MARK: - Initializers

    override init(frame: CGRect = .zero) {
        super.init(frame: frame)

        self.initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.initialize()
    }

    // MARK: - Instance Methods

    fileprivate func initialize() {
        self.backgroundColor = .white

        self.layer.cornerRadius = 9.0
        self.layer.masksToBounds = true
        
        self.imageView.isUserInteractionEnabled = false
        self.imageView.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(self.imageView)
        
        self.titleLabel.text = "Compose a note".localized()
        
        if UIScreen.main.bounds.height < 667 {
            self.titleLabel.font = Fonts.regular(ofSize: 14.0)
        } else {
            self.titleLabel.font = Fonts.regular(ofSize: 16.0)
        }
        
        self.titleLabel.numberOfLines = 1
        self.titleLabel.textAlignment = .center
        self.titleLabel.lineBreakMode = .byTruncatingTail
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false

        self.addSubview(self.titleLabel)
    }

    // MARK: - UIView

    override func layoutSubviews() {
        super.layoutSubviews()

        let contentLeft = self.bounds.width * 24.0 / 375.0
        let contentRight = self.bounds.width - contentLeft
        let contentWidth = contentRight - contentLeft
        
        let contentTop = self.bounds.width * 20.0 / 375.0
        let contentBottom = self.bounds.height - contentTop
        
        let titleLabelHeight = self.titleLabel.sizeThatFits(CGSize(width: contentWidth,
                                                                   height: self.bounds.height)).height
        
        let titleLabelBottom = min(self.bounds.height * 0.5 + 8.0 + titleLabelHeight, contentBottom)
        
        self.titleLabel.frame = CGRect(x: contentLeft,
                                       y: titleLabelBottom - titleLabelHeight,
                                       width: contentWidth,
                                       height: titleLabelHeight).adjusted
        
        let imageViewSize = self.imageView.image?.size ?? .zero
        let imageViewBottom = self.titleLabel.frame.top - 16.0
        
        self.imageView.frame = CGRect(x: (self.bounds.width - imageViewSize.width) * 0.5,
                                      y: max(imageViewBottom - imageViewSize.height, contentTop),
                                      size: imageViewSize).adjusted
    }
}
