//
//  InboxBoostTableViewCell.swift
//  Friendsta
//
//  Created by Elina Batyrova on 06.02.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class InboxBoostTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var senderLabel: UILabel!
    @IBOutlet fileprivate weak var timeLabel: UILabel!
    @IBOutlet fileprivate weak var emojiLabel: UILabel!
    
    @IBOutlet fileprivate weak var newMessageView: UIView!
    
    // MARK: -
    
    var emoji: String? {
        get {
            return self.emojiLabel.text
        }
        
        set {
            self.emojiLabel.text = newValue
        }
    }

    var senderTitle: String? {
        get {
            return self.senderLabel.text
        }
        
        set {
            self.senderLabel.text = newValue
        }
    }
    
    var senderLabelFont: UIFont {
        get {
            return self.senderLabel.font
        }
        
        set {
            self.senderLabel.font = newValue
        }
    }
    
    var timeTitle: String? {
        get {
            return self.timeLabel.text
        }
        
        set {
            self.timeLabel.text = newValue
        }
    }
    
    var isBackgroundHighlighted: Bool {
        get {
            return !self.newMessageView.isHidden
        }
        
        set {
            self.newMessageView.isHidden = !newValue
        }
    }
    
    fileprivate func setupFont() {
        self.timeLabel.font = Fonts.medium(ofSize: 11.0)
    }
    
    // MARK: - Instance Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
