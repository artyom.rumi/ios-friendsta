//
//  LeftImageButton.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/20/18.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class LeftImageButton: UIControl {

    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let transitionOptions: UIView.AnimationOptions = [.transitionCrossDissolve,
                                                                 .allowAnimatedContent,
                                                                 .allowUserInteraction]

        static let horizontalSpaceBetweenImageAndTitle: CGFloat = 7.0
        static let imageWidth: CGFloat = 36.0
        static let imageHeight: CGFloat = 36.0
    }
    
    // MARK: - Instance Properties
    
    fileprivate var titleLabel = UILabel()
    
    fileprivate var imageView = RoundImageView(image: UIImage(named: "ProfileImagePlaceholder"))
    
    // MARK: -
    
    @IBInspectable var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    @IBInspectable var image: UIImage? {
        get {
            return self.imageView.image
        }
        
        set {
            self.imageView.image = newValue
        }
    }
    
    @IBInspectable var fontSize: CGFloat {
        get {
            return self.titleLabel.font.pointSize
        }
        
        set {
            self.titleLabel.font = Fonts.medium(ofSize: newValue)
        }
    }
    
    @IBInspectable var titleColor: UIColor {
        get {
            return self.titleLabel.textColor
        }
        
        set {
            self.titleLabel.textColor = newValue
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: Constants.imageWidth + Constants.horizontalSpaceBetweenImageAndTitle + self.titleLabel.frame.width, height: Constants.imageHeight)
    }
    
    // MARK: - UIControl
    
    override var isHighlighted: Bool {
        didSet {
            if self.isHighlighted {
                UIView.transition(with: self, duration: 0.05, options: Constants.transitionOptions, animations: {
                    self.titleLabel.textColor = Colors.whiteText.withAlphaComponent(0.75)
                    self.imageView.isHighlighted = true
                })
            } else {
                UIView.transition(with: self, duration: 0.25, options: Constants.transitionOptions, animations: {
                    self.titleLabel.textColor = Colors.whiteText
                    self.imageView.isHighlighted = false
                })
            }
        }
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.titleLabel.font = Fonts.medium(ofSize: 17.0)
        self.titleLabel.textColor = Colors.whiteText
        self.titleLabel.numberOfLines = 1
        self.titleLabel.textAlignment = .center
        self.titleLabel.isUserInteractionEnabled = false
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        self.imageView.contentMode = .scaleAspectFill
        
        self.addSubview(self.titleLabel)
        self.addSubview(self.imageView)
        
        NSLayoutConstraint.activate([self.imageView.widthAnchor.constraint(equalToConstant: Constants.imageWidth),
                                     self.imageView.heightAnchor.constraint(equalToConstant: Constants.imageHeight),
                                     self.imageView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
                                     self.imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor)])
        
        NSLayoutConstraint.activate([self.titleLabel.centerYAnchor.constraint(equalTo: self.imageView.centerYAnchor),
                                     self.titleLabel.leftAnchor.constraint(equalTo: self.imageView.rightAnchor, constant: Constants.horizontalSpaceBetweenImageAndTitle),
                                     self.titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: self.trailingAnchor)])
    }
}
