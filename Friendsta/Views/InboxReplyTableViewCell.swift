//
//  InboxReplyTableViewCell.swift
//  Friendsta
//
//  Created by Дамир Зарипов on 23/05/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class InboxReplyTableViewCell: UITableViewCell {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var timeLabel: UILabel!
    @IBOutlet fileprivate weak var avatarImageView: RoundImageView!
    @IBOutlet fileprivate weak var senderLabel: UILabel!
    
    @IBOutlet fileprivate weak var newMessageView: UIView!
    
    // MARK: -
    
    var avatarImage: UIImage? {
        get {
            return self.avatarImageView.image
        }
        
        set {
            self.avatarImageView.image = newValue
        }
    }
    
    var isBackgroundHighlighted: Bool {
        get {
            return !self.newMessageView.isHidden
        }
        
        set {
            self.newMessageView.isHidden = !newValue
        }
    }
    
    var timeTitle: String? {
        get {
            return self.timeLabel.text
        }
        
        set {
            self.timeLabel.text = newValue
        }
    }
    
    var senderLabelFont: UIFont {
        get {
            return self.senderLabel.font
        }
        
        set {
            self.senderLabel.font = newValue
        }
    }
    
    var senderTitle: String? {
        get {
            return self.senderLabel.text
        }
        
        set {
            self.senderLabel.text = newValue
        }
    }
    
    var avatarImageTarget: UIImageView {
        return self.avatarImageView
    }
    
    fileprivate func setupFont() {
        self.timeLabel.font = Fonts.medium(ofSize: 11.0)
    }
    
    // MARK: - Instance Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.setupFont()
    }
}
