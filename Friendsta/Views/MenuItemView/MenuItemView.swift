//
//  MenuItemView.swift
//  Friendsta
//
//  Created by Elina Batyrova on 20.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class MenuItemView: NibLoadingView {
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    // MARK: -
    
    var iconImage: UIImage? {
        get {
            return self.iconImageView.image
        }
        
        set {
            self.iconImageView.image = newValue
        }
    }
    
    var title: String? {
        get {
            return self.titleLabel.text
        }
        
        set {
            self.titleLabel.text = newValue
        }
    }
    
    var onViewTapped: (() -> Void)?
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.configureIconsShadow()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.configureIconsShadow()
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onContentViewTouchUpInside(_ sender: Any) {
        Log.high("onContentViewTouchUpInside()", from: self)
        
        self.onViewTapped?()
    }
    
    // MARK: -
    
    private func configureIconsShadow() {
        super.awakeFromNib()
        
        self.iconImageView.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
        self.titleLabel.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
    }
}
