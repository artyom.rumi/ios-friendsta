//
//  CommentTableViewCell.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 16/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class CommentTableViewCell: UITableViewCell {

    // MARK: - Instance Properties

    @IBOutlet private weak var commentLabel: UILabel!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var chatButton: UIButton!
    @IBOutlet private weak var moreOptionsButton: UIButton!
    
    // MARK: -

    var comment: String? {
        get {
            return self.commentLabel.text
        }

        set {
            self.commentLabel.text = newValue
        }
    }

    var time: String? {
        get {
            return self.timeLabel.text
        }

        set {
            self.timeLabel.text = newValue
        }
    }

    var commentFont: UIFont {
        get {
            return self.commentLabel.font
        }

        set {
            self.commentLabel.font = newValue
        }
    }
    
    var isChatButtonHidden: Bool {
        get {
            return self.chatButton.isHidden
        }
        
        set {
            self.chatButton.isHidden = newValue
        }
    }
    
    var isMoreOptionsButtonHidden: Bool {
        get {
            return self.moreOptionsButton.isHidden
        }
        
        set {
            self.moreOptionsButton.isHidden = newValue
        }
    }

    // MARK: -

    var onChatButtonClicked: (() -> Void)?
    var onMoreOptionsButtonClicked: (() -> Void)?

    // MARK: - Instance Methods

    @IBAction private func onChatButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onMoreOptionsButtonTouchUpInside()", from: self)

        self.onChatButtonClicked?()
    }
    
    @IBAction private func onMoreOptionsButtonTouchUpInside(_ sender: Any) {
        Log.high("onMoreOptionsButtonTouchUpInside()", from: self)
        
        self.onMoreOptionsButtonClicked?()
    }
}
