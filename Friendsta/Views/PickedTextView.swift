//
//  PickedTextView.swift
//  Friendsta
//
//  Created by Marat Galeev on 25.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class PickedTextView: UITextView {
    
    // MARK: - Instance Properties
    
    fileprivate weak var inputPickerView: UIPickerView?
    fileprivate weak var inputToolbar: UIToolbar?
    
    // MARK: -
    
    var hasInputToolbar: Bool {
        get {
            return (self.inputToolbar != nil)
        }
        
        set {
            if newValue {
                if self.inputToolbar == nil {
                    self.createInputToolbar()
                }
            } else {
                self.inputAccessoryView = nil
            }
        }
    }
    
    var pickerValues: [String] = [] {
        didSet {
            self.inputPickerView?.reloadAllComponents()
        }
    }
    
    var onPickerValueChanged: ((_ value: String) -> Void)?
    
    var onInputDoneButtonClicked: (() -> Void)?
    var onInputCancelButtonClicked: (() -> Void)?
    
    // MARK: - Initializers
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    @objc fileprivate func onInputDoneButtonTouchUpInside(_ sender: UIBarButtonItem) {
        self.onInputDoneButtonClicked?()
    }
    
    @objc fileprivate func onInputCancelButtonTouchUpInside(_ sender: UIBarButtonItem) {
        self.onInputCancelButtonClicked?()
    }
    
    // MARK: -
    
    fileprivate func initialize() {
        let pickerView = UIPickerView()
        
        pickerView.dataSource = self
        pickerView.delegate = self
        
        pickerView.backgroundColor = Colors.whiteBackground
        pickerView.showsSelectionIndicator = true
        
        self.inputView = pickerView
        self.inputPickerView = pickerView
    }
    
    fileprivate func createInputToolbar() {
        let toolbar = UIToolbar()
        
        toolbar.barStyle = .default
        toolbar.isTranslucent = true
        toolbar.barTintColor = Colors.whiteBackground
        toolbar.tintColor = Colors.primary
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done",
                                         style: .done,
                                         target: self,
                                         action: #selector(self.onInputDoneButtonTouchUpInside(_:)))
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                          target: nil,
                                          action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel",
                                           style: .done,
                                           target: self,
                                           action: #selector(self.onInputCancelButtonTouchUpInside(_:)))
        
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        self.inputAccessoryView = toolbar
        self.inputToolbar = toolbar
    }
}

// MARK: - UIPickerViewDataSource

extension PickedTextView: UIPickerViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerValues.count
    }
}

// MARK: - UIPickerViewDelegate

extension PickedTextView: UIPickerViewDelegate {
    
    // MARK: - Instance Methods
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerValues[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.onPickerValueChanged?(self.pickerValues[row])
    }
}
