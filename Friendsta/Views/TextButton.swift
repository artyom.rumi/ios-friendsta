//
//  TextButton.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 29.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable final class TextButton: UIButton { }
