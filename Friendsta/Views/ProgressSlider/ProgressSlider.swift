//
//  ProgressSlider.swift
//  Friendsta
//
//  Created by Elina Batyrova on 01.04.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import UIKit

class ProgressSlider: UIView {
   
    // MARK: - Instance Properties
    
    @IBInspectable var numberOfSegments: Int = 0 {
        didSet {
            self.apply()
        }
    }
    
    fileprivate var duration: TimeInterval = 5.0
    
    fileprivate var segments = [Segment]()
    
    fileprivate var hasDoneLayout = false
    
    fileprivate var currentAnimationIndex = 0
    
    // MARK: -
    
    fileprivate var topColor = UIColor.white {
        didSet {
            self.updateColors()
        }
    }
    
    fileprivate var bottomColor = UIColor.white.withAlphaComponent(0.25) {
        didSet {
            self.updateColors()
        }
    }
    
    fileprivate var padding: CGFloat = 2.0
    
    fileprivate var isPaused: Bool = false {
        didSet {
            if self.isPaused {
                for segment in self.segments {
                    let layer = segment.topSegmentView.layer
                    let pausedTime = layer.convertTime(CACurrentMediaTime(), from: nil)
                    
                    layer.speed = 0.0
                    layer.timeOffset = pausedTime
                }
            } else {
                let segment = self.segments[self.currentAnimationIndex]
                let layer = segment.topSegmentView.layer
                let pausedTime = layer.timeOffset
                
                layer.speed = 1.0
                layer.timeOffset = 0.0
                layer.beginTime = 0.0
                
                let timeSincePause = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
                layer.beginTime = timeSincePause
            }
        }
    }
    
    // MARK: -
    
    var progressSliderChangedIndex: ((_ : Int) -> Void)?
    var progressSliderFinished: (() -> Void)?
    
    // MARK: - Initializers
    
    init(numberOfSegments: Int, duration: TimeInterval = 5.0) {
        self.duration = duration
        
        self.numberOfSegments = numberOfSegments
        
        super.init(frame: CGRect.zero)
        
        self.apply()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - UIView
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if self.hasDoneLayout {
            return
        } else {
            let width = (self.frame.width - (self.padding * CGFloat(self.segments.count - 1))) / CGFloat(self.segments.count)
            
            for (index, segment) in self.segments.enumerated() {
                let segmentFrame = CGRect(x: CGFloat(index) * (width + self.padding), y: 0, width: width, height: self.frame.height)
                
                segment.bottomSegmentView.frame = segmentFrame
                segment.topSegmentView.frame = segmentFrame
                segment.topSegmentView.frame.size.width = 0
                
                let cornerRadius = self.frame.height / 2
                
                segment.bottomSegmentView.layer.cornerRadius = cornerRadius
                segment.topSegmentView.layer.cornerRadius = cornerRadius
            }
            
            self.hasDoneLayout = true
            
            self.animate()
        }
    }
    
    // MARK: - Instance Methods
    
    fileprivate func apply() {
        for _ in 0..<self.numberOfSegments {
            let segment = Segment()
            
            self.addSubview(segment.topSegmentView)
            self.addSubview(segment.bottomSegmentView)
            
            self.segments.append(segment)
        }
        
        self.updateColors()
    }
    
    fileprivate func updateColors() {
        for segment in self.segments {
            segment.topSegmentView.backgroundColor = self.topColor
            segment.bottomSegmentView.backgroundColor = self.bottomColor
        }
    }
    
    // MARK: - Animations
    
    fileprivate func next() {
        let nextIndex = self.currentAnimationIndex + 1
        
        if nextIndex < self.segments.count {
            self.progressSliderChangedIndex?(nextIndex)
            
            self.animate(animationIndex: nextIndex)
        } else {
            self.progressSliderFinished?()
        }
    }
    
    fileprivate func animate(animationIndex: Int = 0) {
        let nextSegment = self.segments[animationIndex]
        
        self.currentAnimationIndex = animationIndex
        self.isPaused = false
        
        UIView.animate(withDuration: self.duration, delay: 0.0, options: .curveLinear, animations: {
            nextSegment.topSegmentView.frame.size.width = nextSegment.bottomSegmentView.frame.width
        }) { (finished) in
            if !finished {
                return
            }
            
            self.next()
        }
    }
    
    // MARK: -
    
    func startAnimation() {
        self.animate()
    }
    
    func skip() {
        let currentSegment = self.segments[self.currentAnimationIndex]
        currentSegment.topSegmentView.frame.size.width = currentSegment.bottomSegmentView.frame.width
        currentSegment.topSegmentView.layer.removeAllAnimations()
        
        self.next()
    }
    
    func rewind() {
        let currentSegment = self.segments[self.currentAnimationIndex]
        currentSegment.topSegmentView.layer.removeAllAnimations()
        currentSegment.topSegmentView.frame.size.width = 0
        
        let previousIndex = max(self.currentAnimationIndex - 1, 0)
        
        let previousSegment = self.segments[previousIndex]
        previousSegment.topSegmentView.frame.size.width = 0
        
        self.progressSliderChangedIndex?(previousIndex)
        self.animate(animationIndex: previousIndex)
    }
    
    func stop() {
        self.isPaused = true
    }
    
    func restartAnimation() {
        self.isPaused = false
    }
}

// MARK: - Segment

private class Segment {
    
    // MARK: - Instance Properties
    
    fileprivate var topSegmentView = UIView()
    fileprivate var bottomSegmentView = UIView()
    
    // MARK: - Initializers
    
    init() {}
}
