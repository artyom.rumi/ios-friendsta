//
//  CameraFlashButton.swift
//  Friendsta
//
//  Created by Elina Batyrova on 11.10.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class CameraFlashButton: UIButton {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
    
        static let onModeIcon = UIImage(named: "CameraFlashOnMode")
        static let offModeIcon = UIImage(named: "CameraFlashOffMode")
    }
    
    // MARK: -
    
    enum Mode {
    
        // MARK: - Enumeration Cases
        
        case on
        case off
    }
    
    // MARK: - Instance Properties
    
    var mode: Mode = .off {
        didSet {
            self.applyMode()
        }
    }
    
    // MARK: - Initializers
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        self.applyMode()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.applyMode()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func applyMode() {
        switch self.mode {
        case .on:
            self.setImage(Constants.onModeIcon, for: .normal)
            
        case .off:
            self.setImage(Constants.offModeIcon, for: .normal)
        }
    }
}
