//
//  UIView+Extensions.swift
//  Friendsta
//
//  Created by Lance Samaria on 3/19/20.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

///// ********* Lance's Code ********* /////
extension UIView {

   /// Flip view horizontally.
   func flipX() {
       transform = CGAffineTransform(scaleX: -transform.a, y: transform.d)
   }

   /// Flip view vertically.
   func flipY() {
       transform = CGAffineTransform(scaleX: transform.a, y: -transform.d)
   }
}
///// ********* Lance's Code ********* /////
