//
//  UIDevice+Extensions.swift
//  Friendsta
//
//  Created by Lance Samaria on 3/19/20.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

///// ********* Lance's Code ********* /////
extension UIDevice {
    
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
    
    var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
}
///// ********* Lance's Code ********* /////
