//
//  SchoolGradeLevel.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 15/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum SchoolGradeLevel: Int {
    
    // MARK: - Enumeration cases
    
    case middle
    case high
}
