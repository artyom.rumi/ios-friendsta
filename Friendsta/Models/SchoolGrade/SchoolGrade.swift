//
//  SchoolGrade.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 14/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

struct SchoolGrade {
    
    // MARK: - Instance Properties
    
    let level: SchoolGradeLevel
    let number: Int
    let classYear: Int
}
