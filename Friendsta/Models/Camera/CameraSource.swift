//
//  CameraSource.swift
//  Friendsta
//
//  Created by Дамир Зарипов on 21/02/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

enum PhotoCreationSource {
    
    // MARK: - Enumeration Cases
    
    case chat
    case feed
    case menu
    case reply
    case profile
}
