//
//  ProfileSettingScope.swift
//  Friendsta
//
//  Created by Дамир Зарипов on 28/03/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

enum ProfileSettingSource {
    
    // MARK: - Enumeration cases
    
    case sumbitPoke
    case getHelp
    case giveFeedack
}
