//
//  ConnectionList.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 31/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ConnectionList: AnyObject {

    // MARK: - Instance Properties

    var hasNextPage: Bool { get }
    var itemCount: Int { get }
    var moreCount: Int { get }
}
