//
//  Gender.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 07.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum Gender: Int16 {
    
    // MARK: - Enumeration Cases
    
    case nonBinary
    
    case male
    case female
}
