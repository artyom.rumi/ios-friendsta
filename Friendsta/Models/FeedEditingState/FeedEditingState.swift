//
//  FeedEditingState.swift
//  Friendsta
//
//  Created by Elina Batyrova on 09.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import Foundation

enum FeedEditingState {
    
    // MARK: - Enumeration Cases
    
    case repostToAnonymous
    case repostToTimeline
}
