//
//  TutorialStage.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 14.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum TutorialStage {
    
    // MARK: - Enumeration Cases
    
    case first
    case second
    case third
    
    // MARK: - Instance Properties
    
    var steps: [TutorialStep] {
        switch self {
        case .first:
            return [.first, .second, .third]
            
        case .second:
            return [.fourth]
            
        case .third:
            return [.fifth]
        }
    }
}
