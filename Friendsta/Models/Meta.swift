//
//  Meta.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 30/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

struct Meta {

    // MARK: - Instance Properties

    let count: Int64
}
