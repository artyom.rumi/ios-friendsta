//
//  WelcomeStep.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 14.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum WelcomeStep {
    
    // MARK: - Enumeration Cases
    
    case first
    case second
    case third
    case forth
}
