//
//  WelcomeStage.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 14.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum WelcomeStage {
    
    // MARK: - Enumeration Cases
    
    case main
    
    // MARK: - Instance Properties
    
    var steps: [WelcomeStep] {
        return [.first, .second, .third, .forth]
    }
}
