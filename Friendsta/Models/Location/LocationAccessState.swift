//
//  LocationAccessState.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 16.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum LocationAccessState {
    
    // MARK: - Enumeration Cases

    case notAvailable
    case notDetermined
    case authorized
    case restricted
    case denied
}
