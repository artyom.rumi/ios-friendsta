//
//  Location.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 16.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

struct Location {
    
    // MARK: - Type Properties
    
    static let northPole = Location(latitude: 90.0, longitude: 0.0)
    static let southPole = Location(latitude: -90.0, longitude: 0.0)
    
    // MARK: - Instance Properties
    
    let latitude: Double
    let longitude: Double
}
