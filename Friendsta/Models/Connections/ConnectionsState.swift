//
//  ConnectionsState.swift
//  Friendsta
//
//  Created by Elina Batyrova on 16.03.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import Foundation

enum ConnectionsState {
    
    // MARK: - Enumeration Cases
    
    case adding
    case keeping
}
