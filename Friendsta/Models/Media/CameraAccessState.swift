//
//  CameraAccessState.swift
//  Friendsta
//
//  Created by Elina Batyrova on 23.10.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum CameraAccessState {
    
    // MARK: - Enumeration Cases
    
    case notDetermined
    case authorized
    case restricted
    case denied
}
