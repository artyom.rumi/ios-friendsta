//
//  PeopleModelList.swift
//  Friendsta
//
//  Created by Elina Batyrova on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

class PeopleModelList {
    
    // MARK: - Instance Properties
    
    let items: [PeopleModelItem]
    let perPageCount: Int
    
    var page = 1
    
    // MARK: -
    
    var moreCount: Int {
        let moreCount = self.items.count - self.perPageCount * self.page
        
        return min(moreCount, self.perPageCount)
    }
    
    var currentPageCount: Int {
        return self.perPageCount * self.page
    }
    
    var hasNextPage: Bool {
        return Double(self.items.count) / Double(self.perPageCount * self.page) > 1.0
    }
    
    // MARK: - Initializers
    
    init(items: [PeopleModelItem], perPageCount: Int = 5) {
        self.items = items
        self.perPageCount = perPageCount
    }
}
