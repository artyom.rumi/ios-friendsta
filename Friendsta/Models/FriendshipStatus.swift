//
//  FriendshipStatus.swift
//  Friendsta
//
//  Created by Nikita on 24/06/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

enum FriendshipStatusType: String {

    // MARK: - Instance Properties
    
    case uninvited
    case invited
    case friend
    case affirmed
}
