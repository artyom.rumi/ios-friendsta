//
//  UserRole.swift
//  Friendsta
//
//  Created by Marat Galeev on 19.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum UserRole: Int16 {
    
    // MARK: - Enumeration cases
    
    case ordinary
    case test
    case bot
}
