//
//  Photo.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

struct Photo {
    
    // MARK: - Instance Properties
    
    let imageURL: URL?
    
    let smallImageURL: URL?
    let mediumImageURL: URL?
    let largeImageURL: URL?
}
