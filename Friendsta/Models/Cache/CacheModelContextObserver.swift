//
//  CacheModelContextObserver.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 11.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol CacheModelContextObserver: ModelContextObserver {
    
    // MARK: - Nested Types
    
    associatedtype Context: ModelContext
    associatedtype Object: ModelObject
    
    // MARK: - Instance Properties
    
    var context: Context { get }
    
    var objectsRemovedEvent: Event<[Object]> { get }
    var objectsAppendedEvent: Event<[Object]> { get }
    var objectsUpdatedEvent: Event<[Object]> { get }
    var objectsChangedEvent: Event<[Object]> { get }
    
    // MARK: - Instance Methods
    
    func startObserving()
    func stopObserving()
}

// MARK: -

extension CacheModelContextObserver {
    
    // MARK: - Instance Methods
    
    func filter(objects: [ModelObject]) -> [Object] {
        return objects.compactMap({ object in
            return object as? Object
        })
    }
    
    // MARK: -
    
    func startObserving() {
        self.context.addObserver(self)
    }
    
    func stopObserving() {
        self.context.removeObserver(self)
    }
    
    // MARK: - ModelContextObserver
    
    func modelContext(_ modelContext: ModelContext, didRemoveObjects objects: [ModelObject]) {
        let objects = self.filter(objects: objects)
        
        if !objects.isEmpty {
            self.objectsRemovedEvent.emit(data: objects)
        }
    }
    
    func modelContext(_ modelContext: ModelContext, didAppendObjects objects: [ModelObject]) {
        let objects = self.filter(objects: objects)
        
        if !objects.isEmpty {
            self.objectsAppendedEvent.emit(data: objects)
        }
    }
    
    func modelContext(_ modelContext: ModelContext, didUpdateObjects objects: [ModelObject]) {
        let objects = self.filter(objects: objects)
        
        if !objects.isEmpty {
            self.objectsUpdatedEvent.emit(data: objects)
        }
    }
    
    func modelContext(_ modelContext: ModelContext, didChangeObjects objects: [ModelObject]) {
        let objects = self.filter(objects: objects)
        
        if !objects.isEmpty {
            self.objectsChangedEvent.emit(data: objects)
        }
    }
}
