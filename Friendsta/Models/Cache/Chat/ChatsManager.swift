//
//  ChatsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 14.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ChatsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[Chat]> { get }
    var objectsAppendedEvent: Event<[Chat]> { get }
    var objectsUpdatedEvent: Event<[Chat]> { get }
    var objectsChangedEvent: Event<[Chat]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> Chat?
    func first() -> Chat?
    
    func last(with uid: Int64) -> Chat?
    func last() -> Chat?
    
    func fetch(with uid: Int64) -> [Chat]
    func fetch() -> [Chat]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: Chat)
    func append() -> Chat
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}

// MARK: -

extension ChatsManager {
    
    // MARK: - Instance Methods
    
    func firstOrNew(withUID uid: Int64) -> Chat {
        return self.first(with: uid) ?? self.append()
    }
}
