//
//  DefaultChatsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 14.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultChatsManager: CoreDataModelManager, ChatsManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = Chat
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[Chat]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[Chat]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[Chat]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[Chat]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with uid: Int64) -> NSPredicate {
        return NSPredicate(format: "uid == %d", uid)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "modifiedDate", ascending: true)
    }
    
    func first(with uid: Int64) -> Chat? {
        return self.first(with: self.createPredicate(with: uid))
    }
    
    func last(with uid: Int64) -> Chat? {
        return self.last(with: self.createPredicate(with: uid))
    }
    
    func fetch(with uid: Int64) -> [Chat] {
        return self.fetch(with: self.createPredicate(with: uid))
    }
    
    func count(with uid: Int64) -> Int {
        return self.count(with: self.createPredicate(with: uid))
    }
    
    func clear(with uid: Int64) {
        self.clear(with: self.createPredicate(with: uid))
    }
}
