//
//  ContactsAccessState.swift
//  Friendsta
//
//  Created by Marat Galeev on 22.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum ContactsAccessState {
    
    // MARK: - Enumeration Cases
    
    case notDetermined
    case authorized
    case restricted
    case denied
}
