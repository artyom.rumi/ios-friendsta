//
//  DefaultContactsManager.swift
//  Friendsta
//
//  Created by Marat Galeev on 22.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

class DefaultContactsManager: CoreDataModelManager, ContactsManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = Contact
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[Contact]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[Contact]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[Contact]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[Contact]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with phoneNumber: String) -> NSPredicate {
        return NSPredicate(format: "phoneNumber == %@", phoneNumber)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "displayName", ascending: true)
    }
    
    func first(with phoneNumber: String) -> Contact? {
        return self.first(with: self.createPredicate(with: phoneNumber))
    }
    
    func last(with phoneNumber: String) -> Contact? {
        return self.last(with: self.createPredicate(with: phoneNumber))
    }
    
    func fetch(with phoneNumber: String) -> [Contact] {
        return self.fetch(with: self.createPredicate(with: phoneNumber))
    }
    
    func count(with phoneNumber: String) -> Int {
        return self.count(with: self.createPredicate(with: phoneNumber))
    }
    
    func clear(with phoneNumber: String) {
        self.clear(with: self.createPredicate(with: phoneNumber))
    }
}
