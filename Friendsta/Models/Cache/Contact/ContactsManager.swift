//
//  ContactsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ContactsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[Contact]> { get }
    var objectsAppendedEvent: Event<[Contact]> { get }
    var objectsUpdatedEvent: Event<[Contact]> { get }
    var objectsChangedEvent: Event<[Contact]> { get }
    
    // MARK: - Instance Methods
    
    func first(with phoneNumber: String) -> Contact?
    func first() -> Contact?
    
    func last(with phoneNumber: String) -> Contact?
    func last() -> Contact?
    
    func fetch(with phoneNumber: String) -> [Contact]
    func fetch() -> [Contact]
    
    func count(with phoneNumber: String) -> Int
    func count() -> Int
    
    func clear(with phoneNumber: String)
    func clear()
    
    func remove(object: Contact)
    func append() -> Contact
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
