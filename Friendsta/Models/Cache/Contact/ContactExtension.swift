//
//  ContactExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension Contact {
    
    // MARK: - Instance Properties
    
    var avatarURL: URL? {
        get {
            if let avatarRawURL = self.avatarRawURL {
                return URL(string: avatarRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.avatarRawURL = newValue?.absoluteString
        }
    }
}
