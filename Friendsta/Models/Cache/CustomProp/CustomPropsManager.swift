//
//  CustomPropsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 12/10/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol CustomPropsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[CustomProp]> { get }
    var objectsAppendedEvent: Event<[CustomProp]> { get }
    var objectsUpdatedEvent: Event<[CustomProp]> { get }
    var objectsChangedEvent: Event<[CustomProp]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> CustomProp?
    func first() -> CustomProp?
    
    func last(with uid: Int64) -> CustomProp?
    func last() -> CustomProp?
    
    func fetch(with uid: Int64) -> [CustomProp]
    func fetch() -> [CustomProp]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: CustomProp)
    func append() -> CustomProp
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
