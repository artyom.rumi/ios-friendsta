//
//  ChatMasksManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 14.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ChatMasksManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[ChatMask]> { get }
    var objectsAppendedEvent: Event<[ChatMask]> { get }
    var objectsUpdatedEvent: Event<[ChatMask]> { get }
    var objectsChangedEvent: Event<[ChatMask]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> ChatMask?
    func first(withUserUID userUID: Int64) -> ChatMask?
    func first() -> ChatMask?
    
    func last(with uid: Int64) -> ChatMask?
    func last(withUserUID userUID: Int64) -> ChatMask?
    func last() -> ChatMask?
    
    func fetch(with uid: Int64) -> [ChatMask]
    func fetch(withUserUID userUID: Int64) -> [ChatMask]
    func fetch() -> [ChatMask]
    
    func count(with uid: Int64) -> Int
    func count(withUserUID userUID: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear(withUserUID userUID: Int64)
    func clear()
    
    func remove(object: ChatMask)
    func append() -> ChatMask
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
