//
//  DefaultChatMasksManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 14.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultChatMasksManager: CoreDataModelManager, ChatMasksManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = ChatMask
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[ChatMask]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[ChatMask]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[ChatMask]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[ChatMask]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with uid: Int64) -> NSPredicate {
        return NSPredicate(format: "uid == %d", uid)
    }
    
    fileprivate func createPredicate(withUserUID userUID: Int64) -> NSPredicate {
        return NSPredicate(format: "userUID == %d", userUID)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "uid", ascending: true)
    }
    
    func first(with uid: Int64) -> ChatMask? {
        return self.first(with: self.createPredicate(with: uid))
    }
    
    func first(withUserUID userUID: Int64) -> ChatMask? {
        return self.first(with: self.createPredicate(withUserUID: userUID))
    }
    
    func last(with uid: Int64) -> ChatMask? {
        return self.last(with: self.createPredicate(with: uid))
    }
    
    func last(withUserUID userUID: Int64) -> ChatMask? {
        return self.last(with: self.createPredicate(withUserUID: userUID))
    }
    
    func fetch(with uid: Int64) -> [ChatMask] {
        return self.fetch(with: self.createPredicate(with: uid))
    }
    
    func fetch(withUserUID userUID: Int64) -> [ChatMask] {
        return self.fetch(with: self.createPredicate(withUserUID: userUID))
    }
    
    func count(with uid: Int64) -> Int {
        return self.count(with: self.createPredicate(with: uid))
    }
    
    func count(withUserUID userUID: Int64) -> Int {
        return self.count(with: self.createPredicate(withUserUID: userUID))
    }
    
    func clear(with uid: Int64) {
        self.clear(with: self.createPredicate(with: uid))
    }
    
    func clear(withUserUID userUID: Int64) {
        self.clear(with: self.createPredicate(withUserUID: userUID))
    }
}
