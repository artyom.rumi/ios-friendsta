//
//  ChatMaskExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 14.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension ChatMask {
    
    // MARK: - Instance Properties
    
    var avatarURL: URL? {
        get {
            if let avatarRawURL = self.avatarRawURL {
                return URL(string: avatarRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.avatarRawURL = newValue?.absoluteString
        }
    }
    
    var smallAvatarURL: URL? {
        get {
            if let avatarRawURL = self.smallAvatarRawURL {
                return URL(string: avatarRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.smallAvatarRawURL = newValue?.absoluteString
        }
    }
    
    var mediumAvatarURL: URL? {
        get {
            if let avatarRawURL = self.mediumAvatarRawURL {
                return URL(string: avatarRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.mediumAvatarRawURL = newValue?.absoluteString
        }
    }
    
    var largeAvatarURL: URL? {
        get {
            if let avatarRawURL = self.largeAvatarRawURL {
                return URL(string: avatarRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.largeAvatarRawURL = newValue?.absoluteString
        }
    }
    
    var avatar: Photo? {
        get {
            return Photo(imageURL: self.avatarURL,
                         smallImageURL: self.smallAvatarURL,
                         mediumImageURL: self.mediumAvatarURL,
                         largeImageURL: self.largeAvatarURL)
        }
        
        set {
            self.avatarURL = newValue?.imageURL
            
            self.smallAvatarURL = newValue?.smallImageURL
            self.mediumAvatarURL = newValue?.mediumImageURL
            self.largeAvatarURL = newValue?.largeImageURL
        }
    }
}
