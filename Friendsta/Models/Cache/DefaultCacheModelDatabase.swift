//
//  DefaultCacheModelDatabase.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 08.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

class DefaultCacheModelDatabase: CoreDataModelDatabase<DefaultCacheModelContext>, CacheModelDatabase { }
