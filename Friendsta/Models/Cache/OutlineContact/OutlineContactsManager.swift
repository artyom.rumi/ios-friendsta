//
//  OutlineContactsManager.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol OutlineContactsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[OutlineContact]> { get }
    var objectsAppendedEvent: Event<[OutlineContact]> { get }
    var objectsUpdatedEvent: Event<[OutlineContact]> { get }
    var objectsChangedEvent: Event<[OutlineContact]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> OutlineContact?
    func first() -> OutlineContact?
    
    func last(with uid: Int64) -> OutlineContact?
    func last() -> OutlineContact?
    
    func fetch(with uid: Int64) -> [OutlineContact]
    func fetch() -> [OutlineContact]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: OutlineContact)
    func append() -> OutlineContact
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
