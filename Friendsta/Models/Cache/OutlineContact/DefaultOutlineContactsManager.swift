//
//  DefaultOutlineContactsManager.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultOutlineContactsManager: CoreDataModelManager, OutlineContactsManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = OutlineContact
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[OutlineContact]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[OutlineContact]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[OutlineContact]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[OutlineContact]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with uid: Int64) -> NSPredicate {
        return NSPredicate(format: "uid == %d", uid)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "uid", ascending: true)
    }
    
    func first(with uid: Int64) -> OutlineContact? {
        return self.first(with: self.createPredicate(with: uid))
    }
    
    func last(with uid: Int64) -> OutlineContact? {
        return self.last(with: self.createPredicate(with: uid))
    }
    
    func fetch(with uid: Int64) -> [OutlineContact] {
        return self.fetch(with: self.createPredicate(with: uid))
    }
    
    func count(with uid: Int64) -> Int {
        return self.count(with: self.createPredicate(with: uid))
    }
    
    func clear(with uid: Int64) {
        self.clear(with: self.createPredicate(with: uid))
    }
}
