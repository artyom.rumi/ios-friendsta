//
//  PropCardListManager.swift
//  Friendsta
//
//  Created by Elina Batyrova on 24.05.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol PropCardListManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[PropCardListItem]> { get }
    var objectsAppendedEvent: Event<[PropCardListItem]> { get }
    var objectsUpdatedEvent: Event<[PropCardListItem]> { get }
    var objectsChangedEvent: Event<[PropCardListItem]> { get }
    
    // MARK: - Instance Methods
    
    func first(with listType: PropCardListType) -> PropCardListItem?
    func first() -> PropCardListItem?
    
    func last(with listType: PropCardListType) -> PropCardListItem?
    func last() -> PropCardListItem?
    
    func fetch(with listType: PropCardListType) -> [PropCardListItem]
    func fetch() -> [PropCardListItem]
    
    func count(with listType: PropCardListType) -> Int
    func count() -> Int
    
    func clear(with listType: PropCardListType)
    func clear()
    
    func remove(object: PropCardListItem)
    func append() -> PropCardListItem
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
