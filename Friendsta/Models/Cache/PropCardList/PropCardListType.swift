//
//  PropCardListType.swift
//  Friendsta
//
//  Created by Elina Batyrova on 24.05.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

enum PropCardListType: Int16 {
    
    // MARK: - Enumeration Cases
    
    case prop
    case reaction
}
