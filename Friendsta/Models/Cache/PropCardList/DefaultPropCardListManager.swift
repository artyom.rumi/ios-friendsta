//
//  DefaultPropCardListManager.swift
//  Friendsta
//
//  Created by Elina Batyrova on 24.05.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultPropCardListManager: CoreDataModelManager, PropCardListManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = PropCardListItem
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[PropCardListItem]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[PropCardListItem]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[PropCardListItem]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[PropCardListItem]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with listType: PropCardListType) -> NSPredicate {
        return NSPredicate(format: "listRawType == %d", listType.rawValue)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "sorting", ascending: true)
    }
    
    func first(with listType: PropCardListType) -> PropCardListItem? {
        return self.first(with: self.createPredicate(with: listType))
    }
    
    func last(with listType: PropCardListType) -> PropCardListItem? {
        return self.last(with: self.createPredicate(with: listType))
    }
    
    func fetch(with listType: PropCardListType) -> [PropCardListItem] {
        return self.fetch(with: self.createPredicate(with: listType))
    }
    
    func count(with listType: PropCardListType) -> Int {
        return self.count(with: self.createPredicate(with: listType))
    }
    
    func clear(with listType: PropCardListType) {
        self.clear(with: self.createPredicate(with: listType))
    }
}
