//
//  PropCardListItemExtension.swift
//  Friendsta
//
//  Created by Elina Batyrova on 24.05.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension PropCardListItem {
    
    // MARK: - Instance Properties
    
    var listType: PropCardListType? {
        get {
            return PropCardListType(rawValue: self.listRawType)
        }
        
        set {
            if let newValue = newValue {
                self.listRawType = newValue.rawValue
            } else {
                self.listRawType = -1
            }
        }
    }
}
