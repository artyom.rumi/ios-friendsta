//
//  PropListsManager.swift
//  Friendsta
//
//  Created by Marat Galeev on 29.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol PropListsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[PropListItem]> { get }
    var objectsAppendedEvent: Event<[PropListItem]> { get }
    var objectsUpdatedEvent: Event<[PropListItem]> { get }
    var objectsChangedEvent: Event<[PropListItem]> { get }
    
    // MARK: - Instance Methods
    
    func first(with listType: PropListType) -> PropListItem?
    func first() -> PropListItem?
    
    func last(with listType: PropListType) -> PropListItem?
    func last() -> PropListItem?
    
    func fetch(with listType: PropListType) -> [PropListItem]
    func fetch() -> [PropListItem]
    
    func count(with listType: PropListType) -> Int
    func count() -> Int
    
    func clear(with listType: PropListType)
    func clear()
    
    func remove(object: PropListItem)
    func append() -> PropListItem
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
