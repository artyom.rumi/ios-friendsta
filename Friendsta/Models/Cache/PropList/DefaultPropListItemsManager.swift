//
//  DefaultPropListsManager.swift
//  Friendsta
//
//  Created by Marat Galeev on 29.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultPropListsManager: CoreDataModelManager, PropListsManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = PropListItem
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[PropListItem]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[PropListItem]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[PropListItem]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[PropListItem]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with listType: PropListType) -> NSPredicate {
        return NSPredicate(format: "listRawType == %d", listType.rawValue)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "sorting", ascending: true)
    }
    
    func first(with listType: PropListType) -> PropListItem? {
        return self.first(with: self.createPredicate(with: listType))
    }
    
    func last(with listType: PropListType) -> PropListItem? {
        return self.last(with: self.createPredicate(with: listType))
    }
    
    func fetch(with listType: PropListType) -> [PropListItem] {
        return self.fetch(with: self.createPredicate(with: listType))
    }
    
    func count(with listType: PropListType) -> Int {
        return self.count(with: self.createPredicate(with: listType))
    }
    
    func clear(with listType: PropListType) {
        self.clear(with: self.createPredicate(with: listType))
    }
}
