//
//  PropListType.swift
//  Friendsta
//
//  Created by Marat Galeev on 29.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum PropListType: Int16 {
    
    // MARK: - Enumeration Cases
    
    case special
    case popular
    case recent
    case all
}
