//
//  PropListItemExtension.swift
//  Friendsta
//
//  Created by Marat Galeev on 29.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension PropListItem {
    
    // MARK: - Instance Properties
    
    var listType: PropListType? {
        get {
            return PropListType(rawValue: self.listRawType)
        }
        
        set {
            if let newValue = newValue {
                self.listRawType = newValue.rawValue
            } else {
                self.listRawType = -1
            }
        }
    }
}
