//
//  UsersManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol UsersManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[User]> { get }
    var objectsAppendedEvent: Event<[User]> { get }
    var objectsUpdatedEvent: Event<[User]> { get }
    var objectsChangedEvent: Event<[User]> { get }
    
    // MARK: - Instance Methods
        
    func first(with uid: Int64) -> User?
    func first() -> User?
    
    func last(with uid: Int64) -> User?
    func last() -> User?
    
    func fetch(with uid: Int64) -> [User]
    func fetch() -> [User]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: User)
    func append() -> User

    func searchByFullName(with text: String) -> [User]
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}

// MARK: -

extension UsersManager {
    
    // MARK: - Instance Methods
    
    func firstOrNew(withUID uid: Int64) -> User {
        return self.first(with: uid) ?? self.append()
    }
}
