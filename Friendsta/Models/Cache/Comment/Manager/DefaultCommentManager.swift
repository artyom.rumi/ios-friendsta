//
//  DefaultCommentManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultCommentManager: CoreDataModelManager, CommentManager, CacheModelContextObserver {

    // MARK: - Nested Types

    typealias Object = Comment

    // MARK: - Instance Properties

    unowned let context: CoreDataModelContext

    fileprivate(set) lazy var objectsRemovedEvent = Event<[Comment]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[Comment]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[Comment]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[Comment]>()

    // MARK: - Initializers

    init(context: CoreDataModelContext) {
        self.context = context
    }

    // MARK: - Instance Methods

    fileprivate func createPredicate(with uid: Int64) -> NSPredicate {
        return NSPredicate(format: "uid == %d", uid)
    }

    // MARK: -

    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "uid", ascending: true)
    }

    func first(with uid: Int64) -> Comment? {
        return self.first(with: self.createPredicate(with: uid))
    }

    func last(with uid: Int64) -> Comment? {
        return self.last(with: self.createPredicate(with: uid))
    }

    func fetch(with uid: Int64) -> [Comment] {
        return self.fetch(with: self.createPredicate(with: uid))
    }

    func count(with uid: Int64) -> Int {
        return self.count(with: self.createPredicate(with: uid))
    }

    func clear(with uid: Int64) {
        self.clear(with: self.createPredicate(with: uid))
    }

    func append(withUID uid: Int64) -> Comment {
        let comment = self.append()

        comment.uid = uid

        return comment
    }
}
