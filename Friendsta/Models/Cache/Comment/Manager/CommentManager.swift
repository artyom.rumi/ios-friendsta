//
//  CommentManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol CommentManager {

    // MARK: - Instance Properties

    var objectsRemovedEvent: Event<[Comment]> { get }
    var objectsAppendedEvent: Event<[Comment]> { get }
    var objectsUpdatedEvent: Event<[Comment]> { get }
    var objectsChangedEvent: Event<[Comment]> { get }

    // MARK: - Instance Methods

    func firstOrNew(withUID uid: Int64) -> Comment

    func first(with uid: Int64) -> Comment?
    func first() -> Comment?

    func last(with uid: Int64) -> Comment?
    func last() -> Comment?

    func fetch(with uid: Int64) -> [Comment]
    func fetch() -> [Comment]

    func count(with uid: Int64) -> Int
    func count() -> Int

    func clear(with uid: Int64)
    func clear()

    func remove(object: Comment)

    func append(withUID uid: Int64) -> Comment
    func append() -> Comment

    // MARK: -

    func startObserving()
    func stopObserving()
}

// MARK: -

extension CommentManager {

    // MARK: - Instance Methods

    func firstOrNew(withUID uid: Int64) -> Comment {
        return self.first(with: uid) ?? self.append()
    }
}
