//
//  CommentExtension.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension Comment {

    // MARK: - Instance Properties

    var status: CommentStatus? {
        get {
            if let rawStatus = self.rawStatus {
                return CommentStatus(rawValue: rawStatus)
            } else {
                return nil
            }
        }

        set {
            self.rawStatus = newValue?.rawValue
        }
    }
}
