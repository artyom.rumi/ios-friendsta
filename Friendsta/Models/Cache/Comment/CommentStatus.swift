//
//  CommentStatus.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 22/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

enum CommentStatus: String {

    // MARK: - Enumeration Cases

    case unread = "Unread"
    case read = "Read"
}
