//
//  ChatMembersManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 16.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ChatMembersManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[ChatMember]> { get }
    var objectsAppendedEvent: Event<[ChatMember]> { get }
    var objectsUpdatedEvent: Event<[ChatMember]> { get }
    var objectsChangedEvent: Event<[ChatMember]> { get }
    
    // MARK: - Instance Methods
    
    func first(withChatMaskUID chatMaskUID: Int64, chatUID: Int64) -> ChatMember?
    func first(withChatMaskUID chatMaskUID: Int64) -> ChatMember?
    func first(withUserUID userUID: Int64, chatUID: Int64) -> ChatMember?
    func first(withUserUID userUID: Int64) -> ChatMember?
    func first() -> ChatMember?
    
    func last(withChatMaskUID chatMaskUID: Int64, chatUID: Int64) -> ChatMember?
    func last(withChatMaskUID chatMaskUID: Int64) -> ChatMember?
    func last(withUserUID userUID: Int64, chatUID: Int64) -> ChatMember?
    func last(withUserUID userUID: Int64) -> ChatMember?
    func last() -> ChatMember?
    
    func fetch(withChatMaskUID chatMaskUID: Int64, chatUID: Int64) -> [ChatMember]
    func fetch(withChatMaskUID chatMaskUID: Int64) -> [ChatMember]
    func fetch(withUserUID userUID: Int64, chatUID: Int64) -> [ChatMember]
    func fetch(withUserUID userUID: Int64) -> [ChatMember]
    func fetch() -> [ChatMember]
    
    func count(withChatMaskUID chatMaskUID: Int64, chatUID: Int64) -> Int
    func count(withChatMaskUID chatMaskUID: Int64) -> Int
    func count(withUserUID userUID: Int64, chatUID: Int64) -> Int
    func count(withUserUID userUID: Int64) -> Int
    func count() -> Int
    
    func clear(withChatMaskUID chatMaskUID: Int64, chatUID: Int64)
    func clear(withChatMaskUID chatMaskUID: Int64)
    func clear(withUserUID userUID: Int64, chatUID: Int64)
    func clear(withUserUID userUID: Int64)
    func clear()
    
    func remove(object: ChatMember)
    func append() -> ChatMember
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
