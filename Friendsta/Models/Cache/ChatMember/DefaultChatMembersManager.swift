//
//  DefaultChatMembersManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 16.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultChatMembersManager: CoreDataModelManager, ChatMembersManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = ChatMember
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[ChatMember]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[ChatMember]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[ChatMember]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[ChatMember]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods

    fileprivate func createPredicate(withChatMaskUID chatMaskUID: Int64, chatUID: Int64) -> NSPredicate {
        return NSCompoundPredicate(andPredicateWithSubpredicates: [NSPredicate(format: "mask.uid == %d", chatMaskUID),
                                                                   NSPredicate(format: "chat.uid == %d", chatUID)])
    }
    
    fileprivate func createPredicate(withChatMaskUID chatMaskUID: Int64) -> NSPredicate {
        return NSPredicate(format: "mask.uid == %d", chatMaskUID)
    }
    
    fileprivate func createPredicate(withUserUID userUID: Int64, chatUID: Int64) -> NSPredicate {
        return NSCompoundPredicate(andPredicateWithSubpredicates: [NSPredicate(format: "user.uid == %d", userUID),
                                                                   NSPredicate(format: "chat.uid == %d", chatUID)])
    }
    
    fileprivate func createPredicate(withUserUID userUID: Int64) -> NSPredicate {
        return NSPredicate(format: "user.uid == %d", userUID)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "isDeanonymized", ascending: true)
    }
    
    func first(withChatMaskUID chatMaskUID: Int64, chatUID: Int64) -> ChatMember? {
        return self.first(with: self.createPredicate(withChatMaskUID: chatMaskUID, chatUID: chatUID))
    }
    
    func first(withChatMaskUID chatMaskUID: Int64) -> ChatMember? {
        return self.first(with: self.createPredicate(withChatMaskUID: chatMaskUID))
    }
    
    func first(withUserUID userUID: Int64, chatUID: Int64) -> ChatMember? {
        return self.first(with: self.createPredicate(withUserUID: userUID, chatUID: chatUID))
    }
    
    func first(withUserUID userUID: Int64) -> ChatMember? {
        return self.first(with: self.createPredicate(withUserUID: userUID))
    }
    
    func last(withChatMaskUID chatMaskUID: Int64, chatUID: Int64) -> ChatMember? {
        return self.last(with: self.createPredicate(withChatMaskUID: chatMaskUID, chatUID: chatUID))
    }
    
    func last(withChatMaskUID chatMaskUID: Int64) -> ChatMember? {
        return self.last(with: self.createPredicate(withChatMaskUID: chatMaskUID))
    }
    
    func last(withUserUID userUID: Int64, chatUID: Int64) -> ChatMember? {
        return self.last(with: self.createPredicate(withUserUID: userUID, chatUID: chatUID))
    }
    
    func last(withUserUID userUID: Int64) -> ChatMember? {
        return self.last(with: self.createPredicate(withUserUID: userUID))
    }
    
    func fetch(withChatMaskUID chatMaskUID: Int64, chatUID: Int64) -> [ChatMember] {
        return self.fetch(with: self.createPredicate(withChatMaskUID: chatMaskUID, chatUID: chatUID))
    }
    
    func fetch(withChatMaskUID chatMaskUID: Int64) -> [ChatMember] {
        return self.fetch(with: self.createPredicate(withChatMaskUID: chatMaskUID))
    }
    
    func fetch(withUserUID userUID: Int64, chatUID: Int64) -> [ChatMember] {
        return self.fetch(with: self.createPredicate(withUserUID: userUID, chatUID: chatUID))
    }
    
    func fetch(withUserUID userUID: Int64) -> [ChatMember] {
        return self.fetch(with: self.createPredicate(withUserUID: userUID))
    }
    
    func count(withChatMaskUID chatMaskUID: Int64, chatUID: Int64) -> Int {
        return self.count(with: self.createPredicate(withChatMaskUID: chatMaskUID, chatUID: chatUID))
    }
    
    func count(withChatMaskUID chatMaskUID: Int64) -> Int {
        return self.count(with: self.createPredicate(withChatMaskUID: chatMaskUID))
    }
    
    func count(withUserUID userUID: Int64, chatUID: Int64) -> Int {
        return self.count(with: self.createPredicate(withUserUID: userUID, chatUID: chatUID))
    }
    
    func count(withUserUID userUID: Int64) -> Int {
        return self.count(with: self.createPredicate(withUserUID: userUID))
    }
    
    func clear(withChatMaskUID chatMaskUID: Int64, chatUID: Int64) {
        return self.clear(with: self.createPredicate(withChatMaskUID: chatMaskUID, chatUID: chatUID))
    }
    
    func clear(withChatMaskUID chatMaskUID: Int64) {
        return self.clear(with: self.createPredicate(withChatMaskUID: chatMaskUID))
    }
    
    func clear(withUserUID userUID: Int64, chatUID: Int64) {
        return self.clear(with: self.createPredicate(withUserUID: userUID, chatUID: chatUID))
    }
    
    func clear(withUserUID userUID: Int64) {
        return self.clear(with: self.createPredicate(withUserUID: userUID))
    }
}
