//
//  FeedListType.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

enum FeedListType {

    // MARK: - Enumeration Cases

    case unknown
    case all
    case timeline
    case anonymous
    case discover

    // MARK: - Instance Properties

    var rawValue: Int16 {
        switch self {
        case .unknown:
            return 0

        case .all:
            return 1
            
        case .timeline:
            return 2
            
        case .anonymous:
            return 3
            
        case .discover:
            return 4
        }
    }

    // MARK: - Initializers

    init?(rawValue: Int16) {
        switch rawValue {
        case 0:
            self = .unknown

        case 1:
            self = .all
            
        case 2:
            self = .timeline
            
        case 3:
            self = .anonymous
            
        case 4:
            self = .discover

        default:
            return nil
        }
    }
}

// MARK: - Hashable

extension FeedListType: Hashable {

    // MARK: - Instance Properties

    func hash(into hasher: inout Hasher) {
        hasher.combine(Int(self.rawValue))
    }
}
