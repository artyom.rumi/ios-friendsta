//
//  FeedListExtension.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension FeedList {

    // MARK: - Instance Properties

    var listType: FeedListType {
        get {
            return FeedListType(rawValue: self.listRawType) ?? .unknown
        }

        set {
            self.listRawType = newValue.rawValue
        }
    }

    var count: Int {
        return self.rawFeeds?.count ?? 0
    }

    var allFeeds: [Feed] {
        return (self.rawFeeds?.array as? [Feed]) ?? []
    }

    // MARK: - Instance Methods

    func clearFeeds() {
        if let feeds = self.rawFeeds {
            self.removeFromRawFeeds(feeds)
        }
    }
}
