//
//  DefaultFeedListManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultFeedListManager: CoreDataModelManager, FeedListManager, CacheModelContextObserver {

    // MARK: - Nested Types

    typealias Object = FeedList

    // MARK: - Instance Properties

    unowned let context: CoreDataModelContext

    fileprivate(set) lazy var objectsRemovedEvent = Event<[FeedList]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[FeedList]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[FeedList]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[FeedList]>()

    // MARK: - Initializers

    init(context: CoreDataModelContext) {
        self.context = context
    }

    // MARK: - Instance Methods

    fileprivate func createPredicate(with listType: FeedListType) -> NSPredicate {
        return NSPredicate(format: "listRawType == %d", listType.rawValue)
    }

    // MARK: -

    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "listRawType", ascending: true)
    }

    func first(with listType: FeedListType) -> FeedList? {
        return self.first(with: self.createPredicate(with: listType))
    }

    func last(with listType: FeedListType) -> FeedList? {
        return self.last(with: self.createPredicate(with: listType))
    }

    func fetch(with listType: FeedListType) -> [FeedList] {
        return self.fetch(with: self.createPredicate(with: listType))
    }

    func count(with listType: FeedListType) -> Int {
        return self.count(with: self.createPredicate(with: listType))
    }

    func clear(with listType: FeedListType) {
        self.clear(with: self.createPredicate(with: listType))
    }

    func append(withListType listType: FeedListType) -> FeedList {
        let feedList = self.append()

        feedList.listType = listType

        return feedList
    }
}
