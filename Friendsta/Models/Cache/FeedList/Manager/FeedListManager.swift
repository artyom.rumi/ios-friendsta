//
//  FeedListManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol FeedListManager {

    // MARK: - Instance Properties

    var objectsRemovedEvent: Event<[FeedList]> { get }
    var objectsAppendedEvent: Event<[FeedList]> { get }
    var objectsUpdatedEvent: Event<[FeedList]> { get }
    var objectsChangedEvent: Event<[FeedList]> { get }

    // MARK: - Instance Methods

    func firstOrNew(withListType listType: FeedListType) -> FeedList

    func first(with listType: FeedListType) -> FeedList?
    func first() -> FeedList?

    func last(with listType: FeedListType) -> FeedList?
    func last() -> FeedList?

    func fetch(with listType: FeedListType) -> [FeedList]
    func fetch() -> [FeedList]

    func count(with listType: FeedListType) -> Int
    func count() -> Int

    func clear(with listType: FeedListType)
    func clear()

    func remove(object: FeedList)

    func append(withListType listType: FeedListType) -> FeedList
    func append() -> FeedList

    // MARK: -

    func startObserving()
    func stopObserving()
}

// MARK: -

extension FeedListManager {

    // MARK: - Instance Methods

    func firstOrNew(withListType listType: FeedListType) -> FeedList {
        return self.first(with: listType) ?? self.append(withListType: listType)
    }
}
