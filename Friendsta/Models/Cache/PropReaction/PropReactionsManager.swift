//
//  PropReactionsManager.swift
//  Friendsta
//
//  Created by Marat Galeev on 12.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol PropReactionsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[PropReaction]> { get }
    var objectsAppendedEvent: Event<[PropReaction]> { get }
    var objectsUpdatedEvent: Event<[PropReaction]> { get }
    var objectsChangedEvent: Event<[PropReaction]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> PropReaction?
    func first() -> PropReaction?
    
    func last(with uid: Int64) -> PropReaction?
    func last() -> PropReaction?
    
    func fetch(with uid: Int64) -> [PropReaction]
    func fetch() -> [PropReaction]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: PropReaction)
    func append() -> PropReaction
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
