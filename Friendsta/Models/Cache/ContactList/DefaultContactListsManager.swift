//
//  DefaultContactListsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultContactListsManager: CoreDataModelManager, ContactListsManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = ContactListItem
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[ContactListItem]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[ContactListItem]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[ContactListItem]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[ContactListItem]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with listType: ContactListType) -> NSPredicate {
        return NSPredicate(format: "listRawType == %d", listType.rawValue)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "sorting", ascending: true)
    }
    
    func first(with listType: ContactListType) -> ContactListItem? {
        return self.first(with: self.createPredicate(with: listType))
    }
    
    func last(with listType: ContactListType) -> ContactListItem? {
        return self.last(with: self.createPredicate(with: listType))
    }
    
    func fetch(with listType: ContactListType) -> [ContactListItem] {
        return self.fetch(with: self.createPredicate(with: listType))
    }
    
    func count(with listType: ContactListType) -> Int {
        return self.count(with: self.createPredicate(with: listType))
    }
    
    func clear(with listType: ContactListType) {
        self.clear(with: self.createPredicate(with: listType))
    }
}
