//
//  ContactListItemExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension ContactListItem {
    
    // MARK: - Instance Properties
    
    var listType: ContactListType? {
        get {
            return ContactListType(rawValue: self.listRawType)
        }
        
        set {
            if let newValue = newValue {
                self.listRawType = newValue.rawValue
            } else {
                self.listRawType = -1
            }
        }
    }
}
