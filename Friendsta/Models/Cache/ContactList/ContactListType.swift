//
//  ContactListType.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum ContactListType: Int16 {
    
    // MARK: - Enumeration Cases
    
    case all
}
