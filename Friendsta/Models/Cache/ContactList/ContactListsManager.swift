//
//  ContactListsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ContactListsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[ContactListItem]> { get }
    var objectsAppendedEvent: Event<[ContactListItem]> { get }
    var objectsUpdatedEvent: Event<[ContactListItem]> { get }
    var objectsChangedEvent: Event<[ContactListItem]> { get }
    
    // MARK: - Instance Methods
    
    func first(with listType: ContactListType) -> ContactListItem?
    func first() -> ContactListItem?
    
    func last(with listType: ContactListType) -> ContactListItem?
    func last() -> ContactListItem?
    
    func fetch(with listType: ContactListType) -> [ContactListItem]
    func fetch() -> [ContactListItem]
    
    func count(with listType: ContactListType) -> Int
    func count() -> Int
    
    func clear(with listType: ContactListType)
    func clear()
    
    func remove(object: ContactListItem)
    func append() -> ContactListItem
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
