//
//  PropReceiverExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 28.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension PropReceiver {
    
    // MARK: - Instance Properties
    
    var hasRequest: Bool {
        return (self.request != nil)
    }
}
