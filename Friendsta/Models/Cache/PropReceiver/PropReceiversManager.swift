//
//  PropReceiversManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 28.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol PropReceiversManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[PropReceiver]> { get }
    var objectsAppendedEvent: Event<[PropReceiver]> { get }
    var objectsUpdatedEvent: Event<[PropReceiver]> { get }
    var objectsChangedEvent: Event<[PropReceiver]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> PropReceiver?
    func first() -> PropReceiver?
    
    func last(with uid: Int64) -> PropReceiver?
    func last() -> PropReceiver?
    
    func fetch(with uid: Int64) -> [PropReceiver]
    func fetch() -> [PropReceiver]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: PropReceiver)
    func append() -> PropReceiver
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
