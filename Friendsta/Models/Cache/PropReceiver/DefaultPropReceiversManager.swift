//
//  DefaultPropReceiversManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 28.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultPropReceiversManager: CoreDataModelManager, PropReceiversManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = PropReceiver
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[PropReceiver]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[PropReceiver]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[PropReceiver]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[PropReceiver]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with uid: Int64) -> NSPredicate {
        return NSPredicate(format: "uid == %d", uid)
    }
    
    // MARK: - PropNotificationsManager
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "uid", ascending: true)
    }
    
    func first(with uid: Int64) -> PropReceiver? {
        return self.first(with: self.createPredicate(with: uid))
    }
    
    func last(with uid: Int64) -> PropReceiver? {
        return self.last(with: self.createPredicate(with: uid))
    }
    
    func fetch(with uid: Int64) -> [PropReceiver] {
        return self.fetch(with: self.createPredicate(with: uid))
    }
    
    func count(with uid: Int64) -> Int {
        return self.count(with: self.createPredicate(with: uid))
    }
    
    func clear(with uid: Int64) {
        self.clear(with: self.createPredicate(with: uid))
    }
}
