//
//  ChatTextMessagesManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ChatTextMessagesManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[ChatTextMessage]> { get }
    var objectsAppendedEvent: Event<[ChatTextMessage]> { get }
    var objectsUpdatedEvent: Event<[ChatTextMessage]> { get }
    var objectsChangedEvent: Event<[ChatTextMessage]> { get }
    
    // MARK: - Instance Methods
    
    func first(withChatUID: Int64) -> ChatTextMessage?
    func first(with uid: String) -> ChatTextMessage?
    func first() -> ChatTextMessage?
    
    func last(withChatUID: Int64) -> ChatTextMessage?
    func last(with uid: String) -> ChatTextMessage?
    func last() -> ChatTextMessage?
    
    func fetch(withChatUID: Int64) -> [ChatTextMessage]
    func fetch(with uid: String) -> [ChatTextMessage]
    func fetch() -> [ChatTextMessage]
    
    func count(withChatUID: Int64) -> Int
    func count(with uid: String) -> Int
    func count() -> Int
    
    func clear(withChatUID: Int64)
    func clear(with uid: String)
    func clear()
    
    func remove(object: ChatTextMessage)
    func append() -> ChatTextMessage
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
