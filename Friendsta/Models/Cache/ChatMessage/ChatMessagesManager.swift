//
//  ChatMessagesManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ChatMessagesManager {

    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[ChatMessage]> { get }
    var objectsAppendedEvent: Event<[ChatMessage]> { get }
    var objectsUpdatedEvent: Event<[ChatMessage]> { get }
    var objectsChangedEvent: Event<[ChatMessage]> { get }
    
    // MARK: - Instance Methods
    
    func createDefaultSortDescriptor() -> NSSortDescriptor
    
    // MARK: -
    
    func first(withChatUID: Int64) -> ChatMessage?
    func first(with uid: String) -> ChatMessage?
    func first() -> ChatMessage?
    
    func last(withChatUID: Int64) -> ChatMessage?
    func last(with uid: String) -> ChatMessage?
    func last() -> ChatMessage?
    
    func fetch(withChatUID: Int64) -> [ChatMessage]
    func fetch(with uid: String) -> [ChatMessage]
    func fetch() -> [ChatMessage]
    
    func count(withChatUID: Int64) -> Int
    func count(with uid: String) -> Int
    func count() -> Int
    
    func clear(withChatUID: Int64)
    func clear(with uid: String)
    func clear()
    
    func remove(object: ChatMessage)
    func append() -> ChatMessage
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
