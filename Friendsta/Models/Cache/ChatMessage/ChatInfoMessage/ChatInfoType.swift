//
//  ChatInfoType.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum ChatInfoType: Int16 {
    
    // MARK: - Enumeration Cases
    
    case match
    case oneReveal
    case mutualReveal
    case cancelReveal
}
