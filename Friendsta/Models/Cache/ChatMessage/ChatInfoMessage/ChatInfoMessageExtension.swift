//
//  ChatInfoMessageExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension ChatInfoMessage {
    
    // MARK: - Instance Properties
    
    var type: ChatInfoType? {
        get {
            return ChatInfoType(rawValue: self.rawType)
        }
        
        set {
            if let newValue = newValue {
                self.rawType = newValue.rawValue
            } else {
                self.rawType = -1
            }
        }
    }
}
