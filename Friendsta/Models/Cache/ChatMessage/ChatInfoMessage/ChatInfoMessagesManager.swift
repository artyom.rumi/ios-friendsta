//
//  ChatInfoMessagesManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ChatInfoMessagesManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[ChatInfoMessage]> { get }
    var objectsAppendedEvent: Event<[ChatInfoMessage]> { get }
    var objectsUpdatedEvent: Event<[ChatInfoMessage]> { get }
    var objectsChangedEvent: Event<[ChatInfoMessage]> { get }
    
    // MARK: - Instance Methods
    
    func first(withChatUID: Int64) -> ChatInfoMessage?
    func first(with uid: String) -> ChatInfoMessage?
    func first() -> ChatInfoMessage?
    
    func last(withChatUID: Int64) -> ChatInfoMessage?
    func last(with uid: String) -> ChatInfoMessage?
    func last() -> ChatInfoMessage?
    
    func fetch(withChatUID: Int64) -> [ChatInfoMessage]
    func fetch(with uid: String) -> [ChatInfoMessage]
    func fetch() -> [ChatInfoMessage]
    
    func count(withChatUID: Int64) -> Int
    func count(with uid: String) -> Int
    func count() -> Int
    
    func clear(withChatUID: Int64)
    func clear(with uid: String)
    func clear()
    
    func remove(object: ChatInfoMessage)
    func append() -> ChatInfoMessage
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
