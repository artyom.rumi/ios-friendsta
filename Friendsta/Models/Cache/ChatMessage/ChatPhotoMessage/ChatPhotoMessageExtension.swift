//
//  ChatPhotoMessageExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension ChatPhotoMessage {
    
    // MARK: - Instance Properties
    
    var photoLocalURL: URL? {
        get {
            if let photoRawURL = self.photoLocalRawURL {
                return URL(string: photoRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.photoLocalRawURL = newValue?.absoluteString
        }
    }
    
    var photoURL: URL? {
        get {
            if let photoRawURL = self.photoRawURL {
                return URL(string: photoRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.photoRawURL = newValue?.absoluteString
        }
    }
    
    var smallPhotoURL: URL? {
        get {
            if let photoRawURL = self.smallPhotoRawURL {
                return URL(string: photoRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.smallPhotoRawURL = newValue?.absoluteString
        }
    }
    
    var mediumPhotoURL: URL? {
        get {
            if let photoRawURL = self.mediumPhotoRawURL {
                return URL(string: photoRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.mediumPhotoRawURL = newValue?.absoluteString
        }
    }
    
    var largePhotoURL: URL? {
        get {
            if let photoRawURL = self.largePhotoRawURL {
                return URL(string: photoRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.largePhotoRawURL = newValue?.absoluteString
        }
    }
    
    var photo: Photo? {
        get {
            return Photo(imageURL: self.photoURL,
                         smallImageURL: self.smallPhotoURL,
                         mediumImageURL: self.mediumPhotoURL,
                         largeImageURL: self.largePhotoURL)
        }
        
        set {
            self.photoURL = newValue?.imageURL
            
            self.smallPhotoURL = newValue?.smallImageURL
            self.mediumPhotoURL = newValue?.mediumImageURL
            self.largePhotoURL = newValue?.largeImageURL
        }
    }
}
