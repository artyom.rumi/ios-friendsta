//
//  ChatPhotoMessagesManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ChatPhotoMessagesManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[ChatPhotoMessage]> { get }
    var objectsAppendedEvent: Event<[ChatPhotoMessage]> { get }
    var objectsUpdatedEvent: Event<[ChatPhotoMessage]> { get }
    var objectsChangedEvent: Event<[ChatPhotoMessage]> { get }
    
    // MARK: - Instance Methods
    
    func createDefaultSortDescriptor() -> NSSortDescriptor
    
    // MARK: -
    
    func first(withChatUID: Int64) -> ChatPhotoMessage?
    func first(with uid: String) -> ChatPhotoMessage?
    func first() -> ChatPhotoMessage?
    
    func last(withChatUID: Int64) -> ChatPhotoMessage?
    func last(with uid: String) -> ChatPhotoMessage?
    func last() -> ChatPhotoMessage?
    
    func fetch(withChatUID: Int64) -> [ChatPhotoMessage]
    func fetch(with uid: String) -> [ChatPhotoMessage]
    func fetch() -> [ChatPhotoMessage]
    
    func count(withChatUID: Int64) -> Int
    func count(with uid: String) -> Int
    func count() -> Int
    
    func clear(withChatUID: Int64)
    func clear(with uid: String)
    func clear()
    
    func remove(object: ChatPhotoMessage)
    func append() -> ChatPhotoMessage
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
