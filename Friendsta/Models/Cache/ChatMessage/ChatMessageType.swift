//
//  ChatMessageType.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum ChatMessageType {
    
    // MARK: - Enumeration Cases
    
    case text
    case photo
    case info
}
