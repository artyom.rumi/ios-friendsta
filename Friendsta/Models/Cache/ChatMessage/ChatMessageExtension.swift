//
//  ChatMessageExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension ChatMessage {
    
    // MARK: - Instance Properties
    
    var isViewed: Bool {
        return (self.viewedDate != nil)
    }
    
    var isErased: Bool {
        return (self.erasedDate != nil)
    }
}
