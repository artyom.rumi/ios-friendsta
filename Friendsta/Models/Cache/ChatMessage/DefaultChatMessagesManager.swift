//
//  DefaultChatMessagesManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultChatMessagesManager: CoreDataModelManager, ChatMessagesManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = ChatMessage
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[ChatMessage]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[ChatMessage]>()
    fileprivate(set) lazy var objectsRemovedEvent = Event<[ChatMessage]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[ChatMessage]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(withChatUID chatUID: Int64) -> NSPredicate {
        return NSPredicate(format: "chatUID == %d", chatUID)
    }
    
    fileprivate func createPredicate(with uid: String) -> NSPredicate {
        return NSPredicate(format: "uid == %@", uid)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "createdDate", ascending: false)
    }
    
    func first(withChatUID chatUID: Int64) -> ChatMessage? {
        return self.first(with: self.createPredicate(withChatUID: chatUID))
    }
    
    func first(with uid: String) -> ChatMessage? {
        return self.first(with: self.createPredicate(with: uid))
    }
    
    func last(withChatUID chatUID: Int64) -> ChatMessage? {
        return self.last(with: self.createPredicate(withChatUID: chatUID))
    }
    
    func last(with uid: String) -> ChatMessage? {
        return self.last(with: self.createPredicate(with: uid))
    }
    
    func fetch(withChatUID chatUID: Int64) -> [ChatMessage] {
        return self.fetch(with: self.createPredicate(withChatUID: chatUID))
    }
    
    func fetch(with uid: String) -> [ChatMessage] {
        return self.fetch(with: self.createPredicate(with: uid))
    }
    
    func count(withChatUID chatUID: Int64) -> Int {
        return self.count(with: self.createPredicate(withChatUID: chatUID))
    }
    
    func count(with uid: String) -> Int {
        return self.count(with: self.createPredicate(with: uid))
    }
    
    func clear(withChatUID chatUID: Int64) {
        return self.clear(with: self.createPredicate(withChatUID: chatUID))
    }
    
    func clear(with uid: String) {
        return self.clear(with: self.createPredicate(with: uid))
    }
}
