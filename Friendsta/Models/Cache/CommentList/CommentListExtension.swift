//
//  CommentListExtension.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension CommentList {

    // MARK: - Instance Properties

    var listType: CommentListType {
        get {
            return CommentListType(rawValue: self.listRawType, feedUID: self.feedUID) ?? .unknown
        }

        set {
            self.listRawType = newValue.rawValue
            self.feedUID = newValue.feedUID
        }
    }

    var count: Int {
        return self.rawComments?.count ?? 0
    }

    var isEmpty: Bool {
        return self.allComments.isEmpty
    }

    var allComments: [Comment] {
        return (self.rawComments?.array as? [Comment]) ?? []
    }

    // MARK: - Instance Subscripts

    subscript(index: Int) -> Comment {
        return self.rawComments![index] as! Comment
    }

    // MARK: - Instance Methods

    func clearComments() {
        if let comments = self.rawComments {
            self.removeFromRawComments(comments)
        }
    }
}
