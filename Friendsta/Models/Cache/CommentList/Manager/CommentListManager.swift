//
//  CommentListManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol CommentListManager {

    // MARK: - Instance Properties

    var objectsRemovedEvent: Event<[CommentList]> { get }
    var objectsAppendedEvent: Event<[CommentList]> { get }
    var objectsUpdatedEvent: Event<[CommentList]> { get }
    var objectsChangedEvent: Event<[CommentList]> { get }

    // MARK: - Instance Methods

    func firstOrNew(withListType listType: CommentListType) -> CommentList

    func first(with listType: CommentListType) -> CommentList?
    func first() -> CommentList?

    func last(with listType: CommentListType) -> CommentList?
    func last() -> CommentList?

    func fetch(with listType: CommentListType) -> [CommentList]
    func fetch() -> [CommentList]

    func count(with listType: CommentListType) -> Int
    func count() -> Int

    func clear(with listType: CommentListType)
    func clear()

    func remove(object: CommentList)

    func append(withListType listType: CommentListType) -> CommentList
    func append() -> CommentList

    // MARK: -

    func startObserving()
    func stopObserving()
}

// MARK: -

extension CommentListManager {

    // MARK: - Instance Methods

    func firstOrNew(withListType listType: CommentListType) -> CommentList {
        return self.first(with: listType) ?? self.append(withListType: listType)
    }
}
