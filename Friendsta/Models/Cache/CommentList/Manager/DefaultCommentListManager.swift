//
//  DefaultCommentListManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultCommentListManager: CoreDataModelManager, CommentListManager, CacheModelContextObserver {

    // MARK: - Nested Types

    typealias Object = CommentList

    // MARK: - Instance Properties

    unowned let context: CoreDataModelContext

    fileprivate(set) lazy var objectsRemovedEvent = Event<[CommentList]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[CommentList]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[CommentList]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[CommentList]>()

    // MARK: - Initializers

    init(context: CoreDataModelContext) {
        self.context = context
    }

    // MARK: - Instance Methods

    fileprivate func createPredicate(with listType: CommentListType) -> NSPredicate {
        var predicates = [NSPredicate(format: "listRawType == %d", listType.rawValue)]

        switch listType {
        case .feed(let feedUID):
            predicates.append(NSPredicate(format: "feedUID == %d", feedUID))

        case .unknown:
            break
        }

        return NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
    }

    // MARK: -

    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "listRawType", ascending: true)
    }

    func first(with listType: CommentListType) -> CommentList? {
        return self.first(with: self.createPredicate(with: listType))
    }

    func last(with listType: CommentListType) -> CommentList? {
        return self.last(with: self.createPredicate(with: listType))
    }

    func fetch(with listType: CommentListType) -> [CommentList] {
        return self.fetch(with: self.createPredicate(with: listType))
    }

    func count(with listType: CommentListType) -> Int {
        return self.count(with: self.createPredicate(with: listType))
    }

    func clear(with listType: CommentListType) {
        self.clear(with: self.createPredicate(with: listType))
    }

    func append(withListType listType: CommentListType) -> CommentList {
        let commentList = self.append()

        commentList.listType = listType

        return commentList
    }
}
