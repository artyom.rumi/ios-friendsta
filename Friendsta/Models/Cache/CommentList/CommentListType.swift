//
//  CommentListType.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

enum CommentListType {

    // MARK: - Enumeration Cases

    case unknown
    case feed(uid: Int64)

    // MARK: - Instance Properties

    var rawValue: Int16 {
        switch self {
        case .unknown:
            return 0

        case .feed:
            return 1
        }
    }

    var feedUID: Int64 {
        switch self {
        case .unknown:
            return 0

        case .feed(let feedUID):
            return feedUID
        }
    }

    // MARK: - Initializers

    init?(rawValue: Int16, feedUID: Int64) {
        switch rawValue {
        case 0:
            self = .unknown

        case 1:
            self = .feed(uid: feedUID)

        default:
            return nil
        }
    }
}
