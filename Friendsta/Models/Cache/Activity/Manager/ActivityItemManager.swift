//
//  ActivityManager.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/22/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ActivityManager {

    // MARK: - Instance Properties

    var objectsRemovedEvent: Event<[Activity]> { get }
    var objectsAppendedEvent: Event<[Activity]> { get }
    var objectsUpdatedEvent: Event<[Activity]> { get }
    var objectsChangedEvent: Event<[Activity]> { get }

    // MARK: - Instance Methods

    func firstOrNew(withUID uid: Int64) -> Activity

    func first(with uid: Int64) -> Activity?
    func first() -> Activity?

    func last(with uid: Int64) -> Activity?
    func last() -> Activity?

    func fetch(with uid: Int64) -> [Activity]
    func fetch() -> [Activity]

    func count(with uid: Int64) -> Int
    func count() -> Int

    func clear(with uid: Int64)
    func clear()

    func remove(object: Activity)

    func append(withUID uid: Int64) -> Activity
    func append() -> Activity

    // MARK: -

    func startObserving()
    func stopObserving()
}

// MARK: -

extension ActivityManager {

    // MARK: - Instance Methods

    func firstOrNew(withUID uid: Int64) -> Activity {
        return self.first(with: uid) ?? self.append()
    }
}
