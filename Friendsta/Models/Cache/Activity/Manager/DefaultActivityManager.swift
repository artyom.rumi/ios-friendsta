//
//  DefaultActivityManager.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/22/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultActivityManager: CoreDataModelManager, ActivityManager, CacheModelContextObserver {

    // MARK: - Nested Types

    typealias Object = Activity

    // MARK: - Instance Properties

    unowned let context: CoreDataModelContext

    private(set) lazy var objectsRemovedEvent = Event<[Activity]>()
    private(set) lazy var objectsAppendedEvent = Event<[Activity]>()
    private(set) lazy var objectsUpdatedEvent = Event<[Activity]>()
    private(set) lazy var objectsChangedEvent = Event<[Activity]>()

    // MARK: - Initializers

    init(context: CoreDataModelContext) {
        self.context = context
    }

    // MARK: - Instance Methods

    private func createPredicate(with uid: Int64) -> NSPredicate {
        return NSPredicate(format: "uid == %d", uid)
    }

    // MARK: -

    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "uid", ascending: true)
    }

    func first(with uid: Int64) -> Activity? {
        return self.first(with: self.createPredicate(with: uid))
    }

    func last(with uid: Int64) -> Activity? {
        return self.last(with: self.createPredicate(with: uid))
    }

    func fetch(with uid: Int64) -> [Activity] {
        return self.fetch(with: self.createPredicate(with: uid))
    }

    func count(with uid: Int64) -> Int {
        return self.count(with: self.createPredicate(with: uid))
    }

    func clear(with uid: Int64) {
        self.clear(with: self.createPredicate(with: uid))
    }

    func append(withUID uid: Int64) -> Activity {
        let activity = self.append()

        activity.uid = uid

        return activity
    }
}
