//
//  ActivityType.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/22/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

enum ActivityType: String {

    // MARK: - Enumeration cases

    case like = "new_like"
    case repost = "new_repost"
    case comment = "new_comment"
    case newFriend = "new_friend"
    case expireFriend72h = "72h_expired_friend"
    case expireFriend12h = "12h_expired_friend"
    case expiredFriendship = "expired_friendship"
    // added by lava on 0121
    case rate = "new_rate"
    case rateFriend = "new_rate_friend"

    // MARK: - Type Methods

    static func value(from rawValue: String?) -> ActivityType {
        guard let rawValue = rawValue, let activityType = ActivityType(rawValue: rawValue) else {
            return .like
        }
        return activityType
    }
}
