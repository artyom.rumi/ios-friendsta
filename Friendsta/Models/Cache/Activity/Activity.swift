//
//  Activity.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/22/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension Activity {
    
    // MARK: - Instance Properties

    var type: ActivityType {
        get {
            ActivityType.value(from: self.rawType)
        }

        set {
            self.rawType = newValue.rawValue
        }
    }
}
