//
//  SchoolsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol SchoolsManager {

    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[School]> { get }
    var objectsAppendedEvent: Event<[School]> { get }
    var objectsUpdatedEvent: Event<[School]> { get }
    var objectsChangedEvent: Event<[School]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> School?
    func first() -> School?
    
    func last(with uid: Int64) -> School?
    func last() -> School?
    
    func fetch(with uid: Int64) -> [School]
    func fetch() -> [School]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: School)
    func append() -> School
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
