//
//  CacheModel.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 09.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol CacheModel {
    
    // MARK: - Instance Properties
    
    var identifier: String { get }

    var viewContext: CacheModelContext { get }
}
