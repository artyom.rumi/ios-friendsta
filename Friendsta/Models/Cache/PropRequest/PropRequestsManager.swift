//
//  PropRequestsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 27.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol PropRequestsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[PropRequest]> { get }
    var objectsAppendedEvent: Event<[PropRequest]> { get }
    var objectsUpdatedEvent: Event<[PropRequest]> { get }
    var objectsChangedEvent: Event<[PropRequest]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> PropRequest?
    func first() -> PropRequest?
    
    func last(with uid: Int64) -> PropRequest?
    func last() -> PropRequest?
    
    func fetch(with uid: Int64) -> [PropRequest]
    func fetch() -> [PropRequest]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: PropRequest)
    func append() -> PropRequest
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
