//
//  AccountUsersManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol AccountUsersManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[AccountUser]> { get }
    var objectsAppendedEvent: Event<[AccountUser]> { get }
    var objectsUpdatedEvent: Event<[AccountUser]> { get }
    var objectsChangedEvent: Event<[AccountUser]> { get }
    var objectsSchoolUpdatedEvent: Event<[AccountUser]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> AccountUser?
    func first() -> AccountUser?
    
    func last(with uid: Int64) -> AccountUser?
    func last() -> AccountUser?
    
    func fetch(with uid: Int64) -> [AccountUser]
    func fetch() -> [AccountUser]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: AccountUser)
    func append() -> AccountUser
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
