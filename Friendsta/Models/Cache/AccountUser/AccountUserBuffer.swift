//
//  AccountUserBuffer.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

class AccountUserBuffer {
    
    // MARK: - Type Properties
    
    static let shared = AccountUserBuffer()
    
    // MARK: - Instance Properties
    
    var firstName: String?
    var lastName: String?
    
    var age: Int?
    var bio: String?
    
    var classYear: Int?
    
    var schoolUID: Int64?
    var schoolTitle: String?
    var schoolLocation: String?
    
    var gender: Gender?
    
    var phoneNumbers: [String] = []
    
    var hideTopPokesList: Bool = false
    
    // MARK: -
    
    var fullName: String? {
        if let firstName = self.firstName, !firstName.isEmpty {
            if let lastName = self.lastName, !lastName.isEmpty {
                return "\(firstName) \(lastName)"
            } else {
                return firstName
            }
        } else {
            return self.lastName
        }
    }
    
    // MARK: - Instance Methods
    
    func reset() {
        self.firstName = nil
        self.lastName = nil
        
        self.age = nil
        self.bio = nil
        
        self.classYear = nil
        
        self.schoolUID = nil
        self.schoolTitle = nil
        self.schoolLocation = nil
        
        self.gender = nil
        
        self.phoneNumbers = []
        
        self.hideTopPokesList = false
    }
}
