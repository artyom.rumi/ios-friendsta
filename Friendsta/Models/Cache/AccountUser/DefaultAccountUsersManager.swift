//
//  DefaultAccountUsersManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 07.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultAccountUsersManager: CoreDataModelManager, AccountUsersManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = AccountUser
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[AccountUser]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[AccountUser]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[AccountUser]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[AccountUser]>()
    fileprivate(set) lazy var objectsSchoolUpdatedEvent = Event<[AccountUser]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
 
    fileprivate func createPredicate(with uid: Int64) -> NSPredicate {
        return NSPredicate(format: "uid == %d", uid)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "uid", ascending: true)
    }
    
    func first(with uid: Int64) -> AccountUser? {
        return self.first(with: self.createPredicate(with: uid))
    }
    
    func last(with uid: Int64) -> AccountUser? {
        return self.last(with: self.createPredicate(with: uid))
    }
    
    func fetch(with uid: Int64) -> [AccountUser] {
        return self.fetch(with: self.createPredicate(with: uid))
    }
    
    func count(with uid: Int64) -> Int {
        return self.count(with: self.createPredicate(with: uid))
    }
    
    func clear(with uid: Int64) {
        self.clear(with: self.createPredicate(with: uid))
    }

    func modelContext(_ modelContext: ModelContext, didUpdateObjects objects: [ModelObject]) {
        let objects = self.filter(objects: objects)

        if !objects.isEmpty {
            self.objectsUpdatedEvent.emit(data: objects)

            let schoolUIDKeyPath = \AccountUser.schoolUID

            guard let schoolUIDPropertyName = schoolUIDKeyPath._kvcKeyPathString else {
                return
            }

            let updatedSchoolUIDObjects = objects.filter { $0.changedValues().keys.contains(schoolUIDPropertyName) }

            if !updatedSchoolUIDObjects.isEmpty {
                self.objectsSchoolUpdatedEvent.emit(data: updatedSchoolUIDObjects)
            }
        }
    }
}
