//
//  AccountUserExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 07.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension AccountUser {
    
    // MARK: - Instance Properties
    
    var avatarPlaceholderURL: URL? {
        get {
            if let avatarPlaceholderRawURL = self.avatarPlaceholderRawURL {
                return URL(string: avatarPlaceholderRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.avatarPlaceholderRawURL = newValue?.absoluteString
        }
    }
    
    var smallAvatarPlaceholderURL: URL? {
        get {
            if let avatarPlaceholderRawURL = self.smallAvatarPlaceholderRawURL {
                return URL(string: avatarPlaceholderRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.smallAvatarPlaceholderRawURL = newValue?.absoluteString
        }
    }
    
    var mediumAvatarPlaceholderURL: URL? {
        get {
            if let avatarPlaceholderRawURL = self.mediumAvatarPlaceholderRawURL {
                return URL(string: avatarPlaceholderRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.mediumAvatarPlaceholderRawURL = newValue?.absoluteString
        }
    }
    
    var largeAvatarPlaceholderURL: URL? {
        get {
            if let avatarPlaceholderRawURL = self.largeAvatarPlaceholderRawURL {
                return URL(string: avatarPlaceholderRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.largeAvatarPlaceholderRawURL = newValue?.absoluteString
        }
    }
    
    var avatarURL: URL? {
        get {
            if let avatarRawURL = self.avatarRawURL {
                return URL(string: avatarRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.avatarRawURL = newValue?.absoluteString
        }
    }
    
    var smallAvatarURL: URL? {
        get {
            if let avatarRawURL = self.smallAvatarRawURL {
                return URL(string: avatarRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.smallAvatarRawURL = newValue?.absoluteString
        }
    }
    
    var mediumAvatarURL: URL? {
        get {
            if let avatarRawURL = self.mediumAvatarRawURL {
                return URL(string: avatarRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.mediumAvatarRawURL = newValue?.absoluteString
        }
    }
    
    var largeAvatarURL: URL? {
        get {
            if let avatarRawURL = self.largeAvatarRawURL {
                return URL(string: avatarRawURL)
            } else {
                return nil
            }
        }
        
        set {
            self.largeAvatarRawURL = newValue?.absoluteString
        }
    }
    
    var avatar: Photo? {
        get {
            return Photo(imageURL: self.avatarURL,
                         smallImageURL: self.smallAvatarURL,
                         mediumImageURL: self.mediumAvatarURL,
                         largeImageURL: self.largeAvatarURL)
        }
        
        set {
            self.avatarURL = newValue?.imageURL
            
            self.smallAvatarURL = newValue?.smallImageURL
            self.mediumAvatarURL = newValue?.mediumImageURL
            self.largeAvatarURL = newValue?.largeImageURL
        }
    }
    
    var avatarPlaceholder: Photo? {
        get {
            return Photo(imageURL: self.avatarPlaceholderURL,
                         smallImageURL: self.smallAvatarPlaceholderURL,
                         mediumImageURL: self.mediumAvatarPlaceholderURL,
                         largeImageURL: self.largeAvatarPlaceholderURL)
        }
        
        set {
            self.avatarPlaceholderURL = newValue?.imageURL
            
            self.smallAvatarPlaceholderURL = newValue?.smallImageURL
            self.mediumAvatarPlaceholderURL = newValue?.mediumImageURL
            self.largeAvatarPlaceholderURL = newValue?.largeImageURL
        }
    }
    
    var fullName: String? {
        if let firstName = self.firstName, !firstName.isEmpty {
            if let lastName = self.lastName, !lastName.isEmpty {
                return "\(firstName) \(lastName)"
            } else {
                return firstName
            }
        } else {
            return self.lastName
        }
    }
    
    var gender: Gender {
        get {
            return Gender(rawValue: self.rawGender) ?? .nonBinary
        }
        
        set {
            self.rawGender = newValue.rawValue
        }
    }
    
    var link: String {
        return "\(Keys.webURL)\(self.uid)-\(self.firstName ?? "")-\(self.lastName ?? "")"
    }
}
