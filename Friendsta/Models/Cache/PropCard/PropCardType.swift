//
//  PropCardType.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 12/10/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum PropCardType: Int16 {
    
    // MARK: - Enumeration Cases
    
    case unknown
    case regular
    case custom
}
