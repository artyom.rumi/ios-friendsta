//
//  PropCardsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 12.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultPropCardsManager: CoreDataModelManager, PropCardsManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = PropCard
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[PropCard]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[PropCard]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[PropCard]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[PropCard]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with uid: Int64) -> NSPredicate {
        return NSPredicate(format: "uid == %d", uid)
    }
    
    // MARK: - PropCardsManager
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "createdDate", ascending: true)
    }
    
    func first(with uid: Int64) -> PropCard? {
        return self.first(with: self.createPredicate(with: uid))
    }
    
    func last(with uid: Int64) -> PropCard? {
        return self.last(with: self.createPredicate(with: uid))
    }
    
    func fetch(with uid: Int64) -> [PropCard] {
        return self.fetch(with: self.createPredicate(with: uid))
    }
    
    func count(with uid: Int64) -> Int {
        return self.count(with: self.createPredicate(with: uid))
    }
    
    func clear(with uid: Int64) {
        self.clear(with: self.createPredicate(with: uid))
    }
}
