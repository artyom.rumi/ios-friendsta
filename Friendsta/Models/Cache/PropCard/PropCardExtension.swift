//
//  PropCardExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 12.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension PropCard {
    
    // MARK: - Instance Properties
    
    var type: PropCardType {
        get {
            return PropCardType(rawValue: self.rawType) ?? .unknown
        }
        
        set {
            self.rawType = newValue.rawValue
        }
    }
    
    var senderGender: Gender {
        get {
            return Gender(rawValue: self.senderRawGender) ?? .nonBinary
        }
        
        set {
            self.senderRawGender = newValue.rawValue
        }
    }
    
    var isViewed: Bool {
        return (self.viewedDate != nil)
    }
}
