//
//  PropCardsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 12.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol PropCardsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[PropCard]> { get }
    var objectsAppendedEvent: Event<[PropCard]> { get }
    var objectsUpdatedEvent: Event<[PropCard]> { get }
    var objectsChangedEvent: Event<[PropCard]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> PropCard?
    func first() -> PropCard?
    
    func last(with uid: Int64) -> PropCard?
    func last() -> PropCard?
    
    func fetch(with uid: Int64) -> [PropCard]
    func fetch() -> [PropCard]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: PropCard)
    func append() -> PropCard
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
