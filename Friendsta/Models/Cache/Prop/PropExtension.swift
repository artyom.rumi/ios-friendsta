//
//  PropExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 24.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension Prop {
    
    // MARK: - Instance Properties
    
    var gender: Gender {
        get {
            return Gender(rawValue: self.rawGender) ?? .nonBinary
        }
        
        set {
            self.rawGender = newValue.rawValue
        }
    }
}
