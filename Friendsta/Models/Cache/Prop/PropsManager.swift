//
//  PropsManager.swift
//  Friendsta
//
//  Created by Marat Galeev on 28.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol PropsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[Prop]> { get }
    var objectsAppendedEvent: Event<[Prop]> { get }
    var objectsUpdatedEvent: Event<[Prop]> { get }
    var objectsChangedEvent: Event<[Prop]> { get }
    
    // MARK: - Instance Methods
    
    func first(with uid: Int64) -> Prop?
    func first() -> Prop?
    
    func last(with uid: Int64) -> Prop?
    func last() -> Prop?
    
    func fetch(with uid: Int64) -> [Prop]
    func fetch() -> [Prop]
    
    func count(with uid: Int64) -> Int
    func count() -> Int
    
    func clear(with uid: Int64)
    func clear()
    
    func remove(object: Prop)
    func append() -> Prop

    // MARK: -
    
    func startObserving()
    func stopObserving()
}
