//
//  DefaultChatListsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 21.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultChatListsManager: CoreDataModelManager, ChatListsManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = ChatListItem
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[ChatListItem]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[ChatListItem]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[ChatListItem]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[ChatListItem]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with listType: ChatListType) -> NSPredicate {
        return NSPredicate(format: "listRawType == %d", listType.rawValue)
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "sorting", ascending: true)
    }
    
    func first(with listType: ChatListType) -> ChatListItem? {
        return self.first(with: self.createPredicate(with: listType))
    }
    
    func last(with listType: ChatListType) -> ChatListItem? {
        return self.last(with: self.createPredicate(with: listType))
    }
    
    func fetch(with listType: ChatListType) -> [ChatListItem] {
        return self.fetch(with: self.createPredicate(with: listType))
    }
    
    func count(with listType: ChatListType) -> Int {
        return self.count(with: self.createPredicate(with: listType))
    }
    
    func clear(with listType: ChatListType) {
        self.clear(with: self.createPredicate(with: listType))
    }
}
