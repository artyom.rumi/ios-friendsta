//
//  ChatListsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 21.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol ChatListsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[ChatListItem]> { get }
    var objectsAppendedEvent: Event<[ChatListItem]> { get }
    var objectsUpdatedEvent: Event<[ChatListItem]> { get }
    var objectsChangedEvent: Event<[ChatListItem]> { get }
    
    // MARK: - Instance Methods
    
    func first(with listType: ChatListType) -> ChatListItem?
    func first() -> ChatListItem?
    
    func last(with listType: ChatListType) -> ChatListItem?
    func last() -> ChatListItem?
    
    func fetch(with listType: ChatListType) -> [ChatListItem]
    func fetch() -> [ChatListItem]
    
    func count(with listType: ChatListType) -> Int
    func count() -> Int
    
    func clear(with listType: ChatListType)
    func clear()
    
    func remove(object: ChatListItem)
    func append() -> ChatListItem
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
