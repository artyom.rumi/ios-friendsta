//
//  ChatListType.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum ChatListType: Int16 {
    
    // MARK: - Enumeration Cases
    
    case all
}
