//
//  ChatListItemExtension.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension ChatListItem {
    
    // MARK: - Instance Properties
    
    var listType: ChatListType? {
        get {
            return ChatListType(rawValue: self.listRawType)
        }
        
        set {
            if let newValue = newValue {
                self.listRawType = newValue.rawValue
            } else {
                self.listRawType = -1
            }
        }
    }
}
