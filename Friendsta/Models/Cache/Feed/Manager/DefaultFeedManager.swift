//
//  DefaultFeedManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultFeedManager: CoreDataModelManager, FeedManager, CacheModelContextObserver {

    // MARK: - Nested Types

    typealias Object = Feed

    // MARK: - Instance Properties

    unowned let context: CoreDataModelContext

    fileprivate(set) lazy var objectsRemovedEvent = Event<[Feed]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[Feed]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[Feed]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[Feed]>()

    // MARK: - Initializers

    init(context: CoreDataModelContext) {
        self.context = context
    }

    // MARK: - Instance Methods

    fileprivate func createPredicate(with uid: Int64) -> NSPredicate {
        return NSPredicate(format: "uid == %d", uid)
    }

    // MARK: -

    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "uid", ascending: true)
    }

    func first(with uid: Int64) -> Feed? {
        return self.first(with: self.createPredicate(with: uid))
    }

    func last(with uid: Int64) -> Feed? {
        return self.last(with: self.createPredicate(with: uid))
    }

    func fetch(with uid: Int64) -> [Feed] {
        return self.fetch(with: self.createPredicate(with: uid))
    }

    func count(with uid: Int64) -> Int {
        return self.count(with: self.createPredicate(with: uid))
    }

    func clear(with uid: Int64) {
        self.clear(with: self.createPredicate(with: uid))
    }

    func append(withUID uid: Int64) -> Feed {
        let feed = self.append()

        feed.uid = uid

        return feed
    }
}
