//
//  FeedManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol FeedManager {

    // MARK: - Instance Properties

    var objectsRemovedEvent: Event<[Feed]> { get }
    var objectsAppendedEvent: Event<[Feed]> { get }
    var objectsUpdatedEvent: Event<[Feed]> { get }
    var objectsChangedEvent: Event<[Feed]> { get }

    // MARK: - Instance Methods

    func firstOrNew(withUID uid: Int64) -> Feed

    func first(with uid: Int64) -> Feed?
    func first() -> Feed?

    func last(with uid: Int64) -> Feed?
    func last() -> Feed?

    func fetch(with uid: Int64) -> [Feed]
    func fetch() -> [Feed]

    func count(with uid: Int64) -> Int
    func count() -> Int

    func clear(with uid: Int64)
    func clear()

    func remove(object: Feed)

    func append(withUID uid: Int64) -> Feed
    func append() -> Feed

    // MARK: -

    func startObserving()
    func stopObserving()
}

// MARK: -

extension FeedManager {

    // MARK: - Instance Methods

    func firstOrNew(withUID uid: Int64) -> Feed {
        return self.first(with: uid) ?? self.append()
    }
}
