//
//  FeedExtension.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension Feed {

    // MARK: - Nested Types

    typealias UID = Int64

    // MARK: - Instance Properties

    var originalImageURL: URL? {
        get {
            if let rawOriginalImageURL = self.rawOriginalImageURL {
                return URL(string: rawOriginalImageURL)
            } else {
                return nil
            }
        }

        set {
            self.rawOriginalImageURL = newValue?.absoluteString
        }
    }

    var photo: Photo? {
        get {
            guard let rawOriginalImageURL = self.rawOriginalImageURL,
                let rawSmallImageURL = self.rawSmallImageURL,
                let rawMediumImageURL = self.rawMediumImageURL,
                let rawLargeImageURL = self.rawLargeImageURL else {
                return nil
            }

            return Photo(imageURL: URL(string: rawOriginalImageURL),
                         smallImageURL: URL(string: rawSmallImageURL),
                         mediumImageURL: URL(string: rawMediumImageURL),
                         largeImageURL: URL(string: rawLargeImageURL))
        }

        set {
            self.rawOriginalImageURL = newValue?.imageURL?.absoluteString
            self.rawSmallImageURL = newValue?.smallImageURL?.absoluteString
            self.rawMediumImageURL = newValue?.mediumImageURL?.absoluteString
            self.rawLargeImageURL = newValue?.largeImageURL?.absoluteString
        }
    }
}
