//
//  SchoolListType.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum SchoolListType {
    
    // MARK: - Enumeration Cases
    
    case unknown
    case nearby(classYear: Int)
    case all(classYear: Int)
    
    // MARK: - Instance Properties
    
    var rawValue: Int16 {
        switch self {
        case .unknown:
            return 0
            
        case .nearby:
            return 1
            
        case .all:
            return 2
        }
    }
    
    var classYear: Int {
        switch self {
        case .unknown:
            return 0
            
        case .nearby(let classYear):
            return classYear
            
        case .all(let classYear):
            return classYear
        }
    }
    
    // MARK: - Initializers
    
    init?(rawValue: Int16, classYear: Int) {
        switch rawValue {
        case 0:
            self = .unknown
            
        case 1:
            self = .nearby(classYear: classYear)
        
        case 2:
            self = .all(classYear: classYear)
            
        default:
            return nil
        }
    }
}

// MARK: - Equatable

extension SchoolListType: Equatable {
    
    // MARK: - Type Methods
    
    static func == (left: SchoolListType, right: SchoolListType) -> Bool {
        return ((left.rawValue == right.rawValue) && (left.classYear == right.classYear))
    }
    
    static func != (left: SchoolListType, right: SchoolListType) -> Bool {
        return !(left == right)
    }
    
    static func ~= (left: SchoolListType, right: SchoolListType) -> Bool {
        return (left == right)
    }
}

// MARK: - Hashable

extension SchoolListType: Hashable {
    
    // MARK: - Instance Properties
    
    var hashValue: Int {
        switch self {
        case .unknown:
            return 0
            
        case .nearby:
            return classYear
            
        case .all(let classYear):
            return classYear << 15
        }
    }
}
