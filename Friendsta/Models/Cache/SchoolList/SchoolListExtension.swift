//
//  SchoolList.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.08.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

extension SchoolList {
    
    // MARK: - Instance Properties
    
    var listType: SchoolListType {
        get {
            return SchoolListType(rawValue: self.listRawType, classYear: Int(self.classYear)) ?? .unknown
        }
        
        set {
            self.listRawType = newValue.rawValue
            self.classYear = Int16(newValue.classYear)
        }
    }
    
    var location: Location {
        get {
            return Location(latitude: self.locationLatitude, longitude: self.locationLongitude)
        }
        
        set {
            self.locationLatitude = newValue.latitude
            self.locationLongitude = newValue.longitude
        }
    }
    
    var count: Int {
        return self.rawSchools?.count ?? 0
    }
    
    var isEmpty: Bool {
        // swiftlint:disable empty_count
        return self.count == 0
    }
    
    // MARK: -
    
    subscript(index: Int) -> School {
        return self.rawSchools![index] as! School
    }
    
    // MARK: - Instance Subscripts
    
    func insert(school: School, at index: Int) {
        self.insertIntoRawSchools(school, at: index)
    }
    
    func removeSchool(at index: Int) -> School {
        let school = self.rawSchools![index] as! School
        
        self.removeFromRawSchools(at: index)
        
        return school
    }
    
    func append(school: School) {
        self.addToRawSchools(school)
    }
    
    func remove(school: School) {
        self.removeFromRawSchools(school)
    }
    
    func clearSchools() {
        self.nextPage = 1
        
        if let schools = self.rawSchools {
            self.removeFromRawSchools(schools)
        }
    }
}
