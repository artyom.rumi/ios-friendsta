//
//  DefaultSchoolListsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.08.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultSchoolListsManager: CoreDataModelManager, SchoolListsManager, CacheModelContextObserver {
    
    // MARK: - Nested Types
    
    typealias Object = SchoolList
    
    // MARK: - Instance Properties
    
    unowned let context: CoreDataModelContext
    
    fileprivate(set) lazy var objectsRemovedEvent = Event<[SchoolList]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[SchoolList]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[SchoolList]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[SchoolList]>()
    
    // MARK: - Initializers
    
    init(context: CoreDataModelContext) {
        self.context = context
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createPredicate(with listType: SchoolListType) -> NSPredicate {
        return NSCompoundPredicate(andPredicateWithSubpredicates: [NSPredicate(format: "listRawType == %d", listType.rawValue),
                                                                   NSPredicate(format: "classYear == %d", listType.classYear)])
    }
    
    // MARK: -
    
    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "listRawType", ascending: true)
    }
    
    func firstOrNew(with listType: SchoolListType) -> SchoolList {
        let object = self.first(with: listType) ?? self.append()
        
        object.listType = listType
        
        return object
    }
    
    func lastOrNew(with listType: SchoolListType) -> SchoolList {
        let object = self.last(with: listType) ?? self.append()
        
        object.listType = listType
        
        return object
    }
    
    func first(with listType: SchoolListType) -> SchoolList? {
        return self.first(with: self.createPredicate(with: listType))
    }
    
    func last(with listType: SchoolListType) -> SchoolList? {
        return self.last(with: self.createPredicate(with: listType))
    }
    
    func fetch(with listType: SchoolListType) -> [SchoolList] {
        return self.fetch(with: self.createPredicate(with: listType))
    }
    
    func count(with listType: SchoolListType) -> Int {
        return self.count(with: self.createPredicate(with: listType))
    }
    
    func clear(with listType: SchoolListType) {
        self.clear(with: self.createPredicate(with: listType))
    }
}
