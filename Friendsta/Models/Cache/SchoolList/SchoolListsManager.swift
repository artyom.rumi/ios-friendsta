//
//  SchoolListsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.08.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol SchoolListsManager {
    
    // MARK: - Instance Properties
    
    var objectsRemovedEvent: Event<[SchoolList]> { get }
    var objectsAppendedEvent: Event<[SchoolList]> { get }
    var objectsUpdatedEvent: Event<[SchoolList]> { get }
    var objectsChangedEvent: Event<[SchoolList]> { get }
    
    // MARK: - Instance Methods
    
    func firstOrNew(with listType: SchoolListType) -> SchoolList
    func firstOrNew() -> SchoolList

    func lastOrNew(with listType: SchoolListType) -> SchoolList
    func lastOrNew() -> SchoolList

    func first(with listType: SchoolListType) -> SchoolList?
    func first() -> SchoolList?
    
    func last(with listType: SchoolListType) -> SchoolList?
    func last() -> SchoolList?
    
    func fetch(with listType: SchoolListType) -> [SchoolList]
    func fetch() -> [SchoolList]

    func count(with listType: SchoolListType) -> Int
    func count() -> Int
    
    func clear(with listType: SchoolListType)
    func clear()
    
    func remove(object: SchoolList)
    func append() -> SchoolList
    
    // MARK: -
    
    func startObserving()
    func stopObserving()
}
