//
//  UserListManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 30/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol UserListManager {

    // MARK: - Instance Properties

    var objectsRemovedEvent: Event<[UserList]> { get }
    var objectsAppendedEvent: Event<[UserList]> { get }
    var objectsUpdatedEvent: Event<[UserList]> { get }
    var objectsChangedEvent: Event<[UserList]> { get }

    // MARK: - Instance Methods

    func firstOrNew(withListType listType: UserListType) -> UserList

    func first(with listType: UserListType) -> UserList?
    func first() -> UserList?

    func last(with listType: UserListType) -> UserList?
    func last() -> UserList?

    func fetch(with listType: UserListType) -> [UserList]
    func fetch() -> [UserList]

    func count(with listType: UserListType) -> Int
    func count() -> Int

    func clear(with listType: UserListType)
    func clear()

    func remove(object: UserList)

    func append(withListType listType: UserListType) -> UserList
    func append() -> UserList

    // MARK: -

    func startObserving()
    func stopObserving()
}

// MARK: -

extension UserListManager {

    // MARK: - Instance Methods

    func firstOrNew(withListType listType: UserListType) -> UserList {
        return self.first(with: listType) ?? self.append(withListType: listType)
    }
}
