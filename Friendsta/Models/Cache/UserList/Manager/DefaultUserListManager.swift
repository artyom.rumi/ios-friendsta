//
//  DefaultUserListManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 30/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultUserListManager: CoreDataModelManager, UserListManager, CacheModelContextObserver {

    // MARK: - Nested Types

    typealias Object = UserList

    // MARK: - Instance Properties

    unowned let context: CoreDataModelContext

    fileprivate(set) lazy var objectsRemovedEvent = Event<[UserList]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[UserList]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[UserList]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[UserList]>()

    // MARK: - Initializers

    init(context: CoreDataModelContext) {
        self.context = context
    }

    // MARK: - Instance Methods

    fileprivate func createPredicate(with listType: UserListType) -> NSPredicate {
        return NSPredicate(format: "listRawType == %d", listType.rawValue)
    }

    // MARK: -

    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "listRawType", ascending: true)
    }

    func first(with listType: UserListType) -> UserList? {
        return self.first(with: self.createPredicate(with: listType))
    }

    func last(with listType: UserListType) -> UserList? {
        return self.last(with: self.createPredicate(with: listType))
    }

    func fetch(with listType: UserListType) -> [UserList] {
        return self.fetch(with: self.createPredicate(with: listType))
    }

    func count(with listType: UserListType) -> Int {
        return self.count(with: self.createPredicate(with: listType))
    }

    func clear(with listType: UserListType) {
        self.clear(with: self.createPredicate(with: listType))
    }

    func append(withListType listType: UserListType) -> UserList {
        let userList = self.append()

        userList.listType = listType

        return userList
    }
}
