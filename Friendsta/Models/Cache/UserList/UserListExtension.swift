//
//  UserListExtension.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 30/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension UserList {

    // MARK: - Instance Properties

    var listType: UserListType {
        get {
            return UserListType(rawValue: self.listRawType) ?? .unknown
        }

        set {
            self.listRawType = newValue.rawValue
        }
    }

    var count: Int {
        return self.rawUsers?.count ?? 0
    }

    var allUsers: [User] {
        return (self.rawUsers?.array as? [User]) ?? []
    }

    var hasNextPage: Bool {
        return self.totalCount != self.allUsers.count && self.nextPage > 0
    }

    // MARK: - Instance Subscripts

    subscript(index: Int) -> User {
        return self.rawUsers![index] as! User
    }

    // MARK: - Instance Methods

    func clearUsers() {
        if let users = self.rawUsers {
            self.removeFromRawUsers(users)
        }
    }
}

// MARK: - ConnectionList

extension UserList: ConnectionList {

    // MARK: - Instance Properties

    var itemCount: Int {
        return self.allUsers.count
    }

    var moreCount: Int {
        return Int(self.totalCount) - self.itemCount
    }
}
