//
//  UserListType.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

indirect enum UserListType: Equatable {

    // MARK: - Enumeration Cases

    case unknown

    case invitationInlineContacts
    case invitationSchoolmates

    case friends
    case invited
    case expired
    case pokeInlineContacts
    case pokeSchoolmates

    case all(type: UserListType)

    // MARK: - Instance Properties

    var rawValue: Int16 {
        switch self {
        case .unknown:
            return 0

        case .invitationInlineContacts:
            return 1

        case .invitationSchoolmates:
            return 2

        case .friends:
            return 3

        case .invited:
            return 4

        case .expired:
            return 5

        case .pokeInlineContacts:
            return 6

        case .pokeSchoolmates:
            return 7

        case .all(let type):
            return 8 + type.rawValue
        }
    }

    // MARK: - Initializers

    init?(rawValue: Int16) {
        switch rawValue {
        case 0:
            self = .unknown

        case 1:
            self = .invitationInlineContacts

        case 2:
            self = .invitationSchoolmates

        case 3:
            self = .friends

        case 4:
            self = .invited

        case 5:
            self = .expired

        case 6:
            self = .pokeInlineContacts

        case 7:
            self = .pokeSchoolmates

        default:
            let rawUserListType = rawValue - 8

            if let userListType = UserListType(rawValue: rawUserListType) {
                self = .all(type: userListType)
            } else {
                return nil
            }
        }
    }
}
