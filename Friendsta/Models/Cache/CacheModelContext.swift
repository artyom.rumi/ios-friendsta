//
//  CacheModelContext.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 09.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol CacheModelContext: ModelContext {
    
    // MARK: - Instance Properties
    
    var accountUsersManager: AccountUsersManager { get }
    
    var usersManager: UsersManager { get }
    var contactsManager: ContactsManager { get }
    var outlineContactsManager: OutlineContactsManager { get }
    var schoolsManager: SchoolsManager { get }
    var customPropsManager: CustomPropsManager { get }
    var propsManager: PropsManager { get }
    var feedManager: FeedManager { get }
    var commentManager: CommentManager { get }

    var contactListsManager: ContactListsManager { get }
    var schoolListsManager: SchoolListsManager { get }
    var propListsManager: PropListsManager { get }
    
    var propRequestsManager: PropRequestsManager { get }
    var propReceiversManager: PropReceiversManager { get }
    var propReactionsManager: PropReactionsManager { get }
    var propCardsManager: PropCardsManager { get }
    var propCardListManager: PropCardListManager { get }
    var feedListManager: FeedListManager { get }
    var userListManager: UserListManager { get }
    var outlineContactListManager: OutlineContactListManager { get }
    var commentListManager: CommentListManager { get }
    
    var chatsManager: ChatsManager { get }
    
    var chatMasksManager: ChatMasksManager { get }
    var chatMembersManager: ChatMembersManager { get }
    
    var chatMessagesManager: ChatMessagesManager { get }
    
    var chatTextMessagesManager: ChatTextMessagesManager { get }
    var chatPhotoMessagesManager: ChatPhotoMessagesManager { get }
    var chatInfoMessagesManager: ChatInfoMessagesManager { get }
    
    var chatListsManager: ChatListsManager { get }

    var activityManager: ActivityManager { get }
}

extension CacheModelContext {
    
    // MARK: - Instance Methods
    
    func clear() {
        self.accountUsersManager.clear()
        
        self.usersManager.clear()
        self.contactsManager.clear()
        self.schoolsManager.clear()
        self.propsManager.clear()
        self.customPropsManager.clear()
        self.feedManager.clear()
        self.commentManager.clear()

        self.contactListsManager.clear()
        self.schoolListsManager.clear()
        self.propListsManager.clear()
        
        self.propRequestsManager.clear()
        self.propReceiversManager.clear()
        self.propReactionsManager.clear()
        self.propCardsManager.clear()
        self.propCardListManager.clear()
        self.feedListManager.clear()
        self.userListManager.clear()
        self.outlineContactListManager.clear()
        self.commentListManager.clear()
        
        self.chatsManager.clear()
        
        self.chatMasksManager.clear()
        self.chatMembersManager.clear()
        
        self.chatMessagesManager.clear()
        
        self.chatTextMessagesManager.clear()
        self.chatPhotoMessagesManager.clear()
        self.chatInfoMessagesManager.clear()
        
        self.chatListsManager.clear()

        self.activityManager.clear()
        
        self.save()
    }
}
