//
//  DefaultCacheModel.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 08.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

final class DefaultCacheModel: CacheModel {
    
    // MARK: - Type Properties
    
    static let shared = DefaultCacheModel(database: DefaultCacheModelDatabase(identifier: "CacheModel"))
    
    // MARK: - Instance Properties
    
    let database: DefaultCacheModelDatabase
    
    // MARK: - CacheModel
    
    var identifier: String {
        return self.database.identifier
    }
    
    var viewContext: CacheModelContext {
        return self.database.viewContext
    }
    
    // MARK: - Initializers
    
    init(database: DefaultCacheModelDatabase) {
        self.database = database
    }
}
