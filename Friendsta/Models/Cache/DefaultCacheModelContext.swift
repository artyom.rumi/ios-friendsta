//
//  DefaultCacheModelContext.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 08.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

final class DefaultCacheModelContext: CoreDataModelContext, CacheModelContext {
    
    // MARK: - Instance Properties
        
    private(set) lazy var accountUsersManager: AccountUsersManager = {
        return DefaultAccountUsersManager(context: self)
    }()

    private(set) lazy var usersManager: UsersManager = {
        return DefaultUsersManager(context: self)
    }()

    private(set) lazy var contactsManager: ContactsManager = {
        return DefaultContactsManager(context: self)
    }()
    
    private(set) lazy var outlineContactsManager: OutlineContactsManager = {
        return DefaultOutlineContactsManager(context: self)
    }()
    
    private(set) lazy var schoolsManager: SchoolsManager = {
        return DefaultSchoolsManager(context: self)
    }()
    
    private(set) lazy var propsManager: PropsManager = {
        return DefaultPropsManager(context: self)
    }()
    
    private(set) lazy var customPropsManager: CustomPropsManager = {
        return DefaultCustomPropsManager(context: self)
    }()

    private(set) lazy var feedManager: FeedManager = {
        return DefaultFeedManager(context: self)
    }()

    private(set) lazy var commentManager: CommentManager = {
        return DefaultCommentManager(context: self)
    }()

    // MARK: -

    private(set) lazy var contactListsManager: ContactListsManager = {
        return DefaultContactListsManager(context: self)
    }()
    
    private(set) lazy var schoolListsManager: SchoolListsManager = {
        return DefaultSchoolListsManager(context: self)
    }()
    
    private(set) lazy var propListsManager: PropListsManager = {
        return DefaultPropListsManager(context: self)
    }()

    private(set) lazy var feedListManager: FeedListManager = {
        return DefaultFeedListManager(context: self)
    }()

    private(set) lazy var userListManager: UserListManager = {
        return DefaultUserListManager(context: self)
    }()

    private(set) lazy var outlineContactListManager: OutlineContactListManager = {
        return DefaultOutlineContactListManager(context: self)
    }()

    private(set) lazy var commentListManager: CommentListManager = {
        return DefaultCommentListManager(context: self)
    }()

    // MARK: -
    
    private(set) lazy var propRequestsManager: PropRequestsManager = {
        return DefaultPropRequestsManager(context: self)
    }()

    private(set) lazy var propReceiversManager: PropReceiversManager = {
        return DefaultPropReceiversManager(context: self)
    }()
    
    private(set) lazy var propReactionsManager: PropReactionsManager = {
        return DefaultPropReactionsManager(context: self)
    }()

    private(set) lazy var propCardsManager: PropCardsManager = {
        return DefaultPropCardsManager(context: self)
    }()
    
    private(set) lazy var propCardListManager: PropCardListManager = {
        return DefaultPropCardListManager(context: self)
    }()
    
    private(set) lazy var chatsManager: ChatsManager = {
        return DefaultChatsManager(context: self)
    }()
    
    private(set) lazy var chatMasksManager: ChatMasksManager = {
        return DefaultChatMasksManager(context: self)
    }()
    
    private(set) lazy var chatMembersManager: ChatMembersManager = {
        return DefaultChatMembersManager(context: self)
    }()
    
    private(set) lazy var chatMessagesManager: ChatMessagesManager = {
        return DefaultChatMessagesManager(context: self)
    }()
    
    private(set) lazy var chatTextMessagesManager: ChatTextMessagesManager = {
        return DefaultChatTextMessagesManager(context: self)
    }()
    
    private(set) lazy var chatPhotoMessagesManager: ChatPhotoMessagesManager = {
        return DefaultChatPhotoMessagesManager(context: self)
    }()
    
    private(set) lazy var chatInfoMessagesManager: ChatInfoMessagesManager = {
        return DefaultChatInfoMessagesManager(context: self)
    }()
    
    private(set) lazy var chatListsManager: ChatListsManager = {
        return DefaultChatListsManager(context: self)
    }()

    private(set) lazy var activityManager: ActivityManager = {
        return DefaultActivityManager(context: self)
    }()
}
