//
//  CacheModelDatabase.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 09.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol CacheModelDatabase: ModelDatabase where Context: CacheModelContext { }
