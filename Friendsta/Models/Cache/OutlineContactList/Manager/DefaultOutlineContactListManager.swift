//
//  DefaultOutlineContactListManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 30/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

final class DefaultOutlineContactListManager: CoreDataModelManager, OutlineContactListManager, CacheModelContextObserver {

    // MARK: - Nested Types

    typealias Object = OutlineContactList

    // MARK: - Instance Properties

    unowned let context: CoreDataModelContext

    fileprivate(set) lazy var objectsRemovedEvent = Event<[OutlineContactList]>()
    fileprivate(set) lazy var objectsAppendedEvent = Event<[OutlineContactList]>()
    fileprivate(set) lazy var objectsUpdatedEvent = Event<[OutlineContactList]>()
    fileprivate(set) lazy var objectsChangedEvent = Event<[OutlineContactList]>()

    // MARK: - Initializers

    init(context: CoreDataModelContext) {
        self.context = context
    }

    // MARK: - Instance Methods

    fileprivate func createPredicate(with listType: OutlineContactListType) -> NSPredicate {
        return NSPredicate(format: "listRawType == %d", listType.rawValue)
    }

    // MARK: -

    func createDefaultSortDescriptor() -> NSSortDescriptor {
        return NSSortDescriptor(key: "listRawType", ascending: true)
    }

    func first(with listType: OutlineContactListType) -> OutlineContactList? {
        return self.first(with: self.createPredicate(with: listType))
    }

    func last(with listType: OutlineContactListType) -> OutlineContactList? {
        return self.last(with: self.createPredicate(with: listType))
    }

    func fetch(with listType: OutlineContactListType) -> [OutlineContactList] {
        return self.fetch(with: self.createPredicate(with: listType))
    }

    func count(with listType: OutlineContactListType) -> Int {
        return self.count(with: self.createPredicate(with: listType))
    }

    func clear(with listType: OutlineContactListType) {
        self.clear(with: self.createPredicate(with: listType))
    }

    func append(withListType listType: OutlineContactListType) -> OutlineContactList {
        let feedList = self.append()

        feedList.listType = listType

        return feedList
    }
}
