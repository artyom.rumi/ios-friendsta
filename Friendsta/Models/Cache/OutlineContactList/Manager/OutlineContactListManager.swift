//
//  OutlineContactListManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 30/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

protocol OutlineContactListManager {

    // MARK: - Instance Properties

    var objectsRemovedEvent: Event<[OutlineContactList]> { get }
    var objectsAppendedEvent: Event<[OutlineContactList]> { get }
    var objectsUpdatedEvent: Event<[OutlineContactList]> { get }
    var objectsChangedEvent: Event<[OutlineContactList]> { get }

    // MARK: - Instance Methods

    func firstOrNew(withListType listType: OutlineContactListType) -> OutlineContactList

    func first(with listType: OutlineContactListType) -> OutlineContactList?
    func first() -> OutlineContactList?

    func last(with listType: OutlineContactListType) -> OutlineContactList?
    func last() -> OutlineContactList?

    func fetch(with listType: OutlineContactListType) -> [OutlineContactList]
    func fetch() -> [OutlineContactList]

    func count(with listType: OutlineContactListType) -> Int
    func count() -> Int

    func clear(with listType: OutlineContactListType)
    func clear()

    func remove(object: OutlineContactList)

    func append(withListType listType: OutlineContactListType) -> OutlineContactList
    func append() -> OutlineContactList

    // MARK: -

    func startObserving()
    func stopObserving()
}

// MARK: -

extension OutlineContactListManager {

    // MARK: - Instance Methods

    func firstOrNew(withListType listType: OutlineContactListType) -> OutlineContactList {
        return self.first(with: listType) ?? self.append(withListType: listType)
    }
}
