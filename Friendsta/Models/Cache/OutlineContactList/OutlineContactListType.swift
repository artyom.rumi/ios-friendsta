//
//  OutlineContactListType.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 30/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

enum OutlineContactListType: Int16 {

    // MARK: - Enumeration Cases

    case unknown
    case all
    case paginal
}
