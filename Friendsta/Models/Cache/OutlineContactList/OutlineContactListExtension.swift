//
//  OutlineContactListExtension.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 30/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension OutlineContactList {

    // MARK: - Instance Properties

    var listType: OutlineContactListType {
        get {
            return OutlineContactListType(rawValue: self.listRawType) ?? .unknown
        }

        set {
            self.listRawType = newValue.rawValue
        }
    }

    var count: Int {
        return self.rawOutlineContacts?.count ?? 0
    }

    var allOutlineContacts: [OutlineContact] {
        return (self.rawOutlineContacts?.array as? [OutlineContact]) ?? []
    }

    var hasNextPage: Bool {
        return self.totalCount != self.allOutlineContacts.count && self.nextPage > 0
    }

    // MARK: - Instance Subscripts

    subscript(index: Int) -> OutlineContact {
        return self.rawOutlineContacts![index] as! OutlineContact
    }

    // MARK: - Instance Methods

    func clearOutlineContacts() {
        if let outlineContacts = self.rawOutlineContacts {
            self.removeFromRawOutlineContacts(outlineContacts)
        }
    }
}

// MARK: - ConnectionList

extension OutlineContactList: ConnectionList {

    // MARK: - Instance Properties

    var itemCount: Int {
        return self.allOutlineContacts.count
    }

    var moreCount: Int {
        return Int(self.totalCount) - self.itemCount
    }
}
