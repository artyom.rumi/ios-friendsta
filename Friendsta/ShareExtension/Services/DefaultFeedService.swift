//
//  DefaultFeedService.swift
//  FriendstaStagingShareExtension
//
//  Created by Elina Batyrova on 30/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaNetwork
import PromiseKit
import MobileCoreServices

struct DefaultFeedService: FeedService {
    
    // MARK: - Instance Properties
    
    let apiWebService: APIWebService
    
    // MARK: - Instance Methods
    
    private func createProgressiveImage(from photo: UIImage) -> Data? {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        
        guard let imageURL = documentsURL?.appendingPathComponent("progressive.jpg") else {
            return nil
        }
        
        let targetURL = imageURL as CFURL
        
        guard let destination = CGImageDestinationCreateWithURL(targetURL, kUTTypeJPEG, 1, nil) else {
            return nil
        }
        
        guard let cfBooleanTrue = kCFBooleanTrue else {
            return nil
        }
        
        let jfifProperties = [kCGImagePropertyJFIFIsProgressive: cfBooleanTrue] as NSDictionary
        
        let properties = [
            kCGImageDestinationLossyCompressionQuality: Limits.photoImageQuality,
            kCGImagePropertyJFIFDictionary: jfifProperties
            ] as NSDictionary
        
        guard let cgImage = photo.cgImage else {
            return nil
        }
        
        CGImageDestinationAddImage(destination, cgImage, properties)
        CGImageDestinationFinalize(destination)
        
        return try? Data(contentsOf: imageURL)
    }
    
    // MARK: - FeedService
    
    func sendFeed(photo: UIImage, text: String? = nil) -> Promise<Void> {
        return Promise(resolver: { seal in
            guard let data = self.createProgressiveImage(from: photo) else {
                return seal.reject(WebError.aborted)
            }
            
            let multiPartItem = WebMultiPartDataItem(data: data,
                                                     name: "image",
                                                     fileName: "feed.jpg",
                                                     mimeType: "image/jpg")
            firstly {
                self.apiWebService.jsonObject(uploadingMultiPart: [multiPartItem], params: ["text": text ?? ""], to: "v1/my/feeds")
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func sendFeed(text: String?, link: URL) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                               path: "v1/my/feeds",
                                                               params: ["text": text ?? "", "link": link]))
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                seal.reject(error)
            }
        })
    }
}
