//
//  FeedService.swift
//  FriendstaStagingShareExtension
//
//  Created by Elina Batyrova on 30/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol FeedService {
    
    // MARK: - Instance Methods
    
    func sendFeed(photo: UIImage, text: String?) -> Promise<Void>
    func sendFeed(text: String?, link: URL) -> Promise<Void>
}

// MARK: -

extension FeedService {
    
    // MARK: - Instance Methods
    
    func sendFeed(photo: UIImage, text: String? = nil) -> Promise<Void> {
        return self.sendFeed(photo: photo, text: text)
    }
}
