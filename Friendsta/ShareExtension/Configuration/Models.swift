//
//  Models.swift
//  FriendstaStagingShareExtension
//
//  Created by Elina Batyrova on 30/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaNetwork

enum Models {
    
    // MARK: - Type Properties
    
    static let account: AccountModel = DefaultAccountModel(userDefaultsManager: Managers.userDefaults,
                                                           keychainWrapperManager: Managers.keychainWrapper)
}
