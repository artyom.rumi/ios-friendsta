//
//  Limits.swift
//  FriendstaStagingShareExtension
//
//  Created by Elina Batyrova on 30/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

enum Limits {
    
    // MARK: - Type Properties

    static let photoImageQuality: CGFloat = 0.8
}
