//
//  Services.swift
//  FriendstaStagingShareExtension
//
//  Created by Elina Batyrova on 30/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaNetwork

enum Services {
    
    // MARK: - Type Properties
    
    static let accountProvider: AccountProvider = DefaultAccountProvider(model: Models.account)
    
    static let webRequestAdapter: WebRequestAdapter = DefaultAPIWebRequestAdapter(serverBaseURL: Keys.apiServerBaseURL,
                                                                                  accountProvider: Services.accountProvider)
    
    static let webService: WebService = AlamofireWebService(serverBaseURL: Keys.apiServerBaseURL,
                                                            requestAdapter: Services.webRequestAdapter,
                                                            activityIndicator: UIWebActivityIndicator.shared)
    
    static let apiWebService: APIWebService = DefaultAPIWebService(accountProvider: Services.accountProvider,
                                                                   webService: Services.webService)
    
    static let feedService: FeedService = DefaultFeedService(apiWebService: Services.apiWebService)
}
