//
//  ShareViewController.swift
//  ShareExtension
//
//  Created by Elina Batyrova on 25/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import Social
import MobileCoreServices
import PromiseKit
import FriendstaNetwork
import FriendstaTools
import SVProgressHUD

class ShareViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    private enum Identifiers {
        
        // MARK: - Type Properties
        
        static let imageType = "public.image"
        static let jpegType = "public.jpeg"
        static let pngType = "public.png"
        static let URLType = "public.url"
        static let textType = "public.plain-text"
    }
    
    // MARK: -
    
    private enum ShareContent {
        
        // MARK: - Enumeration Cases
        
        case link
        case photo
        case textOnly
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let successMessageText = "Posted!"
        
        static let linkPreviewHeight: CGFloat = 180
        static let modalPresentationTopDistanceHeight: CGFloat = 24
        static let cornerRadius: CGFloat = 12
        
        static let textFeedBackgroundImage = UIImage(named: "InboxBackground")
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var contentView: UIView!
    @IBOutlet private weak var contentViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var textViewPlaceholderLabel: UILabel!
    
    @IBOutlet private weak var photoPreviewImageView: UIImageView!
    
    @IBOutlet private weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var photoImageHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var linkInformationView: UIView!
    @IBOutlet private weak var linkDescriptionLabel: UILabel!
    @IBOutlet private weak var linkLabel: UILabel!
    
    @IBOutlet private weak var loadingPreviewView: UIView!
    
    // MARK: -
    
    private var sharingPhoto: UIImage?
    private var sharingURL: URL?
    
    private var shareContent: ShareContent?
    
    // MARK: - Instance Methods
    
    @IBAction private func onCloseButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onCloseButtonTouchUpInside()", from: self)
        
        self.closeController()
    }
    
    @IBAction private func onPostButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onPostButtonTouchUpInside()", from: self)
        
        guard let shareContent = self.shareContent else {
            return
        }
        
        let loadingViewController = LoadingViewController()
        
        self.textView.resignFirstResponder()
        
        switch shareContent {
        case .photo:
            guard let photo = self.sharingPhoto else {
                return
            }
            
            self.present(loadingViewController, animated: true, completion: {
                self.share(photo: photo,
                           text: self.textView.text,
                           loadingViewController: loadingViewController)
            })
        
        case .link:
            guard let link = self.sharingURL else {
                return
            }
            
            self.present(loadingViewController, animated: true, completion: {
                self.share(link: link,
                           text: self.textView.text,
                           loadingViewController: loadingViewController)
            })
            
        case .textOnly:
            if let text = self.textView.text, !text.isEmpty {
                let textFeedPhoto = self.renderPhoto(with: text)
                
                self.present(loadingViewController, animated: true, completion: {
                    self.share(photo: textFeedPhoto,
                               text: nil,
                               loadingViewController: loadingViewController)
                })
            } else {
                self.showMessage(withTitle: "Something went wrong".localized(),
                                 message: "Please write something to share.".localized(),
                                 okHandler: nil)
            }
        }
    }
    
    // MARK: -
    
    private func renderPhoto(with text: String) -> UIImage {
        Log.high("renderPhoto(with text: \(text))", from: self)

        let containerView = UIView(frame: self.view.bounds)

        let label = UILabel()

        label.text = text
        label.font = Fonts.medium(ofSize: 24.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false

        let imageView = UIImageView(image: Constants.textFeedBackgroundImage)

        imageView.translatesAutoresizingMaskIntoConstraints = false

        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(imageView)
        containerView.addSubview(label)

        NSLayoutConstraint.activate([
            containerView.heightAnchor.constraint(equalToConstant: self.view.frame.height),
            containerView.widthAnchor.constraint(equalToConstant: self.view.frame.width)
        ])

        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: containerView.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            imageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            imageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor)
        ])

        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 16.0),
            label.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -16.0),
            label.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: 16.0),
            label.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -16.0)
        ])

        containerView.layoutIfNeeded()

        return containerView.asImage()
    }
    
    // MARK: -
    
    private func share(photo: UIImage, text: String?, loadingViewController: LoadingViewController) {
        firstly {
            Services.feedService.sendFeed(photo: photo, text: text)
        }.done {
            SVProgressHUD.showSuccess(withStatus: Constants.successMessageText)
            
            loadingViewController.dismiss(animated: true, completion: {
                SVProgressHUD.dismiss(completion: {
                    self.closeController()
                })
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(error: error, okHandler: {
                    self.closeController()
                })
            })
        }
    }
    
    private func share(link: URL, text: String?, loadingViewController: LoadingViewController) {
        firstly {
            Services.feedService.sendFeed(text: text, link: link)
        }.done {
            SVProgressHUD.showSuccess(withStatus: Constants.successMessageText)
            
            loadingViewController.dismiss(animated: true, completion: {
                SVProgressHUD.dismiss(completion: {
                    self.closeController()
                })
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(error: error, okHandler: {
                    self.closeController()
                })
            })
        }
    }
    
    // MARK: -
    
    private func closeController() {
        guard let extensionContext = self.extensionContext else {
            fatalError()
        }
        
        extensionContext.completeRequest(returningItems: nil)
        
        self.unsubscribeFromKeyboardNotifications()
    }
    
    private func handle(error: Error?, okHandler: (() -> Void)?) {
        if let webError = error as? WebError {
            switch webError {
            case .unauthorized:
                self.showMessage(withTitle: "Please login".localized(),
                                 message: "To share this content you should be logged in to Friendsta.".localized(),
                                 okHandler: okHandler)
                
            case .connection, .timeOut:
                self.showMessage(withTitle: "No Internet Connection".localized(),
                                 message: "Check your wi-fi or mobile data connection.".localized(),
                                 okHandler: okHandler)
                
            default:
                self.showMessage(withTitle: "Something went wrong".localized(),
                                 message: "Please let us know what went wrong or try again later.".localized(),
                                 okHandler: okHandler)
            }
        } else {
            self.showMessage(withTitle: "Something went wrong".localized(),
                             message: "Error with sharing this type of content.".localized(),
                             okHandler: okHandler)
        }
    }

    func showMessage(withTitle title: String?, message: String?, okHandler: (() -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: { action in
            okHandler?()
        }))
        
        self.present(alertController, animated: true)
    }
    
    // MARK: -
    
    private func capturePhoto(attachment: NSItemProvider, completionHandler: @escaping (UIImage?) -> Void) {
        attachment.loadItem(forTypeIdentifier: Identifiers.imageType, options: nil, completionHandler: { data, _ in
            guard let url = data as? URL else {
                completionHandler(nil)
                
                return
            }
            
            do {
                let photoData = try Data(contentsOf: url)
                
                let capturedPhoto = UIImage(data: photoData)
                
                completionHandler(capturedPhoto)
            } catch {
                completionHandler(nil)
            }
        })
    }
    
    private func captureURL(attachment: NSItemProvider, completionHandler: @escaping (URL?) -> Void) {
        attachment.loadItem(forTypeIdentifier: Identifiers.URLType, options: nil, completionHandler: { data, _ in
            guard let url = data as? URL else {
                completionHandler(nil)
                
                return
            }
            
            completionHandler(url)
        })
    }
    
    private func captureText(attachment: NSItemProvider, completionHandler: @escaping (String?) -> Void) {
        attachment.loadItem(forTypeIdentifier: Identifiers.textType, options: nil, completionHandler: { data, _ in
            guard let text = data as? String else {
                completionHandler(nil)
                
                return
            }
            
            completionHandler(text)
        })
    }
    
    private func loadPreviewFor(link: URL) {
        Managers.linkPreviewManager.getInformationFrom(link: link, completion: { linkString, linkDescription, imageURLString, error in
            if let imageURLString = imageURLString, let imageURL = URL(string: imageURLString) {
                Managers.imageLoader.loadImage(for: imageURL, completionHandler: { image in
                    DispatchQueue.main.async {
                        self.loadingPreviewView.isHidden = true

                        self.photoPreviewImageView.image = image
                    }
                })
            }
            
            DispatchQueue.main.async {
                self.linkDescriptionLabel.text = linkDescription
                self.linkLabel.text = linkString
            }
        })
    }
    
    private func renderToVertical(image: UIImage) -> UIImage {
        let imageView = UIImageView(frame: self.view.bounds)
        
        imageView.backgroundColor = UIColor.black
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
            
        return imageView.asImage()
    }
    
    private func setupLinkPreview(attachment: NSItemProvider) {
        self.linkInformationView.isHidden = false
        self.photoImageHeightConstraint.constant = Constants.linkPreviewHeight
        self.loadingPreviewView.isHidden = true
        self.photoPreviewImageView.isHidden = false
        
        self.captureURL(attachment: attachment, completionHandler: { url in
            if let url = url {
                self.sharingURL = url
                
                self.loadPreviewFor(link: url)
            } else {
                self.handle(error: nil, okHandler: {
                    self.closeController()
                })
            }
        })
    }
    
    private func setupPhotoPreview(attachment: NSItemProvider) {
        self.linkInformationView.isHidden = true
        self.photoImageHeightConstraint.constant = UIScreen.main.bounds.height
        self.loadingPreviewView.isHidden = false
        self.photoPreviewImageView.isHidden = false
        
        self.capturePhoto(attachment: attachment, completionHandler: { image in
            if let image = image {
                let resultImage: UIImage
                
                if image.imageOrientation == .up {
                    resultImage = self.renderToVertical(image: image)
                } else {
                    resultImage = image.fixedOrientation() ?? image
                }
                
                self.sharingPhoto = resultImage
                
                DispatchQueue.main.async {
                    self.loadingPreviewView.isHidden = true
                    
                    self.photoPreviewImageView.image = resultImage
                }
            } else {
                self.handle(error: nil, okHandler: {
                    self.closeController()
                })
            }
        })
    }
    
    private func setupTextOnlyPreview(attachment: NSItemProvider) {
        self.linkInformationView.isHidden = true
        self.loadingPreviewView.isHidden = true
        self.photoPreviewImageView.isHidden = true
        
        self.captureText(attachment: attachment, completionHandler: { text in
            if let text = text {
                DispatchQueue.main.async {
                    self.textView.text = text
                    
                    self.textViewPlaceholderLabel.isHidden = true
                }
            } else {
                self.handle(error: nil, okHandler: {
                    self.closeController()
                })
            }
        })
    }
    
    private func configureView() {
        self.contentView.layer.cornerRadius = Constants.cornerRadius
        
        self.photoPreviewImageView.layer.cornerRadius = Constants.cornerRadius
        self.photoPreviewImageView.clipsToBounds = true
        self.photoPreviewImageView.layer.borderColor = UIColor.lightGray.cgColor
        self.photoPreviewImageView.layer.borderWidth = 1
        
        self.linkInformationView.clipsToBounds = true
        self.linkInformationView.layer.cornerRadius = Constants.cornerRadius
        self.linkInformationView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        self.linkInformationView.layer.borderColor = UIColor.lightGray.cgColor
        self.linkInformationView.layer.borderWidth = 1
        
        self.loadingPreviewView.isHidden = false
        
        if #available(iOS 13, *) {
            self.view.backgroundColor = UIColor.clear

            self.contentViewTopConstraint.constant = 0
            
            self.isModalInPresentation = true
        }
        
        SVProgressHUD.setContainerView(self.contentView)
    }
    
    private func setupContent() {
        guard let content = self.extensionContext?.inputItems.first as? NSExtensionItem else {
            return
        }
        
        guard let attachments = content.attachments else {
            return
        }
        
        for attachment in attachments {
            guard let registeredTypeIdentifier = attachment.registeredTypeIdentifiers.first else {
                return
            }
            
            switch registeredTypeIdentifier {
            case Identifiers.imageType, Identifiers.jpegType, Identifiers.pngType:
                self.shareContent = .photo
                
                self.setupPhotoPreview(attachment: attachment)
                
            case Identifiers.URLType:
                self.shareContent = .link
                
                self.setupLinkPreview(attachment: attachment)
                
            case Identifiers.textType:
                self.shareContent = .textOnly
                
                self.setupTextOnlyPreview(attachment: attachment)
                
            default:
                return
            }
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureView()
        self.setupContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribeToKeyboardNotifications()
        
        self.textView.becomeFirstResponder()
    }
}

// MARK: - KeyboardHandler

extension ShareViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        if #available(iOS 13, *) {
            let safeAreaTopHeight = self.view.safeAreaInsets.top
            
            self.bottomSpacerViewHeightConstraint.constant = keyboardHeight + safeAreaTopHeight + Constants.modalPresentationTopDistanceHeight
        } else {
            self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        }
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

// MARK: - UITextViewDelegate

extension ShareViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        self.textViewPlaceholderLabel.isHidden = !(text.isEmpty && textView.text.isEmpty)
        
        return true
    }
}
