//
//  TransparentNavigationController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class TransparentNavigationController: LoggedNavigationController {
    
    // MARK: - Instance Properties
    
    override var childForStatusBarStyle: UIViewController? {
        return self.topViewController
    }
    
    override var childForStatusBarHidden: UIViewController? {
        return self.topViewController
    }
    
    // MARK: - Instance Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationBar.isTranslucent = true
        self.navigationBar.titleTextAttributes = [.font: Fonts.semiBold(ofSize: 17.0)]
    }
}
