//
//  EmptyStateViewCustomer.swift
//  Friendsta
//
//  Created by Elina Batyrova on 14.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaNetwork

protocol EmptyStateViewCustomer {
    
    // MARK: - Instance Properties
    
    var emptyStateView: EmptyStateView! { get }
    
    // MARK: - Instance Methods
    
    func showEmptyState(image: UIImage?, title: String, message: String, action: EmptyStateAction?, activity: Bool)
    func showEmptyState(image: UIImage?, title: String, message: String, action: EmptyStateAction?)
    func showEmptyState(image: UIImage?, title: String, message: String)
    
    func showEmptyState(title: String, message: String, action: EmptyStateAction?, activity: Bool)
    func showEmptyState(title: String, message: String, action: EmptyStateAction?)
    func showEmptyState(title: String, message: String)
    
    func hideEmptyState()
    
    func showNoCameraAccessState(allowHandler: (() -> Void)?)
    func showNoLibraryAccessState(allowHandler: (() -> Void)?)
    
    func showNoLocationAccessState(allowHandler: (() -> Void)?)
    func showNoContactsAccessState(allowHandler: (() -> Void)?)
    
    func showFailedState(withError error: Error, retryHandler: (() -> Void)?)
}

// MARK: -

extension EmptyStateViewCustomer {
    
    // MARK: - Instance Methods
    
    func showEmptyState(image: UIImage?, title: String, message: String, action: EmptyStateAction?, activity: Bool) {
        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action
        
        if activity {
            self.emptyStateView.showActivityIndicator()
        } else {
            self.emptyStateView.hideActivityIndicator()
        }
        
        self.emptyStateView.isHidden = false
    }
    
    func showEmptyState(image: UIImage?, title: String, message: String, action: EmptyStateAction?) {
        self.showEmptyState(image: image, title: title, message: message, action: action, activity: false)
    }
    
    func showEmptyState(image: UIImage?, title: String, message: String) {
        self.showEmptyState(image: image, title: title, message: message, action: nil, activity: false)
    }
    
    func showEmptyState(title: String, message: String, action: EmptyStateAction?, activity: Bool) {
        self.showEmptyState(image: nil, title: title, message: message, action: action, activity: activity)
    }
    
    func showEmptyState(title: String, message: String, action: EmptyStateAction?) {
        self.showEmptyState(image: nil, title: title, message: message, action: action, activity: false)
    }
    
    func showEmptyState(title: String, message: String) {
        self.showEmptyState(image: nil, title: title, message: message, action: nil, activity: false)
    }
    
    func showEmptyState(title: String, message: String, activity: Bool) {
        self.showEmptyState(image: nil, title: title, message: message, action: nil, activity: activity)
    }
    
    func hideEmptyState() {
        self.emptyStateView.hideActivityIndicator()
        
        self.emptyStateView.isHidden = true
    }
    
    func showNoCameraAccessState(allowHandler: (() -> Void)?) {
        let action: EmptyStateAction?
        
        if let allowHandler = allowHandler {
            action = EmptyStateAction(title: "Allow access to camera".localized(),
                                      isPrimary: true,
                                      onClicked: allowHandler)
        } else {
            action = nil
        }
        
        self.showEmptyState(title: "Allow access to camera".localized(),
                            message: "Your privacy is important. We use camera to take a photo.".localized(),
                            action: action)
    }
    
    func showNoLibraryAccessState(allowHandler: (() -> Void)?) {
        let action: EmptyStateAction?
        
        if let allowHandler = allowHandler {
            action = EmptyStateAction(title: "Allow access to photos".localized(),
                                      isPrimary: true,
                                      onClicked: allowHandler)
        } else {
            action = nil
        }
        
        self.showEmptyState(title: "Allow access to photos".localized(),
                            message: "Your privacy is important. We use photo library to choose a photo.".localized(),
                            action: action)
    }
    
    func showNoLocationAccessState(allowHandler: (() -> Void)?) {
        let action: EmptyStateAction?
        
        if let allowHandler = allowHandler {
            action = EmptyStateAction(title: "Allow access to location".localized(),
                                      isPrimary: true,
                                      onClicked: allowHandler)
        } else {
            action = nil
        }
        
        self.showEmptyState(title: "Allow access to location".localized(),
                            message: "Your privacy is important. We use location to find nearby schools.".localized(),
                            action: action)
    }
    
    func showNoContactsAccessState(allowHandler: (() -> Void)?) {
        let action: EmptyStateAction?
        
        if let allowHandler = allowHandler {
            action = EmptyStateAction(title: "Allow access to contacts".localized(),
                                      isPrimary: true,
                                      onClicked: allowHandler)
        } else {
            action = nil
        }
        
        self.showEmptyState(title: "Allow access to contacts".localized(),
                            message: "We use contacts to find your friends on the app. We will never contact your friends without your permission.".localized(),
                            action: action)
    }
    
    func showFailedState(withError error: Error, retryHandler: (() -> Void)?) {
        let action: EmptyStateAction?
        
        if let retryHandler = retryHandler {
            action = EmptyStateAction(title: "Try again".localized(),
                                      isPrimary: false,
                                      onClicked: retryHandler)
        } else {
            action = nil
        }
        
        switch error as? WebError {
        case .some(.connection), .some(.timeOut):
            self.showEmptyState(title: "No Internet Connection".localized(),
                                message: "Check your wi-fi or mobile data connection.".localized(),
                                action: action)
            
        default:
            self.showEmptyState(title: "Something went wrong".localized(),
                                message: "Please let us know what went wrong or try again later.".localized(),
                                action: action)
        }
    }
}
