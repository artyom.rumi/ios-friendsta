//
//  AlertMessagePresenter.swift
//  Friendsta
//
//  Created by Elina Batyrova on 14.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaNetwork

protocol AlertMessagePresenter {
    
    // MARK: - Instance Properties
    
    var presenterController: UIViewController { get }
    
    // MARK: - Instance Methods
    
    func showMessage(withTitle title: String?, message: String?, okHandler: (() -> Void)?)
    func showMessage(withError error: Error, okHandler: (() -> Void)?)
    
    func showAccessRequest(withTitle title: String?, message: String?)
    
    func showCameraAccessRequest()
    func showLibraryAccessRequest()
    
    func showLocationAccessRequest()
    func showContactsAccessRequest()
}

// MARK: -

extension AlertMessagePresenter {
 
    // MARK: - Instance Methods
    
    func showMessage(withTitle title: String?, message: String?, okHandler: (() -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: { action in
            okHandler?()
        }))
        
        self.presenterController.present(alertController, animated: true)
    }
    
    func showMessage(withTitle title: String?, message: String?) {
        self.showMessage(withTitle: title, message: message, okHandler: nil)
    }
    
    func showMessage(withError error: Error, okHandler: (() -> Void)?) {
        switch error as? WebError {
        case .some(.connection), .some(.timeOut):
            self.showMessage(withTitle: "No Internet Connection".localized(),
                             message: "Check your wi-fi or mobile data connection.".localized(),
                             okHandler: okHandler)
            
        default:
            self.showMessage(withTitle: "Something went wrong".localized(),
                             message: "Please let us know what went wrong or try again later.".localized(),
                             okHandler: okHandler)
        }
    }
    
    func showMessage(withError error: Error) {
        self.showMessage(withError: error, okHandler: nil)
    }
    
    func showAccessRequest(withTitle title: String?, message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        
        self.presenterController.present(alertController, animated: true)
    }
    
    func showCameraAccessRequest() {
        self.showAccessRequest(withTitle: "Please allow access".localized(),
                               message: "The app needs access to your camera.\n\nPlease go to Settings and set to ON.".localized())
    }
    
    func showLibraryAccessRequest() {
        self.showAccessRequest(withTitle: "Please allow access".localized(),
                               message: "The app needs access to your photo library.\n\nPlease go to Settings and set to ON.".localized())
    }
    
    func showLocationAccessRequest() {
        self.showAccessRequest(withTitle: "Please allow access".localized(),
                               message: "The app needs access to your location.\n\nPlease go to Settings and set to ON.".localized())
    }
    
    func showContactsAccessRequest() {
        self.showAccessRequest(withTitle: "Please allow access".localized(),
                               message: "The app needs access to your contacts.\n\nPlease go to Settings and set to ON.".localized())
    }
}

// MARK: -

extension AlertMessagePresenter where Self: UIViewController {
    
    // MARK: - Instance Properties
    
    var presenterController: UIViewController {
        return self
    }
}
