//
//  ImagePickerController.swift
//  Friendsta
//
//  Created by Marat Galeev on 16.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import AVFoundation

class ImagePickerController: UIImagePickerController {
    
    // MARK: - Nested Types
    
    enum MediaSource {
        
        // MARK: - Enumeration Cases
        
        case camera
        case photoLibrary
    }
    
    // MARK: - Instance Properties
    
    fileprivate(set) var mediaSource: MediaSource?
    
    var onImagePicked: ((_ image: UIImage) -> Void)?
    
    // MARK: - Instance Methods
    
    fileprivate func applyCameraSource(allowsEditing: Bool) {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            return self.showCameraError()
        }
        
        self.allowsEditing = allowsEditing
        self.sourceType = .camera
        self.cameraCaptureMode = .photo
    }
    
    fileprivate func applyGallerySource(allowsEditing: Bool) {
        self.allowsEditing = allowsEditing
        self.sourceType = .photoLibrary
    }
    
    fileprivate func showAccessError() {
        let alertController = UIAlertController(title: "Please allow access".localized(),
                                                message: "The application needs access to your camera and photo library to take photos.\n\nPlease go to Settings > Privacy > Camera and set to ON.".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
            }
        }))
        
        self.present(alertController, animated: true)
    }
    
    fileprivate func showCameraError() {
        let alertController = UIAlertController(title: nil,
                                                message: "Camera is not available".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { _ in
            self.dismiss(animated: true)
        }))
        
        self.present(alertController, animated: true)
    }
    
    // MARK: -
    
    func apply(mediaSource: MediaSource, allowsEditing: Bool = false) {
        self.mediaSource = mediaSource
        
        switch mediaSource {
        case .camera:
            self.applyCameraSource(allowsEditing: allowsEditing)
            
        case .photoLibrary:
            self.applyGallerySource(allowsEditing: allowsEditing)
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.tintColor = Colors.primary
        
        self.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized, .notDetermined, .restricted:
            break
            
        case .denied:
            self.showAccessError()
        }
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
}

// MARK: - UIImagePickerControllerDelegate

extension ImagePickerController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - Instance Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let onImagePicked = self.onImagePicked {
            if let image = info[.editedImage] as? UIImage {
                onImagePicked(image)
            } else if let image = info[.originalImage] as? UIImage {
                onImagePicked(image)
            }
        } else {
            self.dismiss(animated: true)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true)
    }
}
