//
//  FeedViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 13/06/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//
// swiftlint:disable file_length
// swiftlint:disable type_body_length
// swiftlint:disable function_body_length
// swiftlint:disable vertical_whitespace
// swiftlint:disable multiple_closures_with_trailing_closure
// swiftlint:disable private_outlet

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

import SwiftKeychainWrapper

class FeedViewController: LoggedViewController, ErrorMessagePresenter, EmptyStateViewCustomer {
    
    //... rating
    var starArray: [UIButton] = []
    var rateCount : Int = 0
    var currentFeedIndex = 0
    ///// ********* Lance's Code ********* /////
    fileprivate lazy var bottomRectangeCloudView: RectangeCloudView = {
        let rectangeCloudView = RectangeCloudView()
        rectangeCloudView.translatesAutoresizingMaskIntoConstraints = false
        
        rectangeCloudView.gradientColors = [Colors.lightBlueGuideView.cgColor,
                                            Colors.blueGuideView.cgColor]
        
        rectangeCloudView.backgroundColor = UIColor.clear
        
        rectangeCloudView.title = "Post anonymous conversation starters"
        rectangeCloudView.buttonTitle = "Got it!"
        
        rectangeCloudView.layer.cornerRadius = 5
        rectangeCloudView.layer.masksToBounds = true
        
        //rectangeCloudView.shouldFlipVetically = true
        rectangeCloudView.pointerTrailingLength = view.frame.width / 2 - 16
        
        rectangeCloudView.onActionButtonClicked = { [weak self] in
            self?.bottomRectangeCloudViewGotItButtonWasPressed()
        }
        
        return rectangeCloudView
    }()
    
    let keyWindow = UIApplication.shared.keyWindow
    fileprivate func displayBottomRectangeCloudView() {
        
        configureRectangeCloudViewTintedBackgroundView()
        
        displayTopRectangeCloudView()
        
        view.addSubview(bottomRectangeCloudView)
        bottomRectangeCloudView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -25).isActive = true
        bottomRectangeCloudView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        bottomRectangeCloudView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        bottomRectangeCloudView.heightAnchor.constraint(equalToConstant: 74).isActive = true
    }
    
    fileprivate func bottomRectangeCloudViewGotItButtonWasPressed() {
        
        UIView.animate(withDuration: 0.33, animations: {
            self.bottomRectangeCloudView.alpha = 0
            self.topRectangeCloudView.alpha = 1
        }) { (_) in
            self.bottomRectangeCloudView.removeFromSuperview()
        }
    }
    
    fileprivate let tintedBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        return view
    }()
    
    fileprivate func configureRectangeCloudViewTintedBackgroundView() {
        view.addSubview(tintedBackgroundView)
        tintedBackgroundView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tintedBackgroundView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tintedBackgroundView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        tintedBackgroundView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
    }
    
    fileprivate lazy var topRectangeCloudView: RectangeCloudView = {
        let rectangeCloudView = RectangeCloudView()
        rectangeCloudView.translatesAutoresizingMaskIntoConstraints = false
        
        let myOrangeColor = hexStringToUIColor(hex: "fd9978")
        rectangeCloudView.gradientColors = [myOrangeColor.cgColor, Colors.orangeGuideView.cgColor]
        
        rectangeCloudView.backgroundColor = UIColor.clear
        
        rectangeCloudView.title = "Read replies and chat incognito!"
        rectangeCloudView.buttonTitle = "Got it!"
        
        rectangeCloudView.layer.cornerRadius = 5
        rectangeCloudView.layer.masksToBounds = true
        
        rectangeCloudView.shouldFlipVertically = true
        rectangeCloudView.pointerTrailingLength = 16
        
        rectangeCloudView.onActionButtonClicked = { [weak self] in
            self?.topRectangeCloudViewGotItButtonWasPressed()
        }
        
        return rectangeCloudView
    }()
    
    fileprivate func displayTopRectangeCloudView() {
        
        topRectangeCloudView.alpha = 0
        view.addSubview(topRectangeCloudView)
        
        if UIDevice.current.hasTopNotch {
            //... consider notch
            topRectangeCloudView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        } else {
            //... don't have to consider notch
            topRectangeCloudView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50).isActive = true
        }
        topRectangeCloudView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
        topRectangeCloudView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16).isActive = true
        topRectangeCloudView.heightAnchor.constraint(equalToConstant: 74).isActive = true
    }
    
    fileprivate func topRectangeCloudViewGotItButtonWasPressed() {
        
        KeychainWrapper.standard.set(true, forKey: "wasTopRectangeCloudViewGotItButtonPressed")
        
        UIView.animate(withDuration: 0.15, animations: {
            self.topRectangeCloudView.alpha = 0
            self.tintedBackgroundView.alpha = 0
        }) { (_) in
            self.topRectangeCloudView.removeFromSuperview()
            self.tintedBackgroundView.removeFromSuperview()
        }
    }
    
    @objc fileprivate func removeRectangeCloudViewCallouts() {
        bottomRectangeCloudViewGotItButtonWasPressed()
    }
    
    func hexStringToUIColor(hex:String) -> UIColor {
        var cString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                       green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                       blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                       alpha: CGFloat(1.0)
        )
    }
    //OutlineContactsTableViewController
    ///// ********* Lance's Code ********* /////
    
    // MARK: - Nested Types

    private typealias CommentSegueData = (shouldShowKeyboard: Bool, feed: Feed)

    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let feedCollectionCellIdentifier = "FeedCollectionCell"
        static let feedLinkCollectionCellIdentifier = "FeedLinkCollectionCell"
        static let feedEndCollectionCellIdentifier = "FeedEndCollectionCell"
        static let feedEmptyStateCellIdentifier = "FeedEmptyStateCell"

        static let endStateViewCount = 1
        
        static let friendsSegment = 1
        static let discoverSegment = 0
    }

    // MARK: -

    private enum Segues {

        // MARK: - Type Properties

        static let unauthorize = "Unauthorize"

        static let showComments = "ShowComments"
        static let showChatMessages = "ShowChatMessages"
        static let showWriteCommentBox = "ShowWriteCommentBox"
    }
    
    // MARK: -
    
    private enum ScrollDirection {
        
        // MARK: - Enumeration Cases
        
        case top
        case bottom
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var collectionViewFlowLayout: UICollectionViewFlowLayout!

    @IBOutlet private weak var emptyStateViewOutlet: EmptyStateView!
    
    @IBOutlet private weak var segmentedControl: CustomSegmentedControl!
    
    @IBOutlet private weak var chatMessagesBadgeView: RoundControl!
    @IBOutlet private weak var chatMessagesBadgeLabel: UILabel!
    
    // MARK: -
    
    lazy var emptyStateView: EmptyStateView! = {
        return self.emptyStateViewOutlet
    }()
    
    // MARK: -

    private let connectionsPanelTransition = PanelTransition(topPadding: 59, cornerRadius: 18, options: [.translucent, .interactive])
    private let textFeedPanelTransition = PanelTransition(topPadding: 62, cornerRadius: 12, options: [.translucent, .closeButton])
    private let commentsPanelTransition = PanelTransition(topPadding: UIScreen.main.bounds.height / 4, options: [.interactive])

    private var promiseQueues: [Feed.UID: PromiseQueue] = [:]

    private var feedListType: FeedListType = .discover
    private var feeds: [Feed] = []

    private var pagingManager: PagingManager?

    private var shouldApplyData = true
    
    private var pageViewController: MainPageViewController? {
        return self.tabBarController?.parent as? MainPageViewController
    }
    
    private var scrollDirection: ScrollDirection = .bottom
    
    private var previousScrollOffsetVerticalPosition: CGFloat = 0
    
    private var shouldScrollToTop: Bool = false
    
    // added by lava on 0125
    private var shouldScrollToBottom : Bool = false
    
    private var hasNewRatePost: Bool = false
    // end by lava on 0125
    
    private var lastContentOffset = CGPoint.zero
    
    // MARK: -

    deinit {
        self.unsubscribeFromFeedEvents()
    }
   
    // MARK: - Instance Methods
    
    @IBAction private func onTextFeedPostingFinished(segue: UIStoryboardSegue) {
        Log.high("onTextFeedPostingFinished(withSegue: \(String(describing: segue.identifier)))", from: self)
        
        self.refreshFeedList(listType: self.feedListType, shouldScrollToTop: true)
        
        if let mainTabBarController = self.tabBarController as? MainTabBarController {
            mainTabBarController.onFeedTabSelected?()
        }
    }
    
    @IBAction private func onPhotoPostingFinished(segue: UIStoryboardSegue) {
        Log.high("onPhotoPostingFinished(withSegue: \(String(describing: segue.identifier)))", from: self)
        
        self.refreshFeedList(listType: self.feedListType, shouldScrollToTop: true)
        
        if let mainTabBarController = self.tabBarController as? MainTabBarController {
            mainTabBarController.onFeedTabSelected?()
        }
    }
    
    @IBAction private func onLinkPostingFinished(segue: UIStoryboardSegue) {
        Log.high("onLinkPostingFinished(withSegue: \(String(describing: segue.identifier)))", from: self)
        
        self.refreshFeedList(listType: self.feedListType, shouldScrollToTop: true)
        
        if let mainTabBarController = self.tabBarController as? MainTabBarController {
            mainTabBarController.onFeedTabSelected?()
        }
    }
    
    @objc fileprivate func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.refreshFeedList(listType: self.feedListType)
    }
    
    @IBAction private func onCameraButtonTouchUpInside(_ sender: Any) {
        Log.high("onCameraButtonTouchUpInside()", from: self)
        
        guard let pageViewController = self.pageViewController else {
            return
        }
        
        pageViewController.show(controller: .makePhotoViewController)
    }
    
    @IBAction private func onChatsButtonTouchUpInside(_ sender: Any) {
        Log.high("onChatsButtonTouchUpInside()", from: self)
        
        guard let pageViewController = self.pageViewController else {
            return
        }
        pageViewController.feedsFromFeedVC = feeds
        pageViewController.show(controller: .chatsViewController)
    }
    
    @IBAction private func onCommentsClosed(_ segue: UIStoryboardSegue) {
        Log.high("onCommentsClosed(\(String(describing: segue.identifier)))", from: self)
    }

    // MARK: -

    private func showLoadingState() {
        self.showEmptyState(title: "Loading feeds".localized(),
                            message: "We are loading list of feeds. Please wait a bit".localized())

        self.emptyStateView.showActivityIndicator()
    }

    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try Again".localized(), isPrimary: true, onClicked: {
            retryHandler?()
        })

        switch error as? WebError {
        case .some(.connection), .some(.timeOut):
            if self.feeds.isEmpty {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your Wi-Fi or mobile data connection.".localized(),
                                    action: action)
            }

        case .some(.unauthorized):
            self.shouldApplyData = true

            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)

        default:
            if self.feeds.isEmpty {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        }
    }

    // MARK: -

    private func subscribeToFeedEvents() {
        self.unsubscribeFromFeedEvents()

        let feedManager = Services.cacheViewContext.feedManager

        feedManager.objectsChangedEvent.connect(self, handler: { [weak self] feeds in
            self?.shouldApplyData = true
        })

        feedManager.startObserving()
    }

    private func unsubscribeFromFeedEvents() {
        Services.cacheViewContext.feedManager.objectsChangedEvent.disconnect(self)
    }

    // MARK: -

    private func updatePagingManager() {
        let heights = (0 ..< self.feeds.count + Constants.endStateViewCount).map { index in
            self.calculateItemSize(for: index).height
        }

        let anchors = (0 ..< heights.count).map { index -> CGPoint in
            let offset = heights.prefix(index).reduce(0, +)

            return CGPoint(x: 0, y: offset)
        }

        self.pagingManager = PagingManager(scrollView: self.collectionView, anchors: anchors, topHeaderPoint: CGPoint(x: 0, y: 0) )
    }

    // MARK: -

    private func apply(feedListType: FeedListType, shouldRefresh: Bool = true, shouldScrollToBottom: Bool = false) {
        self.feedListType = feedListType

        let feedList = Services.cacheViewContext.feedListManager.firstOrNew(withListType: feedListType)

        if shouldRefresh {
            self.refreshFeedList(listType: feedListType, shouldScrollToBottom: shouldScrollToBottom)
        } else {
            self.apply(feedList: feedList)
        }
    }

    private func apply(feedList: FeedList, shouldScrollToTop: Bool = false, shouldScrollToBottom: Bool = false) {
        Log.high("apply(feedList: \(feedList.count))", from: self)
        print("---------- get apply part for getting feedlist ---------------")
        self.feeds = []
        self.hasNewRatePost = false
        /// added by lava on 0123
        if self.feedListType == .discover {
            for feedone in feedList.allFeeds {
                self.feeds.append(feedone)
                if !feedone.isRated {
                    self.hasNewRatePost = true
                    break
                }
            }
//            for feedone in feedList.allFeeds {
//                if feedone.isRated {
//                    self.feeds.append(feedone)
//                }
//            }
//            for feedone in feedList.allFeeds {
//               if !feedone.isRated {
//                self.feeds.append(feedone)
//                self.hasNewRatePost = true
//                break
//               }
//           }
        } else {
            self.feeds = feedList.allFeeds
        }

//        self.feeds = feedList.allFeeds
        self.shouldScrollToTop = shouldScrollToTop

        self.hideEmptyState()
        
        self.configureCollectionViewFlowLayout()

        self.collectionView.reloadData()

        if shouldScrollToTop {
            self.collectionView.setContentOffset(CGPoint.zero, animated: true)
        }

        self.updatePagingManager()

        self.shouldApplyData = false
        
        if shouldScrollToBottom {
            /// scroll down after 2 seconds
//            print("------ this is the scroll down part -----")
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                // your code here
                let bottomOffset = CGPoint(x: 0, y: self.collectionView.contentSize.height - self.collectionView.bounds.height + self.collectionView.contentInset.bottom)
                self.collectionView.setContentOffset(bottomOffset, animated: true)
            }
            
        }
    }

    // MARK: -

    private func refreshFeedList(listType: FeedListType, shouldScrollToTop: Bool = false, shouldScrollToBottom: Bool = false) {
        Log.high("refreshFeedList()", from: self)

        if self.feeds.isEmpty {
            self.showLoadingState()
        }
        
        let promise = (listType == .all) ? Services.feedService.fetchFeed() : Services.feedService.fetchDiscover()

        firstly {
            promise
        }.ensure {
            if let mainTabBarController = self.tabBarController as? MainTabBarController {
                mainTabBarController.hideFeedLoadingState()
            }
        }.done { feedList in
            self.apply(feedList: feedList, shouldScrollToTop: shouldScrollToTop, shouldScrollToBottom: shouldScrollToBottom)
        }.catch { error in
            self.handle(stateError: error)
        }
    }

    private func putLike(on feed: Feed, queue: PromiseQueue) {
        Log.high("putLike(feed: \(feed.uid))", from: self)

        queue.add {
            Services.feedService.putLike(for: feed.uid)
        }.ensure {
            self.apply(feedListType: self.feedListType, shouldRefresh: false)
        }.cauterize()
    }

    private func removeLike(on feed: Feed, queue: PromiseQueue) {
        Log.high("removeLike(feed: \(feed.uid))", from: self)

        queue.add {
            Services.feedService.removeLike(for: feed.uid)
        }.ensure {
            self.apply(feedListType: self.feedListType, shouldRefresh: false)
        }.cauterize()
    }
    
    // added by lava on 0120
    private func putRates(on feed: Feed, queue: PromiseQueue) {
        Log.high("putRate(feed: \(feed.uid))", from: self)
        
        if self.rateCount == 9 {
            firstly {
                Services.usersService.refresh(userUID: feed.userUID)
            }.done { user in
//               print(user)
               firstly {
                   Services.usersService.inviteUser(user: user)
               }.done { _ in
                   print("------ inviteUser -----")
               }.catch { _ in
                   print("----- catch error -----")
               }
            }.catch { error in
                print(error)
            }
            
        }
        
        queue.add {
            Services.feedService.putRates(for: feed.uid, rates: Int64(self.rateCount+1))
        }.ensure {
            self.apply(feedListType: self.feedListType, shouldRefresh: true, shouldScrollToBottom: true)
        }.cauterize()
    }
    // end by lava
    
    private func report(user: User, loadingViewController: LoadingViewController) {
        Log.high("report(user: \(user.uid))", from: self)

        firstly {
            Services.usersService.report(user: user)
        }.done {
            loadingViewController.dismiss(animated: true, completion: {
                self.showMessage(withTitle: "Thank you!".localized(), message: "Your report will be reviewed by our team very soon.".localized())
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(stateError: error)
            })
        }
    }

    private func block(user: User, loadingViewController: LoadingViewController) {
        Log.high("block(user: \(user.uid))", from: self)

        firstly {
            Services.usersService.block(user: user)
        }.done {
            loadingViewController.dismiss(animated: true)
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(stateError: error)
            })
        }
    }

    private func showMoreOptions(for userUID: Int64) {
        if let user = Services.cacheViewContext.usersManager.first(with: userUID) {
            self.showMoreOptions(for: user)
        } else {
            let loadingViewController = LoadingViewController()

            self.present(loadingViewController, animated: true, completion: {
                firstly {
                    Services.usersService.refresh(userUID: userUID)
                }.done { user in
                    loadingViewController.dismiss(animated: true, completion: {
                        self.showMoreOptions(for: user)
                    })
                }.catch { error in
                    loadingViewController.dismiss(animated: true, completion: {
                        self.handle(stateError: error)
                    })
                }
            })
        }
    }

    private func markAsRead(commentUIDs: [Int64], for feedUID: Int64) {
        Log.high("markAsRead(commentUIDs: \(commentUIDs.count), feedUID: \(feedUID))", from: self)

        guard !commentUIDs.isEmpty else {
            return
        }

        firstly {
            Services.commentService.markAsRead(commentUIDs: commentUIDs, for: feedUID)
        }.done { feed in
            let feedList = Services.cacheViewContext.feedListManager.firstOrNew(withListType: self.feedListType)

            self.apply(feedList: feedList)
        }.cauterize()
    }

    private func repost(feed: Feed) {
        Log.high("repost feed: \(feed.uid))", from: self)

        let loadingViewController = LoadingViewController()

        self.present(loadingViewController, animated: true) {
            firstly {
                Services.feedService.repost(feed: feed)
            }.done { feed in
                loadingViewController.dismiss(animated: true) {
                    self.refreshFeedList(listType: self.feedListType, shouldScrollToTop: true)
                }
            }.catch { error in
                loadingViewController.dismiss(animated: true) {
                    self.showMessage(withError: error)
                }
            }
        }
    }

    // MARK: -

    private func showMoreOptions(for user: User) {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let blockSenderAction = UIAlertAction(title: "Block Sender".localized(), style: .default, handler: { [unowned self] action in
            self.showBlockOptions(for: user)
        })

        let reportContentAction = UIAlertAction(title: "Report Content".localized(), style: .default, handler: { [unowned self] (action) in
            self.showReportOptions(for: user)
        })

        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)

        actionSheetController.addAction(blockSenderAction)
        actionSheetController.addAction(reportContentAction)
        actionSheetController.addAction(cancelAction)

        self.present(actionSheetController, animated: true)
    }

    private func showReportOptions(for user: User) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alertController.view.tintColor = Colors.primary

        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)

        let reportAction = UIAlertAction(title: "Report".localized(), style: .destructive, handler: { action in
            let loadingViewController = LoadingViewController()

            self.present(loadingViewController, animated: true, completion: {
                self.report(user: user, loadingViewController: loadingViewController)
            })
        })

        alertController.addAction(cancelAction)
        alertController.addAction(reportAction)

        self.present(alertController, animated: true)
    }

    private func showBlockOptions(for user: User) {
        let alertController = UIAlertController(title: String(format: "Block Messages.".localized()),
                                                message: "You will never receive messages from this user again.".localized(),
                                                preferredStyle: .alert)

        alertController.view.tintColor = Colors.primary

        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)

        let blockAction = UIAlertAction(title: "Block".localized(), style: .destructive, handler: { action in
            let loadingViewController = LoadingViewController()

            self.present(loadingViewController, animated: true, completion: {
                self.block(user: user, loadingViewController: loadingViewController)
            })
        })

        alertController.addAction(cancelAction)
        alertController.addAction(blockAction)

        self.present(alertController, animated: true)
    }

    // MARK: -

    private func configureCollectionViewFlowLayout() {
        let height = UIScreen.main.bounds.height - (self.tabBarController?.tabBar.bounds.height ?? 0)

        self.collectionViewFlowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width,
                                                        height: height)
    }

    private func configureCollectionView() {
        self.collectionView.decelerationRate = .fast
        self.collectionView.contentInsetAdjustmentBehavior = .never
    }

    private func configureMainTabBarController() {
        guard let mainTabBarController = self.tabBarController as? MainTabBarController else {
            return
        }

        mainTabBarController.onFeedTabReselected = {
            mainTabBarController.showFeedLoadingState()

            self.refreshFeedList(listType: self.feedListType, shouldScrollToTop: true)
        }
    }
    
    private func configureSegmentedControl() {
        self.segmentedControl.onSegmentSelected = { [unowned self] index in
            switch index {
            case Constants.friendsSegment:
                //  self.segmentedControl.buttonTitles = ["FRIENDS".localized(), "Friends+".localized()]
                //  changed by lava on 1012
                self.segmentedControl.buttonTitles = ["Nearby".localized(), "FRIENDS".localized()]
                
                self.apply(feedListType: .all)
                
                self.reconfigureContentOffset()
                
            case Constants.discoverSegment:
                //  self.segmentedControl.buttonTitles = ["Friends".localized(), "FRIENDS+".localized()]
                //  changed by lava on 1012
                self.segmentedControl.buttonTitles = ["NEARBY".localized(), "Friends".localized()]
                
                self.apply(feedListType: .discover)
                
                self.reconfigureContentOffset()
                
            default:
                fatalError()
            }
        }
    }
    
    private func reconfigureContentOffset() {
        let currentContentOffset = self.collectionView.contentOffset
        
        self.collectionView.setContentOffset(self.lastContentOffset, animated: false)
        self.updatePagingManager()
        self.lastContentOffset = currentContentOffset
    }
    
    // MARK: -
    
    private func setScrollViewOffset(verticalPosition: CGFloat) {
        self.collectionView.contentOffset = CGPoint(x: self.collectionView.contentOffset.x, y: verticalPosition)
    }
    
    private func calculateItemSize(for index: Int) -> CGSize {
        let height = UIScreen.main.bounds.height - (self.tabBarController?.tabBar.bounds.height ?? 0)
        
        return CGSize(width: UIScreen.main.bounds.width, height: height)
    }
    
    private func onChatTapped(loadingViewController: LoadingViewController, feed: Feed) {
        firstly {
            Services.usersService.refresh(userUID: feed.userUID)
        }.done { user in
            loadingViewController.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: Segues.showChatMessages, sender: (feed, user))
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.showMessage(withError: error)
            })
        }
    }
    
    // MARK: -
    
    private func createMenuItems(cell: UICollectionViewCell, for feed: Feed) -> [MenuItemView] {
        var menuItems: [MenuItemView] = []
        
        menuItems.append(self.createLikeMenuItem(cell: cell, for: feed))

       // if Services.accountUser?.uid != feed.userUID {
         //   menuItems.append(self.createChatMenuItem(for: feed))
        //}

        menuItems.append(self.createCommentMenuItem(for: feed))
        
        /* menuItems.append(self.createRepostMenuItem(for: feed))
        
        if Services.accountUser?.uid != feed.userUID {
            repostMenuItem.isHidden = false
        } else {
            repostMenuItem.isHidden = true
        }
        */
        return menuItems
    }

    private func createLikeMenuItem(cell: UICollectionViewCell, for feed: Feed) -> MenuItemView {
        let likeMenuItem = MenuItemView()
        
        likeMenuItem.isHidden = self.feedListType == .discover ? true : false

        likeMenuItem.iconImage = feed.isLiked ? Images.likeFillIcon : Images.likeIcon

        likeMenuItem.title = "\(feed.likeCount)"

        likeMenuItem.onViewTapped = {
            let promiseQueue: PromiseQueue

            if let dictionaryPromiseQueue = self.promiseQueues[feed.uid] {
                promiseQueue = dictionaryPromiseQueue
            } else {
                promiseQueue = PromiseQueue(name: "LikeFeed \(feed.uid)")

                self.promiseQueues[feed.uid] = promiseQueue
            }

            if feed.isLiked {
                self.removeLike(on: feed, queue: promiseQueue)

                feed.likeCount -= 1
            } else {
                self.putLike(on: feed, queue: promiseQueue)

                feed.likeCount += 1
            }

            feed.isLiked.toggle()

            likeMenuItem.iconImage = feed.isLiked ? Images.likeFillIcon : Images.likeIcon
            likeMenuItem.title = "\(feed.likeCount)"
        }

        return likeMenuItem
    }

    private func createChatMenuItem(for feed: Feed) -> MenuItemView {
        let chatMenuItem = MenuItemView()
        
        chatMenuItem.isHidden = self.feedListType == .discover ? true : false
        
        chatMenuItem.iconImage = Images.chatIcon
        chatMenuItem.title = "Chat".localized()
        chatMenuItem.onViewTapped = { [unowned self] in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: { [unowned self] in
                self.onChatTapped(loadingViewController: loadingViewController, feed: feed)
            })
        }

        return chatMenuItem
    }

    private func createCommentMenuItem(for feed: Feed) -> MenuItemView {
        let commentMenuItem = MenuItemView()
        
        commentMenuItem.isHidden = self.feedListType == .discover ? true : false

        commentMenuItem.iconImage = Images.commentWhiteIcon
        commentMenuItem.title = feed.commentCount == 0 ? "Comment".localized() : "\(feed.commentCount)"
        commentMenuItem.onViewTapped = {
            let data: CommentSegueData = (shouldShowKeyboard: false, feed: feed)

            self.performSegue(withIdentifier: Segues.showComments, sender: data)
        }

        return commentMenuItem
    }
    
    let repostMenuItem = MenuItemView()
    private func createRepostMenuItem(for feed: Feed) -> MenuItemView {
        
        repostMenuItem.isHidden = self.feedListType == .discover ? true : false

        repostMenuItem.iconImage = Images.repostIcon
        repostMenuItem.title = "Repost".localized()
        
        repostMenuItem.onViewTapped = {
            self.repost(feed: feed)
        }

        return repostMenuItem
    }
    
    private func configureLinkPreview(cell: FeedLinkCollectionViewCell, feed: Feed) {
        guard let linkString = feed.link else {
            fatalError()
        }

        guard let linkURL = URL(string: linkString) else {
            return
        }

        cell.linkPreview.menuItems = self.createMenuItems(cell: cell, for: feed)

        cell.linkPreview.onLinkTapped = {
            UIApplication.shared.open(linkURL, options: [:], completionHandler: nil)
        }

        cell.linkPreview.isBottomRightButtonHidden = false

        cell.linkPreview.bottomRightButtonImage = Images.threeDotsIcon

        cell.linkPreview.onBottomRightButtonTapped = {
            self.showMoreOptions(for: feed.userUID)
        }

        cell.linkPreview.isBottomRightButtonHidden = Services.accountUser?.uid != feed.userUID
    }
    
    private func configurePhotoPreview(cell: FeedCollectionViewCell, feed: Feed) {
        cell.photoPreview.menuItems = self.createMenuItems(cell: cell, for: feed)
        
        cell.photoPreview.isBottomRightButtonHidden = false

        cell.photoPreview.bottomRightButtonImage = Images.threeDotsIcon

        cell.photoPreview.onBottomRightButtonTapped = {
            self.showMoreOptions(for: feed.userUID)
        }
    }

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        self.configureCollectionViewFlowLayout()
        self.configureCollectionView()
        self.configureMainTabBarController()
        self.configureSegmentedControl()

        self.subscribeToFeedEvents()

        self.shouldApplyData = true
        
        ///// ********* Lance's Code ********* /////
        let wasTopRectangeCloudViewGotItButtonPressed = KeychainWrapper.standard.bool(forKey: "wasTopRectangeCloudViewGotItButtonPressed") ?? false
        if !wasTopRectangeCloudViewGotItButtonPressed {
            displayBottomRectangeCloudView()
            NotificationCenter.default.addObserver(self, selector: #selector(removeRectangeCloudViewCallouts), name: Notification.Name(rawValue: "removeRectangeCloudViewCalloutsInFeedViewVC"), object: nil)
        }
        ///// ********* Lance's Code ********* /////
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        if self.shouldApplyData {
            self.apply(feedListType: self.feedListType)
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onApplicationWillEnterForeground(_ :)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        self.collectionView.collectionViewLayout.invalidateLayout()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let destinationViewController = segue.destination

        let dictionaryReceiver: DictionaryReceiver?

        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }

        switch segue.identifier {
        case Segues.showComments:
            
            Log.high("---------- switch case shwocomments parts --------------", from: self)
            
            guard let data = sender as? CommentSegueData, let destinationViewController = segue.destination as? CommentsTableViewController else {
                fatalError()
            }

            destinationViewController.transitioningDelegate = self.commentsPanelTransition
            destinationViewController.modalPresentationStyle = .custom
            
            let commentsDismissHandler: ((_ : CommentsTableViewController.CommentDismissData) -> Void) = { data in
                self.markAsRead(commentUIDs: data.readCommentUIDs, for: data.feed.uid)
            }

            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["shouldShowKeyboard": data.shouldShowKeyboard,
                                                      "feed": data.feed,
                                                      "dismissHandler": commentsDismissHandler])
            }
            
        case Segues.showWriteCommentBox:
            guard let data = sender as? CommentSegueData else {
                fatalError()
            }
            
            let commentsDismissHandler: ((WriteCommentViewController.CommentDismissData) -> Void) = { _ in
                self.refreshFeedList(listType: self.feedListType)
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["feed": data.feed,
                                                      "dismissHandler": commentsDismissHandler])
            }
            
            destinationViewController.transitioningDelegate = self.commentsPanelTransition
            destinationViewController.modalPresentationStyle = .custom
            
        case Segues.showChatMessages:
            guard let destinationViewController = segue.destination as? ChatMessagesRoutingController else {
                fatalError()
            }
            
            if let sender = sender as? (feed: Feed, chatMember: User) {
                destinationViewController.apply(feed: sender.feed, potentialChatMember: sender.chatMember)
            }
            
        default:
            break
        }
    }

    func scrollToTop() {
        self.collectionView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func updateChatMessagesBadge(number: Int) {
        self.chatMessagesBadgeView.isHidden = (number == 0)
        self.chatMessagesBadgeLabel.isHidden = (number == 0)
        
        self.chatMessagesBadgeLabel.text = "\(number)"
    }
}

// MARK: - UICollectionViewDataSource

extension FeedViewController: UICollectionViewDataSource {
    
    // MARK: - Instance Methods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.feeds.isEmpty ? 1 : self.hasNewRatePost ? self.feeds.count : self.feeds.count + Constants.endStateViewCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell
        
        if self.feeds.isEmpty {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.feedEmptyStateCellIdentifier, for: indexPath)
            
            self.configure(feedEmptyStateCell: cell as! FeedEmptyStateCollectionViewCell)
        } else {
            if indexPath.row == self.feeds.count {
                cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.feedEndCollectionCellIdentifier, for: indexPath)
            } else {
                if self.feeds[indexPath.row].link != nil {
                    cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.feedLinkCollectionCellIdentifier, for: indexPath)
                    
                    self.configure(feedLinkCell: cell as! FeedLinkCollectionViewCell, at: indexPath)
                } else {
                    cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.feedCollectionCellIdentifier, for: indexPath)
                    
                    self.configure(feedCell: cell as! FeedCollectionViewCell, at: indexPath)
                }
            }
        }

        return cell
    }
}

// MARK: - Cell Configurations

extension FeedViewController {

    // MARK: - Instance Methods

    private func configure(feedEmptyStateCell: FeedEmptyStateCollectionViewCell) {
        feedEmptyStateCell.addFriendsButtonPressed = {
            guard let pageViewController = self.pageViewController else {
                return
            }
            
            pageViewController.show(controller: .chatsViewController)
        }
    }

    private func configure(feedCell cell: FeedCollectionViewCell, at indexPath: IndexPath) {
        let feed = self.feeds[indexPath.row]
        
        self.configurePhotoPreview(cell: cell, feed: feed)

        cell.isTextLabelHidden = (feed.text == nil)
        cell.text = feed.text
        cell.isViewAllCommentsLabelHidden = self.feedListType == .discover ? true : (feed.commentCount == 0)
        cell.isViewAllCommentsButtonHidden = self.feedListType == .discover ? true : (feed.commentCount == 0)
//        cell.isViewAllCommentsLabelHidden = true
//        cell.isViewAllCommentsButtonHidden = true
        cell.viewAllComments = "\(feed.commentCount) people commented".localized()
        //cell.commentAction = feed.commentCount == 0 ? "Be the first to comment!".localized() : "Add a comment".localized()
        
        cell.isRepostIconHidden = !feed.isRepost

        if feed.isRepost {
            cell.isEngagedFriendsHidden = false
            cell.engagedFriends = feed.userUID == Services.accountUser?.uid ? "Reposted by me" : "A friend reposted"
        } else {
            cell.isEngagedFriendsHidden = (feed.engagedFriendsCount == 0)
            cell.engagedFriends = feed.engagedFriendsCount > 1 ? "\(feed.engagedFriendsCount) friends engaged with this post" : "\(feed.engagedFriendsCount) friend engaged with this post"
        }
        
        if let createdDate = feed.createdDate {
            cell.createdTime = FeedDateFormatter.shared.string(from: createdDate)
        } else {
            cell.createdTime = nil
        }
        //cell.commentAction =  "Chat incognito, then reveal!"
//        print(Services.accountUser?.uid)
//        print(feed.userUID)
//        print("------66666 this is the rate 66666-----")
//        print(feed.rate)
        
//        if Services.accountUser?.uid != feed.userUID || self.feedListType == .discover {
        if self.feedListType == .discover {
            cell.roundedControl.isHidden = true
            repostMenuItem.isHidden = self.feedListType == .discover ? true : false
            cell.roundedControl.backgroundColor = UIColor.clear
            cell.roundedControl.borderColor = UIColor.white
            cell.commentAction =  "Chat incognito..."
            cell.onAddCommentControlTapped = { [unowned self] in
                //let data: CommentSegueData = (shouldShowKeyboard: true, feed: feed)
                //self.performSegue(withIdentifier: Segues.showWriteCommentBox, sender: data)
                //self.performSegue(withIdentifier: Segues.showChatMessages, sender: feed)
                //Segues.showChatMessages
                let loadingViewController = LoadingViewController()
                self.onChatTapped(loadingViewController: loadingViewController, feed: feed)
            }
        } else {
            cell.roundedControl.isHidden = false
//            cell.commentAction =  ""
            //repostMenuItem.isHidden = true
            cell.roundedControl.backgroundColor = UIColor.clear
            cell.roundedControl.borderColor = UIColor.white
            cell.commentAction =  "Chat incognito..."
            cell.onAddCommentControlTapped = { [unowned self] in
                //let data: CommentSegueData = (shouldShowKeyboard: true, feed: feed)
                //self.performSegue(withIdentifier: Segues.showWriteCommentBox, sender: data)
                //self.performSegue(withIdentifier: Segues.showChatMessages, sender: feed)
                //Segues.showChatMessages
                let loadingViewController = LoadingViewController()
                self.onChatTapped(loadingViewController: loadingViewController, feed: feed)
            }
        }
        
        cell.onViewAllCommentsButtonTapped = { [unowned self] in
            let data: CommentSegueData = (shouldShowKeyboard: false, feed: feed)

            self.performSegue(withIdentifier: Segues.showComments, sender: data)
        }
    }
    
    @objc func rateBtnPressed(sender: UIButton) {
        
        let feed = self.feeds[currentFeedIndex]
        
        if feed.isRated { return }
        
        for i in starArray {
            i.setImage(#imageLiteral(resourceName: "RevealStarIcon"), for: .normal)
        }
        rateCount = 0
        for i in starArray
        {
            if i == sender {break}
            rateCount += 1
        }
//        print("hey, star! - \(rateCount)")
        if(rateCount >= 0) {
            for i in 0...rateCount {
                starArray[i].setImage(#imageLiteral(resourceName: "StarIcon"), for: .normal)
            }
        }
        
        let promiseQueue: PromiseQueue

        if let dictionaryPromiseQueue = self.promiseQueues[feed.uid] {
            promiseQueue = dictionaryPromiseQueue
        } else {
            promiseQueue = PromiseQueue(name: "rateFeed \(feed.uid)")

            self.promiseQueues[feed.uid] = promiseQueue
        }
       
        self.putRates(on: feed, queue: promiseQueue)
        
//        feed.rateCount += 1
//        feed.rate += Double(rateCount)
//        self.feeds[currentFeedIndex].isRated = true
//        self.feeds[currentFeedIndex].rateCount = feed.rateCount
//        self.feeds[currentFeedIndex].rate = feed.rate
    }
    
    private func configure(feedLinkCell cell: FeedLinkCollectionViewCell, at indexPath: IndexPath) {
        print("%%%%%%%%%%%%%%%%%%%%%% this is the feedlinkcollectionview cell configure  %%%%%%%%%%%%%%%%%%%%%")
        let feed = self.feeds[indexPath.row]
        
        self.configureLinkPreview(cell: cell, feed: feed)

        cell.isTextLabelHidden = (feed.text == nil)
        cell.text = feed.text
        cell.isViewAllCommentsLabelHidden = self.feedListType == .discover ? true : (feed.commentCount == 0)
        cell.isViewAllCommentsButtonHidden = self.feedListType == .discover ? true : (feed.commentCount == 0)
//        cell.isViewAllCommentsLabelHidden = true
//        cell.isViewAllCommentsButtonHidden = true
        cell.viewAllComments = "\(feed.commentCount) people commented".localized()
        //cell.commentAction = feed.commentCount == 0 ? "Be the first to comment!".localized() : "Add a comment".localized()
        
        cell.isRepostIconHidden = !feed.isRepost

        if feed.isRepost {
            cell.isEngagedFriendsHidden = false
            cell.engagedFriends = feed.userUID == Services.accountUser?.uid ? "Reposted by me" : "A friend reposted"
        } else {
            cell.isEngagedFriendsHidden = (feed.engagedFriendsCount == 0)
            cell.engagedFriends = feed.engagedFriendsCount > 1 ? "\(feed.engagedFriendsCount) friends engaged with this post" : "\(feed.engagedFriendsCount) friend engaged with this post"
        }
        
        if let createdDate = feed.createdDate {
            cell.createdTime = FeedDateFormatter.shared.string(from: createdDate)
        } else {
            cell.createdTime = nil
        }
//        if Services.accountUser?.uid != feed.userUID {
        if self.feedListType == .discover {
            repostMenuItem.isHidden = self.feedListType == .discover ? true : false
            cell.roundedControl.isHidden = true
            cell.roundedControl.backgroundColor = UIColor.clear
            cell.roundedControl.borderColor = UIColor.black
            cell.commentAction =  "Chat incognito..."
            cell.onAddCommentControlTapped = { [unowned self] in
                //let data: CommentSegueData = (shouldShowKeyboard: true, feed: feed)
                //self.performSegue(withIdentifier: Segues.showWriteCommentBox, sender: data)
                //self.performSegue(withIdentifier: Segues.showChatMessages, sender: feed)
                //Segues.showChatMessages
                let loadingViewController = LoadingViewController()
                self.onChatTapped(loadingViewController: loadingViewController, feed: feed)
                
            }
        } else {
            cell.roundedControl.isHidden = false
            cell.commentAction =  ""
            //repostMenuItem.isHidden = true
            //cell.commentAction =  "Thank you for sharing!"
            cell.roundedControl.backgroundColor = UIColor.clear
            cell.roundedControl.borderColor = UIColor.black
            cell.commentAction =  "Chat incognito..."
            cell.onAddCommentControlTapped = { [unowned self] in
                //let data: CommentSegueData = (shouldShowKeyboard: true, feed: feed)
                //self.performSegue(withIdentifier: Segues.showWriteCommentBox, sender: data)
                //self.performSegue(withIdentifier: Segues.showChatMessages, sender: feed)
                //Segues.showChatMessages
                let loadingViewController = LoadingViewController()
                self.onChatTapped(loadingViewController: loadingViewController, feed: feed)
                
            }
            
        }
        
        cell.onViewAllCommentsButtonTapped = { [unowned self] in
            let data: CommentSegueData = (shouldShowKeyboard: false, feed: feed)

            self.performSegue(withIdentifier: Segues.showComments, sender: data)
        }
    }
    
    private func setupLinkPreview(cell: FeedLinkCollectionViewCell, at indexPath: IndexPath) {
        let feed = self.feeds[indexPath.row]
        
        guard let link = feed.link else {
            return
        }
        
        guard let linkURL = URL(string: link) else {
            return
        }
        
        cell.linkPreview.isLoadingViewHidden = false
        
        Managers.linkPreviewManager.getInformationFrom(link: linkURL, completion: { linkString, linkDescription, imageURLString, error in
            if let imageURLString = imageURLString, let imageURL = URL(string: imageURLString) {
                Services.imageLoader.loadImage(for: imageURL, in: cell.linkPreview.linkImageViewTarget, placeholder: nil, completionHandler: { image in
                    cell.linkPreview.isLoadingViewHidden = true
                })
            }
            
            DispatchQueue.main.async {
                cell.linkPreview.linkDescription = linkDescription
                cell.linkPreview.linkURL = linkString
            }
        })
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension FeedViewController: UICollectionViewDelegateFlowLayout {

    // MARK: - Instance Methods

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? FeedCollectionViewCell {
            currentFeedIndex = indexPath.row
            let feed = self.feeds[currentFeedIndex]
            if let imageURL = self.feeds[currentFeedIndex].photo?.imageURL {
                print("------ this is the feedViewController -----")
                
                Services.imageLoader.loadImageProgressive(for: imageURL, in: cell.photoPreview.imageViewTarget)
            }
            
            // ... Stars for Rating
            
            starArray.removeAll()
            rateCount = 0
            starArray.append(cell.rate10)
            starArray.append(cell.rate9)
            starArray.append(cell.rate8)
            starArray.append(cell.rate7)
            starArray.append(cell.rate6)
            starArray.append(cell.rate5)
            starArray.append(cell.rate4)
            starArray.append(cell.rate3)
            starArray.append(cell.rate2)
            starArray.append(cell.rate1)
            for i in 0...9 {
                starArray[i].setImage(#imageLiteral(resourceName: "RevealStarIcon"), for: .normal)
            }
            var overRate = feed.rateCount == 0 ? 0 : feed.rate/Double(feed.rateCount)
            if overRate >= 10.0 { overRate=10.0 }
            
            cell.rateScoreView.isHidden = true
            cell.rateScoreLabel.isHidden = true
            
            // ----- 10 rate tip part
            if self.feedListType != .discover || feed.isRated || Managers.userDefaultsManager.isUserHasSeenTenRateToRequestFriend {
                cell.rateTenTipView.isHidden = true
            }
            
            if feed.isRated && feed.rateCount > 0 && round(overRate) >= 1 {
//                print("----------- this is the rating information for yellow stars -------------")
                var end_val = Int(round(overRate))-1
                if end_val > 9 { end_val = 9 }
                for i in 0...end_val {
                    rateCount += 1
                    starArray[i].setImage(#imageLiteral(resourceName: "StarIcon"), for: .normal)
                }
            }

            for i in starArray
            {
                i.addTarget(self, action: #selector(rateBtnPressed(sender:)), for: .touchUpInside)
            }
            

            cell.RateView.applyGradient(isVertical: true, colorArray: [#colorLiteral(red: 0.9921568627, green: 0.4509803922, blue: 0.5411764706, alpha: 0.9), #colorLiteral(red: 0.9921568627, green: 0.7921568627, blue: 0.3333333333, alpha: 0.9)])
//            cell.RateView.backgroundColor = #colorLiteral(red: 0.9921568627, green: 0.4509803922, blue: 0.5411764706, alpha: 0.9)
            cell.RateView.layer.borderWidth = 2.5
            cell.RateView.layer.masksToBounds = true
            cell.RateView.layer.borderColor = UIColor.white.withAlphaComponent(0.9).cgColor
            cell.RateView.layer.cornerRadius = 16
            cell.RateView.isHidden = self.feedListType == .discover ? false : true
            
            // this is the rated part gradient ----
            if (feed.isRated && feed.rateCount > 0) {
                cell.noneRateView.bounds.size.width = 32
                cell.noneRateView.layer.masksToBounds = true
                cell.noneRateView.layer.cornerRadius = 16
//                print("Feed.RateCount: \(feed.rateCount)")
                let starHeight: CGFloat = 32 * CGFloat(overRate)
                cell.noneRateView.center.y = 320 - starHeight/2
                cell.noneRateView.bounds.size.height = starHeight
//                print(starHeight)
                //                cell.noneRateView.backgroundColor = UIColor.green
//                print("---------------- this is the rated area filled with gradient -------------------")
                cell.noneRateView.applyGradient(isVertical: true, colorArray: [#colorLiteral(red: 0.9921568627, green: 0.7921568627, blue: 0.3333333333, alpha: 1), #colorLiteral(red: 0.9921568627, green: 0.5568627451, blue: 0.4784313725, alpha: 1)])
            } else {
                cell.noneRateView.bounds.size.height = 0
            }
            
            // ----- rate scroe label and view part -----
//            print("*********************************** feedListType ****************************")
//            print(self.feedListType)
            if self.feedListType == .discover && feed.rateCount > 0 && feed.isRated {
                cell.rateScoreLabel.text = String(round(overRate * 10)/10)
                cell.rateScoreView.center.y = CGFloat(320.0 - 32*overRate)
                cell.rateScoreView.isHidden = false
                cell.rateScoreLabel.isHidden = false
            } else {
                cell.rateScoreView.isHidden = true
            }
            
        } else if let cell = cell as? FeedLinkCollectionViewCell {
            self.setupLinkPreview(cell: cell, at: indexPath)
        }
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? FeedCollectionViewCell {
            Services.imageLoader.cancelLoading(in: cell.photoPreview.imageViewTarget)
        } else if let cell = cell as? FeedLinkCollectionViewCell {
            Services.imageLoader.cancelLoading(in: cell.linkPreview.linkImageViewTarget)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < self.feeds.count {
            /// changed by lava on 0119, removed this for show comments when tapped
            Log.high("******************* switch case shwocomments parts --------------", from: self)
            if (self.feedListType != .discover) {
                 let feed = self.feeds[indexPath.row]
                           
                              let data: CommentSegueData = (shouldShowKeyboard: false, feed: feed)
                           
                            self.performSegue(withIdentifier: Segues.showComments, sender: data)
                
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        self.calculateItemSize(for: indexPath.row)
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        self.pagingManager?.contentViewWillEndDragging(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.pagingManager?.contentViewWillBeginDragging(scrollView)
    }
}

