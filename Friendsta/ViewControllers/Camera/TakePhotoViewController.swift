//
//  TakePhotoViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10.10.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import PromiseKit
import SVProgressHUD
import FriendstaTools

class TakePhotoViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    enum Segues {
        
        // MARK: - Type Properties
        
        static let openTextReply = "OpenTextReply"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var captureView: UIView!
    
    @IBOutlet private weak var emptyStateContainerView: UIView!
    @IBOutlet private weak var emptyStateView: EmptyStateView!
    
    @IBOutlet private weak var flashModeButton: CameraFlashButton!
    
    @IBOutlet private weak var libraryButton: UIButton!
    @IBOutlet private weak var captureButton: UIButton!
    @IBOutlet private weak var closeButton: UIButton!
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var writeAPostButton: UIButton!
    @IBOutlet private weak var writeAPostShapeImageView: UIImageView!
    @IBOutlet private weak var writeAPostLabel: UILabel!

    @IBOutlet private weak var captureButtonBottomConstraint: NSLayoutConstraint!

    @IBOutlet private weak var swipeRightView: UIView!
    // MARK: -
    
    private let captureSession = AVCaptureSession()
    
    private var frontCameraDevice: AVCaptureDevice?
    private var backCameraDevice: AVCaptureDevice?
    
    private var frontCameraInput: AVCaptureDeviceInput?
    private var backCameraInput: AVCaptureDeviceInput?
    
    private var capturePhotoOutput: AVCapturePhotoOutput?
    private var capturePreviewLayer: AVCaptureVideoPreviewLayer?
    
    private var cameraPosition: AVCaptureDevice.Position = .back
    
    private var transitionAnimation: CustomLeftToRightTransition!
    
    private var propCard: PropCard?

    private var source: PhotoCreationSource = .feed
    
    // MARK: -
    
    var onPhotoTaken: ((_ photoImage: UIImage) -> Void)?
    var onCanceled: (() -> Void)?
    var onMakePhotoClicked: (() -> Void)?
    var onBack: (() -> Void)?
    var onWriteAPostTapped: (() -> Void)?
    
    // MARK: - UIViewController
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onTextReplySendingFinished(segue: UIStoryboardSegue) {
        Log.high("onTextReplySendingFinished(withSegue: \(String(describing: segue.identifier)))", from: self)
        
        self.dismiss(animated: true, completion: nil)
        self.showRepliedState()
    }
    
    @IBAction private func onSwipeRight(_ sender: Any) {
        Log.high("onSwipeRight()", from: self)
        
        self.performSegue(withIdentifier: Segues.openTextReply, sender: nil)
    }
    
    @IBAction private func onWriteAPostButtonTouchUpInside(_ sender: Any) {
        Log.high("onWriteAPostButtonTouchUpInside()", from: self)
        self.onWriteAPostTapped?()
    }

    @IBAction private func onCloseButtonTouchUpInside(_ sender: Any) {
        Log.high("onCloseButtonTouchUpInside()", from: self)
        
        self.onCanceled?()
    }
    
    @IBAction private func onFlashModeButtonTouchUpInside(_ sender: Any) {
        Log.high("onFlashModeButtonTouchUpInside()", from: self)
        
        switch self.flashModeButton.mode {
        case .on:
            self.flashModeButton.mode = .off
            
        case .off:
            self.flashModeButton.mode = .on
        }
    }
    
    @IBAction private func onLibraryButtonTouchUpInside(_ sender: Any) {
        Log.high("onLibraryButtonTouchUpInside()", from: self)
        
        self.pickPhotoImage()
    }
    
    @IBAction private func onCaptureButtonTouchUpInside(_ sender: Any) {
        Log.high("onCaptureButtonTouchUpInside", from: self)
        
        self.capturePhoto()
        
    }
    
    @IBAction private func onSwitchButtonTouchUpInside(_ sender: Any) {
        Log.high("onSwitchButtonTouchUpInside()", from: self)
        
        self.captureSession.beginConfiguration()
        
        switch self.cameraPosition {
        case .back:
            self.switchCamera(to: .front)
            
        case .front:
            self.switchCamera(to: .back)
            
        case .unspecified:
            break
            
        @unknown default:
            break
        }
        
        self.captureSession.commitConfiguration()
    }
    
    @IBAction private func onBackButtonTouchUpInside(_ sender: UIButton) {
        self.onBack?()
    }
  
    // MARK: -
    
    private func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()
        
        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action
        
        self.emptyStateContainerView.isHidden = false
    }
    
    private func hideEmptyState() {
        self.emptyStateContainerView.isHidden = true
    }
    
    private func showCaptureButton() {
        self.captureButton.isHidden = false
    }
    
    private func hideCaptureButton() {
        self.captureButton.isHidden = true
    }
    
    private func showNoCameraAccessState() {
        let action = EmptyStateAction(title: "Allow access to camera".localized(), type: .primaryWhite, onClicked: { [unowned self] in
            switch Services.mediaService.cameraAccessState {
            case .notDetermined:
                self.requestCameraAccess()
                
            case .denied, .restricted:
                self.showCameraAccessRequest()
                
            case .authorized:
                self.updateCaptureSession()
            }
        })
        
        self.showEmptyState(title: "Share updates!".localized(),
                            message: "Your privacy is important. We use camera to take a photo.".localized(),
                            action: action)
    }
    
    private func showCameraAccessRequest() {
        let alertController = UIAlertController(title: #""Nicely" Would Like to Access Your Camera"#.localized(),
                                                message: "This lets you share photos from your library and save photos...".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.blackText
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        
        self.present(alertController, animated: true)
    }
    
    private func showLibraryAccessRequest() {
        let alertController = UIAlertController(title: #""Nicely" Would Like to Access Your Photos"#.localized(),
                                                message: "This lets you share photos from your library and save photos...".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.blackText
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        
        self.present(alertController, animated: true)
    }
    
    private func showRepliedState() {
        SVProgressHUD.showSuccess(withStatus: "Replied!".localized())
        SVProgressHUD.dismiss(withDelay: 2)
    }
    
    // MARK: -
    
    private func createFrontCameraDevice() {
        guard self.frontCameraDevice == nil else {
            return
        }
        
        let frontCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .front)
        
        if let frontCameraDevice = frontCameraDevice {
            self.frontCameraDevice = frontCameraDevice
            
            do {
                self.frontCameraInput = try AVCaptureDeviceInput(device: frontCameraDevice)
            } catch let error {
                Log.high("createFrontCameraDevice() :: Error: \(error.localizedDescription)", from: self)
            }
        }
    }
    
    private func createBackCameraDevice() {
        guard self.backCameraDevice == nil else {
            return
        }
        
        let backCameraDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
        
        if let backCameraDevice = backCameraDevice {
            self.backCameraDevice = backCameraDevice
            
            if backCameraDevice.isFocusModeSupported(.continuousAutoFocus) {
                if (try? backCameraDevice.lockForConfiguration()) != nil {
                    backCameraDevice.focusMode = .continuousAutoFocus
                    
                    backCameraDevice.unlockForConfiguration()
                }
            }
            
            do {
                self.backCameraInput = try AVCaptureDeviceInput(device: backCameraDevice)
            } catch let error {
                Log.high("createBackCameraDevice() :: Error: \(error.localizedDescription)", from: self)
            }
        }
    }
    
    private func createCapturePreviewLayer() {
        if let capturePreviewLayer = self.capturePreviewLayer {
            capturePreviewLayer.removeFromSuperlayer()
        }
        
        let capturePreviewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        
        capturePreviewLayer.frame = self.captureView.bounds
        
        capturePreviewLayer.videoGravity = .resizeAspectFill
        capturePreviewLayer.connection?.videoOrientation = .portrait
        
        self.capturePreviewLayer = capturePreviewLayer
        
        self.captureView.layer.addSublayer(capturePreviewLayer)
    }
    
    private func createCapturePhotoOutput() {
        guard self.capturePhotoOutput == nil else {
            return
        }
        
        let capturePhotoOutput = AVCapturePhotoOutput()
        
        self.capturePhotoOutput = capturePhotoOutput
        
        if self.captureSession.canAddOutput(capturePhotoOutput) {
            self.captureSession.addOutput(capturePhotoOutput)
        }
    }
    
    private func updateCaptureSession() {
        switch Services.mediaService.cameraAccessState {
        case .notDetermined, .denied, .restricted:
            self.showNoCameraAccessState()
            self.hideCaptureButton()
            
        case .authorized:
            self.hideEmptyState()
            self.showCaptureButton()
            
            guard !self.captureSession.isRunning else {
                return
            }

            self.captureSession.sessionPreset = .high
            
            self.createFrontCameraDevice()
            self.createBackCameraDevice()
            
            self.createCapturePhotoOutput()
            self.createCapturePreviewLayer()

            if let frontCameraInput = self.frontCameraInput, self.captureSession.canAddInput(frontCameraInput) {
                self.captureSession.addInput(frontCameraInput)
            }
            
            self.switchCamera(to: self.cameraPosition)
            
            guard (!self.captureSession.inputs.isEmpty) && (!self.captureSession.outputs.isEmpty) else {
                return
            }
            
            DispatchQueue.global(qos: .userInitiated).async(execute: {
                self.captureSession.startRunning()
            })
        }
    }
    
    private func configureLibraryButton() {
        self.libraryButton.backgroundColor = .clear
        self.libraryButton.layer.cornerRadius = 6.0
        self.libraryButton.imageView?.layer.cornerRadius = 6.0
    }
    
    private func updateLibraryButton() {
        switch Services.mediaService.libraryAccessState {
        case .notDetermined, .denied, .restricted:
            self.libraryButton.layer.borderWidth = 0.0
            self.libraryButton.layer.borderColor = UIColor.clear.cgColor
            
        case .authorized:
            let fetchOptions = PHFetchOptions()
            
            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
            fetchOptions.fetchLimit = 1
            
            let fetchedAssets = PHAsset.fetchAssets(with: .image, options: fetchOptions)
            
            if let firstFetchedAsset = fetchedAssets.firstObject {
                PHImageManager.default().requestImage(for: firstFetchedAsset,
                                                      targetSize: self.libraryButton.bounds.size,
                                                      contentMode: .aspectFit,
                                                      options: nil,
                                                      resultHandler: { [weak self] image, info in
                    self?.libraryButton.setImage(image, for: .normal)
                    self?.libraryButton.layer.borderWidth = 2.0
                    self?.libraryButton.layer.borderColor = UIColor.white.cgColor
                })
            } else {
                self.libraryButton.setImage(Images.galleryIcon, for: .normal)
            }
        }
    }

    private func updateSwipeRightView() {
        self.swipeRightView.isHidden = self.source != .reply
    }

    private func configureCloseButton() {
        if self.source == .profile {
            self.closeButton.setImage(Images.backArrowButton, for: .normal)
        } else {
            self.closeButton.setImage(Images.closeButton, for: .normal)
        }
        
        self.closeButton.isHidden = !(self.source == .reply || self.source == .menu || self.source == .profile || self.source == .chat)
    }

    private func configureBackButton() {
        self.backButton.isHidden = (self.onBack == nil)
    }

    private func configureWriteAPostButton() {
        if self.onWriteAPostTapped != nil {
            self.writeAPostButton.isHidden = false
            self.writeAPostLabel.isHidden = Managers.userDefaultsManager.isUserHasSeenWriteAPostMessage
            self.writeAPostShapeImageView.isHidden = Managers.userDefaultsManager.isUserHasSeenWriteAPostMessage

            Managers.userDefaultsManager.isUserHasSeenWriteAPostMessage = true
        } else {
            self.writeAPostButton.isHidden = true
            self.writeAPostLabel.isHidden = true
            self.writeAPostShapeImageView.isHidden = true
        }
    }
    
    // MARK: -
    
    private func requestCameraAccess() {
        Log.high("requestCameraAccess()", from: self)
        
        firstly {
            Services.mediaService.requestCameraAccess()
        }.done { accessState in
            switch accessState {
            case .notDetermined, .denied, .restricted:
                self.showNoCameraAccessState()
                
            case .authorized:
                self.updateCaptureSession()
            }
        }
    }
    
    private func requestLibraryAccess() {
        Log.high("requestLibraryAccess()", from: self)
        
        firstly {
            Services.mediaService.requestLibraryAccess()
        }.done { accessState in
            switch accessState {
            case .notDetermined, .denied, .restricted:
                break
                
            case .authorized:
                self.pickPhotoImage()
            }
        }
    }
    
    private func pickPhotoImage() {
        Log.high("pickPhotoImage()", from: self)
        
        switch Services.mediaService.libraryAccessState {
        case .notDetermined:
            self.requestLibraryAccess()
            
        case .denied, .restricted:
            self.showLibraryAccessRequest()
            
        case .authorized:
            let imagePickerController = ImagePickerController()
            
            imagePickerController.onImagePicked = { [unowned imagePickerController] photoImage in
                imagePickerController.dismiss(animated: true, completion: { [unowned self] in
                    self.onPhotoTaken?(photoImage)
                })
            }
            
            imagePickerController.apply(mediaSource: .photoLibrary, allowsEditing: false)
            
            self.present(imagePickerController, animated: true)
        }
    }
    
    private func switchCamera(to cameraPosition: AVCaptureDevice.Position) {
        Log.high("switchCamera(to: \(cameraPosition))", from: self)
        
        self.cameraPosition = cameraPosition
        
        switch cameraPosition {
        case .back:
            if let frontCameraInput = self.frontCameraInput {
                self.captureSession.removeInput(frontCameraInput)
            }
            
            if let backCameraInput = self.backCameraInput, self.captureSession.canAddInput(backCameraInput) {
                self.captureSession.addInput(backCameraInput)
            }
            
        case .front:
            if let backCameraInput = self.backCameraInput {
                self.captureSession.removeInput(backCameraInput)
            }
            
            if let frontCameraInput = self.frontCameraInput, self.captureSession.canAddInput(frontCameraInput) {
                self.captureSession.addInput(frontCameraInput)
            }
            
        case .unspecified:
            fatalError()

        @unknown default:
            fatalError()
        }
    }

    // MARK: -
    
    func apply(propCard: PropCard?, source: PhotoCreationSource) {
        Log.high("apply(propCard: \(String(describing: propCard?.uid))", from: self)
        
        self.propCard = propCard
        self.source = source
        
        self.cameraPosition = (self.source == .reply) ? .front : .back
    }

    func capturePhoto() {
        Log.high("capturePhoto()", from: self)

        guard self.captureSession.isRunning else {
            return
        }

        let settings = AVCapturePhotoSettings()

        switch self.flashModeButton.mode {
        case .on:
            settings.flashMode = .on

        case .off:
            settings.flashMode = .off
        }

        self.capturePhotoOutput?.capturePhoto(with: settings, delegate: self)

        self.captureButton.isEnabled = false
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateCaptureSession()
        self.updateLibraryButton()
        self.updateSwipeRightView()
        
        self.configureLibraryButton()
        self.configureCloseButton()
        self.configureBackButton()
        self.configureWriteAPostButton()
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.setNeedsStatusBarAppearanceUpdate()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.capturePreviewLayer?.frame = self.captureView.bounds
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.openTextReply:
            guard let destination = segue.destination as? TextReplyViewController else {
                return
            }
            
            self.transitionAnimation = CustomLeftToRightTransition()
            
            destination.transitioningDelegate = self.transitionAnimation
            
            guard let propCard = self.propCard else {
                fatalError()
            }
            
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            dictionaryReceiver?.apply(dictionary: ["propCard": propCard])
            
        default:
            return
        }
    }
}

// MARK: - AVCapturePhotoCaptureDelegate

extension TakePhotoViewController: AVCapturePhotoCaptureDelegate {
    
    // MARK: - Instance Methods

    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let imageData = photo.fileDataRepresentation(), let photoImage = UIImage(data: imageData), let cgImage = photoImage.cgImage {
            if self.cameraPosition == .front {
                let flippedImage = UIImage(cgImage: cgImage, scale: photoImage.scale, orientation: .leftMirrored)
                
                self.onPhotoTaken?(flippedImage)
            } else {
                self.onPhotoTaken?(photoImage)
            }
        }

        self.captureButton.isEnabled = true
    }
}

// MARK: - DictionaryReceiver

extension TakePhotoViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods

    func apply(dictionary: [String: Any]) {
        let propCard = dictionary["propCard"] as? PropCard

        guard let source = dictionary["source"] as? PhotoCreationSource else {
            return
        }
        
        self.apply(propCard: propCard, source: source)
    }
}
