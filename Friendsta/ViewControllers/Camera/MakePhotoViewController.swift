//
//  MakePhotoViewController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 24/10/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import SVProgressHUD
import FriendstaTools
import FriendstaNetwork

class MakePhotoViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Type Properties
    
    static let storyboardIdentifier = "MakePhotoViewController"
    
    // MARK: - Nested Types
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let embedPhotoTaking = "EmbedPhotoTaking"
        static let embedPhotoEditing = "EmbedPhotoEditing"
        
        static let cancelPhotoMaking = "CancelPhotoMaking"
        
        static let finishPhotoPosting = "FinishPhotoPosting"
        
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var photoTakingContainerView: UIView!
    @IBOutlet private weak var photoEditingContainerView: UIView!
    
    private weak var takePhotoViewController: TakePhotoViewController!
    private weak var editPhotoViewController: EditPhotoViewController!
    /// added by lava on 1013
    private weak var sendToVC: SendToVC!
    
    // MARK: -

    var onBack: (() -> Void)?
    var onWriteAPostTapped: (() -> Void)?

    // MARK: -
    
    private var source: PhotoCreationSource = .feed
    private var photoImage: UIImage?
    private var shouldShowTakePhotoViewController = true
    
    private var isIncognitoFeedPosting: Bool?
    
    // MARK: - UIViewController
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Instance Methods
    
    private func onEmbedPhotoTaking(segue: UIStoryboardSegue, sender: Any?) {
        guard let takePhotoViewController = segue.destination as? TakePhotoViewController else {
            fatalError()
        }

        self.takePhotoViewController = takePhotoViewController
        self.takePhotoViewController.apply(propCard: nil, source: self.source)

        self.takePhotoViewController.onBack = self.onBack
        self.takePhotoViewController.onWriteAPostTapped = self.onWriteAPostTapped

        self.takePhotoViewController.onPhotoTaken = { [unowned self] photoImage in
            self.editPhotoViewController.apply(source: self.source, photoImage: photoImage)
            self.photoEditingContainerView.alpha = 0.0
            
            self.showPhotoEditingContainerView()
        }

        self.takePhotoViewController.onCanceled = { [unowned self] in
            if self.source == .profile {
                self.navigationController?.popViewController(animated: true)
            } else {
                self.dismiss(animated: true)
            }
        }
    }
    
    private func onEmbedPhotoEditing(segue: UIStoryboardSegue, sender: Any?) {
        guard let editPhotoViewController = segue.destination as? EditPhotoViewController else {
            fatalError()
        }
        
        self.editPhotoViewController = editPhotoViewController
        self.editPhotoViewController.hidesBottomBarWhenPushed = true

        self.editPhotoViewController.onBack = self.onBack
        
        self.editPhotoViewController.apply(source: self.source)
        
        self.editPhotoViewController.onPhotoEdited = { [unowned self] photoImage, isIncognito in
            self.isIncognitoFeedPosting = isIncognito
            self.performAction(with: photoImage)
        }
        
        self.editPhotoViewController.onCanceled = { [unowned self] in
            if self.shouldShowTakePhotoViewController {
                self.hidePhotoEditingContainerView()
            } else {
                self.dismiss(animated: true)
            }
        }
    }
    
    // MARK: -

    private func hidePhotoEditingContainerView() {
        self.photoTakingContainerView.isHidden = false

        UIView.animate(withDuration: 0.25, animations: {
            self.photoEditingContainerView.alpha = 0.0
            self.tabBarController?.tabBar.isHidden = false
        }, completion: { finished in
            self.photoEditingContainerView.isHidden = true
        })
    }
    
    private func hidePhotoTakingContainerView() {
        self.photoEditingContainerView.isHidden = false

        UIView.animate(withDuration: 0.25, animations: {
            self.photoTakingContainerView.alpha = 0.0
            self.tabBarController?.tabBar.isHidden = false
        }, completion: { finished in
            self.photoTakingContainerView.isHidden = true
        })
    }
    
    private func showPhotoEditingContainerView() {
        self.photoEditingContainerView.isHidden = false

        UIView.animate(withDuration: 0.25, animations: {
            self.photoEditingContainerView.alpha = 1.0
            self.tabBarController?.tabBar.isHidden = true
        }, completion: { finished in
            self.photoTakingContainerView.isHidden = true
        })
    }

    // MARK: -
    
    private func showSuccessState(completion: @escaping SVProgressHUDDismissCompletion) {
        SVProgressHUD.dismiss()
        SVProgressHUD.setImageViewSize(CGSize(width: 200.0, height: 0.0))
        SVProgressHUD.show(UIImage(), status: "Photo shared to all your friends".localized())
        SVProgressHUD.dismiss(withDelay: 1.0, completion: completion)
    }
    
    // MARK: -
    
    private func apply(source: PhotoCreationSource) {
        Log.high("apply(source: \(source))", from: self)
        
        self.shouldShowTakePhotoViewController = true
        
        self.source = source
        
        if self.isViewLoaded {
            self.hidePhotoEditingContainerView()
        }
    }
    
    private func apply(source: PhotoCreationSource, photoImage: UIImage) {
        Log.high("apply(source: \(source), photoImage: \(photoImage.size))", from: self)
        
        self.shouldShowTakePhotoViewController = false
        
        self.source = source
        self.photoImage = photoImage
        
        if self.isViewLoaded {
            self.hidePhotoTakingContainerView()
            
            self.editPhotoViewController.apply(source: source, photoImage: photoImage)
        }
    }
    
    // MARK: -
    
    func performAction(with photoImage: UIImage) {
        Log.high("performAction(withPhotoImage: \(photoImage.size))", from: self)

        SVProgressHUD.show()

        DispatchQueue.global(qos: .userInitiated).async(execute: {
            var photo = photoImage
            
            if photoImage.size.width > Limits.photoImageMaxWidth {
                if let photoImage = photoImage.scaled(to: Limits.photoImageMaxWidth) {
                    photo = photoImage
                }
            }
            
            firstly {
                Services.feedService.sendFeed(photo: photo, isIncognito: self.isIncognitoFeedPosting ?? true)
            }.ensure {
                SVProgressHUD.dismiss()
            }.done { feed in
                if let imageURL = feed.photo?.imageURL {
                    Services.imageLoader.saveImage(for: imageURL, image: photo)
                }
                
                if self.source == .menu || self.source == .profile {
                    self.performSegue(withIdentifier: Segues.finishPhotoPosting, sender: self)
                } else {
                    self.onBack?()
                }

                self.hidePhotoEditingContainerView()
            }.catch { error in
                self.handle(actionError: error)
            }
        })
    }
    
    private func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.barTintColor = UIColor.clear
        self.tabBarController?.tabBar.backgroundImage = UIImage()
        self.tabBarController?.tabBar.shadowImage = UIImage()
        self.navigationController?.isNavigationBarHidden = true
        self.setNeedsStatusBarAppearanceUpdate()
        
        if let photoImage = self.photoImage {
            self.apply(source: self.source, photoImage: photoImage)
        } else {
            self.apply(source: self.source)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.barTintColor = UIColor(red: 90/255.0, green: 90/255.0, blue: 90/255.0)
        self.tabBarController?.tabBar.backgroundImage = nil
        self.tabBarController?.tabBar.shadowImage = nil
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.embedPhotoTaking:
            self.onEmbedPhotoTaking(segue: segue, sender: sender)
            
        case Segues.embedPhotoEditing:
            self.onEmbedPhotoEditing(segue: segue, sender: sender)
            
        default:
            break
        }
    }
}

// MARK: - DictionaryReceiver

extension MakePhotoViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    @objc func apply(dictionary: [String: Any]) {
        guard let source = dictionary["source"] as? PhotoCreationSource else {
            return
        }
        
        if let photoImage = dictionary["photoImage"] as? UIImage {
            self.apply(source: source, photoImage: photoImage)
        } else {
            self.apply(source: source)
        }
    }
}
