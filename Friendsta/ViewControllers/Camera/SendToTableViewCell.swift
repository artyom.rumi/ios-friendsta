//
//  SendToTableViewCell.swift
//  Friendsta
//
//  Created by 图娜福尔 on 2020/10/10.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

class SendToTableViewCell: UITableViewCell {
    
    @IBOutlet weak var publicImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var checkImg: UIImageView!    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
