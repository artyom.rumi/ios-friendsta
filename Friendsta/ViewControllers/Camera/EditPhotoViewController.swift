//
//  EditPhotoViewController.swift
//  Friendsta
//
//  Created by Damir Zaripov on 15/10/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class EditPhotoViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let sendPhoto = "SendPhoto"
    }
    
    // MARK: -
    
    private enum CGColors {
        
        // MARK: - Type Properties
        
        static let anonymousPostingGuideView = UIColor(redByte: 29, greenByte: 124, blueByte: 255).cgColor
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let anonymousPostingGuideViewTitle = "Switch it off if you want to share to your timeline".localized()
        static let anonymousPostingGuideViewButtonTitle = "Got it!".localized()
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var photoImageView: UIImageView!

    @IBOutlet private weak var performButton: UIButton!

    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var addTextButton: UIButton!
    @IBOutlet private weak var backButton: UIButton!

    @IBOutlet private weak var textViewTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var textViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var draggingAreaView: UIView!
    @IBOutlet private weak var deletionAreaView: UIView!
    
    @IBOutlet private weak var anonymousPostingSwitch: UISwitch!
    @IBOutlet private weak var anonymousPostingLabel: UILabel!
    @IBOutlet private weak var anonymousPostingGuideView: RectangeCloudView!
    @IBOutlet private weak var anonymousPostingSwitchTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var avatarImageView: RoundImageView!
    
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var schoolInfoLabel: UILabel!
    
    // MARK: -
    
    private var textViewTouchPosition: CGPoint?
    
    // MARK: -
    
    private(set) var photoImage: UIImage?
    
    private(set) var hasAppliedData = false
    
    private var source: PhotoCreationSource?
    
    private var isAnonymousPosting: Bool {
        return (self.source == .chat || (self.source == .profile ? false : self.anonymousPostingSwitch.isOn))
    }
    
    // MARK: -
    
    var onPhotoEdited: ((_ photoImage: UIImage, _ isIncognito: Bool?) -> Void)?
    var onCanceled: (() -> Void)?
    var onBack: (() -> Void)?

    /// added by lava on 1012
    var renderedImage: UIImage = UIImage()
    
    // MARK: - UIViewController
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSendToVC" {
            let controller = segue.destination as! SendToVC
            controller.editedImage = self.renderedImage ?? UIImage()
            controller.isAnnoymous = self.isAnonymousPosting
        }
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onAnonymityStateSwitchValueChanged(_ sender: Any) {
        Log.high("onAnonymityStateSwitchValueChanged()", from: self)
        
        self.anonymousPostingLabel.alpha = self.anonymousPostingSwitch.isOn ? 1 : 0.5
        
        self.avatarImageView.isHidden = self.anonymousPostingSwitch.isOn
        self.fullNameLabel.isHidden = self.anonymousPostingSwitch.isOn
        self.schoolInfoLabel.isHidden = self.anonymousPostingSwitch.isOn
    }
    
    @IBAction private func onPhotoImageViewTapGestureRecognized(_ sender: Any) {
        Log.high("onPhotoImageViewTapGestureRecognized", from: self)
        
        self.textView.resignFirstResponder()
    }
    
    @IBAction private func onTextViewPanGestureRecognized(_ sender: UIPanGestureRecognizer) {
        guard !self.textView.isFirstResponder else {
            return
        }
        
        let touchPosition = sender.location(in: self.view)
        
        switch sender.state {
        case .began:
            self.textViewTouchPosition = touchPosition
            
            self.showTextDraggingState()
            
        case .changed:
            if let textViewTouchPosition = self.textViewTouchPosition {
                self.textViewTopConstraint.constant += touchPosition.y - textViewTouchPosition.y
            } else {
                self.showTextDraggingState()
            }
            
            if let textViewTouchPosition = self.textViewTouchPosition {
                if self.deletionAreaView.frame.contains(touchPosition) {
                    if !self.deletionAreaView.frame.contains(textViewTouchPosition) {
                        self.activateDeletionArea()
                    }
                } else {
                    if self.deletionAreaView.frame.contains(textViewTouchPosition) {
                        self.deactivateDeletionArea()
                    }
                }
            } else {
                if self.deletionAreaView.frame.contains(touchPosition) {
                    self.activateDeletionArea()
                }
            }
            
            self.textViewTouchPosition = touchPosition
            
        case .ended:
            self.textViewTouchPosition = nil
            
            self.hideTextDraggingState()
         
            if self.deletionAreaView.frame.contains(touchPosition) {
                self.deleteText()
            }
            
        case .cancelled, .failed, .possible:
            self.textViewTouchPosition = nil
            
            self.hideTextDraggingState()
        }
    }
    
    @IBAction fileprivate func onCloseButtonTouchUpInside(_ sender: Any) {
        Log.high("onCloseButtonTouchUpInside()", from: self)

        if !self.textView.isHidden {
            let alertController = UIAlertController(title: "Are you sure?".localized(),
                                                    message: "You are about to delete. You will lose your edits you made.".localized(),
                                                    preferredStyle: .alert)

            let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)

            let deleteAction = UIAlertAction(title: "Delete".localized(), style: .destructive, handler: { [unowned self] action in
                self.onCanceled?()
            })

            alertController.addAction(cancelAction)
            alertController.addAction(deleteAction)

            self.present(alertController, animated: true)
        } else {
            self.onCanceled?()
        }
    }
    
    @IBAction private func onAddTextButtonTouchUpInside(_ sender: Any) {
        Log.high("onAddTextButtonTouchUpInside()", from: self)
        
        if self.textView.isHidden {
            self.showTextView()
        } else {
            self.textView.becomeFirstResponder()
        }
    }
    
    @IBAction private func onPerformButtonTouchUpInside(_ sender: Any) {
        Log.high("onPerformButtonTouchUpInside", from: self)
        
//        if let editedPhotoImage = self.renderPhoto() {
//            self.onPhotoEdited?(editedPhotoImage, self.isAnonymousPosting)
//        }
        
//        toSendToVC changed by lava on 1008
        if let editedPhotoImage = self.renderPhoto() {
            print("---- this is the data for feed-------------")
            print(editedPhotoImage)
            print(self.isAnonymousPosting)
            /// added by lava on 1012
            self.renderedImage = editedPhotoImage
            self.performSegue(withIdentifier: "toSendToVC", sender: nil)
        }
        
    }

    @IBAction private func onBackButtonTapped(_ sender: Any) {
        self.onBack?()
    }

    // MARK: -

    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if error != nil {
            self.showMessage(withTitle: "Something went wrong".localized(),
                             message: "Please let us know what went wrong or try again later.".localized())
        } else {
            self.showMessage(withTitle: "Saved!".localized(),
                             message: "Your image has been saved to your photos.".localized())
        }
    }
    
    // MARK: -
    
    private func loadAvatarImage(with accountUser: AccountUser) {
        self.avatarImageView.image = Images.avatarLargePlaceholder
        
        if let imageURL = accountUser.largeAvatarURL ?? accountUser.largeAvatarPlaceholderURL {
            self.activityIndicatorView.startAnimating()

            Services.imageLoader.loadImage(for: imageURL, in: self.avatarImageView, placeholder: Images.avatarLargePlaceholder, completionHandler: { image in
                self.activityIndicatorView.stopAnimating()
            })
        }
    }
    
    private func savePhotoToCameraRoll() {
        guard let editedPhotoImage = self.renderPhoto() else {
            return
        }

        UIImageWriteToSavedPhotosAlbum(editedPhotoImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    private func showTextView() {
        self.textViewTopConstraint.priority = UILayoutPriority(rawValue: 250.0)
        self.textViewTopConstraint.constant = self.draggingAreaView.frame.midY - self.textView.frame.height * 0.5
        
        self.textView.text = nil
        self.textView.isHidden = false
        
        self.textView.layer.removeAllAnimations()
        
        UIView.animate(withDuration: 0.25, animations: {
            self.textView.alpha = 1.0
        }, completion: { finished in
            if finished {
                self.textView.becomeFirstResponder()
            }
        })
    }
    
    private func hideTextView() {
        self.textView.layer.removeAllAnimations()
        
        UIView.animate(withDuration: 0.25, animations: {
            self.textView.alpha = 0
        }, completion: { finished in
            if finished {
                self.textView.isHidden = true
            }
        })
    }
    
    private func showTextDraggingState() {
        self.performButton.layer.removeAllAnimations()
        self.anonymousPostingLabel.layer.removeAllAnimations()
        self.anonymousPostingSwitch.layer.removeAllAnimations()
        self.deletionAreaView.layer.removeAllAnimations()
        self.anonymousPostingGuideView.layer.removeAllAnimations()
        
        UIView.animate(withDuration: 0.25, animations: {
            self.performButton.alpha = 0.0
            self.anonymousPostingLabel.alpha = 0.0
            self.anonymousPostingSwitch.alpha = 0.0
            self.anonymousPostingGuideView.alpha = 0.0
        }, completion: { finished in
            if finished {
                self.performButton.isHidden = true
                self.anonymousPostingLabel.isHidden = true
                self.anonymousPostingSwitch.isHidden = true
                self.anonymousPostingGuideView.isHidden = true
                
                self.deletionAreaView.isHidden = false
                
                UIView.animate(withDuration: 0.25, animations: {
                    self.deletionAreaView.transform = .identity
                    self.deletionAreaView.alpha = 0.7
                })
            }
        })
    }
    
    private func hideTextDraggingState() {
        self.deletionAreaView.layer.removeAllAnimations()
        self.performButton.layer.removeAllAnimations()
        self.anonymousPostingLabel.layer.removeAllAnimations()
        self.anonymousPostingSwitch.layer.removeAllAnimations()
        self.anonymousPostingGuideView.layer.removeAllAnimations()

        UIView.animate(withDuration: 0.25, animations: {
            self.deletionAreaView.transform = .identity
            self.deletionAreaView.alpha = 0.0
        }, completion: { finished in
            if finished {
                self.deletionAreaView.isHidden = true
                
                self.performButton.isHidden = false
                self.anonymousPostingLabel.isHidden = (self.source == .chat || self.source == .profile)
                self.anonymousPostingSwitch.isHidden = (self.source == .chat || self.source == .profile)
                self.anonymousPostingGuideView.isHidden = ((self.source == .chat || self.source == .profile) || Managers.userDefaultsManager.isUserHasSeenAnonymousPostingGuide)

                UIView.animate(withDuration: 0.25, animations: {
                    self.performButton.alpha = 1.0
                    self.anonymousPostingLabel.alpha = 1.0
                    self.anonymousPostingSwitch.alpha = 1.0
                    self.anonymousPostingGuideView.alpha = 1.0
                })
            }
        })
    }
    
    private func activateDeletionArea() {
        self.deletionAreaView.layer.removeAllAnimations()
        
        UIView.animate(withDuration: 0.25, animations: {
            self.deletionAreaView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.deletionAreaView.alpha = 1.0
        })
    }
    
    private func deactivateDeletionArea() {
        self.deletionAreaView.layer.removeAllAnimations()
        
        UIView.animate(withDuration: 0.25, animations: {
            self.deletionAreaView.transform = .identity
            self.deletionAreaView.alpha = 0.7
        })
    }
    
    // MARK: -
    
    private func deleteText() {
        Log.extra("deleteText()", from: self)
        
        let alertController = UIAlertController(title: nil, message: "Are you sure?".localized(), preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: { action in
            self.hideTextView()
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .default))
        
        self.present(alertController, animated: true)
    }
    
    private func renderPhoto() -> UIImage? {
        Log.high("renderPhoto()", from: self)
        
        let photoImageAspectRatio = self.draggingAreaView.frame.height / self.draggingAreaView.frame.width
        
        let photoImageSize = CGSize(width: Limits.photoImageMaxWidth,
                                    height: Limits.photoImageMaxWidth * photoImageAspectRatio)
        
        UIGraphicsBeginImageContextWithOptions(photoImageSize, false, 0)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        let scale = photoImageSize.width / self.photoImageView.bounds.width
        
        context.scaleBy(x: scale, y: scale)
        
        context.translateBy(x: 0.0,
                            y: (photoImageSize.height / scale - self.photoImageView.bounds.height) * 0.5)
        
        self.photoImageView.layer.render(in: context)
        
        context.translateBy(x: 0.0,
                            y: self.textView.frame.top - self.photoImageView.frame.top)
        
        self.textView.layer.render(in: context)
        
        let editedPhotoImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()

        return editedPhotoImage
    }
    
    // MARK: -
    
    func apply(source: PhotoCreationSource, photoImage: UIImage? = nil) {
        Log.high("apply(source: \(source), photoImage: \(photoImage?.size)", from: self)
        
        self.photoImage = photoImage
        self.source = source
        
        if self.isViewLoaded {
            self.photoImageView.image = photoImage
            self.configureShareButton()

            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    private func configureFont() {
        self.textView.font = Fonts.regular(ofSize: 17.0)
    }
    
    private func configureShareButton() {
        switch self.source {
        case .some(.chat):
            self.performButton.isHidden = false
            self.performButton.setTitle("Send".localized(), for: .normal)

        case .some(.menu), .some(.feed), .some(.profile):
            self.performButton.isHidden = false
//            self.performButton.setTitle("Post this".localized(), for: .normal)
            self.performButton.setTitle("Send to".localized(), for: .normal)
            
        case .some(.reply):
            self.performButton.isHidden = false
            self.performButton.setTitle("Reply".localized(), for: .normal)

        case .none:
            break
        }
    }

    private func configureAddTextButton() {
        self.addTextButton.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
    }

    private func configureBackButton() {
        self.backButton.isHidden = (self.onBack == nil)
    }
    
    private func configureAnonymousPostingViews() {
        self.anonymousPostingSwitch.isHidden = (self.source == .chat || self.source == .profile)
        self.anonymousPostingSwitch.layer.cornerRadius = 16
        
        self.anonymousPostingLabel.isHidden = (self.source == .chat || self.source == .profile)
        
        self.anonymousPostingGuideView.isHidden = ((self.source == .chat || self.source == .profile) || Managers.userDefaultsManager.isUserHasSeenAnonymousPostingGuide)
        
        self.anonymousPostingGuideView.gradientColors = [CGColors.anonymousPostingGuideView, CGColors.anonymousPostingGuideView]
        self.anonymousPostingGuideView.backgroundColor = UIColor.clear
        
        self.anonymousPostingGuideView.pointerTrailingLength = self.anonymousPostingSwitch.frame.width / 3 + self.anonymousPostingSwitchTrailingConstraint.constant
        
        self.anonymousPostingGuideView.title = Constants.anonymousPostingGuideViewTitle
        self.anonymousPostingGuideView.buttonTitle = Constants.anonymousPostingGuideViewButtonTitle
        
        self.anonymousPostingGuideView.onActionButtonClicked = {
            Managers.userDefaultsManager.isUserHasSeenAnonymousPostingGuide = true
            
            self.configureAnonymousPostingViews()
        }
    }
    
    private func configureUserInformation() {
        self.avatarImageView.isHidden = self.isAnonymousPosting
        self.fullNameLabel.isHidden = self.isAnonymousPosting
        self.schoolInfoLabel.isHidden = self.isAnonymousPosting
        
        if let accountUser = Services.accountUser {
            self.loadAvatarImage(with: accountUser)
            
            self.fullNameLabel.text = accountUser.fullName
            
            let schoolTitle = accountUser.schoolTitle ?? "unknown school".localized()
            
            if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(accountUser.classYear)) {
                self.schoolInfoLabel.text = "\(schoolGradeNumber)TH GRADE, \(schoolTitle)".localized()
            } else {
                self.schoolInfoLabel.text = "FINISHED, \(schoolTitle)".localized()
            }
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureFont()
        self.configureAddTextButton()
        self.configureBackButton()
        self.configureAnonymousPostingViews()
        self.configureUserInformation()
        
        self.textView.textContainerInset = UIEdgeInsets(top: self.textView.textContainerInset.top,
                                                        left: self.textView.textContainerInset.left + 8.0,
                                                        bottom: self.textView.textContainerInset.bottom,
                                                        right: self.textView.textContainerInset.right + 8.0)
        
        self.textView.textContainer.maximumNumberOfLines = 1
        self.textView.textContainer.lineBreakMode = .byTruncatingTail
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribeToKeyboardNotifications()
        
        if let photoImage = self.photoImage, !self.hasAppliedData, let source = self.source {
            self.apply(source: source, photoImage: photoImage)
        }
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.textView.endEditing(true)

        self.unsubscribeFromKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.hasAppliedData = false
    }
}

// MARK: - UITextViewDelegate

extension EditPhotoViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.textViewTopConstraint.priority = UILayoutPriority(rawValue: 250.0)
        
        self.view.layer.removeAllAnimations()
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text?.isEmpty ?? true {
            self.hideTextView()
        } else {
            self.textViewTopConstraint.priority = UILayoutPriority(rawValue: 750.0)
            
            self.view.layer.removeAllAnimations()
            
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            
            return false
        }
        
        return true
    }
}

// MARK: - KeyboardHandler

extension EditPhotoViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.textViewBottomConstraint.constant = keyboardHeight
        
        if keyboardHeight > CGFloat.leastNonzeroMagnitude {
            UIView.animate(withDuration: 0.25) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

// MARK: - DictionaryReceiver

extension EditPhotoViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let source = dictionary["source"] as? PhotoCreationSource else {
            return
        }
        
        let photoImage = dictionary["photoImage"] as? UIImage
        
        self.apply(source: source, photoImage: photoImage)
    }
}
