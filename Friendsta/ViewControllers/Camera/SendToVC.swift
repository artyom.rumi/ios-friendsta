//
//  toSendToVC.swift
//  Friendsta
//
//  Created by 图娜福尔 on 2020/10/10.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools
import PromiseKit
import SVProgressHUD
import FriendstaNetwork

class SendToVC: LoggedViewController, ErrorMessagePresenter {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var optionalTableView: UITableView!
    @IBOutlet weak var contentTableView: UITableView!
    @IBOutlet weak var sendButton: UIButton!
    
    var titles: [String] = ["Public", "Friends"]
    var titleImages: [UIImage] = [#imageLiteral(resourceName: "public_icon01"), #imageLiteral(resourceName: "friend_icon02")]
    var isFriend: Bool = false
    
     // MARK: - UIViewController
    
    var editedImage: UIImage = UIImage()
    var isAnnoymous: Bool = false
    var isPublic : Bool  = true
    
    var onPhotoEdited: ((_ photoImage: UIImage, _ isIncognito: Bool?) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        optionalTableView.delegate = self
        optionalTableView.dataSource = self
        contentTableView.delegate = self
        contentTableView.dataSource = self
//        searchBar.delegate = self
                
        searchBar.barTintColor = UIColor.white
        searchBar.setBackgroundImage(UIImage.init(), for: UIBarPosition.any, barMetrics: UIBarMetrics.default)
        
        sendButton.layer.cornerRadius = sendButton.frame.height/2
        
        contentTableView.isHidden = true
        
        print("----- this is the data from previouse controller")
        print(self.editedImage)
        print(self.isAnnoymous)
    }

    override func viewWillAppear(_ animated: Bool) {        
        super.viewWillAppear(animated)
        
        /*
        self.subscribeToKeyboardNotifications()
        
        if let photoImage = self.photoImage, !self.hasAppliedData, let source = self.source {
            self.apply(source: source, photoImage: photoImage)
        }
        */
        
        self.navigationController?.isNavigationBarHidden = true
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        print("--- this is the back button action in the send to-----")
//        self.onBack!()
//        self.onCanceled?()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        //send action
        print("----- this is the action for post feeds -----")
        self.onPhotoEdited?(self.editedImage, self.isAnnoymous)
        
        Log.high("performAction(withPhotoImage: \(self.editedImage.size))", from: self)

        SVProgressHUD.show()

        DispatchQueue.global(qos: .userInitiated).async(execute: {
            var photo = self.editedImage
            
            if self.editedImage.size.width > Limits.photoImageMaxWidth {
                if let photoImage = self.editedImage.scaled(to: Limits.photoImageMaxWidth) {
                    photo = photoImage
                }
            }
            
            firstly {
                Services.feedService.sendFeed(photo: photo, isIncognito: self.isAnnoymous ?? true, isPublic: self.isPublic)
            }.ensure {
                SVProgressHUD.dismiss()
            }.done { feed in
                if let imageURL = feed.photo?.imageURL {
                    Services.imageLoader.saveImage(for: imageURL, image: photo)
                }
                
                self.performSegue(withIdentifier: "goToHome", sender: nil)
                           
            }.catch { error in
                self.handle(actionError: error)
            }
        })
    }
    
    private func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
          switch error as? WebError {
          case .some(.unauthorized):
              self.performSegue(withIdentifier: "goToHome", sender: nil)
              
          default:
              self.showMessage(withError: error, okHandler: okHandler)
          }
      }
    
}

extension SendToVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        if tableView == optionalTableView {
            return ""
            
        }
        else {
            return "Suggested"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == optionalTableView {
            return 2
            
        }
        else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == optionalTableView {
            let cell = Bundle.main.loadNibNamed("SendToTableViewCell", owner: self, options: nil)?.first as! SendToTableViewCell
            
            cell.titleLbl.text = self.titles[indexPath.row]
            cell.publicImg.image = self.titleImages[indexPath.row]
            
            if indexPath.row == 0 {
                cell.contentLbl.text = "All" }
            else {
                cell.contentLbl.text = "11 people"
            }
            if (isFriend) {
                if indexPath.row == 0 {
                    cell.checkImg.image = #imageLiteral(resourceName: "CheckmarkOff") }
                else {
                    cell.checkImg.image = #imageLiteral(resourceName: "CheckmarkOn") }
            } else {
                if indexPath.row == 0 {
                    cell.checkImg.image = #imageLiteral(resourceName: "CheckmarkOn")
                }
                else {
                    cell.checkImg.image = #imageLiteral(resourceName: "CheckmarkOff")
                }
            }
            return cell }
        else {
            let cell = Bundle.main.loadNibNamed("SendToTableViewCell", owner: self, options: nil)?.first as! SendToTableViewCell
            /*
            cell.titleLbl.text = self.titles[indexPath.row]
            cell.publicImg.image = self.titleImages[indexPath.row]
            
            if indexPath.row == 0
            {
                cell.contentLbl.text = "All"
            }
            else
            {
                cell.contentLbl.text = "11 people"
            }
            if (isFriend)
            {
                if indexPath.row == 0
                {
                    cell.checkImg.image = #imageLiteral(resourceName: "CheckmarkOff")
                }
                else
                {
                    cell.checkImg.image = #imageLiteral(resourceName: "CheckmarkOn")
                }
            }
            else
            {
                if indexPath.row == 0
                {
                    cell.checkImg.image = #imageLiteral(resourceName: "CheckmarkOn")
                }
                else
                {
                    cell.checkImg.image = #imageLiteral(resourceName: "CheckmarkOff")
                }
            }
            */
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        ///select action
        print("selectedIndex: \(indexPath.row)")
        if tableView == optionalTableView {
             if indexPath.row == 0 {
                isFriend = false
                isPublic = true
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none) }
             else {
                isFriend = true
                isPublic = false
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
             }
        } else {
            //other tableview
        }
    }
}
