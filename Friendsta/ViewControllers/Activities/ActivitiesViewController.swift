//
//  ActivitiesViewController.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/22/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//
// swiftlint:disable file_length
// swiftlint:disable type_body_length
// swiftlint:disable function_body_length
// swiftlint:disable vertical_whitespace
import UIKit
import PromiseKit
import FriendstaTools
import SwiftKeychainWrapper

import UserNotifications //>>> 1.
import SwiftKeychainWrapper

class ActivitiesViewController: UIViewController {
    
    ///// ********* Lance's Code ********* /////
    
    //>>> 2.
    func hexStringToUIColor(hex: String) -> UIColor {
        var cString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue: UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                       green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                       blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                       alpha: CGFloat(1.0)
        )
    }
    
    //>>> 3.
    // MARK: - Programmatic UIElements
    fileprivate lazy var notificationButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.setTitle("Enable Notifications", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        
        let yellowColor = hexStringToUIColor(hex: "ffffcc")
        
        button.backgroundColor = yellowColor
        button.addTarget(self, action: #selector(notificationButtonPressed), for: .touchUpInside)
        
        button.layer.cornerRadius = 0
        button.layer.masksToBounds = true
        button.sizeToFit()
        
        return button
    }()
    
    //>>> 4.
    @objc fileprivate func notificationButtonPressed() {
        
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        DispatchQueue.main.async {
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                })
            } else {
                print("Settings URL did Not Open")
            }
        }
    }
    
    //>>> 5.
    fileprivate var notificationButtonHeightConstraint: NSLayoutConstraint?
    fileprivate func configureAnchors() {
        
        notificationButtonHeightConstraint?.isActive = false
        
        view.addSubview(notificationButton)
        notificationButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        notificationButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        notificationButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        notificationButtonHeightConstraint = notificationButton.heightAnchor.constraint(equalToConstant: notificationButtonNormalHeight)
        notificationButtonHeightConstraint?.isActive = true
    }
    
    //>>> 6.
    fileprivate func toggleNotificationButtonHeightToZero() {
        notificationButtonHeightConstraint?.isActive = false
        notificationButtonHeightConstraint = notificationButton.heightAnchor.constraint(equalToConstant: 0)
        notificationButtonHeightConstraint?.isActive = true
    }
    
    //>>> 7.
    fileprivate func toggleNotificationButtonHeightToNormalHeight() {
        notificationButtonHeightConstraint?.isActive = false
        notificationButtonHeightConstraint = notificationButton.heightAnchor.constraint(equalToConstant: notificationButtonNormalHeight)
        notificationButtonHeightConstraint?.isActive = true
    }
    
    //>>> 8.
    fileprivate func decideWetherOrNotToEnableNotifications() {
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                // Already authorized
                
                DispatchQueue.main.async { [weak self] in
                    self?.toggleNotificationButtonHeightToZero()
                }
                
            } else {
                // Either denied or notDetermined
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                    // add your own
                    //UNUserNotificationCenter.current().delegate = self
                    if !granted {
                        print("not granted")
                        
                        DispatchQueue.main.async { [weak self] in
                            self?.toggleNotificationButtonHeightToNormalHeight()
                        }
                        
                    } else {
                        print("granted")
                    }
                }
            }
        }
    }
    
    //>>> 9.
    public func set2LinesOfTextForNotifcationButton() {
        
        let button = self.notificationButton
        
        //getting the range to separate the button title strings
        //let buttonText = "\(buttonTitle)\n\(String(numOfFollowing))" as NSString
        let buttonText = "Notifications are off\nTap to re-enable" as NSString
        let newlineRange: NSRange = buttonText.range(of: "\n")
        
        //getting both substrings
        var substring1 = ""
        var substring2 = ""

        if(newlineRange.location != NSNotFound) {
            substring1 = buttonText.substring(to: newlineRange.location)
            substring2 = buttonText.substring(from: newlineRange.location)
        }
        
        let attrString1 = NSMutableAttributedString(string: substring1,
                                                    attributes: [NSMutableAttributedString.Key.font:
                                                        UIFont.boldSystemFont(ofSize: 16),
                                                                 NSMutableAttributedString.Key.foregroundColor: UIColor.black])
        
        let attrString2 = NSMutableAttributedString(string: substring2,
                                                    attributes: [NSMutableAttributedString.Key.font:
                                                        UIFont.systemFont(ofSize: 14),
                                                                 NSMutableAttributedString.Key.foregroundColor: UIColor.black])

        //appending both attributed strings
        attrString1.append(attrString2) //<<
        button.setAttributedTitle(attrString1, for: [])
    }
    
    //>>> 10.
    fileprivate let notificationButtonNormalHeight: CGFloat = 60
    
    //>>> 11.
    @IBOutlet private weak var tableViewTopConstraint: NSLayoutConstraint!
    
    fileprivate func configureNotifications() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(removeRevealButtonTimerLabelCheckBackInLabelSetKeychainWrapperChatSessionIdToNil),
//                                               name: NSNotification.Name(rawValue: "removeRevealButtonInChatMessagesTVC"),
//                                               object: nil)
    }
    
    @objc fileprivate func appWillEnterForeground() {
        
        //>>> 16.
        decideWetherOrNotToEnableNotifications()
    }
    
    // MARK: - Nested Types

    enum Constants {
        static let activityCellReuseIdentifier = "ActivityTableViewCell"
        static let activityUserCellReuseIdentifier = "ActivityUserTableViewCell"
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var tableView: UITableView!

    // MARK: -

    private var refreshControl: UIRefreshControl?

    private var activities: [Activity] = []

    // MARK: - UIViewController Methods

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableView()
        
        /*
        ///// ********* Lance's Code ********* /////
        showRevealButton()
        addTimerLabelAndCheckBackInLabelAnchors()
        configureRevealTimer(chatSessionId: "123")
        ///// ********* Lance's Code ********* /////
        */
        
        //>>> 12.
        configureNotifications()
        configureAnchors()
        
        //>>> 14.
        tableViewTopConstraint.isActive = false
        tableViewTopConstraint = tableView.topAnchor.constraint(equalTo: notificationButton.bottomAnchor)
        tableViewTopConstraint.isActive = true
        
        //>>> 15.
        set2LinesOfTextForNotifcationButton()
        decideWetherOrNotToEnableNotifications()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.refreshActivities()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        self.markActivitiesAsRead()
    }

    // MARK: - Instance Methods

    private func configureTableView() {
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.tableFooterView = UIView()
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(onTableRefreshControlRequested(_:)), for: .valueChanged)
        self.tableView.refreshControl = self.refreshControl
    }

    // MARK: -

    private func refreshActivities() {
        firstly {
            Services.activityService.refreshAllActivities()
        }.done { activities in
            self.apply(activities: activities)
            
            self.refreshControl?.endRefreshing()
        }.catch { error in
            self.refreshControl?.endRefreshing()
        }
    }
    
    private func apply(activities: [Activity]) {
        Log.high("apply(activities: \(activities.count))", from: self)
        
        let sortedActivities = activities.sorted { firstActivity, secondActivity in
            let firstDate = firstActivity.createdAt ?? Date()
            let secondDate = secondActivity.createdAt ?? Date()
            
            return firstDate > secondDate
        }
        
        self.activities = sortedActivities
        
        if self.activities.isEmpty {
            self.showNoDataState()
        }
        
        self.tableView.reloadData()
    }

    private func showNoDataState() {
    }

    private func markActivitiesAsRead() {
        let unreadActivities = self.activities.filter { $0.isUnread == true }
        if !unreadActivities.isEmpty {
            firstly {
                Services.activityService.markAsRead(activities: unreadActivities)
            }.done { _ in
                self.apply(activities: Services.cacheViewContext.activityManager.fetch())
            }.catch { error in
            }
        }
    }

    private func showUser(with userID: Int64) {
        guard let userViewController = UIStoryboard(name: "User", bundle: Bundle.main).instantiateInitialViewController() as? UserViewController else {
            fatalError("Can't init UserViewController")
        }

        userViewController.apply(userID: userID)
        self.show(userViewController, sender: self)
    }

    private func showFeed(with feedID: Int64, shouldOpenComments: Bool = false) {
        guard let feedPreviewViewController = UIStoryboard(name: "FeedPreview", bundle: Bundle.main).instantiateInitialViewController() as? FeedPreviewViewController else {
            fatalError("Can't init FeedPreviewViewController")
        }
        
        feedPreviewViewController.apply(feedID: feedID, shouldOpenComments: shouldOpenComments)
        
        self.show(feedPreviewViewController, sender: self)
    }

    // MARK: -

    @objc private func onTableRefreshControlRequested(_ sender: Any) {
        self.refreshActivities()
    }
}

// MARK: - UITableViewDataSource Methods

extension ActivitiesViewController: UITableViewDataSource {

    // MARK: - UITableViewDataSource Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.activities.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let activity = self.activities[indexPath.row]
        
        print("---------+++++++++++ this is the activities from table data --------------")
        print(activity.type)
        print(activity)
        print("--------- this is the activities endpoint --------------")
        switch activity.type {
        case .like, .comment, .repost, .rate:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.activityCellReuseIdentifier, for: indexPath) as? ActivityTableViewCell else {
                fatalError("Can't init ActivityTableViewCell")
            }
            self.configure(cell: cell, activity: activity)
            return cell

        case .newFriend, .expireFriend12h, .expireFriend72h, .expiredFriendship, .rateFriend:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.activityUserCellReuseIdentifier, for: indexPath) as? ActivityUserTableViewCell else {
                fatalError("Can't init ActivityUserTableViewCell")
            }
            self.configure(cell: cell, activity: activity)
            return cell
        }
    }
}

// MARK: - UITableViewDataSource Helpers

extension ActivitiesViewController {

    // MARK: - UITableViewDataSource Helpers

    func configure(cell: ActivityTableViewCell, activity: Activity) {
        switch activity.type {
        case .like, .rate:
            cell.cellType = .like
            
            cell.onCellSelected = {
                self.showFeed(with: activity.feedID)
            }

        case .comment:
            cell.cellType = .comment
            
            cell.onCellSelected = {
                self.showFeed(with: activity.feedID, shouldOpenComments: true)
            }

        case .repost:
            cell.cellType = .repost

        default:
            cell.cellType = .like
        }

        cell.title = activity.message
        cell.subTitle = activity.subtitle
        cell.setIsUnread(activity.isUnread)

        if let createdDate = activity.createdAt {
            cell.date = FeedDateFormatter.shared.string(from: createdDate)
        }

        if let imageURLString = activity.imageURL, let imageURL = URL(string: imageURLString) {
            Services.imageLoader.loadImage(for: imageURL, completionHandler: { image in
                cell.feedImage = image
            })
        }
    }

    func configure(cell: ActivityUserTableViewCell, activity: Activity) {
        switch activity.type {
        case .newFriend:
            cell.cellType = .newFriend

        case .expireFriend12h:
            cell.cellType = .expireFriend12h

        case .expireFriend72h:
            cell.cellType = .expireFriend72h

        case .expiredFriendship:
            cell.cellType = .expiredFriendship
            
        case .rateFriend:
            cell.cellType = .rateFriend

        default:
            cell.cellType = .newFriend
        }

        cell.title = activity.message
        cell.subTitle = activity.subtitle
        cell.setIsUnread(activity.isUnread)

        cell.onKeepTapped = {
            print("------- this is the tapped event for notification -------")
            if activity.friendID != 0 {
                if activity.type == .rateFriend {
                    firstly {
                        Services.usersService.refresh(userUID: activity.friendID)
                    }.done { user in
                       print(user)
                       firstly {
                           Services.usersService.inviteUser(user: user)
                       }.done { _ in
                           print("------ inviteUser in the rate notificaiton -----")
                       }.catch { _ in
                           print("----- catch error in the rate notfication -----")
                       }
                    }.catch { error in
                        print(error)
                    }
                } else {
                    cell.keepActivityIndicatorAnimating = true

                    firstly {
                        Services.usersService.keepUser(with: activity.friendID)
                    }.then {
                        Services.activityService.refreshAllActivities()
                    }.done { activities in
                        cell.keepActivityIndicatorAnimating = false
                        self.apply(activities: activities)
                    }.catch { _ in
                        cell.keepActivityIndicatorAnimating = false
                    }
                }
            }
        }

        cell.onCellSelected = {
            self.showUser(with: activity.friendID)
        }

        cell.avatarImage = UIImage(named: "AvatarSmallPlaceholder")

        if let imageURLString = activity.imageURL, let imageURL = URL(string: imageURLString) {
            Services.imageLoader.loadImage(for: imageURL, completionHandler: { image in
                cell.avatarImage = image
            })
        }
    }
}

// MARK: - UITableViewDelegate

extension ActivitiesViewController: UITableViewDelegate { }

/*************************************////////////////////TEMP-CODE/////////////////////////************************************/

/*
///// ********* Lance's Code ********* /////

// revealButton
fileprivate lazy var revealButton: UIButton = {
    let button = UIButton(type: .system)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.setTitle("Reveal Now", for: .normal)
    button.setTitleColor(UIColor.white, for: .normal)
    button.backgroundColor = Colors.orangeGuideView
    button.addTarget(self, action: #selector(revealButtonPressed), for: .touchUpInside)
    
    //button.titleLabel?.textAlignment = .center
    button.layer.cornerRadius = 16
    button.layer.masksToBounds = true
    button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    //button.backgroundColor = .lightGray
    //button.isUserInteractionEnabled = false
    
    return button
}()

@objc fileprivate func revealButtonPressed() {
    
    let storyboard = UIStoryboard(name: "OutlineContacts", bundle: nil)
    let outlineContactsTVC = storyboard.instantiateViewController(withIdentifier: "OutlineContactsTableViewController") as! OutlineContactsTableViewController
    
    outlineContactsTVC.isComingFromChatTVC = true
    outlineContactsTVC.chatSessionId = chatSessionId
    outlineContactsTVC.timerHrs = timerHrs
    outlineContactsTVC.timerMins = timerMins
    outlineContactsTVC.timerSecs = timerSecs
    outlineContactsTVC.revealTimerText = revealTimerLabel.text
    
    present(outlineContactsTVC, animated: true, completion: nil)
    
    /*
    let storyboard = UIStoryboard(name: "Community", bundle: nil)
    let CommunityTVC = storyboard.instantiateViewController(withIdentifier: "CommunityTableViewController") as! CommunityTableViewController
    present(CommunityTVC, animated: true, completion: nil)
    */
}

fileprivate func disableUIElements() {
    revealButton.backgroundColor = .lightGray
    revealButton.isUserInteractionEnabled = false
}

fileprivate func enableUIElements() {
    revealButton.backgroundColor = Colors.orangeGuideView
    revealButton.isUserInteractionEnabled = true
}

fileprivate func showRevealButton() {
    
    // https://stackoverflow.com/questions/35284913/swift-countdown-timer-displays-days-hours-seconds-remaining
    // https://stackoverflow.com/questions/28571194/using-nsuserdefaults-to-add-a-24hour-countdown-timer-in-swift
    
    view.addSubview(revealButton)
    revealButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
    revealButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
    revealButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
}

@objc fileprivate func removeRevealButtonTimerLabelCheckBackInLabelSetKeychainWrapperChatSessionIdToNil() {
    
    revealButton.removeFromSuperview()
    revealTimerLabel.removeFromSuperview()
    staticCheckBackInLabel.removeFromSuperview()
    
    if let chatSessionId = chatSessionId {
        KeychainWrapper.standard.removeObject(forKey: chatSessionId)
    }
}

// timerLabel + staticCheckBackInLabel
fileprivate lazy var staticCheckBackInLabel: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Check back in"
    label.font = UIFont.systemFont(ofSize: 17)
    label.textColor = UIColor.gray
    label.textAlignment = .left
    return label
}()

fileprivate lazy var revealTimerLabel: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.monospacedDigitSystemFont(ofSize: 21, weight: UIFont.Weight.regular)
    label.textAlignment = .left
    return label
}()

fileprivate func addTimerLabelAndCheckBackInLabelAnchors() {
    
    view.addSubview(staticCheckBackInLabel)
    view.addSubview(revealTimerLabel)
    
    staticCheckBackInLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
    staticCheckBackInLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
    
    revealTimerLabel.topAnchor.constraint(equalTo: staticCheckBackInLabel.bottomAnchor, constant: 0).isActive = true
    revealTimerLabel.leadingAnchor.constraint(equalTo: staticCheckBackInLabel.leadingAnchor, constant: 0).isActive = true
}

///////////////////////////////////////////////////////////////////////////////////////////-------------------------------------------------------------///////////////////////////////////////////////////////////////////////////////////////////
fileprivate var chatSessionId: String?
fileprivate var revealTimer: Timer?
fileprivate var timerHrs: Int?
fileprivate var timerMins: Int?
fileprivate var timerSecs: Int?

fileprivate func configureRevealTimer(chatSessionId: String) {
    print("+++++\(chatSessionId)")
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    let currentDate: TimeInterval = Date().timeIntervalSince1970
    let startDateAsDate: Date = Date(timeIntervalSince1970: currentDate)
    let revealDateAsDate: Date? = Calendar.current.date(byAdding: .hour, value: +24, to: startDateAsDate)
    
    let startDateFormatted = dateFormatter.string(from: startDateAsDate)
    print("startDate: \(startDateFormatted)")
    
    guard let safeRevealDateAsDate = revealDateAsDate else { return }
    let revealDateFormattedAsStr = dateFormatter.string(from: safeRevealDateAsDate)
    print("revealDate: \(revealDateFormattedAsStr)")
    
    let revealDate = safeRevealDateAsDate.timeIntervalSince1970
    
    KeychainWrapper.standard.set(revealDate, forKey: chatSessionId)
    
    self.chatSessionId = chatSessionId
    startRevealTimer()
}

fileprivate func startRevealTimer() {
    revealTimer?.invalidate()
    revealTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDownDate), userInfo: nil, repeats: true)
}

fileprivate func invalidateRevealTimer() {
    revealTimer?.invalidate()
    revealTimer = nil
}

@objc fileprivate func countDownDate() {
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    guard let chatSessionId = chatSessionId,
        let revealDateAsDouble = KeychainWrapper.standard.double(forKey: chatSessionId) else {
        invalidateRevealTimer()
        return
    }
    
    if !revealButton.isUserInteractionEnabled {
        enableUIElements()
    }
    
    let revealDate: Date = Date(timeIntervalSince1970: revealDateAsDouble)
    
    let currentDate = Date()
    let calendar = Calendar.current
    let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: revealDate)
    
    let countdown = "Days \(diffDateComponents.day),  Hours: \(diffDateComponents.hour), Minutes: \(diffDateComponents.minute), Seconds: \(diffDateComponents.second)"
    print(countdown)
    
    timerHrs = diffDateComponents.hour
    timerMins = diffDateComponents.minute
    timerSecs = diffDateComponents.second
    
    if let hours = diffDateComponents.hour, let minutes = diffDateComponents.minute, let seconds = diffDateComponents.second {
        
        let hrStr = String(hours)
        let minStr = String(minutes)
        let secondsStr = seconds > 9 ? "\(String(seconds))" : "0\(String(seconds))"
        
        revealTimerLabel.text = "\(hrStr):\(minStr):\(secondsStr)"
    }
    
    if currentDate >= revealDate {
        
        invalidateRevealTimer()
        removeRevealButtonTimerLabelCheckBackInLabelSetKeychainWrapperChatSessionIdToNil()
        
    } else {
        
        print("reveal time hasn't happened yet")
    }
}
///// ********* Lance's Code ********* /////
*/

/*************************************////////////////////TEMP-CODE/////////////////////////************************************/
