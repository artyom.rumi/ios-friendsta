//
//  GroupChatMessagesTableViewDelegate.swift
//  Friendsta
//
//  Created by Elina Batyrova on 27.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class GroupChatMessagesTableViewDelegate: NSObject {
    
    // MARK: - Instance Properties
    
    private var chatMessages: [ChatMessage] = []
    private var chat: Chat?
    
    // MARK: -
    
    var onMessageViewed: ((_ chatMessage: ChatMessage) -> Void)?
    
    // MARK: - Instance Methods

    private func loadAvatar(in imageView: UIImageView, with chatMessage: ChatMessage) {
        let chatMember = self.chat?.members?.allObjects.first(where: { member -> Bool in
            return (member as? ChatMember)?.user?.uid == chatMessage.userUID
        })
        
        if let chatMember = chatMember as? ChatMember {
            if let imageURL = chatMember.user?.smallAvatarURL {
                Services.imageLoader.loadImage(for: imageURL, in: imageView, placeholder: Images.avatarSmallPlaceholder)
            }
        }
    }
    
    private func loadPhotoImage(for cell: IncomingPhotoMessageTableViewCell, with photoMessage: ChatPhotoMessage) {
        cell.photoImage = nil

        cell.showLoadingState()

        if let imageURL = photoMessage.largePhotoURL {
            Services.imageLoader.loadImage(for: imageURL, in: cell.photoImageViewTarget, completionHandler: { [weak cell] image in
                cell?.hideLoadingState()
            })
        }
    }

    private func loadPhotoImage(for cell: OutgoingPhotoMessageTableViewCell, with photoMessage: ChatPhotoMessage) {
        cell.photoImage = nil

        cell.showLoadingState()

        if let imageURL = photoMessage.largePhotoURL {
            Services.imageLoader.loadImage(for: imageURL, in: cell.photoImageViewTarget, completionHandler: { [weak cell] image in
                cell?.hideLoadingState()
            })
        }
    }
    
    // MARK: -
    
    func apply(chat: Chat, chatMessages: [ChatMessage]) {
        Log.high("apply(chat: \(chat.uid), chatMessages: \(chatMessages.count))", from: self)
        
        self.chat = chat
        self.chatMessages = chatMessages
    }
}

// MARK: - UITableViewDelegate

extension GroupChatMessagesTableViewDelegate: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell is ChatmatesListTableViewCell {
            return
        }
        
        let chatMessage = self.chatMessages[indexPath.row]
        
        self.onMessageViewed?(chatMessage)
        
        switch chatMessage {
        case is ChatTextMessage:
            if let cell = cell as? IncomingTextMessageWithAvatarTableViewCell {
                self.loadAvatar(in: cell.avatarImageViewTarget, with: chatMessage)
            }
            
        case let photoMessage as ChatPhotoMessage:
            if let cell = cell as? IncomingPhotoMessageTableViewCell, cell.photoImage == nil {
                self.loadPhotoImage(for: cell, with: photoMessage)
            }
            
            if let cell = cell as? OutgoingPhotoMessageTableViewCell, cell.photoImage == nil {
                self.loadPhotoImage(for: cell, with: photoMessage)
            }

            if let cell = cell as? IncomingPhotoMessageWithAvatarTableViewCell {
                self.loadAvatar(in: cell.avatarImageViewTarget, with: photoMessage)
            }

            if let cell = cell as? IncomingCommentMessageWithAvatarTableViewCell {
                self.loadAvatar(in: cell.avatarImageViewTarget, with: photoMessage)
            }
            
        default:
            break
        }
    }
}
