//
//  GroupChatMessagesTableViewDataSource.swift
//  Friendsta
//
//  Created by Elina Batyrova on 24.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class GroupChatMessagesTableViewDataSource: NSObject {
    
    // MARK: - Nested Types
    
    private enum Identifiers {
        
        // MARK: - Type Properties
        
        static let chatmatesListTableCell = "ChatmatesListTableCell"
        static let outgoingTextMessageTableCell = "OutgoingTextMessageTableCell"
        static let incomingTextMessageTableCell = "IncomingTextMessageTableCell"
        static let incomingTextMessageWithAvatarTableCell = "IncomingTextMessageWithAvatarTableCell"
        static let incomingPhotoMessageTableCell = "IncomingPhotoMessageTableCell"
        static let incomingPhotoMessageWithAvatarTableCell = "IncomingPhotoMessageWithAvatarTableCell"
        static let outgoingPhotoMessageTableCell = "OutgoingPhotoMessageTableCell"
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let numberOfSections = 2
        
        static let messagesSection = 0
        static let beginningMessageSection = 1
    }
    
    // MARK: - Instance Properties
    
    private var chatMessages: [ChatMessage] = []
    private var chat: Chat?
    
    // MARK: -
    
    var onErrorButtonTapped: ((_ chatMessage: ChatMessage?) -> Void)?
    var onPhotoTapped: ((_ photoMessage: ChatPhotoMessage) -> Void)?
    
    // MARK: - Instance Methods
    
    func registerCells(tableView: UITableView) {
        tableView.register(ChatmatesListTableViewCell().nib,
                           forCellReuseIdentifier: Identifiers.chatmatesListTableCell)
        tableView.register(OutgoingTextMessageTableViewCell().nib,
                           forCellReuseIdentifier: Identifiers.outgoingTextMessageTableCell)
        tableView.register(IncomingTextMessageTableViewCell().nib,
                           forCellReuseIdentifier: Identifiers.incomingTextMessageTableCell)
        tableView.register(IncomingTextMessageWithAvatarTableViewCell().nib,
                           forCellReuseIdentifier: Identifiers.incomingTextMessageWithAvatarTableCell)
        tableView.register(IncomingPhotoMessageTableViewCell().nib,
                           forCellReuseIdentifier: Identifiers.incomingPhotoMessageTableCell)
        tableView.register(IncomingPhotoMessageWithAvatarTableViewCell().nib,
                           forCellReuseIdentifier: Identifiers.incomingPhotoMessageWithAvatarTableCell)
        tableView.register(OutgoingPhotoMessageTableViewCell().nib,
                           forCellReuseIdentifier: Identifiers.outgoingPhotoMessageTableCell)
    }
    
    func apply(chat: Chat, chatMessages: [ChatMessage]) {
        Log.high("apply(chat: \(chat.uid), chatMessages: \(chatMessages.count))", from: self)
        
        self.chat = chat
        self.chatMessages = chatMessages
    }
}

// MARK: - UITableViewDataSource

extension GroupChatMessagesTableViewDataSource: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == Constants.beginningMessageSection ? 1 : self.chatMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let accountUserUID = Services.accountAccess?.userUID else {
            fatalError()
        }
        
        let cell: UITableViewCell
        
        if indexPath.section == Constants.messagesSection {
            switch self.chatMessages[indexPath.row] {
            case let textMessage as ChatTextMessage:
                if textMessage.userUID == accountUserUID {
                    cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.outgoingTextMessageTableCell,
                                                         for: indexPath)

                    self.configure(outgoingTextMessageCell: cell as! OutgoingTextMessageTableViewCell, with: textMessage)
                } else {
                    if textMessage.isAnonymous {
                        cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.incomingTextMessageTableCell,
                                                             for: indexPath)

                        self.configure(incomingTextMessageCell: cell as! IncomingTextMessageTableViewCell, with: textMessage)
                    } else {
                        cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.incomingTextMessageWithAvatarTableCell,
                                                             for: indexPath)

                        self.configure(incomingTextMessageCell: cell as! IncomingTextMessageWithAvatarTableViewCell, with: textMessage)
                    }
                }
                
            case let photoMessage as ChatPhotoMessage:
                if photoMessage.userUID == accountUserUID {
                    cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.outgoingPhotoMessageTableCell,
                                                         for: indexPath)
                    
                    self.configure(outgoingPhotoMessageCell: cell as! OutgoingPhotoMessageTableViewCell, with: photoMessage)
                } else {
                    if photoMessage.isAnonymous {
                        cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.incomingPhotoMessageTableCell,
                                                             for: indexPath)
                        
                        self.configure(incomingPhotoMessageCell: cell as! IncomingPhotoMessageTableViewCell, with: photoMessage)
                    } else {
                        cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.incomingPhotoMessageWithAvatarTableCell,
                                                             for: indexPath)
                        
                        self.configure(incomingPhotoMessageCell: cell as! IncomingPhotoMessageWithAvatarTableViewCell, with: photoMessage)
                    }
                }
                
            default:
                fatalError()
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.chatmatesListTableCell, for: indexPath)
            
            self.configure(chatmatesListCell: cell as! ChatmatesListTableViewCell)
        }
        
        cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        
        return cell
    }
}

// MARK: - UITableViewDataSource Helper

extension GroupChatMessagesTableViewDataSource {
    
    // MARK: - Instance Methods
    
    private func configure(outgoingTextMessageCell cell: OutgoingTextMessageTableViewCell, with textMessage: ChatTextMessage) {
        cell.message = textMessage.text

        if let createdDate = textMessage.createdDate {
            cell.date = ChatMessageDateFormatter.shared.string(from: createdDate)
        } else {
            cell.date = nil
        }

        cell.isSent = textMessage.isSent
        cell.isFailed = textMessage.isFailed
        cell.isViewed = textMessage.isViewed

        cell.onErrorButtonClicked = { [unowned self, weak textMessage] in
            self.onErrorButtonTapped?(textMessage)
        }
        
        if textMessage.isAnonymous {
            cell.cloudViewColor = Colors.outcomingAnonymousCloudView
        } else {
            cell.cloudViewColor = Colors.outcomingNotAnonymousCloudView
        }
    }
    
    private func configure(outgoingPhotoMessageCell cell: OutgoingPhotoMessageTableViewCell, with photoMessage: ChatPhotoMessage) {
        if let createdDate = photoMessage.createdDate {
            cell.date = ChatMessageDateFormatter.shared.string(from: createdDate)
        } else {
            cell.date = nil
        }

        if photoMessage.isSent || photoMessage.isFailed {
            cell.hideLoadingState()
        } else {
            cell.showLoadingState()
        }

        cell.isSent = photoMessage.isSent
        cell.isFailed = photoMessage.isFailed
        cell.isViewed = photoMessage.isViewed

        cell.onErrorButtonClicked = { [unowned self, weak photoMessage] in
            self.onErrorButtonTapped?(photoMessage)
        }

        cell.photoImage = nil

        cell.onViewPhotoButtonClicked = { [unowned self, weak photoMessage] in
            if let photoMessage = photoMessage {
                self.onPhotoTapped?(photoMessage)
            }
        }
    }
    
    private func configure(incomingTextMessageCell cell: IncomingTextMessageTableViewCell, with textMessage: ChatTextMessage) {
        cell.message = textMessage.text
        
        if textMessage.isViewed {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                cell.isViewed = textMessage.isViewed
            })
        } else {
            cell.isViewed = textMessage.isViewed
        }
    
        if let createdDate = textMessage.createdDate {
            cell.date = ChatMessageDateFormatter.shared.string(from: createdDate)
        } else {
            cell.date = nil
        }
        
        let chatMember = self.chat?.members?.allObjects.first(where: { member -> Bool in
            return (member as? ChatMember)?.user?.uid == textMessage.userUID
        }) as? ChatMember
        
        if textMessage.isAnonymous {
            cell.author = chatMember?.user?.relationshipStatus?.localized() ?? "Unknown".localized()
            
            cell.cloudViewColor = Colors.incomingAnonymousCloudView
        } else {
            cell.author = chatMember?.user?.fullName
            
            cell.cloudViewColor = Colors.incomingNotAnonymousCloudView
        }
    }

    private func configure(incomingTextMessageCell cell: IncomingTextMessageWithAvatarTableViewCell, with textMessage: ChatTextMessage) {
        self.configure(incomingTextMessageCell: cell as IncomingTextMessageTableViewCell, with: textMessage)
    }
    
    private func configure(chatmatesListCell cell: ChatmatesListTableViewCell) {
        guard let chatMembers = self.chat?.members?.allObjects as? [ChatMember] else {
            return
        }
        
        guard let accountUserUID = Services.accountAccess?.userUID else {
            fatalError()
        }
        
        let filteredChatMembers = chatMembers.filter { chatMember in
            return chatMember.user?.uid != accountUserUID
        }
        
        var userTokenViews: [UserTokenView] = []
        
        for chatMember in filteredChatMembers {
            guard let user = chatMember.user else {
                return
            }
            
            if let token = UserTokenView.initWithTitle(user.fullName, imageURL: user.avatar?.smallImageURL) {
                userTokenViews.append(token)
            }
        }
        
        cell.userTokenViews = userTokenViews
    }
    
    private func configure(incomingPhotoMessageCell cell: IncomingPhotoMessageTableViewCell, with photoMessage: ChatPhotoMessage) {
        if let createdDate = photoMessage.createdDate {
            cell.date = ChatMessageDateFormatter.shared.string(from: createdDate)
        } else {
            cell.date = nil
        }

        cell.photoImage = nil

        cell.onViewPhotoButtonClicked = { [unowned self, weak photoMessage] in
            if let photoMessage = photoMessage {
                self.onPhotoTapped?(photoMessage)
            }
        }
        
        let chatMember = self.chat?.members?.allObjects.first(where: { member -> Bool in
            return (member as? ChatMember)?.user?.uid == photoMessage.userUID
        }) as? ChatMember
        
        if photoMessage.isAnonymous {
            cell.author = chatMember?.user?.relationshipStatus?.localized() ?? "Unknown".localized()
            
            cell.cloudViewColor = Colors.incomingAnonymousCloudView
        } else {
            cell.author = chatMember?.user?.fullName
            
            cell.cloudViewColor = Colors.incomingNotAnonymousCloudView
        }
    }

    private func configure(incomingPhotoMessageCell cell: IncomingPhotoMessageWithAvatarTableViewCell, with photoMessage: ChatPhotoMessage) {
        self.configure(incomingPhotoMessageCell: cell as IncomingPhotoMessageTableViewCell, with: photoMessage)
    }
}
