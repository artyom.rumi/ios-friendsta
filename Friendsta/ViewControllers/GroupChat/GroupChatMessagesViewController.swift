//
//  GroupChatMessagesViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 22.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class GroupChatMessagesViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    private enum Role {
        
        // MARK: - Enumeration Cases
        
        case anonymous
        case revealed
        
        // MARK: - Type Methods
        
        mutating func toggle() {
            switch self {
            case .anonymous:
                self = .revealed
                
            case .revealed:
                self = .anonymous
            }
        }
    }
    
    // MARK: -
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let unauthorize = "Unauthorize"
        
        static let sendChatPhoto = "SendChatPhoto"
        static let showChatPhoto = "ShowChatPhoto"
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let messagesSection = 0
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var anonymousSwitchView: AnonymousSwitchView!
    
    @IBOutlet private weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var inputTextViewContainer: UIView!
    @IBOutlet private weak var inputTextView: TextView!
    @IBOutlet private weak var inputTextViewPlaceholderLabel: UILabel!
    
    @IBOutlet private weak var sendButton: UIButton!
    
    @IBOutlet private weak var tableView: UITableView!
    
    @IBOutlet private weak var welcomeMessageView: UIView!
    
    // MARK: -
    
    private var currentRole: Role = .anonymous
    
    private var chat: Chat?
    
    private var tableViewDataSource = GroupChatMessagesTableViewDataSource()
    private var tableViewDelegate = GroupChatMessagesTableViewDelegate()
    
    private var isRefreshingData = false
    private var hasAppliedData = false
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
        self.unsubscribeFromChatMessagesEvents()
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onChatPhotoSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onChatPhotoSendingFinished(withSegue: \(String(describing: segue.identifier)))", from: self)
    }
    
    @objc private func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.refreshChatMessages()
    }
    
    @IBAction private func onSendButtonTouchUpInside(_ sender: Any) {
        Log.high("onSendButtonTouchUpInside", from: self)
        
        if !self.inputTextView.text.isEmpty {
            self.sendChatMessage(with: self.inputTextView.text)
        }
    }
    
    @IBAction private func onSendPhotoButtonTouchUpInside(_ sender: Any) {
        Log.high("onSendPhotoButtonTouchUpInside()", from: self)
        
        self.performSegue(withIdentifier: Segues.sendChatPhoto, sender: self.chat)
    }
    
    // MARK: -
    
    private func sendChatMessage(with text: String) {
        Log.high("sendChatMessage(withText: \(text))", from: self)
        
        guard let chat = self.chat else {
            fatalError()
        }
        
        let isAnonymous = (self.currentRole == .anonymous)
        
        firstly {
            Services.chatMessagesService.send(text: text, isAnonymous: isAnonymous, to: chat)
        }.catch { error in
            self.showMessage(withError: error)
        }
        
        self.inputTextView.text = nil
        self.sendButton.isEnabled = false
        
        if self.tableView.numberOfRows(inSection: Constants.messagesSection) > 0 {
            self.tableView.scrollToRow(at: IndexPath(row: 0, section: Constants.messagesSection), at: .top, animated: false)
        }
    }
    
    private func markAsViewed(chatMessage: ChatMessage) {
        Log.high("markAsViewed(chatMessage: \(String(describing: chatMessage.uid)))", from: self)
        
        firstly {
            Services.chatMessagesService.markAsViewed(chatMessage: chatMessage)
        }.catch { error in
            self.handle(silentError: error)
        }
    }
    
    private func refreshChatMessages() {
        Log.high("refreshChatMessages()", from: self)
        
        guard let chat = self.chat else {
            return
        }
        
        self.isRefreshingData = true
        
        firstly {
            Services.chatsService.refresh(chat: chat)
        }.ensure {
            self.isRefreshingData = false
        }.done { chat in
            self.apply(chat: chat)
        }.catch { error in
            self.handle(silentError: error)
        }
    }
    
    private func subscribeToChatMessagesEvents() {
        self.unsubscribeFromChatMessagesEvents()
        
        let chatMessagesManager = Services.cacheViewContext.chatMessagesManager
        
        chatMessagesManager.objectsChangedEvent.connect(self, handler: { [weak self] chatMessages in
            if let viewController = self, let chat = viewController.chat {
                viewController.apply(chat: Services.cacheViewContext.chatsManager.firstOrNew(withUID: chat.uid))
            }
        })
        
        chatMessagesManager.startObserving()
    }
    
    private func unsubscribeFromChatMessagesEvents() {
        Services.cacheViewContext.chatMessagesManager.objectsChangedEvent.disconnect(self)
    }
    
    // MARK: -
    
    private func configureAnonymousSwitchView() {
        guard let accountUserFullName = Services.accountUser?.fullName else {
            fatalError()
        }
        
        switch self.currentRole {
        case .anonymous:
            self.anonymousSwitchView.backgroundColor = Colors.anonymousView
            self.anonymousSwitchView.currentRole = "You are anonymous".localized()
            self.anonymousSwitchView.changeRoleButtonTitle = "Be \(accountUserFullName)".localized()
            self.inputTextViewPlaceholderLabel.text = "You are anonymous".localized()
            
        case .revealed:
            self.anonymousSwitchView.backgroundColor = Colors.revealedView
            self.anonymousSwitchView.currentRole = "You are \(accountUserFullName)".localized()
            self.anonymousSwitchView.changeRoleButtonTitle = "Be anonymous".localized()
            self.inputTextViewPlaceholderLabel.text = "Reply as \(accountUserFullName)".localized()
        }
        
        self.anonymousSwitchView.onChangeRoleButtonTapped = {
            self.currentRole.toggle()
            
            self.configureAnonymousSwitchView()
        }
    }
    
    private func configureInputTextView() {
        self.inputTextView.textContainerInset = .zero
        self.inputTextView.textContainer.lineFragmentPadding = 0
    }
    
    private func configureInputTextViewContrainer() {
        self.inputTextViewContainer.layer.cornerRadius = 12.0
        self.inputTextViewContainer.layer.shadowColor = UIColor.black.cgColor
        self.inputTextViewContainer.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.inputTextViewContainer.layer.shadowOpacity = 0.5
        self.inputTextViewContainer.layer.masksToBounds = false
        self.inputTextViewContainer.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
    
    private func configureTableView() {
        self.tableView.dataSource = self.tableViewDataSource
        self.tableView.delegate = self.tableViewDelegate
        
        self.tableView.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
        
        self.tableViewDataSource.registerCells(tableView: self.tableView)
        
        self.tableViewDataSource.onErrorButtonTapped = { chatMessage in
            self.showSendingError(chatMessage: chatMessage)
        }
        
        self.tableViewDelegate.onMessageViewed = { chatMessage in
            self.markAsViewed(chatMessage: chatMessage)
        }
        
        self.tableViewDataSource.onPhotoTapped = { photoMessage in
            self.performSegue(withIdentifier: Segues.showChatPhoto, sender: photoMessage)
        }
    }
    
    private func configureNavigationItem() {
        self.navigationItem.title = self.chat?.name
    }
    
    private func configureWelcomeMessage() {
        self.welcomeMessageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
    }
    
    private func showSendingError(chatMessage: ChatMessage?) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alertController.addAction(UIAlertAction(title: "Retry".localized(), style: .default, handler: { action in
            if let chatMessage = chatMessage, !chatMessage.isSent {
                firstly {
                    Services.chatMessagesService.resend(chatMessage: chatMessage)
                }.catch { error in
                    self.handle(silentError: error)
                }
            }
        }))

        alertController.addAction(UIAlertAction(title: "Delete".localized(), style: .destructive, handler: { action in
            if let chatMessage = chatMessage {
                firstly {
                    Services.chatMessagesService.delete(chatMessage: chatMessage)
                }.catch { error in
                    self.handle(silentError: error)
                }
            }
        }))

        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel))

        self.present(alertController, animated: true)
    }
    
    // MARK: -
    
    private func handle(silentError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            break
        }
    }
    
    // MARK: -
    
    private func apply(chat: Chat) {
        Log.high("apply(chat: \(chat.uid))", from: self)
        
        self.chat = chat
        
        let chatMessages = Services.cacheViewContext.chatMessagesManager.fetch(withChatUID: chat.uid)
        
        self.tableViewDataSource.apply(chat: chat, chatMessages: chatMessages)
        self.tableViewDelegate.apply(chat: chat, chatMessages: chatMessages)
        
        if self.isViewLoaded {
            self.tableView.reloadData()
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureInputTextViewContrainer()
        self.configureInputTextView()
        self.configureAnonymousSwitchView()
        self.configureTableView()
        self.configureNavigationItem()
        self.configureWelcomeMessage()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isRefreshingData = false
        
        if let chat = self.chat, !self.hasAppliedData {
            self.apply(chat: chat)
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onApplicationWillEnterForeground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        self.subscribeToKeyboardNotifications()
        self.subscribeToChatMessagesEvents()
        
        self.refreshChatMessages()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
        self.hasAppliedData = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {
        case Segues.sendChatPhoto:
            guard let chat = sender as? Chat else {
                return
            }
            
            let isAnonymous = (self.currentRole == .anonymous)
            
            dictionaryReceiver?.apply(dictionary: ["chat": chat,
                                                   "source": PhotoCreationSource.chat,
                                                   "isAnonymousMessage": isAnonymous])
            
        case Segues.showChatPhoto:
            guard let chatPhotoMessage = sender as? ChatPhotoMessage else {
                fatalError()
            }
            
            dictionaryReceiver?.apply(dictionary: ["chatPhotoMessage": chatPhotoMessage])
            
        default:
            return
        }
    }
}

// MARK: - UITextViewDelegate

extension GroupChatMessagesViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let replacedText: String
        
        self.inputTextViewPlaceholderLabel.isHidden = !(text.isEmpty && textView.text.isEmpty)
        
        guard let range = Range(range, in: textView.text) else {
            fatalError()
        }
        
        if !textView.text.isEmpty {
            replacedText = textView.text.replacingCharacters(in: range, with: text)
        } else {
            replacedText = text
        }
        
        self.sendButton.isEnabled = !replacedText.isEmpty
        
        return true
    }
}

// MARK: - ContainerKeyboardHandler

extension GroupChatMessagesViewController: ContainerKeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

// MARK: - DictionaryReceiver

extension GroupChatMessagesViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let chat = dictionary["chat"] as? Chat else {
            return
        }
        
        self.apply(chat: chat)
    }
}
