//
//  TextReplyViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 08.04.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class TextReplyViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    enum Segues {
        
        // MARK: - Type Properties
        
        static let closeTextReply = "CloseTextReply"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var textReplyView: UIView!
    @IBOutlet fileprivate weak var backgroundImageView: UIImageView!
    
    @IBOutlet fileprivate weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var replyTextView: UITextView!
    @IBOutlet fileprivate weak var placeholderLabel: UILabel!
    
    @IBOutlet fileprivate weak var replyButton: UIButton!
    
    // MARK: -
    
    fileprivate var propCard: PropCard!
    
    // MARK: -
    
    var leftView: UIView {
        return self.textReplyView
    }
    
    var backgroundView: UIImageView {
        return self.backgroundImageView
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    //  MARK: - Instance Methods
    
    @IBAction fileprivate func onSwipeGestureRecognized(_ sender: Any) {
        Log.high("onSwipeGestureRecognized()", from: self)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction fileprivate func onReplyButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onReplyButtonTouchUpInside()", from: self)
        
        self.view.endEditing(true)
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true) {
            self.sendTextReply(text: self.replyTextView.text ?? "", loadingViewController: loadingViewController)
        }
    }
    
    // MARK: -
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:            
            let alertController = UIAlertController(title: nil, message: "Something went wrong or you already replied".localized(), preferredStyle: .alert)
            
            alertController.view.tintColor = Colors.primary
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self.present(alertController, animated: true)
        }
    }
    
    fileprivate func sendTextReply(text: String, loadingViewController: LoadingViewController) {
        Log.high("sendTextReply(text: \(text))", from: self)
        
        firstly {
            Services.propReactionsService.sendPropReaction(for: self.propCard, with: text)
        }.done { _ in
            loadingViewController.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: Segues.closeTextReply, sender: nil)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error)
            })
        }
    }
    
    fileprivate func setupTextReplyView() {
        let path = UIBezierPath(roundedRect:self.textReplyView.bounds, byRoundingCorners:[.topLeft, .topRight], cornerRadii: CGSize(width: 25, height:  25))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.textReplyView.layer.mask = maskLayer
    }
    
    fileprivate func setupFont() {
        self.placeholderLabel.font = Fonts.regular(ofSize: 17.0)
        self.replyTextView.font = Fonts.regular(ofSize: 17.0)
        self.replyButton.titleLabel?.font = Fonts.regular(ofSize: 17.0)
    }
    
    func apply(propCard: PropCard) {
        Log.high("apply(propCard: \(propCard.uid)", from: self)
        
        self.propCard = propCard
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        
        self.placeholderLabel.isHidden = self.replyTextView.hasText
        self.replyButton.isEnabled = self.replyTextView.hasText
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.setupTextReplyView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribeToKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        
        self.unsubscribeFromKeyboardNotifications()
    }
}

// MARK: - UITextViewDelegate

extension TextReplyViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textViewDidChange(_ textView: UITextView) {
        self.placeholderLabel.isHidden = self.replyTextView.hasText
        self.replyButton.isEnabled = self.replyTextView.hasText
    }
}

// MARK: - KeyboardHandler

extension TextReplyViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

// MARK: - DictionaryReceiver

extension TextReplyViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String : Any]) {
        guard let propCard = dictionary["propCard"] as? PropCard else {
            return
        }
        
        self.apply(propCard: propCard)
    }
}
