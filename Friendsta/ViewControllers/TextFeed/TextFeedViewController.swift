//
//  TextFeedViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools

class TextFeedViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let textFeedPostingFinished = "TextFeedPostingFinished"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var textView: UITextView!
    
    @IBOutlet private weak var placeholderLabel: UILabel!
    
    @IBOutlet private weak var postButton: UIButton!
    
    @IBOutlet private weak var bottomSpacerHeightConstraint: NSLayoutConstraint!
    
    // MARK: -
    
    var onSendTextFeedFinished: (() -> Void)?

    var shouldShowKeyboardOnViewWillAppear = true
    
    // MARK: - Instance Methods
    
    @IBAction private func onPostButtonTouchUpInside(_ sender: Any) {
        Log.high("onPostButtonTouchUpInside()", from: self)
    
        self.view.endEditing(true)
        
        let image = self.renderPhoto(with: self.textView.text)
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            self.sendFeed(with: image, loadingViewController: loadingViewController)
        })
    }
    
    // MARK: -
    
    private func renderPhoto(with text: String) -> UIImage {
        Log.high("renderPhoto(with text: \(text))", from: self)
        
        let containerView = UIView(frame: self.view.bounds)
        
        let label = UILabel()
        
        label.text = text
        label.font = Fonts.medium(ofSize: 24.0)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        label.numberOfLines = 0
        label.textColor = Colors.whiteText
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        
        let imageView = UIImageView(image: Images.inboxBackground)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(imageView)
        containerView.addSubview(label)
        
        NSLayoutConstraint.activate([
            containerView.heightAnchor.constraint(equalToConstant: self.view.frame.height),
            containerView.widthAnchor.constraint(equalToConstant: self.view.frame.width)
        ])
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: containerView.topAnchor),
            imageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            imageView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            imageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: imageView.topAnchor, constant: 100.0),
            label.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -100.0),
            label.leadingAnchor.constraint(equalTo: imageView.leadingAnchor, constant: 60.0),
            label.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -60.0)
        ])
        
        containerView.layoutIfNeeded()
        
        return containerView.asImage()
    }
    
    private func sendFeed(with photoImage: UIImage, loadingViewController: LoadingViewController) {
        Log.high("sendFeed(with photoImage: \(photoImage.size))", from: self)
        
        DispatchQueue.global(qos: .userInitiated).async(execute: {
            var photo = photoImage
            
            if photoImage.size.width > Limits.photoImageMaxWidth {
                if let photoImage = photoImage.scaled(to: Limits.photoImageMaxWidth) {
                    photo = photoImage
                }
            }
            
            firstly {
                Services.feedService.sendFeed(photo: photo)
            }.done { feed in
                loadingViewController.dismiss(animated: true, completion: {
                    if let imageURL = feed.photo?.imageURL {
                        Services.imageLoader.saveImage(for: imageURL, image: photo)
                    }
                    
                    self.onSendTextFeedFinished?()
                    
                    self.textView.text = ""
                    
                    self.performSegue(withIdentifier: Segues.textFeedPostingFinished, sender: self)
                })
            }.catch { error in
                loadingViewController.dismiss(animated: true, completion: {
                    self.showMessage(withError: error)
                })
            }
        })
    }
    
    // MARK: -
    
    private func updatePostButtonState() {
        self.postButton.isEnabled = self.textView.hasText
    }
    
    private func updatePlaceholderLabelState() {
        self.placeholderLabel.isHidden = self.textView.hasText
    }
    
    // MARK: - UIViewController
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.shouldShowKeyboardOnViewWillAppear {
            self.showKeyboard()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)

        self.unsubscribeFromKeyboardNotifications()
    }

    // MARK: -

    func showKeyboard() {
        self.subscribeToKeyboardNotifications()

        self.textView.becomeFirstResponder()
    }
}

// MARK: - UITextViewDelegate

extension TextFeedViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textViewDidChange(_ textView: UITextView) {
        self.updatePlaceholderLabelState()
        
        self.updatePostButtonState()
    }
}

// MARK: - KeyboardHandler

extension TextFeedViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

// MARK: - DictionaryReceiver

extension TextFeedViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let onSendTextFeedFinished = dictionary["onSendTextFeedFinished"] as? (() -> Void) else {
            return
        }
        
        self.onSendTextFeedFinished = onSendTextFeedFinished
    }
}
