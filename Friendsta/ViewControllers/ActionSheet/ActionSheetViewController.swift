//
//  ActionSheetViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 07.01.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class ActionSheetViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let closeActionSheet = "CloseActionSheet"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var actionSheetView: UIView!
    
    @IBOutlet fileprivate weak var secretBoostSlider: SliderView!
    @IBOutlet fileprivate weak var friendBoostSlider: SliderView!
    
    fileprivate var delegate: ActionSheetDelegate?
    
    fileprivate var propListItem: PropListItem?
    
    fileprivate(set) var hasAppliedData = false
    fileprivate(set) var shouldHideFriendSlider = false
    
    var bottomView: UIView {
        return self.actionSheetView
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onCloseButtonTouchUpInside(_ sender: Any) {
        Log.high("onCloseButtonTouchUpInside()", from: self)
        
        self.dismiss(animated: true)
    }

    fileprivate func apply(propListItem: PropListItem, shouldHideFriendSlider: Bool) {
        self.propListItem = propListItem
        
        self.shouldHideFriendSlider = shouldHideFriendSlider
        
        if self.isViewLoaded {
            self.friendBoostSlider.isHidden = self.shouldHideFriendSlider
            
            self.secretBoostSlider.didSliderCompleted = { [unowned self] in
                // TODO: - Add secret parameter to sender
                self.performSegue(withIdentifier: Segues.closeActionSheet, sender: ["propListItem": propListItem, "boostType": BoostType.secret])
            }
            
            self.friendBoostSlider.didSliderCompleted = { [unowned self] in
                // TODO: - Add friend parameter to sender
                self.performSegue(withIdentifier: Segues.closeActionSheet, sender: ["propListItem": propListItem, "boostType": BoostType.friend])
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.actionSheetView.layer.cornerRadius = 9.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let propListItem = self.propListItem, !self.hasAppliedData {
            self.apply(propListItem: propListItem, shouldHideFriendSlider: self.shouldHideFriendSlider)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.didActionSheetClosed()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.closeActionSheet:
            guard let dictionary = sender as? [String: Any] else {
                fatalError()
            }
            
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: dictionary)
            }
            
        default:
            return
        }
    }
}

// MARK: - DictionaryReceiver

extension ActionSheetViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let propListItem = dictionary["propListItem"] as? PropListItem else {
            return
        }
        
        guard let isFriendOrSuperFriend = dictionary["isFriendOrSuperFriend"] as? Bool else {
            return
        }
        
        self.apply(propListItem: propListItem, shouldHideFriendSlider: isFriendOrSuperFriend)
        
        guard let delegate = dictionary["actionSheetDelegate"] else {
            return
        }
        
        self.delegate = delegate as? ActionSheetDelegate
    }
}
