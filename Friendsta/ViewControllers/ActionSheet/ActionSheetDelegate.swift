//
//  ActionSheetDelegate.swift
//  Friendsta
//
//  Created by Elina Batyrova on 11.01.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ActionSheetDelegate {
    
    // MARK: - Instance Methods
    
    func didActionSheetClosed()
}
