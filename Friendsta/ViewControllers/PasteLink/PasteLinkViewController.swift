//
//  PasteLinkViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 28.11.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools
import PromiseKit
import SVProgressHUD

class PasteLinkViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let successMessageText = "Posted!"
        
        static let linkPreviewHeight: CGFloat = 180
        static let cornerRadius: CGFloat = 12
    }
    
    // MARK: -
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let linkPostingFinished = "LinkPostingFinished"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var contentView: UIView!
    
    @IBOutlet private weak var linkTextView: UITextView!
    @IBOutlet private weak var linkTextViewPlaceholderLabel: UILabel!
    
    @IBOutlet private weak var descriptionTextView: UITextView!
    @IBOutlet private weak var descriptionTextViewPlaceholderLabel: UILabel!
    
    @IBOutlet private weak var linkPreviewImageView: UIImageView!
    
    @IBOutlet private weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var photoImageHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var linkInformationView: UIView!
    @IBOutlet private weak var linkDescriptionLabel: UILabel!
    @IBOutlet private weak var linkLabel: UILabel!
    
    @IBOutlet private weak var loadingPreviewView: UIView!

    // MARK: - Instance Methods
    
    @IBAction private func onCloseButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onCloseButtonTouchUpInside()", from: self)
        
        self.dismiss(animated: true)
    }
    
    @IBAction private func onPostButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onPostButtonTouchUpInside()", from: self)
        
        let loadingViewController = LoadingViewController()
        
        self.descriptionTextView.resignFirstResponder()
        self.linkTextView.resignFirstResponder()
        
        let linkString = self.linkTextView.text.filter({ !$0.isNewline })
                
        if linkString.isValidURL, let link = URL(string: linkString) {
            self.present(loadingViewController, animated: true, completion: {
                self.share(link: link,
                           text: self.descriptionTextView.text,
                           loadingViewController: loadingViewController)
            })
        } else {
            self.showLinkError()
        }
    }
    
    // MARK: -
    
    private func showLinkError() {
        self.showMessage(withTitle: "Something went wrong",
                         message: "Please make sure that you pasted the link!",
                         okHandler: {
            self.linkTextView.becomeFirstResponder()
        })
    }
    
    // MARK: -
    
    private func share(link: URL, text: String?, loadingViewController: LoadingViewController) {
        firstly {
            Services.feedService.sendFeed(text: text, link: link)
        }.done {
            SVProgressHUD.showSuccess(withStatus: Constants.successMessageText)
            
            loadingViewController.dismiss(animated: true, completion: {
                SVProgressHUD.dismiss(completion: {
                    self.performSegue(withIdentifier: Segues.linkPostingFinished, sender: self)
                })
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.showMessage(withError: error)
            })
        }
    }
    
    // MARK: -
    
    private func setupContent() {
        self.contentView.layer.cornerRadius = Constants.cornerRadius
        
        self.linkPreviewImageView.layer.cornerRadius = Constants.cornerRadius
        self.linkPreviewImageView.clipsToBounds = true
        self.linkPreviewImageView.layer.borderColor = UIColor.lightGray.cgColor
        self.linkPreviewImageView.layer.borderWidth = 1
        
        self.linkInformationView.clipsToBounds = true
        self.linkInformationView.layer.cornerRadius = Constants.cornerRadius
        self.linkInformationView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        self.linkInformationView.layer.borderColor = UIColor.lightGray.cgColor
        self.linkInformationView.layer.borderWidth = 1
        
        self.loadingPreviewView.isHidden = false
    }
    
    private func loadPreviewFor(link: URL) {
        Managers.linkPreviewManager.getInformationFrom(link: link, completion: { linkString, linkDescription, imageURLString, error in
            if let imageURLString = imageURLString, let imageURL = URL(string: imageURLString) {
                Managers.imageLoader.loadImage(for: imageURL, completionHandler: { image in
                    DispatchQueue.main.async {
                        self.loadingPreviewView.isHidden = true

                        self.linkPreviewImageView.image = image
                    }
                })
            }
            
            DispatchQueue.main.async {
                self.linkDescriptionLabel.text = linkDescription
                self.linkLabel.text = linkString
            }
        })
    }
    
    private func setupPreviewWith(link: String) {
        let linkString = self.linkTextView.text.filter({ !$0.isNewline })
        
        if let linkURL = URL(string: linkString) {
            self.loadPreviewFor(link: linkURL)
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupContent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribeToKeyboardNotifications()
        
        self.linkTextView.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unsubscribeFromKeyboardNotifications()
    }
}

// MARK: - KeyboardHandler

extension PasteLinkViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

// MARK: - UITextViewDelegate

extension PasteLinkViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == self.descriptionTextView, !self.linkTextView.text.isEmpty {
            self.setupPreviewWith(link: self.linkTextView.text)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        switch textView {
        case self.descriptionTextView:
            self.descriptionTextViewPlaceholderLabel.isHidden = !(text.isEmpty && textView.text.isEmpty)
            
            return true
            
        case self.linkTextView:
            self.linkTextViewPlaceholderLabel.isHidden = !(text.isEmpty && textView.text.isEmpty)
            
            if text.contains(where: { $0.isNewline }) {
                self.setupPreviewWith(link: textView.text)
            }
            
            return true
            
        default:
            return false
        }
    }
}
