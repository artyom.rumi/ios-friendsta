//
//  EditChatNameViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 21.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools

class EditChatNameViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let showGroupChatMessages = "ShowGroupChatMessages"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var groupNameTextField: UITextField!
    
    @IBOutlet private weak var doneBarButtonItem: UIBarButtonItem!
    
    // MARK: -
    
    private var selectedUsers: [User]?
    
    // MARK: - Instance Methods
    
    @IBAction private func onDoneBarButtonItemTouchUpInside(_ sender: UIBarButtonItem) {
        Log.high("onDoneBarButtonItemTouchUpInside()", from: self)
        
        guard let users = self.selectedUsers else {
            fatalError()
        }
        
        guard let groupName = self.groupNameTextField.text else {
            fatalError()
        }
        
        guard !groupName.isEmpty else {
            fatalError()
        }
        
        let loadingViewController = LoadingViewController()
        
        self.groupNameTextField.resignFirstResponder()
        
        self.present(loadingViewController, animated: true, completion: {
            self.createChat(name: groupName, users: users, loadingViewController: loadingViewController)
        })
    }
    
    // MARK: -
    
    private func createChat(name: String, users: [User], loadingViewController: LoadingViewController) {
        Log.high("createChat(name: \(name), users: \(users.count))", from: self)
    
        firstly {
            Services.chatsService.createChat(with: users, name: name)
        }.done { chat in
            loadingViewController.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: Segues.showGroupChatMessages, sender: chat)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.showMessage(withError: error)
            })
        }
    }
    
    // MARK: -
    
    private func configureDoneBarButtonItem(text: String) {
        self.doneBarButtonItem.isEnabled = !(text.isEmpty)
    }
    
    // MARK: -
    
    private func apply(selectedUsers: [User]) {
        Log.high("apply(selectedUsers: \(selectedUsers.count))", from: self)
        
        self.selectedUsers = selectedUsers
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureDoneBarButtonItem(text: self.groupNameTextField.text ?? "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.groupNameTextField.becomeFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {
        case Segues.showGroupChatMessages:
            guard let chat = sender as? Chat else {
                return
            }
            
            dictionaryReceiver?.apply(dictionary: ["chat": chat])
            
        default:
            return
        }
    }
}

// MARK: - UITextFieldDelegate

extension EditChatNameViewController: UITextFieldDelegate {
    
    // MARK: - Instance Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let replacedText: String
        
        if let text = self.groupNameTextField.text, let range = Range(range, in: text) {
            replacedText = text.replacingCharacters(in: range, with: string)
        } else {
            replacedText = string
        }
        
        self.configureDoneBarButtonItem(text: replacedText)
        
        return true
    }
}

// MARK: - DictionaryReceiver

extension EditChatNameViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let selectedUsers = dictionary["selectedUsers"] as? [User] else {
            return
        }
        
        self.apply(selectedUsers: selectedUsers)
    }
}
