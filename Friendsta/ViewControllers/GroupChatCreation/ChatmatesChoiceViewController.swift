//
//  ChatmatesChoiceViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 14.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class ChatmatesChoiceViewController: LoggedViewController, EmptyStateViewCustomer {
    
    // MARK: - Nested Types
    
    private enum Identifiers {
        
        // MARK: - Type Properties
        
        static let sectionHeaderView = "SectionHeaderView"
        static let userTableCell = "UserTableCell"
    }
    
    // MARK: -
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let unauthorize = "Unauthorize"
        static let showEditChatName = "ShowEditChatName"
    }
    
    // MARK: -
    
    private enum FilterType {
        
        // MARK: - Enumeration Cases
        
        case none
        case search(text: String)
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let tokenViewMinHeight: CGFloat = 48.0
        static let tokenViewMaxHeight: CGFloat = 150.0
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var tableView: UITableView!
    
    @IBOutlet private weak var tokenViewHolder: TokenViewHolder!
    @IBOutlet private weak var tokenViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var emptyStateViewOutlet: EmptyStateView!
    
    @IBOutlet private weak var nextBarButtonItem: UIBarButtonItem!
    
    @IBOutlet private weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    private weak var tableRefreshControl: UIRefreshControl!
    
    // MARK: -
    
    private var allUsers: [User] = []
    private var users: [User] = []
    private var selectedUsers: [User] = []
    
    private var shouldRefreshData = true
    
    private var filterType: FilterType = .none
    
    // MARK: -
    
    lazy var emptyStateView: EmptyStateView! = {
        return self.emptyStateViewOutlet
    }()
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onNextBarButtonItemTouchUpInside(_ sender: UIBarButtonItem) {
        Log.high("onNextBarButtonItemTouchUpInside()", from: self)
        
        guard !self.selectedUsers.isEmpty else {
            fatalError()
        }
        
        self.performSegue(withIdentifier: Segues.showEditChatName, sender: self.selectedUsers)
    }
    
    @objc private func onTableRefreshControlRequested(_ sender: Any) {
        Log.high("onTableRefreshControlRequested()", from: self)

        self.refreshUserList()
    }
    
    // MARK: -
    
    private func refreshUserList() {
        Log.high("refreshUserList", from: self)
        
        if !self.tableRefreshControl.isRefreshing {
            if self.allUsers.isEmpty || !self.emptyStateView.isHidden || self.shouldRefreshData {
                self.showLoadingState()
            }
        }
        
        firstly {
            Services.usersService.refreshAllUserList(of: .pokeSchoolmates)
        }.ensure {
            if self.tableRefreshControl.isRefreshing {
                self.tableRefreshControl.endRefreshing()
            }
        }.done { friendsUserList in
            self.apply(filterType: self.filterType, users: friendsUserList.allUsers)
            
            self.shouldRefreshData = false
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshUserList()
            })
        }
    }
    
    // MARK: -
    
    private func showLoadingState() {
        if self.emptyStateView.isHidden {
            let title = "Searching your friends".localized()
            let message = "We are loading list of your friends. Please wait a bit...".localized()

            self.showEmptyState(title: title, message: message, activity: true)
        } else {
            self.emptyStateView.showActivityIndicator()
        }
    }

    private func showNoDataState() {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: { [unowned self] in
            self.refreshUserList()
        })

        self.showEmptyState(title: "Friends not found".localized(),
                            message: "You can invite friends on People tab.".localized(),
                            action: action)
    }

    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: {
            retryHandler?()
        })

        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)

        case .some(.connection), .some(.timeOut):
            if self.allUsers.isEmpty {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            }

        default:
            if self.allUsers.isEmpty {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        }
    }
    
    // MARK: -
    
    private func configureTableRefreshControl() {
        let tableRefreshControl = UIRefreshControl()

        tableRefreshControl.addTarget(self,
                                      action: #selector(self.onTableRefreshControlRequested(_:)),
                                      for: .valueChanged)

        self.tableView.refreshControl = tableRefreshControl

        self.tableRefreshControl = tableRefreshControl
    }
    
    private func configureNextBarButtonItem() {
        self.nextBarButtonItem.isEnabled = (!self.selectedUsers.isEmpty)
    }
    
    private func configureTokenViewHolder() {
        self.tokenViewHolder.layoutIfNeeded()
        
        self.tokenViewHolder.dataSource = self
        self.tokenViewHolder.delegate = self
        
        self.tokenViewHolder.reloadData()
    }
    
    private func reloadTokenViewHolder() {
        self.tokenViewHolder.textView.text = ""
        
        self.tokenViewHolder.reloadData()
        self.tokenViewHolder.layoutIfNeeded()
    }
    
    // MARK: -
    
    private func setFilter(for text: String) {
        if !text.isEmpty {
            self.apply(filterType: .search(text: text), users: self.allUsers)
        } else {
            self.apply(filterType: .none, users: self.allUsers)
        }
    }
    
    private func apply(filterType: FilterType, users: [User]) {
        Log.high("apply(userList: \(users.count))", from: self)
        
        self.allUsers = users
        self.filterType = filterType
        
        switch filterType {
        case .none:
            self.users = self.allUsers
        
        case .search(let text):
            let searchText = text.lowercased()
            
            self.users = self.allUsers.filter({ user in
                return user.fullName?.lowercased().contains(searchText) ?? false
            })
        }
        
        if self.allUsers.isEmpty {
            self.showNoDataState()
        } else {
            self.hideEmptyState()
        }
        
        if self.isViewAppeared {
            self.tableView.beginUpdates()
            self.tableView.reloadSections(IndexSet(integersIn: 0..<self.tableView.numberOfSections), with: .fade)
            self.tableView.endUpdates()
        } else {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableRefreshControl()
        self.configureTokenViewHolder()
        self.configureNextBarButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribeToKeyboardNotifications()
        
        let friendsUserList = Services.cacheViewContext.userListManager.firstOrNew(withListType: .all(type: .pokeSchoolmates))
        
        self.apply(filterType: self.filterType, users: friendsUserList.allUsers)
        
        if self.shouldRefreshData {
            self.refreshUserList()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {
        case Segues.showEditChatName:
            guard let selectedUsers = sender as? [User] else {
                return
            }
            
            dictionaryReceiver?.apply(dictionary: ["selectedUsers": selectedUsers])
            
        default:
            return
        }
    }
}

// MARK: - UITableViewDataSource

extension ChatmatesChoiceViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.userTableCell, for: indexPath)
        
        self.configure(chatmateCell: cell as! ChatmateTableViewCell, at: indexPath)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ChatmatesChoiceViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ChatmateTableViewCell else {
            return
        }
        
        guard let avatarURL = self.users[indexPath.row].smallAvatarURL else {
            return
        }
        
        Services.imageLoader.loadImage(for: avatarURL, in: cell.avatarImageView, placeholder: Images.avatarSmallPlaceholder)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        let user = self.users[indexPath.row]
        
        let cell = tableView.cellForRow(at: indexPath) as? ChatmateTableViewCell
        
        if self.selectedUsers.contains(object: user) {
            cell?.isChoosen = false
            
            self.selectedUsers.remove(object: user)
        } else {
            cell?.isChoosen = true
            
            self.selectedUsers.append(user)
            
            if !self.tokenViewHolder.textView.text.isEmpty {
                self.setFilter(for: "")
            }
        }
        
        self.reloadTokenViewHolder()
        
        self.configureNextBarButtonItem()
    }
}

// MARK: - UITableViewDataSource Helpers

extension ChatmatesChoiceViewController {
    
    // MARK: - Instance Methods
    
    private func configure(chatmateCell: ChatmateTableViewCell, at indexPath: IndexPath) {
        let user = self.users[indexPath.row]
        
        chatmateCell.fullName = user.fullName
        
        let schoolTitle = user.schoolTitle ?? "unknown school".localized()
        
        if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(user.classYear)) {
            chatmateCell.schoolInfo = "\(schoolGradeNumber)TH GRADE, \(schoolTitle)".localized()
        } else {
            chatmateCell.schoolInfo = "FINISHED, \(schoolTitle)".localized()
        }
        
        chatmateCell.isChoosen = self.selectedUsers.contains(object: user)
    }
}

// MARK: - TokenViewDataSource

extension ChatmatesChoiceViewController: TokenViewDataSource {
    
    // MARK: - Instance Methods
    
    func insetsForTokenView(_ tokenView: TokenViewHolder) -> UIEdgeInsets? {
        return UIEdgeInsets(top: 6, left: 4, bottom: 5, right: 5)
    }
    
    func numberOfTokensForTokenView(_ tokenView: TokenViewHolder) -> Int {
        return self.selectedUsers.count
    }
    
    func titleForTokenViewPlaceholder(_ tokenView: TokenViewHolder) -> String? {
        return "Search".localized()
    }
    
    func fontForTokenView(_ tokenView: TokenViewHolder) -> UIFont? {
        return Fonts.regular(ofSize: 14.0)
    }
    
    func fontColorForTokenViewPlaceholder(_ tokenView: TokenViewHolder) -> UIColor? {
        return UIColor.black.withAlphaComponent(0.2)
    }
    
    func tokenView(_ tokenView: TokenViewHolder, viewForTokenAtIndex index: Int) -> UIView? {
        let user = self.selectedUsers[index]
        
        return UserTokenView.initWithTitle(user.firstName, imageURL: user.avatar?.smallImageURL)
    }
    
    func tintColorForTokenView(_ tokenView: TokenViewHolder) -> UIColor? {
        return Colors.secondary
    }
}

// MARK: - TokenViewDelegate

extension ChatmatesChoiceViewController: TokenViewDelegate {
    
    // MARK: - Instance Methods
    
    func tokenView(_ tokenView: TokenViewHolder, didDeleteTokenAtIndex index: Int) {
        if self.selectedUsers.count > index {
            var userToDelete: User?
            
            self.apply(filterType: .none, users: self.allUsers)
            
            _ = self.users.firstIndex { user in
                if user == self.selectedUsers[index] {
                    userToDelete = user
                    
                    return true
                }
                
                return false
            }
            
            guard let user = userToDelete else {
                return
            }
            
            self.selectedUsers.remove(object: user)
            
            self.tableView.reloadData()
            
            self.reloadTokenViewHolder()
            
            self.tokenViewHolder.textView.becomeFirstResponder()
        }
    }
    
    func tokenView(_ tokenView: TokenViewHolder, didChangeText text: String) {
        self.setFilter(for: text)
    }
    
    func tokenView(_ tokenView: TokenViewHolder, didEnterText text: String) {
        self.setFilter(for: text)
    }
    
    func tokenView(_ tokenView: TokenViewHolder, contentSizeChanged size: CGSize) {
        self.tokenViewHeightConstraint.constant = max(Constants.tokenViewMinHeight, min(size.height, Constants.tokenViewMaxHeight))
        
        self.view.layoutIfNeeded()
    }
}

// MARK: - KeyboardHandler

extension ChatmatesChoiceViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
