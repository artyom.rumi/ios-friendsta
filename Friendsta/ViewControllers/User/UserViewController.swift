//
//  AccountProfileViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 06.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class UserViewController: LoggedViewController, ErrorMessagePresenter {
    
    ///// ********* Lance's Code ********* /////
    public var showCancelButton = false
    
    fileprivate lazy var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "BlackCloseButton"), for: .normal)
        button.tintColor = .black
        button.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        return button
    }()

    @objc fileprivate func cancelButtonPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    fileprivate var cancelBarButtonItem: UIBarButtonItem?
    
    ///// ********* Lance's Code ********* /////
    
    // MARK: - Nested Types

    private enum Segues {

        // MARK: - Type Properties
        static let showFeedPreview = "ShowFeedPreview"

        static let unauthorize = "Unauthorize"
    }

    // MARK: -

    private enum Identifiers {

        // MARK: - Type Properties

        static let userProfileInfoCell = "UserProfileInfoCell"
        static let feedImageCell = "FeedImageCell"
        static let feedLinkCell = "FeedLinkCell"
        static let emptyStateCell = "EmptyStateCell"

        static let segmentedControlReusableView = "SegmentedControlReusableView"
    }

    // MARK: -

    private enum Constants {

        // MARK: - Type Properties

        static let numberOfSections = 2

        static let profileInfoSection = 0
        static let feedsSection = 1

        static let numberOfItemsInProfileSection = 1

        static let feedsSectionHeaderHeight: CGFloat = 50
        static let feedImageCellHeight: CGFloat = 128

        static let countOfSpacingBetweenItems: CGFloat = 3

        static let countOfEmptyStateCells = 1

        static let timelineLoadingMessage = "We are updating timeline... Please wait a bit...".localized()
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var collectionViewFlowLayout: UICollectionViewFlowLayout!

    @IBOutlet private weak var emptyStateView: EmptyStateView!

    // MARK: -

    private var userID: Int64?

    private var accountUser: User?

    private var isRefreshingData = false

    private var feeds: [Feed] = []

    private var shouldApplyData = false

    private var isInitialRefreshingData: Bool {
        return self.isRefreshingData && self.feeds.isEmpty
    }

    // MARK: -

    deinit {
        self.unsubscribeFromFeedEvents()
    }

    // MARK: - Instance Methods

    @objc private func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)

        self.refreshVisibleContent()
    }

    // MARK: -

    private func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()

        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action

        self.emptyStateView.isHidden = false
    }

    private func hideEmptyState() {
        self.emptyStateView.isHidden = true
    }

    private func showLoadingState() {
        if self.emptyStateView.isHidden {
            self.showEmptyState(title: "Loading profile".localized(),
                                message: "We are loading profile.\nPlease wait a bit.".localized())
        }

        self.emptyStateView.showActivityIndicator()
    }

    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action: EmptyStateAction?

        if let retryHandler = retryHandler {
            action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: retryHandler)
        } else {
            action = nil
        }

        switch error as? WebError {
        case .some(.unauthorized):
            self.shouldApplyData = true

            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)

        case .some(.connection), .some(.timeOut):
            if self.accountUser == nil {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            }

        default:
            if self.accountUser == nil {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        }
    }

    // MARK: -

    func apply(userID: Int64) {
        Log.high("apply(userID:\(userID)", from: self)

        self.userID = userID

        if self.shouldApplyData {
            if let user = Services.cacheViewContext.usersManager.first(with: userID) {
                self.apply(user: user)
            } else {
                self.refreshVisibleContent()
            }
        }
    }

    func apply(user: User) {
        Log.high("apply(user: \(user.uid)", from: self)

        self.accountUser = user
        self.userID = user.uid

        self.refreshTimelineContent()
    }

    private func refreshVisibleContent() {
        Log.high("refreshVisibleContent()", from: self)

        self.isRefreshingData = true

        if (self.accountUser == nil) || (!self.emptyStateView.isHidden) {
            self.showLoadingState()
        }

        if let userID = self.userID {
            self.showLoadingState()

            firstly {
                Services.usersService.refresh(userUID: userID)
            }.done { user in
                self.accountUser = user
                self.hideEmptyState()
                self.apply(user: user)
            }.catch { error in
                self.hideEmptyState()
                self.handle(stateError: error, retryHandler: self.refreshVisibleContent)
            }
        }
    }

    private func refreshTimelineContent() {
        if let userID = self.userID {
            self.showLoadingState()
            
            firstly {
                Services.feedService.fetchTimelineFeeds(for: userID)
            }.done { feedList in
                self.feeds = feedList.allFeeds
                self.hideEmptyState()
                self.isRefreshingData = false
                self.collectionView.reloadData()
                self.shouldApplyData = false
            }.catch { error in
                self.hideEmptyState()
                self.isRefreshingData = false
                self.handle(stateError: error, retryHandler: self.refreshTimelineContent)
            }
        }
    }

    // MARK: -

    private func subscribeToFeedEvents() {
        self.unsubscribeFromFeedEvents()

        let feedManager = Services.cacheViewContext.feedManager

        feedManager.objectsChangedEvent.connect(self, handler: { [weak self] feeds in
            self?.shouldApplyData = true
        })

        feedManager.startObserving()
    }

    private func unsubscribeFromFeedEvents() {
        Services.cacheViewContext.feedManager.objectsChangedEvent.disconnect(self)
    }

    // MARK: -

    private func configureCollectionViewFlowLayout() {
        let spacing = self.collectionViewFlowLayout.sectionInset.left + self.collectionViewFlowLayout.sectionInset.right

        let width = UIScreen.main.bounds.size.width - spacing

        self.collectionViewFlowLayout.estimatedItemSize = CGSize(width: width, height: 1)
    }

    private func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.shadowImage = nil
    }

    // MARK: - View Controller Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if showCancelButton {
            
            cancelBarButtonItem = UIBarButtonItem(customView: cancelButton)
            navigationItem.leftBarButtonItem = cancelBarButtonItem
        }
        
        self.configureCollectionViewFlowLayout()

        self.subscribeToFeedEvents()

        self.shouldApplyData = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.configureNavigationBar()

        if let user = self.accountUser {
            self.apply(user: user)
        } else if let userID = self.userID {
            self.apply(userID: userID)
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        let dictionaryReceiver: DictionaryReceiver?

        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }

        switch segue.identifier {
        case Segues.showFeedPreview:
            guard let feed = sender as? Feed else {
                return
            }

            dictionaryReceiver?.apply(dictionary: ["feed": feed])

        default:
            break
        }
    }
}

// MARK: - UICollectionViewDataSource

extension UserViewController: UICollectionViewDataSource {

    // MARK: - Instance Methods

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Constants.numberOfSections
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Constants.profileInfoSection:
            return Constants.numberOfItemsInProfileSection

        case Constants.feedsSection:
            if self.isInitialRefreshingData {
                return Constants.countOfEmptyStateCells
            } else {
                return self.feeds.count
            }

        default:
            fatalError()
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case Constants.profileInfoSection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.userProfileInfoCell, for: indexPath)

            self.configure(userProfileInfoCell: cell as! UserInfoCollectionViewCell)

            return cell

        case Constants.feedsSection:
            if self.isInitialRefreshingData {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.emptyStateCell, for: indexPath)

                self.configure(emptyStateCell: cell as! EmptyStateCollectionViewCell)

                return cell
            } else {
                let feed = self.feeds[indexPath.row]

                if feed.link != nil {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.feedLinkCell, for: indexPath)

                    return cell
                } else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.feedImageCell, for: indexPath)

                    return cell
                }
            }

        default:
            fatalError()
        }
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let reusableView = self.collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Identifiers.segmentedControlReusableView, for: indexPath)

            return reusableView
        } else {
            fatalError()
        }
    }
}

// MARK: - Cell and View Configurations

extension UserViewController {

    // MARK: - Instance Methods

    private func configure(emptyStateCell: EmptyStateCollectionViewCell) {
        if self.isRefreshingData {
            emptyStateCell.message = Constants.timelineLoadingMessage
            emptyStateCell.isActivityIndicatorHidden = false
        }
    }

    private func configure(userProfileInfoCell: UserInfoCollectionViewCell) {
        guard let accountUser = self.accountUser else {
            return
        }

        userProfileInfoCell.fullName = accountUser.fullName

        let schoolTitle = accountUser.schoolTitle ?? "unknown school".localized()

        if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(accountUser.classYear)) {
            userProfileInfoCell.schoolInfo = String(format: "%dth grade %@".localized(), schoolGradeNumber, schoolTitle)
        } else {
            userProfileInfoCell.schoolInfo = String(format: "Finished %@".localized(), schoolTitle)
        }

        let friendsCountText = NSMutableAttributedString(string: "\(accountUser.friendsCount)", attributes: [.font: Fonts.bold(ofSize: 15.0)])
        let friendsText = NSAttributedString(string: accountUser.friendsCount == 1 ? " friend".localized() : " friends".localized(), attributes: [.font: Fonts.regular(ofSize: 15.0)])

        friendsCountText.append(friendsText)

        userProfileInfoCell.friendsCountAttributedText = friendsCountText

        userProfileInfoCell.bioInfo = accountUser.bio

        userProfileInfoCell.set(friendship: accountUser.friendshipStatus)

        userProfileInfoCell.onAddButtonTapped = {
            userProfileInfoCell.addButtonActivityIndicatorAnimating = true

            firstly {
                Services.usersService.inviteUser(user: accountUser)
            }.done { _ in
                self.accountUser = Services.cacheViewContext.usersManager.first(with: accountUser.uid)
                    ?? nil
                userProfileInfoCell.addButtonActivityIndicatorAnimating = false
                self.collectionView.reloadData()
            }.catch { _ in
                userProfileInfoCell.addButtonActivityIndicatorAnimating = false
            }
        }

        userProfileInfoCell.onKeepButtonTapped = {
            userProfileInfoCell.addButtonActivityIndicatorAnimating = true

            firstly {
                Services.usersService.keep(user: accountUser)
            }.done { _ in
                self.accountUser = Services.cacheViewContext.usersManager.first(with: accountUser.uid)
                    ?? nil
                userProfileInfoCell.addButtonActivityIndicatorAnimating = false
                self.collectionView.reloadData()
            }.catch { _ in
                userProfileInfoCell.addButtonActivityIndicatorAnimating = false
            }
        }
    }
}

// MARK: - UICollectionViewDelegate

extension UserViewController: UICollectionViewDelegateFlowLayout {

    // MARK: - Instance Methods

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)

        switch indexPath.section {
        case Constants.feedsSection:
            if self.feeds.count > indexPath.row {
                let feed = self.feeds[indexPath.row]
                self.performSegue(withIdentifier: Segues.showFeedPreview, sender: feed)
            }

        default:
            return
        }
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? UserInfoCollectionViewCell {
            cell.avatarImageViewTarget.image = Images.avatarLargePlaceholder

            if let imageURL = self.accountUser?.largeAvatarURL {
                cell.showActivityIndicator()

                Services.imageLoader.loadImage(for: imageURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarLargePlaceholder, completionHandler: { image in
                    cell.hideActivityIndicator()
                })
            }
        } else if let cell = cell as? FeedImageCollectionViewCell {
            let imageURL: URL?

            let feed = self.feeds[indexPath.row]

            imageURL = feed.originalImageURL

            if let url = imageURL {
                Services.imageLoader.loadImageProgressive(for: url, in: cell.feedImageViewTarget)
            }
        } else if let cell = cell as? FeedLinkProfileCollectionViewCell {
            let feed = self.feeds[indexPath.row]

            guard let link = feed.link else {
                return
            }

            guard let linkURL = URL(string: link) else {
                return
            }

            Managers.linkPreviewManager.getInformationFrom(link: linkURL, completion: { [weak cell] _, linkDescription, _, error in
                cell?.linkDescription = linkDescription
            })
        }
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? AccountProfileInfoCollectionViewCell {
            Services.imageLoader.cancelLoading(in: cell.avatarImageViewTarget)
        } else if let cell = cell as? FeedImageCollectionViewCell {
            Services.imageLoader.cancelLoading(in: cell.feedImageViewTarget)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section {
        case Constants.profileInfoSection:
            return CGSize.zero

        case Constants.feedsSection:
            return CGSize(width: UIScreen.main.bounds.size.width, height: Constants.feedsSectionHeaderHeight)

        default:
            fatalError()
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.isInitialRefreshingData {
            return self.collectionViewFlowLayout.estimatedItemSize
        } else {
            switch indexPath.section {
            case Constants.profileInfoSection:
                return self.collectionViewFlowLayout.estimatedItemSize

            case Constants.feedsSection:
                    let betweenItems = Constants.countOfSpacingBetweenItems * self.collectionViewFlowLayout.minimumInteritemSpacing
                    let leading = self.collectionViewFlowLayout.sectionInset.left
                    let trailing = self.collectionViewFlowLayout.sectionInset.right

                    let spacing = leading + betweenItems + trailing

                    let feedCellWidth = ((UIScreen.main.bounds.size.width - spacing) / 4).rounded(.down)

                    return CGSize(width: feedCellWidth, height: Constants.feedImageCellHeight)

            default:
                fatalError()
            }
        }
    }
}
