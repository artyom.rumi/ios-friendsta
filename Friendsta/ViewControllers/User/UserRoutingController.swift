//
//  UserRoutingController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 29.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class UserRoutingController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let embedContent = "EmbedContent"
        static let sendCustomProp = "SendCustomProp"
        static let sendProp = "SendProp"
        static let showChat = "ShowChat"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties
    
    fileprivate var userViewController: UserViewController!
    
    // MARK: -
    
    fileprivate(set) var user: User?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onUserCustomPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onUserCustomPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onUserPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onUserPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onUserChatClosed(segue: UIStoryboardSegue) {
        Log.high("onUserChatClosed(\(String(describing: segue.identifier)))", from: self)
    }
    
    fileprivate func onEmbedContent(segue: UIStoryboardSegue, sender: Any?) {
        guard let userViewController = segue.destination as? UserViewController else {
            fatalError("Invalid segue destination (\(String(describing: segue.identifier))")
        }
        
        self.userViewController = userViewController
    }
    
    fileprivate func onSendCustomProp(segue: UIStoryboardSegue, sender: Any?) {
        guard let user = sender as? User else {
            fatalError()
        }
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        dictionaryReceiver?.apply(dictionary: ["user": user])
    }
    
    fileprivate func onSendProp(segue: UIStoryboardSegue, sender: Any?) {
        guard let user = sender as? User else {
            fatalError()
        }
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        dictionaryReceiver?.apply(dictionary: ["user": user])
    }
    
    fileprivate func onShowChat(segue: UIStoryboardSegue, sender: Any?) {
        guard let chat = sender as? Chat else {
            fatalError()
        }
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        dictionaryReceiver?.apply(dictionary: ["chat": chat])
    }
    
    // MARK: -
    
    func apply(user: User) {
        Log.high("apply(user: \(String(describing: user.fullName))", from: self)
        
        self.user = user
        
        if self.isViewLoaded {
            self.userViewController.apply(user: user)
                
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let user = self.user, !self.hasAppliedData {
            self.apply(user: user)
        }

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.embedContent:
            self.onEmbedContent(segue: segue, sender: sender)
            
        case Segues.showChat:
            self.onShowChat(segue: segue, sender: sender)
            
        default:
            break
        }
    }
}

// MARK: - DictionaryReceiver

extension UserRoutingController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let user = dictionary["user"] as? User else {
            return
        }
        
        self.apply(user: user)
    }
}
