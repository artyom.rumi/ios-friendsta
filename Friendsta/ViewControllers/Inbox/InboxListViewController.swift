//
//  InboxListViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 05.02.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import SCSDKCreativeKit
import FriendstaTools
import FriendstaNetwork

class InboxListViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let unauthorize = "Unauthorize"
        static let inviteFriends = "InviteFriends"
        static let showDetailBoost = "ShowDetailBoost"
        static let showReply = "ShowReply"
        static let showInstagramInstruction = "ShowInstagramInstruction"
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let tableCellIdentifier = "TableCell"
        static let tableReplayCellIdentifier = "TableCellReply"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var navigationTitleLabel: UILabel!
    
    @IBOutlet private weak var counterContainerView: RoundView!
    
    @IBOutlet private weak var newPropsLabel: UILabel!
    @IBOutlet private weak var allPropsLabel: UILabel!
    
    @IBOutlet private weak var tableView: UITableView!
        
    @IBOutlet private weak var emptyStateContainerView: UIView!
    @IBOutlet private weak var emptyStateView: EmptyStateView!
    
    // MARK: -
    
    private var propCardsTimer: Timer?
    
    // MARK: -
    
    private(set) var propCards: [PropCard] = []
    
    private(set) var isRefreshingData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromPropCardsEvents()
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onCopyLinkTapped(_ sender: Any) {
        Log.high("onCopyLinkTapped()", from: self)
        
        self.showShareOptions()
    }
    
    @objc private func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.refreshPropCards()
    }
    
    // MARK: -
    
    private func showNewPropsLabel() {
        self.counterContainerView.isHidden = false
    }
    
    private func hideNewPropsLabel() {
        self.counterContainerView.isHidden = true
    }
    
    private func updateNewPropsLabel() {
        let newPropCount = self.propCards.reduce(0, { result, propCard in
            var value: Int
            
            if (propCard.reaction != nil) {
                value = propCard.reaction?.readAtDate == nil ? 1 : 0
            } else {
                value = !propCard.isViewed ? 1 : 0
            }
            
            return result + value
        })
        
        self.newPropsLabel.text = String(format: "%d NEW".localized(), newPropCount)
        
        if newPropCount > 0 {
            self.showNewPropsLabel()
        } else {
            self.hideNewPropsLabel()
        }
    }
    
    private func markAsViewed(propCard: PropCard) {
        Log.high("markAsViewed(\(propCard.uid))", from: self)
        
        self.unsubscribeFromPropCardsEvents()
        
        firstly {
            Services.propCardsService.markAsViewed(propCard: propCard)
        }.ensure {
            self.subscribeToPropCardsEvents()
        }.catch { error in
            self.handle(silentError: error)
        }
    }
    
    private func copyLink() {
        if let link = Services.accountUser?.link {
            UIPasteboard.general.string = link
            self.showCopyLinkSuccess()
        } else {
            firstly {
                Services.accountUserService.refreshAccountUser()
            }.done {[weak self]  accountUser in
                self?.copyLink()
            }.catch { [weak self] error in
                self?.showFailedState(withError: error, retryHandler: { [weak self] in
                    self?.copyLink()
                })
            }
        }
    }
    
    private func showCopyLinkSuccess() {
        let alertController = UIAlertController(title: "Link Copied".localized(), message: "Paste it to your Snapchat Story or Instagram Bio to receive feedback".localized(), preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        alertController.addAction(closeAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showShareOptions() {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let shareToSnapchatAction = UIAlertAction(title: "Share to your Snap".localized(), style: .default) { [unowned self] (action) in
            self.shareToSnapchat()
        }
        
        let shareToInstagramAction = UIAlertAction(title: "Share to your Insta".localized(), style: .default) { [unowned self] (action) in
            self.performSegue(withIdentifier: Segues.showInstagramInstruction, sender: nil)
        }
        
        let copyMyLinkAction = UIAlertAction(title: "Copy my link".localized(), style: .default) { (action) in
            self.copyLink()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action) in
            actionSheetController.dismiss(animated: true, completion: nil)
        }
        
        actionSheetController.addAction(shareToInstagramAction)
        actionSheetController.addAction(shareToSnapchatAction)
        actionSheetController.addAction(copyMyLinkAction)
        actionSheetController.addAction(cancelAction)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    private func shareToSnapchat() {
        guard let stickerImage = UIImage(named: "SnapchatShareSticker"), let link = Services.accountUser?.link else { return }
        
        let sticker = SCSDKSnapSticker(stickerImage: stickerImage)
        sticker.posY = 0.6
        
        let snap = SCSDKNoSnapContent()
        snap.sticker = sticker
        snap.attachmentUrl = link
        
        let api = SCSDKSnapAPI(content: snap)
        api.startSnapping(completionHandler: nil)
    }
    
    // MARK: -
    
    private func showFailedState(withError error: Error, retryHandler: (() -> Void)?) {
        let action: EmptyStateAction?
        
        if let retryHandler = retryHandler {
            action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: retryHandler)
        } else {
            action = nil
        }
        
        switch error as? WebError {
        case .some(.unauthorized):
            break
            
        case .some(.connection), .some(.timeOut):
            self.showEmptyState(title: "No Internet Connection".localized(),
                                message: "Check your wi-fi or mobile data connection.".localized(),
                                action: action)
            
        default:
            self.showEmptyState(title: "Something went wrong".localized(),
                                message: "Please let us know what went wrong or try again later.".localized(),
                                action: action)
        }
    }
    
    private func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()
        
        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action
        
        self.emptyStateContainerView.isHidden = false
        
        self.counterContainerView.isHidden = true
        
        self.allPropsLabel.isHidden = true
    }
    
    private func hideEmptyState() {
        self.emptyStateContainerView.isHidden = true
        
        self.counterContainerView.isHidden = false
        
        self.allPropsLabel.isHidden = false
        
        self.updateNewPropsLabel()
    }
    
    private func showNoDataState() {
        self.counterContainerView.isHidden = true
        
        let action = EmptyStateAction(title: "Share your link".localized(), type: .primaryWhite, onClicked: {
            self.showShareOptions()
        })
        
        self.showEmptyState(image: UIImage(named: "EyesIcon"), title: "Share Your Friendsta Link".localized(), message: "Let your followers leave you fun anonymous messages through your personal Friendsta link.".localized(), action: action)
    }
    
    private func showLoadingState() {
        if self.emptyStateContainerView.isHidden {
            self.showEmptyState(title: "Loading boosts".localized(),
                                message: "We are loading boosts.\nPlease wait a bit.".localized())
        }
        
        self.emptyStateView!.showActivityIndicator()
    }
    
    // MARK: -
    
    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: {
            retryHandler?()
        })
        
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        case .some(.connection), .some(.timeOut):
            if self.propCards.isEmpty {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            }
            
        case .some(.badRequest), .some(.badResponse), .some(.server), .some(.access), .some(.security):
            if self.propCards.isEmpty {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        default:
          break
        }
    }
    
    private func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    private func handle(silentError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            break
        }
    }
    
    // MARK: -
    
    @objc private func inviteFriends() {
        self.performSegue(withIdentifier: Segues.inviteFriends, sender: self)
    }
    
    private func refreshPropCards() {
        Log.high("refreshPropCards()", from: self)
        
        self.isRefreshingData = true
        
        if self.propCards.isEmpty || (!self.emptyStateContainerView.isHidden) {
            self.showLoadingState()
        }
        
        firstly {
            Services.propCardsService.refreshPropCardsLists()
        }.ensure {
            self.isRefreshingData = false
        }.done { propCardsLists in
            self.apply(propCards: Services.cacheViewContext.propCardsManager.fetch())
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshPropCards()
            })
        }
    }
    
    private func resetPropCardsTimer() {
        self.propCardsTimer?.invalidate()
        self.propCardsTimer = nil
    }
    
    private func restartPropCardsTimer() {
        self.resetPropCardsTimer()
        
        guard !self.isRefreshingData else {
            return
        }
        
        self.propCardsTimer = Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false, block: { [weak self] timer in
            self?.apply(propCards: Services.cacheViewContext.propCardsManager.fetch())
        })
    }
    
    private func restartPropCardsTimer(with propCards: [PropCard]) {
        self.restartPropCardsTimer()
    }
    
    private func subscribeToPropCardsEvents() {
        self.unsubscribeFromPropCardsEvents()
        
        let propCardsManager = Services.cacheViewContext.propCardsManager
        
        propCardsManager.objectsChangedEvent.connect(self, handler: { [weak self] propCards in
            self?.restartPropCardsTimer(with: propCards)
        })
        
        propCardsManager.startObserving()
    }
    
    private func unsubscribeFromPropCardsEvents() {
        Services.cacheViewContext.propCardsManager.objectsChangedEvent.disconnect(self)
    }
    
    private func apply(propCards: [PropCard], canShowState: Bool = true) {
        Log.high("apply(propCards: \(propCards.count))", from: self)
        
        guard let accountUserUID = Services.accountAccess?.userUID else {
            return
        }
        
        let sortedPropCards = propCards.sorted(by: { propCard1, propCard2 in
            let date1: Date
            let date2: Date
            
            if let reactionCreatedAt = propCard1.reaction?.createdAtDate, propCard1.reaction?.sender?.uid != accountUserUID {
                date1 = reactionCreatedAt
            } else {
                date1 = propCard1.createdDate ?? Date()
            }
            
            if let reactionCreatedAt = propCard2.reaction?.createdAtDate, propCard2.reaction?.sender?.uid != accountUserUID {
                date2 = reactionCreatedAt
            } else {
                date2 = propCard2.createdDate ?? Date()
            }
            
            return date1 > date2
        })
        
        self.propCards = sortedPropCards
        
        if self.isViewLoaded {
            
            self.allPropsLabel.text = String(format: "%d".localized(), self.propCards.count)
            
            self.tableView.reloadData()
            
            if self.propCards.isEmpty && canShowState {
                self.showNoDataState()
            } else {
                self.hideEmptyState()
            }
        }
    }
    
    // MARK: -
    
    private func loadAvatarImage(for cell: InboxReplyTableViewCell, with prop: PropCard) {
        cell.avatarImage = #imageLiteral(resourceName: "AvatarSmallPlaceholder")
        
        guard let reaction = prop.reaction else {
            fatalError()
        }
        
        guard let sender = reaction.sender else {
            fatalError()
        }
        
        if let avatarURL = sender.avatarURL {
            Services.imageLoader.loadImage(for: avatarURL, in: cell.avatarImageTarget)
        }
    }
    
    private func config(cell: InboxReplyTableViewCell, at indexPath: IndexPath) {
        let propCard = self.propCards[indexPath.row]
        
        if (propCard.reaction?.readAtDate) != nil {
            cell.senderLabelFont = Fonts.medium(ofSize: 17.0)
            cell.isBackgroundHighlighted = false
        } else {
            cell.senderLabelFont = Fonts.bold(ofSize: 17.0)
            cell.isBackgroundHighlighted = true
        }
        
        let pastTime: String?
        
        if let createdDate = propCard.reaction?.createdAtDate {
            pastTime = PastTimeFormatter.shared.string(fromDate: createdDate)
        } else {
            pastTime = nil
        }
        
        cell.timeTitle = pastTime

        guard let senderName = propCard.reaction?.sender?.fullName else {
            cell.senderTitle = "Unknown".localized()
            return
        }
        cell.senderTitle = "\(senderName) replied"
    }
    
    private func config(cell: InboxBoostTableViewCell, at indexPath: IndexPath) {
        let propCard = self.propCards[indexPath.row]
        
        if propCard.isViewed {
            cell.senderLabelFont = Fonts.medium(ofSize: 17.0)
            cell.isBackgroundHighlighted = false
        } else {
            cell.senderLabelFont = Fonts.bold(ofSize: 17.0)
            cell.isBackgroundHighlighted = true
        }
        
        switch propCard.type {
        case .unknown:
            cell.emoji = nil
            cell.senderTitle = nil
            
        case .regular:
            cell.emoji = propCard.prop?.emoji
    
        case .custom:
            cell.emoji = propCard.customProp?.emoji
        }
        
        let pastTime: String?
        
        if let createdDate = propCard.createdDate {
            pastTime = PastTimeFormatter.shared.string(fromDate: createdDate)
        } else {
            pastTime = nil
        }
        
        cell.timeTitle = pastTime
        
        switch propCard.senderGender {
        case .female:
            cell.senderTitle = "FROM A GIRL"
            
        case .male:
            cell.senderTitle = "FROM A BOY"
            
        case .nonBinary:
            cell.senderTitle = "FROM YOUR LINK"
        }
    }
    
    // MARK: -
    
    private func setupFont() {
        self.navigationTitleLabel.font = Fonts.black(ofSize: 17.0)
        self.newPropsLabel.font = Fonts.semiBold(ofSize: 13.0)
        self.allPropsLabel.font = Fonts.medium(ofSize: 44)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.apply(propCards: Services.cacheViewContext.propCardsManager.fetch(),
                   canShowState: true)

        self.setupFont()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isRefreshingData = false

        self.refreshPropCards()
        
        self.subscribeToPropCardsEvents()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onApplicationWillEnterForeground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unsubscribeFromPropCardsEvents()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {            
        case Segues.showDetailBoost, Segues.showReply:
            guard let propCard = sender as? PropCard else {
                fatalError()
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["propCard": propCard])
            }
            
        default:
            return
        }
    }
}

// MARK: - UITableViewDataSource

extension InboxListViewController: UITableViewDataSource {

    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.propCards.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let accountUserUID = Services.accountAccess?.userUID else {
            fatalError()
        }
        
        if let sender = propCards[indexPath.row].reaction?.sender, sender.uid != accountUserUID {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableReplayCellIdentifier,
                                                     for: indexPath) as! InboxReplyTableViewCell
            self.config(cell: cell, at: indexPath)
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableCellIdentifier,
                                                     for: indexPath) as! InboxBoostTableViewCell
            self.config(cell: cell, at: indexPath)
            
            return cell
        }
    }
}

// MARK: - UITableViewDelegate

extension InboxListViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? InboxReplyTableViewCell {
                self.loadAvatarImage(for: cell, with: self.propCards[indexPath.row])
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let propCard = self.propCards[indexPath.row]
        self.markAsViewed(propCard: propCard)

        guard let accountUserUID = Services.accountAccess?.userUID else {
            fatalError()
        }

        if let sender = propCard.reaction?.sender, sender.uid != accountUserUID {
            self.performSegue(withIdentifier: Segues.showReply, sender: propCard)
        } else {
            self.performSegue(withIdentifier: Segues.showDetailBoost, sender: propCard)
        }
    }
}
