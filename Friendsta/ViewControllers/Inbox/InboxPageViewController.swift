//
//  InboxPageViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 28.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

class InboxPageViewController: LoggedPageViewController {
    
    // MARK: - Instance Properties
    
    fileprivate var propViewControllers: [UIViewController] = []
    
    // MARK: -
    
    var onActualPropCardIndexChanged: ((_ index: Int) -> Void)?
    var onVisualPropCardIndexChanged: ((_ index: Int) -> Void)?
    
    fileprivate(set) var propCards: [PropCard] = []
    
    fileprivate(set) var actualPropCardIndex = 0
    fileprivate(set) var visualPropCardIndex = 0
    
    fileprivate(set) var hasAppliedData = false
    
    var actualPropCard: PropCard? {
        let propCardIndex = self.actualPropCardIndex
        
        guard (propCardIndex >= 0) && (propCardIndex < self.propCards.count) else {
            return nil
        }
        
        return self.propCards[propCardIndex]
    }
    
    var visualPropCard: PropCard? {
        let propCardIndex = self.visualPropCardIndex
        
        guard (propCardIndex >= 0) && (propCardIndex < self.propCards.count) else {
            return nil
        }
        
        return self.propCards[propCardIndex]
    }
    
    // MARK: - Instance Methods
    
    fileprivate func updateViewControllers(animated: Bool) {
        let propCardIndex = self.actualPropCardIndex
        
        if (propCardIndex >= 0) && (propCardIndex < self.propViewControllers.count) {
            self.onActualPropCardIndexChanged?(propCardIndex)
            self.onVisualPropCardIndexChanged?(propCardIndex)
            
            self.setViewControllers([self.propViewControllers[propCardIndex]],
                                    direction: .forward,
                                    animated: animated,
                                    completion: nil)
        }
    }
    
    // MARK: -
    
    func apply(propCards: [PropCard], propCardIndex: Int = 0) {
        self.propCards = propCards
        
        self.actualPropCardIndex = propCardIndex
        self.visualPropCardIndex = propCardIndex
        
        self.propViewControllers = []
        
        if self.isViewLoaded {
            let storyboard = UIStoryboard(name: "Inbox", bundle: nil)
            
            for propCard in propCards {
                let propViewController = storyboard.instantiateViewController(withIdentifier: "InboxProp")
                
                if let propViewController = propViewController as? InboxPropViewController {
                    propViewController.apply(propCard: propCard)
                }
                
                self.propViewControllers.append(propViewController)
            }
            
            self.dataSource = nil
            self.dataSource = self
            
            self.updateViewControllers(animated: false)
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    func apply(propCardIndex: Int) {
        let propCardIndex = max(min(propCardIndex, self.propCards.count), 0)
        
        self.actualPropCardIndex = propCardIndex
        self.visualPropCardIndex = propCardIndex
        
        if self.isViewLoaded {
            self.updateViewControllers(animated: true)
        }
    }
    
    func takeSnapshot(size: CGSize) -> UIImage? {
        guard let controller = self.propViewControllers[actualPropCardIndex] as? InboxPropViewController else { return nil }
        
        let snaphshotView = SnapshotView(frame: CGRect(x: 0, y: 0, size: size), image: controller.takeSnapshot())

        return snaphshotView.asImage()
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.delegate = self
    
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        if !self.hasAppliedData {
            self.apply(propCards: self.propCards, propCardIndex: self.actualPropCardIndex)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
}

// MARK: - UIPageViewControllerDataSource

extension InboxPageViewController: UIPageViewControllerDataSource {
    
    // MARK: - Instance Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let propCardIndex = self.propViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousPropCardIndex = propCardIndex - 1
        
        guard (previousPropCardIndex >= 0) && (previousPropCardIndex < self.propViewControllers.count) else {
            return nil
        }
        
        return self.propViewControllers[previousPropCardIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let propCardIndex = self.propViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextPropCardIndex = propCardIndex + 1
        
        guard (nextPropCardIndex >= 0) && (nextPropCardIndex < self.propViewControllers.count) else {
            return nil
        }
        
        return self.propViewControllers[nextPropCardIndex]
    }
}

// MARK: - UIPageViewControllerDelegate

extension InboxPageViewController: UIPageViewControllerDelegate {
    
    // MARK: - Instance Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        if let propViewController = pendingViewControllers.first {
            if let propCardIndex = self.propViewControllers.index(of: propViewController) {
                self.actualPropCardIndex = propCardIndex
        
                self.onActualPropCardIndexChanged?(propCardIndex)
            }
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let propViewController = pageViewController.viewControllers?.first {
            if let propCardIndex = self.propViewControllers.index(of: propViewController) {
                self.visualPropCardIndex = propCardIndex
            
                self.onVisualPropCardIndexChanged?(propCardIndex)
            }
        }
    }
}
