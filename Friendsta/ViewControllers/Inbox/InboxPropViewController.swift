//
//  InboxPropViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 28.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import Photos
import SCSDKCreativeKit
import SVProgressHUD
import FriendstaTools
import FriendstaNetwork

class InboxPropViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Enumeration Cases
        
        static let unauthorize = "Unauthorize"
        static let replyWithPhoto = "ReplyWithPhoto"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var contentView: UIView!
    
    @IBOutlet fileprivate weak var propEmojiLabel: UILabel!
    @IBOutlet fileprivate weak var propMessageLabel: UILabel!
    @IBOutlet fileprivate weak var propDescriptionLabel: UILabel!
    
    @IBOutlet fileprivate weak var shareButton: UIButton!
    
    @IBOutlet fileprivate weak var replyView: UIView!
    @IBOutlet fileprivate weak var swipeUpLabel: UILabel!
    @IBOutlet fileprivate weak var arrowUpIcon: UIImageView!

    @IBOutlet fileprivate var swipeToReplyGestureRecognizer: UISwipeGestureRecognizer!
    
    // MARK: -
    
    fileprivate(set) var actualPropCardIndex = 0
    
    // MARK: -
    
    fileprivate(set) var propCard: PropCard?
    
    fileprivate(set) var hasAppliedData = false
    fileprivate var isSharingToInstagram = false
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onOpenMoreActionsTouchUpInside(_ sender: Any) {
        Log.high("onOpenMoreActionsTouchUpInside()", from: self)
        
        self.showMoreOptions()
    }
    
    // MARK: -
    
    fileprivate func showWaitingState(for reactionButton: Button) {
        UIView.animate(withDuration: 0.25, animations: {
            reactionButton.alpha = 0.5
        }, completion: { finished in
            UIView.animate(withDuration: 0.7, delay: 0.0, options: [.autoreverse, .repeat], animations: {
                reactionButton.alpha = 0.8
            })
        })
    }
    
    fileprivate func handle(error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func showMoreOptions() {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        guard let propCard = self.propCard else {
            return
        }
        
        let blockSenderAction = UIAlertAction(title: "Block Sender".localized(), style: .default) { [unowned self] (action) in
            self.block(propCard: propCard)
        }
        
        let reportContentAction = UIAlertAction(title: "Report Content".localized(), style: .default) { [unowned self] (action) in
            self.report(propCard: propCard)
        }

        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action) in
            actionSheetController.dismiss(animated: true, completion: nil)
        }
        
        actionSheetController.addAction(blockSenderAction)
        actionSheetController.addAction(reportContentAction)
        actionSheetController.addAction(cancelAction)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    fileprivate func report(propCard: PropCard) {
        Log.high("report(propCard: PropCard) ", from: self)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel))
        
        alertController.addAction(UIAlertAction(title: "Report".localized(), style: .destructive, handler: { action in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: {
                loadingViewController.dismiss(animated: true, completion: {
                    self.showMessage(withTitle: "Thank you!".localized(),
                                     message: "Your report will be reviewed by our team very soon.".localized())
                })
            })
        }))
        
        self.present(alertController, animated: true)
    }
    
    fileprivate func block(propCard: PropCard) {
        Log.high("block(propCard:PropCard)", from: self)
        
        let alertController = UIAlertController(title: String(format: "Block Messages.".localized()),
                                                message: "You will never receive messages from this user again.".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel))
        
        alertController.addAction(UIAlertAction(title: "Block".localized(), style: .destructive, handler: { action in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: {
                loadingViewController.dismiss(animated: true, completion: nil)
            })
        }))
        
        self.present(alertController, animated: true)
    }
    
    // MARK: -
    
    func apply(propCard: PropCard) {
        self.propCard = propCard
        
        if self.isViewLoaded {
            self.scrollView.setContentOffset(CGPoint.zero, animated: false)
            
            switch propCard.type {
            case .unknown:
                self.propEmojiLabel.text = nil
                self.propMessageLabel.text = nil
                self.propDescriptionLabel.text = nil
                
            case .regular:
                self.propEmojiLabel.text = propCard.prop?.emoji
                self.propMessageLabel.text = propCard.prop?.message
                
            case .custom:
                self.propEmojiLabel.text = propCard.customProp?.emoji
                self.propMessageLabel.text = propCard.customProp?.message
            }
            
            let pastTime: String?
            
            if let createdDate = propCard.createdDate {
                pastTime = PastTimeFormatter.shared.string(fromDate: createdDate)
            } else {
                pastTime = nil
            }
            
            self.replyView.isHidden = false
            self.shareButton.isHidden = true
            
            switch propCard.senderGender {
            case .nonBinary:
                self.propDescriptionLabel.text = pastTime
                self.replyView.isHidden = true
                self.shareButton.isHidden = false
                
            case .male:
                if let pastTime = pastTime {
                    self.propDescriptionLabel.text = String(format: "%@ FROM A BOY".localized(), pastTime)
                } else {
                    self.propDescriptionLabel.text = "FROM A BOY".localized()
                }
                
            case .female:
                if let pastTime = pastTime {
                    self.propDescriptionLabel.text = String(format: "%@ FROM A GIRL".localized(), pastTime)
                } else {
                    self.propDescriptionLabel.text = "FROM A GIRL".localized()
                }
            }
            
            if self.propCard?.reaction != nil {
                self.arrowUpIcon.isHidden = true
                self.swipeUpLabel.text = "Reply sent"
                self.replyView.removeGestureRecognizer(self.swipeToReplyGestureRecognizer)
            } else {
                self.arrowUpIcon.isHidden = false
                self.swipeUpLabel.text = "Swipe up to reply"
                self.replyView.addGestureRecognizer(self.swipeToReplyGestureRecognizer)
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    func takeSnapshot() -> UIImage {
        return self.contentView.asImage()
    }
    
    fileprivate func showShareOptions() {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let shareToInstagramAction = UIAlertAction(title: "Share to Instagram".localized(), style: .default) { (action) in
            guard let image = self.takeSnapshot(size: CGSize(width: 1080, height: 1920)) else {
                return
            }
            
            guard let url = URL(string: "instagram://app") else {
                return
            }
            
            if UIApplication.shared.canOpenURL(url) {
                firstly {
                    Services.mediaService.requestLibraryAccess()
                }.done { accessState in
                    switch accessState {
                    case .notDetermined, .denied, .restricted:
                        self.showLibraryAccessRequest()
                        
                    case .authorized:
                        self.isSharingToInstagram = true
                        UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
                        self.dismiss(animated: true)
                    }
                }
            } else {
                self.showMessage(withTitle: "Oops.".localized(), message: "Looks like Instagram is not installed".localized(), okHandler: { [unowned self] in
                    self.dismiss(animated: true)
                })
            }
        }
        
        let shareToSnapchatAction = UIAlertAction(title: "Share to Snapchat".localized(), style: .default) { (action) in
            guard let image = self.takeSnapshot(size: CGSize(width: 1080, height: 1920)) else {
                return
            }
            guard let url = URL(string: "snapchat://app") else {
                return
            }
            
            if UIApplication.shared.canOpenURL(url) {
                let snapImage = image
                let photo = SCSDKSnapPhoto(image: snapImage)
                let photoContent = SCSDKPhotoSnapContent(snapPhoto: photo)
                let api = SCSDKSnapAPI(content: photoContent)
                api.startSnapping(completionHandler: { [unowned self] error in
                    self.dismiss(animated: true)
                })
            } else {
                self.showMessage(withTitle: "Oops.".localized(), message: "Looks like Snapchat is not installed".localized(), okHandler: { [unowned self] in
                    self.dismiss(animated: true)
                })
            }
        }
        
        let downloadScreenshotAction = UIAlertAction(title: "Save to Camera Roll".localized(), style: .default) {[unowned self] (action) in
            guard let image = self.takeSnapshot(size: CGSize(width: 1080, height: 1920)) else {
                return
            }
            
            firstly {
                Services.mediaService.requestLibraryAccess()
            }.done { accessState in
                switch accessState {
                case .notDetermined, .denied, .restricted:
                    self.showLibraryAccessRequest()
                    
                case .authorized:
                    self.isSharingToInstagram = false
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.show()
                    }
                    
                    UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
                    self.dismiss(animated: true)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action) in
            actionSheetController.dismiss(animated: true, completion: nil)
        }
        
        actionSheetController.addAction(shareToInstagramAction)
        actionSheetController.addAction(shareToSnapchatAction)
        actionSheetController.addAction(downloadScreenshotAction)
        actionSheetController.addAction(cancelAction)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    @objc fileprivate func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if error == nil {
            if self.isSharingToInstagram {
                let fetchOptions = PHFetchOptions()
                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                
                let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                
                if let lastAsset = fetchResult.firstObject {
                    let localIdentifier = lastAsset.localIdentifier
                    let instagramUrlString = "instagram://library?LocalIdentifier=" + localIdentifier
                    
                    guard let instagramUrl = URL(string: instagramUrlString) else { return }
                    
                    if UIApplication.shared.canOpenURL(instagramUrl) {
                        UIApplication.shared.open(instagramUrl, options: [:], completionHandler: nil)
                    } else {
                        self.showMessage(withTitle: "Oops".localized(), message: "Looks like Instagram is not installed".localized())
                    }
                }
            } else {
                SVProgressHUD.dismiss()
                SVProgressHUD.showSuccess(withStatus: "Saved".localized())
                SVProgressHUD.dismiss(withDelay: 1.0)
            }
        } else {
            self.showMessage(withTitle: "Oops".localized(), message: error?.localizedDescription)
        }
    }
    
    fileprivate func showLibraryAccessRequest() {
        let alertController = UIAlertController(title: "Please allow access".localized(),
                                                message: "The application needs access to your photo library.\n\nPlease go to Settings and set to ON.".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        
        self.present(alertController, animated: true)
    }
    
    func takeSnapshot(size: CGSize) -> UIImage? {
        let snaphshotView = SnapshotView(frame: CGRect(x: 0, y: 0, size: size), image: self.takeSnapshot())
        
        return snaphshotView.asImage()
    }

    fileprivate func showRepliedState() {
        SVProgressHUD.showSuccess(withStatus: "Replied!".localized())
        SVProgressHUD.dismiss(withDelay: 2)
    }
    
    // MARK: -
    
    @IBAction fileprivate func onShareButtonTapped(_ sender: Any) {
        self.showShareOptions()
    }
    
    @IBAction fileprivate func onScrollViewTapped(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction fileprivate func onSwipeUp(_ sender: Any) {
        if self.propCard?.senderGender != .nonBinary {
            self.performSegue(withIdentifier: Segues.replyWithPhoto, sender: self)
        }
    }
    // MARK: -
    
    @IBAction fileprivate func onInboxPhotoMakingFinished(segue: UIStoryboardSegue) {
        Log.high("onInboxPhotoMakingFinished(withSegue: \(String(describing: segue.identifier)))", from: self)

        self.dismiss(animated: true)
        self.showRepliedState()
    }
    
    @IBAction fileprivate func onInboxPhotoMakingCanceled(segue: UIStoryboardSegue) {
        Log.high("onInboxPhotoMakingCanceled(withSegue: \(String(describing: segue.identifier)))", from: self)
    }
    
    // MARK: -
    
    fileprivate func setupShareButton() {
        self.shareButton.layer.cornerRadius = self.shareButton.frame.height/2
        self.shareButton.layer.masksToBounds = true
        self.shareButton.titleLabel?.numberOfLines = 2
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        let titleAttributes: [NSAttributedString.Key: Any] = [.font: Fonts.semiBold(ofSize: 17.0), .foregroundColor: Colors.primary, .paragraphStyle: paragraph]
        
        let attributedTitle = NSAttributedString(string: "Share", attributes: titleAttributes)
        
        let subtitleAttributes: [NSAttributedString.Key: Any] = [.font: Fonts.semiBold(ofSize: 12.0), .foregroundColor: Colors.grayText.withAlphaComponent(0.61), .paragraphStyle: paragraph]
        
        let attributedSubtitle = NSAttributedString(string: "\n Find out who it is? 🤔", attributes: subtitleAttributes)
        
        let fullTitle = NSMutableAttributedString(attributedString: attributedTitle)
        fullTitle.append(attributedSubtitle)
        
        self.shareButton.setAttributedTitle(fullTitle, for: .normal)
    }
    
    fileprivate func setupFont() {
        self.propEmojiLabel.font = Fonts.regular(ofSize: 60.0)
        
        if UIScreen.main.bounds.height < 667 {
            self.propMessageLabel.font = Fonts.medium(ofSize: 20.0)
        } else {
            self.propMessageLabel.font = Fonts.medium(ofSize: 24.0)
        }
        
        self.propDescriptionLabel.font = Fonts.semiBold(ofSize: 13.0)
        
        self.swipeUpLabel.font = Fonts.medium(ofSize: 19.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        self.setupShareButton()

        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let propCard = self.propCard, !self.hasAppliedData {
            self.apply(propCard: propCard)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.replyWithPhoto:
            guard let propCard = self.propCard else {
                fatalError()
            }
            
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            dictionaryReceiver?.apply(dictionary: ["propCard": propCard, "source": PhotoCreationSource.reply])
            
        default:
            break
        }
    }
}

// MARK: - DictionaryReceiver

extension InboxPropViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let propCard = dictionary["propCard"] as? PropCard else {
            return
        }
        
        self.apply(propCard: propCard)
    }
}
