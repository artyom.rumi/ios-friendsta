//
//  InboxViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 28.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import Photos
import SCSDKCreativeKit
import SVProgressHUD
import FriendstaTools
import FriendstaNetwork

class InboxViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let embedContent =  "EmbedContent"
        static let unauthorize = "Unauthorize"
        static let inviteFriends = "InviteFriends"
    }
    
    // MARK: - Instance Properties

    @IBOutlet fileprivate weak var navigationTitleLabel: UILabel!
    
    @IBOutlet fileprivate weak var pageContainerView: UIView!
    @IBOutlet fileprivate weak var counterContainerView: UIView!
    @IBOutlet fileprivate weak var newPropsLabel: UILabel!
    
    @IBOutlet fileprivate weak var pageControl: FlexyPageControl!
    @IBOutlet fileprivate weak var FriendstaLabel: UILabel!
    
    @IBOutlet fileprivate weak var bottomBackgroundImageView: UIImageView!
    @IBOutlet fileprivate weak var shareButton: UIButton!
    
    @IBOutlet fileprivate weak var emptyStateContainerView: UIView!
    @IBOutlet fileprivate weak var emptyStateView: EmptyStateView!
    
    fileprivate weak var pageViewController: InboxPageViewController!

    // MARK: -
    
    fileprivate var propCardsTimer: Timer?
    
    // MARK: -

    fileprivate(set) var propCards: [PropCard] = []
    
    fileprivate(set) var isRefreshingData = false
    fileprivate var isSharingToInstagram = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromPropCardsEvents()
    }
    
    @objc fileprivate func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.refreshPropCards()
    }
    
    fileprivate func onPropCardIndexChanged(_ index: Int) {
        Log.high("onPropCardIndexChanged(\(index))", from: self)
        
        if index < self.propCards.count {
            self.markAsViewed(propCard: self.propCards[index])
        }
        
        self.updateNewPropsLabel()
        self.updatePageControl()
    }

    @IBAction fileprivate func onShareButtonTapped(_ sender: Any) {
        self.showShareOptions()
    }
    
    // MARK: -
    
    fileprivate func showNewPropsLabel() {
        self.counterContainerView.isHidden = false
    }

    fileprivate func hideNewPropsLabel() {
        self.counterContainerView.isHidden = true
    }

    fileprivate func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()
        
        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action
        
        self.emptyStateContainerView.isHidden = false
        
        self.pageContainerView.isHidden = true
        self.counterContainerView.isHidden = true
    }
    
    fileprivate func hideEmptyState() {
        self.emptyStateContainerView.isHidden = true
        
        self.pageContainerView.isHidden = false
        self.counterContainerView.isHidden = false
        self.bottomBackgroundImageView.isHidden = false
        self.shareButton.isHidden = false
        self.FriendstaLabel.isHidden = false
        
        self.updateNewPropsLabel()
        self.updatePageControl()
    }
    
    fileprivate func showNoDataState() {
        self.pageContainerView.isHidden = true
        self.counterContainerView.isHidden = true
        self.bottomBackgroundImageView.isHidden = true
        self.shareButton.isHidden = true
        self.FriendstaLabel.isHidden = true

        let action = EmptyStateAction(title: "Invite Friends".localized(), type: .primaryWhite, onClicked: {
            self.inviteFriends()
        })
        
        self.showEmptyState(image: UIImage(named: "EyesIcon"), title: "No new messages".localized(), message: "Invite people you know to get more boosts!".localized(), action: action)
    }
    
    fileprivate func showLoadingState() {
        if self.emptyStateContainerView.isHidden {
            self.showEmptyState(title: "Loading boosts".localized(),
                                message: "We are loading boosts.\nPlease wait a bit.".localized())
        }
        
        self.emptyStateView!.showActivityIndicator()
    }
    
    fileprivate func showShareOptions() {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let shareToInstagramAction = UIAlertAction(title: "Share to Instagram".localized(), style: .default) { (action) in
            guard let image = self.pageViewController.takeSnapshot(size: CGSize(width: 1080, height: 1080)) else { return }
            guard let url = URL(string: "instagram://app") else { return }
            if UIApplication.shared.canOpenURL(url) {
                firstly {
                    Services.mediaService.requestLibraryAccess()
                }.done { accessState in
                    switch accessState {
                    case .notDetermined, .denied, .restricted:
                        self.showLibraryAccessRequest()
                        
                    case .authorized:
                        self.isSharingToInstagram = true
                        UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
                    }
                }
            } else {
                self.showMessage(withTitle: "Oops.".localized(), message: "Looks like Instagram is not installed".localized())
            }
        }
        
        let shareToSnapchatAction = UIAlertAction(title: "Share to Snapchat".localized(), style: .default) { (action) in
            guard let image = self.pageViewController.takeSnapshot(size: CGSize(width: 1080, height: 1080)) else { return }
            guard let url = URL(string: "snapchat://app") else { return }
            
            if UIApplication.shared.canOpenURL(url) {
                let snapImage = image
                let photo = SCSDKSnapPhoto(image: snapImage)
                let photoContent = SCSDKPhotoSnapContent(snapPhoto: photo)
                let api = SCSDKSnapAPI(content: photoContent)
                api.startSnapping(completionHandler: nil)
            } else {
                 self.showMessage(withTitle: "Oops.".localized(), message: "Looks like Snapchat is not installed".localized())
            }
        }
        
        let downloadScreenshotAction = UIAlertAction(title: "Download Screenshot".localized(), style: .default) {[unowned self] (action) in
            guard let image = self.pageViewController.takeSnapshot(size: CGSize(width: 1080, height: 1920)) else { return }
            
            firstly {
                Services.mediaService.requestLibraryAccess()
            }.done { accessState in
                switch accessState {
                case .notDetermined, .denied, .restricted:
                    self.showLibraryAccessRequest()
                    
                case .authorized:
                    self.isSharingToInstagram = false
                    
                    DispatchQueue.main.async {
                        SVProgressHUD.show()
                    }
                    
                    UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(image:didFinishSavingWithError:contextInfo:)), nil)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action) in
            actionSheetController.dismiss(animated: true, completion: nil)
        }
        
        actionSheetController.addAction(shareToInstagramAction)
        actionSheetController.addAction(shareToSnapchatAction)
        actionSheetController.addAction(downloadScreenshotAction)
        actionSheetController.addAction(cancelAction)
        
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    fileprivate func showLibraryAccessRequest() {
        let alertController = UIAlertController(title: "Please allow access".localized(),
                                                message: "The application needs access to your photo library.\n\nPlease go to Settings and set to ON.".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        
        self.present(alertController, animated: true)
    }
    
    // MARK: -
    
    fileprivate func updateNewPropsLabel() {
        let newPropCount = self.propCards.reduce(0, { result, propCard in
            return result + (propCard.isViewed ? 0 : 1)
        })
        
        self.newPropsLabel.text = String(format: "%d NEW".localized(), newPropCount)
        
        if newPropCount > 0 {
            self.showNewPropsLabel()
        } else {
            self.hideNewPropsLabel()
        }
    }

    fileprivate func updatePageControl() {
        let propCardIndex = self.pageViewController.visualPropCardIndex
        
        self.pageControl.currentPage = propCardIndex
    }
    
    // MARK: -
    
    fileprivate func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: {
            retryHandler?()
        })
        
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        case .some(.connection), .some(.timeOut):
            if self.propCards.isEmpty {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            }
            
        default:
            if self.propCards.isEmpty {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        }
    }
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func handle(silentError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            break
        }
    }
    
    @objc fileprivate func image(image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if error == nil {
            if self.isSharingToInstagram {
                let fetchOptions = PHFetchOptions()
                fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                
                let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                
                if let lastAsset = fetchResult.firstObject {
                    let localIdentifier = lastAsset.localIdentifier
                    let instagramUrlString = "instagram://library?LocalIdentifier=" + localIdentifier
                    
                    guard let instagramUrl = URL(string: instagramUrlString) else { return }
                    
                    if UIApplication.shared.canOpenURL(instagramUrl) {
                        UIApplication.shared.open(instagramUrl, options: [:], completionHandler: nil)
                    } else {
                        self.showMessage(withTitle: "Oops".localized(), message: "Looks like Instagram is not installed".localized())
                    }
                }
            } else {
                SVProgressHUD.dismiss()
                SVProgressHUD.showSuccess(withStatus: "Saved".localized())
                SVProgressHUD.dismiss(withDelay: 1.0)
            }
        } else {
            self.showMessage(withTitle: "Oops".localized(), message: error?.localizedDescription)
        }
    }
    
    fileprivate func markAsViewed(propCard: PropCard) {
        Log.high("markAsViewed(\(propCard.uid))", from: self)
        
        self.unsubscribeFromPropCardsEvents()
        
        firstly {
            Services.propCardsService.markAsViewed(propCard: propCard)
        }.ensure {
            self.subscribeToPropCardsEvents()
        }.catch { error in
            self.handle(silentError: error)
        }
    }
    
    @objc fileprivate func inviteFriends() {
        self.performSegue(withIdentifier: Segues.inviteFriends, sender: self)
    }
    
    fileprivate func refreshPropCards() {
        Log.high("refreshPropCards()", from: self)
        
        self.isRefreshingData = true
        
        if self.propCards.isEmpty || (!self.emptyStateContainerView.isHidden) {
            self.showLoadingState()
        }
        
        firstly {
            Services.propCardsService.refreshPropCards()
        }.ensure {
            self.isRefreshingData = false
        }.done { propCards in
            self.apply(propCards: propCards)
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshPropCards()
            })
        }
    }
    
    fileprivate func resetPropCardsTimer() {
        self.propCardsTimer?.invalidate()
        self.propCardsTimer = nil
    }
        
    fileprivate func restartPropCardsTimer() {
        self.resetPropCardsTimer()
        
        guard !self.isRefreshingData else {
            return
        }
        
        self.propCardsTimer = Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false, block: { [weak self] timer in
            self?.apply(propCards: Services.cacheViewContext.propCardsManager.fetch())
        })
    }
    
    fileprivate func restartPropCardsTimer(with propCards: [PropCard]) {
        self.restartPropCardsTimer()
    }
    
    fileprivate func subscribeToPropCardsEvents() {
        self.unsubscribeFromPropCardsEvents()
        
        let propCardsManager = Services.cacheViewContext.propCardsManager
        
        propCardsManager.objectsChangedEvent.connect(self, handler: { [weak self] propCards in
            self?.restartPropCardsTimer(with: propCards)
        })
        
        propCardsManager.startObserving()
    }
    
    fileprivate func unsubscribeFromPropCardsEvents() {
        Services.cacheViewContext.propCardsManager.objectsChangedEvent.disconnect(self)
    }
    
    fileprivate func apply(propCards: [PropCard], canShowState: Bool = true) {
        Log.high("apply(propCards: \(propCards.count))", from: self)
        
        self.propCards = propCards

        if self.isViewLoaded {
            let propCardIndex: Int
            
            if let propCardUID = self.pageViewController.actualPropCard?.uid {
                propCardIndex = propCards.index(where: { propCard in
                    return propCard.uid == propCardUID
                }) ?? 0
            } else {
                propCardIndex = max((propCards.index(where: { propCard in
                    return !propCard.isViewed
                }) ?? propCards.count) - 1, 0)
            }
            
            self.pageViewController.apply(propCards: propCards, propCardIndex: propCardIndex)
            
            self.pageControl.numberOfPages = self.propCards.count
            
            if self.propCards.isEmpty && canShowState {
                self.showNoDataState()
            } else {
                self.hideEmptyState()
            }
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.newPropsLabel.font = Fonts.semiBold(ofSize: 13.0)
        self.navigationTitleLabel.font = Fonts.semiBold(ofSize: 17.0)
        self.FriendstaLabel.font = Fonts.semiBold(ofSize: 12.0)
    }
    
    fileprivate func setupShareButton() {
        self.shareButton.layer.cornerRadius = self.shareButton.frame.height/2
        self.shareButton.layer.masksToBounds = true
        self.shareButton.titleLabel?.numberOfLines = 2
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        
        let titleAttributes: [NSAttributedString.Key: Any] = [.font: Fonts.semiBold(ofSize: 17.0), .foregroundColor: Colors.whiteText, .paragraphStyle: paragraph]
        
        let attributedTitle = NSAttributedString(string: "Share", attributes: titleAttributes)
        
        let subtitleAttributes: [NSAttributedString.Key: Any] = [.font: Fonts.semiBold(ofSize: 12.0), .foregroundColor: Colors.whiteText.withAlphaComponent(0.61), .paragraphStyle: paragraph]
        
        let attributedSubtitle = NSAttributedString(string: "\n Find out who it is? 🤔", attributes: subtitleAttributes)
        
        let fullTitle = NSMutableAttributedString(attributedString: attributedTitle)
        fullTitle.append(attributedSubtitle)
            
        self.shareButton.setAttributedTitle(fullTitle, for: .normal)
    }

    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        
        self.setupShareButton()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isRefreshingData = false
        
        self.apply(propCards: Services.cacheViewContext.propCardsManager.fetch(),
                   canShowState: true)
        
        self.refreshPropCards()
        
        self.subscribeToPropCardsEvents()

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onApplicationWillEnterForeground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unsubscribeFromPropCardsEvents()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.embedContent:
            guard let pageViewController = segue.destination as? InboxPageViewController else {
                fatalError()
            }
            
            self.pageViewController = pageViewController
            
            self.pageViewController.onVisualPropCardIndexChanged = { [unowned self] index in
                self.onPropCardIndexChanged(index)
            }
            
        default:
            break
        }
    }
}
