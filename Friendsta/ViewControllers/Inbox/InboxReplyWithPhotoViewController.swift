//
//  InboxReplyWithPhotoViewController.swift
//  Friendsta
//
//  Created by Nikita Asabin on 3/29/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class InboxReplyWithPhotoViewController: MakePhotoViewController {

    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let unauthorize = "Unauthorize"
        static let finishPhotoSending = "FinishPhotoSending"
        static let embedPhotoTaking = "EmbedPhotoTaking"
    }
    
    // MARK: - Instance Properties
    
    fileprivate(set) var propCard: PropCard?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - Instance Methods
    
    fileprivate func handle(silentError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            let alertController = UIAlertController(title: nil, message: "Something went wrong or you already replied".localized(), preferredStyle: .alert)
            
            alertController.view.tintColor = Colors.primary
            alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self.present(alertController, animated: true)
        }
    }
    
    // MARK: -
    
    func apply(propCard: PropCard) {
        Log.high("apply(propCard: \(String(describing: propCard.uid)))", from: self)
        
        self.propCard = propCard
        
        if self.isViewLoaded {
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - MakePhotoViewController
    
    override func performAction(with photoImage: UIImage) {
        Log.high("performAction(withPhotoImage: \(photoImage.size))", from: self)
        
        guard let propCard = self.propCard else {
            return
        }
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            DispatchQueue.global(qos: .userInitiated).async(execute: {
                let photoData: Data?
                
                if photoImage.size.width > Limits.photoImageMaxWidth {
                    if let photoImage = photoImage.scaled(to: Limits.photoImageMaxWidth) {
                        photoData = photoImage.jpegData(compressionQuality: Limits.photoImageQuality)
                    } else {
                        photoData = photoImage.jpegData(compressionQuality: Limits.photoImageQuality)
                    }
                } else {
                    photoData = photoImage.jpegData(compressionQuality: Limits.photoImageQuality)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
                    loadingViewController.dismiss(animated: true, completion: {
                        guard let photoData = photoData else {
                            return
                        }

                        firstly {
                            Services.propReactionsService.sendPropReaction(for: propCard, with: photoData)
                        }.done { chatPhotoMessage in
                            self.performSegue(withIdentifier: Segues.finishPhotoSending, sender: self)
                        }.catch { error in
                            self.handle(silentError: error)
                        }
                    })
                })
            })
        })
    }
    
    override func apply(dictionary: [String: Any]) {
        super.apply(dictionary: dictionary)
        
        guard let propCard = dictionary["propCard"] as? PropCard else {
            return
        }
        
        self.apply(propCard: propCard)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let propCard = self.propCard, !self.hasAppliedData {
            self.apply(propCard: propCard)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.embedPhotoTaking:
            guard let propCard = self.propCard else {
                fatalError()
            }
            
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            dictionaryReceiver?.apply(dictionary: ["propCard": propCard,
                                                   "source": PhotoCreationSource.reply])
            
        default:
            return
        }
    }
}
