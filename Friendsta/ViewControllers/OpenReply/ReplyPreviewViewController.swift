//
//  ReplyPreviewViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 21.05.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class ReplyPreviewViewController: UIViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    enum Segues {
        
        // MARK: - Type Properties
        
        static let openReply = "OpenReply"
        static let openPropsList = "OpenPropsList"
    }
    
    // MARK: -
    
    enum Constants {
        
        // MARK: - Type Properties
        
        static let textReplyPlaceholderImage = UIImage(named: "TextReplyPlaceholder")
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var emojiLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var instructionLabel: UILabel!
    
    @IBOutlet private weak var swipeToOpenReplyView: UIView!
    @IBOutlet private weak var replyImageView: UIImageView!
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet private weak var reactWithAPokeButton: UIButton!
    
    // MARK: -
    
    private(set) var propCard: PropCard?
    
    private(set) var hasAppliedData = false
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onUserCustomPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onUserCustomPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onUserPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onUserPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
    }

    @IBAction private func onScrollViewTapped(_ sender: Any) {
        Log.high("onScrollViewTapped()", from: self)
        
        self.dismiss(animated: true)
    }

    @IBAction private func onSwipeGestureRecognized(_ sender: Any) {
        Log.high("onSwipeGestureRecognized()", from: self)
        
        self.performSegue(withIdentifier: Segues.openReply, sender: self.propCard)
    }
    
    @IBAction private func onReactWithAPokeButtonTouchUpInside(_ sender: Any) {
        Log.high("onReactWithAPokeButtonTouchUpInside()", from: self)
        
        self.performSegue(withIdentifier: Segues.openPropsList, sender: self.propCard?.reaction?.sender)
    }

    @IBAction private func onMoreButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onMoreButtonTouchUpInside()", from: self)

        self.showMoreOptions()
    }

    // MARK: -

    private func showMoreOptions() {
        let actionSheetController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        guard let propCard = self.propCard else {
            return
        }

        let blockSenderAction = UIAlertAction(title: "Block Sender".localized(), style: .default, handler: { [unowned self] action in
            self.block(propCard: propCard)
        })

        let reportContentAction = UIAlertAction(title: "Report Content".localized(), style: .default, handler: { [unowned self] (action) in
            self.report(propCard: propCard)
        })

        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)

        actionSheetController.addAction(blockSenderAction)
        actionSheetController.addAction(reportContentAction)
        actionSheetController.addAction(cancelAction)

        self.present(actionSheetController, animated: true)
    }

    private func report(propCard: PropCard) {
        Log.high("report(propCard: \(propCard.uid))", from: self)

        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        alertController.view.tintColor = Colors.primary

        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)

        let reportAction = UIAlertAction(title: "Report".localized(), style: .destructive, handler: { action in
            let loadingViewController = LoadingViewController()

            self.present(loadingViewController, animated: true, completion: {
                // TODO: - Implement report with PropCard on back-end side

                loadingViewController.dismiss(animated: true, completion: {
                    self.showMessage(withTitle: "Thank you!".localized(), message: "Your report will be reviewed by our team very soon.".localized())
                })
            })
        })

        alertController.addAction(cancelAction)
        alertController.addAction(reportAction)

        self.present(alertController, animated: true)
    }

    private func block(propCard: PropCard) {
        Log.high("block(propCard: \(propCard.uid))", from: self)

        let alertController = UIAlertController(title: String(format: "Block Messages.".localized()),
                                                message: "You will never receive messages from this user again.".localized(),
                                                preferredStyle: .alert)

        alertController.view.tintColor = Colors.primary

        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)

        let blockAction = UIAlertAction(title: "Block".localized(), style: .destructive, handler: { action in
            let loadingViewController = LoadingViewController()

            self.present(loadingViewController, animated: true, completion: {
                // TODO: - Implement block with PropCard on back-end side

                loadingViewController.dismiss(animated: true, completion: nil)
            })
        })

        alertController.addAction(cancelAction)
        alertController.addAction(blockAction)

        self.present(alertController, animated: true)
    }
    
    // MARK: -
    
    func apply(propCard: PropCard) {
        self.propCard = propCard
        
        if self.isViewLoaded {
            switch propCard.type {
            case .unknown:
                self.emojiLabel.text = nil
                self.messageLabel.text = nil
                self.dateLabel.text = nil
                
            case .regular:
                self.emojiLabel.text = propCard.prop?.emoji
                self.messageLabel.text = propCard.prop?.message
                
            case .custom:
                self.emojiLabel.text = propCard.customProp?.emoji
                self.messageLabel.text = propCard.customProp?.message
            }
            
            guard let reaction = self.propCard?.reaction else {
                return
            }
            
            guard let senderName = reaction.sender?.firstName else {
                return
            }
            
            guard let senderFullName = reaction.sender?.fullName else {
                return
            }
            
            let pastTime: String?
            
            if let createdDate = propCard.createdDate {
                pastTime = PastTimeFormatter.shared.string(fromDate: createdDate)
            } else {
                pastTime = nil
            }
            
            if let pastTime = pastTime {
                self.dateLabel.text = String(format: "%@ TO \(senderFullName)".localized(), pastTime)
            } else {
                self.dateLabel.text = "TO \(senderFullName)".localized()
            }
            
            self.instructionLabel.text = "Check \(senderName)'s Reply"
            
            self.replyImageView.layer.shadowColor = UIColor.black.cgColor
            self.replyImageView.layer.shadowOpacity = 0.5
            self.replyImageView.layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
            
            if let reactionPhotoURL = reaction.largePhotoURL, let url = URL(string: reactionPhotoURL) {
                self.replyImageView.image = Constants.textReplyPlaceholderImage
                
                self.activityIndicator.startAnimating()

                Services.imageLoader.loadImage(for: url, in: self.replyImageView, placeholder: Constants.textReplyPlaceholderImage, completionHandler: { image in
                    self.activityIndicator.stopAnimating()
                })
            } else {
                self.replyImageView.image = Constants.textReplyPlaceholderImage
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    private func setupOpenReplyAvailability() {
        self.replyImageView.isHidden = self.propCard?.reaction?.readAtDate != nil
        
        self.swipeToOpenReplyView.isHidden = self.propCard?.reaction?.readAtDate != nil
        self.swipeToOpenReplyView.isUserInteractionEnabled = self.propCard?.reaction?.readAtDate == nil
        
        self.reactWithAPokeButton.isHidden = self.propCard?.reaction?.readAtDate == nil
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupOpenReplyAvailability()

        if let propCard = self.propCard, !self.hasAppliedData {
            self.apply(propCard: propCard)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.openReply:
            guard let propCard = sender as? PropCard else {
                fatalError()
            }
            
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["propCard": propCard])
            }
            
        case Segues.openPropsList:
            guard let user = sender as? User else {
                fatalError()
            }
            
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["user": user])
            }
            
        default:
            return
        }
    }
}

// MARK: - DictionaryReceiver

extension ReplyPreviewViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let propCard = dictionary["propCard"] as? PropCard else {
            return
        }
        
        self.apply(propCard: propCard)
    }
}
