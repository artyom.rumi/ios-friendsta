//
//  ReplyViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 22.05.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import SVProgressHUD
import FriendstaTools
import FriendstaNetwork

class ReplyViewController: UIViewController {
    
    // MARK: - Nested Types
    
    enum Constants {
        
        // MARK: - Type Properties
        
        static let inboxBackgroundImage = UIImage(named: "InboxBackground")
    }
    
    // MARK: -
    
    enum Segues {
        
        // MARK: - Type Properties
        
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var messageLabel: UILabel!
    
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet private weak var replyImageView: UIImageView!
    
    @IBOutlet private weak var progressSlider: ProgressSlider!
    
    // MARK: -
    
    private var propCard: PropCard?
    
    private(set) var hasAppliedData = false
    
    // MARK: - Instance Methods
    
    @IBAction private func onTapGestureRecognized(_ sender: Any) {
        Log.high("onTapGestureRecognized()", from: self)
                
        self.markReplyAsViewedAndDismiss()
    }
    
    // MARK: -
    
    private func apply(propCard: PropCard) {
        Log.high("apply(propCard: \(propCard.uid))", from: self)
        
        self.propCard = propCard
        
        if self.isViewLoaded {
            guard let reaction = self.propCard?.reaction else {
                return
            }
            
            if let message = reaction.message {
                self.messageLabel.isHidden = false
                self.messageLabel.text = message
            } else {
                self.messageLabel.isHidden = true
            }
            
            if let reactionPhotoURL = reaction.largePhotoURL, let url = URL(string: reactionPhotoURL) {
                self.replyImageView.image = Constants.inboxBackgroundImage
                
                self.activityIndicator.startAnimating()

                Services.imageLoader.loadImage(for: url, in: self.replyImageView, placeholder: Constants.inboxBackgroundImage, completionHandler: { image in
                    self.activityIndicator.stopAnimating()
                })
            } else {
                self.replyImageView.image = Constants.inboxBackgroundImage
            }
            
            self.progressSlider.progressSliderFinished = {
                self.markReplyAsViewedAndDismiss()
            }
                        
            self.hasAppliedData = true
        }
    }
    
    private func markReplyAsViewedAndDismiss() {
        guard let propCard = self.propCard else {
            return
        }
        
        SVProgressHUD.show()
        
        firstly {
            Services.propReactionsService.markReplyAsViewed(propCard: propCard)
        }.done { propReaction in
            SVProgressHUD.dismiss()
            
            self.dismiss(animated: false, completion: nil)
        }.catch { error in
            SVProgressHUD.dismiss()
            
            self.handle(silentError: error)
        }
    }
    
    // MARK: -
    
    fileprivate func handle(silentError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            break
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let propCard = self.propCard, !self.hasAppliedData {
            self.apply(propCard: propCard)
        }
    }
}

// MARK: - DictionaryReceiver

extension ReplyViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let propCard = dictionary["propCard"] as? PropCard else {
            return
        }
        
        self.apply(propCard: propCard)
    }
}
