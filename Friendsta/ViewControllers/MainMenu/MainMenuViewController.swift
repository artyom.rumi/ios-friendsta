//
//  MainMenuViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 18.11.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import UIKit
import FriendstaTools
import PromiseKit

class MainMenuViewController: LoggedViewController {
    
    // MARK: - Type Properties
    
    static let storyboardName = "MainMenu"
    
    // MARK: - Nested Types
    
    private enum Images {
        
        // MARK: - Type Properties
        
        static let writePostOptionImage = UIImage(named: "WritePostOptionIcon")
        static let takePhotoOptionImage = UIImage(named: "TakePhotoOptionIcon")
        static let openPhotoLibraryOptionImage = UIImage(named: "OpenPhotoLibraryOptionIcon")
        static let attachLinkOptionImage = UIImage(named: "AttachLinkOptionIcon")
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let menuOptionHeight = 60
        static let menuOptionsDistance = 100.0
    }
    
    // MARK: -
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let showTextFeed = "ShowTextFeed"
        static let showMakePhoto = "ShowMakePhoto"
        static let showPasteLink = "ShowPasteLink"
        
        static let closeMainMenu = "CloseMainMenu"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var mainView: RoundControl!
    @IBOutlet private weak var mainMenuPlusImageView: UIImageView!
    
    @IBOutlet private var tapGestureRecognizer: UITapGestureRecognizer!
    
    // MARK: -
    
    private var leftBottomOptionView = MenuOptionControl()
    private var leftTopOptionView = MenuOptionControl()
    private var rightTopOptionView = MenuOptionControl()
    private var rightBottomOptionView = MenuOptionControl()
    
    // MARK: -
    
    private let textFeedPanelTransition = PanelTransition(topPadding: 62, cornerRadius: 12, options: [.closeButton])
        
    // MARK: - Instance Methods
    
    @IBAction private func onViewTapGestureRecognized(_ sender: UITapGestureRecognizer) {
        Log.high("onViewTapGestureRecognized()", from: self)
                
        self.closeMenuOptions()
    }
    
    @IBAction private func onMainViewTouchUpInside(_ sender: Any) {
        Log.high("onMainViewTouchUpInside()", from: self)
                
        self.closeMenuOptions()
    }
    
    // MARK: -
    
    @objc private func onWritePostOptionTouchUpInside(_ sender: Any) {
        Log.high("onWriteToPostOptionTouchUpInside()", from: self)
        
        self.performSegue(withIdentifier: Segues.showTextFeed, sender: self)
    }
    
    @objc private func onTakePhotoOptionTouchUpInside(_ sender: Any) {
        Log.high("onTakePhotoOptionTouchUpInside()", from: self)
        
        self.performSegue(withIdentifier: Segues.showMakePhoto, sender: self)
    }
    
    @objc private func onOpenPhotoLibraryOptionTouchUpInside(_ sender: Any) {
        Log.high("onOpenPhotoLibraryOptionTouchUpInside()", from: self)
        
        self.openPhotoLibrary()
    }
    
    @objc private func onAttachLinkOptionTouchUpInside(_ sender: Any) {
        Log.high("onAttachLinkOptionTouchUpInside()", from: self)
        
        self.performSegue(withIdentifier: Segues.showPasteLink, sender: self)
    }
    
    // MARK: -
    
    private func requestLibraryAccess() {
        Log.high("requestLibraryAccess()", from: self)
        
        firstly {
            Services.mediaService.requestLibraryAccess()
        }.done { accessState in
            switch accessState {
            case .notDetermined, .denied, .restricted:
                break
                
            case .authorized:
                self.openPhotoLibrary()
            }
        }
    }
    
    private func showLibraryAccessRequest() {
        let alertController = UIAlertController(title: #""Nicely" Would Like to Access Your Photos"#.localized(),
                                                message: "This lets you share photos from your library and save photos...".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.blackText
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        
        self.present(alertController, animated: true)
    }
    
    private func openPhotoLibrary() {
        switch Services.mediaService.libraryAccessState {
        case .notDetermined:
            self.requestLibraryAccess()
            
        case .denied, .restricted:
            self.showLibraryAccessRequest()
            
        case .authorized:
            let imagePickerController = ImagePickerController()
            
            imagePickerController.onImagePicked = { [unowned imagePickerController] photoImage in
                imagePickerController.dismiss(animated: true, completion: { [unowned self] in
                    self.performSegue(withIdentifier: Segues.showMakePhoto, sender: photoImage)
                })
            }
            
            imagePickerController.apply(mediaSource: .photoLibrary, allowsEditing: false)
            
            self.present(imagePickerController, animated: true)
        }
    }
    
    // MARK: -
    
    private func moveAllOptionsToInitialPoint() {
        self.leftBottomOptionView.center = self.mainView.center
        self.leftTopOptionView.center = self.mainView.center
        self.rightTopOptionView.center = self.mainView.center
        self.rightBottomOptionView.center = self.mainView.center
    }
    
    private func closeMenuOptions() {
        UIView.animate(withDuration: 0.25, animations: {
            self.moveAllOptionsToInitialPoint()
            
            self.mainView.transform = .identity
            self.mainView.backgroundColor = UIColor.white
            
            self.mainMenuPlusImageView.tintColor = UIColor.black
            
            self.view.backgroundColor = UIColor.clear
        }, completion: { completed in
            if completed {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
                    self.performSegue(withIdentifier: Segues.closeMainMenu, sender: self)
                })
            }
        })
    }
    
    private func openMenuOptions() {
        
        ///// ********* Lance's Code ********* /////
        NotificationCenter.default.post(name: Notification.Name("removeRectangeCloudViewCalloutsInFeedViewVC"), object: nil)
        ///// ********* Lance's Code ********* /////
        
        let bottomOptionViewAngleSin = sin(Double.pi / 8)
        let topOptionViewAngleSin = sin(3 * Double.pi / 8)
        
        let topOptionViewYPosition = topOptionViewAngleSin * Constants.menuOptionsDistance
        let topOptionViewXPosition = sqrt(pow(Constants.menuOptionsDistance, 2) - pow(topOptionViewYPosition, 2))
                
        let bottomOptionViewYPosition = bottomOptionViewAngleSin * Constants.menuOptionsDistance
        let bottomOptionViewXPosition = sqrt(pow(Constants.menuOptionsDistance, 2) - pow(bottomOptionViewYPosition, 2))
        
        UIView.animate(withDuration: 0.25) { [unowned self] in
            self.rightTopOptionView.center = CGPoint(x: Double(self.rightTopOptionView.center.x) + topOptionViewXPosition,
                                                     y: Double(self.rightTopOptionView.frame.bottom) - topOptionViewYPosition)

            self.leftTopOptionView.center = CGPoint(x: Double(self.leftTopOptionView.center.x) - topOptionViewXPosition,
                                                    y: Double(self.leftTopOptionView.frame.bottom) - topOptionViewYPosition)
            
            self.rightBottomOptionView.center = CGPoint(x: Double(self.rightBottomOptionView.center.x) + bottomOptionViewXPosition,
                                                        y: Double(self.rightBottomOptionView.frame.bottom) - bottomOptionViewYPosition)

            self.leftBottomOptionView.center = CGPoint(x: Double(self.leftBottomOptionView.center.x) - bottomOptionViewXPosition,
                                                       y: Double(self.leftBottomOptionView.frame.bottom) - bottomOptionViewYPosition)
            
            self.mainView.transform = CGAffineTransform(rotationAngle: .pi / 4)
            self.mainView.backgroundColor = UIColor(redByte: 153, greenByte: 153, blueByte: 153)
            
            self.mainMenuPlusImageView.tintColor = UIColor.white
        }
    }
    
    private func configureMenuOptionsView() {
        self.leftBottomOptionView.frame = CGRect(x: 0, y: 0, width: Constants.menuOptionHeight, height: Constants.menuOptionHeight)
        self.leftTopOptionView.frame = CGRect(x: 0, y: 0, width: Constants.menuOptionHeight, height: Constants.menuOptionHeight)
        self.rightTopOptionView.frame = CGRect(x: 0, y: 0, width: Constants.menuOptionHeight, height: Constants.menuOptionHeight)
        self.rightBottomOptionView.frame = CGRect(x: 0, y: 0, width: Constants.menuOptionHeight, height: Constants.menuOptionHeight)
        
        self.leftBottomOptionView.image = Images.writePostOptionImage
        self.leftTopOptionView.image = Images.takePhotoOptionImage
        self.rightTopOptionView.image = Images.openPhotoLibraryOptionImage
        self.rightBottomOptionView.image = Images.attachLinkOptionImage
        
        self.leftBottomOptionView.backgroundColor = UIColor.white
        self.leftTopOptionView.backgroundColor = UIColor.white
        self.rightTopOptionView.backgroundColor = UIColor.white
        self.rightBottomOptionView.backgroundColor = UIColor.white
        
        self.moveAllOptionsToInitialPoint()
        
        self.view.addSubview(self.leftBottomOptionView)
        self.view.addSubview(self.leftTopOptionView)
        self.view.addSubview(self.rightTopOptionView)
        self.view.addSubview(self.rightBottomOptionView)
        
        self.leftBottomOptionView.addTarget(self, action: #selector(self.onWritePostOptionTouchUpInside(_ :)), for: .touchUpInside)
        self.leftTopOptionView.addTarget(self, action: #selector(self.onTakePhotoOptionTouchUpInside(_ :)), for: .touchUpInside)
        self.rightTopOptionView.addTarget(self, action: #selector(self.onOpenPhotoLibraryOptionTouchUpInside(_ :)), for: .touchUpInside)
        self.rightBottomOptionView.addTarget(self, action: #selector(self.onAttachLinkOptionTouchUpInside(_ :)), for: .touchUpInside)
        
        self.view.bringSubviewToFront(self.mainView)
    }
    
    private func configureTapGesture() {
        self.tapGestureRecognizer.cancelsTouchesInView = false
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTapGesture()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.configureMenuOptionsView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.openMenuOptions()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let destinationViewController = segue.destination
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {
        case Segues.showTextFeed:
            destinationViewController.transitioningDelegate = self.textFeedPanelTransition
            destinationViewController.modalPresentationStyle = .custom
            
        case Segues.showMakePhoto:
            if let photoImage = sender as? UIImage {
                dictionaryReceiver?.apply(dictionary: ["photoImage": photoImage,
                                                       "source": PhotoCreationSource.menu])
            } else {
                dictionaryReceiver?.apply(dictionary: ["source": PhotoCreationSource.menu])
            }
            
        default:
            return
        }
    }
}
