//
//  CommentsTableViewController.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 16/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork
import SVProgressHUD

class CommentsTableViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    typealias CommentDismissData = (readCommentUIDs: [Int64], feed: Feed)

    // MARK: -

    private enum Constants {

        // MARK: - Type Properties

        static let commentTableCellIdentifier = "CommentTableCell"
    }

    // MARK: -

    private enum Segues {

        // MARK: - Type Properties

        static let unauthorize = "Unauthorize"

        static let closeComments = "CloseComments"

        static let showChatMessages = "ShowChatMessages"
    }
    
    // MARK: -
    
    private enum CommentState {
        
        // MARK: - Enumeration Cases
        
        case send
        case edit(comment: Comment)
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var commentCountLabel: UILabel!

    @IBOutlet private weak var topView: UIView!
    @IBOutlet private weak var tableView: UITableView!

    @IBOutlet private weak var emptyStateView: EmptyStateView!

    @IBOutlet private weak var messageView: UIView!
    @IBOutlet private weak var sendButton: UIButton!
    @IBOutlet private weak var textView: TextView!

    @IBOutlet private weak var messageViewToTableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet private weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private weak var tableViewToSafeAreaBottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var textViewHeightConstraint: NSLayoutConstraint!

    // MARK: -

    private lazy var dimmingView: UIView = {
        let view = UIView()

        view.backgroundColor = Colors.blackBackground.withAlphaComponent(0.66)
        view.isHidden = true
        view.alpha = 0
        view.frame = CGRect(x: 0, y: 0, size: UIScreen.main.bounds.size)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self,
                                                          action: #selector(self.onDimmingViewTapGestureRecognized(_:)))

        view.addGestureRecognizer(tapGestureRecognizer)

        return view
    }()

    // MARK: -

    private var commentList: CommentList!
    private var commentListType: CommentListType = .unknown
    
    private var commentState: CommentState = .send

    private var shouldShowKeyboard: Bool?
    private var feed: Feed?

    private var readCommentUIDs = Set<Int64>()

    private var shouldApplyData = true
    
    private var dismissHandler: ((_ : CommentDismissData) -> Void)?

    // MARK: - Instance Methods

    @IBAction private func onCloseButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onCloseButtonTouchUpInside()", from: self)

        self.dismiss(animated: true)
    }

    @IBAction private func onSendButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onSendButtonTouchUpInside()", from: self)

        let loadingViewController = LoadingViewController()

        self.view.endEditing(true)
        
        self.present(loadingViewController, animated: true, completion: {
            switch self.commentState {
            case .send:
                self.postComment(with: self.textView.text, loadingViewController: loadingViewController)
                
            case .edit(let comment):
                self.edit(comment: comment, newText: self.textView.text, loadingViewController: loadingViewController)
            }
        })
    }

    @objc private func onDimmingViewTapGestureRecognized(_ sender: UITapGestureRecognizer) {
        Log.high("onDimmingViewTapGestureRecognized()", from: self)

        self.view.endEditing(true)
    }

    // MARK: -

    private func showNoDataState() {
        self.showEmptyState(title: "Comments not found".localized())
    }

    private func hideEmptyState() {
        self.emptyStateView.isHidden = true
    }

    private func showLoadingState() {
        self.showEmptyState(title: "Loading comments".localized(),
                            message: "We are loading list of comments. Please wait a bit".localized())

        self.emptyStateView.showActivityIndicator()
    }

    private func showEmptyState(image: UIImage? = nil, title: String?, message: String? = nil, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()

        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action

        self.emptyStateView.isHidden = false
    }

    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try Again".localized(), isPrimary: true, onClicked: {
            retryHandler?()
        })

        switch error as? WebError {
        case .some(.connection), .some(.timeOut):
            if self.commentList.isEmpty {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your Wi-Fi or mobile data connection.".localized(),
                                    action: action)
            }

        case .some(.unauthorized):
            self.shouldApplyData = true

            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)

        default:
            if self.commentList.isEmpty {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        }
    }

    // MARK: -

    private func updateSendButtonState() {
        self.sendButton.isEnabled = self.textView.hasText
        
        switch self.commentState {
        case .send:
            self.sendButton.setTitle("Send".localized(), for: .normal)
            
        case .edit:
            self.sendButton.setTitle("Edit".localized(), for: .normal)
        }
    }

    private func updateTextViewHeight() {
        let isPinnedToTop = (self.topView.frame.origin.y == self.messageView.frame.origin.y)
        let isOversize = (self.textView.contentSize.height >= textView.bounds.height)

        let fixedWidth = self.textView.frame.size.width
        let newHeight = self.textView.sizeThatFits(CGSize(width: fixedWidth, height: .greatestFiniteMagnitude)).height

        if isPinnedToTop, isOversize {
            self.textView.isScrollEnabled = true
        } else {
            self.textViewHeightConstraint.constant = newHeight
            self.textView.isScrollEnabled = false
        }

        self.view.layoutIfNeeded()
    }

    private func updateDimmingViewState() {
        guard let keyWindow = UIApplication.shared.keyWindow else {
            return
        }

        let convertedPoint = self.view.convert(self.messageView.frame.origin, to: keyWindow)

        self.dimmingView.frame = CGRect(x: 0, y: 0, width: keyWindow.bounds.width, height: convertedPoint.y)
        self.dimmingView.isHidden = (self.bottomSpacerViewHeightConstraint.constant == 0)
        self.dimmingView.alpha = (self.bottomSpacerViewHeightConstraint.constant == 0) ? 0 : 1
    }

    // MARK: -

    private func refreshCommentList() {
        Log.high("refreshCommentList()", from: self)

        guard let feedUID = self.feed?.uid else {
            fatalError()
        }

        if (self.commentList.isEmpty) || (!self.emptyStateView.isHidden) {
            self.showLoadingState()
        }

        firstly {
            Services.commentService.fetchComments(for: feedUID)
        }.done { commentList in
            self.apply(commentList: commentList)
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshCommentList()
            })
        }
    }

    private func postComment(with text: String, loadingViewController: LoadingViewController) {
        Log.high("postComment(text: \(text))", from: self)

        guard let feedUID = self.feed?.uid else {
            fatalError()
        }

        firstly {
            Services.commentService.postComment(with: text, for: feedUID)
        }.done { commentList in
            SVProgressHUD.showSuccess(withStatus: "Sent!".localized())
            
            loadingViewController.dismiss(animated: true, completion: {
                SVProgressHUD.dismiss()
                
                self.textView.text = nil

                self.updateTextViewHeight()

                self.updateSendButtonState()

                self.apply(commentList: commentList)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(stateError: error)
            })
        }
    }
    
    private func delete(comment: Comment, loadingViewController: LoadingViewController) {
        Log.high("delete(comment:\(comment.uid))", from: self)
        
        guard let feedUID = self.feed?.uid else {
            fatalError()
        }
        
        firstly {
            Services.commentService.deleteComment(with: comment.uid, for: feedUID)
        }.done { commentList in
            loadingViewController.dismiss(animated: true, completion: {
                self.textView.text = nil

                self.updateTextViewHeight()

                self.updateSendButtonState()

                self.apply(commentList: commentList)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(stateError: error)
            })
        }
    }
    
    private func edit(comment: Comment, newText: String, loadingViewController: LoadingViewController) {
        Log.high("edit(comment: \(comment.uid), newText: \(newText))", from: self)
        
        guard let feedUID = self.feed?.uid else {
            fatalError()
        }
        
        firstly {
            Services.commentService.editComment(with: comment.uid, for: feedUID, newText: newText)
        }.done { comment in
            loadingViewController.dismiss(animated: true, completion: {
                self.commentState = .send
                
                self.textView.text = nil
                
                self.updateTextViewHeight()
                
                self.updateSendButtonState()
                
                self.apply(commentListType: self.commentListType)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(stateError: error)
            })
        }
    }
    
    private func showMoreOptions(for comment: Comment) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.view.tintColor = Colors.primary
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .cancel)
        
        let editCommentAction = UIAlertAction(title: "Edit comment".localized(), style: .default, handler: { action in
            self.commentState = .edit(comment: comment)
            
            self.textView.becomeFirstResponder()
            self.textView.text = comment.text
                        
            self.updateSendButtonState()
        })
        
        let deleteCommentAction = UIAlertAction(title: "Delete comment".localized(), style: .destructive, handler: { action in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: {
                self.delete(comment: comment, loadingViewController: loadingViewController)
            })
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(editCommentAction)
        alertController.addAction(deleteCommentAction)
        
        self.present(alertController, animated: true)
    }

    private func onChatTapped(loadingViewController: LoadingViewController, comment: Comment) {
        guard let potentialChatMember = comment.sender else {
            return
        }
        
        firstly {
            Services.usersService.refresh(userUID: potentialChatMember.uid)
        }.done { user in
            loadingViewController.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: Segues.showChatMessages, sender: (comment, user))
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.showMessage(withError: error)
            })
        }
    }

    // MARK: -

    private func apply(shouldShowKeyboard: Bool, feed: Feed, dismissHandler: @escaping ((_ : CommentDismissData) -> Void)) {
        Log.high("apply(shouldShowKeyboard: \(shouldShowKeyboard), feed: \(feed.uid))", from: self)

        self.shouldShowKeyboard = shouldShowKeyboard
        self.feed = feed
        self.dismissHandler = dismissHandler

        if self.isViewLoaded {
            self.apply(commentListType: .feed(uid: feed.uid))

            self.shouldApplyData = false
        } else {
            self.shouldApplyData = true
        }
    }

    private func apply(commentListType: CommentListType) {
        Log.high("apply(commentListType: \(commentListType.rawValue))", from: self)

        self.commentListType = commentListType

        let commentList = Services.cacheViewContext.commentListManager.firstOrNew(withListType: commentListType)

        self.apply(commentList: commentList, canShowState: false)

        self.refreshCommentList()
    }

    private func apply(commentList: CommentList, canShowState: Bool = true) {
        Log.high("apply(commentList: \(commentList.count))", from: self)

        self.commentList = commentList

        if commentList.isEmpty && canShowState {
            self.showNoDataState()
        } else {
            self.hideEmptyState()
        }

        if commentList.count == 1 {
            self.commentCountLabel.text = String(format: "%d comment".localized(), commentList.count)
        } else {
            self.commentCountLabel.text = String(format: "%d comments".localized(), commentList.count)
        }

        self.commentCountLabel.isHidden = false

        self.tableView.reloadData()

        self.shouldApplyData = false
    }

    // MARK: -

    private func configureMessageView() {
        self.messageView.layer.applyShadow(color: Colors.blackShadow, alpha: 0.15, x: 0, y: 2, blur: 10)
    }

    private func configureTextView() {
        self.textView.defaultFont = Fonts.medium(ofSize: 14)
        self.textView.placeholderFont = Fonts.regular(ofSize: 14)
    }

    private func configureDimmingView() {
        UIApplication.shared.keyWindow?.addSubview(self.dimmingView)
    }
    
    private func removeDimmingView() {
        self.dimmingView.removeFromSuperview()
    }

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        self.shouldApplyData = true

        self.configureMessageView()
        self.configureTextView()
        self.configureDimmingView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if self.shouldApplyData, let shouldShowKeyboard = self.shouldShowKeyboard, let feed = self.feed, let dismissHandler = self.dismissHandler {
            self.apply(shouldShowKeyboard: shouldShowKeyboard, feed: feed, dismissHandler: dismissHandler)
        }

        self.subscribeToKeyboardNotifications()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if self.shouldShowKeyboard ?? false {
            self.textView.becomeFirstResponder()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.removeDimmingView()
      
        guard let feed = self.feed else {
            fatalError()
        }
        
        let data: CommentDismissData = (readCommentUIDs: Array(self.readCommentUIDs), feed: feed)
        
        self.dismissHandler?(data)

        self.unsubscribeFromKeyboardNotifications()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        switch segue.identifier {
        case Segues.showChatMessages:
            guard let destinationNavigationController = segue.destination as? UINavigationController,
                let destinationViewController = destinationNavigationController.topViewController as? ChatMessagesRoutingController else {
                    fatalError()
            }
            
            if let sender = sender as? (comment: Comment, chatMember: User), let feed = self.feed {
                destinationViewController.apply(comment: sender.comment, for: feed, potentialChatMember: sender.chatMember)
            }

        default:
            break
        }
    }
}

// MARK: - UITableViewDataSource

extension CommentsTableViewController: UITableViewDataSource {

    // MARK: - Instance Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.commentList?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.commentTableCellIdentifier, for: indexPath)

        self.configure(commentCell: cell as! CommentTableViewCell, at: indexPath)

        return cell
    }
}

// MARK: - UITableViewDataSource Helpers

extension CommentsTableViewController {

    // MARK: - Instance Methods

    private func configure(commentCell cell: CommentTableViewCell, at indexPath: IndexPath) {
        let comment = self.commentList[indexPath.row]

        cell.comment = comment.text

        if let createdDate = comment.createdDate {
            cell.time = CommentDateFormatter.string(from: createdDate)
        } else {
            cell.time = nil
        }

        switch comment.status {
        case .some(.read), .none:
            cell.commentFont = Fonts.medium(ofSize: 14)

        case .some(.unread):
            cell.commentFont = Fonts.bold(ofSize: 14)
        }
        
        guard let userID = Services.accountUser?.uid else {
            fatalError()
        }
        
        cell.isChatButtonHidden = (userID == comment.sender?.uid)
        cell.isMoreOptionsButtonHidden = !(userID == comment.sender?.uid)

        cell.onChatButtonClicked = { [unowned self] in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: { [unowned self] in
                self.onChatTapped(loadingViewController: loadingViewController, comment: comment)
            })
        }
        
        cell.onMoreOptionsButtonClicked = {
            self.showMoreOptions(for: comment)
        }
    }
}

// MARK: - UITableViewDelegate

extension CommentsTableViewController: UITableViewDelegate {

    // MARK: - Instance Methods

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let comment = self.commentList[indexPath.row]

        if comment.status == .some(.unread) {
            self.readCommentUIDs.insert(comment.uid)
        }
    }
}

// MARK: - UITextViewDelegate

extension CommentsTableViewController: UITextViewDelegate {

    // MARK: - Instance Methods

    func textViewDidChange(_ textView: UITextView) {
        self.updateTextViewHeight()
        self.updateDimmingViewState()
        self.updateSendButtonState()
    }
}

// MARK: - KeyboardHandler

extension CommentsTableViewController: KeyboardHandler {

    // MARK: - Instance Properties

    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight

        if keyboardHeight == 0 {
            self.tableViewToSafeAreaBottomConstraint.priority = .defaultHigh
        } else {
            self.tableViewToSafeAreaBottomConstraint.priority = .defaultLow
        }

        self.view.layoutIfNeeded()

        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()

            self.updateDimmingViewState()
        })
    }
}

// MARK: - DictionaryReceiver

extension CommentsTableViewController: DictionaryReceiver {

    // MARK: - Instance Methods

    func apply(dictionary: [String: Any]) {
        guard let shouldShowKeyboard = dictionary["shouldShowKeyboard"] as? Bool, let feed = dictionary["feed"] as? Feed else {
            return
        }
        
        guard let dismissHandler = dictionary["dismissHandler"] as? ((_ : CommentDismissData) -> Void) else {
            return
        }

        self.apply(shouldShowKeyboard: shouldShowKeyboard, feed: feed, dismissHandler: dismissHandler)
    }
}
