//
//  WriteCommentViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 31/10/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import SVProgressHUD
import FriendstaTools
import PromiseKit

class WriteCommentViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    typealias CommentDismissData = (Feed)
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var messageView: UIView!
    @IBOutlet private weak var sendButton: UIButton!
    @IBOutlet private weak var textView: TextView!
    @IBOutlet private weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: -

    private lazy var dimmingView: UIView = {
        let view = UIView()

        view.backgroundColor = Colors.blackBackground.withAlphaComponent(0.66)
        view.isHidden = true
        view.alpha = 0
        view.frame = CGRect(x: 0, y: 0, size: UIScreen.main.bounds.size)

        return view
    }()
    
    private var feed: Feed?
    private var dismissHandler: ((CommentDismissData) -> Void)?
    
    // MARK: - Instance Methods
    
    @IBAction private func onViewTouchUpInside(_ sender: Any) {
        Log.high("", from: self)
        
        self.dismiss(animated: true)
    }
    
    @IBAction private func onSendButtonTouchUpInside(_ sender: Any) {
        Log.high("onSendButtonTouchUpInside()", from: self)
        
        let loadingViewController = LoadingViewController()

        self.view.endEditing(true)
        
        self.postComment(with: self.textView.text, loadingViewController: loadingViewController)
    }
    
    // MARK: -
    
    private func postComment(with text: String, loadingViewController: LoadingViewController) {
        Log.high("postComment(text: \(text))", from: self)

        guard let feedUID = self.feed?.uid else {
            fatalError()
        }

        firstly {
            Services.commentService.postComment(with: text, for: feedUID)
        }.done { commentList in
            SVProgressHUD.showSuccess(withStatus: "Sent!".localized())
            
            loadingViewController.dismiss(animated: true, completion: {
                SVProgressHUD.dismiss(withDelay: 2)
                
                self.dismiss(animated: true)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.showMessage(withError: error)
            })
        }
    }
    
    private func configureMessageView() {
        self.messageView.layer.applyShadow(color: Colors.blackShadow, alpha: 0.15, x: 0, y: 2, blur: 10)
    }
    
    private func updateDimmingViewState() {
        guard let keyWindow = UIApplication.shared.keyWindow else {
            return
        }

        let convertedPoint = self.view.convert(self.messageView.frame.origin, to: keyWindow)

        self.dimmingView.frame = CGRect(x: 0, y: 0, width: keyWindow.bounds.width, height: convertedPoint.y)
        self.dimmingView.isHidden = (self.bottomSpacerViewHeightConstraint.constant == 0)
        self.dimmingView.alpha = (self.bottomSpacerViewHeightConstraint.constant == 0) ? 0 : 1
    }
    
    private func configureDimmingView() {
        UIApplication.shared.keyWindow?.addSubview(self.dimmingView)
    }
    
    private func removeDimmingView() {
        self.dimmingView.removeFromSuperview()
    }
    
    private func apply(feed: Feed, dismissHandler: @escaping ((CommentDismissData) -> Void)) {
        self.feed = feed
        self.dismissHandler = dismissHandler
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureMessageView()
        self.configureDimmingView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribeToKeyboardNotifications()
        
        self.textView.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.removeDimmingView()
        
        guard let feed = self.feed else {
            fatalError()
        }
      
        let data: CommentDismissData = (feed)

        self.dismissHandler?(data)
        
        self.unsubscribeFromKeyboardNotifications()
    }
}

// MARK: - KeyboardHandler

extension WriteCommentViewController: KeyboardHandler {

    // MARK: - Instance Properties

    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
            
            self.updateDimmingViewState()
        })
    }
}

// MARK: - UITextViewDelegate

extension WriteCommentViewController: UITextViewDelegate {

    // MARK: - Instance Methods

    func textViewDidChange(_ textView: UITextView) {
        self.sendButton.isEnabled = self.textView.hasText
    }
}

// MARK: - DictionaryReceiver

extension WriteCommentViewController: DictionaryReceiver {

    // MARK: - Instance Methods

    func apply(dictionary: [String: Any]) {
        guard let feed = dictionary["feed"] as? Feed else {
            return
        }
        
        guard let dismissHandler = dictionary["dismissHandler"] as? ((CommentDismissData) -> Void) else {
            return
        }
        
        self.apply(feed: feed, dismissHandler: dismissHandler)
    }
}
