//
//  TutorialStepViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 05.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class TutorialStepViewController: LoggedViewController {
    
    // MARK: - Instance Properties

    @IBOutlet fileprivate weak var emojiImageView: UIImageView!
    @IBOutlet fileprivate weak var messageLabel: UILabel!
    
    // MARK: -
    
    fileprivate(set) var step: TutorialStep?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - Instance Methods
    
    func apply(step: TutorialStep) {
        Log.high("apply(step: \(step))", from: self)
        
        self.step = step
        
        if self.isViewLoaded {
            switch step {
            case .first:
                self.emojiImageView.image = #imageLiteral(resourceName: "TutorialEmoji1")
                self.messageLabel.text = "One of the next 5 friends requested boosts".localized()
                
            case .second:
                self.emojiImageView.image = #imageLiteral(resourceName: "TutorialEmoji2")
                self.messageLabel.text = "You can’t know who exactly, but you can send boosts to all of them".localized()
                
            case .third:
                self.emojiImageView.image = #imageLiteral(resourceName: "TutorialEmoji3")
                self.messageLabel.text = "They will get your boosts but will not know they came from you".localized()
                
            case .fourth:
                self.emojiImageView.image = #imageLiteral(resourceName: "TutorialEmoji4")
                self.messageLabel.text = "Excellent! Keep sending boosts to your friends in waiting".localized()
                
            case .fifth:
                self.emojiImageView.image = #imageLiteral(resourceName: "TutorialEmoji5")
                self.messageLabel.text = "Awesome! Everyone got their boosts. We'll let you know when someone makes a new request".localized()
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.messageLabel.font = Fonts.heavy(ofSize: 22.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let step = self.step, !self.hasAppliedData {
            self.apply(step: step)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
}
