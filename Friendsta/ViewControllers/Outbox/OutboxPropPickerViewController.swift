//
//  OutboxPropPickerViewController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools

class OutboxPropPickerViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
    
        // MARK: - Type Properties
        
        static let matrixTopLeftPropIndex = 0
        static let matrixTopRightPropIndex = 1
        static let matrixBottomLeftPropIndex = 2
        static let matrixBottomRightPropIndex = 3
        
        static let matrixPropCount = 4
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var firstPropMatrixView: PropMatrixView!
    @IBOutlet fileprivate weak var secondPropMatrixView: PropMatrixView!
    
    @IBOutlet fileprivate weak var ejectedPropMatrixSlotView: UIView!
    @IBOutlet fileprivate weak var firstPropMatrixSlotView: UIView!
    @IBOutlet fileprivate weak var secondPropMatrixSlotView: UIView!
    
    @IBOutlet fileprivate weak var shufflePropsButton: Button!
    @IBOutlet fileprivate weak var searchPropsButton: Button!
    
    @IBOutlet fileprivate weak var shufflePropsButtonEnabledSlotView: UIView!
    @IBOutlet fileprivate weak var shufflePropsButtonDisabledSlotView: UIView!
    
    // MARK: -
    
    fileprivate var propMatrixViews: [PropMatrixView] = []
    
    fileprivate var nextPropsAnimator: UIViewPropertyAnimator?
    
    // MARK: -
    
    fileprivate(set) var propList: [PropListItem] = []
    fileprivate(set) var gender: Gender = .nonBinary
    fileprivate(set) var canSendCustomProp = false
    
    fileprivate(set) var nonBinaryPropList: [PropListItem] = []
    fileprivate(set) var malePropList: [PropListItem] = []
    fileprivate(set) var femalePropList: [PropListItem] = []

    fileprivate(set) var randomPropList: [PropListItem] = []
    
    fileprivate(set) var hasAppliedData = false
    
    var onPropPicked: ((_ prop: PropListItem, _ view: UIView) -> Void)?
    var onCustomPropPicked: (() -> Void)?
    var onSearchTapped: (() -> Void)?
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onShufflePropsButtonTouchUpInside(_ sender: Any) {
        Log.high("onShufflePropsButtonTouchUpInside()", from: self)
        
        self.activateNextProps(with: self.gender, canSendCustomProp: self.canSendCustomProp)
    }
    
    @IBAction fileprivate func onSearchButtonTouchUpInside(_ sender: Any) {
        Log.high("onPropsButtonTouchUpInside()", from: self)
        self.onSearchTapped?()
    }
    
    fileprivate func onTopLeftPropButtonClicked(sender: PropMatrixView) {
        self.onPropPicked?(self.randomPropList[Constants.matrixTopLeftPropIndex], sender.topLeftView)
    }
    
    fileprivate func onTopRightPropButtonClicked(sender: PropMatrixView) {
        self.onPropPicked?(self.randomPropList[Constants.matrixTopRightPropIndex], sender.topRightView)
    }
    
    fileprivate func onBottomLeftPropButtonClicked(sender: PropMatrixView) {
        self.onPropPicked?(self.randomPropList[Constants.matrixBottomLeftPropIndex], sender.bottomLeftView)
    }
    
    fileprivate func onBottomRightPropButtonClicked(sender: PropMatrixView) {
        self.onPropPicked?(self.randomPropList[Constants.matrixBottomRightPropIndex], sender.bottomRightView)
    }
    
    fileprivate func onCustomPropButtonClicked(sender: PropMatrixView) {
        self.onCustomPropPicked?()
    }
    
    // MARK: -
    
    fileprivate func configMatrixTopLeftProp(at propMatrixIndex: Int, propIndex: Int) {
        self.propMatrixViews[propMatrixIndex].isTopLeftPropEnabled = (propIndex < self.randomPropList.count)
        
        if self.propMatrixViews[propMatrixIndex].isTopLeftPropEnabled {
            self.propMatrixViews[propMatrixIndex].topLeftPropEmoji = self.randomPropList[propIndex].prop?.emoji
            self.propMatrixViews[propMatrixIndex].topLeftPropMessage = self.randomPropList[propIndex].prop?.message
        } else {
            self.propMatrixViews[propMatrixIndex].topLeftPropEmoji = nil
            self.propMatrixViews[propMatrixIndex].topLeftPropMessage = nil
        }
    }
    
    fileprivate func configMatrixTopRightProp(at propMatrixIndex: Int, propIndex: Int) {
        self.propMatrixViews[propMatrixIndex].isTopRightPropEnabled = (propIndex < self.randomPropList.count)
        
        if self.propMatrixViews[propMatrixIndex].isTopRightPropEnabled {
            self.propMatrixViews[propMatrixIndex].topRightPropEmoji = self.randomPropList[propIndex].prop?.emoji
            self.propMatrixViews[propMatrixIndex].topRightPropMessage = self.randomPropList[propIndex].prop?.message
        } else {
            self.propMatrixViews[propMatrixIndex].topRightPropEmoji = nil
            self.propMatrixViews[propMatrixIndex].topRightPropMessage = nil
        }
    }
    
    fileprivate func configMatrixBottomLeftProp(at propMatrixIndex: Int, propIndex: Int) {
        self.propMatrixViews[propMatrixIndex].isBottomLeftPropEnabled = (propIndex < self.randomPropList.count)
        
        if self.propMatrixViews[propMatrixIndex].isBottomLeftPropEnabled {
            self.propMatrixViews[propMatrixIndex].bottomLeftPropEmoji = self.randomPropList[propIndex].prop?.emoji
            self.propMatrixViews[propMatrixIndex].bottomLeftPropMessage = self.randomPropList[propIndex].prop?.message
        } else {
            self.propMatrixViews[propMatrixIndex].bottomLeftPropEmoji = nil
            self.propMatrixViews[propMatrixIndex].bottomLeftPropMessage = nil
        }
    }
    
    fileprivate func configMatrixBottomRightProp(at propMatrixIndex: Int, propIndex: Int) {
        self.propMatrixViews[propMatrixIndex].isBottomRightPropEnabled = (propIndex < self.randomPropList.count)
        
        if self.propMatrixViews[propMatrixIndex].isBottomRightPropEnabled {
            self.propMatrixViews[propMatrixIndex].bottomRightPropEmoji = self.randomPropList[propIndex].prop?.emoji
            self.propMatrixViews[propMatrixIndex].bottomRightPropMessage = self.randomPropList[propIndex].prop?.message
        } else {
            self.propMatrixViews[propMatrixIndex].bottomRightPropEmoji = nil
            self.propMatrixViews[propMatrixIndex].bottomRightPropMessage = nil
        }
    }
    
    fileprivate func configPropMatrixView(at propMatrixIndex: Int) {
        self.configMatrixTopLeftProp(at: propMatrixIndex, propIndex: Constants.matrixTopLeftPropIndex)
        self.configMatrixTopRightProp(at: propMatrixIndex, propIndex: Constants.matrixTopRightPropIndex)
        self.configMatrixBottomLeftProp(at: propMatrixIndex, propIndex: Constants.matrixBottomLeftPropIndex)
        self.configMatrixBottomRightProp(at: propMatrixIndex, propIndex: Constants.matrixBottomRightPropIndex)
        
        self.propMatrixViews[propMatrixIndex].isCustomPropHidden = !self.canSendCustomProp
    }
    
    fileprivate func enableShufflePropsButton() {
        self.shufflePropsButton.isUserInteractionEnabled = true
    }
    
    fileprivate func disableShufflePropsButton() {
        self.shufflePropsButton.isUserInteractionEnabled = false
    }
    
    fileprivate func layoutShufflePropsButton() {
        if self.nextPropsAnimator == nil {
            if self.shufflePropsButton.isUserInteractionEnabled {
                self.shufflePropsButton.frame = self.shufflePropsButtonEnabledSlotView.frame
            } else {
                self.shufflePropsButton.frame = self.shufflePropsButtonDisabledSlotView.frame
            }
        }
    }
    
    fileprivate func layoutPropMatrixViews() {
        self.propMatrixViews[0].frame = self.firstPropMatrixSlotView.frame
        self.propMatrixViews[0].alpha = 1.0
        
        if self.nextPropsAnimator == nil {
            self.propMatrixViews[1].frame = self.secondPropMatrixSlotView.frame
        } else {
            self.propMatrixViews[1].frame = self.ejectedPropMatrixSlotView.frame
        }
        
        self.propMatrixViews[1].alpha = 0.0
    }
    
    fileprivate func prepareGenderPropLists() {
        self.nonBinaryPropList = self.propList.filter({ propListItem in
            switch propListItem.prop?.gender {
            case .some(.nonBinary):
                return true
                
            case .some(.male), .some(.female), .none:
                return false
            }
        })
        
        self.malePropList = self.propList.filter({ propListItem in
            switch propListItem.prop?.gender {
            case .some(.nonBinary), .some(.male):
                return true
                
            case .some(.female), .none:
                return false
            }
        })
        
        self.femalePropList = self.propList.filter({ propListItem in
            switch propListItem.prop?.gender {
            case .some(.nonBinary), .some(.female):
                return true
                
            case .some(.male), .none:
                return false
            }
        })
    }
    
    fileprivate func prepareRandomPropList() {
        let propList: [PropListItem]
        
        switch self.gender {
        case .nonBinary:
            propList = self.nonBinaryPropList
            
        case .male:
            propList = self.malePropList
            
        case .female:
            propList = self.femalePropList
        }
        
        guard !propList.isEmpty else {
            return
        }
        
        let randomPropCount = max(self.randomPropList.count + propList.count, 8)
        
        var randomPropQueue = propList
        
        while self.randomPropList.count < randomPropCount {
            let randomIndex = arc4random_uniform(UInt32(randomPropQueue.count))
            
            self.randomPropList.append(randomPropQueue.remove(at: Int(randomIndex)))
            
            if randomPropQueue.isEmpty {
                randomPropQueue = propList
            }
        }
    }
    
    // MARK: -
    
    @discardableResult
    func activateNextProps(with gender: Gender, canSendCustomProp: Bool, newPropList: [PropListItem]? = nil) -> Guarantee<Void> {
        Log.high("activateNextProps(withGender: \(gender))", from: self)
        
        return Guarantee(resolver: { completion in
            self.propMatrixViews.append(self.propMatrixViews.remove(at: 0))
            
            if let newPropList = newPropList {
                self.apply(propList: newPropList, gender: gender, canSendCustomProp: canSendCustomProp)
            } else {
                if self.gender != gender {
                    self.gender = gender
                    
                    self.randomPropList = []
                } else if self.randomPropList.count >= Constants.matrixPropCount {
                    self.randomPropList.removeFirst(Constants.matrixPropCount)
                }
                
                if self.randomPropList.count < Constants.matrixPropCount {
                    self.prepareRandomPropList()
                }
                
                self.configPropMatrixView(at: 0)
                
                self.apply(propList: self.propList, gender: gender, canSendCustomProp: canSendCustomProp)
            }
            
            self.disableShufflePropsButton()
            
            let animator = UIViewPropertyAnimator(duration: 0.25, curve: .linear)
            
            animator.addAnimations({ [weak self] in
                self?.layoutPropMatrixViews()
            })
            
            animator.addCompletion({ [weak self] position in
                if let viewController = self {
                    viewController.nextPropsAnimator = nil
                    
                    viewController.enableShufflePropsButton()
                    viewController.layoutPropMatrixViews()
                }
                
                completion(Void())
            })
            
            self.nextPropsAnimator = animator
            
            animator.startAnimation()
        })
    }
    
    @discardableResult
    func showWaitingState() -> Guarantee<Void> {
        Log.high("showWaitingState()", from: self)
        
        return Guarantee(resolver: { completion in
            self.propMatrixViews[0].showWaitingState()
            self.propMatrixViews[1].hideWaitingState()
            
            self.disableShufflePropsButton()
            
            UIView.animate(withDuration: 0.25, animations: {
                self.layoutShufflePropsButton()
            }, completion: { finished in
                completion(Void())
            })
        })
    }
    
    @discardableResult
    func hideWaitingState() -> Guarantee<Void> {
        Log.high("hideWaitingState()", from: self)
        
        return Guarantee(resolver: { completion in
            self.propMatrixViews[0].hidePropSelectionState()
            self.propMatrixViews[1].hidePropSelectionState()
            
            self.propMatrixViews[0].hideWaitingState()
            self.propMatrixViews[1].hideWaitingState()
            
            self.enableShufflePropsButton()
            
            UIView.animate(withDuration: 0.25, animations: {
                self.layoutShufflePropsButton()
            }, completion: { finished in
                completion(Void())
            })
        })
    }
    
    func apply(propList: [PropListItem], gender: Gender, canSendCustomProp: Bool) {
        Log.high("apply(propList: \(propList.count), gender: \(gender))", from: self)
        
        self.propList = propList
        self.gender = gender
        self.canSendCustomProp = canSendCustomProp
        
        if self.isViewLoaded {
            self.randomPropList = []
            
            self.prepareGenderPropLists()
            self.prepareRandomPropList()
            
            self.configPropMatrixView(at: 0)
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.shufflePropsButton.layer.masksToBounds = false
        self.shufflePropsButton.layer.shadowColor = UIColor.black.cgColor
        self.shufflePropsButton.layer.shadowOpacity = 0.5
        self.shufflePropsButton.layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        
        self.searchPropsButton.layer.masksToBounds = false
        self.searchPropsButton.layer.shadowColor = UIColor.black.cgColor
        self.searchPropsButton.layer.shadowOpacity = 0.5
        self.searchPropsButton.layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        
        self.propMatrixViews = [self.firstPropMatrixView, self.secondPropMatrixView]
        
        for propMatrixView in self.propMatrixViews {
            propMatrixView.onTopLeftPropButtonClicked = { [unowned self, unowned propMatrixView] in
                self.onTopLeftPropButtonClicked(sender: propMatrixView)
            }
            
            propMatrixView.onTopRightPropButtonClicked = { [unowned self, unowned propMatrixView] in
                self.onTopRightPropButtonClicked(sender: propMatrixView)
            }
            
            propMatrixView.onBottomLeftPropButtonClicked = { [unowned self, unowned propMatrixView] in
                self.onBottomLeftPropButtonClicked(sender: propMatrixView)
            }
            
            propMatrixView.onBottomRightPropButtonClicked = { [unowned self, unowned propMatrixView] in
                self.onBottomRightPropButtonClicked(sender: propMatrixView)
            }
            
            propMatrixView.onCustomPropButtonClicked = { [unowned self, unowned propMatrixView] in
                self.onCustomPropButtonClicked(sender: propMatrixView)
            }
        }
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(propList: self.propList, gender: self.gender, canSendCustomProp: self.canSendCustomProp)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.layoutShufflePropsButton()
        self.layoutPropMatrixViews()
    }
}
