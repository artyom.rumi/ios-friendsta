//
//  TutorialViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 05.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class TutorialViewController: LoggedViewController {

    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let embedContent =  "EmbedContent"
        static let finishTutorial = "FinishTutorial"
    }
    
    // MARK: - Instance Properties

    @IBOutlet fileprivate weak var pageControl: UIPageControl!
    @IBOutlet fileprivate weak var nextButton: SecondaryButton!
    
    fileprivate weak var pageViewController: TutorialPageViewController!
    
    // MARK: -
    
    fileprivate(set) var stage: TutorialStage?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onNextButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onNextButtonTouchUpInside", from: self)
        
        let nextStepIndex = self.pageControl.currentPage + 1
        
        if nextStepIndex < self.pageViewController.steps.count {
            self.pageViewController.apply(stepIndex: nextStepIndex)
        } else {
            self.performSegue(withIdentifier: Segues.finishTutorial, sender: self)
        }
    }
    
    // MARK: -
    
    fileprivate func updateNextButton() {
        if self.pageControl.currentPage == self.pageViewController.steps.count - 1 {
            switch self.stage {
            case .some(.first):
                self.nextButton.setTitle("Let's start".localized(), for: .normal)
                
            case .some(.second):
                self.nextButton.setTitle("Continue sending".localized(), for: .normal)
                
            case .some(.third):
                self.nextButton.setTitle("Make someone smile".localized(), for: .normal)
                
            case .none:
                break
            }
        } else {
            self.nextButton.setTitle("Next".localized(), for: .normal)
        }
    }
    
    // MARK: -
    
    func apply(stage: TutorialStage) {
        Log.high("apply(stage: \(stage))", from: self)
        
        self.stage = stage
        
        if self.isViewLoaded {
            self.pageViewController.apply(steps: stage.steps, stepIndex: self.pageControl.currentPage)
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.nextButton.titleLabel?.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(stage: self.stage ?? .first)
        }

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.embedContent:
            guard let pageViewController = segue.destination as? TutorialPageViewController else {
                fatalError()
            }
            
            self.pageViewController = pageViewController
            
            self.pageViewController.onStepIndexChanged = { [unowned self] stepIndex in
                self.pageControl.numberOfPages = self.pageViewController.steps.count
                self.pageControl.currentPage = stepIndex
                
                self.updateNextButton()
            }
            
        default:
            break
        }
    }
}
