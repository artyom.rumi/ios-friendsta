//
//  TutorialPageViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 05.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class TutorialPageViewController: LoggedPageViewController {

    // MARK: - Instance Properties
    
    fileprivate var stepViewControllers: [UIViewController] = []
    
    // MARK: -
    
    var onStepIndexChanged: ((_ stepIndex: Int) -> Void)?

    fileprivate(set) var steps: [TutorialStep] = []
    fileprivate(set) var stepIndex = 0
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - Instance Methods
    
    fileprivate func updateViewControllers(animated: Bool) {
        if (self.stepIndex >= 0) && (self.stepIndex < self.stepViewControllers.count) {
            self.onStepIndexChanged?(self.stepIndex)
            
            self.setViewControllers([self.stepViewControllers[self.stepIndex]],
                                    direction: .forward,
                                    animated: animated,
                                    completion: nil)
        }
    }
    
    // MARK: -
    
    func apply(steps: [TutorialStep], stepIndex: Int) {
        Log.high("apply(steps: \(steps.count), stepIndex: \(stepIndex))", from: self)
        
        self.steps = steps
        self.stepIndex = stepIndex
        
        self.stepViewControllers = []
        
        if self.isViewLoaded {
            let storyboard = UIStoryboard(name: "Outbox", bundle: nil)
            
            for step in self.steps {
                let stepViewController = storyboard.instantiateViewController(withIdentifier: "TutorialStepViewController")
                
                if let stepViewController = stepViewController as? TutorialStepViewController {
                    stepViewController.apply(step: step)
                }
                
                self.stepViewControllers.append(stepViewController)
            }
            
            self.dataSource = nil
            self.dataSource = self
            
            self.updateViewControllers(animated: false)
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    func apply(stepIndex: Int) {
        Log.high("apply(stepIndex: \(stepIndex))", from: self)
        
        self.stepIndex = max(min(stepIndex, self.steps.count), 0)
        
        if self.isViewLoaded {
            self.updateViewControllers(animated: true)
        }
    }
    
    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(steps: self.steps, stepIndex: self.stepIndex)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
}

// MARK: - UIPageViewControllerDataSource

extension TutorialPageViewController: UIPageViewControllerDataSource {
    
    // MARK: - Instance Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let stepIndex = self.stepViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousStepIndex = stepIndex - 1
        
        guard (previousStepIndex >= 0) && (previousStepIndex < self.stepViewControllers.count) else {
            return nil
        }
        
        return self.stepViewControllers[previousStepIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let stepIndex = self.stepViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextStepIndex = stepIndex + 1
        
        guard (nextStepIndex >= 0) && (nextStepIndex < self.stepViewControllers.count) else {
            return nil
        }
        
        return self.stepViewControllers[nextStepIndex]
    }
}

// MARK: - UIPageViewControllerDelegate

extension TutorialPageViewController: UIPageViewControllerDelegate {
    
    // MARK: - Instance Methods
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let stepViewController = pageViewController.viewControllers?.first {
            if let stepIndex = self.stepViewControllers.index(of: stepViewController) {
                self.stepIndex = stepIndex
        
                self.onStepIndexChanged?(stepIndex)
            }
        }
    }
}
