//
//  OutboxPropReceiversViewController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools

class OutboxPropReceiversViewController: LoggedViewController {
    
    // MARK: - Instance Properties

    @IBOutlet fileprivate weak var firstPropReceiverAvatarImageView: UIImageView!
    @IBOutlet fileprivate weak var secondPropReceiverAvatarImageView: UIImageView!
    @IBOutlet fileprivate weak var thirdPropReceiverAvatarImageView: UIImageView!
    @IBOutlet fileprivate weak var fourthPropReceiverAvatarImageView: UIImageView!
    
    @IBOutlet fileprivate weak var ejectedPropReceiverSlotView: UIView!
    @IBOutlet fileprivate weak var firstPropReceiverSlotView: UIView!
    @IBOutlet fileprivate weak var secondPropReceiverSlotView: UIView!
    @IBOutlet fileprivate weak var thirdPropReceiverSlotView: UIView!
    @IBOutlet fileprivate weak var fourthPropReceiverSlotView: UIView!

    @IBOutlet fileprivate weak var skipPropReceiverButton: Button!
    
    @IBOutlet fileprivate weak var skipPropReceiverButtonVisibleSlotView: UIView!
    @IBOutlet fileprivate weak var skipPropReceiverButtonHiddenSlotView: UIView!

    @IBOutlet fileprivate weak var propReceiverFullNameLabel: UILabel!
    @IBOutlet fileprivate weak var propReceiverSchoolInfoLabel: UILabel!
    
    // MARK: -
    
    fileprivate var propReceiverAvatarImageViews: [UIImageView] = []
    
    fileprivate var nextPropReceiversAnimator: UIViewPropertyAnimator?
    
    // MARK: -
    
    var onSkipPropReceiverButtonClicked: (() -> Void)?
    var onPropReceiverAvatarImageViewClicked: ((_ user: User) -> Void)?
    
    fileprivate(set) var propReceivers: [PropReceiver] = []
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onSkipPropReceiverButtonTouchUpInside(_ sender: Any) {
        Log.high("onSkipPropReceiverButtonTouchUpInside()", from: self)
        
        self.onSkipPropReceiverButtonClicked?()
    }
    
    @IBAction fileprivate func onPropReceiverAvatarImageViewTapped(_ sender: UITapGestureRecognizer) {
        Log.high("onPropReceiverAvatarImageViewTapped()", from: self)
        
        if let propReceiverAvatarImageView = sender.view as? UIImageView {
            if let propReceiverIndex = self.propReceiverAvatarImageViews.index(of: propReceiverAvatarImageView) {
                if propReceiverIndex == 0 {
                    if let user = self.propReceivers[propReceiverIndex].user {
                        self.onPropReceiverAvatarImageViewClicked?(user)
                    }
                } else {
                    self.onSkipPropReceiverButtonClicked?()
                }
            }
        }
    }
    
    @IBAction fileprivate func onSwipeLeft(_ sender: Any) {
        Log.high("onSwipeLeft()", from: self)
        
        self.onSkipPropReceiverButtonClicked?()
    }
    
    // MARK: -
    
    fileprivate func loadAvatarImage(at propReceiverIndex: Int) {
        let avatarImageView = self.propReceiverAvatarImageViews[propReceiverIndex]
        
        avatarImageView.image = #imageLiteral(resourceName: "AvatarLargePlaceholder")
        
        if let imageURL = self.propReceivers[propReceiverIndex].user?.largeAvatarURL {
            Services.imageLoader.loadImage(for: imageURL, in: avatarImageView, placeholder: #imageLiteral(resourceName: "AvatarLargePlaceholder"))
        }
    }
    
    fileprivate func configPropReceiverAvatarImage(at propReceiverIndex: Int) {
        if propReceiverIndex < self.propReceivers.count {
            self.loadAvatarImage(at: propReceiverIndex)
        } else {
            self.propReceiverAvatarImageViews[propReceiverIndex].image = nil
        }
    }
    
    fileprivate func updatePropReceiverInfoLabels() {
        let fullName: String?
        let schoolInfo: String?
        
        if let user = self.propReceivers.first?.user {
            fullName = user.fullName
            
            let schoolTitle = user.schoolTitle ?? "unknown school".localized()
            
            if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(user.classYear)) {
                schoolInfo = String(format: "%dth grade %@".localized(), schoolGradeNumber, schoolTitle)
            } else {
                schoolInfo = String(format: "Finished %@".localized(), schoolTitle)
            }
        } else {
            fullName = nil
            schoolInfo = nil
        }
        
        if (self.propReceiverFullNameLabel.text != fullName) || (self.propReceiverSchoolInfoLabel.text != schoolInfo) {
            UIView.animate(withDuration: 0.15, animations: {
                self.propReceiverFullNameLabel.alpha = 0.1
                self.propReceiverSchoolInfoLabel.alpha = 0.1
            }, completion: { finished in
                self.propReceiverFullNameLabel.text = fullName
                self.propReceiverSchoolInfoLabel.text = schoolInfo
                
                UIView.animate(withDuration: 0.15, animations: {
                    self.propReceiverFullNameLabel.alpha = 1.0
                    self.propReceiverSchoolInfoLabel.alpha = 1.0
                })
            })
        }
    }
    
    fileprivate func layoutSkipPropReceiverButton() {
        self.skipPropReceiverButton.frame = self.skipPropReceiverButtonVisibleSlotView.frame
    }
    
    fileprivate func layoutPropReceiverAvatarImageViews() {
        self.propReceiverAvatarImageViews[0].frame = self.firstPropReceiverSlotView.frame
        self.propReceiverAvatarImageViews[1].frame = self.secondPropReceiverSlotView.frame
        self.propReceiverAvatarImageViews[2].frame = self.thirdPropReceiverSlotView.frame
        
        if self.nextPropReceiversAnimator == nil {
            self.propReceiverAvatarImageViews[3].frame = self.fourthPropReceiverSlotView.frame
        } else {
            self.propReceiverAvatarImageViews[3].frame = self.ejectedPropReceiverSlotView.frame
        }
    }
    
    fileprivate func enablePropReceiverAvatarImageViews() {
        self.propReceiverAvatarImageViews[0].isUserInteractionEnabled = true
        self.propReceiverAvatarImageViews[1].isUserInteractionEnabled = true
        self.propReceiverAvatarImageViews[2].isUserInteractionEnabled = true
        self.propReceiverAvatarImageViews[3].isUserInteractionEnabled = true
    }
    
    fileprivate func disablePropReceiverAvatarImageViews() {
        self.propReceiverAvatarImageViews[0].isUserInteractionEnabled = false
        self.propReceiverAvatarImageViews[1].isUserInteractionEnabled = false
        self.propReceiverAvatarImageViews[2].isUserInteractionEnabled = false
        self.propReceiverAvatarImageViews[3].isUserInteractionEnabled = false
    }
    
    // MARK: -

    @discardableResult
    func activateNextPropReceiver(newPropReceivers: [PropReceiver]? = nil) -> Guarantee<Void> {
        Log.high("activateNextPropReceiver()", from: self)
        
        return Guarantee(resolver: { completion in
            self.propReceiverAvatarImageViews.append(self.propReceiverAvatarImageViews.remove(at: 0))
            
            self.view.bringSubviewToFront(self.propReceiverAvatarImageViews[0])
            
            if let newPropReceivers = newPropReceivers {
                self.apply(propReceivers: newPropReceivers)
            } else {
                if !self.propReceivers.isEmpty {
                    self.propReceivers.removeFirst()
                }
                
                self.configPropReceiverAvatarImage(at: 2)
                
                self.updatePropReceiverInfoLabels()
            }
            
            self.disablePropReceiverAvatarImageViews()
            
            let animator = UIViewPropertyAnimator(duration: 0.25, curve: .linear)
            
            animator.addAnimations({ [weak self] in
                self?.layoutPropReceiverAvatarImageViews()
            })
            
            animator.addCompletion({ [weak self] position in
                if let viewController = self {
                    viewController.nextPropReceiversAnimator = nil
                    
                    viewController.layoutPropReceiverAvatarImageViews()
                    viewController.enablePropReceiverAvatarImageViews()
                }
                
                completion(Void())
            })
            
            self.nextPropReceiversAnimator = animator
            
            animator.startAnimation()
        })
    }
    
    func showWaitingState() {
        Log.high("showWaitingState()", from: self)
        
        self.skipPropReceiverButton.isEnabled = false
    }
    
    func hideWaitingState() {
        Log.high("hideWaitingState()", from: self)
        
        self.skipPropReceiverButton.isEnabled = true
    }
    
    func apply(propReceivers: [PropReceiver]) {
        Log.high("apply(propReceivers: \(propReceivers.count))", from: self)
        
        self.propReceivers = propReceivers
        
        if self.isViewLoaded {
            self.configPropReceiverAvatarImage(at: 0)
            self.configPropReceiverAvatarImage(at: 1)
            self.configPropReceiverAvatarImage(at: 2)
            
            self.updatePropReceiverInfoLabels()
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.propReceiverFullNameLabel.font = Fonts.heavy(ofSize: 22.0)
        self.propReceiverSchoolInfoLabel.font = Fonts.medium(ofSize: 15.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        
        self.skipPropReceiverButton.layer.masksToBounds = false
        self.skipPropReceiverButton.layer.shadowColor = UIColor.black.cgColor
        self.skipPropReceiverButton.layer.shadowOpacity = 0.5
        self.skipPropReceiverButton.layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        
        self.propReceiverAvatarImageViews = [self.firstPropReceiverAvatarImageView,
                                             self.secondPropReceiverAvatarImageView,
                                             self.thirdPropReceiverAvatarImageView,
                                             self.fourthPropReceiverAvatarImageView]
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(propReceivers: self.propReceivers)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.layoutSkipPropReceiverButton()
        self.layoutPropReceiverAvatarImageViews()
    }
}
