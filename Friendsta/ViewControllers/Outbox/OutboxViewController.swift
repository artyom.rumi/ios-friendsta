//
//  OutboxViewController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class OutboxViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let embedPropReceivers = "EmbedPropReceivers"
        static let embedPropPicker = "EmbedPropPicker"
        static let showTutorial = "ShowTutorial"
        static let showFriends = "ShowFriends"
        static let showUser = "ShowUser"
        static let sendCustomProp = "SendCustomProp"
        static let sendProp = "SendProp"
        static let addFriends = "AddFriends"
        static let showActionSheet = "ShowActionSheet"
    }
    
    // MARK: -
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let propReceiversMinCount = 5
    }
    
    // MARK: - Instance Properties
   
    @IBOutlet fileprivate weak var emptyStateContainerView: UIView!
    @IBOutlet fileprivate weak var emptyStateView: EmptyStateView!
   
    @IBOutlet fileprivate weak var inviteButton: UIButton!
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    fileprivate weak var propReceiversViewController: OutboxPropReceiversViewController!
    fileprivate weak var propPickerViewController: OutboxPropPickerViewController!
    
    fileprivate var propPickedPosition: Int!
    
    // MARK: -
    
    fileprivate var propReceiversTimer: Timer?
    
    // MARK: -
    
    var propReceivers: [PropReceiver] {
        return self.propReceiversViewController.propReceivers
    }

    var propList: [PropListItem] {
        return self.propPickerViewController.propList
    }
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onTutorialFinished(segue: UIStoryboardSegue) {
        Log.high("onTutorialFinished(\(String(describing: segue.identifier)))", from: self)
        
        firstly {
            Services.accountProvider.captureModel()
        }.done { accountSession in
            var settings = accountSession.model.settings
        
            if !settings.firstTutorialCompleted {
                settings.firstTutorialCompleted = true
            } else if !settings.secondTutorialCompleted {
                settings.secondTutorialCompleted = true
            } else if !settings.thirdTutorialCompleted {
                settings.thirdTutorialCompleted = true
                
                self.unsubscribeFromPropRequestsEvents()
            }
            
            accountSession.model.settingsManager.update(settings: settings)
            accountSession.model.settingsManager.saveSettings()
        }
    }
    
    @IBAction fileprivate func onCopyLinkTapped(_ sender: Any) {
        self.copyLink()
    }
    
    @IBAction fileprivate func onFriendsClosed(segue: UIStoryboardSegue) {
        Log.high("onFriendsClosed(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onFriendsInvitingFinished(segue: UIStoryboardSegue) {
        Log.high("onFriendsInvitingFinished(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onUserClosed(segue: UIStoryboardSegue) {
        Log.high("onOutboxUserClosed(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onCustomPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onCustomPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onCameraClosed(segue: UIStoryboardSegue) {
        Log.high("onCameraClosed(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onActionSheetClosed(segue: UIStoryboardSegue) {
        Log.high("onActionSheetClosed(\(String(describing: segue.identifier)))", from: self)
    }
    
    // MARK: -
    
    fileprivate func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()
        
        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action
        
        self.emptyStateContainerView.isHidden = false
        
        self.propReceiversViewController.viewIfLoaded?.isHidden = true
        self.propPickerViewController.viewIfLoaded?.isHidden = true
    }
    
    fileprivate func hideEmptyState() {
        self.emptyStateContainerView.isHidden = true
        
        self.propReceiversViewController.viewIfLoaded?.isHidden = false
        self.propPickerViewController.viewIfLoaded?.isHidden = false
    }
    
    fileprivate func showFailedState(withError error: Error, retryHandler: (() -> Void)?) {
        let action: EmptyStateAction?
        
        if let retryHandler = retryHandler {
            action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: retryHandler)
        } else {
            action = nil
        }
        
        switch error as? WebError {
        case .some(.unauthorized):
            break
            
        case .some(.connection), .some(.timeOut):
            self.showEmptyState(title: "No Internet Connection".localized(),
                                message: "Check your wi-fi or mobile data connection.".localized(),
                                action: action)
            
        default:
            self.showEmptyState(title: "Something went wrong".localized(),
                                message: "Please let us know what went wrong or try again later.".localized(),
                                action: action)
        }
    }
    
    fileprivate func showNoFriendsState() {
        let action = EmptyStateAction(title: "Copy Link".localized(), isPrimary: false, onClicked: { [unowned self] in
            self.copyLink()
        })
        
        self.showEmptyState(title: "Share Your Friendsta Link".localized(),
                            message: "Let your followers leave you fun anonymous messages through your personal Friendsta link.".localized(),
                            action: action)
    }
    
    fileprivate func showNoPropsState() {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: { [unowned self] in
            self.refreshSpecialProps()
        })
        
        self.showEmptyState(title: "No boosts".localized(),
                            message: "We already checking the problem, please, try again later.".localized(),
                            action: action)
    }
    
    fileprivate func showLoadingState() {
        if self.emptyStateContainerView.isHidden {
            self.showEmptyState(title: "Loading boosts".localized(),
                                message: "We are loading boosts.\nPlease wait a bit.".localized())
        }
        
        self.emptyStateView!.showActivityIndicator()
    }
    
    fileprivate func showWaitingState() {
        self.propReceiversViewController.showWaitingState()
        self.propPickerViewController.showWaitingState()
    }
    
    fileprivate func copyLink() {
        if let link = Services.accountUser?.link {
            UIPasteboard.general.string = link
            self.showCopyLinkSuccess()
        } else {
            firstly {
                Services.accountUserService.refreshAccountUser()
            }.done {[weak self]  accountUser in
                self?.copyLink()
            }.catch { [weak self] error in
                self?.showFailedState(withError: error, retryHandler: { [weak self] in
                    self?.copyLink()
                })
            }
        }
    }
    
    fileprivate func showCopyLinkSuccess() {
        let alertController = UIAlertController(title: "Link Copied".localized(), message: "Paste it to your Snapchat Story or Instagram Bio to receive feedback".localized(), preferredStyle: .alert)
        let closeAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        alertController.addAction(closeAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func hideWaitingState() {
        self.propReceiversViewController.hideWaitingState()
        self.propPickerViewController.hideWaitingState()
    }
    
    fileprivate func setupFont() {
        self.inviteButton.titleLabel?.font = Fonts.medium(ofSize: 12.0)
        self.titleLabel.font = Fonts.black(ofSize: 17.0)
    }
    
    // MARK: -

    fileprivate func activateNextPropReceiver() {
        Log.high("activateNextPropReceiver()", from: self)
        
        if !self.emptyStateContainerView.isHidden {
            self.showLoadingState()
        }
        
        firstly {
            Services.propReceiversService.refreshPropReceivers(minCount: Constants.propReceiversMinCount)
        }.done { [weak self] propReceivers in
            if let viewController = self {
                let propReceiverUser = propReceivers.first?.user
                let propReceiverGender = propReceiverUser?.gender ?? .nonBinary
                let propReceiverCanSendCustomProp = propReceiverUser?.canSendCustomProp ?? false
                
                firstly {
                    when(viewController.propReceiversViewController.activateNextPropReceiver(newPropReceivers: propReceivers),
                         viewController.propPickerViewController.activateNextProps(with: propReceiverGender,
                                                                                   canSendCustomProp: propReceiverCanSendCustomProp,
                                                                                   newPropList: nil))
                }.done { [weak viewController] status in
                    if let viewController = viewController {
                        if viewController.propReceivers.isEmpty {
                            viewController.showNoFriendsState()
                        } else {
                            viewController.hideEmptyState()
                        }
                        
                        viewController.hideWaitingState()
                    }
                }
            }
        }.catch { [weak self] error in
            self?.showFailedState(withError: error, retryHandler: { [weak self] in
                self?.activateNextPropReceiver()
            })
        }
    }
    
    fileprivate func send(propListItem: PropListItem, boostType: BoostType?) {
        Log.high("send(propListItem: \(String(describing: propListItem.prop?.uid)))", from: self)
        
        guard let propReceiver = self.propReceivers.first, let prop = propListItem.prop else {
            return
        }
        
        if !self.emptyStateContainerView.isHidden {
            self.showLoadingState()
        }
        
        self.showWaitingState()
        
        firstly {
            Services.propReceiversService.send(prop: prop, to: propReceiver, boostType: boostType)
        }.done { [weak self] in
            self?.activateNextPropReceiver()
        }.catch { [weak self] error in
            if let viewController = self {
                viewController.hideWaitingState()
            
                viewController.showFailedState(withError: error, retryHandler: { [weak viewController] in
                    viewController?.send(propListItem: propListItem, boostType: boostType)
                })
            }
        }
    }
    
    fileprivate func skipPropReceiver() {
        Log.high("skipPropReceiver()", from: self)
        
        guard let propReceiver = self.propReceivers.first else {
            return
        }
        
        if !self.emptyStateContainerView.isHidden {
            self.showLoadingState()
        }
        
        self.showWaitingState()
        
        firstly {
            Services.propReceiversService.skip(propReceiver: propReceiver)
        }.done { [weak self] in
            self?.activateNextPropReceiver()
        }.catch { [weak self] error in
            if let viewController = self {
                viewController.hideWaitingState()
            
                viewController.showFailedState(withError: error, retryHandler: { [weak viewController] in
                    viewController?.skipPropReceiver()
                })
            }
        }
    }
    
    fileprivate func refreshPropReceivers() {
        Log.high("refreshPropReceivers()", from: self)
        
        if self.propReceivers.isEmpty || (!self.emptyStateContainerView.isHidden) {
            self.showLoadingState()
        }
        
        firstly {
            Services.propReceiversService.refreshPropReceivers(minCount: Constants.propReceiversMinCount)
        }.done { [weak self] propReceivers in
            self?.apply(propReceivers: propReceivers)
        }.catch { [weak self] error in
            self?.showFailedState(withError: error, retryHandler: { [weak self] in
                self?.refreshPropReceivers()
            })
        }
    }
    
    fileprivate func refreshSpecialProps() {
        Log.high("refreshSpecialProps()", from: self)
        
        if self.propList.isEmpty || (!self.emptyStateContainerView.isHidden) {
            self.showLoadingState()
        }
        
        firstly {
            Services.propsService.refreshSpecialProps()
        }.done { [weak self] propList in
            self?.apply(propList: propList)
        }.catch { [weak self] error in
            self?.showFailedState(withError: error, retryHandler: { [weak self] in
                self?.refreshSpecialProps()
            })
        }
    }
    
    fileprivate func restartPropReceiversTimer() {
        self.propReceiversTimer?.invalidate()
        
        self.propReceiversTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false, block: { [weak self] timer in
            if let viewController = self {
                if !Services.accountSettings.firstTutorialCompleted {
                    if viewController.propReceivers.first?.hasRequest ?? false {
                        viewController.performSegue(withIdentifier: Segues.showTutorial, sender: self)
                    }
                } else if !Services.accountSettings.secondTutorialCompleted {
                    let propReceiver = viewController.propReceivers.first(where: { propReceiver in
                        return propReceiver.hasRequest
                    })
                    
                    if let propRequest = propReceiver?.request, propRequest.progress > 0 {
                        viewController.performSegue(withIdentifier: Segues.showTutorial, sender: self)
                    }
                } else if !Services.accountSettings.thirdTutorialCompleted {
                    if Services.cacheViewContext.propRequestsManager.count() == 0 {
                        viewController.performSegue(withIdentifier: Segues.showTutorial, sender: self)
                    }
                }
            }
        })
    }
    
    fileprivate func subscribeToPropReceiversEvents() {
        let propReceiversManager = Services.cacheViewContext.propReceiversManager
        
        propReceiversManager.objectsUpdatedEvent.disconnect(self)
        propReceiversManager.objectsAppendedEvent.disconnect(self)
        propReceiversManager.objectsRemovedEvent.disconnect(self)
        
        propReceiversManager.objectsUpdatedEvent.connect(self, handler: { [weak self] propRequests in
            self?.restartPropReceiversTimer()
        })
        
        propReceiversManager.objectsAppendedEvent.connect(self, handler: { [weak self] propRequests in
            self?.restartPropReceiversTimer()
        })
        
        propReceiversManager.objectsRemovedEvent.connect(self, handler: { [weak self] propRequests in
            self?.restartPropReceiversTimer()
        })
        
        propReceiversManager.startObserving()
    }
    
    fileprivate func unsubscribeFromPropRequestsEvents() {
        let propReceiversManager = Services.cacheViewContext.propReceiversManager
        
        propReceiversManager.objectsUpdatedEvent.disconnect(self)
        propReceiversManager.objectsAppendedEvent.disconnect(self)
        propReceiversManager.objectsRemovedEvent.disconnect(self)
    }
    
    fileprivate func apply(propReceivers: [PropReceiver]) {
        Log.high("apply(propReceivers: \(propReceivers.count))", from: self)
        
        self.propReceiversViewController.apply(propReceivers: propReceivers)
    
        if self.isViewLoaded {
            if self.propReceivers.count < Constants.propReceiversMinCount {
                self.showNoFriendsState()
            } else {
                if !self.propList.isEmpty {
                    self.hideEmptyState()
                }
            }
        }
    }
    
    fileprivate func apply(propList: [PropListItem]) {
        Log.high("apply(propList: \(propList.count))", from: self)
        
        let propReceiverUser = self.propReceivers.first?.user
        let propReceiverGender = propReceiverUser?.gender ?? .nonBinary
        let propReceiverCanSendCustomProp = propReceiverUser?.canSendCustomProp ?? false
        
        self.propPickerViewController.apply(propList: propList,
                                            gender: propReceiverGender,
                                            canSendCustomProp: propReceiverCanSendCustomProp)
        
        if self.isViewLoaded {
            if self.propList.isEmpty {
                self.showNoPropsState()
            } else {
                if self.propReceivers.count >= Constants.propReceiversMinCount {
                    self.hideEmptyState()
                }
            }
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupFont()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribeToPropReceiversEvents()
        
        let propReceivers = Services.cacheViewContext.propReceiversManager.fetch()
        
        if propReceivers.isEmpty {
            self.refreshPropReceivers()
        } else {
            self.apply(propReceivers: propReceivers)
            
            if self.propReceivers.count < Constants.propReceiversMinCount {
                self.refreshPropReceivers()
            }
        }
        
        let propList = Services.cacheViewContext.propListsManager.fetch(with: .special)
        
        if propList.isEmpty {
            self.refreshSpecialProps()
        } else {
            self.apply(propList: propList)
        }
        
        self.restartPropReceiversTimer()
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.unsubscribeFromPropRequestsEvents()
    }

    // swiftlint:disable cyclomatic_complexity
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.embedPropReceivers:
            guard let propReceiversViewController = segue.destination as? OutboxPropReceiversViewController else {
                fatalError()
            }
                
            self.propReceiversViewController = propReceiversViewController
            
            self.propReceiversViewController.onSkipPropReceiverButtonClicked = { [unowned self] in
                self.skipPropReceiver()
            }
            
            self.propReceiversViewController.onPropReceiverAvatarImageViewClicked = { [unowned self] user in
                self.performSegue(withIdentifier: Segues.showUser, sender: user)
            }
            
        case Segues.embedPropPicker:
            guard let propPickerViewController = segue.destination as? OutboxPropPickerViewController else {
                fatalError()
            }
            
            self.propPickerViewController = propPickerViewController
            
            self.propPickerViewController.onPropPicked = { [unowned self] propListItem, view in
                
                guard let propReceiver = self.propReceivers.first else {
                    return
                }
                
                let isFriendOrSuperFriend = propReceiver.user?.canSendCustomProp
                
                self.tabBarController?.performSegue(withIdentifier: Segues.showActionSheet, sender: (propListItem, view, isFriendOrSuperFriend))
            }
            
            self.propPickerViewController.onSearchTapped = { [unowned self] in
                guard let propReceiver = self.propReceivers.first else {
                    return
                }
                
                self.performSegue(withIdentifier: Segues.sendProp, sender: propReceiver)
            }
            
            self.propPickerViewController.onCustomPropPicked = { [unowned self] in
                guard let propReceiver = self.propReceivers.first else {
                    return
                }
                
                self.performSegue(withIdentifier: Segues.sendCustomProp, sender: propReceiver)
            }
            
        case Segues.showTutorial:
            if let tutorialViewController = segue.destination as? TutorialViewController {
                if !Services.accountSettings.firstTutorialCompleted {
                    tutorialViewController.apply(stage: .first)
                } else if !Services.accountSettings.secondTutorialCompleted {
                    tutorialViewController.apply(stage: .second)
                } else if !Services.accountSettings.thirdTutorialCompleted {
                    tutorialViewController.apply(stage: .third)
                }
            }
            
        case Segues.sendProp, Segues.sendCustomProp:
            guard let propReceiver = sender as? PropReceiver else {
                fatalError()
            }
                
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["propReceiver": propReceiver])
            }
            
        case Segues.showUser:
            guard let user = sender as? User else {
                fatalError()
            }
            
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["user": user])
            }
            
        default:
            break
        }
    }
}

// MARK: - DictionaryReceiver

extension OutboxViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let propListItem = dictionary["propListItem"] as? PropListItem else {
            return
        }
        let boostType = dictionary["boostType"] as? BoostType
        
        self.send(propListItem: propListItem, boostType: boostType)
    }
}
