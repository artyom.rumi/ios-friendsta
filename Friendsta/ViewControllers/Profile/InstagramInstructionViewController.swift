//
//  InstagramInstructionViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 18.03.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools

class InstagramInstructionViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var smallInformationView: UIView!
    @IBOutlet fileprivate weak var bigInformationView: UIView!
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onOpenInstagramTouchUpInside(_ sender: Any) {
        Log.high("onOpenInstagramTouchUpInside()", from: self)
        
        guard let instagramURL = URL(string: "instagram://app") else {
            return
        }
        
        if UIApplication.shared.canOpenURL(instagramURL) {
            UIApplication.shared.open(instagramURL, options: [:], completionHandler: nil)
        } else {
            self.showMessage(withTitle: "Oops".localized(), message: "Looks like Instagram is not installed".localized())
        }
    }
    
    @IBAction fileprivate func onCloseControllerTouchUpInside(_ sender: Any) {
        Log.high("onCloseControllerTouchUpInside()", from: self)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: -
    
    fileprivate func copyLink() {
        if let link = Services.accountUser?.link {
            UIPasteboard.general.string = link
        } else {
            firstly {
                Services.accountUserService.refreshAccountUser()
            }.done {[weak self]  accountUser in
                self?.copyLink()
            }.catch { [weak self] error in
                self?.showMessage(withError: error, okHandler: {
                    self?.copyLink()
                })
            }
        }
    }
    
    fileprivate func setupViews() {
        self.smallInformationView.layer.cornerRadius = 7.0
        self.bigInformationView.layer.cornerRadius = 10.0
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViews()
        self.copyLink()
    }
}
