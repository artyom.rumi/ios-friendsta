//
//  TextEditorViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 20.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class FeedbackViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let finishFeedback = "FinishFeedback"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var textView: UITextView!
    @IBOutlet fileprivate weak var placeholderLabel: UILabel!
    @IBOutlet fileprivate weak var sendButton: UIButton!
    @IBOutlet fileprivate weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction func onSendButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onSendButtonTouchUpInside()", from: self)
        
        self.view.endEditing(true)
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true) {
            self.sendFeedback(text: self.textView.text ?? "", loadingViewController: loadingViewController)
        }
    }
    
    // MARK: -
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func sendFeedback(text: String, loadingViewController: LoadingViewController) {
        Log.high("sendFeedback(text: \(text))", from: self)
        
        firstly {
            Services.feedbackService.sendFeedback(text: text)
        }.done {
            loadingViewController.dismiss(animated: true, completion: {
                self.showMessage(withTitle: "Thank you!".localized(),
                                 message: "Your feedback will be reviewed by our team very soon.".localized(),
                                 okHandler: {
                    self.performSegue(withIdentifier: Segues.finishFeedback, sender: self)
                })
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error)
            })
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.placeholderLabel.font = Fonts.regular(ofSize: 17.0)
        self.textView.font = Fonts.regular(ofSize: 17.0)
        self.sendButton.titleLabel?.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        
        self.placeholderLabel.isHidden = self.textView.hasText
        self.sendButton.isEnabled = self.textView.hasText
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.subscribeToKeyboardNotifications()

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.textView.becomeFirstResponder()

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        
        self.unsubscribeFromKeyboardNotifications()
    }
}

// MARK: - UITextViewDelegate

extension FeedbackViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textViewDidChange(_ textView: UITextView) {
        self.placeholderLabel.isHidden = self.textView.hasText
        self.sendButton.isEnabled = self.textView.hasText
    }
}

// MARK: - KeyboardHandler

extension FeedbackViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
