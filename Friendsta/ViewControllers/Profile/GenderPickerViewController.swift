//
//  GenderPickerViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 14.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class GenderPickerViewController: LoggedViewController {
    
    // MARK: - Instance Properties
    
    fileprivate weak var pickerView: UIPickerView!
    
    // MARK: -
    
    let genders: [Gender] = [.nonBinary, .male, .female]
    
    fileprivate(set) var gender: Gender = .nonBinary
    
    fileprivate(set) var hasAppliedData = false
    
    var onGenderPicked: ((_ gender: Gender) -> Void)?
    
    // MARK: - Initializers
    
    override init(nibName nibNameOrNil: String? = nil, bundle nibBundleOrNil: Bundle? = nil) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.initialize()
    }
    
    // MARK: - Instance Methods
    
    fileprivate func initialize() {
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .coverVertical
    }
    
    // MARK: -
    
    @objc fileprivate func onViewTapped(_ sender: UITapGestureRecognizer) {
        Log.high("onViewTapped()", from: self)
        
        if !self.view.point(inside: sender.location(in: self.pickerView), with: nil) {
            self.dismiss(animated: true)
        }
    }
    
    // MARK: -
    
    func apply(gender: Gender) {
        Log.high("apply(gender: \(gender))", from: self)
        
        self.gender = gender

        if self.isViewLoaded {
            self.pickerView.reloadAllComponents()
            
            if let genderIndex = self.genders.firstIndex(of: gender) {
                self.pickerView.selectRow(genderIndex, inComponent: 0, animated: true)
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onViewTapped)))
        
        let pickerView = UIPickerView()
        
        pickerView.backgroundColor = Colors.whiteBackground
        pickerView.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addSubview(pickerView)
        
        NSLayoutConstraint.activate([pickerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
                                     pickerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
                                     pickerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)])
        
        self.pickerView = pickerView
        
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(gender: self.gender)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
}

// MARK: - UIPickerViewDataSource

extension GenderPickerViewController: UIPickerViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.genders.count
    }
}

// MARK: - UIPickerViewDelegate

extension GenderPickerViewController: UIPickerViewDelegate {
    
    // MARK: - Instance Methods
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel()
        
        label.font = Fonts.regular(ofSize: 17.0)
        label.textAlignment = NSTextAlignment.center
        
        switch row {
        case 0:
            label.text = "Non binary".localized()
            
        case 1:
            label.text = "Boy".localized()
            
        case 2:
            label.text = "Girl".localized()
            
        default:
            fatalError()
        }
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.onGenderPicked?(self.genders[row])
    }
}
