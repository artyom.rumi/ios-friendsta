//
//  ManageAccountViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 20.03.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools

class ManageAccountViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    enum Segues {
        
        // MARK: - Type Properties
        
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: -
    
    enum Constants {
        
        // MARK: - Type Properties
        
        static let tableRowCount = 4
        
        static let firstSpacerTableCellRow = 0
        static let logoutTableCellRow = 1
        static let secondSpacerTableCellRow = 2
        static let deactivateAccountTableCellRow = 3
        
        static let spacerTableCell = "SpacerTableCell"
        static let logoutTableCell = "LogoutTableCell"
        static let deactivateAccountTableCell = "DeactivateAccountTableCell"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    // MARK: - Instance Methods
    
    fileprivate func logout() {
        let alertController = UIAlertController(title: nil,
                                                message: "Are you sure?".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: { action in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: {
                firstly {
                    Services.authorizationService.signOut()
                }.ensure {
                    loadingViewController.dismiss(animated: true, completion: {
                        Managers.userDefaultsManager.isUserHasSeenFriendsScreen = false
                        Managers.userDefaultsManager.isUserHasSeenCommunityScreen = false
                        
                        self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
                    })
                }.cauterize()
            })
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .default))
        
        self.present(alertController, animated: true)
    }
    
    fileprivate func sendDeactivateAccountEmailAndLogout() {
        Log.high("sendDeactivateAccountEmail()", from: self)
        
        guard let accountUserUID = Services.accountProvider.model.access?.userUID else {
            return
        }
        
        let alertController = UIAlertController(title: nil,
                                                message: "Are you sure?".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: { action in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: {
                firstly {
                    Services.feedbackService.sendFeedback(text: String(format: "Deactivate account with user id: %d.", accountUserUID))
                }.then {
                    Services.authorizationService.signOut()
                }.ensure {
                    loadingViewController.dismiss(animated: true, completion: {
                        self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
                    })
                }.catch { error in
                    loadingViewController.dismiss(animated: true, completion: {
                        self.showMessage(withError: error)
                    })
                }
            })
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .default))
        
        self.present(alertController, animated: true)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - UITableViewDataSource

extension ManageAccountViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.tableRowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        
        switch indexPath.row {
        case Constants.firstSpacerTableCellRow:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.spacerTableCell, for: indexPath)
        
        case Constants.logoutTableCellRow:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.logoutTableCell, for: indexPath)
            
        case Constants.secondSpacerTableCellRow:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.spacerTableCell, for: indexPath)
            
        case Constants.deactivateAccountTableCellRow:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.deactivateAccountTableCell, for: indexPath)
            
        default:
            fatalError()
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ManageAccountViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case Constants.logoutTableCellRow:
            self.logout()
            
        case Constants.deactivateAccountTableCellRow:
            self.sendDeactivateAccountEmailAndLogout()
            
        default:
            return
        }
    }
}
