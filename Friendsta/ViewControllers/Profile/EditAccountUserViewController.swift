//
//  EditAccountUserViewController.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 24/04/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class EditAccountUserViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let showAccountUserSchoolGrades = "ShowAccountUserSchoolGrades"
        static let showAccountUserSchools = "ShowAccountUserSchools"
        static let finishAccountUserEditing = "FinishAccountUserEditing"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - 
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let genderTableCellIdentifier = "GenderTableCell"
        static let schoolGradeTableCellIdentifier = "SchoolGradeTableCell"
        static let schoolTableCellIdentifier = "SchoolTableCell"
        static let bioTableCellIdentifier = "BioTableCell"
        
        static let infoSection = 0
        
        static let genderTableCellRow = 0
        static let schoolGradeTableCellRow = 1
        static let schoolTableCellRow = 2
        static let bioTableCellRow = 3
        
        static let tableSectionCount = 1
    }

    // MARK: - Instance Properties
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var tableHeaderView: UIView!
    
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var firstNameTextField: UITextField!
    @IBOutlet private weak var lastNameTextField: UITextField!
    @IBOutlet private weak var avatarActivityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet private weak var doneBarButtonItem: UIBarButtonItem!

    // MARK: -
    
    private var accountUserBuffer = AccountUserBuffer()
    
    // MARK: -
    
    private(set) var accountUser: AccountUser?
    
    private(set) var isAvatarChanged = false
    private(set) var isAvatarRemoved = false
    
    private(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onAccountUserSchoolGradeSelected(segue: UIStoryboardSegue) {
        Log.high("onAccountUserSchoolGradeSelected(withSegue: \(String(describing: segue.identifier)))", from: self)
        
        guard let accountUser = self.accountUser else {
            return
        }
        
        if self.accountUserBuffer.classYear != Int(accountUser.classYear) {
            self.accountUserBuffer.schoolUID = nil
            self.accountUserBuffer.schoolTitle = nil
            self.accountUserBuffer.schoolLocation = nil
        } else {
            self.accountUserBuffer.schoolUID = accountUser.schoolUID
            self.accountUserBuffer.schoolTitle = accountUser.schoolTitle
            self.accountUserBuffer.schoolLocation = accountUser.schoolLocation
        }
        
        self.tableView.reloadRows(at: [IndexPath(row: Constants.schoolGradeTableCellRow, section: 0), IndexPath(row: Constants.schoolTableCellRow, section: 0)], with: .none)
        
        self.updateDoneButton()
    }
    
    @IBAction private func onAccountUserSchoolSelected(segue: UIStoryboardSegue) {
        Log.high("onAccountUserSchoolSelected(withSegue: \(String(describing: segue.identifier)))", from: self)
        
        self.tableView.reloadRows(at: [IndexPath(row: Constants.schoolTableCellRow, section: 0)], with: .none)
        
        self.updateDoneButton()
    }
    
    @IBAction private func onAccountUserGenderChoosed(segue: UIStoryboardSegue) {
        Log.high("onAccountUserGenderChoosed(withSegue: \(String(describing: segue.identifier)))", from: self)
        
        self.tableView.reloadRows(at: [IndexPath(row: Constants.genderTableCellRow, section: 0)], with: .none)
        
        self.updateDoneButton()
    }
    
    @IBAction private func onViewTapped(_ sender: Any) {
        Log.high("onViewTapped()", from: self)
        
        self.view.endEditing(true)
    }
    
    @IBAction private func onUploadPhotoButtonTouchUpInside(_ sender: Any) {
        Log.high("onUploadPhotoButtonTouchUpInside()", from: self)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "Take Photo".localized(), style: .default, handler: { action in
            let imagePickerController = ImagePickerController()
            
            imagePickerController.modalTransitionStyle = .coverVertical
            imagePickerController.modalPresentationStyle = .overFullScreen
            
            imagePickerController.onImagePicked = { [unowned self, unowned imagePickerController] image in
                imagePickerController.dismiss(animated: true)
                
                self.avatarImageView.image = image
                
                self.isAvatarChanged = true
                self.isAvatarRemoved = false
                
                self.updateDoneButton()
            }
            
            imagePickerController.apply(mediaSource: .camera, allowsEditing: true)
            
            self.present(imagePickerController, animated: true)
        }))
        
        alertController.addAction(UIAlertAction(title: "Choose From Library".localized(), style: .default, handler: { action in
            let imagePickerController = ImagePickerController()
            
            imagePickerController.modalTransitionStyle = .coverVertical
            imagePickerController.modalPresentationStyle = .overFullScreen
            
            imagePickerController.onImagePicked = { [unowned self, unowned imagePickerController] image in
                imagePickerController.dismiss(animated: true)
                
                self.avatarImageView.image = image
                
                self.isAvatarChanged = true
                self.isAvatarRemoved = false
                
                self.updateDoneButton()
            }
            
            imagePickerController.apply(mediaSource: .photoLibrary, allowsEditing: true)
            
            self.present(imagePickerController, animated: true)
        }))
        
        if !self.isAvatarRemoved {
            alertController.addAction(UIAlertAction(title: "Delete Photo".localized(), style: .default, handler: { action in
                if let avatarURL = self.accountUser?.largeAvatarPlaceholderURL {
                    self.loadAvatarImage(from: avatarURL)
                } else {
                    self.avatarImageView.image = #imageLiteral(resourceName: "AvatarLargePlaceholder")
                }
                
                self.isAvatarChanged = true
                self.isAvatarRemoved = true
                
                self.updateDoneButton()
            }))
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
        
        self.present(alertController, animated: true)
    }
    
    @IBAction private func onDoneButtonTouchUpInside(_ sender: Any) {
        Log.high("onDoneButtonTouchUpInside()", from: self)
        
        self.view.endEditing(true)
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            self.updateAccountUser(loadingViewController: loadingViewController)
        })
    }
    
    // MARK: -
    
    private func updateDoneButton() {
        guard let accountUser = self.accountUser else {
            return
        }
        
        self.doneBarButtonItem.isEnabled = false
        
        guard self.accountUserBuffer.schoolUID != nil else {
            return
        }
        
        if self.isAvatarChanged {
            self.doneBarButtonItem.isEnabled = true
        } else if self.accountUserBuffer.firstName != accountUser.firstName {
            self.doneBarButtonItem.isEnabled = true
        } else if self.accountUserBuffer.lastName != accountUser.lastName {
            self.doneBarButtonItem.isEnabled = true
        } else if self.accountUserBuffer.gender != accountUser.gender {
            self.doneBarButtonItem.isEnabled = true
        } else if self.accountUserBuffer.classYear != Int(accountUser.classYear) {
            self.doneBarButtonItem.isEnabled = true
        } else if self.accountUserBuffer.schoolUID != accountUser.schoolUID {
            self.doneBarButtonItem.isEnabled = true
        } else if self.accountUserBuffer.bio != accountUser.bio {
            self.doneBarButtonItem.isEnabled = true
        } else if self.accountUserBuffer.hideTopPokesList != accountUser.isHideTopPokesEnabled {
            self.doneBarButtonItem.isEnabled = true
        }
    }
    
    private func loadAvatarImage(from imageURL: URL) {
        self.avatarActivityIndicatorView.startAnimating()
        
        self.avatarImageView.image = #imageLiteral(resourceName: "AvatarLargePlaceholder")

        Services.imageLoader.loadImage(for: imageURL, in: self.avatarImageView, placeholder: #imageLiteral(resourceName: "AvatarLargePlaceholder"), completionHandler: { image in
            self.avatarActivityIndicatorView.stopAnimating()
        })
    }
    
    private func config(genderTableCell cell: EditAccountUserTableViewCell) {
        switch self.accountUserBuffer.gender {
        case .some(.male):
            cell.title = "Boy".localized()
            
        case .some(.female):
            cell.title = "Girl".localized()
            
        case .some(.nonBinary), .none:
            cell.title = "Non-binary".localized()
        }
    }
    
    private func config(schoolGradeTableCell cell: EditAccountUserTableViewCell) {
        cell.title = nil
        
        if let classYear = self.accountUserBuffer.classYear {
            if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(classYear)) {
                cell.title = String(format: "%d grade".localized(), schoolGradeNumber)
            } else {
                cell.title = String(format: "Already graduated %d".localized(), classYear)
            }
        }
    }
    
    private func config(schoolTableCell cell: EditAccountUserTableViewCell) {
        cell.title = self.accountUserBuffer.schoolTitle
    }
    
    private func config(bioTableCell cell: EditAccountUserBioTableViewCell) {
        cell.bio = self.accountUserBuffer.bio
        
        cell.onBioChanged = { [unowned self] bio in
            self.accountUserBuffer.bio = bio
            
            self.updateDoneButton()
        }
        
        cell.onPreferredHeightChanged = { [unowned self] in
            UIView.setAnimationsEnabled(false)
            
            self.tableView.beginUpdates()
            self.tableView.endUpdates()
            
            let contentOffset = self.tableView.contentSize.height - self.tableView.frame.size.height
            
            if contentOffset > 0 {
                self.tableView.setContentOffset(CGPoint(x: 0, y: contentOffset), animated: false)
            }
            
            UIView.setAnimationsEnabled(true)
        }
    }

    private func layoutTableHeaderView() {
        let tableViewHeight = self.tableHeaderView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        
        self.tableHeaderView.frame = CGRect(x: 0,
                                            y: 0,
                                            width: self.tableView.bounds.width,
                                            height: tableViewHeight)
        
        self.tableView.tableHeaderView = self.tableHeaderView
    }
    
    // MARK: -
    
    private func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    private func uploadAccountUser(avatarImage: UIImage, loadingViewController: LoadingViewController) {
        Log.high("uploadAccountUser(avatarImage: \(avatarImage.size))", from: self)
        
        DispatchQueue.global(qos: .userInitiated).async(execute: {
            let avatarData: Data?
            
            if avatarImage.size.width > Limits.avatarImageMaxWidth {
                if let avatarImage = avatarImage.scaled(to: Limits.avatarImageMaxWidth) {
                    avatarData = avatarImage.jpegData(compressionQuality: Limits.avatarImageQuality)
                } else {
                    avatarData = avatarImage.jpegData(compressionQuality: Limits.avatarImageQuality)
                }
            } else {
                avatarData = avatarImage.jpegData(compressionQuality: Limits.avatarImageQuality)
            }
            
            DispatchQueue.main.async(execute: {
                if let avatarData = avatarData {
                    firstly {
                        Services.accountUserService.uploadAccountUserAvatar(with: avatarData)
                    }.done { accountUser in
                        loadingViewController.dismiss(animated: true, completion: {
                            NotificationCenter.default.post(name: .profileAvatarUpdated, object: nil)
                            
                            self.performSegue(withIdentifier: Segues.finishAccountUserEditing, sender: nil)
                        })
                    }.catch { error in
                        loadingViewController.dismiss(animated: true, completion: {
                            self.handle(actionError: error)
                        })
                    }
                } else {
                    loadingViewController.dismiss(animated: true, completion: {
                        self.showMessage(withTitle: "Invalid image".localized(),
                                         message: "Please choose another image".localized())
                    })
                }
            })
        })
    }
    
    private func deleteAccountUserAvatar(loadingViewController: LoadingViewController) {
        Log.high("deleteAccountUserAvatar()", from: self)
        
        firstly {
            Services.accountUserService.deleteAccountUserAvatar()
        }.done { accountUser in
            loadingViewController.dismiss(animated: true, completion: {
                NotificationCenter.default.post(name: .profileAvatarUpdated, object: nil)
                
                self.performSegue(withIdentifier: Segues.finishAccountUserEditing, sender: nil)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error)
            })
        }
    }
    
    private func updateAccountUser(loadingViewController: LoadingViewController) {
        Log.high("updateAccountUser()", from: self)
        
        guard let accountUser = self.accountUser else {
            return
        }
        
        firstly {
            Services.accountUserService.updateAccountUser(from: self.accountUserBuffer, uid: accountUser.uid)
        }.done { accountUser in
            if self.isAvatarChanged {
                if self.isAvatarRemoved {
                    self.deleteAccountUserAvatar(loadingViewController: loadingViewController)
                } else {
                    guard let avatarImage = self.avatarImageView.image else {
                        return
                    }
                    
                    self.uploadAccountUser(avatarImage: avatarImage, loadingViewController: loadingViewController)
                }
            } else {
                loadingViewController.dismiss(animated: true, completion: {
                    self.performSegue(withIdentifier: Segues.finishAccountUserEditing, sender: nil)
                })
            }
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error)
            })
        }
    }
    
    // MARK: -
    
    func apply(accountUser: AccountUser) {
        Log.high("apply(accountUser: \(String(describing: accountUser.fullName)))", from: self)
        
        self.accountUser = accountUser
        
        self.accountUserBuffer.firstName = accountUser.firstName
        self.accountUserBuffer.lastName = accountUser.lastName
        self.accountUserBuffer.age = Int(accountUser.age)
        
        self.accountUserBuffer.classYear = Int(accountUser.classYear)
        
        self.accountUserBuffer.schoolUID = accountUser.schoolUID
        self.accountUserBuffer.schoolTitle = accountUser.schoolTitle
        self.accountUserBuffer.schoolLocation = accountUser.schoolLocation
        
        self.accountUserBuffer.gender = accountUser.gender
        
        self.accountUserBuffer.bio = accountUser.bio
        
        self.accountUserBuffer.hideTopPokesList = accountUser.isHideTopPokesEnabled
        
        self.isAvatarChanged = false
        self.isAvatarRemoved = (accountUser.avatarURL == nil)
        
        if self.isViewLoaded {
            self.updateDoneButton()
            
            if let avatarURL = accountUser.largeAvatarURL ?? accountUser.largeAvatarPlaceholderURL {
                self.loadAvatarImage(from: avatarURL)
            } else {
                self.avatarImageView.image = #imageLiteral(resourceName: "AvatarLargePlaceholder")
            }
            
            self.firstNameTextField.text = self.accountUserBuffer.firstName
            self.lastNameTextField.text = self.accountUserBuffer.lastName
            
            if self.isViewAppeared {
                self.tableView.beginUpdates()
                self.tableView.deleteSections(IndexSet(integersIn: 0..<self.tableView.numberOfSections), with: .fade)
                self.tableView.insertSections(IndexSet(integersIn: 0..<Constants.tableSectionCount), with: .fade)
                self.tableView.endUpdates()
            } else {
                self.tableView.reloadData()
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    private func setupFont() {
        self.firstNameTextField.font = Fonts.regular(ofSize: 17.0)
        self.lastNameTextField.font = Fonts.regular(ofSize: 17.0)
    }

    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let accountUser = self.accountUser, !self.hasAppliedData {
            self.apply(accountUser: accountUser)
        }
        
        self.subscribeToKeyboardNotifications()

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unsubscribeFromKeyboardNotifications()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.layoutTableHeaderView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.showAccountUserSchoolGrades, Segues.showAccountUserSchools:
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["accountUserBuffer": self.accountUserBuffer])
            }
            
        default:
            break
        }
    }
}

// MARK: - UITableViewDataSource

extension EditAccountUserViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.tableSectionCount
    }
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Constants.infoSection:
            return (self.accountUser == nil) ? 0 : 4
        
        default:
            fatalError()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch indexPath.section {
        case Constants.infoSection:
            switch indexPath.row {
            case Constants.genderTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.genderTableCellIdentifier, for: indexPath)
                
                self.config(genderTableCell: cell as! EditAccountUserTableViewCell)
                
            case Constants.schoolGradeTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolGradeTableCellIdentifier, for: indexPath)
                
                self.config(schoolGradeTableCell: cell as! EditAccountUserTableViewCell)
                
            case Constants.schoolTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.schoolTableCellIdentifier, for: indexPath)
                
                self.config(schoolTableCell: cell as! EditAccountUserTableViewCell)
                
            case Constants.bioTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.bioTableCellIdentifier, for: indexPath)
                
                self.config(bioTableCell: cell as! EditAccountUserBioTableViewCell)
                
            default:
                fatalError()
            }
            
        default:
            fatalError()
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension EditAccountUserViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case Constants.genderTableCellRow:
            let pickerView = GenderPickerViewController()
            
            if let gender = self.accountUserBuffer.gender {
                pickerView.apply(gender: gender)
            }
            
            pickerView.onGenderPicked = { gender in
                self.accountUserBuffer.gender = gender
                
                self.tableView.reloadRows(at: [IndexPath(row: Constants.genderTableCellRow, section: 0)], with: .none)
                
                self.updateDoneButton()
            }
            
            self.present(pickerView, animated: true)
            
        case Constants.schoolGradeTableCellRow, Constants.schoolTableCellRow, Constants.bioTableCellRow:
            break
            
        default:
            fatalError()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case Constants.genderTableCellRow, Constants.schoolGradeTableCellRow, Constants.schoolTableCellRow:
            break
            
        case Constants.bioTableCellRow:
            return UITableView.automaticDimension
            
        default:
            fatalError()
        }
        
        return 44.0
    }
}

// MARK: - UITextFieldDelegate

extension EditAccountUserViewController: UITextFieldDelegate {
    
    // MARK: - Instance Methods
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let replacedText: String
        
        if let text = textField.text, let range = Range(range, in: text) {
            replacedText = text.replacingCharacters(in: range, with: string)
        } else {
            replacedText = string
        }
        
        switch textField {
        case self.firstNameTextField:
            self.accountUserBuffer.firstName = replacedText
        
        case self.self.lastNameTextField:
            self.accountUserBuffer.lastName = replacedText
            
        default:
            fatalError()
        }
        
        self.updateDoneButton()
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.firstNameTextField:
            self.lastNameTextField.becomeFirstResponder()
            
        case self.lastNameTextField:
            self.lastNameTextField.resignFirstResponder()
            
        default:
            fatalError()
        }
        
        return false
    }
}

// MARK: - KeyboardScrollableHandler

extension EditAccountUserViewController: KeyboardScrollableHandler {
    
    // MARK: - Instance Properties
    
    var scrollableView: UITableView {
        return self.tableView
    }
}

// MARK: - DictionaryReceiver

extension EditAccountUserViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let accountUser = dictionary["accountUser"] as? AccountUser else {
            return
        }
        
        self.apply(accountUser: accountUser)
    }
}
