//
//  AccountProfileViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 06.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class AccountProfileViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
        
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let showEditAccountUser = "ShowEditAccountUser"
        static let showMakePhoto = "ShowMakePhoto"
        static let showFeedPreview = "ShowFeedPreview"
        static let showConnections = "ShowConnections"
        
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: -
    
    private enum Identifiers {
        
        // MARK: - Type Properties
        
        static let accountProfileInfoCell = "AccountProfileInfoCell"
        static let addFeedCell = "AddFeedCell"
        static let feedImageCell = "FeedImageCell"
        static let feedLinkCell = "FeedLinkCell"
        static let emptyStateCell = "EmptyStateCell"
        
        static let segmentedControlReusableView = "SegmentedControlReusableView"
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let numberOfSections = 2
        
        static let profileInfoSection = 0
        static let feedsSection = 1
        
        static let numberOfItemsInProfileSection = 1
        
        static let feedsSectionHeaderHeight: CGFloat = 50
        static let feedImageCellHeight: CGFloat = 128
        
        static let addFeedCellRowIndex = 0
        
        static let countOfSpacingBetweenItems: CGFloat = 3
        
        static let countOfEmptyStateCells = 1
        static let countOfAddFeedCells = 1
        
        static let timelineLoadingMessage = "We are updating your timeline... Please wait a bit...".localized()
        static let anonymousLoadingMessage = "We are updating your anonymous posts... Please wait a bit...".localized()
        static let anonymousNoDataMessage = "Your anonymous posts will appear here. Click the plus icon below to share anonymously with your friends.".localized()
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var collectionViewFlowLayout: UICollectionViewFlowLayout!
    
    @IBOutlet private weak var emptyStateView: EmptyStateView!
    
    private weak var collectionRefreshControl: UIRefreshControl!
    
    // MARK: -
    
    private var accountUser: AccountUser?
    
    private var isRefreshingData = false
    
    private var feedListType: FeedListType = .timeline
    private var feeds: [Feed] = []
    
    private var shouldApplyData = true
    
    private var isAnonymousFeedEmpty: Bool {
        return self.feedListType == .anonymous && self.feeds.isEmpty
    }
    
    private var isInitialRefreshingData: Bool {
        return self.isRefreshingData && self.feeds.isEmpty
    }
    
    // MARK: -

    deinit {
        self.unsubscribeFromFeedEvents()
    }
        
    // MARK: - Instance Methods
    
    @IBAction private func onAccountUserEditingFinished(segue: UIStoryboardSegue) {
        Log.high("onAccountUserEditingFinished()", from: self)
        
        self.refreshVisibleContent()
    }
    
    @objc private func onCollectionRefreshControlRequested(_ refreshControl: UIRefreshControl) {
        Log.high("onTableRefreshControlRequested()", from: self)
        
        self.refreshVisibleContent()
    }
    
    @objc private func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.refreshVisibleContent()
    }
    
    @IBAction private func onPhotoPostingFinished(segue: UIStoryboardSegue) {
        Log.high("onPhotoPostingFinished(withSegue: \(String(describing: segue.identifier)))", from: self)

        self.refreshVisibleContent()
    }
    
    // MARK: -
    
    private func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()
        
        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action
        
        self.emptyStateView.isHidden = false
    }
    
    private func hideEmptyState() {
        self.emptyStateView.isHidden = true
    }
    
    private func showLoadingState() {
        if self.emptyStateView.isHidden {
            self.showEmptyState(title: "Updating profile".localized(),
                                message: "We are updating your profile.\nPlease wait a bit.".localized())
        }
        
        self.emptyStateView.showActivityIndicator()
    }
    
    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action: EmptyStateAction?
        
        if let retryHandler = retryHandler {
            action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: retryHandler)
        } else {
            action = nil
        }
        
        switch error as? WebError {
        case .some(.unauthorized):
            self.shouldApplyData = true
            
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        case .some(.connection), .some(.timeOut):
            if self.accountUser == nil {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            }
            
        default:
            if self.accountUser == nil {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        }
    }
    
    // MARK: -
    
    private func apply(accountUser: AccountUser, feedListType: FeedListType) {
        Log.high("apply(accountUser: \(accountUser.uid), feedListType: \(feedListType.rawValue)", from: self)
        
        self.accountUser = accountUser
        self.feedListType = feedListType
        
        let feedList = Services.cacheViewContext.feedListManager.firstOrNew(withListType: feedListType)
        
        self.feeds = feedList.allFeeds
        
        self.hideEmptyState()
        
        self.collectionView.reloadData()
        
        self.shouldApplyData = false
    }
    
    private func refreshVisibleContent() {
        switch self.feedListType {
        case .anonymous:
            self.refreshAnonymousContent()
            
        case .timeline:
            self.refreshTimelineContent()
            
        case .all, .discover, .unknown:
            fatalError("Unexpected feedListType.")
        }
    }
    
    private func refreshTimelineContent() {
        Log.high("refreshTimelineContent()", from: self)
        
        self.isRefreshingData = true
        
        if !self.collectionRefreshControl.isRefreshing {
            if (self.accountUser == nil) || (!self.emptyStateView.isHidden) {
                self.showLoadingState()
            }
        }
        
        firstly {
            when(fulfilled: Services.accountUserService.refreshAccountUser(),
                 Services.feedService.fetchTimelineFeeds())
        }.ensure {
            if self.collectionRefreshControl.isRefreshing {
                self.collectionRefreshControl.endRefreshing()
            }

            self.isRefreshingData = false
        }.done { result in
            let (accountUser, _) = result
            
            self.apply(accountUser: accountUser, feedListType: .timeline)
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshVisibleContent()
            })
        }
    }
    
    private func refreshAnonymousContent() {
        Log.high("refreshAnonymousContent()", from: self)
        
        self.isRefreshingData = true
        
        if !self.collectionRefreshControl.isRefreshing {
            if (self.accountUser == nil) || (!self.emptyStateView.isHidden) {
                self.showLoadingState()
            }
        }
        
        firstly {
            when(fulfilled: Services.accountUserService.refreshAccountUser(),
                 Services.feedService.fetchAnonymousFeeds())
        }.ensure {
            if self.collectionRefreshControl.isRefreshing {
                self.collectionRefreshControl.endRefreshing()
            }

            self.isRefreshingData = false
        }.done { result in
            let (accountUser, _) = result
            
            self.apply(accountUser: accountUser, feedListType: .anonymous)
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshVisibleContent()
            })
        }
    }

    // MARK: -

    private func subscribeToFeedEvents() {
        self.unsubscribeFromFeedEvents()

        let feedManager = Services.cacheViewContext.feedManager

        feedManager.objectsChangedEvent.connect(self, handler: { [weak self] feeds in
            self?.shouldApplyData = true
        })

        feedManager.startObserving()
    }

    private func unsubscribeFromFeedEvents() {
        Services.cacheViewContext.feedManager.objectsChangedEvent.disconnect(self)
    }
    
    // MARK: -
    
    private func configureCollectionViewFlowLayout() {
        let spacing = self.collectionViewFlowLayout.sectionInset.left + self.collectionViewFlowLayout.sectionInset.right
        
        let width = UIScreen.main.bounds.size.width - spacing
        
        self.collectionViewFlowLayout.estimatedItemSize = CGSize(width: width, height: 1)
    }
    
    private func configureNavigationBar() {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.shadowImage = nil
    }
    
    private func configureRefreshControl() {
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self,
                                 action: #selector(self.onCollectionRefreshControlRequested(_:)),
                                 for: .valueChanged)
        
        self.collectionRefreshControl = refreshControl
        self.collectionView.refreshControl = collectionRefreshControl
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureCollectionViewFlowLayout()
        self.configureRefreshControl()
        
        self.subscribeToFeedEvents()
        
        self.shouldApplyData = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureNavigationBar()
        
        self.isRefreshingData = false

        if self.shouldApplyData {
            if let accountUser = Services.accountUser {
                self.apply(accountUser: accountUser, feedListType: self.feedListType)
            }
            
            self.refreshVisibleContent()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {
        case Segues.showEditAccountUser:
            guard let accountUser = sender as? AccountUser else {
                return
            }
            
            dictionaryReceiver?.apply(dictionary: ["accountUser": accountUser])
            
        case Segues.showMakePhoto:
            dictionaryReceiver?.apply(dictionary: ["source": PhotoCreationSource.profile])
            
        case Segues.showFeedPreview:
            guard let feed = sender as? Feed else {
                return
            }
            
            dictionaryReceiver?.apply(dictionary: ["feed": feed])
            
        case Segues.showConnections:
            dictionaryReceiver?.apply(dictionary: ["state": ConnectionsState.keeping])
        
        default:
            break
        }
    }
}

// MARK: - UICollectionViewDataSource

extension AccountProfileViewController: UICollectionViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Constants.numberOfSections
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case Constants.profileInfoSection:
            return Constants.numberOfItemsInProfileSection
            
        case Constants.feedsSection:
            if self.isInitialRefreshingData {
                return Constants.countOfEmptyStateCells
            } else {
                switch self.feedListType {
                case .timeline:
                    return Constants.countOfAddFeedCells + self.feeds.count
                    
                case .anonymous:
                    return self.feeds.isEmpty ? Constants.countOfEmptyStateCells : self.feeds.count
                    
                case .all, .discover, .unknown:
                    fatalError("Unexpected feedListType.")
                }
            }
            
        default:
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case Constants.profileInfoSection:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.accountProfileInfoCell, for: indexPath)
            
            self.configure(accountProfileInfoCell: cell as! AccountProfileInfoCollectionViewCell)
            
            return cell
            
        case Constants.feedsSection:
            if self.isInitialRefreshingData {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.emptyStateCell, for: indexPath)
                
                self.configure(emptyStateCell: cell as! EmptyStateCollectionViewCell)
                
                return cell
            } else if self.feedListType == .timeline && indexPath.row == Constants.addFeedCellRowIndex {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.addFeedCell, for: indexPath)
                
                return cell
            } else if self.isAnonymousFeedEmpty {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.emptyStateCell, for: indexPath)
                
                self.configure(emptyStateCell: cell as! EmptyStateCollectionViewCell)
                
                return cell
            } else {
                let feed = (self.feedListType == .timeline) ? self.feeds[indexPath.row - Constants.countOfAddFeedCells] : self.feeds[indexPath.row]
                
                if feed.link != nil {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.feedLinkCell, for: indexPath)
                                        
                    return cell
                } else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.feedImageCell, for: indexPath)

                    return cell
                }
            }
            
        default:
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionHeader {
            let reusableView = self.collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Identifiers.segmentedControlReusableView, for: indexPath)
            
            self.configure(segmentedControlReusableView: reusableView as! SegmentedControlCollectionReusableView)

            return reusableView
        } else {
            fatalError()
        }
    }
}

// MARK: - Cell and View Configurations

extension AccountProfileViewController {
    
    // MARK: - Instance Methods
    
    private func configure(emptyStateCell: EmptyStateCollectionViewCell) {
        switch self.feedListType {
        case .anonymous:
            if self.isRefreshingData {
                emptyStateCell.message = Constants.anonymousLoadingMessage
                emptyStateCell.isActivityIndicatorHidden = false
            } else {
                emptyStateCell.message = Constants.anonymousNoDataMessage
                emptyStateCell.isActivityIndicatorHidden = true
            }
            
        case .timeline:
            if self.isRefreshingData {
                emptyStateCell.message = Constants.timelineLoadingMessage
                emptyStateCell.isActivityIndicatorHidden = false
            }
            
        case .all, .discover, .unknown:
            fatalError("Unexpected feedListType.")
        }
    }
    
    private func configure(accountProfileInfoCell: AccountProfileInfoCollectionViewCell) {
        guard let accountUser = self.accountUser else {
            return
        }
        
        accountProfileInfoCell.fullName = accountUser.fullName
        
        let schoolTitle = accountUser.schoolTitle ?? "unknown school".localized()
        
        if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(accountUser.classYear)) {
            accountProfileInfoCell.schoolInfo = String(format: "%dth grade %@".localized(), schoolGradeNumber, schoolTitle)
        } else {
            accountProfileInfoCell.schoolInfo = String(format: "Finished %@".localized(), schoolTitle)
        }
        
        let friendsCountText = NSMutableAttributedString(string: "\(accountUser.friendsCount)", attributes: [.font: Fonts.bold(ofSize: 15.0)])
        let friendsText = NSAttributedString(string: accountUser.friendsCount == 1 ? " friend".localized() : " friends".localized(), attributes: [.font: Fonts.regular(ofSize: 15.0)])
        
        friendsCountText.append(friendsText)
        
        accountProfileInfoCell.friendsCountAttributedText = friendsCountText
        
        accountProfileInfoCell.bioInfo = accountUser.bio
        
        accountProfileInfoCell.onEditButtonTapped = { [unowned self] in
            self.performSegue(withIdentifier: Segues.showEditAccountUser, sender: self.accountUser)
        }
        
        accountProfileInfoCell.onFriendsCountTapped = { [unowned self] in
            self.performSegue(withIdentifier: Segues.showConnections, sender: self.accountUser)
        }
    }
    
    private func configure(segmentedControlReusableView: SegmentedControlCollectionReusableView) {
        guard let accountUser = self.accountUser else {
            fatalError()
        }
        
        segmentedControlReusableView.onAnonymousSelected = { [unowned self] in
            self.apply(accountUser: accountUser, feedListType: .anonymous)
            
            self.refreshAnonymousContent()
        }
        
        segmentedControlReusableView.onTimelineSelected = { [unowned self] in
            self.apply(accountUser: accountUser, feedListType: .timeline)
            
            self.refreshTimelineContent()
        }
    }
}

// MARK: - UICollectionViewDelegate

extension AccountProfileViewController: UICollectionViewDelegateFlowLayout {
    
    // MARK: - Instance Methods
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        switch indexPath.section {
        case Constants.feedsSection:
            switch self.feedListType {
            case .timeline:
                if indexPath.row == Constants.addFeedCellRowIndex {
                    self.performSegue(withIdentifier: Segues.showMakePhoto, sender: self)
                } else {
                    let feed = self.feeds[indexPath.row - Constants.countOfAddFeedCells]
                    
                    self.performSegue(withIdentifier: Segues.showFeedPreview, sender: feed)
                }
                
            case .anonymous:
                let feed = self.feeds[indexPath.row]
                
                self.performSegue(withIdentifier: Segues.showFeedPreview, sender: feed)
                
            case .all, .discover, .unknown:
                fatalError()
            }
            
        default:
            return
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? AccountProfileInfoCollectionViewCell {
            if let imageURL = self.accountUser?.largeAvatarURL ?? self.accountUser?.largeAvatarPlaceholderURL {
                cell.showActivityIndicator()

                Services.imageLoader.loadImage(for: imageURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarLargePlaceholder, completionHandler: { image in
                    cell.hideActivityIndicator()
                })
            }
        } else if let cell = cell as? FeedImageCollectionViewCell {
            let imageURL: URL?
            
            switch self.feedListType {
            case .timeline:
                let feed = self.feeds[indexPath.row - Constants.countOfAddFeedCells]
                
                imageURL = feed.originalImageURL
                
            case .anonymous:
                let feed = self.feeds[indexPath.row]
                
                imageURL = feed.originalImageURL
                
            case .all, .discover, .unknown:
                fatalError("Unexpected feedListType.")
            }
            
            if let url = imageURL {
                Services.imageLoader.loadImageProgressive(for: url, in: cell.feedImageViewTarget)
            }
        } else if let cell = cell as? FeedLinkProfileCollectionViewCell {
            let feed = (self.feedListType == .timeline) ? self.feeds[indexPath.row - Constants.countOfAddFeedCells] : self.feeds[indexPath.row]
            
            guard let link = feed.link else {
                return
            }
            
            guard let linkURL = URL(string: link) else {
                return
            }
                                    
            Managers.linkPreviewManager.getInformationFrom(link: linkURL, completion: { [weak cell] _, linkDescription, _, error in
                cell?.linkDescription = linkDescription
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? AccountProfileInfoCollectionViewCell {
            Services.imageLoader.cancelLoading(in: cell.avatarImageViewTarget)
        } else if let cell = cell as? FeedImageCollectionViewCell {
            Services.imageLoader.cancelLoading(in: cell.feedImageViewTarget)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch section {
        case Constants.profileInfoSection:
            return CGSize.zero
            
        case Constants.feedsSection:
            return CGSize(width: UIScreen.main.bounds.size.width, height: Constants.feedsSectionHeaderHeight)
            
        default:
            fatalError()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.isInitialRefreshingData {
            return self.collectionViewFlowLayout.estimatedItemSize
        } else {
            switch indexPath.section {
            case Constants.profileInfoSection:
                return self.collectionViewFlowLayout.estimatedItemSize

            case Constants.feedsSection:
                if self.isAnonymousFeedEmpty {
                    return self.collectionViewFlowLayout.estimatedItemSize
                } else {
                    let betweenItems = Constants.countOfSpacingBetweenItems * self.collectionViewFlowLayout.minimumInteritemSpacing
                    let leading = self.collectionViewFlowLayout.sectionInset.left
                    let trailing = self.collectionViewFlowLayout.sectionInset.right

                    let spacing = leading + betweenItems + trailing

                    let feedCellWidth = ((UIScreen.main.bounds.size.width - spacing) / 4).rounded(.down)

                    return CGSize(width: feedCellWidth, height: Constants.feedImageCellHeight)
                }

            default:
                fatalError()
            }
        }
    }
}
