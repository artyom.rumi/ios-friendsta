//
//  ProfileSettingsViewController.swift
//  Friendsta
//
//  Created by Дамир Зарипов on 21/03/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class ProfileSettingsViewController: LoggedViewController {
    
    // MARK: -
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let generalSectionHeaderIdentifier = "GeneralSectionHeader"
        static let sumbitPokeTableCellIdentifier = "SumbitPokeTableCell"
        static let getHelpTableCellIdentifier = "GetHelpTableCell"
        static let giveFeedbackTableCellIdentifier = "GiveFeedbackTableCell"
        static let faqTableCellIdentifier = "FAQTableCell"
        static let rateUsTableCellIdentifier = "RateUsTableCell"
        static let manageAccountSectionHeaderIdentifier = "ManageAccountSectionHeader"
        static let manageAccountTableCellIdentifier = "ManageAccountTableCell"
        
        static let generalSectionHeaderHeight: CGFloat = 30.0
        static let manageAccountSectionHeaderHeight: CGFloat = 35.0
        
        static let generalTableSection = 0
       
        static let sumbitPokeTableCellRow = 0
        static let getHelpTableCellRow = 1
        static let giveFeedbackTableCellRow = 2
        static let faqTableCellRow = 3
        static let rateUsTableCellRow = 4
        static let emptyTableCell2Row = 5
        
        static let manageAccountTableSection = 1
        
        static let manageAccountTableCellRow = 0
        
        static let tableRowCount = 8
        
        static let tableSectionCount = 2
    }
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let propseNote = "PropseNote"
        static let showManageAccount = "ShowManageAccount"
    }
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    // MARK: 
    
    @IBAction fileprivate func onProposePropFinished(segue: UIStoryboardSegue) {
        Log.high("onProposePropFinished()", from: self)
    }
    
    // MARK: -
    
    fileprivate func openFAQPage() {
        Log.high("openFAQPage()", from: self)
        
        if let url = URL(string: "http://Friendstasocial.com/faq/"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    fileprivate func openAppPage() {
        Log.high("openAppPage()", from: self)
        
        if let url = URL(string: "https://itunes.apple.com/us/app/Friendsta-anonymous-feedback/id1453199885?l=ru&ls=1&mt=8"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction fileprivate func onTermsButtonTouchUpInside(_ sender: Any) {
         Log.high("onTermsButtonTouchUpInside()", from: self)
        
        if let url = URL(string: "http://Friendstasocial.com/terms/"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    
    @IBAction fileprivate func onPrivacyButtonTouchUpInside(_ sender: Any) {
        Log.high("onPrivacyButtonTouchUpInside()", from: self)
        
        if let url = URL(string: "http://Friendstasocial.com/privacy/"),
            UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:])
        }
    }
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {
        case Segues.propseNote:
            guard let source = sender as? ProfileSettingSource else {
                fatalError()
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["source": source])
            }
            
        default:
            break
        }
    }
}

// - UITableViewDataSource

extension ProfileSettingsViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.tableSectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Constants.generalTableSection:
            return 5
            
        case Constants.manageAccountTableSection:
            return 1
            
        default:
            fatalError()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        
        switch indexPath.section {
        case Constants.generalTableSection:
            switch indexPath.row {
            case Constants.sumbitPokeTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.sumbitPokeTableCellIdentifier, for: indexPath)
                
            case Constants.getHelpTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.getHelpTableCellIdentifier, for: indexPath)
                
            case Constants.giveFeedbackTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.giveFeedbackTableCellIdentifier, for: indexPath)
                
            case Constants.faqTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.faqTableCellIdentifier, for: indexPath)
                
            case Constants.rateUsTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.rateUsTableCellIdentifier, for: indexPath)
                
            default:
                fatalError()
            }
            
        case Constants.manageAccountTableSection:
            switch indexPath.row {
            case Constants.manageAccountTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.manageAccountTableCellIdentifier, for: indexPath)
                
            default:
                fatalError()
            }
        default:
            fatalError()
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ProfileSettingsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.section {
        case Constants.generalTableSection:
            switch indexPath.row {
            case Constants.sumbitPokeTableCellRow:
                self.performSegue(withIdentifier: Segues.propseNote, sender: ProfileSettingSource.sumbitPoke)
                
            case Constants.getHelpTableCellRow:
                self.performSegue(withIdentifier: Segues.propseNote, sender: ProfileSettingSource.getHelp)
                
            case Constants.giveFeedbackTableCellRow:
                self.performSegue(withIdentifier: Segues.propseNote, sender: ProfileSettingSource.giveFeedack)
                
            case Constants.faqTableCellRow:
                self.openFAQPage()
                
            case Constants.rateUsTableCellRow:
                self.openAppPage()
                
            default:
                fatalError()
            }
        
        case Constants.manageAccountTableSection:
            switch indexPath.row {
            case Constants.manageAccountTableCellRow:
                self.performSegue(withIdentifier: Segues.showManageAccount, sender: self)
                
            default:
                fatalError()
            }
        default:
            fatalError()
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case Constants.generalTableSection:
            return tableView.dequeueReusableCell(withIdentifier: Constants.generalSectionHeaderIdentifier)
            
        case Constants.manageAccountTableSection:
            return tableView.dequeueReusableCell(withIdentifier: Constants.manageAccountSectionHeaderIdentifier)
            
        default:
            fatalError()
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case Constants.generalTableSection:
            return Constants.generalSectionHeaderHeight
            
        case Constants.manageAccountTableSection:
            return Constants.manageAccountSectionHeaderHeight
            
        default:
            fatalError()
        }
    }
}
