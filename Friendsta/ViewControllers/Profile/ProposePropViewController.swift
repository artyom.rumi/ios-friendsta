//
//  ProposePropsViewController.swift
//  Friendsta
//
//  Created by Artur Krasnyh on 27/06/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class ProposePropViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let finishProposeProp = "FinishProposeProp"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var textView: UITextView!
    @IBOutlet fileprivate weak var placeholderLabel: UILabel!
    @IBOutlet fileprivate weak var sendButton: UIButton!
    @IBOutlet fileprivate weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: -
    
    fileprivate var source: ProfileSettingSource?
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction func onSendButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onSendButtonTouchUpInside()", from: self)
        
        self.view.endEditing(true)
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true) {
            let content = self.addInfoMessage(to: self.textView.text ?? "")
            self.proposeProp(content: content, loadingViewController: loadingViewController)
        }
    }
    
    // MARK: -
    
    fileprivate func handle(actionError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error)
        }
    }
    
    fileprivate func proposeProp(content: String, loadingViewController: LoadingViewController) {
        Log.high("proposeProp(content: \(content))", from: self)
        
        firstly {
            Services.propsService.proposeProp(content: content)
        }.done {
            loadingViewController.dismiss(animated: true, completion: {
                self.showMessage(withTitle: "Thank you!".localized(),
                                 message: "Your boost will be reviewed by our team very soon.".localized(),
                                 okHandler: {
                    self.performSegue(withIdentifier: Segues.finishProposeProp, sender: self)
                })
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error)
            })
        }
    }
    
    // MARK: -
    
    func apply(source: ProfileSettingSource) {
        Log.high("apply(source: \(source))", from: self)
        
        self.source = source
        
        switch source {
        case .sumbitPoke:
            self.navigationItem.title = "Sumbit a poke"
            
        case .getHelp:
            self.navigationItem.title = "Get help"
            
        case .giveFeedack:
            self.navigationItem.title = "Give feedback"
        }
    }
    
    fileprivate func addInfoMessage(to content: String) -> String {
        guard let source = self.source else {
            fatalError()
        }
        
        switch source {
        case .getHelp:
            return "GET HELP: " + content
            
        case .giveFeedack:
            guard let userID = Services.accountUser?.uid else {
                fatalError()
            }
            return "FEEDBACK FROM ID \(userID): " + content
            
        case .sumbitPoke:
            return content
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.placeholderLabel.font = Fonts.regular(ofSize: 17.0)
        self.textView.font = Fonts.regular(ofSize: 17.0)
        self.sendButton.titleLabel?.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        
        self.placeholderLabel.isHidden = self.textView.hasText
        self.sendButton.isEnabled = self.textView.hasText
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribeToKeyboardNotifications()
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.textView.becomeFirstResponder()
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        
        self.unsubscribeFromKeyboardNotifications()
    }
}

// MARK: - UITextViewDelegate

extension ProposePropViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textViewDidChange(_ textView: UITextView) {
        self.placeholderLabel.isHidden = self.textView.hasText
        self.sendButton.isEnabled = self.textView.hasText
    }
}

// MARK: - KeyboardHandler

extension ProposePropViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
    
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

// MARK: - DictionaryReceiver

extension ProposePropViewController: DictionaryReceiver {
    
    func apply(dictionary: [String: Any]) {
        guard let source = dictionary["source"] as? ProfileSettingSource else {
            return
        }
        
        self.apply(source: source)
    }
}
