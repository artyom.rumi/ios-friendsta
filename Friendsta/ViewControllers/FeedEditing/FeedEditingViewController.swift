//
//  FeedEditingViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 09.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools
import PromiseKit

class FeedEditingViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let shareToAnonymousButtonTitle = "Share to your anonymous feed".localized()
        static let shareToTimelineButtonTitle = "Post this to your timeline".localized()
    }
    
    // MARK: -
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let finishPhotoPosting = "FinishPhotoPosting"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var linkPreviewView: LinkPreviewView!
    @IBOutlet private weak var photoPreviewView: PhotoPreviewView!
    
    @IBOutlet private weak var avatarHolderView: RoundShadowViewWithImage!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var schoolInfoLabel: UILabel!
    
    @IBOutlet private weak var shareButton: UIButton!
    
    // MARK: -
    
    private var feed: Feed?
    private var feedEditingState: FeedEditingState?
    
    // MARK: - Instance Methods
    
    @IBAction private func onShareButtonTouchUpInside(_ sender: Any) {
        Log.high("onShareButtonTouchUpInside()", from: self)
        
        guard let feed = self.feed else {
            fatalError()
        }
        
        guard let state = self.feedEditingState else {
            return
        }
        
        let isIncognito = (state == .repostToAnonymous)
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            self.changeStatus(feed: feed, isIncognito: isIncognito, loadingViewController: loadingViewController)
        })
    }
    
    // MARK: -
    
    private func changeStatus(feed: Feed, isIncognito: Bool, loadingViewController: LoadingViewController) {
        Log.high("changeStatus(feed: \(feed.uid), isIncognito: \(isIncognito))", from: self)
        
        firstly {
            Services.feedService.changeStatus(feed: feed, isIncognito: isIncognito)
        }.done { feed in
            loadingViewController.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: Segues.finishPhotoPosting, sender: self)
            })
        }.catch { error in
            self.showMessage(withError: error)
        }
    }
    
    private func loadPreviewFor(link: URL) {
        Managers.linkPreviewManager.getInformationFrom(link: link, completion: { linkString, linkDescription, imageURLString, error in
            if let imageURLString = imageURLString, let imageURL = URL(string: imageURLString) {
                Managers.imageLoader.loadImage(for: imageURL, completionHandler: { image in
                    DispatchQueue.main.async {
                        self.linkPreviewView.isLoadingViewHidden = true
                        
                        self.linkPreviewView.linkImageViewTarget.image = image
                    }
                })
            }
            
            DispatchQueue.main.async {
                self.linkPreviewView.linkURL = linkString
                self.linkPreviewView.linkDescription = linkDescription
            }
        })
    }
    
    private func loadAvatarImage(with accountUser: AccountUser) {
        self.avatarHolderView.placeholderImage = Images.avatarLargePlaceholder
        
        if let imageURL = accountUser.largeAvatarURL ?? accountUser.largeAvatarPlaceholderURL {
            self.activityIndicatorView.startAnimating()

            Services.imageLoader.loadImage(for: imageURL, in: self.avatarHolderView.imageViewTarget, placeholder: Images.avatarLargePlaceholder, completionHandler: { image in
                self.activityIndicatorView.stopAnimating()
            })
        }
    }

    // MARK: -
    
    private func configureUserInformation() {
        self.fullNameLabel.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
        self.schoolInfoLabel.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)

        if let accountUser = Services.accountUser {
            self.loadAvatarImage(with: accountUser)

            self.fullNameLabel.text = accountUser.fullName

            let schoolTitle = accountUser.schoolTitle ?? "unknown school".localized()

            if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(accountUser.classYear)) {
                self.schoolInfoLabel.text = "\(schoolGradeNumber)TH GRADE, \(schoolTitle)".localized()
            } else {
                self.schoolInfoLabel.text = "FINISHED, \(schoolTitle)".localized()
            }
        }
    }
    
    private func configureLinkPreview() {
        guard let feed = self.feed else {
            fatalError()
        }
        
        guard let linkString = feed.link else {
            fatalError()
        }
        
        guard let linkURL = URL(string: linkString) else {
            return
        }
                
        self.linkPreviewView.onLinkTapped = {
            UIApplication.shared.open(linkURL, options: [:], completionHandler: nil)
        }
        
        self.loadPreviewFor(link: linkURL)
    }
    
    private func configurePhotoPreview() {
        guard let feed = self.feed else {
            fatalError()
        }
        
        guard let photoURL = feed.originalImageURL else {
            return
        }
        
        Services.imageLoader.loadImageProgressive(for: photoURL, in: self.photoPreviewView.imageViewTarget)
    }
    
    // MARK: -
    
    private func setupRepostToAnonymousState() {
        self.fullNameLabel.isHidden = true
        self.schoolInfoLabel.isHidden = true
        self.avatarHolderView.isHidden = true
        
        self.shareButton.setTitle(Constants.shareToAnonymousButtonTitle, for: .normal)
    }
    
    private func setupRepostToTimelineState() {
        self.fullNameLabel.isHidden = false
        self.schoolInfoLabel.isHidden = false
        self.avatarHolderView.isHidden = false
        
        self.shareButton.setTitle(Constants.shareToTimelineButtonTitle, for: .normal)
    }
    
    // MARK: -
    
    private func apply(feed: Feed, state: FeedEditingState) {
        Log.high("apply(feed: \(feed.uid))", from: self)
        
        self.feed = feed
        self.feedEditingState = state
        
        if self.isViewLoaded {
            self.linkPreviewView.isHidden = (feed.link == nil)
            self.photoPreviewView.isHidden = (feed.link != nil)
            
            if feed.link != nil {
                self.configureLinkPreview()
            } else {
                self.configurePhotoPreview()
            }
            
            switch state {
            case .repostToAnonymous:
                self.setupRepostToAnonymousState()
                
            case .repostToTimeline:
                self.setupRepostToTimelineState()
            }
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureUserInformation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let feed = self.feed, let state = self.feedEditingState {
            self.apply(feed: feed, state: state)
        }
    }
}

// MARK: - DictionaryReceiver

extension FeedEditingViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let feed = dictionary["feed"] as? Feed else {
            return
        }
        
        guard let state = dictionary["feedEditingState"] as? FeedEditingState else {
            return
        }
        
        self.apply(feed: feed, state: state)
    }
}
