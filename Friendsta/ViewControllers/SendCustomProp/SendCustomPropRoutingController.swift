//
//  SendCustomPropRoutingController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 08/10/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools

class SendCustomPropRoutingViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let embedContent = "EmbedContent"
        static let finishPropSending = "FinishPropSending"
        static let unauthorize = "Unauthorize"
        static let showPropsList = "ShowPropsList"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var propReceiverAvatarImageView: RoundImageView!
    @IBOutlet fileprivate weak var propReceiverNameLabel: UILabel!
    
    fileprivate weak var sendCustomPropViewController: SendCustomPropViewController!
    
    // MARK: -
    
    fileprivate(set) var propReceiver: PropReceiver?
    
    fileprivate(set) var shouldRemovePropReceiver = false
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    fileprivate func onEmbedContent(segue: UIStoryboardSegue, sender: Any?) {
        guard let sendCustomPropViewController = segue.destination as? SendCustomPropViewController else {
            fatalError()
        }
        
        self.sendCustomPropViewController = sendCustomPropViewController
        
        self.sendCustomPropViewController.finishPropSending = { [unowned self] in
            self.performSegue(withIdentifier: Segues.finishPropSending, sender: self)
        }
        
        self.sendCustomPropViewController.unauthorize = { [unowned self] in
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
        }
        
        self.sendCustomPropViewController.showPropsList = { [unowned self] in
            self.performSegue(withIdentifier: Segues.showPropsList, sender: self.propReceiver?.user)
        }
    }
    
    fileprivate func onFinishPropSending(segue: UIStoryboardSegue, sender: Any?) {
        Log.high("onFinishPropSending()", from: self)
        
        if let propReceiver = self.propReceiver, self.shouldRemovePropReceiver {
            self.remove(propReceiver: propReceiver)
        }
    }
    
    fileprivate func onShowPropsList(segue: UIStoryboardSegue, sender: Any?) {
        Log.high("onOpenAllPropsList()", from: self)
        
        let dictionaryReceiver: DictionaryReceiver?

        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
    
        guard let user = sender as? User else {
            fatalError()
        }
        
        if let dictionaryReceiver = dictionaryReceiver {
            dictionaryReceiver.apply(dictionary: ["user": user])
        }
    }
    
    // MARK: -
    
    fileprivate func loadPropReceiverAvatarImage(with user: User) {
        self.propReceiverAvatarImageView.image = #imageLiteral(resourceName: "AvatarLargePlaceholder")
        
        if let imageURL = user.smallAvatarURL {
            Services.imageLoader.loadImage(for: imageURL, in: self.propReceiverAvatarImageView, placeholder: #imageLiteral(resourceName: "AvatarLargePlaceholder"))
        }
    }
    
    // MARK: -
    
    fileprivate func remove(propReceiver: PropReceiver) {
        firstly {
            Services.cacheProvider.captureModel()
        }.done { cacheSession in
            cacheSession.model.viewContext.propReceiversManager.remove(object: propReceiver)
            cacheSession.model.viewContext.save()
        }
    }
    
    // MARK: -
    
    func apply(propReceiver: PropReceiver, shouldRemovePropReceiver: Bool) {
        Log.high("apply(propReceiver: \(String(describing: propReceiver.user?.fullName)))", from: self)
        
        if let previousPropReceiver = self.propReceiver, self.shouldRemovePropReceiver {
            if previousPropReceiver !== propReceiver {
                self.remove(propReceiver: previousPropReceiver)
            }
        }
        
        self.propReceiver = propReceiver
        
        self.shouldRemovePropReceiver = shouldRemovePropReceiver
        
        if self.isViewLoaded {
            self.sendCustomPropViewController.apply(propReceiver: propReceiver)
            
            if let user = propReceiver.user {
                self.loadPropReceiverAvatarImage(with: user)
                
                self.propReceiverNameLabel.text = String(format: "Send to %@".localized(),
                                                         user.firstName ?? "unknown user".localized())
            } else {
                self.propReceiverAvatarImageView.image = #imageLiteral(resourceName: "AvatarLargePlaceholder")
                
                self.propReceiverNameLabel.text = String(format: "Send to %@".localized(), "unknown user".localized())
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    func apply(user: User) {
        Log.high("apply(user: \(String(describing: user.fullName)))", from: self)
        
        let propReceiver = Services.cacheViewContext.propReceiversManager.append()
        
        propReceiver.uid = 0
        propReceiver.user = user
        
        self.apply(propReceiver: propReceiver, shouldRemovePropReceiver: true)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let propReceiver = self.propReceiver, !self.hasAppliedData {
            self.apply(propReceiver: propReceiver, shouldRemovePropReceiver: self.shouldRemovePropReceiver)
        }
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.embedContent:
            self.onEmbedContent(segue: segue, sender: sender)
            
        case Segues.finishPropSending:
            self.onFinishPropSending(segue: segue, sender: sender)
            
        case Segues.showPropsList:
            self.onShowPropsList(segue: segue, sender: sender)
            
        default:
            break
        }
    }
}

// MARK: - DictionaryReceiver

extension SendCustomPropRoutingViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        if let propReceiver = dictionary["propReceiver"] as? PropReceiver {
            let shouldRemovePropReceiver = dictionary["shouldRemovePropReceiver"] as? Bool
            
            self.apply(propReceiver: propReceiver, shouldRemovePropReceiver: shouldRemovePropReceiver ?? false)
        } else if let user = dictionary["user"] as? User {
            self.apply(user: user)
        }
    }
}
