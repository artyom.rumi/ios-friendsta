//
//  SendCustomPropViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 01.10.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import Nuke
import FriendstaTools
import FriendstaNetwork

class SendCustomPropViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let finishCustomPropSending = "FinishCustomPropSending"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var textView: UITextView!
    @IBOutlet fileprivate weak var placeholderLabel: UILabel!
    
    @IBOutlet fileprivate weak var sendButton: UIButton!
    @IBOutlet fileprivate weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: -
    
    fileprivate(set) var propReceiver: PropReceiver?
    
    fileprivate(set) var hasAppliedData = false
    
    var finishPropSending: (() -> Void)?
    var unauthorize: (() -> Void)?
    var showPropsList: (() -> Void)?
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onSendButtonTouchUpInside(_ sender: Any) {
        Log.high("onSendButtonTouchUpInside()", from: self)
        
        self.view.endEditing(true)
        
        self.sendCustomProp()
    }
    
    @IBAction fileprivate func onChooseBoostButtonTouchUpInside(_ sender: Any) {
        Log.high("onChooseBoostButtonTouchUpInside()", from: self)
        
        self.showPropsList?()
    }
    
    // MARK: -
    
    fileprivate func updateTextField(with text: String?) {
        self.textView.text = text
        
        self.placeholderLabel.isHidden = self.textView.hasText
        self.sendButton.isEnabled = self.textView.hasText
    }
    
    // MARK: -
    
    fileprivate func handle(actionError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error)
        }
    }
    
    fileprivate func sendCustomProp() {
        Log.high("sendCustomProp()", from: self)
        
        guard let propReceiver = self.propReceiver else {
            return
        }
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            firstly {
                Services.propReceiversService.sendCustomProp(content: self.textView.text ?? "",
                                                             to: propReceiver)
            }.done { status in
                loadingViewController.dismiss(animated: true, completion: {
                    self.finishPropSending?()
                })
            }.catch { error in
                loadingViewController.dismiss(animated: true, completion: {
                    self.handle(actionError: error)
                })
            }
        })
    }
    
    // MARK: -
    
    func apply(propReceiver: PropReceiver) {
        Log.high("apply(propReceiver: \(String(describing: propReceiver.user?.fullName)))", from: self)
        
        self.propReceiver = propReceiver
        
        if self.isViewLoaded {
            self.updateTextField(with: nil)
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.textView.font = Fonts.regular(ofSize: 17.0)
        self.placeholderLabel.font = Fonts.regular(ofSize: 17.0)
        self.sendButton.titleLabel?.font = Fonts.semiBold(ofSize: 17.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.subscribeToKeyboardNotifications()
        
        if let propReceiver = self.propReceiver, !self.hasAppliedData {
            self.apply(propReceiver: propReceiver)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.textView.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        
        self.hasAppliedData = false
        
        self.unsubscribeFromKeyboardNotifications()
    }
}

// MARK: - UITextViewDelegate

extension SendCustomPropViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textViewDidChange(_ textView: UITextView) {
        self.updateTextField(with: textView.text)
    }
}

// MARK: - KeyboardHandler

extension SendCustomPropViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

// MARK: - DictionaryReceiver

extension SendCustomPropViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let propReceiver = dictionary["propReceiver"] as? PropReceiver else {
            return
        }
        
        self.apply(propReceiver: propReceiver)
    }
}
