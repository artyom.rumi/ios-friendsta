//
//  FriendsTableViewController.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 15/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class FriendsTableViewController: LoggedViewController {

    // MARK: - Nested Types

    private typealias SectionData = (section: Section, list: UserList)

    // MARK: -

    private enum Constants {

        // MARK: - Type Properties

        static let showMoreTableCellIdentifier = "ShowMoreTableCell"
        static let friendTableCellIdentifier = "FriendTableCell"

        static let connectionsTableSectionHeaderIdentifier = "ConnectionsTableSectionHeader"
        static let invitesTableSectionHeaderIdentifier = "InvitesTableSectionHeader"

        static let showMoreCellCount = 1
    }
    
    // MARK: -
    
    private enum Section {
        
        // MARK: - Enumeration Cases
        
        case friends
        case invitedUsers
        case expiredFriends
        case pokeInlineContacts
        case pokeSchoolmates
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var tableView: UITableView!

    @IBOutlet private weak var emptyStateView: EmptyStateView!

    private weak var tableRefreshControl: UIRefreshControl!

    // MARK: -

    private var sections: [SectionData] = []

    // MARK: -

    private var friendsUserListPage: Int = 1

    private var isAllListsEmpty: Bool {
        return self.sections.isEmpty
    }

    // MARK: -

    private var isRefreshingData = false
    private var shouldRefreshData = false

    // MARK: -

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    // MARK: -

    var showFriendProps: ((User) -> Void)?
    var showFriendUser: ((User) -> Void)?
    var showAllUsers: ((UserListType) -> Void)?
    var unauthorize: (() -> Void)?

    // MARK: - Initializers

    deinit {
        self.unsubscribeFromAccountUserSchoolEvents()
    }

    // MARK: - Instance Methods

    @objc fileprivate func onTableRefreshControlRequested(_ sender: Any) {
        Log.high("onTableRefreshControlRequested()", from: self)

        self.refreshUserList()
    }

    // MARK: -

    private func showLoadingState() {
        if self.emptyStateView.isHidden {
            let title = "Searching your friends".localized()
            let message = "We are loading list of your friends. Please wait a bit.".localized()

            self.showEmptyState(title: title, message: message)
        }

        self.emptyStateView.showActivityIndicator()
    }

    private func showNoDataState() {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: { [unowned self] in
            self.refreshUserList()
        })

        self.showEmptyState(title: "Friends not found".localized(),
                            message: "You can invite friends on community tab".localized(),
                            action: action)
    }

    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: {
            retryHandler?()
        })

        switch error as? WebError {
        case .some(.unauthorized):
            self.unauthorize?()

        case .some(.connection), .some(.timeOut):
            if self.isAllListsEmpty {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            }

        default:
            if self.isAllListsEmpty {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        }
    }

    private func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()

        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action

        self.emptyStateView.isHidden = false
    }

    private func hideEmptyState() {
        self.emptyStateView.isHidden = true
    }

    // MARK: -

    private func refreshUserList() {
        Log.high("refreshUserList()", from: self)

        self.isRefreshingData = true

        if !self.tableRefreshControl.isRefreshing {
            if self.isAllListsEmpty || !self.emptyStateView.isHidden || self.shouldRefreshData {
                self.showLoadingState()
            }
        }

        let userService = Services.usersService

        firstly {
            when(fulfilled: userService.refreshFriends(),
                 userService.refreshInvitedFriends(),
                 userService.refreshExpiredFriends(),
                 userService.refreshPokeInlineContacts(),
                 userService.refreshPokeSchoolmates()
            )
        }.ensure {
            if self.tableRefreshControl.isRefreshing {
                self.tableRefreshControl.endRefreshing()
            }

            self.isRefreshingData = false
        }.done { friendsUserList, invitedFriendsUserList, expiredFriendsUserList, pokeInlineContactsUserList, pokeSchoolmatesUserList in
            self.apply(friendsUserList: friendsUserList,
                       invitedFriendsUserList: invitedFriendsUserList,
                       expiredFriendsUserList: expiredFriendsUserList,
                       pokeInlineContactsUserList: pokeInlineContactsUserList,
                       pokeSchoolmatesUserList: pokeSchoolmatesUserList)
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshUserList()
            })
        }
    }
    
    private func loadDataFromCache() {
        Log.high("loadDataFromCache()", from: self)
        
        let friendsUserList = Services.cacheViewContext.userListManager.firstOrNew(withListType: .friends)
        let invitedFriendsUserList = Services.cacheViewContext.userListManager.firstOrNew(withListType: .invited)
        let expiredFriendsUserList = Services.cacheViewContext.userListManager.firstOrNew(withListType: .expired)
        let pokeInlineContactsUserList = Services.cacheViewContext.userListManager.firstOrNew(withListType: .pokeInlineContacts)
        let pokeSchoolmatesUserList = Services.cacheViewContext.userListManager.firstOrNew(withListType: .pokeSchoolmates)
        
        self.apply(friendsUserList: friendsUserList,
                   invitedFriendsUserList: invitedFriendsUserList,
                   expiredFriendsUserList: expiredFriendsUserList,
                   pokeInlineContactsUserList: pokeInlineContactsUserList,
                   pokeSchoolmatesUserList: pokeSchoolmatesUserList)
    }

    private func loadMore(of userList: UserList, at sectionIndex: Int, showMoreCell cell: ShowMoreTableViewCell) {
        Log.high("loadMore(of userList: \(userList.listType))", from: self)

        cell.showActivityIndicator()

        firstly {
            Services.usersService.loadMoreUsers(of: userList)
        }.ensure {
            cell.hideActivityIndicator()
        }.done { userList in
            self.sections[sectionIndex] = (self.sections[sectionIndex].section, userList)

            self.tableView.reloadData()

            DispatchQueue.main.async(execute: {
                let indexPath = IndexPath(row: self.fetchNumberOfRows(in: sectionIndex) - 1, section: sectionIndex)

                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            })
        }.catch { error in
            self.handle(stateError: error)
        }
    }

    // MARK: -

    private func subscribeToAccountUserScoolEvents() {
        self.unsubscribeFromAccountUserSchoolEvents()

        let accountUsersManager = Services.cacheViewContext.accountUsersManager

        accountUsersManager.objectsSchoolUpdatedEvent.connect(self, handler: { [weak self] accountUsers in
            self?.shouldRefreshData = true
        })

        accountUsersManager.startObserving()
    }

    private func unsubscribeFromAccountUserSchoolEvents() {
        Services.cacheViewContext.accountUsersManager.objectsSchoolUpdatedEvent.disconnect(self)
    }

    // MARK: -
    
    private func apply(friendsUserList: UserList,
                       invitedFriendsUserList: UserList,
                       expiredFriendsUserList: UserList,
                       pokeInlineContactsUserList: UserList,
                       pokeSchoolmatesUserList: UserList) {
        Log.high("apply(friendsUserList: \(friendsUserList.count), invitedFriendsUserList: \(invitedFriendsUserList.count), expiredFriendsUserList: \(expiredFriendsUserList.count), pokeInlineContactsUserList: \(pokeInlineContactsUserList.count), pokeSchoolmatesUserList: \(pokeSchoolmatesUserList.count))", from: self)
        
        self.sections.removeAll()
        
        if !friendsUserList.allUsers.isEmpty {
            let sectionData: SectionData = (.friends, friendsUserList)

            self.sections.append(sectionData)
        }
        
        if !invitedFriendsUserList.allUsers.isEmpty {
            let sectionData: SectionData = (.invitedUsers, invitedFriendsUserList)

            self.sections.append(sectionData)
        }
        
        if !expiredFriendsUserList.allUsers.isEmpty {
            let sectionData: SectionData = (.expiredFriends, expiredFriendsUserList)

            self.sections.append(sectionData)
        }

        if !pokeInlineContactsUserList.allUsers.isEmpty {
            let sectionData: SectionData = (.pokeInlineContacts, pokeInlineContactsUserList)

            self.sections.append(sectionData)
        }

        if !pokeSchoolmatesUserList.allUsers.isEmpty {
            let sectionData: SectionData = (.pokeSchoolmates, pokeSchoolmatesUserList)

            self.sections.append(sectionData)
        }

        if self.isAllListsEmpty {
            self.showNoDataState()
        } else {
            self.hideEmptyState()
        }
        
        Managers.userDefaultsManager.isUserHasSeenFriendsScreen = true

        self.shouldRefreshData = false
        
        self.tableView.reloadData()
    }

    // MARK: -
    
    private func fetchNumberOfRows(in sectionIndex: Int) -> Int {
        let userList = self.sections[sectionIndex].list
        
        if userList.hasNextPage {
            return userList.allUsers.count + Constants.showMoreCellCount
        } else {
            return userList.allUsers.count
        }
    }
    
    // MARK: -

    private func configureTableRefreshControl() {
        let tableRefreshControl = UIRefreshControl()

        tableRefreshControl.addTarget(self,
                                      action: #selector(self.onTableRefreshControlRequested(_:)),
                                      for: .valueChanged)

        self.tableView.refreshControl = tableRefreshControl

        self.tableRefreshControl = tableRefreshControl
    }
    
    // MARK: -
    
    func updateContentWithCacheData() {
        self.loadDataFromCache()
    }

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        self.subscribeToAccountUserScoolEvents()

        self.configureTableRefreshControl()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Managers.userDefaultsManager.isUserHasSeenFriendsScreen, !self.shouldRefreshData {
            self.loadDataFromCache()
        } else {
            self.refreshUserList()
        }

        self.setNeedsStatusBarAppearanceUpdate()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.setNeedsStatusBarAppearanceUpdate()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.tableView.sizeHeaderToFit()
    }
}

// MARK: - UITableViewDataSource

extension FriendsTableViewController: UITableViewDataSource {

    // MARK: - Instance Methods

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchNumberOfRows(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        let userList = self.sections[indexPath.section].list
        
        if indexPath.row == userList.allUsers.count {
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.showMoreTableCellIdentifier, for: indexPath)
            
            self.configure(showMoreCell: cell as! ShowMoreTableViewCell, at: indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.friendTableCellIdentifier, for: indexPath)

            self.configure(friendCell: cell as! FriendTableViewCell, at: indexPath)
        }
        
        return cell
    }
}

// MARK: - UITableViewDataSource Helpers

extension FriendsTableViewController {

    // MARK: - Instance Methods

    private func configure(showMoreCell cell: ShowMoreTableViewCell, at indexPath: IndexPath) {
        let userList = self.sections[indexPath.section].list

        cell.showMoreButtonTitle = "Show \(min(userList.moreCount, Limits.friendMinCount)) more"

        cell.onShowMoreTapped = { [unowned self] in
            self.loadMore(of: userList, at: indexPath.section, showMoreCell: cell)
        }
    }

    private func configure(friendCell cell: FriendTableViewCell, at indexPath: IndexPath) {
        let user = self.sections[indexPath.section].list[indexPath.row]
        let section = self.sections[indexPath.section].section

        cell.fullName = user.fullName
        cell.isFriendCountHidden = false

        if user.friendsCount > Limits.maxFriendCount {
            cell.friendCount = "\(Limits.maxFriendCount)+"
        } else {
            cell.friendCount = "\(user.friendsCount)"
        }

        switch user.friendshipStatus {
        case .some(.affirmed) where section == .friends:
            cell.info = "Affirmed".localized()
            cell.infoTextColor = Colors.redText

        case .some(.friend) where section == .friends:
            if user.daysToExpiredFriendship == 0 {
                cell.info = "Today".localized()
                cell.infoTextColor = Colors.redText
            } else if user.daysToExpiredFriendship == 1 {
                cell.info = String(format: "%d day".localized(), user.daysToExpiredFriendship)
                cell.infoTextColor = Colors.redText
            } else if user.daysToExpiredFriendship <= 2 {
                cell.info = String(format: "%d days".localized(), user.daysToExpiredFriendship)
                cell.infoTextColor = Colors.redText
            } else {
                cell.info = String(format: "%d days".localized(), user.daysToExpiredFriendship)
                cell.infoTextColor = Colors.grayText
            }

        case .some(.invited), .some(.uninvited), .some(.affirmed), .some(.friend), .none:
            let schoolTitle = user.schoolTitle ?? "unknown school".localized()

            if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(user.classYear)) {
                cell.info = String(format: "%d grade %@".localized(), schoolGradeNumber, schoolTitle)
            } else {
                cell.info = String(format: "Finished".localized(), schoolTitle)
            }

            cell.infoTextColor = Colors.grayText
            cell.isFriendCountHidden = true
        }
        
        cell.friendshipStatus = user.friendshipStatus
        cell.isKeepControlHidden = (user.friendshipStatus == nil)
        
        cell.onAddTapped = {
            cell.isKeepLoadingActivityIndicatorAnimating = true

            firstly {
                Services.usersService.inviteUser(user: user)
            }.done { user in
                cell.friendshipStatus = user.friendshipStatus
                cell.isKeepLoadingActivityIndicatorAnimating = false
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }.catch { _ in
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }
        }

        cell.onKeepTapped = {
            cell.isKeepLoadingActivityIndicatorAnimating = true
            
            firstly {
                Services.usersService.keep(user: user)
            }.done { user in
                cell.friendshipStatus = user.friendshipStatus
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }.catch { _ in
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }
        }
    }

    private func configure(connectionsTableSectionHeaderView headerView: ConnectionsTableSectionHeaderView, at section: Section) {
        switch section {
        case .friends:
            headerView.title = "These are your solid friends!".localized()
            headerView.subtitle = "Check their updates and stay connected!".localized()

            headerView.onShowAllButtonClicked = { [unowned self] in
                self.showAllUsers?(.friends)
            }

        case .expiredFriends:
            headerView.title = "Hi, be connected with them again!".localized()
            headerView.subtitle = "Your friend status is expired.".localized()

            headerView.onShowAllButtonClicked = { [unowned self] in
                self.showAllUsers?(.expired)
            }

        case .pokeInlineContacts:
            headerView.title = "Your contacts!".localized()
            headerView.subtitle = "Do not miss anything from your peers!".localized()

            headerView.onShowAllButtonClicked = { [unowned self] in
                self.showAllUsers?(.pokeInlineContacts)
            }

        case .pokeSchoolmates:
            headerView.title = "Your classmates!".localized()
            headerView.subtitle = "Do not miss anything from your peers!".localized()

            headerView.onShowAllButtonClicked = { [unowned self] in
                self.showAllUsers?(.pokeSchoolmates)
            }

        default:
            fatalError()
        }
    }
}

// MARK: - UITableViewDelegate

extension FriendsTableViewController: UITableViewDelegate {

    // MARK: - Instance Methods

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? FriendTableViewCell else {
            return
        }

        guard let avatarURL = self.sections[indexPath.section].list[indexPath.row].smallAvatarURL else {
            return
        }

        Services.imageLoader.loadImage(for: avatarURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarSmallPlaceholder)
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? FriendTableViewCell else {
            return
        }

        Services.imageLoader.cancelLoading(in: cell.avatarImageViewTarget)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let user = self.sections[indexPath.section].list[indexPath.row]
        
        self.showFriendUser?(user)
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection sectionIndex: Int) -> UIView? {
        let sectionHeaderView: UIView?
        
        let section = self.sections[sectionIndex].section

        switch section {
        case .friends, .expiredFriends, .pokeInlineContacts, .pokeSchoolmates:
            sectionHeaderView = tableView.dequeueReusableCell(withIdentifier: Constants.connectionsTableSectionHeaderIdentifier)

            self.configure(connectionsTableSectionHeaderView: sectionHeaderView as! ConnectionsTableSectionHeaderView, at: section)

        case .invitedUsers:
            sectionHeaderView = tableView.dequeueReusableCell(withIdentifier: Constants.invitesTableSectionHeaderIdentifier)
        }

        return sectionHeaderView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.sections[section].list.allUsers.isEmpty ? 0 : UITableView.automaticDimension
    }
}
