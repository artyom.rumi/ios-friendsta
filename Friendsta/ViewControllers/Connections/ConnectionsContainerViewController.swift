//
//  ConnectionsContainerViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 05.09.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

// swiftlint:disable file_length
// swiftlint:disable type_body_length
// swiftlint:disable function_body_length
// swiftlint:disable vertical_whitespace

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class ConnectionsContainerViewController: LoggedViewController, ErrorMessagePresenter {
    
    ///// ********* Lance's Code ********* /////
    
    public var isComingFromAvatarPickerVC = false
    public var isAvatarChanged = false
    public var isAvatarRemoved = false
    public var avatarImageViewImage: UIImage?
    public var newAccountUser: AccountUser?
    fileprivate var doneBarButtonItem: UIBarButtonItem?
    
    @objc fileprivate func doneButtonPressed() {
        
        if newAccountUser == nil {
            newAccountUser = Services.accountUser
        }
        
        if self.isAvatarChanged {
            if self.isAvatarRemoved {
                let loadingViewController = LoadingViewController()
                
                self.present(loadingViewController, animated: true, completion: {
                    self.deleteAccountUserAvatar(loadingViewController: loadingViewController)
                })
            } else {
                guard let avatarImage = self.avatarImageViewImage else {
                    return
                }
                
                let loadingViewController = LoadingViewController()
                
                self.present(loadingViewController, animated: true, completion: {
                    self.uploadAccountUser(avatarImage: avatarImage, loadingViewController: loadingViewController)
                })
            }
        } else {
            
            self.performSegue(withIdentifier: Segues.finishAuthorization, sender: self.newAccountUser)
            /*
            if self.newAccountUser?.avatarURL == nil {
                
                 let alertController = UIAlertController(title: "Hold up!".localized(),
                 message: "Adding a photo makes it easier for your friends to send you boosts".localized(),
                 preferredStyle: .alert)
                 
                 alertController.view.tintColor = Colors.primary
                 
                 alertController.addAction(UIAlertAction(title: "Not Now".localized(), style: .cancel, handler: { action in
                 self.performSegue(withIdentifier: Segues.finishAuthorization, sender: self.newAccountUser)
                 }))
                 
                 alertController.addAction(UIAlertAction(title: "Add Photo".localized(), style: .default))
                 
                 self.present(alertController, animated: true)
            }
            */
        }
    }
    
    fileprivate func uploadAccountUser(avatarImage: UIImage, loadingViewController: LoadingViewController) {
        
        if newAccountUser == nil {
            newAccountUser = Services.accountUser
        }
        
        DispatchQueue.global(qos: .userInitiated).async(execute: {
            let avatarData: Data?
            
            if avatarImage.size.width > Limits.avatarImageMaxWidth {
                if let avatarImage = avatarImage.scaled(to: Limits.avatarImageMaxWidth) {
                    avatarData = avatarImage.jpegData(compressionQuality: Limits.avatarImageQuality)
                } else {
                    avatarData = avatarImage.jpegData(compressionQuality: Limits.avatarImageQuality)
                }
            } else {
                avatarData = avatarImage.jpegData(compressionQuality: Limits.avatarImageQuality)
            }
            
            DispatchQueue.main.async(execute: {
                if let avatarData = avatarData {
                    firstly {
                        Services.accountUserService.uploadAccountUserAvatar(with: avatarData)
                    }.done { accountUser in
                        //self.apply(accountUser: self.newAccountUser)
                        
                        loadingViewController.dismiss(animated: true, completion: {
                            self.performSegue(withIdentifier: Segues.finishAuthorization, sender: self.newAccountUser)
                        })
                    }.catch { error in
                        loadingViewController.dismiss(animated: true, completion: {
                            self.handle(actionError: error)
                        })
                    }
                } else {
                    
                    self.performSegue(withIdentifier: Segues.finishAuthorization, sender: self.newAccountUser)
                    
                    /*
                     loadingViewController.dismiss(animated: true, completion: {
                     self.showMessage(withTitle: "Invalid image".localized(),
                     message: "Please choose another image".localized())
                     })
                     */
                }
            })
        })
    }
    
    fileprivate func deleteAccountUserAvatar(loadingViewController: LoadingViewController) {
        
        if newAccountUser == nil {
            newAccountUser = Services.accountUser
        }
        
        firstly {
            Services.accountUserService.deleteAccountUserAvatar()
        }.done { accountUser in
            //            if let accountUser = accountUser {
            //                self.apply(accountUser: accountUser)
            //            } else {
            //                self.isAvatarChanged = false
            //                self.isAvatarRemoved = false
            //            }
            
            loadingViewController.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: Segues.finishAuthorization, sender: self.newAccountUser)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error)
            })
        }
    }
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorizeNotFinished, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate lazy var doneButton: UIButton = {
        let button = UIButton(type: .system)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        button.setTitle("Done", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        
        button.backgroundColor = Colors.primary
        button.addTarget(self, action: #selector(doneButtonPressed), for: .touchUpInside)
        
        button.layer.cornerRadius = 15
        button.layer.masksToBounds = true
        button.sizeToFit()
        
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 7.5, bottom: 0, right: 7.5)
        
        return button
    }()
    
    deinit {
        print("ConnectionsContainerVC -DEINIT")
    }
    
    @IBOutlet weak var topContainerView: UIView!
    
    ///// ********* Lance's Code ********* /////
    
    // MARK: - Nested Types
    
    private enum Segues {
        
        // MARK: - Type Properties

        static let unauthorize = "Unauthorize"

        static let showFriendUser = "ShowFriendUser"
        static let showFriendProps = "ShowFriendProps"
        static let showUsers = "ShowUsers"
        static let showOutlineContacts = "ShowOutlineContacts"

        static let embedConnectionsSearch = "EmbedConnectionsSearch"
        static let embedCommunity = "EmbedCommunity"
        static let embedFriends = "EmbedFriends"
        
        ///// ********* Lance's Code ********* /////
        static let unauthorizeNotFinished = "UnauthorizeNotFinished"
        static let finishAuthorization = "FinishAuthorization"
        ///// ********* Lance's Code ********* /////
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let communitySegmentIndex = 0
        static let friendsSegmentIndex = 1

        static let shareText = "Join me on the new social app Friendsta at www.nicelysocial.com".localized()
    }
    
    // MARK: -
    
    private enum FilterType: Equatable {
        
        // MARK: - Enumeration Cases
        
        case none
        case search(text: String)
    }

    // MARK: - Instance Properties
    
    @IBOutlet private weak var backButton: UIButton!
    @IBOutlet private weak var searchTextField: SearchTextField!
    @IBOutlet private weak var shareButton: UIButton!

    @IBOutlet private weak var friendsContainerView: UIView!
    @IBOutlet private weak var communityContainerView: UIView!
    @IBOutlet private weak var connectionsSearchContainerView: UIView!

    // MARK: -

    private var connectionsSearchTableViewController: ConnectionsSearchTableViewController!
    private var communityTableViewController: CommunityTableViewController!
    private var friendsTableViewController: FriendsTableViewController!
    
    // MARK: -

    private var filterType: FilterType = .none
    private var state: ConnectionsState = .adding

    private var shouldApplyData = true

    // MARK: -

    var onBack: (() -> Void)?
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onFriendsCustomPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onFriendsCustomPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
        
        self.updateContentWithCacheData()
    }
    
    @IBAction private func onFriendsPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onFriendsPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
        
        self.updateContentWithCacheData()
    }
    
    @IBAction private func onSearchTextFieldChanged(_ sender: UITextField) {
        Log.high("onSearchTextFieldChanged()", from: self)
        
        if let searchText = self.searchTextField.text, !searchText.isEmpty {
            self.apply(filterType: .search(text: searchText))
        } else {
            self.apply(filterType: .none)
        }
    }

    @IBAction private func onSearchTextFieldPrimaryActionTriggered(_ sender: Any) {
        self.view.endEditing(true)
    }

    @IBAction private func onShareButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onShareButtonTouchUpInside()", from: self)

        let activityViewController = UIActivityViewController(activityItems: [Constants.shareText], applicationActivities: nil)

        self.present(activityViewController, animated: true)
    }
    
    @IBAction private func onBackButtonTouchUpInside(_ sender: Any) {
        self.onBack?()
    }

    // MARK: -

    private func apply(segment: Int) {
        switch segment {
        case Constants.communitySegmentIndex:
            if self.filterType == .none {
                self.showCommunityContainerView()
            } else {
                self.connectionsSearchTableViewController.apply(searchType: .community)
            }

        case Constants.friendsSegmentIndex:
            if self.filterType == .none {                
                self.showFriendsContainerView()
            } else {
                self.connectionsSearchTableViewController.apply(searchType: .friends)
            }

        default:
            fatalError()
        }
    }

    private func apply(filterType: FilterType) {
        self.filterType = filterType // .none

        switch filterType {
        case .search(let text):
            self.connectionsSearchTableViewController.apply(searchText: text)

            self.showConnectionsSearchContainerView()

        case .none:
            switch self.state {
            case .adding:
                 self.showCommunityContainerView()

            case .keeping:
                self.showFriendsContainerView()
            }
        }
    }

    private func apply(state: ConnectionsState) {
        Log.high("apply(state: \(state))", from: self)
        
        self.state = state // .adding
        
        if self.isViewLoaded {
            if self.state == .adding {
                self.apply(segment: 0)
            } else {
                self.apply(segment: 1)
            }
        }
    }
    
    private func updateContentWithCacheData() {
        switch self.state {
        case .adding:
            self.communityTableViewController.updateContentWithCacheData()
            
        case .keeping:
            self.friendsTableViewController.updateContentWithCacheData()
        }
    }

    // MARK: -

    private func showConnectionsSearchContainerView() {
        self.connectionsSearchContainerView.isHidden = false
        self.communityContainerView.isHidden = true
        self.friendsContainerView.isHidden = true
    }

    private func showCommunityContainerView() {
        self.communityContainerView.isHidden = false
        self.connectionsSearchContainerView.isHidden = true
        self.friendsContainerView.isHidden = true
    }

    private func showFriendsContainerView() {
        self.friendsContainerView.isHidden = false
        self.communityContainerView.isHidden = true
        self.connectionsSearchContainerView.isHidden = true
    }

    // MARK: -

    private func onEmbedConnectionsSearchTableViewController(with segue: UIStoryboardSegue) {
        guard let connectionsSearchTableViewController = segue.destination as? ConnectionsSearchTableViewController else {
            fatalError()
        }

        self.connectionsSearchTableViewController = connectionsSearchTableViewController

        connectionsSearchTableViewController.showFriendUser = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendUser, sender: user)
        }

        connectionsSearchTableViewController.showFriendProps = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendProps, sender: user)
        }
    }

    private func onEmbedCommunityTableViewController(with segue: UIStoryboardSegue) {
        guard let communityTableViewController = segue.destination as? CommunityTableViewController else {
            fatalError()
        }
        
        self.communityTableViewController = communityTableViewController

        communityTableViewController.showFriendUser = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendUser, sender: user)
        }

        communityTableViewController.showFriendProps = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendProps, sender: user)
        }

        communityTableViewController.showAllUsers = { [unowned self] userListType in
            self.performSegue(withIdentifier: Segues.showUsers, sender: userListType)
        }
        
        communityTableViewController.showAllOutlineContacts = { [unowned self] in
            self.performSegue(withIdentifier: Segues.showOutlineContacts, sender: self)
        }

        communityTableViewController.unauthorize = { [unowned self] in
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
        }
    }

    private func onEmbedFriendsTableViewController(with segue: UIStoryboardSegue) {
        guard let friendsTableViewController = segue.destination as? FriendsTableViewController else {
            fatalError()
        }
        
        self.friendsTableViewController = friendsTableViewController

        friendsTableViewController.showFriendUser = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendUser, sender: user)
        }

        friendsTableViewController.showFriendProps = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendProps, sender: user)
        }

        friendsTableViewController.showAllUsers = { [unowned self] userListType in
            self.performSegue(withIdentifier: Segues.showUsers, sender: userListType)
        }

        friendsTableViewController.unauthorize = { [unowned self] in
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
        }
    }

    private func configureSearchTextField() {
        self.searchTextField.font = Fonts.regular(ofSize: 14.0)
    }

    private func configureBackButton() {
        self.backButton.isHidden = (self.onBack == nil)
    }
    
    private func configureCurrentStateNavigationBar() {
        let shouldHideNavBar = (self.state == .adding)
        
        self.navigationController?.setNavigationBarHidden(shouldHideNavBar, animated: true)
        
        ///// ********* Lance's Code ********* /////
        if isComingFromAvatarPickerVC {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        }
        ///// ********* Lance's Code ********* /////
    }
    
    private func configureInitialStateNavigationBar() {
        guard let currentState = self.navigationController?.isNavigationBarHidden else {
            return
        }
        
        self.navigationController?.setNavigationBarHidden(!currentState, animated: true)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ///// ********* Lance's Code ********* /////
        if isComingFromAvatarPickerVC {
            doneBarButtonItem = UIBarButtonItem(customView: doneButton)
            navigationItem.rightBarButtonItem = doneBarButtonItem
            topContainerView.isHidden = true
        } else {
            navigationItem.rightBarButtonItem = nil
            topContainerView.isHidden = false
        }
        ///// ********* Lance's Code ********* /////
        
        self.configureBackButton()
        self.configureSearchTextField()
        
        self.hidesBottomBarWhenPushed = (self.state == .keeping)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureCurrentStateNavigationBar()
        
        self.setNeedsStatusBarAppearanceUpdate()

        self.apply(state: self.state)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.configureInitialStateNavigationBar()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        let destinationViewController = segue.destination
        
        switch segue.identifier {
            
            ///// ********* Lance's Code ********* /////
        case Segues.finishAuthorization:
            
            guard let accountUser = sender as? AccountUser else {
                return
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["accountUser": accountUser])
            }
            ///// ********* Lance's Code ********* /////
            
        case Segues.showFriendUser, Segues.showFriendProps:
            guard let user = sender as? User else {
                fatalError()
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["user": user])
            }

            if segue.identifier == Segues.showFriendUser {
                destinationViewController.transitioningDelegate = PanelTransition.userRouting
                destinationViewController.modalPresentationStyle = .custom
            }

        case Segues.showUsers:
            guard let userListType = sender as? UserListType else {
                fatalError()
            }

            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["userListType": userListType])
            }

        case Segues.embedConnectionsSearch:
            self.onEmbedConnectionsSearchTableViewController(with: segue)

        case Segues.embedCommunity:
            self.onEmbedCommunityTableViewController(with: segue)

        case Segues.embedFriends:
            self.onEmbedFriendsTableViewController(with: segue)
            
        default:
            break
        }
    }
}

// MARK: - UITextFieldDelegate

extension ConnectionsContainerViewController: UITextFieldDelegate {

    // MARK: - Instance Methods

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.endEditing(true)

        self.apply(filterType: .none)

        return true
    }
}

// MARK: - DictionaryReceiver

extension ConnectionsContainerViewController: DictionaryReceiver {

    // MARK: - Instance Methods

    func apply(dictionary: [String: Any]) {
        guard let state = dictionary["state"] as? ConnectionsState else {
            return
        }

        self.apply(state: state)
    }
}
