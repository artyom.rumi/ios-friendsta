//
//  ConnectionsSearchTableViewController.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 17/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class ConnectionsSearchTableViewController: LoggedViewController, ErrorMessagePresenter {

    // MARK: - Nested Types

    private enum Constants {

        // MARK: - Type Properties

        static let friendTableCellIdentifier = "FriendTableCell"
        static let communityTableCellIdentifier = "CommunityTableCell"
    }

    // MARK: -

    enum SearchType {

        // MARK: - Enumeration Cases

        case community
        case friends
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var tableView: UITableView!

    @IBOutlet private weak var emptyStateView: EmptyStateView!

    // MARK: -

    private var users: [User] = []
    private var searchText: String?
    private var searchType: SearchType = .community

    private var throttler = Throttler(minimumDelay: 0.25)

    // MARK: -

    var showFriendProps: ((User) -> Void)?
    var showFriendUser: ((User) -> Void)?

    // MARK: - Instance Methods

    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: {
            retryHandler?()
        })

        switch error as? WebError {
        case .some(.connection), .some(.timeOut):
            if self.users.isEmpty {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            } else {
                self.showMessage(withError: error)
            }

        default:
            if self.users.isEmpty {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            } else {
                self.showMessage(withError: error)
            }
        }
    }

    private func showEmptyState(image: UIImage? = nil, title: String? = nil, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()

        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action

        self.emptyStateView.isHidden = false
    }

    private func showLoadingState() {
        self.showEmptyState(title: "Searching users".localized(),
                            message: "We are loading list of users. Please wait a bit.".localized())

        self.emptyStateView.showActivityIndicator()
    }

    private func showNoDataState(for keywords: String) {
        self.showEmptyState(message: String(format: "We couldn’t find anything for “%@”".localized(), keywords))
    }

    private func hideEmptyState() {
        self.emptyStateView.isHidden = true
    }

    // MARK: -

    private func invite(user: User, in cell: CommunityTableViewCell) {
        Log.high("invite(user: \(user.uid))", from: self)

        cell.showActivityIndicator()

        firstly {
            Services.usersService.inviteUser(user: user)
        }.ensure { [weak cell] in
            cell?.hideActivityIndicator()
        }.done { [weak cell, weak self] _ in
            guard let viewController = self, let searchText = viewController.searchText else {
                return
            }

            viewController.users = Services.cacheViewContext.usersManager.searchByFullName(with: searchText)

            cell?.addButtonImage = Images.hoursGlassIcon
        }.catch { error in
            self.showMessage(withError: error)
        }
    }

    private func searchUsers(for searchType: SearchType, by keywords: String) {
        let searchPromise: Promise<[User]>

        switch searchType {
        case .community:
            searchPromise = Services.usersService.searchCommunitiesUsers(by: keywords)

        case .friends:
            searchPromise = Services.usersService.searchFriendsUsers(by: keywords)
        }

        self.showLoadingState()

        firstly {
            searchPromise
        }.done { users in
            self.apply(users: users, keywords: keywords)
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [unowned self] in
                self.searchUsers(for: searchType, by: keywords)
            })
        }
    }

    // MARK: -

    private func apply(users: [User], keywords: String) {
        Log.high("apply(users: \(users.count), keywords: \(keywords))", from: self)

        self.users = users

        if users.isEmpty {
            self.showNoDataState(for: keywords)
        } else {
            self.hideEmptyState()
        }

        self.tableView.reloadData()
    }

    // MARK: -

    func apply(searchText: String) {
        Log.high("apply(searchText: \(searchText))", from: self)

        guard !searchText.isEmpty else {
            return
        }

        self.searchText = searchText

        self.throttler.throttle { [unowned self] in
            self.searchUsers(for: self.searchType, by: searchText)
        }
    }

    func apply(searchType: SearchType) {
        Log.high("apply(searchType: \(searchType))", from: self)

        self.searchType = searchType

        if let searchText = self.searchText {
            self.apply(searchText: searchText)
        }
    }

    // MARK: - UIViewController

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.subscribeToKeyboardNotifications()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.unsubscribeFromKeyboardNotifications()
    }
}

// MARK: - UITableViewDataSource

extension ConnectionsSearchTableViewController: UITableViewDataSource {

    // MARK: - Instance Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.users.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell

        switch self.searchType {
        case .community:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.communityTableCellIdentifier, for: indexPath)

            self.configure(communityCell: cell as! CommunityTableViewCell, at: indexPath)

        case .friends:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.friendTableCellIdentifier, for: indexPath)

            self.configure(friendCell: cell as! FriendTableViewCell, at: indexPath)
        }

        return cell
    }
}

// MARK: - UITableViewDataSource Helpers

extension ConnectionsSearchTableViewController {

    // MARK: - Instance Methods

    private func configure(friendCell cell: FriendTableViewCell, at indexPath: IndexPath) {
        let user = self.users[indexPath.row]

        cell.fullName = user.fullName
        cell.isFriendCountHidden = false

        if user.friendsCount > Limits.maxFriendCount {
            cell.info = "\(Limits.maxFriendCount)+"
        } else {
            cell.info = "\(user.friendsCount)"
        }

        switch user.friendshipStatus {
        case .some(.affirmed):
            cell.info = "Affirmed".localized()
            cell.infoTextColor = Colors.redText

        case .some(.friend):
            if user.daysToExpiredFriendship == 0 {
                cell.info = "Today".localized()
                cell.infoTextColor = Colors.redText
            } else if user.daysToExpiredFriendship == 1 {
                cell.info = String(format: "%d day".localized(), user.daysToExpiredFriendship)
                cell.infoTextColor = Colors.redText
            } else if user.daysToExpiredFriendship <= 2 {
                cell.info = String(format: "%d days".localized(), user.daysToExpiredFriendship)
                cell.infoTextColor = Colors.redText
            } else {
                cell.info = String(format: "%d days".localized(), user.daysToExpiredFriendship)
                cell.infoTextColor = Colors.grayText
            }

        case .some(.invited), .some(.uninvited), .none:
            let schoolTitle = user.schoolTitle ?? "unknown school".localized()

            if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(user.classYear)) {
                cell.info = String(format: "%d grade %@".localized(), schoolGradeNumber, schoolTitle)
            } else {
                cell.info = String(format: "Finished".localized(), schoolTitle)
            }

            cell.infoTextColor = Colors.grayText
            cell.isFriendCountHidden = true
        }
        
        cell.friendshipStatus = user.friendshipStatus
        cell.isKeepControlHidden = (user.friendshipStatus == nil)
        
        cell.onAddTapped = {
            cell.isKeepLoadingActivityIndicatorAnimating = true

            firstly {
                Services.usersService.inviteUser(user: user)
            }.done { user in
                cell.friendshipStatus = user.friendshipStatus
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }.catch { _ in
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }
        }
        
        cell.onKeepTapped = {
            cell.isKeepLoadingActivityIndicatorAnimating = true
            
            firstly {
                Services.usersService.keep(user: user)
            }.done { user in
                cell.friendshipStatus = user.friendshipStatus
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }.catch { _ in
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }
        }
    }

    private func configure(communityCell cell: CommunityTableViewCell, at indexPath: IndexPath) {
        let user = users[indexPath.row]

        cell.avatarImage = nil
        cell.fullName = user.fullName

        let schoolTitle = user.schoolTitle ?? "unknown school".localized()

        if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(user.classYear)) {
            cell.schoolInfo = String(format: "%d grade %@".localized(), schoolGradeNumber, schoolTitle)
        } else {
            cell.schoolInfo = String(format: "Finished".localized(), schoolTitle)
        }

        cell.onAddButtonTapped = { [unowned self] in
            self.invite(user: user, in: cell)
        }

        switch user.friendshipStatus {
        case .some(.invited):
            cell.isAddButtonHidden = false
            cell.addButtonImage = Images.hoursGlassIcon

        case .some(.uninvited):
            cell.isAddButtonHidden = false
            cell.addButtonImage = Images.plusIcon

        case .some(.friend), .some(.affirmed), .none:
            cell.isAddButtonHidden = true
        }
    }
}

// MARK: - UITableViewDelegate

extension ConnectionsSearchTableViewController: UITableViewDelegate {

    // MARK: - Instance Methods

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let smallAvatarURL = self.users[indexPath.row].smallAvatarURL else {
            return
        }

        if let cell = cell as? FriendTableViewCell {
            Services.imageLoader.loadImage(for: smallAvatarURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarSmallPlaceholder)
        } else if let cell = cell as? CommunityTableViewCell {
            Services.imageLoader.loadImage(for: smallAvatarURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarSmallPlaceholder)
        }
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? FriendTableViewCell {
            Services.imageLoader.cancelLoading(in: cell.avatarImageViewTarget)
        } else if let cell = cell as? CommunityTableViewCell {
            Services.imageLoader.cancelLoading(in: cell.avatarImageViewTarget)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        let user = self.users[indexPath.row]

        self.showFriendUser?(user)
    }
}

// MARK: - KeyboardScrollableHandler

extension ConnectionsSearchTableViewController: KeyboardScrollableHandler {

    // MARK: - Instance Properties

    var scrollableView: UITableView {
        return self.tableView
    }
}
