//
//  OutlineContactsTableViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 01/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import SwiftKeychainWrapper

import Contacts

class OutlineContactsTableViewController: LoggedViewController, ErrorMessagePresenter {
    
    ///// ********* Lance's Code ********* /////
    public var isComingFromChatTVC = false
    public var chatSessionId: String?
    public var timerHrs: Int?
    public var timerMins: Int?
    public var timerSecs: Int?
    public var revealTimerText: String? {
        didSet {
            
            if let hours = timerHrs, let minutes = timerMins, let seconds = timerSecs {
                
                if !wasRevealTimerTextSet {
                    wasRevealTimerTextSet = true
                    
                    let hrStr = String(hours)
                    let minStr = String(minutes)
                    let secStr = String(seconds)
                    
                    let hoursStr = hours > 9 ? "\(hrStr)" : "0\(hrStr)"
                    let minutesStr = minutes > 9 ? "\(minStr)" : "0\(minStr)"
                    let secondsStr = seconds > 9 ? "\(secStr)" : "0\(secStr)"
                    
                    revealTimerText = "\(hoursStr)h:\(minutesStr)m:\(secondsStr)s"
                    
                    //revealTimerText = "\(hoursStr):\(minutesStr)"
                    
                    //revealTimerText = "\(hoursStr)"
                }
            }
        }
    }
    
    fileprivate var wasRevealTimerTextSet = false
    fileprivate var revealTimer: Timer?
    fileprivate func startRevealTimer() {
        revealTimer?.invalidate()
        revealTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDownDate), userInfo: nil, repeats: true)
        if let revealTimer = revealTimer {
            RunLoop.current.add(revealTimer, forMode: RunLoop.Mode.common)
        }
    }
    
    fileprivate func invalidateRevealTimer() {
        revealTimer?.invalidate()
        revealTimer = nil
    }
    
    @objc fileprivate func countDownDate() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        guard let chatSessionId = chatSessionId,
            let revealDateAsDouble = KeychainWrapper.standard.double(forKey: chatSessionId) else {
            invalidateTimerDismissVC()
            return
        }
        
        let revealDate: Date = Date(timeIntervalSince1970: revealDateAsDouble)
        
        let currentDate = Date()
        let calendar = Calendar.current
        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: revealDate)
        
        let countdown = "Days \(diffDateComponents.day),  Hours: \(diffDateComponents.hour), Minutes: \(diffDateComponents.minute), Seconds: \(diffDateComponents.second)"
        //print(countdown)
        
        timerHrs = diffDateComponents.hour
        timerMins = diffDateComponents.minute
        timerSecs = diffDateComponents.second
        
        if let hours = diffDateComponents.hour, let minutes = diffDateComponents.minute, let seconds = diffDateComponents.second {
            
            let hrStr = String(hours)
            let minStr = String(minutes)
            let secStr = String(seconds)
            
            let hoursStr = hours > 9 ? "\(hrStr)" : "0\(hrStr)"
            let minutesStr = minutes > 9 ? "\(minStr)" : "0\(minStr)"
            let secondsStr = seconds > 9 ? "\(secStr)" : "0\(secStr)"
            
            revealTimerText = "\(hoursStr)h \(minutesStr)m \(secondsStr)s"
            
            //revealTimerText = "\(hoursStr):\(minutesStr)"
            
            //revealTimerText = "\(hoursStr)"
            
            topRevealMessageLabel.attributedText = updateTextInTopRevealMessageLabel()
            bottomRevealMessageLabel.attributedText = updateTextInBottomRevealMessageLabel()
        }
        
        if currentDate >= revealDate {
            
            NotificationCenter.default.post(name: Notification.Name("removeRevealButtonInChatMessagesTVC"), object: nil)
            
            invalidateTimerDismissVC()
            
        } else {
            
            print("OutlineContactsTVC -reveal time hasn't happened yet")
        }
    }
    
    fileprivate func configureNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(inviteSentStopTimerDismissOutlineContactsTVC),
                                               name: NSNotification.Name(rawValue: "inviteSentStopTimerDismissOutlineContactsTVC"),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(appHasEnteredBackground), name: UIApplication.willResignActiveNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc fileprivate func inviteSentStopTimerDismissOutlineContactsTVC() {
        
        if !(self.isViewLoaded) { return }
        
        invalidateTimerDismissVC()
    }
    
    fileprivate func invalidateTimerDismissVC() {
        invalidateRevealTimer()
        
        dismiss(animated: true, completion: nil)
    }
    
    @objc fileprivate func appHasEnteredBackground() {
        
        if isComingFromChatTVC {
            invalidateRevealTimer()
        }
    }
    
    @objc fileprivate func appWillEnterForeground() {
        
        if isComingFromChatTVC {
            
            determineIfContactsWereAllowed()
            
            startRevealTimer()
        }
    }
    
    fileprivate lazy var topRevealMessageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.attributedText = updateTextInTopRevealMessageLabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    
    fileprivate lazy var bottomRevealMessageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.attributedText = updateTextInBottomRevealMessageLabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.sizeToFit()
        return label
    }()
    
    fileprivate func updateTextInTopRevealMessageLabel() -> NSMutableAttributedString? {
        
        guard let timerHrs = timerHrs, let timerMins = timerMins, let timerSecs = timerSecs else {
            let safeAttributedText = NSMutableAttributedString(string: "Invite a friend to immediately reveal!",
            attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17), NSAttributedString.Key.foregroundColor: UIColor.black])
            return safeAttributedText
        }
        
        let hrsToStr = String(timerHrs)
        let minsToStr = String(timerMins)
        let secsToStr = String(timerSecs)
        var displayHrsOrMinsOrSecsText = hrsToStr
        
        let revealTime = "\(revealTimerText ?? "a few")"
        var hrsOrMinutesOrSecs = "hours"
        
        if timerHrs < 1 {
            hrsOrMinutesOrSecs = "minutes"
            displayHrsOrMinsOrSecsText = minsToStr
        }
        
        if timerMins < 1 {
            hrsOrMinutesOrSecs = "seconds"
            displayHrsOrMinsOrSecsText = secsToStr
        }
        
        // Come back in 12 hours!
        let boldFont = UIFont.boldSystemFont(ofSize: 23)
        let monoFontBold = UIFont.monospacedDigitSystemFont(ofSize: 19.5, weight: UIFont.Weight.heavy)
        
        let attributedText = NSMutableAttributedString(string: "Come back in ",
                                                       attributes: [NSAttributedString.Key.font: boldFont,
                                                                    NSAttributedString.Key.foregroundColor: UIColor.black])
        
        // timer
        let attrText1 = NSAttributedString(string: "\(revealTime)!",
                                           attributes: [NSAttributedString.Key.font: monoFontBold,
                                                        NSAttributedString.Key.foregroundColor: UIColor.black])
        
        // hrsOrMinutesOrSecs
        let attrText2 = NSAttributedString(string: " \(hrsOrMinutesOrSecs)!",
            attributes: [NSAttributedString.Key.font: boldFont,
                         NSAttributedString.Key.foregroundColor: UIColor.black])
        
        attributedText.append(attrText1)
        //attributedText.append(attrText2)
        
        return attributedText
    }
    
    fileprivate func updateTextInBottomRevealMessageLabel() -> NSMutableAttributedString? {
        
        guard let timerHrs = timerHrs, let timerMins = timerMins, let timerSecs = timerSecs else {
            let safeAttributedText = NSMutableAttributedString(string: "", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 0.1)])
            return safeAttributedText
        }
        
        let hrsToStr = String(timerHrs)
        let minsToStr = String(timerMins)
        let secsToStr = String(timerSecs)
        var displayHrsOrMinsOrSecsText = hrsToStr
        
        let revealTime = "\(revealTimerText ?? "a few")"
        var hrsOrMinutesOrSecs = "hours"
        
        if timerHrs < 1 {
            hrsOrMinutesOrSecs = "minutes"
            displayHrsOrMinsOrSecsText = minsToStr
        }
        
        if timerMins < 1 {
            hrsOrMinutesOrSecs = "seconds"
            displayHrsOrMinsOrSecsText = secsToStr
        }
        
        // You both agreed to reveal, please come back in 12 hours to see who this was! Can’t wait? Invite friends!
        let regularFont = UIFont.systemFont(ofSize: 17, weight: .medium)
        let monoFontRegular = UIFont.monospacedDigitSystemFont(ofSize: 17, weight: UIFont.Weight.medium)
        
        let attributedText = NSMutableAttributedString(string: "You both agreed to reveal, please come back in ",
            attributes: [NSAttributedString.Key.font: regularFont,
                         NSAttributedString.Key.foregroundColor: UIColor.black])
        
        // timer
        let attrText1 = NSAttributedString(string: revealTime,
                                           attributes: [NSAttributedString.Key.font: monoFontRegular,
                                                        NSAttributedString.Key.foregroundColor: UIColor.black])
        
        // hrsOrMinutesOrSecs
        let attrText2 = NSAttributedString(string: " \(hrsOrMinutesOrSecs)",
            attributes: [NSAttributedString.Key.font: regularFont,
                         NSAttributedString.Key.foregroundColor: UIColor.black])
        
        let attrText3 = NSAttributedString(string: " to see who this was! Can’t wait?\nInvite a friend!",
                                           attributes: [NSAttributedString.Key.font: regularFont,
                                                        NSAttributedString.Key.foregroundColor: UIColor.black])
        
        attributedText.append(attrText1)
        //attributedText.append(attrText2)
        attributedText.append(attrText3)
        
        return attributedText
    }
    
    fileprivate func configureBothRevealMessageLabelsAnchors() {
        view.addSubview(topRevealMessageLabel)
        view.addSubview(bottomRevealMessageLabel)
        
        topRevealMessageLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        topRevealMessageLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        topRevealMessageLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        
        bottomRevealMessageLabel.topAnchor.constraint(equalTo: topRevealMessageLabel.bottomAnchor, constant: 10).isActive = true
        bottomRevealMessageLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 25).isActive = true
        bottomRevealMessageLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -25).isActive = true
    }
    
    @IBOutlet fileprivate weak var containerViewForSearchTextField: UIView!
    @IBOutlet fileprivate weak var containerViewForSearchTextFieldTopConstraint: NSLayoutConstraint!
    
    fileprivate lazy var cancelButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(named: "BlackCloseButton"), for: .normal)
        button.tintColor = .black
        button.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        return button
    }()

    @objc fileprivate func cancelButtonPressed() {
        dismiss(animated: true, completion: nil)
    }
    
    fileprivate var cancelBarButtonItem: UIBarButtonItem?
    
    fileprivate lazy var allowContactsButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Allow access to your contacts", for: .normal)
        button.setTitleColor(UIColor.darkText, for: .normal)
        
        button.backgroundColor = UIColor.white
        button.addTarget(self, action: #selector(allowContactsButtonPressed), for: .touchUpInside)
        
        button.layer.cornerRadius = 15
        button.sizeToFit()
        
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 7.5, bottom: 0, right: 7.5)
        
        button.titleLabel?.font = Fonts.semiBold(ofSize: 17)
        button.layer.shadowColor = UIColor.black.cgColor
        button.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        button.layer.shadowRadius = 1
        button.layer.shadowOpacity = 0.75
        button.layer.masksToBounds = false
        
        return button
    }()
    
    fileprivate func addAllowContactsButtonAnchors() {
        view.addSubview(allowContactsButton)
        allowContactsButton.topAnchor.constraint(equalTo: containerViewForSearchTextField.bottomAnchor, constant: 25).isActive = true
        allowContactsButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        allowContactsButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        view.bringSubviewToFront(allowContactsButton)
    }
    
    fileprivate func showEmptyStateContainerViewShowAllowContactsButton() {
        
        emptyStateView.hideActivityIndicator()
        
        //tableView.isHidden = true
        emptyStateContainerView.isHidden = false
        allowContactsButton.isHidden = false
    }
    
    fileprivate func hideEmptyStateContainerViewHideAllowContactsButton() {
        //tableView.isHidden = false
        emptyStateContainerView.isHidden = true
        allowContactsButton.isHidden = true
    }
    
    @objc fileprivate func determineIfContactsWereAllowed() {
        
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .notDetermined, .denied, .restricted:
            showEmptyStateContainerViewShowAllowContactsButton()
            
        case .authorized:
            hideEmptyStateContainerViewHideAllowContactsButton()
            
        @unknown default:
            break
        }
        /*
        switch Services.contactsService.accessState {

        case .notDetermined, .denied, .restricted:
            showEmptyStateContainerViewShowAllowContactsButton()

        case .authorized:
            hideEmptyStateContainerViewHideAllowContactsButton()
        }
        */
    }
    
    @objc fileprivate func allowContactsButtonPressed() {
        
        let authorizationStatus = CNContactStore.authorizationStatus(for: CNEntityType.contacts)
        
        switch authorizationStatus {
        case .notDetermined:
            requestContactsAccess()
            
        case .denied, .restricted:
            showAccessRequest()
            
        case .authorized:
            hideEmptyStateContainerViewHideAllowContactsButton()
            
        @unknown default:
            break
        }
        
        /*
        switch Services.contactsService.accessState {
        case .notDetermined:
            requestContactsAccess()

        case .denied, .restricted:
            showAccessRequest()

        case .authorized:
            hideEmptyStateContainerViewHideAllowContactsButton()
        }
        */
    }
    
    fileprivate func requestContactsAccess() {
        Log.high("requestContactsAccess()", from: self)

        firstly {
            Services.contactsService.requestAccess()
        }.done { [weak self](accessState) in
            switch accessState {
            case .notDetermined, .denied, .restricted:
                self?.showEmptyStateContainerViewShowAllowContactsButton()

            case .authorized:
                self?.hideEmptyStateContainerViewHideAllowContactsButton()
            }
        }
    }
    
    fileprivate func showAccessRequest() {
        let alertController = UIAlertController(title: "Please allow access".localized(),
                                                message: "The app needs access to your contacts.\n\nPlease go to Settings and set to ON.".localized(),
                                                preferredStyle: .alert)

        alertController.view.tintColor = Colors.primary

        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))

        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))

        present(alertController, animated: true)
    }
    
    ///// ********* Lance's Code ********* /////
    
    // MARK: - Nested Types
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let outlineContactTableCellIdentifier = "OutlineContactTableCell"
    }
    
    // MARK: -
    
    private enum FilterType {
        
        // MARK: - Enumeration Cases
        
        case none
        case search(text: String)
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var searchTextField: SearchTextField!
    
    @IBOutlet private weak var tableView: UITableView!
    
    @IBOutlet private weak var emptyStateContainerView: UIView!
    @IBOutlet private weak var emptyStateView: EmptyStateView!
    
    // MARK: -
    
    private var filterType: FilterType = .none
    
    private var outlineContactList: OutlineContactList!
    
    private var filteredOutlineContacts: [OutlineContact] = []
    
    // MARK: -
    
    private var shouldApplyData = true
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods

    @IBAction private func onSearchTextFieldEditingChanged(_ sender: Any) {
        Log.high("onSearchTextFieldEditingChanged()", from: self)
        
        if let searchText = searchTextField.text, !searchText.isEmpty {
            apply(filterType: .search(text: searchText), outlineContactList: outlineContactList)
        } else {
            apply(filterType: .none, outlineContactList: outlineContactList)
        }
    }
    
    // MARK: -
    
    private func showEmptyState(image: UIImage? = nil, title: String, message: String? = nil, action: EmptyStateAction? = nil) {
        emptyStateView.hideActivityIndicator()
        
        //emptyStateView.image = image
        //emptyStateView.title = title
        //emptyStateView.message = message
        //emptyStateView.action = action
        
        emptyStateContainerView.isHidden = false
        
        allowContactsButton.isHidden = false
    }
    
    private func hideEmptyState() {
        emptyStateContainerView.isHidden = true
        
        allowContactsButton.isHidden = true
    }
    
    private func showNoDataState() {
        showEmptyState(title: "Contacts not found".localized())
    }
    
    private func showLoadingState() {
        if emptyStateContainerView.isHidden {
            showEmptyState(title: "Loading contacts".localized(),
                                message: "We are loading list of contacts. Please wait a bit".localized())
        }
        
        emptyStateView.showActivityIndicator()
    }
    
    // MARK: -
    
    private func invite(outlineContact: OutlineContact, at cell: OutlineContactTableViewCell) {
        cell.showActivityIndicator()
        
        firstly {
            Services.invitationService.sendInvite(to: [outlineContact])
        }.ensure { [weak cell] in
            cell?.hideActivityIndicator()
        }.done { [weak cell] in
            cell?.invitationState = .recentlyInvited
        }.catch { [weak self](error) in
            self?.showMessage(withError: error)
        }
    }
    
    private func refreshOulineContactList() {
        Log.high("refreshUserList()", from: self)
        
        if (outlineContactList.allOutlineContacts.isEmpty) || (emptyStateContainerView.isHidden) {
            showLoadingState()
        }
        
        firstly {
            Services.outlineContactsService.refreshAllOutlineContacts()
        }.done { [weak self](outlineContactList) in
            self?.outlineContactList = outlineContactList
            
            if let safeSelf = self {
                self?.apply(filterType: safeSelf.filterType, outlineContactList: outlineContactList)
            }
            
        }.catch { [weak self](error) in
            self?.showMessage(withError: error, okHandler: { [unowned self] in
                self?.refreshOulineContactList()
            })
        }
    }
    
    // MARK: -
    
    private func configure(outlineContactCell cell: OutlineContactTableViewCell, at indexPath: IndexPath) {
        let outlineContact = filteredOutlineContacts[indexPath.row]
        
        cell.fullName = outlineContact.fullName ?? outlineContact.phoneNumber
        cell.phoneNumber = outlineContact.phoneNumber
        cell.invitationState = outlineContact.isInvited ? .invited : .uninvited
        
        cell.onActionButtonTouchUpInside = { [weak self] in
            self?.invite(outlineContact: outlineContact, at: cell)
        }
    }
    
    // MARK: -

    private func apply(filterType: FilterType, outlineContactList: OutlineContactList) {
        Log.high("apply(filterType: \(filterType), outlineContactList: \(outlineContactList.allOutlineContacts.count))", from: self)
        
        self.filterType = filterType
        self.outlineContactList = outlineContactList
        
        if isViewLoaded {
            switch filterType {
            case .none:
                filteredOutlineContacts = outlineContactList.allOutlineContacts
                
            case .search(let text):
                let searchText = text.lowercased()
                
                filteredOutlineContacts = outlineContactList
                    .allOutlineContacts
                    .filter { $0.fullName?.lowercased().contains(searchText) ?? false }
            }
            
            determineIfContactsWereAllowed()
            
//            if outlineContactList.allOutlineContacts.isEmpty {
//                showNoDataState()
//            } else {
//                hideEmptyState()
//            }
            
            if isViewAppeared {
                tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .fade)
            } else {
                tableView.reloadData()
            }
            
            shouldApplyData = false
        } else {
            shouldApplyData = true
        }
    }
    
    // MARK: - View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isComingFromChatTVC {
            
            if #available(iOS 13.0, *) {
            } else {
                cancelBarButtonItem = UIBarButtonItem(customView: cancelButton)
                navigationItem.leftBarButtonItem = cancelBarButtonItem
            }
            
            configureNotifications()
            
            configureBothRevealMessageLabelsAnchors()
            startRevealTimer()
            
            containerViewForSearchTextFieldTopConstraint.isActive = false
            containerViewForSearchTextFieldTopConstraint = containerViewForSearchTextField.topAnchor.constraint(equalTo: bottomRevealMessageLabel.bottomAnchor, constant: 0)
            containerViewForSearchTextFieldTopConstraint.isActive = true
            
            //determineIfContactsWereAllowed()
        }
        
        addAllowContactsButtonAnchors()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        subscribeToKeyboardNotifications()
        
        if shouldApplyData {
            let outlineContacts = Services.cacheViewContext.outlineContactListManager.firstOrNew(withListType: .all)
            
            apply(filterType: self.filterType, outlineContactList: outlineContacts)
            
            refreshOulineContactList()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        unsubscribeFromKeyboardNotifications()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        invalidateRevealTimer()
    }
    
    // MARK: - Deinit
    deinit {
        print("OutlineContactsTVC -DEINIT")
    }
}

// MARK: - UITableViewDataSource

extension OutlineContactsTableViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredOutlineContacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.outlineContactTableCellIdentifier, for: indexPath) as? OutlineContactTableViewCell else {
            fatalError("Can't init OutlineContactTableViewCell")
        }
        
        configure(outlineContactCell: cell, at: indexPath)
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension OutlineContactsTableViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchTextField.endEditing(true)
    }
}

// MARK: - KeyboardScrollableHandler

extension OutlineContactsTableViewController: KeyboardScrollableHandler {
    
    // MARK: - Instance Properties
    
    var scrollableView: UITableView {
        return tableView
    }
}
