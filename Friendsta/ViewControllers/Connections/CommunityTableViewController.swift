//
//  CommunityTableViewController.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 16/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

// swiftlint:disable file_length
// swiftlint:disable type_body_length
// swiftlint:disable function_body_length
// swiftlint:disable vertical_whitespace

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class CommunityTableViewController: LoggedViewController, ErrorMessagePresenter {
    
    ///// ********* Lance's Code ********* /////
    fileprivate var isComingFromAvatarPickerVC = false {
        didSet {
            
        }
    }
    
    ///// ********* Lance's Code ********* /////
    
    // MARK: - Nested Types

    private typealias SectionData = (section: Section, list: ConnectionList)

    // MARK: -

    private enum Constants {

        // MARK: - Type Properties

        static let showMoreTableCellIdentifier = "ShowMoreTableCell"
        static let communityTableCellIdentifier = "CommunityTableCell"
        static let allowContactTableCellIdentifier = "AllowContactTableCell"
        static let outlineContactTableCellIdentifier = "OutlineContactTableCell"

        static let connectionsTableSectionHeaderIdentifier = "ConnectionsTableSectionHeader"
        
        static let friendRequestGuideTitle = "See who likes you".localized()
        static let friendRequestGuideViewTitle = "Add your friends anonymously.  If they also add you, you will be connected!"	.localized()
        static let friendRequestGuideViewButtonTitle = "Got it!".localized()
        
        static let pokeGuideTitle = "Poke Anyone Anonymously".localized()
        static let pokeGuideDescriptionTitle = "They can reply with text or selfie".localized()
        static let pokeGuideViewTitle = "Tap to poke someone".localized()
        static let pokeGuideViewButtonTitle = "Ok!".localized()

        static let showMoreCellCount = 1
    }
    
    // MARK: -
    
    private enum Section {
        
        // MARK: - Enumeration Cases
        
        case inlineContacts
        case classmates
        case outlineContacts
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var emptyStateView: EmptyStateView!
        
    @IBOutlet private weak var headerGuideTitleLabel: UILabel!
    
    @IBOutlet private weak var guideView: RectangeCloudView!
    @IBOutlet private weak var guideViewTrailingConstraint: NSLayoutConstraint!
    
    private weak var tableRefreshControl: UIRefreshControl!

    // MARK: -

    private var sections: [SectionData] = []

    // MARK: -

    private var isRefreshingData = false
    private var shouldRefreshData = false

    // MARK: -

    private var isAllListEmpty: Bool {
        return self.sections.isEmpty
    }

    private var isContactsAccessAllowed: Bool {
        switch Services.contactsService.accessState {
        case .authorized:
            return true

        case .denied, .notDetermined, .restricted:
            return false
        }
    }
    
    private var friendRequestGuidePointerTrailing: CGFloat?

    // MARK: -

    var showFriendProps: ((User) -> Void)?
    var showFriendUser: ((User) -> Void)?
    var sendCustomProp: ((User) -> Void)?
    var showAllUsers: ((UserListType) -> Void)?
    var showAllOutlineContacts: (() -> Void)?
    var unauthorize: (() -> Void)?

    // MARK: - Initializers

    deinit {
        self.unsubscribeFromAccountUserSchoolEvents()
    }

    // MARK: - Instance Methods

    @objc private func onTableRefreshControlRequested(_ sender: Any) {
        Log.high("onTableRefreshControlRequested()", from: self)

        self.refreshUserList()
    }

    // MARK: -

    private func showLoadingState() {
        if self.emptyStateView.isHidden {
            let title = "Searching your community members".localized()
            let message = "We are loading list of your community members. Please wait a bit.".localized()

            self.showEmptyState(title: title, message: message)
        }

        self.emptyStateView.showActivityIndicator()
    }

    private func showNoDataState() {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: { [unowned self] in
            self.refreshUserList()
        })

        self.showEmptyState(title: "You are an early adopter".localized(),
                            message: "Your schoolmates haven't joined yet.  We will notify you when they do! In the interim, click Done and add friends from your contacts.".localized(),
                            action: action)
    }

    private func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()

        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action

        self.emptyStateView.isHidden = false
    }

    private func hideEmptyState() {
        self.emptyStateView.isHidden = true
    }

    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: {
            retryHandler?()
        })
        
        switch error as? WebError {
            
        case .some(.unauthorized):
            
            self.unauthorize?()
            print(error.localizedDescription)
            
        case .some(.connection), .some(.timeOut):
            
            if self.isAllListEmpty {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            }
            print(error.localizedDescription)
            
        default:
            if self.isAllListEmpty {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
            print(error.localizedDescription)
        }
    }
    
    // MARK: -
    
    private func refreshUserList() {
        Log.high("refreshUserList()", from: self)
        
        self.isRefreshingData = true
        
        if !self.tableRefreshControl.isRefreshing {
            if self.isAllListEmpty || (!self.emptyStateView.isHidden) || self.shouldRefreshData {
                self.showLoadingState()
            }
        }

        let userService = Services.usersService
        let outlineContactsService = Services.outlineContactsService

        firstly {
            when(fulfilled: outlineContactsService.refreshOutlineContacts(),
                 userService.refreshInvitationSchoolmates(),
                 userService.refreshInvitationInlineContacts()
            )
        }.ensure {
            if self.tableRefreshControl.isRefreshing {
                self.tableRefreshControl.endRefreshing()
            }

            self.isRefreshingData = false
        }.done { resutls in
            self.apply(outlineContactList: resutls.0, classmatesUserList: resutls.1, inlineContactsUserList: resutls.2)
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshUserList()
            })
        }
    }

    private func loadMore(of userList: UserList, at sectionIndex: Int, showMoreCell cell: ShowMoreTableViewCell) {
        Log.high("loadMore(of userList: \(userList.listType))", from: self)

        cell.showActivityIndicator()

        firstly {
            Services.usersService.loadMoreUsers(of: userList)
        }.ensure {
            cell.hideActivityIndicator()
        }.done { userList in
            self.sections[sectionIndex] = (self.sections[sectionIndex].section, userList)

            self.tableView.reloadData()

            DispatchQueue.main.async(execute: {
                let indexPath = IndexPath(row: self.fetchNumberOfRows(in: sectionIndex) - 1, section: sectionIndex)

                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            })
        }.catch { error in
            self.handle(stateError: error)
        }
    }

    private func loadMore(of outlineContactList: OutlineContactList, at sectionIndex: Int, showMoreCell cell: ShowMoreTableViewCell) {
        Log.high("loadMore(of outlineContactList: \(outlineContactList.listType))", from: self)

        cell.showActivityIndicator()

        firstly {
            Services.outlineContactsService.loadMoreOutlineContacts(of: outlineContactList)
        }.ensure {
            cell.hideActivityIndicator()
        }.done { outlineContactList in
            self.sections[sectionIndex] = (self.sections[sectionIndex].section, outlineContactList)

            self.tableView.reloadData()

            DispatchQueue.main.async(execute: {
                let indexPath = IndexPath(row: self.fetchNumberOfRows(in: sectionIndex) - 1, section: sectionIndex)

                self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            })
        }.catch { error in
            self.handle(stateError: error)
        }
    }

    private func sendInviteTo(user: User, in cell: CommunityTableViewCell) {
        Log.high("sendInviteTo(user: \(user.uid))", from: self)

        cell.showActivityIndicator()

        firstly {
            Services.usersService.inviteUser(user: user)
        }.ensure {
            cell.hideActivityIndicator()
        }.done { _ in
            cell.addButtonImage = Images.hoursGlassIcon
        }.catch { error in
            self.handle(stateError: error)
        }
    }

    // MARK: -

    private func loadDataFromCache() {
        Log.high("loadDataFromCache()", from: self)

        let outlineContactList = Services.cacheViewContext.outlineContactListManager.firstOrNew(withListType: .paginal)
        let classmatesUserList = Services.cacheViewContext.userListManager.firstOrNew(withListType: .invitationSchoolmates)
        let inlineContactsUserList = Services.cacheViewContext.userListManager.firstOrNew(withListType: .invitationInlineContacts)

        self.apply(outlineContactList: outlineContactList, classmatesUserList: classmatesUserList, inlineContactsUserList: inlineContactsUserList)
    }

    // MARK: -

    private func apply(outlineContactList: OutlineContactList, classmatesUserList: UserList, inlineContactsUserList: UserList) {
        Log.high("apply(outlineContacts: \(outlineContactList.allOutlineContacts.count), classmatesUserList: \(classmatesUserList.allUsers.count), inlineContactsUserList: \(inlineContactsUserList.allUsers.count))", from: self)
        
        self.sections.removeAll()

        if !inlineContactsUserList.allUsers.isEmpty {
            let sectionData: SectionData = (.inlineContacts, inlineContactsUserList)

            self.sections.append(sectionData)
        }
        
        if !classmatesUserList.allUsers.isEmpty {
            let sectionData: SectionData = (.classmates, classmatesUserList)

            self.sections.append(sectionData)
        }
        
        if !outlineContactList.allOutlineContacts.isEmpty {
            let sectionData: SectionData = (.outlineContacts, outlineContactList)

            self.sections.append(sectionData)
        }

        if self.isAllListEmpty {
            self.showNoDataState()
        } else {
            self.hideEmptyState()
        }

        Managers.userDefaultsManager.isUserHasSeenCommunityScreen = true

        self.shouldRefreshData = false

        self.tableView.reloadData()
    }

    // MARK: -

    private func fetchUser(at indexPath: IndexPath) -> User? {
        guard let userList = self.sections[indexPath.section].list as? UserList else {
            fatalError()
        }
        
        return userList[indexPath.row]
    }

    private func fetchNumberOfRows(in sectionIndex: Int) -> Int {
        let section = self.sections[sectionIndex].section
        
        let connectionList = self.sections[sectionIndex].list
        
        if section == .outlineContacts, !self.isContactsAccessAllowed {
            return 1
        } else {
            if connectionList.hasNextPage {
                return connectionList.itemCount + Constants.showMoreCellCount
            } else {
                return connectionList.itemCount
            }
        }
    }

    // MARK: -

    private func requestContactsAccess() {
        Log.high("requestContactsAccess()", from: self)

        firstly {
            Services.contactsService.requestAccess()
        }.done { accessState in
            switch accessState {
            case .notDetermined, .denied, .restricted:
                break

            case .authorized:
                self.refreshUserList()
            }
        }
    }

    private func showAccessRequest() {
        let alertController = UIAlertController(title: "Please allow access".localized(),
                                                message: "The app needs access to your contacts.\n\nPlease go to Settings and set to ON.".localized(),
                                                preferredStyle: .alert)

        alertController.view.tintColor = Colors.primary

        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))

        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }

            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))

        self.present(alertController, animated: true)
    }

    // MARK: -

    private func subscribeToAccountUserSchoolEvents() {
        self.unsubscribeFromAccountUserSchoolEvents()

        let accountUsersManager = Services.cacheViewContext.accountUsersManager

        accountUsersManager.objectsSchoolUpdatedEvent.connect(self, handler: { [weak self] accountUsers in
            self?.shouldRefreshData = true
        })

        accountUsersManager.startObserving()
    }

    private func unsubscribeFromAccountUserSchoolEvents() {
        Services.cacheViewContext.accountUsersManager.objectsSchoolUpdatedEvent.disconnect(self)
    }

    // MARK: -

    private func configureTableRefreshControl() {
        let tableRefreshControl = UIRefreshControl()

        tableRefreshControl.addTarget(self,
                                      action: #selector(self.onTableRefreshControlRequested(_:)),
                                      for: .valueChanged)

        self.tableView.refreshControl = tableRefreshControl

        self.tableRefreshControl = tableRefreshControl
    }
    
    private func configureGuide(addButtonCenterTrailing: CGFloat) {
        let friendRequestGuidePointerTrailing = addButtonCenterTrailing - self.guideViewTrailingConstraint.constant
        
        self.friendRequestGuidePointerTrailing = friendRequestGuidePointerTrailing
        
        if !Managers.userDefaultsManager.isUserHasSeenFriendRequestGuide {
            self.guideView.gradientColors = [Colors.lightBlueGuideView.cgColor,
                                             Colors.blueGuideView.cgColor]
            self.guideView.title = Constants.friendRequestGuideViewTitle
            self.guideView.buttonTitle = Constants.friendRequestGuideViewButtonTitle
            self.guideView.pointerTrailingLength = friendRequestGuidePointerTrailing
            
            self.headerGuideTitleLabel.text = Constants.friendRequestGuideTitle
            
            self.guideView.onActionButtonClicked = {
                Managers.userDefaultsManager.isUserHasSeenFriendRequestGuide = true
                
                self.configureGuide(addButtonCenterTrailing: friendRequestGuidePointerTrailing)
                
                self.tableView.reloadData()
            }
        } else {
            self.tableView.tableHeaderView = nil
        }
        
        self.guideView.setNeedsDisplay()
    }
    
    // MARK: -
    
    func updateContentWithCacheData() {
        self.loadDataFromCache()
    }

    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureTableRefreshControl()

        self.subscribeToAccountUserSchoolEvents()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if Managers.userDefaultsManager.isUserHasSeenCommunityScreen, !self.shouldRefreshData {
            self.loadDataFromCache()
        } else {
            self.refreshUserList()
        }
    }
}

// MARK: - UITableViewDataSource

extension CommunityTableViewController: UITableViewDataSource {

    // MARK: - Instance Methods

    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fetchNumberOfRows(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        let section = self.sections[indexPath.section].section
        
        let connectionList = self.sections[indexPath.section].list
        
        if indexPath.row == connectionList.itemCount {
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.showMoreTableCellIdentifier, for: indexPath)
            
            self.configure(showMoreCell: cell as! ShowMoreTableViewCell, at: indexPath)
        } else {
            if section == .outlineContacts {
                if self.isContactsAccessAllowed {
                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.outlineContactTableCellIdentifier, for: indexPath)
                    
                    self.configure(outlineContactCell: cell as! OutlineContactTableViewCell, at: indexPath)
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.allowContactTableCellIdentifier, for: indexPath)
                    
                    self.configure(allowContactCell: cell as! AllowContactTableViewCell)
                }
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.communityTableCellIdentifier, for: indexPath)
                
                self.configure(communityCell: cell as! CommunityTableViewCell, at: indexPath)
            }
        }

        return cell
    }
}

// MARK: - UITableViewDataSource Helpers

extension CommunityTableViewController {

    // MARK: - Instance Methods

    private func configure(communityCell cell: CommunityTableViewCell, at indexPath: IndexPath) {
        guard let userList = self.sections[indexPath.section].list as? UserList else {
            fatalError()
        }

        let user = userList[indexPath.row]

        cell.avatarImage = nil
        cell.fullName = user.fullName

        let schoolTitle = user.schoolTitle ?? "unknown school".localized()

        if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(user.classYear)) {
            cell.schoolInfo = String(format: "%d grade %@".localized(), schoolGradeNumber, schoolTitle)
        } else {
            cell.schoolInfo = String(format: "Finished".localized(), schoolTitle)
        }

        cell.onAddButtonTapped = { [unowned self] in
            self.sendInviteTo(user: user, in: cell)
        }

        switch user.friendshipStatus {
        case .some(.invited):
            cell.isAddButtonHidden = false
            cell.addButtonImage = Images.hoursGlassIcon

        case .some(.uninvited):
            cell.isAddButtonHidden = false
            cell.addButtonImage = Images.plusIcon

        case .some(.friend), .some(.affirmed), .none:
            cell.isAddButtonHidden = true
        }
    }

    private func configure(showMoreCell cell: ShowMoreTableViewCell, at indexPath: IndexPath) {
        let connectionList = self.sections[indexPath.section].list

        switch connectionList {
        case let userList as UserList:
            cell.showMoreButtonTitle = "Show \(min(userList.moreCount, Limits.friendMinCount)) more"

            cell.onShowMoreTapped = { [unowned self] in
                self.loadMore(of: userList, at: indexPath.section, showMoreCell: cell)
            }

        case let outlineContactList as OutlineContactList:
            cell.showMoreButtonTitle = "Show \(min(outlineContactList.moreCount, Limits.outlineContactMinCount)) more"

            cell.onShowMoreTapped = { [unowned self] in
                self.loadMore(of: outlineContactList, at: indexPath.section, showMoreCell: cell)
            }

        default:
            fatalError()
        }
    }

    private func configure(allowContactCell cell: AllowContactTableViewCell) {
        cell.onAllowContactButtonTapped = { [unowned self] in
            switch Services.contactsService.accessState {
            case .notDetermined:
                self.requestContactsAccess()

            case .denied, .restricted:
                self.showAccessRequest()

            case .authorized:
                self.refreshUserList()
            }
        }
    }

    private func configure(connectionsTableSectionHeaderView headerView: ConnectionsTableSectionHeaderView, at sectionIndex: Int) {
        let section = self.sections[sectionIndex].section

        switch section {
        case .inlineContacts:
            headerView.title = "Your contacts!".localized()
            headerView.subtitle = "Do not miss anything from your peers!".localized()

            headerView.onShowAllButtonClicked = { [unowned self] in
                self.showAllUsers?(.invitationInlineContacts)
            }

        case .classmates:
            headerView.title = "Your classmates!".localized()
            headerView.subtitle = "Do not miss anything from your peers!".localized()

            headerView.onShowAllButtonClicked = { [unowned self] in
                self.showAllUsers?(.invitationSchoolmates)
            }

        case .outlineContacts:
            headerView.isShowAllButtonHidden = !self.isContactsAccessAllowed
            headerView.title = "Invite more friends!".localized()
            headerView.subtitle = "Add more friends to the community".localized()

            headerView.onShowAllButtonClicked = {
                self.showAllOutlineContacts?()
            }
        }
    }

    private func configure(outlineContactCell cell: OutlineContactTableViewCell, at indexPath: IndexPath) {
        guard let outlineContactList = self.sections[indexPath.section].list as? OutlineContactList else {
            fatalError()
        }

        let outlineContact = outlineContactList[indexPath.row]

        cell.fullName = outlineContact.fullName ?? outlineContact.phoneNumber
        cell.phoneNumber = outlineContact.phoneNumber
        cell.invitationState = outlineContact.isInvited ? .invited : .uninvited

        cell.onActionButtonTouchUpInside = {
            cell.showActivityIndicator()

            firstly {
                Services.invitationService.sendInvite(to: [outlineContact])
            }.ensure { [weak cell] in
                cell?.hideActivityIndicator()
            }.done { [weak cell] in
                cell?.invitationState = .recentlyInvited
            }.catch { error in
                self.showMessage(withError: error)
            }
        }
    }
}

// MARK: - UITableViewDelegate

extension CommunityTableViewController: UITableViewDelegate {

    // MARK: - Instance Methods

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? CommunityTableViewCell else {
            return
        }
        
        if let avatarURL = self.fetchUser(at: indexPath)?.smallAvatarURL {
            Services.imageLoader.loadImage(for: avatarURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarSmallPlaceholder)
        }
        
        if self.friendRequestGuidePointerTrailing == nil {
            self.configureGuide(addButtonCenterTrailing: cell.addButtonCenterTrailing)
        }
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? CommunityTableViewCell else {
            return
        }

        Services.imageLoader.cancelLoading(in: cell.avatarImageViewTarget)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let section = self.sections[indexPath.section].section
        
        if section == .classmates || section == .inlineContacts {
            guard let user = self.fetchUser(at: indexPath) else {
                return
            }
            
            self.showFriendUser?(user)
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView = tableView.dequeueReusableCell(withIdentifier: Constants.connectionsTableSectionHeaderIdentifier)

        self.configure(connectionsTableSectionHeaderView: sectionHeaderView as! ConnectionsTableSectionHeaderView, at: section)

        return sectionHeaderView
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !Managers.userDefaultsManager.isUserHasSeenFriendRequestGuide && section == 0 {
            return 0
        } else {
            return UITableView.automaticDimension
        }
    }
}
