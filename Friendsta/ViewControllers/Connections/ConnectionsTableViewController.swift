//
//  ConnectionsTableViewController.swift
//  Nicely
//
//  Created by Elina Batyrova on 05.09.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import Nuke
import PromiseKit

class ConnectionsContainerViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties

        static let unauthorize = "Unauthorize"

        static let showFriendUser = "ShowFriendUser"
        static let showFriendProps = "ShowFriendProps"
        static let sendCustomProp = "SendCustomProp"

        static let embedConnectionsSearch = "EmbedConnectionsSearch"
        static let embedCommunity = "EmbedCommunity"
        static let embedFriends = "EmbedFriends"
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let communitySegmentIndex = 0
        static let friendsSegmentIndex = 1
    }
    
    // MARK: -
    
    private enum FilterType {
        
        // MARK: - Enumeration Cases
        
        case none
        case search(text: String)
    }

    // MARK: - Instance Properties
    
    @IBOutlet private weak var searchTextField: SearchTextField!
    @IBOutlet private weak var customSegmentedControl: CustomSegmentedControl!
    
    @IBOutlet private weak var friendsContainerView: UIView!
    @IBOutlet private weak var communityContainerView: UIView!
    @IBOutlet private weak var connectionsSearchContainerView: UIView!

    // MARK: -

    private var connectionsSearchTableViewController: ConnectionsSearchTableViewController!
    
    // MARK: -

    private var filterType: FilterType = .none
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onFriendsCustomPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onFriendsCustomPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction private func onFriendsPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onFriendsPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction private func onSearchTextFieldChanged(_ sender: UITextField) {
        Log.high("onSearchTextFieldChanged()", from: self)
        
        if let searchText = self.searchTextField.text, !searchText.isEmpty {
            self.apply(filterType: FilterType.search(text: searchText))
        } else {
            self.apply(filterType: FilterType.none)
        }
    }

    @IBAction private func onSearchTextFieldPrimaryActionTriggered(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    // MARK: -

    private func apply(segment: Int) {
        switch segment {
        case Constants.communitySegmentIndex:
            self.showCommunityContainerView()

        case Constants.friendsSegmentIndex:
            self.showFriendsContainerView()

        default:
            fatalError()
        }
    }

    private func apply(filterType: FilterType) {
        self.filterType = filterType

        switch filterType {
        case .search(let text):
            self.connectionsSearchTableViewController.apply(searchText: text)

            self.showConnectionsSearchContainerView()

        case .none:
            if self.customSegmentedControl.selectedSegmentIndex == Constants.communitySegmentIndex {
                self.showCommunityContainerView()
            } else {
                self.showFriendsContainerView()
            }
        }
    }

    // MARK: -

    private func showConnectionsSearchContainerView() {
        self.connectionsSearchContainerView.isHidden = false
        self.communityContainerView.isHidden = true
        self.friendsContainerView.isHidden = true
    }

    private func showCommunityContainerView() {
        self.communityContainerView.isHidden = false
        self.connectionsSearchContainerView.isHidden = true
        self.friendsContainerView.isHidden = true
    }

    private func showFriendsContainerView() {
        self.friendsContainerView.isHidden = false
        self.communityContainerView.isHidden = true
        self.connectionsSearchContainerView.isHidden = true
    }

    // MARK: -

    private func onEmbedConnectionsSearchTableViewController(with segue: UIStoryboardSegue) {
        guard let connectionsSearchTableViewController = segue.destination as? ConnectionsSearchTableViewController else {
            fatalError()
        }

        self.connectionsSearchTableViewController = connectionsSearchTableViewController

        connectionsSearchTableViewController.showFriendUser = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendUser, sender: user)
        }

        connectionsSearchTableViewController.showFriendProps = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendProps, sender: user)
        }

        connectionsSearchTableViewController.sendCustomProp = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.sendCustomProp, sender: user)
        }
    }

    private func onEmbedCommunityTableViewController(with segue: UIStoryboardSegue) {
        guard let communityTableViewController = segue.destination as? CommunityTableViewController else {
            fatalError()
        }

        communityTableViewController.showFriendUser = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendUser, sender: user)
        }

        communityTableViewController.showFriendProps = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendProps, sender: user)
        }

        communityTableViewController.unauthorize = { [unowned self] in
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
        }
    }

    private func onEmbedFriendsTableViewController(with segue: UIStoryboardSegue) {
        guard let friendsTableViewController = segue.destination as? FriendsTableViewController else {
            fatalError()
        }

        friendsTableViewController.showFriendUser = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendUser, sender: user)
        }

        friendsTableViewController.showFriendProps = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showFriendProps, sender: user)
        }

        friendsTableViewController.unauthorize = { [unowned self] in
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
        }
    }

    // MARK: -

    private func configureCustomSegmentedControl() {
        self.customSegmentedControl.onSegmentSelected = { [unowned self] index in
            self.apply(segment: index)
        }
    }

    private func configureSearchTextField() {
        self.searchTextField.font = Fonts.regular(ofSize: 14.0)
        self.searchTextField.delegate = self
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureSearchTextField()
        self.configureCustomSegmentedControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {
        case Segues.showFriendUser, Segues.showFriendProps, Segues.sendCustomProp:
            guard let user = sender as? User else {
                fatalError()
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["user": user])
            }

        case Segues.embedConnectionsSearch:
            self.onEmbedConnectionsSearchTableViewController(with: segue)

        case Segues.embedCommunity:
            self.onEmbedCommunityTableViewController(with: segue)

        case Segues.embedFriends:
            self.onEmbedFriendsTableViewController(with: segue)
            
        default:
            break
        }
    }
}

// MARK: - UITextFieldDelegate

extension ConnectionsContainerViewController: UITextFieldDelegate {

    // MARK: - Instance Properties

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        textField.endEditing(true)

        self.apply(filterType: .none)

        return true
    }
}
