//
//  UsersTableViewController.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 31/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools

class UsersTableViewController: LoggedViewController, ErrorMessagePresenter {

    // MARK: - Nested Types

    private enum Segues {

        // MARK: - Type Properties

        static let showFriendUser = "ShowFriendUser"
        static let showFriendProps = "ShowFriendProps"
        static let unauthorize = "Unauthorize"
    }

    // MARK: -

    private enum Constants {

        // MARK: - Type Properties

        static let friendTableCellIdentifier = "FriendTableCell"
        static let communityTableCellIdentifier = "CommunityTableCell"
    }

    // MARK: -

    private enum FilterType {

        // MARK: - Enumeration Cases

        case none
        case search(text: String)
    }

    // MARK: - Instance Properties

    @IBOutlet private weak var searchTextField: SearchTextField!

    @IBOutlet private weak var tableView: UITableView!

    @IBOutlet private weak var emptyStateContainerView: UIView!
    @IBOutlet private weak var emptyStateView: EmptyStateView!

    // MARK: -

    private var filterType: FilterType = .none

    private var userListType: UserListType = .unknown
    private var userList: UserList!

    private var filteredUsers: [User] = []

    // MARK: -

    private var shouldApplyData = true
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    // MARK: - Instance Methods

    @IBAction private func onFriendsCustomPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onFriendsCustomPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
    }

    @IBAction private func onFriendsPropSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onFriendsPropSendingFinished(\(String(describing: segue.identifier)))", from: self)
    }

    @IBAction private func onSearchTextFieldEditingChanged(_ sender: Any) {
        Log.high("onSearchTextFieldEditingChanged()", from: self)

        if let searchText = self.searchTextField.text, !searchText.isEmpty {
            self.apply(filterType: .search(text: searchText), userList: self.userList)
        } else {
            self.apply(filterType: .none, userList: self.userList)
        }
    }

    // MARK: -

    private func showEmptyState(image: UIImage? = nil, title: String, message: String? = nil, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()

        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action

        self.emptyStateContainerView.isHidden = false
    }

    private func hideEmptyState() {
        self.emptyStateContainerView.isHidden = true
    }

    private func showNoDataState() {
        self.showEmptyState(title: "Users not found".localized())
    }

    private func showLoadingState() {
        self.showEmptyState(title: "Loading users".localized(),
                            message: "We are loading list of users. Please wait a bit".localized())

        self.emptyStateView.showActivityIndicator()
    }

    // MARK: -

    private func invite(user: User, at cell: CommunityTableViewCell) {
        cell.showActivityIndicator()

        firstly {
            Services.usersService.inviteUser(user: user)
        }.ensure { [weak cell] in
            cell?.hideActivityIndicator()
        }.done { [weak cell, weak self] _ in
            guard let viewController = self else {
                return
            }

            viewController.userList = Services.cacheViewContext.userListManager.firstOrNew(withListType: viewController.userListType)

            cell?.addButtonImage = Images.hoursGlassIcon
        }.catch { error in
            self.showMessage(withError: error)
        }
    }

    private func refreshUserList() {
        Log.high("refreshUserList()", from: self)

        if (self.userList.allUsers.isEmpty) || (!self.emptyStateContainerView.isHidden) {
            self.showLoadingState()
        }

        firstly {
            Services.usersService.refreshAllUserList(of: self.userListType)
        }.done { userList in
            self.userList = userList

            self.apply(filterType: self.filterType, userList: userList)
        }.catch { error in
            self.showMessage(withError: error, okHandler: { [unowned self] in
                self.refreshUserList()
            })
        }
    }

    // MARK: -

    private func apply(userListType: UserListType) {
        Log.high("apply(userListType: \(userListType.rawValue))", from: self)

        self.userListType = userListType

        if self.isViewLoaded {
            let userList = Services.cacheViewContext.userListManager.firstOrNew(withListType: UserListType.all(type: userListType))

            self.apply(filterType: self.filterType, userList: userList)

            self.refreshUserList()

            self.shouldApplyData = false
        } else {
            self.shouldApplyData = true
        }
    }

    private func apply(filterType: FilterType, userList: UserList) {
        Log.high("apply(filterType: \(filterType), userList: \(userList.allUsers.count))", from: self)

        self.filterType = filterType
        self.userList = userList

        if self.isViewLoaded {
            switch filterType {
            case .none:
                self.filteredUsers = userList.allUsers

            case .search(let text):
                let searchText = text.lowercased()

                self.filteredUsers = userList.allUsers.filter { $0.fullName?.lowercased().contains(searchText) ?? false }
            }

            if userList.allUsers.isEmpty {
                self.showNoDataState()
            } else {
                self.hideEmptyState()
            }

            if self.isViewAppeared {
                self.tableView.reloadSections(IndexSet(arrayLiteral: 0), with: .fade)
            } else {
                self.tableView.reloadData()
            }

            self.shouldApplyData = false
        } else {
            self.shouldApplyData = true
        }
    }

    // MARK: - UIViewController

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.subscribeToKeyboardNotifications()

        if self.shouldApplyData {
            self.apply(userListType: self.userListType)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.unsubscribeFromKeyboardNotifications()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        let dictionaryReceiver: DictionaryReceiver?

        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }

        let destinationViewController = segue.destination

        switch segue.identifier {
        case Segues.showFriendUser, Segues.showFriendProps:
            guard let user = sender as? User else {
                fatalError()
            }

            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["user": user])
            }

            if segue.identifier == Segues.showFriendUser {
                destinationViewController.transitioningDelegate = PanelTransition.userRouting
                destinationViewController.modalPresentationStyle = .custom
            }

        default:
            break
        }
    }
}

// MARK: - UITableViewDataSource

extension UsersTableViewController: UITableViewDataSource {

    // MARK: - Instance Methods

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filteredUsers.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell

        switch self.userListType {
        case .invitationInlineContacts, .invitationSchoolmates:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.communityTableCellIdentifier, for: indexPath)

            self.configure(communityCell: cell as! CommunityTableViewCell, at: indexPath)

        case .friends, .invited, .expired, .pokeInlineContacts, .pokeSchoolmates:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.friendTableCellIdentifier, for: indexPath)

            self.configure(friendCell: cell as! FriendTableViewCell, at: indexPath)

        case .unknown, .all:
            fatalError()
        }

        return cell
    }
}

// MARK: - UITableViewDataSource Helpers

extension UsersTableViewController {

    // MARK: - Instance Methods

    private func configure(friendCell cell: FriendTableViewCell, at indexPath: IndexPath) {
        let user = self.filteredUsers[indexPath.row]

        cell.fullName = user.fullName
        cell.isFriendCountHidden = false

        if user.friendsCount > Limits.maxFriendCount {
            cell.info = "\(Limits.maxFriendCount)+"
        } else {
            cell.info = "\(user.friendsCount)"
        }

        switch user.friendshipStatus {
        case .some(.affirmed) where self.userListType == .friends:
            cell.info = "Affirmed".localized()
            cell.infoTextColor = Colors.redText

        case .some(.friend) where self.userListType == .friends:
            if user.daysToExpiredFriendship == 0 {
                cell.info = "Today".localized()
                cell.infoTextColor = Colors.redText
            } else if user.daysToExpiredFriendship == 1 {
                cell.info = String(format: "%d day".localized(), user.daysToExpiredFriendship)
                cell.infoTextColor = Colors.redText
            } else if user.daysToExpiredFriendship <= 2 {
                cell.info = String(format: "%d days".localized(), user.daysToExpiredFriendship)
                cell.infoTextColor = Colors.redText
            } else {
                cell.info = String(format: "%d days".localized(), user.daysToExpiredFriendship)
                cell.infoTextColor = Colors.grayText
            }

        case .some(.invited), .some(.uninvited), .some(.affirmed), .some(.friend), .none:
            let schoolTitle = user.schoolTitle ?? "unknown school".localized()

            if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(user.classYear)) {
                cell.info = String(format: "%d grade %@".localized(), schoolGradeNumber, schoolTitle)
            } else {
                cell.info = String(format: "Finished".localized(), schoolTitle)
            }

            cell.infoTextColor = Colors.grayText
            cell.isFriendCountHidden = true
        }
        
        cell.friendshipStatus = user.friendshipStatus
        cell.isKeepControlHidden = (user.friendshipStatus == nil)
        
        cell.onAddTapped = {
            cell.isKeepLoadingActivityIndicatorAnimating = true

            firstly {
                Services.usersService.inviteUser(user: user)
            }.done { user in
                cell.friendshipStatus = user.friendshipStatus
                cell.isKeepLoadingActivityIndicatorAnimating = false
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }.catch { _ in
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }
        }
        
        cell.onKeepTapped = {
            cell.isKeepLoadingActivityIndicatorAnimating = true
            
            firstly {
                Services.usersService.keep(user: user)
            }.done { user in
                cell.friendshipStatus = user.friendshipStatus
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }.catch { _ in
                cell.isKeepLoadingActivityIndicatorAnimating = false
            }
        }
    }

    private func configure(communityCell cell: CommunityTableViewCell, at indexPath: IndexPath) {
        let user = self.filteredUsers[indexPath.row]

        cell.avatarImage = nil
        cell.fullName = user.fullName

        let schoolTitle = user.schoolTitle ?? "unknown school".localized()

        if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(user.classYear)) {
            cell.schoolInfo = String(format: "%d grade %@".localized(), schoolGradeNumber, schoolTitle)
        } else {
            cell.schoolInfo = String(format: "Finished".localized(), schoolTitle)
        }

        cell.onAddButtonTapped = { [unowned self] in
            self.invite(user: user, at: cell)
        }

        switch user.friendshipStatus {
        case .some(.invited):
            cell.isAddButtonHidden = false
            cell.addButtonImage = Images.hoursGlassIcon

        case .some(.uninvited):
            cell.isAddButtonHidden = false
            cell.addButtonImage = Images.plusIcon

        case .some(.friend), .some(.affirmed), .none:
            cell.isAddButtonHidden = true
        }
    }
}

// MARK: - UITableViewDelegate

extension UsersTableViewController: UITableViewDelegate {

    // MARK: - Instance Methods

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let smallAvatarURL = self.filteredUsers[indexPath.row].smallAvatarURL else {
            return
        }

        if let cell = cell as? FriendTableViewCell {
            Services.imageLoader.loadImage(for: smallAvatarURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarSmallPlaceholder)
        } else if let cell = cell as? CommunityTableViewCell {
            Services.imageLoader.loadImage(for: smallAvatarURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarSmallPlaceholder)
        }
    }

    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? FriendTableViewCell {
            Services.imageLoader.cancelLoading(in: cell.avatarImageViewTarget)
        } else if let cell = cell as? CommunityTableViewCell {
            Services.imageLoader.cancelLoading(in: cell.avatarImageViewTarget)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        self.performSegue(withIdentifier: Segues.showFriendUser, sender: self.filteredUsers[indexPath.row])
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.searchTextField.endEditing(true)
    }
}

// MARK: - KeyboardScrollableHandler

extension UsersTableViewController: KeyboardScrollableHandler {

    // MARK: - Instance Properties

    var scrollableView: UITableView {
        return self.tableView
    }
}

// MARK: - DictionaryReceiver

extension UsersTableViewController: DictionaryReceiver {

    // MARK: - Instance Methods

    func apply(dictionary: [String: Any]) {
        guard let userListType = dictionary["userListType"] as? UserListType else {
            return
        }

        self.apply(userListType: userListType)
    }
}
