//
//  FeedPreviewViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 19.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools
import FriendstaNetwork
import PromiseKit

class FeedPreviewViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types

    private typealias CommentSegueData = (shouldShowKeyboard: Bool, feed: Feed)
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let menuItemHeight = 60
    }
    
    // MARK: -
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let showComments = "ShowComments"
        static let showRepostFeed = "ShowRepostFeed"
        static let finishPhotoPosting = "FinishPhotoPosting"
        static let showChat = "ShowChat"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var photoPreviewView: PhotoPreviewView!
    @IBOutlet private weak var linkPreviewView: LinkPreviewView!
        
    @IBOutlet private weak var avatarHolderView: RoundShadowViewWithImage!
    
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var schoolInfoLabel: UILabel!
    
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var emptyStateView: EmptyStateView!
    
    // MARK: -
    
    private var feed: Feed?
    private var feedID: Int64?
    private var shouldOpenComments = false
    
    private var promiseQueues: [Feed.UID: PromiseQueue] = [:]
    
    private let commentsPanelTransition = PanelTransition(topPadding: UIScreen.main.bounds.height / 4, options: [.interactive, .translucent])

    private var navBarShadowImage: UIImage?
    private var navBarTintColor: UIColor?
    
    // MARK: - Instance Methods
    
    func apply(feed: Feed) {
        Log.high("apply(feed: \(feed.uid))", from: self)
        
        self.feed = feed
        
        if self.isViewLoaded {
            self.linkPreviewView.isHidden = (feed.link == nil)
            self.photoPreviewView.isHidden = (feed.link != nil)
            
            self.configureUserInformation()
            
            if feed.link != nil {
                self.configureLinkPreview()
            } else {
                self.configurePhotoPreview()
            }
            
            if self.shouldOpenComments {
                let data: CommentSegueData = (shouldShowKeyboard: false, feed: feed)

                self.performSegue(withIdentifier: Segues.showComments, sender: data)
            }
        }
    }

    func apply(feedID: Int64, shouldOpenComments: Bool = false) {
        Log.high("apply(feedID: \(feedID), shouldOpenComments: \(shouldOpenComments)", from: self)
        
        self.feedID = feedID
        self.shouldOpenComments = shouldOpenComments

        if let feed = Services.cacheViewContext.feedManager.first(with: feedID) {
            self.apply(feed: feed)
        } else {
            self.fetchFeed()
        }
    }

    // MARK: -

    func fetchFeed() {
        if let feedID = self.feedID, self.isViewLoaded {
            self.showLoadingState()

            firstly {
                Services.feedService.fetchFeed(with: feedID)
            }.done { feed in
                self.hideEmptyState()
                self.apply(feed: feed)
            }.catch { error in
                self.hideEmptyState()
                self.handle(stateError: error, retryHandler: self.fetchFeed)
            }
        }
    }
    
    // MARK: -
    
    private func putLike(on feed: Feed, queue: PromiseQueue) {
        Log.high("putLike(feed: \(feed.uid))", from: self)

        queue.add {
            Services.feedService.putLike(for: feed.uid)
        }.ensure {
            if let feed = self.feed {
                self.apply(feed: feed)
            }
        }.cauterize()
    }

    private func removeLike(on feed: Feed, queue: PromiseQueue) {
        Log.high("removeLike(feed: \(feed.uid))", from: self)

        queue.add {
            Services.feedService.removeLike(for: feed.uid)
        }.ensure {
            if let feed = self.feed {
                self.apply(feed: feed)
            }
        }.cauterize()
    }
    
    private func loadPreviewFor(link: URL) {
        Managers.linkPreviewManager.getInformationFrom(link: link, completion: { linkString, linkDescription, imageURLString, error in
            if let imageURLString = imageURLString, let imageURL = URL(string: imageURLString) {
                Managers.imageLoader.loadImage(for: imageURL, completionHandler: { image in
                    DispatchQueue.main.async {
                        self.linkPreviewView.isLoadingViewHidden = true
                        
                        self.linkPreviewView.linkImageViewTarget.image = image
                    }
                })
            }
            
            DispatchQueue.main.async {
                self.linkPreviewView.linkURL = linkString
                self.linkPreviewView.linkDescription = linkDescription
            }
        })
    }
    
    private func loadAvatarImage(with accountUser: AccountUser) {
        self.avatarHolderView.placeholderImage = Images.avatarLargePlaceholder
        
        if let imageURL = accountUser.largeAvatarURL ?? accountUser.largeAvatarPlaceholderURL {
            self.activityIndicatorView.startAnimating()

            Services.imageLoader.loadImage(for: imageURL, in: self.avatarHolderView.imageViewTarget, placeholder: Images.avatarLargePlaceholder, completionHandler: { image in
                self.activityIndicatorView.stopAnimating()
            })
        }
    }

    private func loadAvatarImage(with user: User) {
           self.avatarHolderView.placeholderImage = Images.avatarLargePlaceholder

           if let imageURL = user.largeAvatarURL {
               self.activityIndicatorView.startAnimating()

               Services.imageLoader.loadImage(for: imageURL, in: self.avatarHolderView.imageViewTarget, placeholder: Images.avatarLargePlaceholder, completionHandler: { image in
                   self.activityIndicatorView.stopAnimating()
               })
           }
       }
    
    private func markAsRead(commentUIDs: [Int64], for feedUID: Int64) {
        Log.high("markAsRead(commentUIDs: \(commentUIDs.count), feedUID: \(feedUID))", from: self)

        if commentUIDs.isEmpty {
            return
        }

        firstly {
            Services.commentService.markAsRead(commentUIDs: commentUIDs, for: feedUID)
        }.done { feed in
            self.apply(feed: feed)
        }.cauterize()
    }
    
    private func delete(feed: Feed, loadingViewController: LoadingViewController) {
        Log.high("delete(feed: \(feed.uid))", from: self)
        
        firstly {
            Services.feedService.delete(feed: feed)
        }.done {
            loadingViewController.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: Segues.finishPhotoPosting, sender: self)
            })
        }.catch { error in
            self.showMessage(withError: error)
        }
    }
    
    // MARK: -
    
    private func showDeleteFeedAlert() {
        guard let feed = self.feed else {
            fatalError()
        }
        
        let alertController = UIAlertController(title: "Are you sure?".localized(), message: "You are about to delete the post. You will lose the file completely.".localized(), preferredStyle: .alert)
        
        alertController.view.tintColor = UIColor.black
        
        let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .default)
        
        let okAction = UIAlertAction(title: "OK".localized(), style: .cancel, handler: { action in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: {
                self.delete(feed: feed, loadingViewController: loadingViewController)
            })
        })
        
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true)
    }

    private func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()

        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action

        self.emptyStateView.isHidden = false
    }

    private func hideEmptyState() {
        self.emptyStateView.isHidden = true
    }

    private func showLoadingState() {
        if self.emptyStateView.isHidden {
            self.showEmptyState(title: "Loading feed".localized(),
                                message: "We are loading feed.\nPlease wait a bit.".localized())
        }

        self.emptyStateView.showActivityIndicator()
    }

    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
           let action: EmptyStateAction?

           if let retryHandler = retryHandler {
               action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: retryHandler)
           } else {
               action = nil
           }

           switch error as? WebError {
           case .some(.connection), .some(.timeOut):
               if self.feed == nil {
                   self.showEmptyState(title: "No Internet Connection".localized(),
                                       message: "Check your wi-fi or mobile data connection.".localized(),
                                       action: action)
               }

           default:
               if self.feed == nil {
                   self.showEmptyState(title: "Something went wrong".localized(),
                                       message: "Please let us know what went wrong or try again later.".localized(),
                                       action: action)
               }
           }
       }
    
    // MARK: -
    
    private func configureUserInformation() {
        guard let feed = self.feed else {
            fatalError()
        }
        
        self.fullNameLabel.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
        self.schoolInfoLabel.layer.applyShadow(color: Colors.blackShadow, alpha: 0.5, x: 0, y: 2, blur: 6)
        
        self.fullNameLabel.isHidden = feed.isIncognito
        self.schoolInfoLabel.isHidden = feed.isIncognito
        self.avatarHolderView.isHidden = feed.isIncognito

        if let accountUser = Services.accountUser, feed.userUID == accountUser.uid {
            self.loadAvatarImage(with: accountUser)
            
            self.fullNameLabel.text = accountUser.fullName
            
            let schoolTitle = accountUser.schoolTitle ?? "unknown school".localized()
            
            if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(accountUser.classYear)) {
                self.schoolInfoLabel.text = "\(schoolGradeNumber)TH GRADE, \(schoolTitle)".localized()
            } else {
                self.schoolInfoLabel.text = "FINISHED, \(schoolTitle)".localized()
            }
        } else {
            self.avatarHolderView.placeholderImage = Images.avatarLargePlaceholder

            firstly {
                Services.usersService.refresh(userUID: feed.userUID)
            }.done { user in
                self.loadAvatarImage(with: user)

                self.fullNameLabel.text = user.fullName

                let schoolTitle = user.schoolTitle ?? "unknown school".localized()

                if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(user.classYear)) {
                    self.schoolInfoLabel.text = "\(schoolGradeNumber)TH GRADE, \(schoolTitle)".localized()
                } else {
                    self.schoolInfoLabel.text = "FINISHED, \(schoolTitle)".localized()
                }
            }
        }
    }
    
    private func createMenuItems() -> [MenuItemView] {
        guard let feed = self.feed else {
            fatalError()
        }
        
        var menuItems: [MenuItemView] = []
        
        menuItems.append(self.createLikeMenuItem(for: feed))

        if Services.accountUser?.uid != self.feed?.userUID {
            menuItems.append(self.createChatMenuItem(for: feed))
        }

        menuItems.append(self.createCommentMenuItem(for: feed))
        menuItems.append(self.createRepostMenuItem(for: feed))
        
        return menuItems
    }

    private func createLikeMenuItem(for feed: Feed) -> MenuItemView {
        let likeMenuItem = MenuItemView()

        likeMenuItem.iconImage = feed.isLiked ? Images.likeFillIcon : Images.likeIcon

        likeMenuItem.title = "\(feed.likeCount)"

        likeMenuItem.onViewTapped = {
            let promiseQueue: PromiseQueue

            if let dictionaryPromiseQueue = self.promiseQueues[feed.uid] {
                promiseQueue = dictionaryPromiseQueue
            } else {
                promiseQueue = PromiseQueue(name: "LikeFeed \(feed.uid)")

                self.promiseQueues[feed.uid] = promiseQueue
            }

            if feed.isLiked {
                self.removeLike(on: feed, queue: promiseQueue)

                feed.likeCount -= 1
            } else {
                self.putLike(on: feed, queue: promiseQueue)

                feed.likeCount += 1
            }

            feed.isLiked.toggle()

            if feed.link != nil {
                self.configureLinkPreview()
            } else {
                self.configurePhotoPreview()
            }
        }

        return likeMenuItem
    }

    private func createChatMenuItem(for feed: Feed) -> MenuItemView {
        let chatMenuItem = MenuItemView()
        chatMenuItem.iconImage = Images.chatIcon
        chatMenuItem.title = "Chat".localized()
        chatMenuItem.onViewTapped = { [unowned self] in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: { [unowned self] in
                self.onChatTapped(loadingViewController: loadingViewController, feed: feed)
            })
        }

        return chatMenuItem
    }
    
    private func onChatTapped(loadingViewController: LoadingViewController, feed: Feed) {
        firstly {
            Services.usersService.refresh(userUID: feed.userUID)
        }.done { user in
            loadingViewController.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: Segues.showChat, sender: (feed, user))
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.showMessage(withError: error)
            })
        }
    }

    private func createCommentMenuItem(for feed: Feed) -> MenuItemView {
        let commentMenuItem = MenuItemView()

        commentMenuItem.iconImage = Images.commentWhiteIcon
        commentMenuItem.title = feed.commentCount == 0 ? "Comment".localized() : "\(feed.commentCount)"
        commentMenuItem.onViewTapped = {
            let data: CommentSegueData = (shouldShowKeyboard: false, feed: feed)

            self.performSegue(withIdentifier: Segues.showComments, sender: data)
        }

        return commentMenuItem
    }

    private func createRepostMenuItem(for feed: Feed) -> MenuItemView {
        let repostMenuItem = MenuItemView()

        repostMenuItem.iconImage = Images.repostIcon
        repostMenuItem.title = "Repost".localized()

        repostMenuItem.onViewTapped = {
            self.performSegue(withIdentifier: Segues.showRepostFeed, sender: feed)
        }

        return repostMenuItem
    }
    
    private func configureLinkPreview() {
        guard let feed = self.feed else {
            fatalError()
        }
        
        guard let linkString = feed.link else {
            fatalError()
        }
        
        guard let linkURL = URL(string: linkString) else {
            return
        }
        
        self.linkPreviewView.menuItems = self.createMenuItems()
        
        self.linkPreviewView.onLinkTapped = {
            UIApplication.shared.open(linkURL, options: [:], completionHandler: nil)
        }
        
        self.linkPreviewView.isBottomRightButtonHidden = false
        
        self.linkPreviewView.bottomRightButtonImage = Images.threeDotsIcon
        
        self.linkPreviewView.onBottomRightButtonTapped = {
            self.showDeleteFeedAlert()
        }

        self.linkPreviewView.isBottomRightButtonHidden = Services.accountUser?.uid != feed.userUID
        
        self.loadPreviewFor(link: linkURL)
    }
    
    private func configurePhotoPreview() {
        guard let feed = self.feed else {
            fatalError()
        }
        
        self.photoPreviewView.menuItems = self.createMenuItems()
        
        self.photoPreviewView.isBottomRightButtonHidden = false
        
        self.photoPreviewView.bottomRightButtonImage = Images.threeDotsIcon
        
        self.photoPreviewView.onBottomRightButtonTapped = {
            self.showDeleteFeedAlert()
        }

        self.photoPreviewView.isBottomRightButtonHidden = Services.accountUser?.uid != feed.userUID
        
        guard let photoURL = feed.originalImageURL else {
            return
        }
        
        Services.imageLoader.loadImageProgressive(for: photoURL, in: self.photoPreviewView.imageViewTarget)
    }
    
    private func configureNavigationBar() {
        self.navBarShadowImage = self.navigationController?.navigationBar.shadowImage
        self.navBarTintColor = self.navigationController?.navigationBar.tintColor

        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        self.navigationController?.navigationBar.tintColor = (self.feed?.link != nil) ? .black : .white
    }

    private func configureNavigationBarToPreviousState() {
        self.navigationController?.navigationBar.shadowImage = self.navBarShadowImage
        self.navigationController?.navigationBar.tintColor = self.navBarTintColor
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let feed = self.feed {
            self.apply(feed: feed)
        } else if let feedID = self.feedID {
            self.apply(feedID: feedID, shouldOpenComments: self.shouldOpenComments)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.configureNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.configureNavigationBarToPreviousState()

        Services.imageLoader.cancelLoading(in: self.photoPreviewView.imageViewTarget)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        let dictionaryReceiver: DictionaryReceiver?

        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {
        case Segues.showComments:
            guard let data = sender as? CommentSegueData, let destinationViewController = segue.destination as? CommentsTableViewController else {
                fatalError()
            }

            destinationViewController.transitioningDelegate = self.commentsPanelTransition
            destinationViewController.modalPresentationStyle = .custom
            
            let commentsDismissHandler: ((_ : CommentsTableViewController.CommentDismissData) -> Void) = { data in
                self.markAsRead(commentUIDs: data.readCommentUIDs, for: data.feed.uid)
            }

            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["shouldShowKeyboard": data.shouldShowKeyboard,
                                                      "feed": data.feed,
                                                      "dismissHandler": commentsDismissHandler])
            }
            
        case Segues.showRepostFeed:
            guard let feed = sender as? Feed else {
                return
            }
            
            let feedEditingState: FeedEditingState = feed.isIncognito ? .repostToTimeline : .repostToAnonymous
            
            dictionaryReceiver?.apply(dictionary: ["feed": feed,
                                                   "feedEditingState": feedEditingState])

        case Segues.showChat:
            guard let destinationViewController = segue.destination as? ChatMessagesRoutingController else {
                fatalError()
            }
            
            if let sender = sender as? (feed: Feed, user: User) {
                destinationViewController.apply(feed: sender.feed, potentialChatMember: sender.user)
            }
            
        default:
            return
        }
    }
}

// MARK: - DictionaryReceiver

extension FeedPreviewViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let feed = dictionary["feed"] as? Feed else {
            return
        }
        
        self.apply(feed: feed)
    }
}
