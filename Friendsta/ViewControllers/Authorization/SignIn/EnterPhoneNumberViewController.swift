//
//  EnterPhoneNumberViewController.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 07/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class EnterPhoneNumberViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let confirmPhoneNumber = "ConfirmPhoneNumber"
        static let finishAuthorization = "FinishAuthorization"
        static let signUp = "SignUp"
    }
    
    // MARK: -
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let phoneNumberMinCount = 1
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var phoneCountryCodeLabel: UILabel!
    @IBOutlet fileprivate weak var phoneNumberTextField: UITextField!
    
    @IBOutlet fileprivate weak var phoneCountryButton: UIButton!
    @IBOutlet fileprivate weak var phoneNumberLabel: UILabel!
    @IBOutlet fileprivate weak var explanationLabel: UILabel!
    @IBOutlet fileprivate weak var phoneCountryNameLabel: UILabel!
    @IBOutlet fileprivate weak var nextButton: PrimaryButton!
    
    @IBOutlet fileprivate weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    fileprivate weak var nextBarButton: UIBarButtonItem?
    
    // MARK: -
    
    fileprivate var accessSession: AccessSession?
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onAuthorizationRestarted(segue: UIStoryboardSegue) {
        Log.high("onAuthorizationRestarted(withSegue: \(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onPhoneNumberRejected(segue: UIStoryboardSegue) {
        Log.high("onPhoneNumberRejected(\(String(describing: segue.identifier)))", from: self)
        
        self.accessSession = nil
    }
    
    @IBAction fileprivate func onCountryCodeButtonTouchUpInside(_ sender: Any) {
        Log.high("onCountryCodeButtonTouchUpInside()", from: self)
        
        #if DEBUG
            self.updatePhoneNumberTextField(with: PhoneNumber(countryCode: self.phoneCountryCodeLabel.text!,
                                                              domesticNumber: "(415) 555-2671"))
        #endif
    }
    
    @IBAction fileprivate func onNextButtonTouchUpInside(_ sender: Any) {
        Log.high("onNextButtonTouchUpInside()", from: self)
        
        self.view.endEditing(true)
        
        let phoneNumber = PhoneNumber(countryCode: self.phoneCountryCodeLabel.text!,
                                      domesticNumber: self.phoneNumberTextField.text ?? "")
        
        if (Services.accountAccess?.isValid ?? false) && (self.accessSession?.phoneNumber == phoneNumber.internationalNumber) {
            if Services.accountAccess?.hasUser ?? false {
                self.performSegue(withIdentifier: Segues.finishAuthorization, sender: self)
            } else {
                self.performSegue(withIdentifier: Segues.signUp, sender: self)
            }
        } else {
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: {
                self.signIn(with: phoneNumber, loadingViewController: loadingViewController)
            })
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.phoneNumberLabel.font = Fonts.heavy(ofSize: 22.0)
        self.explanationLabel.font = Fonts.regular(ofSize: 17.0)
        self.phoneCountryCodeLabel.font = Fonts.regular(ofSize: 22.0)
        self.phoneNumberTextField.font = Fonts.regular(ofSize: 21.0)
        self.phoneCountryButton.titleLabel?.font = Fonts.regular(ofSize: 15.0)
        self.phoneCountryNameLabel.font = Fonts.regular(ofSize: 22.0)
    }
    
    fileprivate func updatePhoneNumberTextField(with phoneNumber: PhoneNumber) {
        self.phoneNumberTextField.text = PhoneNumberFormatter.shared.domesticNumber(from: phoneNumber)
        
        let isNextButtonEnabled = phoneNumber.domesticNumber.count >= Constants.phoneNumberMinCount
        
        self.nextButton.isEnabled = isNextButtonEnabled
        self.nextBarButton?.isEnabled = isNextButtonEnabled
    }
    
    // MARK: -
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.badRequest):
            self.showMessage(withTitle: nil,
                             message: "The phone number you entered is incorrect.".localized(),
                             okHandler: okHandler)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func signIn(with phoneNumber: PhoneNumber, loadingViewController: LoadingViewController) {
        Log.high("signIn(withPhoneNumber: \(phoneNumber.internationalNumber))", from: self)
        
        firstly {
            Services.authorizationService.signIn(with: phoneNumber.internationalNumber)
        }.done { accessSession in
            AccountUserBuffer.shared.reset()
            
            self.accessSession = accessSession
            
            loadingViewController.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: Segues.confirmPhoneNumber, sender: accessSession)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error, okHandler: {
                    self.phoneNumberTextField.becomeFirstResponder()
                })
            })
        }
    }
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        guard let phoneCountryCode = self.phoneCountryCodeLabel.text, let domesticNumber = textField.text else { return }
        self.updatePhoneNumberTextField(with: PhoneNumber(countryCode: phoneCountryCode,
                                                          domesticNumber: domesticNumber))
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        self.nextButton.isEnabled = false
        
        if UIScreen.main.bounds.height < 667 {
            self.nextButton.isHidden = true
            
            let nextBarButton = UIBarButtonItem(title: "Next".localized(),
                                                style: .plain,
                                                target: self,
                                                action: #selector(self.onNextButtonTouchUpInside(_:)))
            
            nextBarButton.tintColor = Colors.primary
            nextBarButton.isEnabled = false
            
            self.navigationItem.setRightBarButton(nextBarButton, animated: false)
            
            self.nextBarButton = nextBarButton
        }
        
        self.phoneNumberTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        firstly {
            Services.cacheProvider.captureModel()
        }.done { cacheSession in
            cacheSession.model.viewContext.clear()
            cacheSession.model.viewContext.save()
        }

        self.subscribeToKeyboardNotifications()
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.phoneNumberTextField.becomeFirstResponder()

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        
        self.unsubscribeFromKeyboardNotifications()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.confirmPhoneNumber:
            guard let accessSession = sender as? AccessSession else {
                fatalError()
            }
            
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["accessSession": accessSession])
            }
            
        case Segues.signUp:
            firstly {
                Services.accountProvider.captureModel()
            }.done { accountSession in
                var settings = accountSession.model.settings
                
                settings.firstTutorialCompleted = false
                settings.secondTutorialCompleted = false
                settings.thirdTutorialCompleted = false
                
                accountSession.model.settingsManager.update(settings: settings)
                accountSession.model.settingsManager.saveSettings()
            }
            
        case Segues.finishAuthorization:
            firstly {
                Services.accountProvider.captureModel()
            }.done { accountSession in
                var settings = accountSession.model.settings
                
                settings.firstTutorialCompleted = true
                settings.secondTutorialCompleted = true
                settings.thirdTutorialCompleted = true
                
                accountSession.model.settingsManager.update(settings: settings)
                accountSession.model.settingsManager.saveSettings()
            }
            
        default:
            break
        }
    }
}

// MARK: - UITextFieldDelegate

extension EnterPhoneNumberViewController: UITextFieldDelegate {
    
    // MARK: - Instance Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.onNextButtonTouchUpInside(textField)
        
        return true
    }
}

// MARK: - KeyboardHandler

extension EnterPhoneNumberViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
