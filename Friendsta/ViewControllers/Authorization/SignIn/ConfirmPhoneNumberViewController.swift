//
//  ConfirmPhoneNumberViewController.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 10/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class ConfirmPhoneNumberViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Properties
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let rejectPhoneNumber = "RejectPhoneNumber"
        static let finishAuthorization = "FinishAuthorization"
        static let signUp = "SignUp"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: -
    
    fileprivate enum Constants {
    
        // MARK: - Type Properties
    
        static let codeLenght = 4
    
        static let resendCodeWaitingTime: TimeInterval = 30.0
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var nextBarButton: UIBarButtonItem!
    
    @IBOutlet fileprivate weak var phoneNumberLabel: UILabel!
    @IBOutlet fileprivate weak var codeTextField: UITextField!
    @IBOutlet fileprivate weak var explanationLabel: UILabel!
    
    @IBOutlet fileprivate weak var resendCodeTimerLabel: UILabel!
    @IBOutlet fileprivate weak var resendCodeButton: UIButton!
    
    @IBOutlet fileprivate weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: -
    
    fileprivate var attemptCount = 0
    
    fileprivate var resendCodeTimerEndDate: Date?
    fileprivate var resendCodeTimer: Timer?
    
    // MARK: -
    
    fileprivate(set) var accessSession: AccessSession?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onNextButtonTouchUpInside(_ sender: Any) {
        Log.high("onNextButtonTouchUpInside()", from: self)
        
        self.view.endEditing(true)
        
        guard let accessSession = self.accessSession, let code = self.codeTextField.text else {
            return
        }
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            self.confirm(accessSession: accessSession, with: code, loadingViewController: loadingViewController)
        })
    }
    
    @IBAction fileprivate func onResendCodeButtonTouchUpInside(_ sender: Any) {
        Log.high("onResendCodeButtonTouchUpInside()", from: self)
        
        self.updateCodeTextField(with: nil)
        
        guard let accessSession = self.accessSession else {
            return
        }
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            self.signIn(with: accessSession, loadingViewController: loadingViewController)
        })
    }
    
    // MARK: -
    
   fileprivate func setupFont() {
        self.phoneNumberLabel.font = Fonts.regular(ofSize: 28.0)
        self.codeTextField.font = Fonts.regular(ofSize: 22.0)
        self.explanationLabel.font = Fonts.regular(ofSize: 17.0)
        self.resendCodeTimerLabel.font = Fonts.regular(ofSize: 17.0)
        self.resendCodeButton.titleLabel?.font = Fonts.regular(ofSize: 17.0)
    }
    
    fileprivate func resetResendCodeTimer() {
        self.resendCodeButton.isHidden = false
        self.resendCodeTimerLabel.isHidden = true
        
        self.resendCodeTimer?.invalidate()
        self.resendCodeTimer = nil
    }
    
    fileprivate func updateResendCodeTimer() {
        guard let resendCodeTimerEndDate = self.resendCodeTimerEndDate else {
            return
        }
        
        let resendCodeWaitingTime = resendCodeTimerEndDate.timeIntervalSince(Date())
        
        if resendCodeWaitingTime > 1 {
            self.resendCodeTimerLabel.text = String(format: "Resending code in %02d:%02d".localized(),
                                                    (Int(resendCodeWaitingTime) / 60) % 60,
                                                    Int(resendCodeWaitingTime) % 60)
            
        } else {
            self.resetResendCodeTimer()
        }
    }
    
    fileprivate func updateCodeTextField(with text: String?) {
        guard self.codeTextField.text != text else {
            return
        }
        
        self.codeTextField.text = text
        
        if let textCount = text?.count, textCount >= Constants.codeLenght {
            self.nextBarButton.isEnabled = true
            
            self.onNextButtonTouchUpInside(textField)
        } else {
            self.nextBarButton.isEnabled = false
        }
    }
    
    // MARK: -
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        case .some(.badRequest):
            self.attemptCount += 1
            
            if let attemptMaxCount = self.accessSession?.attemptMaxCount, self.attemptCount >= attemptMaxCount {
                self.showMessage(withTitle: nil,
                                 message: "You’ve tried too many times. Please request a new code.".localized(),
                                 okHandler: {
                    self.resetResendCodeTimer()
                    
                    okHandler?()
                })
            } else if let expiryDate = self.accessSession?.expiryDate, expiryDate.timeIntervalSinceNow < 1.0 {
                self.showMessage(withTitle: nil,
                                 message: "The verification code you entered is expired. Please request a new code.".localized(),
                                 okHandler: {
                    self.resetResendCodeTimer()
                    
                    okHandler?()
                })
            } else {
                self.showMessage(withTitle: nil,
                                 message: "The verification code you entered is incorrect. Please try again.".localized(),
                                 okHandler: okHandler)
            }
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func signIn(with accessSession: AccessSession, loadingViewController: LoadingViewController) {
        Log.high("signIn(withAccessSession: \(accessSession.phoneNumber))", from: self)
        
        firstly {
            Services.authorizationService.signIn(with: accessSession.phoneNumber)
        }.done { accessSession in
            loadingViewController.dismiss(animated: true, completion: {
                self.apply(accessSession: accessSession)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error)
            })
        }
    }
    
    fileprivate func confirm(accessSession: AccessSession, with code: String, loadingViewController: LoadingViewController) {
        Log.high("confirm(accessSession: \(accessSession.phoneNumber))", from: self)
        
        firstly {
            Services.authorizationService.confirm(accessSession: accessSession, with: code)
        }.done { access in
            loadingViewController.dismiss(animated: true, completion: {
                if access.hasUser {
                    self.performSegue(withIdentifier: Segues.finishAuthorization, sender: self)
                } else {
                    self.performSegue(withIdentifier: Segues.signUp, sender: self)
                }
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error, okHandler: {
                    self.codeTextField.becomeFirstResponder()
                })
            })
        }.finally {
            if Services.notificationsService.isRegisteredForNotifications {
                if let deviceToken = Services.accountProvider.model.deviceTokenManager.deviceToken {
                    Services.notificationsService.update(deviceToken: deviceToken)
                }
            }
        }
    }
    
    // MARK: -
    
    func apply(accessSession: AccessSession) {
        Log.high("apply(accessSession: \(accessSession.phoneNumber))", from: self)
        
        self.accessSession = accessSession
        
        if self.isViewLoaded {
            if let phoneNumber = PhoneNumber(internationalNumber: accessSession.phoneNumber) {
                self.phoneNumberLabel.text = PhoneNumberFormatter.shared.string(from: phoneNumber)
            } else {
                self.phoneNumberLabel.text = nil
            }
            
            self.resendCodeButton.isHidden = true
            self.resendCodeTimerLabel.isHidden = false
            
            self.attemptCount = 0
            
            if self.resendCodeTimer == nil {
                self.resendCodeTimerEndDate = Date(timeIntervalSinceNow: Constants.resendCodeWaitingTime)
                
                self.resendCodeTimer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] timer in
                    self?.updateResendCodeTimer()
                })
            }
            
            self.updateResendCodeTimer()
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let accessSession = self.accessSession, !self.hasAppliedData {
            self.apply(accessSession: accessSession)
        }
        
        self.subscribeToKeyboardNotifications()

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.codeTextField.becomeFirstResponder()
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        
        self.unsubscribeFromKeyboardNotifications()
        
        self.hasAppliedData = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        self.resetResendCodeTimer()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.signUp:
            firstly {
                Services.accountProvider.captureModel()
            }.done { accountSession in
                var settings = accountSession.model.settings
            
                settings.firstTutorialCompleted = false
                settings.secondTutorialCompleted = false
                settings.thirdTutorialCompleted = false
                
                accountSession.model.settingsManager.update(settings: settings)
                accountSession.model.settingsManager.saveSettings()
            }
            
        case Segues.finishAuthorization:
            firstly {
                Services.accountProvider.captureModel()
            }.done { accountSession in
                var settings = accountSession.model.settings
                
                settings.firstTutorialCompleted = true
                settings.secondTutorialCompleted = true
                settings.thirdTutorialCompleted = true
                
                accountSession.model.settingsManager.update(settings: settings)
                accountSession.model.settingsManager.saveSettings()
            }
            
        default:
            break
        }
    }
}

// MARK: - UITextFieldDelegate

extension ConfirmPhoneNumberViewController: UITextFieldDelegate {
    
    // MARK: - Instance Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.onNextButtonTouchUpInside(textField)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let replacedText: String
        
        if let text = self.codeTextField.text {
            replacedText = text.replacingCharacters(in: Range(range, in: text)!, with: string)
        } else {
            replacedText = string
        }
        
        let filteredText = replacedText.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let clippedText = filteredText.prefix(count: Constants.codeLenght)
        
        let startPosition = textField.beginningOfDocument
        let cursorOffset = range.location + string.count
        
        self.updateCodeTextField(with: clippedText)
        
        if let cursorPosition = textField.position(from: startPosition, offset: cursorOffset) {
            textField.selectedTextRange = textField.textRange(from: cursorPosition, to: cursorPosition)
        }
        
        return false
    }
}

// MARK: - DictionaryReceiver

extension ConfirmPhoneNumberViewController: DictionaryReceiver {

    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let accessSession = dictionary["accessSession"] as? AccessSession else {
            return
        }
        
        self.apply(accessSession: accessSession)
    }
}

// MARK: - KeyboardHandler

extension ConfirmPhoneNumberViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
