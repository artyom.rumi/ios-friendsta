//
//  WelcomeViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 06.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class WelcomeViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let embedContent =  "EmbedContent"
        static let finishWelcome = "FinishWelcome"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var pageControl: UIPageControl!
    
    fileprivate weak var pageViewController: WelcomePageViewController!
    
    // MARK: -
    
    fileprivate(set) var stage: WelcomeStage?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    fileprivate func onNextButtonTouchUpInside() {        
        let nextStepIndex = self.pageControl.currentPage + 1
        
        if nextStepIndex < self.pageViewController.steps.count {
            self.pageViewController.apply(stepIndex: nextStepIndex)
        } else {
            self.performSegue(withIdentifier: Segues.finishWelcome, sender: self)
        }
    }
    
    // MARK: -
    
    func apply(stage: WelcomeStage) {
        Log.high("apply(stage: \(stage))", from: self)
        
        self.stage = stage
        
        if self.isViewLoaded {
            self.pageViewController.apply(steps: stage.steps, stepIndex: self.pageControl.currentPage)
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(stage: self.stage ?? .main)
        }

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.embedContent:
            guard let pageViewController = segue.destination as? WelcomePageViewController else {
                fatalError()
            }
            
            self.pageViewController = pageViewController
            
            self.pageViewController.onStepIndexChanged = { [unowned self] stepIndex in
                self.pageControl.numberOfPages = self.pageViewController.steps.count
                self.pageControl.currentPage = stepIndex
            }
            
            self.pageViewController.openNextPage = {
                self.onNextButtonTouchUpInside()
            }
            
        default:
            break
        }
    }
}
