//
//  WelcomeStepViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 06.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class WelcomeStepViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let termsOfServiceLinkTitle = "Terms of Service"
        static let termsOfServiceLinkURL = URL(string: "http://Nicelysocial.com/terms/")!
        
        static let privacyPolicyLinkTitle = "Privacy Policy"
        static let privacyPolicyLinkURL = URL(string: "http://Nicelysocial.com/privacy/")!
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var emojiImageView: UIImageView!
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var descriptionLabel: UILabel!
    
    @IBOutlet fileprivate weak var nextButton: PrimaryButton!
    
    @IBOutlet fileprivate weak var agreeToTermsTextView: UITextView!
    
    // MARK: -
    
    fileprivate(set) var step: WelcomeStep?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: -
    
    var nextButtonPressed: (() -> Void)?
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onNextButtonTouchUpInside(_ sender: Any) {
        Log.high("onNextButtonTouchUpInside()", from: self)
        
        self.nextButtonPressed?()
    }
    
    // MARK: -
    
    func apply(step: WelcomeStep) {
        Log.high("apply(step: \(step))", from: self)
        
        self.step = step
        
        if self.isViewLoaded {
            switch step {
            case .first:
                self.emojiImageView.image = #imageLiteral(resourceName: "WelcomeEmoji1")
                
                self.titleLabel.text = "Simple Conversation Starters".localized()
                self.descriptionLabel.text = "Share texts, photos, and links anonymously to attract like-minded people - no hesitation!"
                
                self.agreeToTermsTextView.isHidden = true
                
                self.nextButton.titleLabel?.font = Fonts.semiBold(ofSize: 17.0)
                self.nextButton.setTitle("Next", for: .normal)
                
            case .second:
                self.emojiImageView.image = #imageLiteral(resourceName: "WelcomeEmoji2")
                
                self.titleLabel.text = "Your Extended Circle".localized()
                self.descriptionLabel.text = "Your posts are visible not only to your friends, but also to friends of friends AND their friends."
                
                self.agreeToTermsTextView.isHidden = true
                
                self.nextButton.titleLabel?.font = Fonts.bold(ofSize: 17.0)
                self.nextButton.setTitle("Next", for: .normal)
                
            case .third:
                self.emojiImageView.image = #imageLiteral(resourceName: "ChatWelcomeMessageIcon")
                
                self.titleLabel.text = "Incognito Chat".localized()
                self.descriptionLabel.text = "When you see a post you like, chat with the poster incognito."
                
                self.agreeToTermsTextView.isHidden = true
                
                self.nextButton.titleLabel?.font = Fonts.semiBold(ofSize: 17.0)
                self.nextButton.setTitle("Next", for: .normal)
                
            case .forth:
                self.emojiImageView.image = #imageLiteral(resourceName: "WelcomeEmoji4")
                
                self.titleLabel.text = "Mutual Reveal".localized()
                self.descriptionLabel.text = "Dying to know who it is? Just opt-in for a mutual reveal. If your chatmate also opts-in, you will both be revealed at the same time!"
                
                self.agreeToTermsTextView.isHidden = false
                
                self.nextButton.titleLabel?.font = Fonts.semiBold(ofSize: 17.0)
                self.nextButton.setTitle("Accept & continue", for: .normal)
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    fileprivate func setupTextView() {
        let attributedString = NSMutableAttributedString(string: String(format: "By using this application, you agree to the %@ and %@".localized(),
                                                                        Constants.termsOfServiceLinkTitle, Constants.privacyPolicyLinkTitle))
        
        guard let termsOfServiceLinkStartIndex = attributedString.string.range(of: Constants.termsOfServiceLinkTitle)?.lowerBound.encodedOffset else {
            return
        }
        
        attributedString.addAttribute(.link,
                                      value: Constants.termsOfServiceLinkURL,
                                      range: NSRange(location: termsOfServiceLinkStartIndex, length: Constants.termsOfServiceLinkTitle.count))
        
        guard let privacyPolicyLinkStartIndex = attributedString.string.range(of: Constants.privacyPolicyLinkTitle)?.lowerBound.encodedOffset else {
            return
        }
        
        attributedString.addAttribute(.link,
                                      value: Constants.privacyPolicyLinkURL,
                                      range: NSRange(location: privacyPolicyLinkStartIndex, length: Constants.privacyPolicyLinkTitle.count))

        self.agreeToTermsTextView.attributedText = attributedString
        self.agreeToTermsTextView.font = .systemFont(ofSize: 17)
        self.agreeToTermsTextView.textAlignment = .center
        self.agreeToTermsTextView.linkTextAttributes = [.foregroundColor: Colors.primary]
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.heavy(ofSize: 22.0)
        self.descriptionLabel.font = Fonts.regular(ofSize: 15.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupFont()
        self.setupTextView()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let step = self.step, !self.hasAppliedData {
            self.apply(step: step)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
}
