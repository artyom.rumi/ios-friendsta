//
//  ChooseGenderViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 20.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class ChooseGenderViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let finishGenderChoosing = "FinishGenderChoosing"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var boyGenderButton: GenderButton!
    @IBOutlet fileprivate weak var girlGenderButton: GenderButton!
    @IBOutlet fileprivate weak var nonBinaryGenderButton: GenderButton!
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var explanationLabel: UILabel!
  
    // MARK: -
    
    fileprivate(set) var accountUserBuffer: AccountUserBuffer?
    
    fileprivate(set) var hasAppliedData = false
    
    var finishGenderChoosing: (() -> Void)?
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onGenderButtonTouchUpInside(_ sender: GenderButton) {
        Log.high("onGenderButtonTouchUpInside()", from: self)
        
        guard let accountUserBuffer = self.accountUserBuffer else {
            return
        }
        
        self.boyGenderButton.isSelected = false
        self.girlGenderButton.isSelected = false
        self.nonBinaryGenderButton.isSelected = false
        
        switch sender {
        case is BoyGenderButton:
            accountUserBuffer.gender = Gender.male
            
            self.boyGenderButton.isSelected = true
            
        case is GirlGenderButton:
            accountUserBuffer.gender = Gender.female
            
            self.girlGenderButton.isSelected = true
            
        case is NonBinaryGenderButton:
            accountUserBuffer.gender = Gender.nonBinary
            
            self.nonBinaryGenderButton.isSelected = true
            
        default:
            break
        }
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            self.createAccountUser(from: accountUserBuffer, loadingViewController: loadingViewController)
        })
    }

    // MARK: -
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func createAccountUser(from accountUserBuffer: AccountUserBuffer, loadingViewController: LoadingViewController) {
        // TODO: Check it
        let accountUserPromise: Promise<AccountUser>
        
        if let accessUserUID = Services.accountAccess?.userUID {
            accountUserPromise = Services.accountUserService.updateAccountUser(from: accountUserBuffer,
                                                                               uid: accessUserUID)
        } else {
            accountUserPromise = Services.accountUserService.createAccountUser(from: accountUserBuffer)
        }
        
        firstly {
            return accountUserPromise
        }.done { accountUser in
            loadingViewController.dismiss(animated: true, completion: {
                self.performSegue(withIdentifier: Segues.finishGenderChoosing, sender: self)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error)
            })
        }
    }
    
    // MARK: -
    
    func apply(accountUserBuffer: AccountUserBuffer) {
        Log.high("apply(accountUserBuffer: \(String(describing: accountUserBuffer.fullName)))", from: self)
        
        self.accountUserBuffer = accountUserBuffer
        
        if self.isViewLoaded {
            self.boyGenderButton.isSelected = false
            self.girlGenderButton.isSelected = false
            self.nonBinaryGenderButton.isSelected = false
            
            switch accountUserBuffer.gender {
            case .some(.male):
                self.boyGenderButton.isSelected = true
                
            case .some(.female):
                self.girlGenderButton.isSelected = true
                
            case .some(.nonBinary):
                self.nonBinaryGenderButton.isSelected = true
                
            case .none:
                break
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.heavy(ofSize: 22.0)
        self.explanationLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
        self.setupFont()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(accountUserBuffer: self.accountUserBuffer ?? AccountUserBuffer.shared)
        }

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
}
