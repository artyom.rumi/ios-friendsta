//
//  UnavailableSchoolViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 30.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class UnavailableSchoolViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let contactUsLinkTitle = "Friendstasocial.com"
        
        static let contactUsLinkURL = URL(string: "https://docs.google.com/forms/d/e/1FAIpQLSdrJSXScl7q5wTeVLDGYYaT7oerg3ejKZD2K_UggpAAOP17gg/viewform")!
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var contactUsTextView: UITextView!
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.contactUsTextView.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: - Instance Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        let attributedString = NSMutableAttributedString(string: String(format: "Contact us at %@ to add your school".localized(),
                                                                        Constants.contactUsLinkTitle))
        
        guard let linkStartIndex = attributedString.string.range(of: Constants.contactUsLinkTitle)?.lowerBound.encodedOffset else {
            return
        }
        
        attributedString.addAttribute(.link, value: Constants.contactUsLinkURL,
                                      range: NSRange(location: linkStartIndex, length: Constants.contactUsLinkTitle.count))
        
        self.contactUsTextView.attributedText = attributedString
        self.contactUsTextView.font = .systemFont(ofSize: 17)
        self.contactUsTextView.textAlignment = .center
        self.contactUsTextView.linkTextAttributes = [.foregroundColor: Colors.primary]
        self.setupFont()
    }
}

// MARK: - UITextViewDelegate

extension UnavailableSchoolViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL, options: [:])
        
        return false
    }
}
