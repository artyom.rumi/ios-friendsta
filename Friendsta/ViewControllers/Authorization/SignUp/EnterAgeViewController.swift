//
//  EnterAgeViewController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class EnterAgeViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let finishAgeEntering = "FinishAgeEntering"
    }
    
    // MARK: -
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
    
        static let minAge = 13
        static let maxAge = 100
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var explanationLabel: UILabel!
    @IBOutlet fileprivate weak var ageTextField: UITextField!
    @IBOutlet fileprivate weak var nextButton: UIButton!
    
    @IBOutlet fileprivate weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: -
    
    fileprivate(set) var accountUserBuffer: AccountUserBuffer?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onNextButtonTouchUpInside(_ sender: Any) {
        Log.high("onNextButtonTouchUpInside()", from: self)
        
        self.view.endEditing(true)
        
        guard let accountUserBuffer = self.accountUserBuffer else {
            return
        }
        
        guard let ageText = self.ageTextField.text else {
            return
        }
        
        if let age = Int(ageText), age >= Constants.minAge {
            accountUserBuffer.age = age
            
            self.performSegue(withIdentifier: Segues.finishAgeEntering, sender: sender)
        } else {
            accountUserBuffer.age = nil
            
            self.showAlert(message: "Sorry, it looks like you are not eligible for Friendsta. Thanks for cheking it out.".localized(),
                           handler: {
                self.ageTextField.becomeFirstResponder()
            })
        }
    }
    
    // MARK: -
    
    fileprivate func showAlert(title: String? = nil, message: String?, handler: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: { action in
            handler?()
        }))
        
        self.present(alertController, animated: true)
    }
    
    // MARK: -
    
    fileprivate func updateTextField(with text: String?) {
        if let text = text, let age = Int(text) {
            if age <= Constants.maxAge {
                self.ageTextField.text = text
            }
        } else {
            self.ageTextField.text = text
        }
        
        self.nextButton.isEnabled = !(text?.isEmpty ?? true)
    }
    
    // MARK: -
    
    func apply(accountUserBuffer: AccountUserBuffer) {
        Log.high("apply(accountUserBuffer: \(String(describing: accountUserBuffer.fullName)))", from: self)
        
        self.accountUserBuffer = accountUserBuffer
        
        if self.isViewLoaded {
            if let age = accountUserBuffer.age {
                self.updateTextField(with: String(age))
            } else {
                self.updateTextField(with: nil)
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.explanationLabel.font = Fonts.heavy(ofSize: 22.0)
        self.ageTextField.font = Fonts.regular(ofSize: 22.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(accountUserBuffer: self.accountUserBuffer ?? AccountUserBuffer.shared)
        }
        
        self.subscribeToKeyboardNotifications()

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.ageTextField.becomeFirstResponder()

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        self.view.endEditing(true)
        
        self.unsubscribeFromKeyboardNotifications()

        self.hasAppliedData = false
    }
}

// MARK: - UITextFieldDelegate

extension EnterAgeViewController: UITextFieldDelegate {
    
    // MARK: - Instance Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.onNextButtonTouchUpInside(textField)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let replacedText: String
        
        if let text = self.ageTextField.text {
            replacedText = text.replacingCharacters(in: Range(range, in: text)!, with: string)
        } else {
            replacedText = string
        }

        let startPosition = textField.beginningOfDocument
        let cursorOffset = range.location + string.count
        
        self.updateTextField(with: replacedText.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
        
        if let cursorPosition = textField.position(from: startPosition, offset: cursorOffset) {
            textField.selectedTextRange = textField.textRange(from: cursorPosition, to: cursorPosition)
        }
        
        return false
    }
}

// MARK: - DictionaryReceiver

extension EnterAgeViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let accountUserBuffer = dictionary["accountUserBuffer"] as? AccountUserBuffer else {
            return
        }
        
        self.apply(accountUserBuffer: accountUserBuffer)
    }
}

// MARK: - KeyboardHandler

extension EnterAgeViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
