//
//  AvatarPickerViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 20.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

// swiftlint:disable file_length
// swiftlint:disable type_body_length
// swiftlint:disable function_body_length
// swiftlint:disable vertical_whitespace

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class AvatarPickerViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let unauthorize = "Unauthorize"
        static let finishAuthorization = "FinishAuthorization"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var avatarImageView: UIImageView!
    @IBOutlet fileprivate weak var nextButton: PrimaryButton!
    
    @IBOutlet fileprivate weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var explanationLabel: UILabel!
    @IBOutlet fileprivate weak var uploadPhotoButton: TextButton!
    
    // MARK: -
    
    fileprivate(set) var accountUser: AccountUser?
    
    fileprivate(set) var hasAppliedData = false
    
    fileprivate(set) var isAvatarChanged = false
    fileprivate(set) var isAvatarRemoved = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onUploadPhotoButtonTouchUpInside(_ sender: Any) {
        Log.high("onUploadPhotoButtonTouchUpInside()", from: self)
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "Take Photo".localized(), style: .default, handler: { action in
            let imagePickerController = ImagePickerController()
            
            imagePickerController.modalTransitionStyle = .coverVertical
            imagePickerController.modalPresentationStyle = .overFullScreen
            
            imagePickerController.onImagePicked = { [unowned self, unowned imagePickerController] image in
                imagePickerController.dismiss(animated: true)
                
                self.avatarImageView.image = image
                
                self.isAvatarChanged = true
                self.isAvatarRemoved = false
            }
            
            imagePickerController.apply(mediaSource: .camera, allowsEditing: true)
            
            self.present(imagePickerController, animated: true)
        }))
        
        alertController.addAction(UIAlertAction(title: "Choose From Library".localized(), style: .default, handler: { action in
            let imagePickerController = ImagePickerController()
            
            imagePickerController.modalTransitionStyle = .coverVertical
            imagePickerController.modalPresentationStyle = .overFullScreen
            
            imagePickerController.onImagePicked = { [unowned self, unowned imagePickerController] image in
                imagePickerController.dismiss(animated: true)
                
                self.avatarImageView.image = image
                
                self.isAvatarChanged = true
                self.isAvatarRemoved = false
            }
            
            imagePickerController.apply(mediaSource: .photoLibrary, allowsEditing: true)
            
            self.present(imagePickerController, animated: true)
        }))
        
        if !self.isAvatarRemoved {
            alertController.addAction(UIAlertAction(title: "Delete Photo".localized(), style: .default, handler: { action in
                if let avatarURL = self.accountUser?.largeAvatarPlaceholderURL {
                    self.loadAvatarImage(from: avatarURL)
                } else {
                    self.avatarImageView.image = #imageLiteral(resourceName: "AvatarLargePlaceholder")
                }
                
                self.isAvatarChanged = true
                self.isAvatarRemoved = true
            }))
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
        
        self.present(alertController, animated: true)
    }
    
    @IBAction fileprivate func onNextButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onNextButtonTouchUpInside()", from: self)
        
        let storyboard = UIStoryboard(name: "Connections", bundle: nil)
        let connectionsContainerVC = storyboard.instantiateViewController(withIdentifier: "ConnectionsContainerViewController") as! ConnectionsContainerViewController
        
        connectionsContainerVC.newAccountUser = accountUser
        connectionsContainerVC.isAvatarChanged = isAvatarChanged
        connectionsContainerVC.isAvatarRemoved = isAvatarRemoved
        connectionsContainerVC.avatarImageViewImage = avatarImageView.image
        connectionsContainerVC.isComingFromAvatarPickerVC = true
        
        if let accountUser = accountUser {
            
            let dictionaryReceiver: DictionaryReceiver?
            
            dictionaryReceiver = connectionsContainerVC
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["accountUser": accountUser])
            }
        }
        
        //navigationController?.pushViewController(connectionsContainerVC, animated: true)
  
        if isAvatarChanged == false {
                       let alertController = UIAlertController(title: "Hold up!".localized(),
                                                                message: "Adding a photo makes it easier for your friends to recognize you".localized(),
                                                                preferredStyle: .alert)
        
                        alertController.view.tintColor = Colors.primary
        
                        alertController.addAction(UIAlertAction(title: "Not Now".localized(), style: .cancel, handler: { action in
                            self.navigationController?.pushViewController(connectionsContainerVC, animated: true)  }))
        //
                        alertController.addAction(UIAlertAction(title: "Add Photo".localized(), style: .default, handler: { action in
                            self.onUploadPhotoButtonTouchUpInside(self)       }
                            ))
        //
                        self.present(alertController, animated: true)
         }
         else {
                       self.navigationController?.pushViewController(connectionsContainerVC, animated: true)
         }
    
    /*
        if self.isAvatarChanged {
            if self.isAvatarRemoved {
                let loadingViewController = LoadingViewController()

                self.deleteAccountUserAvatar(loadingViewController: loadingViewController)
                
            } else {
                guard let avatarImage = self.avatarImageView.image else {
                    return
                }

                let loadingViewController = LoadingViewController()

                self.uploadAccountUser(avatarImage: avatarImage, loadingViewController: loadingViewController)
                //self.navigationController?.pushViewController(connectionsContainerVC, animated: true)
            }
        } else {
            if self.accountUser?.avatarURL == nil {
                let alertController = UIAlertController(title: "Hold up!".localized(),
                                                        message: "Adding a photo makes it easier for your friends to recognize you".localized(),
                                                        preferredStyle: .alert)

                alertController.view.tintColor = Colors.primary

                alertController.addAction(UIAlertAction(title: "Not Now".localized(), style: .cancel, handler: { action in
                    self.navigationController?.pushViewController(connectionsContainerVC, animated: true)  }))

                alertController.addAction(UIAlertAction(title: "Add Photo".localized(), style: .default, handler: { action in
                    self.onUploadPhotoButtonTouchUpInside(self)       }
                            ))

                self.present(alertController, animated: true)
            } else {
                self.navigationController?.pushViewController(connectionsContainerVC, animated: true)
            }
        }
 */
    }
    
    // MARK: -
    
    fileprivate func loadAvatarImage(from imageURL: URL) {
        self.activityIndicatorView.startAnimating()

        Services.imageLoader.loadImage(for: imageURL, in: self.avatarImageView, placeholder: #imageLiteral(resourceName: "AvatarLargePlaceholder"), completionHandler: { image in
            self.activityIndicatorView.stopAnimating()
        })
    }
    
    // MARK: -
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func uploadAccountUser(avatarImage: UIImage, loadingViewController: LoadingViewController) {
        DispatchQueue.global(qos: .userInitiated).async(execute: {
            let avatarData: Data?
            
            if avatarImage.size.width > Limits.avatarImageMaxWidth {
                if let avatarImage = avatarImage.scaled(to: Limits.avatarImageMaxWidth) {
                    avatarData = avatarImage.jpegData(compressionQuality: Limits.avatarImageQuality)
                } else {
                    avatarData = avatarImage.jpegData(compressionQuality: Limits.avatarImageQuality)
                }
            } else {
                avatarData = avatarImage.jpegData(compressionQuality: Limits.avatarImageQuality)
            }
            
            DispatchQueue.main.async(execute: {
                if let avatarData = avatarData {
                    firstly {
                        Services.accountUserService.uploadAccountUserAvatar(with: avatarData)
                    }.done { accountUser in
                        self.apply(accountUser: accountUser)
                        
                        loadingViewController.dismiss(animated: true, completion: {
                            self.performSegue(withIdentifier: Segues.finishAuthorization, sender: self.accountUser)
                        })
                    }.catch { error in
                        loadingViewController.dismiss(animated: true, completion: {
                            self.handle(actionError: error)
                        })
                    }
                } else {
                    loadingViewController.dismiss(animated: true, completion: {
                        self.showMessage(withTitle: "Invalid image".localized(),
                                         message: "Please choose another image".localized())
                    })
                }
            })
        })
    }
    
    fileprivate func deleteAccountUserAvatar(loadingViewController: LoadingViewController) {
        firstly {
            Services.accountUserService.deleteAccountUserAvatar()
        }.done { accountUser in
            if let accountUser = accountUser {
                self.apply(accountUser: accountUser)
            } else {
                self.isAvatarChanged = false
                self.isAvatarRemoved = false
            }
            //Victor
            loadingViewController.dismiss(animated: true, completion: {
              self.performSegue(withIdentifier: Segues.finishAuthorization, sender: self.accountUser)
            })
        }.catch { error in
            loadingViewController.dismiss(animated: true, completion: {
                self.handle(actionError: error)
            })
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.heavy(ofSize: 22.0)
        self.uploadPhotoButton.titleLabel?.font = Fonts.semiBold(ofSize: 15.0)
        self.explanationLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: -
    
    func apply(accountUser: AccountUser) {
        Log.high("apply(accountUser: \(String(describing: accountUser.fullName)))", from: self)
        
        self.accountUser = accountUser

        self.isAvatarChanged = false
        self.isAvatarRemoved = (accountUser.avatarURL == nil)
        
        if self.isViewLoaded {
            if let avatarURL = accountUser.largeAvatarURL ?? accountUser.largeAvatarPlaceholderURL {
                self.loadAvatarImage(from: avatarURL)
            } else {
                self.avatarImageView.image = #imageLiteral(resourceName: "AvatarLargePlaceholder")
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
        self.setupFont()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let accountUser = self.accountUser ?? Services.accountUser, !self.hasAppliedData {
            self.apply(accountUser: accountUser)
        }

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.finishAuthorization:
            guard let accountUser = sender as? AccountUser else {
                return
            }
            
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }

            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["accountUser": accountUser])
            }
        
        default:
            break
        }
    }
}

// MARK: - DictionaryReceiver

extension AvatarPickerViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let accountUser = dictionary["accountUser"] as? AccountUser else {
            return
        }
        
        self.apply(accountUser: accountUser)
    }
}
