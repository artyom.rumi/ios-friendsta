//
//  SchoolsRoutingController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 08.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class SchoolsRoutingController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let embedContent = "EmbedContent"
        static let finishSchoolSelection = "FinishSchoolSelection"
        static let showUnavailableSchool = "ShowUnavailableSchool"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties
    
    fileprivate weak var schoolTableViewController: SchoolTableViewController!
    
    // MARK: -
    
    fileprivate(set) var accountUserBuffer: AccountUserBuffer?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    fileprivate func onEmbedContent(segue: UIStoryboardSegue, sender: Any?) {
        guard let schoolTableViewController = segue.destination as? SchoolTableViewController else {
            fatalError()
        }
        
        self.schoolTableViewController = schoolTableViewController
        
        self.schoolTableViewController.finishSchoolSelection = { [unowned self] in
            self.performSegue(withIdentifier: Segues.finishSchoolSelection, sender: self)
        }
        
        self.schoolTableViewController.showUnavailableSchool = { [unowned self] in
            self.performSegue(withIdentifier: Segues.showUnavailableSchool, sender: self)
        }
        
        self.schoolTableViewController.unauthorize = { [unowned self] in
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
        }
    }
    
    // MARK: -
    
    func apply(accountUserBuffer: AccountUserBuffer) {
        Log.high("apply(accountUserBuffer: \(String(describing: accountUserBuffer.fullName)))", from: self)
        
        self.accountUserBuffer = accountUserBuffer
        
        if self.isViewLoaded {
            self.schoolTableViewController.apply(accountUserBuffer: accountUserBuffer)
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let accountUserBuffer = self.accountUserBuffer, !self.hasAppliedData {
            self.apply(accountUserBuffer: accountUserBuffer)
        }
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.embedContent:
            self.onEmbedContent(segue: segue, sender: sender)
            
        default:
            break
        }
    }
}

// MARK: - DictionaryReceiver

extension SchoolsRoutingController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let accountUserBuffer = dictionary["accountUserBuffer"] as? AccountUserBuffer else {
            return
        }
        
        self.apply(accountUserBuffer: accountUserBuffer)
    }
}
