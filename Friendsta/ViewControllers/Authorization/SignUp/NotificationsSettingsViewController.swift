//
//  NotificationsSettingsViewController.swift
//  Friendsta
//
//  Created by Damir Zaripov on 30/10/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class NotificationsSettingsViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let finishAuthorization = "FinishAuthorization"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: -
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let isUserHasSeenNotificationsScreenKey = "UserHasSeenNotificationsScreen"
        
        static let newFriendNotificationsTableCellIdentifier = "NewFriendNotificationsTableCell"
        static let receivedPropNotificationsTableCellIdentifier = "ReceivedPropNotificationsTableCell"
        static let chatMessageNotificationsTableCellIdentifier = "ChatMessageNotificationsTableCell"
        static let newUserNotificationsTableCellIdentifier = "NewUserNotificationsTableCell"
        
        static let tableSectionHeaderIdentifier = "TableSectionHeader"
        static let tableSectionFooterIdentifier = "TableSectionFooter"
        
        static let notificationsTableSection = 0
        
        static let notificationsTableSectionHeaderRow = 0
        static let receivedPropNotificationsTableCellRow = 1
        static let chatMessageNotificationsTableCellRow = 2
        static let newUserNotificationsTableCellRow = 3
        static let newFriendNotificationsTableCellRow = 4
        static let notificationsTableSectionFooterRow = 5
        
        static let tableSectionCount = 1
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var tableHeaderView: UIView!
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    
    // MARK: -
    
    fileprivate var notificationsAccessState: NotificationsAccessState?
    
    // MARK: -
    
    fileprivate(set) var accountUser: AccountUser?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    @objc fileprivate func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.requestNotificationsAccess()
    }
    
    // MARK: -
    
    fileprivate func config(newFriendNotificationsCell cell: SwitchableTableViewCell) {
        guard let accountUser = self.accountUser else {
            return
        }
        
        cell.isOn = accountUser.isNewFriendNotificationsEnabled
        
        cell.onStateChanged = { [unowned self, unowned cell] isOn in
            if self.tableView.allowsSelection {
                cell.showActivityIndicator()
                
                accountUser.isNewFriendNotificationsEnabled = isOn
                
                firstly {
                    Services.accountUserService.update(accountUser: accountUser)
                }.ensure { [weak cell] in
                    cell?.hideActivityIndicator()
                }.done { [weak cell] accountUser in
                    cell?.isOn = accountUser.isNewFriendNotificationsEnabled
                }.catch { error in
                    self.handle(actionError: error)
                }
            }
        }
    }
    
    fileprivate func config(receivedPropNotificationsCell cell: SwitchableTableViewCell) {
        guard let accountUser = self.accountUser else {
            return
        }
        
        cell.isOn = accountUser.isReceivedPropNotificationsEnabled
        
        cell.onStateChanged = { [unowned self, unowned cell] isOn in
            if self.tableView.allowsSelection {
                cell.showActivityIndicator()
                
                accountUser.isReceivedPropNotificationsEnabled = isOn
                
                firstly {
                    Services.accountUserService.update(accountUser: accountUser)
                }.ensure { [weak cell] in
                    cell?.hideActivityIndicator()
                }.done { [weak cell] accountUser in
                    cell?.isOn = accountUser.isReceivedPropNotificationsEnabled
                }.catch { error in
                    self.handle(actionError: error)
                }
            }
        }
    }
    
    fileprivate func config(chatMessageNotificationsCell cell: SwitchableTableViewCell) {
        guard let accountUser = self.accountUser else {
            return
        }
        
        cell.isOn = accountUser.isChatMessageNotificationEnabled
        
        cell.onStateChanged = { [unowned self, unowned cell] isOn in
            if self.tableView.allowsSelection {
                cell.showActivityIndicator()
                
                accountUser.isChatMessageNotificationEnabled = isOn
                
                firstly {
                    Services.accountUserService.update(accountUser: accountUser)
                }.ensure { [weak cell] in
                    cell?.hideActivityIndicator()
                }.done { [weak cell] accountUser in
                    cell?.isOn = accountUser.isChatMessageNotificationEnabled
                }.catch { error in
                    self.handle(actionError: error)
                }
            }
        }
    }
    
    fileprivate func config(newUserNotificationsCell cell: SwitchableTableViewCell) {
        guard let accountUser = self.accountUser else {
            return
        }
        
        cell.isOn = accountUser.isNewUserNotificationsEnabled
        
        cell.onStateChanged = { [unowned self, unowned cell] isOn in
            if self.tableView.allowsSelection {
                cell.showActivityIndicator()
                
                accountUser.isNewUserNotificationsEnabled = isOn
                
                firstly {
                    Services.accountUserService.update(accountUser: accountUser)
                }.ensure { [weak cell] in
                    cell?.hideActivityIndicator()
                }.done { [weak cell] accountUser in
                    cell?.isOn = accountUser.isNewUserNotificationsEnabled
                }.catch { error in
                    self.handle(actionError: error)
                }
            }
        }
    }
    
    fileprivate func config(notificationsSectionFooterView sectionFooterView: SettingsTableSectionFooterView) {
        switch self.notificationsAccessState {
        case .some(.denied):
            sectionFooterView.title = "You can change these anytime under Settings.\n\nWARNING: Push notifications are disabled. To enable please visit: iPhone Settings > Notification Center".localized()
            
        case .some(.notDetermined), .some(.authorized), .none:
            sectionFooterView.title = "You can change these anytime under Settings.".localized()
        }
    }
    
    fileprivate func layoutTableHeaderView() {
        let tableHeaderViewHeight = self.tableHeaderView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        
        self.tableHeaderView.frame = CGRect(x: 0.0,
                                            y: 0.0,
                                            width: self.tableView.bounds.width,
                                            height: tableHeaderViewHeight).adjusted
        
        self.tableView.tableHeaderView = self.tableHeaderView
    }
    
    // MARK: -
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func requestNotificationsAccess() {
        Log.high("requestNotificationsAccess()", from: self)
        
        firstly {
            Services.notificationsService.requestAccess()
        }.done { accessState in
            self.notificationsAccessState = accessState
            
            if self.accountUser != nil {
                self.tableView.reloadSections([Constants.notificationsTableSection], with: .fade)
            }
            
            switch accessState {
            case .notDetermined, .denied:
                break
                
            case .authorized:
                if !Services.notificationsService.isRegisteredForNotifications {
                    Services.notificationsService.registerForNotifications()
                }
            }
        }
    }
    
    // MARK: -
    
    func apply(accountUser: AccountUser) {
        Log.high("apply(accountUser: \(String(describing: accountUser.fullName)))", from: self)
        
        self.accountUser = accountUser
        
        if self.isViewLoaded {
            if self.isViewAppeared {
                self.tableView.beginUpdates()
                self.tableView.deleteSections(IndexSet(integersIn: 0..<self.tableView.numberOfSections), with: .fade)
                self.tableView.insertSections(IndexSet(integersIn: 0..<Constants.tableSectionCount), with: .fade)
                self.tableView.endUpdates()
            } else {
                self.tableView.reloadData()
            }
            
            self.layoutTableHeaderView()
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        if self.titleLabel != nil {
            self.titleLabel.font = Fonts.black(ofSize: 22.0)
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
        
        self.setupFont()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let accountUser = self.accountUser ?? Services.accountUser, !self.hasAppliedData {
            self.apply(accountUser: accountUser)
        }
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onApplicationWillEnterForeground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        self.setNeedsStatusBarAppearanceUpdate()
        
        UserDefaults.standard.set(true, forKey: Constants.isUserHasSeenNotificationsScreenKey)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
        self.hasAppliedData = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.layoutTableHeaderView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.finishAuthorization:
            self.requestNotificationsAccess()
            
        default:
            break
        }
    }
}

// MARK: - UITableViewDataSource

extension NotificationsSettingsViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.tableSectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch indexPath.row {
        case Constants.notificationsTableSectionHeaderRow:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableSectionHeaderIdentifier,
                                                 for: indexPath)
        
        case Constants.newFriendNotificationsTableCellRow:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.newFriendNotificationsTableCellIdentifier,
                                                 for: indexPath)
       
            self.config(newFriendNotificationsCell: cell as! SwitchableTableViewCell)
        
        case Constants.receivedPropNotificationsTableCellRow:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.receivedPropNotificationsTableCellIdentifier,
                                                 for: indexPath)
            
            self.config(receivedPropNotificationsCell: cell as! SwitchableTableViewCell)
            
        case Constants.chatMessageNotificationsTableCellRow:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.chatMessageNotificationsTableCellIdentifier,
                                                 for: indexPath)
            
            self.config(chatMessageNotificationsCell: cell as! SwitchableTableViewCell)
            
        case Constants.newUserNotificationsTableCellRow:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.newUserNotificationsTableCellIdentifier,
                                                 for: indexPath)
            
            self.config(newUserNotificationsCell: cell as! SwitchableTableViewCell)
            
        case Constants.notificationsTableSectionFooterRow:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableSectionFooterIdentifier,
                                                 for: indexPath)
            
            self.config(notificationsSectionFooterView: cell as! SettingsTableSectionFooterView)
            
        default:
            fatalError()
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension NotificationsSettingsViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Constants.notificationsTableSection:
            switch indexPath.row {
            case Constants.notificationsTableSectionHeaderRow:
                return 56.0
                
            case Constants.newFriendNotificationsTableCellRow,
                 Constants.receivedPropNotificationsTableCellRow,
                 Constants.chatMessageNotificationsTableCellRow,
                 Constants.newUserNotificationsTableCellRow:
                break
                
            case Constants.notificationsTableSectionFooterRow:
                return UITableView.automaticDimension
                
            default:
                fatalError()
            }
            
        default:
            fatalError()
        }
        
        return 44.0
    }
}

// MARK: - DictionaryReceiver

extension NotificationsSettingsViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let accountUser = dictionary["accountUser"] as? AccountUser else {
            return
        }
        
        self.apply(accountUser: accountUser)
    }
}
