//
//  SchoolGradeTableViewController.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 15/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class SchoolGradeTableViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let gradeTableCellIdentifier = "GradeTableCell"
        static let graduatedTableCellIdentifier = "GraduatedTableCell"
        
        static let tableSectionHeaderIdentifier = "TableSectionHeader"
        static let tableSectionFooterIdentifier = "TableSectionFooter"
        
        static let middleSchoolTableSection = 0
        static let highSchoolTableSection = 1
        static let graduatedTableSection = 2
        
        static let tableSectionCount = 3
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var pickYourGradeLabel: UILabel!
    
    @IBOutlet fileprivate weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: -
    
    let schoolGrades = [Services.schoolGradesService.generateSchoolGrades(for: .middle),
                        Services.schoolGradesService.generateSchoolGrades(for: .high)]
    
    fileprivate(set) var accountUserBuffer: AccountUserBuffer?
    
    fileprivate(set) var hasAppliedData = false
    
    var finishSchoolGradeSelection: (() -> Void)?
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    fileprivate func config(cell: SchoolGradeTableViewCell, at indexPath: IndexPath) {
        let schoolGrade = self.schoolGrades[indexPath.section][indexPath.row]
        
        cell.number = schoolGrade.number
        cell.classYear = schoolGrade.classYear
    }
    
    fileprivate func config(cell: GraduatedTableViewCell, at indexPath: IndexPath) {
        cell.classYears = Services.schoolGradesService.alreadyGraduatedYears()
        
        if let classYear = self.accountUserBuffer?.classYear {
            cell.classYear = String(format: "Class of %d".localized(), classYear)
        } else {
            cell.classYear = nil
        }
        
        cell.onPickerValueChanged = { [unowned self] classYear in
            cell.classYear = String(format: "Class of %@".localized(), classYear)
            
            self.accountUserBuffer?.classYear = Int(classYear)
        }
        
        cell.onDoneButtonClicked = { [unowned self] in
            self.finishSchoolGradeSelection?()
        }
        
        cell.onCancelButtonClicked = { [weak self] in
            self?.view.endEditing(true)
        }
    }
    
    fileprivate func config(sectionHeaderView: SchoolGradeTableSectionHeaderView, at section: Int) {
        switch self.schoolGrades[section].first!.level {
        case .middle:
            sectionHeaderView.title = "Middle School".localized()
            
        case .high:
            sectionHeaderView.title = "High School".localized()
        }
    }
    
    // MARK: -
    
    func apply(accountUserBuffer: AccountUserBuffer) {
        Log.high("apply(accountUserBuffer: \(String(describing: accountUserBuffer.fullName)))", from: self)
        
        self.accountUserBuffer = accountUserBuffer
        
        if self.isViewLoaded {
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.pickYourGradeLabel.font = Fonts.heavy(ofSize: 22.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(accountUserBuffer: self.accountUserBuffer ?? AccountUserBuffer.shared)
        }

        self.subscribeToKeyboardNotifications()
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unsubscribeFromKeyboardNotifications()
        
        self.hasAppliedData = false
    }
}

// MARK: - UITableViewDataSource

extension SchoolGradeTableViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Constants.tableSectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Constants.middleSchoolTableSection, Constants.highSchoolTableSection:
            return self.schoolGrades[section].count + 1
            
        case Constants.graduatedTableSection:
            return 1
            
        default:
            fatalError()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch indexPath.section {
        case Constants.middleSchoolTableSection, Constants.highSchoolTableSection:
            if indexPath.row == self.schoolGrades[indexPath.section].count {
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableSectionFooterIdentifier,
                                                     for: indexPath)
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.gradeTableCellIdentifier,
                                                     for: indexPath)
                
                self.config(cell: cell as! SchoolGradeTableViewCell, at: indexPath)
            }
            
        case Constants.graduatedTableSection:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.graduatedTableCellIdentifier,
                                                 for: indexPath)
            
            self.config(cell: cell as! GraduatedTableViewCell, at: indexPath)
            
        default:
            fatalError()
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView = tableView.dequeueReusableCell(withIdentifier: Constants.tableSectionHeaderIdentifier)
        
        self.config(sectionHeaderView: sectionHeaderView as! SchoolGradeTableSectionHeaderView, at: section)
        
        return sectionHeaderView
    }
}

// MARK: - UITableViewDelegate

extension SchoolGradeTableViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let accountUserBuffer = self.accountUserBuffer else {
            return
        }
        
        switch indexPath.section {
        case Constants.middleSchoolTableSection, Constants.highSchoolTableSection:
            if indexPath.row == self.schoolGrades[indexPath.section].count {
                break
            }
            
            accountUserBuffer.classYear = self.schoolGrades[indexPath.section][indexPath.row].classYear
            
            self.finishSchoolGradeSelection?()
            
        case Constants.graduatedTableSection:
            tableView.cellForRow(at: indexPath)?.becomeFirstResponder()
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Constants.middleSchoolTableSection, Constants.highSchoolTableSection:
            if indexPath.row != self.schoolGrades[indexPath.section].count {
                return 44.0
            } else {
                return 24.0
            }
            
        case Constants.graduatedTableSection:
            return 44.0
            
        default:
            fatalError()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case Constants.middleSchoolTableSection, Constants.highSchoolTableSection:
            return 56.0
            
        case Constants.graduatedTableSection:
            return 0.0
            
        default:
            fatalError()
        }
    }
}

// MARK: - KeyboardHandler

extension SchoolGradeTableViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
