//
//  RequestContactsViewController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 28/09/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools

class RequestContactsViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let finishContactsRequesting = "FinishContactsRequesting"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate var skipBarButton: UIBarButtonItem!
    
    @IBOutlet fileprivate weak var emptyStateContainerView: UIView!
    @IBOutlet fileprivate weak var emptyStateView: EmptyStateView!
    
    // MARK: -
    
    fileprivate(set) var accountUserBuffer: AccountUserBuffer?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    fileprivate func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()
        
        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action
        
        self.emptyStateContainerView.isHidden = false
    }
    
    fileprivate func hideEmptyState() {
        self.emptyStateContainerView.isHidden = true
    }
    
    fileprivate func showNoContactsAccessState() {
        let action = EmptyStateAction(title: "Grant or Disallow".localized(),
                                      isPrimary: true,
                                      onClicked: { [unowned self] in
            switch Services.contactsService.accessState {
            case .notDetermined:
                self.requestContactsAccess()
                
            case .denied, .restricted:
                self.showContactsAccessRequest()
                
            case .authorized:
                self.requestPhoneNumbers()
            }
        })
        
        self.showEmptyState(title: "Allow access to contacts".localized(),
                            message: "We use contacts to find your friends on the app. We will never contact your friends without your permission.".localized(),
                            action: action)
    }
    
    fileprivate func showLoadingState() {
        if self.emptyStateContainerView.isHidden {
            self.showEmptyState(title: "Searching your contacts".localized(),
                                message: "We are loading list of your contacts. Please wait a bit.".localized())
        }
        
        self.emptyStateView.showActivityIndicator()
    }
    
    fileprivate func showContactsAccessRequest() {
        let alertController = UIAlertController(title: "Please allow access".localized(),
                                                message: "The app needs access to your contacts.\n\nPlease go to Settings and set to ON.".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        
        self.present(alertController, animated: true)
    }
    
    // MARK: -
    
    fileprivate func requestContactsAccess() {
        Log.high("requestContactsAccess()", from: self)
        
        firstly {
            Services.contactsService.requestAccess()
        }.done { accessState in
            switch accessState {
            case .notDetermined:
                self.showNoContactsAccessState()
                self.navigationItem.rightBarButtonItem = nil
                
            case .denied, .restricted:
                self.showNoContactsAccessState()
                self.navigationItem.rightBarButtonItem = self.skipBarButton
                
            case .authorized:
                self.requestPhoneNumbers()
                self.navigationItem.rightBarButtonItem = self.skipBarButton
            }
        }
    }
    
    fileprivate func requestPhoneNumbers() {
        Log.high("requestPhoneNumbers()", from: self)
        
        switch Services.contactsService.accessState {
        case .notDetermined:
            self.showNoContactsAccessState()
            self.navigationItem.rightBarButtonItem = nil

        case .denied, .restricted:
            self.showNoContactsAccessState()
            self.navigationItem.rightBarButtonItem = self.skipBarButton
            
        case .authorized:
            self.skipBarButton.isEnabled = false
            self.navigationItem.rightBarButtonItem = self.skipBarButton

            self.showLoadingState()
            
            firstly {
                after(seconds: 0.25)
            }.then {
                Services.contactsService.requestPhoneNumbers()
            }.done { phoneNumbers in
                self.skipBarButton.isEnabled = true
                
                guard let accountUserBuffer = self.accountUserBuffer else {
                    return
                }
                
                if let phoneNumbers = phoneNumbers {
                    accountUserBuffer.phoneNumbers = phoneNumbers
                    
                    self.performSegue(withIdentifier: Segues.finishContactsRequesting, sender: self)
                } else {
                    self.showNoContactsAccessState()
                }
            }
        }
    }
 
    // MARK: -
    
    func apply(accountUserBuffer: AccountUserBuffer) {
        Log.high("apply(accountUserBuffer: \(String(describing: accountUserBuffer.fullName)))", from: self)
        
        self.accountUserBuffer = accountUserBuffer
        
        if self.isViewLoaded {
            self.hideEmptyState()
            
            self.requestPhoneNumbers()
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(accountUserBuffer: self.accountUserBuffer ?? AccountUserBuffer.shared)
        }
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
}

// MARK: - DictionaryReceiver

extension RequestContactsViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let accountUserBuffer = dictionary["accountUserBuffer"] as? AccountUserBuffer else {
            return
        }
        
        self.apply(accountUserBuffer: accountUserBuffer)
    }
}
