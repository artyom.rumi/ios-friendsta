//
//  EnterFirstNameViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 15.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class EnterFirstNameViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let finishFirstNameEntering = "FinishFirstNameEntering"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var explanationLabel: UILabel!
    
    @IBOutlet fileprivate weak var firstNameTextField: UITextField!
    @IBOutlet fileprivate weak var nextButton: PrimaryButton!
    
    @IBOutlet fileprivate weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: -
    
    fileprivate(set) var accountUserBuffer: AccountUserBuffer?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onNextButtonTouchUpInside(_ sender: Any) {
        Log.high("onNextButtonTouchUpInside()", from: self)
        
        self.view.endEditing(true)
        
        guard let accountUserBuffer = self.accountUserBuffer else {
            return
        }
        
        guard let firstNameText = self.firstNameTextField.text else {
            return
        }
        
        accountUserBuffer.firstName = firstNameText
        
        self.performSegue(withIdentifier: Segues.finishFirstNameEntering, sender: sender)
    }
    
    // MARK: -
    
    fileprivate func updateTextField(with text: String?) {
        self.firstNameTextField.text = text
        
        self.nextButton.isEnabled = !(text?.isEmpty ?? true)
    }
    
    // MARK: -
    
    func apply(accountUserBuffer: AccountUserBuffer) {
        Log.high("apply(accountUserBuffer: \(String(describing: accountUserBuffer.fullName)))", from: self)
        
        self.accountUserBuffer = accountUserBuffer
        
        if self.isViewLoaded {
            self.updateTextField(with: accountUserBuffer.firstName)
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.heavy(ofSize: 22.0)
        self.explanationLabel.font = Fonts.regular(ofSize: 17.0)
        self.firstNameTextField.font = Fonts.regular(ofSize: 22.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
        self.setupFont()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.hasAppliedData {
            self.apply(accountUserBuffer: self.accountUserBuffer ?? AccountUserBuffer.shared)
        }
        
        self.subscribeToKeyboardNotifications()

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.firstNameTextField.becomeFirstResponder()

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        
        self.unsubscribeFromKeyboardNotifications()
        
        self.hasAppliedData = false
    }
}

// MARK: - UITextFieldDelegate

extension EnterFirstNameViewController: UITextFieldDelegate {
    
    // MARK: - Instance Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.onNextButtonTouchUpInside(textField)
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let replacedText: String
        
        if let text = self.firstNameTextField.text {
            replacedText = text.replacingCharacters(in: Range(range, in: text)!, with: string)
        } else {
            replacedText = string
        }
        
        let startPosition = textField.beginningOfDocument
        let cursorOffset = range.location + string.count
        
        self.updateTextField(with: replacedText)
        
        if let cursorPosition = textField.position(from: startPosition, offset: cursorOffset) {
            textField.selectedTextRange = textField.textRange(from: cursorPosition, to: cursorPosition)
        }
        
        return false
    }
}

// MARK: - DictionaryReceiver

extension EnterFirstNameViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let accountUserBuffer = dictionary["accountUserBuffer"] as? AccountUserBuffer else {
            return
        }
        
        self.apply(accountUserBuffer: accountUserBuffer)
    }
}

// MARK: - KeyboardHandler

extension EnterFirstNameViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}
