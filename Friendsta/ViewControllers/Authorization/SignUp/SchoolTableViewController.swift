//
//  SchoolTableViewController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 19.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class SchoolTableViewController: LoggedViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let nearbySchoolListLifeTime: TimeInterval = 60.0
        static let allSchoolListLifeTime: TimeInterval = 604800.0
        
        static let nearbySchoolsSegmentIndex = 0
        static let allSchoolsSegmentIndex = 1
        
        static let tableCellIdentifier = "TableCell"
        static let tableOtherCellIdentifier = "TableOtherCell"
        
        static let tableCellHeight: CGFloat = 64.0
        static let tableOtherCellHeight: CGFloat = 44.0
        
        static let tableLastRowBias = 40
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var searchTextField: SearchTextField!
    @IBOutlet fileprivate weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet fileprivate weak var emptyStateContainerView: UIView!
    @IBOutlet fileprivate weak var emptyStateView: EmptyStateView!
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var tableFooterView: UIView!
    
    @IBOutlet fileprivate weak var tableFooterActivityIndicatorView: UIActivityIndicatorView!
    
    fileprivate weak var tableRefreshControl: UIRefreshControl!
    
    // MARK: -
    
    fileprivate var tableFooterActivityCount = 0
    
    fileprivate var isRefreshingData = false
    fileprivate var isLoadingMoreData = false
    fileprivate var hasUnappliedData = true
    
    fileprivate var searchRefreshTimer: Timer?
    
    // MARK: -
    
    fileprivate(set) var accountUserBuffer: AccountUserBuffer?
    
    fileprivate(set) var schoolListType: SchoolListType = .unknown
    fileprivate(set) var schoolList: SchoolList?
    
    var finishSchoolSelection: (() -> Void)?
    var showUnavailableSchool: (() -> Void)?
    var unauthorize: (() -> Void)?
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onViewTapped(_ sender: Any) {
        Log.high("onViewTapped()", from: self)
        
        self.view.endEditing(true)
    }
    
    @IBAction fileprivate func onSearchTextFieldChanged(_ sender: Any) {
        Log.high("onSearchTextFieldChanged()", from: self)
        
        self.restartSearchRefreshTimer()
    }
    
    @IBAction fileprivate func onSegmentedControlValueChanged(_ sender: Any) {
        Log.high("onSegmentedControlValueChanged()", from: self)
        
        switch self.segmentedControl.selectedSegmentIndex {
        case Constants.nearbySchoolsSegmentIndex:
            self.apply(schoolListType: .nearby(classYear: self.accountUserBuffer?.classYear ?? 0))
            
        case Constants.allSchoolsSegmentIndex:
            self.apply(schoolListType: .all(classYear: self.accountUserBuffer?.classYear ?? 0))
            
        default:
            fatalError()
        }
    }
    
    @objc fileprivate func onTableRefreshControlRequested(_ sender: Any) {
        Log.high("onTableRefreshControlRequested()", from: self)
        
        self.refreshSchoolList()
    }
    
    @objc fileprivate func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.refreshSchoolList()
    }
    
    // MARK: -
    
    fileprivate func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()
        
        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action
        
        self.emptyStateContainerView.isHidden = false
    }
    
    fileprivate func hideEmptyState() {
        self.emptyStateContainerView.isHidden = true
    }
    
    fileprivate func showNoDataState(retryHandler: (() -> Void)?) {
        let action: EmptyStateAction?
        
        if let retryHandler = retryHandler {
            action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: retryHandler)
        } else {
            action = nil
        }
        
        self.showEmptyState(title: "Schools not found".localized(), message: "", action: action)
    }
    
    fileprivate func showLoadingState() {
        if self.emptyStateContainerView.isHidden {
            self.showEmptyState(title: "Loading schools".localized(),
                                message: "We are loading list of schools.\nPlease wait a bit.".localized())
        }
        
        self.emptyStateView.showActivityIndicator()
    }
    
    fileprivate func showNoLocationAccessState() {
        let action = EmptyStateAction(title: "Allow access to location".localized(), isPrimary: true, onClicked: { [unowned self] in
            switch Services.locationService.accessState {
            case .notDetermined:
                self.requestLocationAccess()
                
            case .notAvailable, .denied, .restricted:
                self.showLocationAccessRequest()
                
            case .authorized:
                self.refreshNearbySchoolList()
            }
        })
        
        self.showEmptyState(title: "Allow Access".localized(),
                            message: "Your privacy is important. We use location to find nearby schools.".localized(),
                            action: action)
    }
    
    fileprivate func showLocationAccessRequest() {
        let alertController = UIAlertController(title: "Please allow access".localized(),
                                                message: "The application needs access to your location.\n\nPlease go to Settings and set to ON.".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: nil))
        
        alertController.addAction(UIAlertAction(title: "Settings".localized(), style: .default, handler: { action in
            guard let url = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }))
        
        self.present(alertController, animated: true)
    }
    
    fileprivate func showTableFooterActivityIndicator() {
        self.tableFooterActivityCount += 1
        
        if self.tableFooterActivityCount <= 1 {
            self.tableFooterActivityIndicatorView.startAnimating()
            
            UIView.animate(withDuration: 0.25, animations: {
                self.layoutTableFooterView()
            })
        }
    }
    
    fileprivate func hideTableFooterActivityIndicator() {
        guard self.tableFooterActivityCount > 0 else {
            return
        }
        
        self.tableFooterActivityCount -= 1
        
        if self.tableFooterActivityCount == 0 {
            UIView.animate(withDuration: 0.25, animations: {
                self.layoutTableFooterView()
            }, completion: { finished in
                self.tableFooterActivityIndicatorView.stopAnimating()
            })
        }
    }
    
    // MARK: -
    
    fileprivate func config(cell: SchoolTableViewCell, at indexPath: IndexPath) {
        guard let school = self.schoolList?[indexPath.row] else {
            return
        }
        
        cell.title = school.title
        cell.location = school.location
        cell.memberCount = Int(school.memberCount)
    }
    
    fileprivate func layoutTableFooterView() {
        let tableFooterViewHeight: CGFloat = (self.tableFooterActivityCount > 0) ? 56.0 : 0.0
        
        self.tableFooterView.frame = CGRect(x: 0.0,
                                            y: 0.0,
                                            width: self.tableView.bounds.width,
                                            height: tableFooterViewHeight).adjusted
        
        self.tableView.tableFooterView = self.tableFooterView
    }
    
    // MARK: -
    
    fileprivate func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: {
            retryHandler?()
        })
        
        switch error as? WebError {
        case .some(.unauthorized):
            self.unauthorize?()
            
        case .some(.connection), .some(.timeOut):
            if self.schoolList?.isEmpty ?? true {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            }
            
        default:
            if self.schoolList?.isEmpty ?? true {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        }
    }
    
    fileprivate func handle(silentError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.unauthorize?()
            
        default:
            break
        }
    }
    
    fileprivate func requestLocationAccess() {
        Log.high("requestLocationAccess()", from: self)
        
        firstly {
            Services.locationService.requestAccess()
            }.done { accessState in
                switch accessState {
                case .notAvailable, .notDetermined, .denied, .restricted:
                    self.showNoLocationAccessState()
                    
                case .authorized:
                    self.refreshNearbySchoolList()
                }
        }
    }
    
    fileprivate func loadMoreSchools() {
        Log.high("loadMoreSchools()", from: self)
        
        guard (!self.isLoadingMoreData) && (!self.isRefreshingData) else {
            return
        }
        
        guard let schoolList = self.schoolList else {
            return
        }
        
        self.showTableFooterActivityIndicator()
        
        self.isLoadingMoreData = true
        
        firstly {
            after(seconds: 0.25)
            }.then {
                Services.schoolsService.loadMoreSchools(of: schoolList)
            }.ensure {
                self.hideTableFooterActivityIndicator()
                
                self.isLoadingMoreData = false
            }.done { schoolList in
                if !self.isRefreshingData {
                    self.apply(schoolList: schoolList)
                }
            }.catch { error in
                self.handle(silentError: error)
        }
    }
    
    fileprivate func refreshNearbySchoolList() {
        Log.high("refreshNearbySchoolList()", from: self)
        
        switch Services.locationService.accessState {
        case .notAvailable, .notDetermined, .denied, .restricted:
            self.showNoLocationAccessState()
            
        case .authorized:
            guard let schoolList = self.schoolList else {
                return
            }
            
            self.isRefreshingData = true
            
            if !self.tableRefreshControl.isRefreshing {
                if (self.schoolList?.isEmpty ?? true) || (!self.emptyStateContainerView.isHidden) {
                    self.showLoadingState()
                }
            }
            
            firstly {
                Services.locationService.requestLocation()
                }.then { location in
                    Services.schoolsService.refreshNearbySchoolList(of: Int(schoolList.classYear),
                                                                    location: location ?? .northPole,
                                                                    keywords: self.searchTextField.text)
                }.ensure {
                    if self.tableRefreshControl.isRefreshing {
                        self.tableRefreshControl.endRefreshing()
                    }
                    
                    self.isRefreshingData = false
                }.done { schoolList in
                    if ((self.searchTextField.text ?? "") == (schoolList.keywords ?? "")) {
                        self.apply(schoolList: schoolList)
                    }
                }.catch { error in
                    self.handle(stateError: error, retryHandler: { [weak self] in
                        self?.refreshSchoolList()
                    })
            }
        }
    }
    
    fileprivate func refreshAllSchoolList() {
        Log.high("refreshAllSchoolList()", from: self)
        
        guard let schoolList = self.schoolList else {
            return
        }
        
        self.isRefreshingData = true
        
        if !self.tableRefreshControl.isRefreshing {
            if (self.schoolList?.isEmpty ?? true) || (!self.emptyStateContainerView.isHidden) {
                self.showLoadingState()
            }
        }
        
        firstly {
            Services.schoolsService.refreshAllSchoolList(of: Int(schoolList.classYear), keywords: self.searchTextField.text)
            }.ensure {
                if self.tableRefreshControl.isRefreshing {
                    self.tableRefreshControl.endRefreshing()
                }
                
                self.isRefreshingData = false
            }.done { schoolList in
                if ((self.searchTextField.text ?? "") == (schoolList.keywords ?? "")) {
                    self.apply(schoolList: schoolList)
                }
            }.catch { error in
                self.handle(stateError: error, retryHandler: { [weak self] in
                    self?.refreshSchoolList()
                })
        }
    }
    
    fileprivate func refreshSchoolList() {
        Log.high("refreshSchoolList()", from: self)
        
        self.resetSearchRefreshTimer()
        
        switch self.schoolListType {
        case .unknown:
            break
            
        case .nearby:
            self.refreshNearbySchoolList()
            
        case .all:
            self.refreshAllSchoolList()
        }
    }
    
    fileprivate func resetSearchRefreshTimer() {
        self.searchRefreshTimer?.invalidate()
        self.searchRefreshTimer = nil
    }
    
    fileprivate func restartSearchRefreshTimer() {
        self.resetSearchRefreshTimer()
        
        self.showLoadingState()
        
        self.searchRefreshTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { [weak self] timer in
            self?.refreshSchoolList()
        })
    }
    
    fileprivate func apply(schoolList: SchoolList) {
        Log.high("apply(schoolList: \(schoolList.count))", from: self)
        
        guard self.schoolListType == schoolList.listType else {
            return
        }
        
        self.schoolList = schoolList
        
        self.hideEmptyState()
        
        self.tableView.reloadData()
    }
    
    fileprivate func apply(schoolListType: SchoolListType) {
        Log.high("apply(schoolListType-----: \(schoolListType))", from: self)
        
        self.schoolListType = schoolListType
        
        let schoolList: SchoolList
        
        switch self.schoolListType {
        case .unknown:
            schoolList = Services.cacheViewContext.schoolListsManager.firstOrNew(with: .unknown)
            
        case .nearby:
            self.segmentedControl.selectedSegmentIndex = Constants.nearbySchoolsSegmentIndex
            
            schoolList = Services.cacheViewContext.schoolListsManager.firstOrNew(with: self.schoolListType)
            
        case .all:
            self.segmentedControl.selectedSegmentIndex = Constants.allSchoolsSegmentIndex
            
            schoolList = Services.cacheViewContext.schoolListsManager.firstOrNew(with: self.schoolListType)
        }
        
        if ((self.searchTextField.text ?? "") != (schoolList.keywords ?? "")) {
            schoolList.clearSchools()
        }
        
        self.apply(schoolList: schoolList)
        
        if (self.schoolList?.isEmpty ?? true) || (schoolList.keywords != self.searchTextField.text) {
            self.refreshSchoolList()
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.heavy(ofSize: 22.0)
        self.searchTextField.font = Fonts.regular(ofSize: 14.0)
        self.segmentedControl.setTitleTextAttributes([.font: Fonts.regular(ofSize: 13.0)], for: .normal)
    }
    
    // MARK: -
    
    func apply(accountUserBuffer: AccountUserBuffer) {
        Log.high("apply(accountUserBuffer: \(String(describing: accountUserBuffer.fullName)))", from: self)
        
        self.accountUserBuffer = accountUserBuffer
        
        if self.isViewLoaded {
            self.apply(schoolListType: .all(classYear: accountUserBuffer.classYear ?? 0))
            
            self.hasUnappliedData = false
        } else {
            self.hasUnappliedData = true
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        let tableRefreshControl = UIRefreshControl()
        
        tableRefreshControl.addTarget(self,
                                      action: #selector(self.onTableRefreshControlRequested(_:)),
                                      for: .valueChanged)
        
        self.tableView.refreshControl = tableRefreshControl
        self.tableRefreshControl = tableRefreshControl
        
        self.hasUnappliedData = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isRefreshingData = false
        self.isLoadingMoreData = false
        
        if self.hasUnappliedData {
            self.apply(accountUserBuffer: self.accountUserBuffer ?? AccountUserBuffer.shared)
        }
        
        self.subscribeToKeyboardNotifications()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onApplicationWillEnterForeground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unsubscribeFromKeyboardNotifications()
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
        self.hasUnappliedData = true
    }
}

// MARK: - UITableViewDataSource

extension SchoolTableViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.schoolList?.count ?? 0) + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        if let schoolList = self.schoolList, indexPath.row < schoolList.count {
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableCellIdentifier,
                                                 for: indexPath)
            
            self.config(cell: cell as! SchoolTableViewCell, at: indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableOtherCellIdentifier,
                                                 for: indexPath)
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension SchoolTableViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == self.tableView.numberOfSections - 1 {
            let lastRowIndex = self.tableView.numberOfRows(inSection: indexPath.section) - 1
            
            if (lastRowIndex > 0) && (self.tableView.contentOffset.y > CGFloat.leastNonzeroMagnitude) {
                if indexPath.row > lastRowIndex - Constants.tableLastRowBias {
                    self.loadMoreSchools()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let schoolList = self.schoolList, indexPath.row < schoolList.count {
            return Constants.tableCellHeight
        } else {
            return Constants.tableOtherCellHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let accountUserBuffer = self.accountUserBuffer else {
            return
        }
        
        if let schoolList = self.schoolList, indexPath.row < schoolList.count {
            let school = schoolList[indexPath.row]
            
            accountUserBuffer.schoolUID = school.uid
            accountUserBuffer.schoolTitle = school.title
            accountUserBuffer.schoolLocation = school.location
            
            self.finishSchoolSelection?()
        } else {
            self.showUnavailableSchool?()
        }
    }
}

// MARK: - UITextFieldDelegate

extension SchoolTableViewController: UITextFieldDelegate {
    
    // MARK: - Instance Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}

// MARK: - KeyboardScrollableHandler

extension SchoolTableViewController: KeyboardScrollableHandler {
    
    // MARK: - Instance Properties
    
    var scrollableView: UITableView {
        return self.tableView
    }
}
