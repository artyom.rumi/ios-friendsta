//
//  ChatTableViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 11.05.2018.
//  Copyright © 2018 Decision Accelerator LLC. All rights reserved.
//

// swiftlint:disable file_length
// swiftlint:disable type_body_length
// swiftlint:disable function_body_length
// swiftlint:disable vertical_whitespace

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork
import UserNotifications //>>> 1.
import SwiftKeychainWrapper

class ChatTableViewController: LoggedViewController, ErrorMessagePresenter, EmptyStateViewCustomer {
    
    ///// ********* Lance's Code ********* /////
    
    //>>> 2.
    func hexStringToUIColor(hex: String) -> UIColor {
        var cString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue: UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                       green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                       blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                       alpha: CGFloat(1.0)
        )
    }
    
    //>>> 3.
    // MARK: - Programmatic UIElements
    fileprivate lazy var notificationButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.setTitle("Enable Notifications", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        
        let yellowColor = hexStringToUIColor(hex: "ffffcc")
        
        button.backgroundColor = yellowColor
        button.addTarget(self, action: #selector(notificationButtonPressed), for: .touchUpInside)
        
        button.layer.cornerRadius = 0
        button.layer.masksToBounds = true
        button.sizeToFit()
        
        return button
    }()
    
    //>>> 4.
    @objc fileprivate func notificationButtonPressed() {
        
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        DispatchQueue.main.async {
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                })
            } else {
                print("Settings URL did Not Open")
            }
        }
    }
    
    //>>> 5.
    fileprivate var notificationButtonHeightConstraint: NSLayoutConstraint?
    fileprivate func configureAnchors() {
        
        notificationButtonHeightConstraint?.isActive = false
        
        view.addSubview(notificationButton)
        notificationButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 1).isActive = true
        notificationButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        notificationButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        notificationButtonHeightConstraint = notificationButton.heightAnchor.constraint(equalToConstant: notificationButtonNormalHeight)
        notificationButtonHeightConstraint?.isActive = true
    }
    
    //>>> 6.
    fileprivate func toggleNotificationButtonHeightToZero() {
        notificationButtonHeightConstraint?.isActive = false
        notificationButtonHeightConstraint = notificationButton.heightAnchor.constraint(equalToConstant: 0)
        notificationButtonHeightConstraint?.isActive = true
    }
    
    //>>> 7.
    fileprivate func toggleNotificationButtonHeightToNormalHeight() {
        notificationButtonHeightConstraint?.isActive = false
        notificationButtonHeightConstraint = notificationButton.heightAnchor.constraint(equalToConstant: notificationButtonNormalHeight)
        notificationButtonHeightConstraint?.isActive = true
    }
    
    //>>> 8.
    fileprivate func decideWetherOrNotToEnableNotifications() {
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                // Already authorized
                
                DispatchQueue.main.async { [weak self] in
                    self?.toggleNotificationButtonHeightToZero()
                }
                
            } else {
                // Either denied or notDetermined
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (granted, error) in
                    // add your own
                    //UNUserNotificationCenter.current().delegate = self
                    if !granted {
                        print("not granted")
                        
                        DispatchQueue.main.async { [weak self] in
                            self?.toggleNotificationButtonHeightToNormalHeight()
                        }
                        
                    } else {
                        print("granted")
                    }
                }
            }
        }
    }
    
    //>>> 9.
    public func set2LinesOfTextForNotifcationButton() {
        
        let button = self.notificationButton
        
        //getting the range to separate the button title strings
        //let buttonText = "\(buttonTitle)\n\(String(numOfFollowing))" as NSString
        let buttonText = "Notifications are off\nTap to re-enable" as NSString
        let newlineRange: NSRange = buttonText.range(of: "\n")
        
        //getting both substrings
        var substring1 = ""
        var substring2 = ""

        if(newlineRange.location != NSNotFound) {
            substring1 = buttonText.substring(to: newlineRange.location)
            substring2 = buttonText.substring(from: newlineRange.location)
        }
        
        let attrString1 = NSMutableAttributedString(string: substring1,
                                                    attributes: [NSMutableAttributedString.Key.font:
                                                        UIFont.boldSystemFont(ofSize: 16),
                                                                 NSMutableAttributedString.Key.foregroundColor: UIColor.black])
        
        let attrString2 = NSMutableAttributedString(string: substring2,
                                                    attributes: [NSMutableAttributedString.Key.font:
                                                        UIFont.systemFont(ofSize: 14),
                                                                 NSMutableAttributedString.Key.foregroundColor: UIColor.black])

        //appending both attributed strings
        attrString1.append(attrString2) //<<
        button.setAttributedTitle(attrString1, for: [])
    }
    
    //>>> 10.
    fileprivate let notificationButtonNormalHeight: CGFloat = 60
    
    //>>> 11.
    @IBOutlet private weak var searchTextFieldTopConstraint: NSLayoutConstraint!
    
    public var feedsFromFeedVC = [Feed]()
    public var newFeed: Feed?
    
    ///// ********* Lance's Code ********* /////
    
    // MARK: - Nested Types
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let showChat = "ShowChat"
        static let showGroupChat = "ShowGroupChat"
        static let showChatNoAnimation = "ShowChatNoAnimation"
        static let unauthorize = "Unauthorize"
        static let showChatPhoto = "ShowChatPhoto"
        static let showChatmatesChoice = "ShowChatmatesChoice"
    }
    
    // MARK: -
    
    enum FilterType {
        
        // MARK: - Enumeration Cases
        
        case none
        case search(text: String)
    }
    
    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let tableCellIdentifier = "TableCell"
        
        static let lastWelcomeMessage = "Welcome to your chat!".localized()
        static let lastMatchMessage = "You got a match!".localized()
        static let lastOneRevealMessage = "Your chatmate is ready to reveal!".localized()
        static let lastMutualRevealMessage = "Mutual reveal now available!".localized()
        static let lastCancelRevealMessage = "Chatmate canceled reveal!".localized()
        
        static let lastPhotoMessage = "Photo".localized()
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet private weak var searchTextField: SearchTextField!
        
    @IBOutlet private weak var tableView: UITableView!
    
    @IBOutlet private weak var emptyStateViewOutlet: EmptyStateView!
    
    private var tableRefreshControl: UIRefreshControl!
    
    // MARK: -
    
    private var chatListTimer: Timer?
    
    private lazy var onPhotoClosed: ChatPhotoViewController.ClosePhotoType = { chatPhotoMessage in
        guard let chatUID = chatPhotoMessage?.chatUID, let chat = Services.chatsService.chat(for: chatUID) else {
            return
        }
        
        self.performSegue(withIdentifier: Segues.showChatNoAnimation, sender: chat)
    }
    
    private(set) var filterType: FilterType = .none
    
    private(set) var chatFullList: [ChatListItem] = []
    private(set) var chatList: [ChatListItem] = []
    
    private(set) var isRefreshingData = false
    
    // MARK: -
    
    lazy var emptyStateView: EmptyStateView! = {
        return self.emptyStateViewOutlet
    }()
    
    // MARK: -

    var onBackTapped: (() -> Void)?
    var onChatOpened: (() -> Void)?
    var onChatClosed: (() -> Void)?
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromChatListEvents()
        self.unsubscribeFromChatMessagesEvents()
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onChatPhotoClosed(segue: UIStoryboardSegue) {
        Log.high("onChatPhotoClosed(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction private func onViewTapped(_ sender: Any) {
        Log.high("onViewTapped()", from: self)
        
        self.view.endEditing(true)
    }
    
    @IBAction private func onSearchTextFieldChanged(_ sender: Any) {
        Log.high("onSearchTextFieldChanged()", from: self)
        
        if let searchText = self.searchTextField.text, !searchText.isEmpty {
            self.apply(filterType: .search(text: searchText), chatList: self.chatFullList)
        } else {
            self.apply(filterType: .none, chatList: self.chatFullList)
        }
    }

    @IBAction private func onRightBarButtonItemTouchUpInside(_ sender: Any) {
        Log.high("onRightBarButtonItemTouchUpInside()", from: self)
        
        self.performSegue(withIdentifier: Segues.showChatmatesChoice, sender: self)
    }
    
    @objc private func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.refreshAllChatList()
        
        //>>> 16.
        decideWetherOrNotToEnableNotifications()
    }
    
    @objc fileprivate func onTableRefreshControlRequested(_ sender: Any) {
        Log.high("onTableRefreshControlRequested()", from: self)
        
        self.refreshAllChatList()
    }
    
    // MARK: -

    private func showNoDataState() {
        self.showEmptyState(title: "No new content".localized(),
                            message: "When friends share content or react to yours, it will appear here.".localized(),
                            action: nil)
    }
    
    private func showLoadingState() {
        if self.emptyStateView.isHidden {
            self.showEmptyState(title: "Loading your chats".localized(),
                                message: "Please wait a moment.".localized())
        }
        
        self.emptyStateView.showActivityIndicator()
    }
    
    // MARK: -
    
    private func loadAvatarImage(for cell: ChatTableViewCell, with user: User) {
        cell.avatarImage = nil
        
        if let imageURL = user.mediumAvatarURL {
            Services.imageLoader.loadImage(for: imageURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarSmallPlaceholder)
        }
    }
    
    private func loadAvatarImage(for cell: ChatTableViewCell, with mask: ChatMask) {
        cell.avatarImage = nil
        
        if let imageURL = mask.mediumAvatarURL {
            Services.imageLoader.loadImage(for: imageURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarSmallPlaceholder)
        }
    }
    
    private func loadAvatarImage(for cell: ChatTableViewCell, with photoMessage: ChatPhotoMessage) {
        cell.avatarImage = nil
        
        if let imageURL = photoMessage.smallPhotoURL {
            Services.imageLoader.loadImage(for: imageURL, in: cell.avatarImageViewTarget, placeholder: Images.avatarSmallPlaceholder, completionHandler: { image in
                photoMessage.smallPhotoData = image?.jpegData(compressionQuality: 1)
            })
        }
    }
    
    // MARK: -
    
    private func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    private func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: {
            retryHandler?()
        })
        
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
           
        case .some(.connection), .some(.timeOut):
            if self.chatFullList.isEmpty {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            }
            
        default:
            if self.chatFullList.isEmpty {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        }
    }
    
    private func refreshAllChatList() {
        Log.high("refreshAllChatList()", from: self)
        
        self.isRefreshingData = true
        
        if self.chatList.isEmpty || (!self.emptyStateView.isHidden) {
            self.showLoadingState()
        }
        
        firstly {
            after(seconds: 0.25)
        }.then {
            Services.chatMasksService.refreshChatMasks()
        }.then { chatMasks in
            Services.chatsService.refreshAllChatList()
        }.ensure {
            if self.tableRefreshControl.isRefreshing {
                self.tableRefreshControl.endRefreshing()
            }
            
            self.isRefreshingData = false
        }.done { chatList in
            self.apply(filterType: self.filterType, chatList: chatList)
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshAllChatList()
            })
        }
    }
    
    private func resetChatListTimer() {
        self.chatListTimer?.invalidate()
        self.chatListTimer = nil
    }
    
    private func restartChatListTimer() {
        self.resetChatListTimer()
        
        guard !self.isRefreshingData else {
            return
        }
        
        self.chatListTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { [weak self] timer in
            if let viewController = self {
                viewController.apply(filterType: viewController.filterType,
                                     chatList: Services.cacheViewContext.chatListsManager.fetch(with: .all))
            }
        })
    }
    
    private func restartChatListTimer(with chatListItems: [ChatListItem]) {
        guard chatListItems.contains(where: { chatListItem in
            switch chatListItem.listType {
            case .some(.all):
                return true
                
            default:
                return false
            }
        }) else {
            return
        }
        
        self.restartChatListTimer()
    }
    
    private func restartChatListTimer(with chatMessages: [ChatMessage]) {
        self.restartChatListTimer()
    }
    
    private func subscribeToChatListEvents() {
        self.unsubscribeFromChatListEvents()
        
        let chatListsManager = Services.cacheViewContext.chatListsManager
        
        chatListsManager.objectsChangedEvent.connect(self, handler: { [weak self] chatListItems in
            self?.restartChatListTimer(with: chatListItems)
        })
        
        chatListsManager.startObserving()
    }
    
    private func unsubscribeFromChatListEvents() {
        Services.cacheViewContext.chatListsManager.objectsChangedEvent.disconnect(self)
    }
    
    private func subscribeToChatMessagesEvents() {
        self.unsubscribeFromChatMessagesEvents()
        
        let chatMessagesManager = Services.cacheViewContext.chatMessagesManager
        
        chatMessagesManager.objectsChangedEvent.connect(self, handler: { [weak self] chatMessages in
            self?.restartChatListTimer(with: chatMessages)
        })
        
        chatMessagesManager.startObserving()
    }
    
    private func unsubscribeFromChatMessagesEvents() {
        Services.cacheViewContext.chatMessagesManager.objectsChangedEvent.disconnect(self)
    }
    
    private func apply(filterType: FilterType, chatList: [ChatListItem], canShowState: Bool = true) {
        Log.high("apply(filterType: \(filterType), chatList: \(chatList.count))", from: self)
        
        self.filterType = filterType
        
        let chatMasks = Services.cacheViewContext.chatMasksManager.fetch()
        
        self.chatFullList = chatList.filter({ chatListItem in
            guard let chat = chatListItem.chat else {
                return false
            }
            
            let mask = chatMasks.filter({ (chatMask) -> Bool in
                return chatMask.chatID == chat.uid
            })
            
            chat.chatMask = mask.first
            return !(chat.isErased)

        }).sorted(by: { first, second in
            if let firstModifiedDate = first.chat?.modifiedDate, let secondModifiedDate = second.chat?.modifiedDate {
                return firstModifiedDate > secondModifiedDate
            } else {
                return first.sorting > second.sorting
            }
        })
        
        // Here:
        //chatFullList = chatFullList.filterDuplicates({ $0.chat?.modifiedDate ?? 0 == $1.chat?.modifiedDate ?? 0 })
        for chatListItem in chatFullList {
            if let accountUserUID = Services.accountAccess?.userUID {
                guard let chat = chatListItem.chat else { continue }
                let chatMember = chat.members?.first(where: { chatMember in
                    return ((chatMember as! ChatMember).user?.uid != accountUserUID)
                }) as? ChatMember
                if let potentialChatMember = chatMember, let potentialChatMemberUser = potentialChatMember.user {
                    if potentialChatMemberUser.uid != accountUserUID {
                        // Here:
                    }
                }
            }
        }
        
        /*
        /////
        chatFullList = chatFullList.filterDuplicates({ (chatListItem1, chatListItem2) -> Bool in
            
            let currentDate = Date()
            
            var item1 = currentDate
            if let chat1 = chatListItem1.chat {
                item1 = chat1.modifiedDate ?? Date()
            } else {
                return false
            }
            
            var item2 = currentDate
            if let chat2 = chatListItem2.chat {
                item2 = chat2.modifiedDate ?? Date()
            } else {
                return true
            }
            
            if item1 == item2 {
                return true
            } else {
                return false
            }
        })
        
        chatFullList.sort { (chatListItem1, chatListItem2) -> Bool in
            
            var item1 = Date()
            if let chat1 = chatListItem1.chat {
                item1 = chat1.modifiedDate ?? Date()
            }
            
            var item2 = Date()
            if let chat2 = chatListItem2.chat {
                item2 = chat2.modifiedDate ?? Date()
            }
            
            return item1 > item2
        }
        /////
        */
        
        if self.isViewLoaded {
            switch filterType {
            case .none:
                self.chatList = self.chatFullList
                
            case .search(let text):
                let searchText = text.lowercased()
                
                if let accountUserUID = Services.accountAccess?.userUID {
                    self.chatList = self.chatFullList.filter({ chatListItem in
                        if let chat = chatListItem.chat {
                            return chat.members?.contains(where: { chatMember in
                                let chatMember = chatMember as! ChatMember
                                
                                if chatMember.user?.uid != accountUserUID {
                                    if chat.isIncognito {
                                        return chatMember.mask?.name?.lowercased().contains(searchText) ?? false
                                    } else {
                                        return chatMember.user?.fullName?.lowercased().contains(searchText) ?? false
                                    }
                                } else {
                                    return false
                                }
                            }) ?? false
                        } else {
                            return false
                        }
                    })
                } else {
                    self.chatList = []
                }
            }
            
            if self.chatFullList.isEmpty && canShowState {
                self.showNoDataState()
            } else {
                self.hideEmptyState()
            }
            
            if self.isViewAppeared {
                self.tableView.beginUpdates()
                self.tableView.deleteSections(IndexSet(integersIn: 0..<self.tableView.numberOfSections), with: .fade)
                self.tableView.insertSections(IndexSet(integersIn: 0..<min(self.chatList.count, 1)), with: .fade)
                self.tableView.endUpdates()
            } else {
                self.tableView.reloadData()
            }
        }
        
        self.resetChatListTimer()
    }
    
    private func markAsViewed(chatMessage: ChatMessage) {
        Log.high("markAsViewed(chatMessage: \(String(describing: chatMessage.uid)))", from: self)
        
        firstly {
            Services.chatMessagesService.markAsViewed(chatMessage: chatMessage)
        }.catch { error in
            self.handle(stateError: error, retryHandler: {
                self.markAsViewed(chatMessage: chatMessage)
            })
        }
    }
    
    // MARK: -
    
    private func setupFont() {
        self.searchTextField.font = Fonts.regular(ofSize: 14.0)
        self.emptyStateView.messageFont = Fonts.regular(ofSize: 18.0)
    }

    private func setupBackButton() {
        if self.onBackTapped != nil {
            let backButton = UIBarButtonItem(image: UIImage(named: "BackArrow"), style: .done, target: self, action: #selector(onBackTouchUpInside))
            backButton.tintColor = UIColor.black
            self.navigationItem.leftBarButtonItem = backButton
        }
    }

    @objc private func onBackTouchUpInside() {
        self.onBackTapped?()
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tableRefreshControl = UIRefreshControl()
        
        tableRefreshControl.addTarget(self,
                                      action: #selector(self.onTableRefreshControlRequested(_:)),
                                      for: .valueChanged)
        
        self.tableView.refreshControl = tableRefreshControl
        self.tableRefreshControl = tableRefreshControl
        
        self.setupFont()
        self.setupBackButton()
        
        //>>> 12.
        configureAnchors()
        
        //>>> 14.
        searchTextFieldTopConstraint.isActive = false
        searchTextFieldTopConstraint = searchTextField.topAnchor.constraint(equalTo: notificationButton.bottomAnchor)
        searchTextFieldTopConstraint.isActive = true
        
        //>>> 15.
        set2LinesOfTextForNotifcationButton()
        decideWetherOrNotToEnableNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isRefreshingData = false
        
        self.apply(filterType: self.filterType,
                   chatList: Services.cacheViewContext.chatListsManager.fetch(with: .all),
                   canShowState: false)
        
        if self.chatFullList.isEmpty {
            self.refreshAllChatList()
        }
        
        self.subscribeToChatListEvents()
        self.subscribeToChatMessagesEvents()
        self.subscribeToKeyboardNotifications()

        self.onChatClosed?()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onApplicationWillEnterForeground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.tableRefreshControl.isRefreshing {
            self.tableRefreshControl.endRefreshing()
        }
        
        self.unsubscribeFromChatListEvents()
        self.unsubscribeFromChatMessagesEvents()
        self.unsubscribeFromKeyboardNotifications()
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
    
        switch segue.identifier {
        case Segues.showChat, Segues.showChatNoAnimation, Segues.showGroupChat:
            guard let chat = sender as? Chat else {
                fatalError()
            }
                
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["chat": chat, "feedsFromFeedVC": feedsFromFeedVC])
            }
            
        case Segues.showChatPhoto:
            guard let chatPhotoMessage = sender as? ChatPhotoMessage else {
                fatalError()
            }
            
            let dictionaryReceiver: DictionaryReceiver?
            
            if let navigationController = segue.destination as? UINavigationController {
                dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
            } else {
                dictionaryReceiver = segue.destination as? DictionaryReceiver
            }
            
            dictionaryReceiver?.apply(dictionary: ["chatPhotoMessage": chatPhotoMessage, "onPhotoClosed": onPhotoClosed])
            
        default:
            break
        }
    }
}

// MARK: - UITableViewDataSource

extension ChatTableViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.chatList.isEmpty ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableCellIdentifier,
                                                 for: indexPath)
        
        self.config(cell: cell as! ChatTableViewCell, at: indexPath)
        
        return cell
    }
}

// MARK: - UITableViewDataSource Helper

extension ChatTableViewController {

    // MARK: - Helper Methods
    
    private func config(cell: ChatTableViewCell, at indexPath: IndexPath) {
        guard let chat = self.chatList[indexPath.row].chat else {
            return
        }
        
        guard let accountUserUID = Services.accountAccess?.userUID else {
            return
        }
        
        if chat.isGroupChat {
            cell.avatarImageViewTarget.contentMode = .scaleAspectFit
            cell.avatarImage = Images.groupChatAvatarPlaceholder
            cell.fullName = chat.name
        } else {
            let chatMember = chat.members?.first(where: { chatMember in
                return ((chatMember as! ChatMember).user?.uid != accountUserUID)
            }) as? ChatMember

            cell.avatarImage = nil
            cell.avatarImageViewTarget.contentMode = .scaleAspectFill

            var isRevealed = true

            chat.members?.forEach({ (chatMember) in
                if let member = chatMember as? ChatMember, member.isDeanonymized == false {
                    isRevealed = false
                }
            })
            
            cell.fullNameTextColor = Colors.blackText
            
            /*
            if isRevealed {
                guard let chatMemberName = chatMember?.user?.fullName else {
                    return
                }

                cell.fullName = chatMemberName
            } else {
                cell.fullName = "A friend".localized()
            }
            */
            
            ///// ********* Lance's Code ********* /////
            cell.fullName = ""
            
            let chatUID = "\(chat.uid)"
            let wasRevealed = KeychainWrapper.standard.bool(forKey: "wasRevealed_\(chatUID)") ?? false
            if !wasRevealed {
                cell.fullName = "A friend".localized()
            } else {
                
                if let chatMemberName = chatMember?.user?.fullName {
                    cell.fullName = chatMemberName
                } else {
                    cell.fullName = "A friend".localized()
                }
            }
            
            let chatLastMessage: ChatMessage? = Services.cacheViewContext.chatMessagesManager.first(withChatUID: chat.uid)
            //changeUsernameToSomethingElse(from: chatLastMessage, in: cell)
            
            ///// ********* Lance's Code ********* /////
        }

        let chatLastMessage: ChatMessage? = Services.cacheViewContext.chatMessagesManager.first(withChatUID: chat.uid)
        
        cell.lastMessage = nil
        
        switch chatLastMessage {
        case let chatLastTextMessage as ChatTextMessage:
            if chatLastMessage?.userUID == accountUserUID {
                if let text = chatLastTextMessage.text {
                    cell.lastMessage = "You: ".localized() + text
                    
                }
            } else {
                cell.lastMessage = chatLastTextMessage.text
            }
            
        case is ChatPhotoMessage:
            
            cell.lastMessage = Constants.lastPhotoMessage

        case let chatLastInfoMessage as ChatInfoMessage:
            switch chatLastInfoMessage.type {
            case .some(.match):
                cell.lastMessage = Constants.lastMatchMessage

            case .some(.oneReveal):
                cell.lastMessage = Constants.lastOneRevealMessage
                //self.lastMessageLabel.TextColor = Colors.orangeGuideView
                //lastMessageLabel.textColor
            case .some(.mutualReveal):
                cell.lastMessage = Constants.lastMutualRevealMessage
                //self.lastMessageLabel.TextColor = Colors.orangeGuideView

            case .some(.cancelReveal):
                cell.lastMessage = Constants.lastCancelRevealMessage

            case .none:
                cell.lastMessage = nil
            }

        default:
            cell.lastMessage = Constants.lastWelcomeMessage
        }

        if let createdDate = chatLastMessage?.createdDate {
            cell.lastMessageDate = ChatMessageDateFormatter.shared.string(from: createdDate)
        }

        cell.isViewed = chat.isViewed
    }
    
    func changeUsernameToSomethingElse(from chatMessage: ChatMessage?, in cell: ChatTableViewCell) {
        
        if let chatInfoMessage = chatMessage as? ChatInfoMessage {
            if chatInfoMessage.type == .some(.mutualReveal) || chatInfoMessage.type == .some(.oneReveal) {
                print("yes")
            } else {
                print("no")
            }
        }
        
        guard let chatMessage = chatMessage else {
            return
        }
        
        switch chatMessage {
        case let chatInfoMessage as ChatInfoMessage:
            switch chatInfoMessage.type {
            case .some(let type):
                switch type {
                    
                case .match:
                    print("1")
                    
                case .oneReveal:
                    cell.fullName = "A friend is ready to reveal"
                    //cell.full= Colors.orangeGuideView
                    
                case .mutualReveal:
                    cell.fullName = "Mutual reveal is ready"
                    cell.fullNameTextColor = Colors.orangeGuideView
                    
                case .cancelReveal:
                    print("4")
                }
            default:
                break
            }
        default:
            break
        }
    }
}

// MARK: - UITableViewDelegate

extension ChatTableViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ChatTableViewCell, cell.avatarImage == nil {
            guard let chat = self.chatList[indexPath.row].chat else {
                return
            }
            
            guard chat.members?.count == 2 else {
                return
            }
            
            let firstMessage = Services.cacheViewContext.chatPhotoMessagesManager.last(withChatUID: chat.uid)
            
            cell.avatarImage = nil
            
            if let firstMessage = firstMessage {
                if let imageData = firstMessage.smallPhotoData, let image = UIImage(data: imageData) {
                    cell.avatarImage = image
                } else {
                    self.loadAvatarImage(for: cell, with: firstMessage)
                }
            } else {
                cell.avatarImage = UIImage(named: "TutorialEmoji3")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let chat = self.chatList[indexPath.row].chat else {
            return
        }
        
        if chat.isGroupChat {
            self.performSegue(withIdentifier: Segues.showGroupChat, sender: chat)
        } else {
            self.performSegue(withIdentifier: Segues.showChat, sender: chat)
        }

        self.onChatOpened?()
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard let chat = self.chatList[indexPath.row].chat else {
            return
        }
        
        let loadingViewController = LoadingViewController()

        self.present(loadingViewController, animated: true, completion: {
            let firstMessage = Services.cacheViewContext.chatMessagesManager.last(withChatUID: chat.uid)
            
            if let firstMessage = firstMessage as? ChatPhotoMessage {
                firstMessage.smallPhotoData = nil
            }
            
            firstly {
                Services.chatsService.delete(chat: chat)
            }.done { [weak chat] in
                loadingViewController.dismiss(animated: true, completion: {
                    if let chat = chat {
                        if let chatIndex = self.chatList.firstIndex(where: { chatListItem in
                            return chatListItem.chat === chat
                        }) {
                            self.chatList.remove(at: chatIndex)
                            
                            self.tableView.beginUpdates()
                            
                            self.tableView.deleteRows(at: [IndexPath(row: chatIndex, section: 0)],
                                                      with: .fade)
                            
                            if self.chatList.isEmpty {
                                self.tableView.deleteSections(IndexSet(integersIn: 0..<self.tableView.numberOfSections), with: .fade)
                            }
                            
                            self.tableView.endUpdates()
                        }
                    }
                })
            }.catch { error in
                loadingViewController.dismiss(animated: true, completion: {
                    self.handle(actionError: error)
                })
            }
        })
    }
}

// MARK: - UITextFieldDelegate

extension ChatTableViewController: UITextFieldDelegate {
    
    // MARK: - Instance Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
}

// MARK: - KeyboardScrollableHandler

extension ChatTableViewController: KeyboardScrollableHandler {
    
    // MARK: - Instance Properties
    
    var scrollableView: UITableView {
        return self.tableView
    }
}

extension Array {
    
    func filterDuplicates(_ includeElement: @escaping (_ lhs: Element, _ rhs: Element) -> Bool) -> [Element] {
        var results = [Element]()
        
        forEach { (element) in
            
            let existingElements = results.filter {
                return includeElement(element, $0)
            }
            
            if existingElements.isEmpty {
                results.append(element)
            }
        }
        
        return results
    }
}
