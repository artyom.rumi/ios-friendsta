//
//  ChatMaskViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 21.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class ChatMaskViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let editChatMaskName = "EditChatMaskName"
        static let editChatReveal = "EditChatReveal"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: -
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let chatMemberTableCellIdentifier = "ChatMemberTableCell"
        
        static let editNameTableCellIdentifier = "EditNameTableCell"
        static let reportTableCellIdentifier = "ReportTableCell"
        static let blockTableCellIdentifier = "BlockTableCell"
        static let unblockTableCellIdentifier = "UnblockTableCell"
        
        static let tableSectionHeaderIdentifier = "TableSectionHeader"
        
        static let chatMembersTableSection = 0
        
        static let optionsTableSection = 1
        
        static let optionsTableSectionHeaderRow = 0
        static let editNameTableCellRow = 1
        static let reportTableCellRow = 2
        static let blockTableCellRow = 3
        
        static let tableSectionCount = 2
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    @IBOutlet fileprivate weak var tableHeaderView: UIView!
    
    @IBOutlet fileprivate weak var avatarImageView: UIImageView!
    @IBOutlet fileprivate weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet fileprivate weak var chatMaskNameLabel: UILabel!
    @IBOutlet fileprivate weak var chatMaskRevealLabel: UILabel!
    @IBOutlet fileprivate weak var theBigRevealLabel: UILabel!
    @IBOutlet fileprivate weak var explanationLabel: UILabel!
    
    @IBOutlet fileprivate weak var emptyStateContainerView: UIView!
    @IBOutlet fileprivate weak var emptyStateView: EmptyStateView!
    
    fileprivate weak var tableRefreshControl: UIRefreshControl!
    
    // MARK: -
    
    fileprivate var chatMemberTimer: Timer?
    
    // MARK: -
    
    fileprivate(set) var chatMember: ChatMember?
    fileprivate(set) var chatMembers: [ChatMember] = []
    
    fileprivate(set) var isRefreshingData = false
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromChatMemberEvents()
        self.unsubscribeFromChatMaskEvents()
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onChatMaskNameEditingFinished(segue: UIStoryboardSegue) {
        Log.high("onChatMaskNameEditingFinished(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onChatRevealEditingFinished(segue: UIStoryboardSegue) {
        Log.high("onChatRevealEditingFinished(\(String(describing: segue.identifier)))", from: self)
    }
    
    @objc fileprivate func onTableRefreshControlRequested(_ refreshControl: UIRefreshControl) {
        Log.high("onTableRefreshControlRequested()", from: self)
        
        self.refreshChatMask()
    }
    
    @objc fileprivate func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.refreshChatMask()
    }
    
    // MARK: -
    
    fileprivate func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()
        
        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action
        
        self.emptyStateContainerView.isHidden = false
    }
    
    fileprivate func hideEmptyState() {
        self.emptyStateContainerView.isHidden = true
    }
    
    fileprivate func showNoDataState() {
        self.showEmptyState(title: "Incognito user not found".localized(), message: "", action: nil)
    }
    
    fileprivate func showLoadingState() {
        if self.emptyStateContainerView.isHidden {
            self.showEmptyState(title: "Updating chat settings".localized(),
                                message: "We are updating your chat settings.\nPlease wait a bit.".localized())
        }
        
        self.emptyStateView.showActivityIndicator()
    }
    
    // MARK: -
    
    fileprivate func loadAvatarImage(with chatMember: ChatMember) {
        if let imageURL = chatMember.mask?.largeAvatarURL {
            self.avatarImageView.image = #imageLiteral(resourceName: "AvatarLargePlaceholder")
            
            self.activityIndicatorView.startAnimating()

            Services.imageLoader.loadImage(for: imageURL, in: self.avatarImageView, placeholder: #imageLiteral(resourceName: "AvatarLargePlaceholder"), completionHandler: { image in
                self.activityIndicatorView.stopAnimating()
            })
        } else {
            self.avatarImageView.image = #imageLiteral(resourceName: "ChatMaskPlaceholder")
        }
    }
    
    fileprivate func loadAvatarImage(for cell: ChatMemberTableViewCell, from imageURL: URL) {
        cell.avatarImage = #imageLiteral(resourceName: "AvatarSmallPlaceholder")

        Services.imageLoader.loadImage(for: imageURL, in: cell.avatarImageViewTarget, placeholder: #imageLiteral(resourceName: "AvatarSmallPlaceholder"))
    }
        
    fileprivate func loadAvatarImage(for cell: ChatMemberTableViewCell, with chatMask: ChatMask) {
        if let imageURL = chatMask.smallAvatarURL {
            self.loadAvatarImage(for: cell, from: imageURL)
        } else {
            cell.avatarImage = #imageLiteral(resourceName: "ChatMaskPlaceholder")
        }
    }
    
    fileprivate func loadAvatarImage(for cell: ChatMemberTableViewCell, with user: User) {
        if let imageURL = user.smallAvatarURL {
            self.loadAvatarImage(for: cell, from: imageURL)
        } else {
            cell.avatarImage = #imageLiteral(resourceName: "AvatarSmallPlaceholder")
        }
    }
    
    fileprivate func config(cell: ChatMemberTableViewCell, at indexPath: IndexPath) {
        guard let accountUserUID = Services.accountAccess?.userUID else {
            return
        }
        
        let chatMember = self.chatMembers[indexPath.row]
        
        let isDeanonymized: Bool
        
        if chatMember.isDeanonymized {
            isDeanonymized = !self.chatMembers.contains(where: { chatMember in
                return !chatMember.isDeanonymized
            })
        } else {
            isDeanonymized = false
        }
        
        cell.avatarImage = nil
        
        if let user = chatMember.user, isDeanonymized || (user.uid == accountUserUID) {
            cell.fullName = user.fullName
        } else if let chatMask = chatMember.chat?.chatMask {
            cell.fullName = chatMask.name
        } else {
            cell.fullName = "Unknown user".localized()
        }
        
        if chatMember.isDeanonymized {
            cell.revealStatus = "Ready".localized()
            cell.accessoryType = .none
        } else {
            cell.revealStatus = "Not Ready".localized()
            
            if let user = chatMember.user, user.uid == accountUserUID {
                cell.accessoryType = .disclosureIndicator
            } else {
                cell.accessoryType = .none
            }
        }
        
        if indexPath.row == self.chatMembers.count - 1 {
            cell.isLastRow = true
            cell.selectionStyle = .default
        } else {
            cell.selectionStyle = .none
        }
    }
    
    fileprivate func config(sectionHeaderView: ChatMaskTableSectionHeaderView, at section: Int) {
        switch section {
        case Constants.chatMembersTableSection:
            break
            
        case Constants.optionsTableSection:
            sectionHeaderView.title = "Options".localized()
            
        default:
            fatalError()
        }
    }
    
    fileprivate func layoutTableHeaderView() {
        let tableHeaderViewHeight = self.tableHeaderView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        
        self.tableHeaderView.frame = CGRect(x: 0.0,
                                            y: 0.0,
                                            width: self.tableView.bounds.width,
                                            height: tableHeaderViewHeight)
        
        self.tableView.tableHeaderView = self.tableHeaderView
    }
    
    // MARK: -
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: {
            retryHandler?()
        })
        
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        case .some(.connection), .some(.timeOut):
            self.showEmptyState(title: "No Internet Connection".localized(),
                                message: "Check your wi-fi or mobile data connection.".localized(),
                                action: action)
            
        default:
            self.showEmptyState(title: "Something went wrong".localized(),
                                message: "Please let us know what went wrong or try again later.".localized(),
                                action: action)
        }
    }
    
    fileprivate func reportChatMask() {
        Log.high("reportChatMask()", from: self)
        
        guard let user = self.chatMember?.user else {
            return
        }
        
        let alertController = UIAlertController(title: nil,
                                                message: nil,
                                                preferredStyle: .actionSheet)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel))
        
        alertController.addAction(UIAlertAction(title: "Report".localized(), style: .destructive, handler: { action in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: {
                firstly {
                    Services.usersService.report(user: user)
                }.done {
                    loadingViewController.dismiss(animated: true, completion: {
                        self.showMessage(withTitle: "Thank you!".localized(),
                                         message: "Your report will be reviewed by our team very soon.".localized())
                    })
                }.catch { error in
                    loadingViewController.dismiss(animated: true, completion: {
                        self.handle(actionError: error)
                    })
                }
            })
        }))
        
        self.present(alertController, animated: true)
    }
    
    fileprivate func blockChatMask() {
        Log.high("blockChatMask()", from: self)
        
        guard let chatMember = self.chatMember, let chatMask = chatMember.mask else {
            return
        }
        
        let alertController = UIAlertController(title: String(format: "Block the chat".localized()),
                                                message: "Blocked chat can't receive messages.".localized(),
                                                preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel))
        
        alertController.addAction(UIAlertAction(title: "Block".localized(), style: .destructive, handler: { action in
            let loadingViewController = LoadingViewController()
            
            self.present(loadingViewController, animated: true, completion: {
                firstly {
                    Services.chatMasksService.block(chatMask: chatMask)
                }.done {
                    loadingViewController.dismiss(animated: true, completion: {
                        if let chatMember = self.chatMember {
                            self.apply(chatMember: chatMember)
                        }
                    })
                }.catch { error in
                    loadingViewController.dismiss(animated: true, completion: {
                        if let chatMember = self.chatMember {
                            self.apply(chatMember: chatMember)
                        }
                        
                        self.handle(actionError: error)
                    })
                }
            })
        }))
        
        self.present(alertController, animated: true)
    }
    
    fileprivate func unblockChatMask() {
        Log.high("unblockChatMask()", from: self)
        
        guard let chatMask = self.chatMember?.mask else {
            return
        }
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            firstly {
                Services.chatMasksService.unblock(chatMask: chatMask)
            }.done {
                loadingViewController.dismiss(animated: true, completion: {
                    if let chatMember = self.chatMember {
                        self.apply(chatMember: chatMember)
                    }
                })
            }.catch { error in
                loadingViewController.dismiss(animated: true, completion: {
                    if let chatMember = self.chatMember {
                        self.apply(chatMember: chatMember)
                    }
                    
                    self.handle(actionError: error)
                })
            }
        })
    }
    
    fileprivate func refreshChatMask() {
        Log.high("refreshChatMask()", from: self)
        
        guard let chatMember = self.chatMember, let chat = chatMember.chat, let chatMask = chatMember.mask else {
            return
        }
    
        self.isRefreshingData = true
        
        if !self.tableRefreshControl.isRefreshing {
            if !self.emptyStateContainerView.isHidden {
                self.showLoadingState()
            }
        }
    
        firstly {
            after(seconds: 0.25)
        }.then {
            Services.chatsService.refresh(chat: chat)
        }.then { chat in
            Services.chatMasksService.refresh(chatMask: chatMask)
        }.ensure {
            if self.tableRefreshControl.isRefreshing {
                self.tableRefreshControl.endRefreshing()
            }
            
            self.isRefreshingData = false
        }.done { chatMask in
            self.apply(chatMember: chatMember)
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshChatMask()
            })
        }
    }
    
    fileprivate func resetChatMemberTimer() {
        self.chatMemberTimer?.invalidate()
        self.chatMemberTimer = nil
    }
    
    fileprivate func restartChatMemberTimer() {
        self.resetChatMemberTimer()
        
        guard !self.isRefreshingData else {
            return
        }
        
        self.chatMemberTimer = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false, block: { [weak self] timer in
            if let viewController = self, let chatMember = viewController.chatMember {
                viewController.apply(chatMember: chatMember)
            }
        })
    }
    
    fileprivate func restartChatMemberTimer(with chatMembers: [ChatMember]) {
        guard let chatMember = self.chatMember, chatMembers.contains(object: chatMember) else {
            return
        }
        
        self.restartChatMemberTimer()
    }

    fileprivate func restartChatMemberTimer(with chatMasks: [ChatMask]) {
        guard let chatMask = self.chatMember?.mask, chatMasks.contains(object: chatMask) else {
            return
        }
        
        self.restartChatMemberTimer()
    }

    fileprivate func subscribeToChatMemberEvents() {
        self.unsubscribeFromChatMemberEvents()
        
        let chatMembersManager = Services.cacheViewContext.chatMembersManager
        
        chatMembersManager.objectsChangedEvent.connect(self, handler: { [weak self] chatMembers in
            self?.restartChatMemberTimer(with: chatMembers)
        })
        
        chatMembersManager.startObserving()
    }
    
    fileprivate func unsubscribeFromChatMemberEvents() {
        Services.cacheViewContext.chatMembersManager.objectsChangedEvent.disconnect(self)
    }
    
    fileprivate func subscribeToChatMaskEvents() {
        self.unsubscribeFromChatMaskEvents()
        
        let chatMasksManager = Services.cacheViewContext.chatMasksManager
        
        chatMasksManager.objectsChangedEvent.connect(self, handler: { [weak self] chatMasks in
            self?.restartChatMemberTimer(with: chatMasks)
        })
        
        chatMasksManager.startObserving()
    }
    
    fileprivate func unsubscribeFromChatMaskEvents() {
        Services.cacheViewContext.chatMasksManager.objectsChangedEvent.disconnect(self)
    }
    
    // MARK: -
    
    func apply(chatMember: ChatMember) {
        Log.high("apply(chatMember: \(String(describing: chatMember.user?.fullName)))", from: self)
        
        self.chatMember = chatMember
        
        if let chatMembers = chatMember.chat?.members?.allObjects as? [ChatMember] {
            if let accountUserUID = Services.accountAccess?.userUID {
                self.chatMembers = chatMembers.sorted(by: { first, second in
                    return (first.user?.uid != accountUserUID)
                })
            } else {
                self.chatMembers = chatMembers
            }
        } else {
            self.chatMembers = []
        }
        
        if self.isViewLoaded {
            self.loadAvatarImage(with: chatMember)
            
            self.chatMaskNameLabel.text = chatMember.chat?.chatMask?.name
            
            if chatMember.isDeanonymized {
                self.chatMaskRevealLabel.text = "Ready to reveal".localized()
            } else {
                self.chatMaskRevealLabel.text = "Not ready to reveal".localized()
            }
            
            self.hideEmptyState()
            
            if self.isViewAppeared {
                self.tableView.beginUpdates()
                self.tableView.deleteSections(IndexSet(integersIn: 0..<self.tableView.numberOfSections), with: .fade)
                self.tableView.insertSections(IndexSet(integersIn: 0..<Constants.tableSectionCount), with: .fade)
                self.tableView.endUpdates()
            } else {
                self.tableView.reloadData()
            }
            
            self.layoutTableHeaderView()
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
        
        self.resetChatMemberTimer()
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.chatMaskNameLabel.font = Fonts.heavy(ofSize: 22.0)
        self.chatMaskRevealLabel.font = Fonts.regular(ofSize: 15.0)
        self.theBigRevealLabel.font = Fonts.bold(ofSize: 17.0)
        self.explanationLabel.font = Fonts.regular(ofSize: 17.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        let tableRefreshControl = UIRefreshControl()
        
        tableRefreshControl.addTarget(self,
                                      action: #selector(self.onTableRefreshControlRequested(_:)),
                                      for: .valueChanged)
        
        self.tableView.refreshControl = tableRefreshControl
        self.tableRefreshControl = tableRefreshControl
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isRefreshingData = false
        
        if let chatMember = self.chatMember {
            if !self.hasAppliedData {
                self.apply(chatMember: chatMember)
            }
            
            self.showLoadingState()
            
            self.refreshChatMask()
        } else {
            self.showNoDataState()
        }
        
        self.subscribeToChatMemberEvents()
        self.subscribeToChatMaskEvents()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onApplicationWillEnterForeground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unsubscribeFromChatMemberEvents()
        self.unsubscribeFromChatMaskEvents()
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
        self.hasAppliedData = false
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.layoutTableHeaderView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {
        case Segues.editChatMaskName:
            guard let chatMask = sender as? ChatMask else {
                fatalError()
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["chatMask": chatMask])
            }
            
        case Segues.editChatReveal:
            guard let chat = sender as? Chat else {
                fatalError()
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["chat": chat])
            }
            
        default:
            break
        }
    }
}

// MARK: - UITableViewDataSource

extension ChatMaskViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return (self.chatMember == nil) ? 0 : Constants.tableSectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Constants.chatMembersTableSection:
            return self.chatMembers.count
            
        case Constants.optionsTableSection:
            return 4
            
        default:
            fatalError()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch indexPath.section {
        case Constants.chatMembersTableSection:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.chatMemberTableCellIdentifier, for: indexPath)
            
            self.config(cell: cell as! ChatMemberTableViewCell, at: indexPath)
            
        case Constants.optionsTableSection:
            switch indexPath.row {
            case Constants.optionsTableSectionHeaderRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableSectionHeaderIdentifier, for: indexPath)
                
                self.config(sectionHeaderView: cell as! ChatMaskTableSectionHeaderView, at: indexPath.section)
                
            case Constants.editNameTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.editNameTableCellIdentifier, for: indexPath)
                
            case Constants.reportTableCellRow:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.reportTableCellIdentifier, for: indexPath)
                
            case Constants.blockTableCellRow:
                if let chatMask = self.chatMember?.mask, chatMask.isBlocked {
                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.unblockTableCellIdentifier, for: indexPath)
                } else {
                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.blockTableCellIdentifier, for: indexPath)
                }
                
            default:
                fatalError()
            }
            
        default:
            fatalError()
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension ChatMaskViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case Constants.chatMembersTableSection:
            guard let accountUserUID = Services.accountAccess?.userUID else {
                return
            }
            
            let chatMember = self.chatMembers[indexPath.row]
            
            let isDeanonymized: Bool
            
            if chatMember.isDeanonymized {
                isDeanonymized = !self.chatMembers.contains(where: { chatMember in
                    return !chatMember.isDeanonymized
                })
            } else {
                isDeanonymized = false
            }
            
            let cell = cell as! ChatMemberTableViewCell
            
            if let user = chatMember.user, isDeanonymized || (user.uid == accountUserUID) {
                self.loadAvatarImage(for: cell, with: user)
            } else if let chatMask = chatMember.mask {
                self.loadAvatarImage(for: cell, with: chatMask)
            } else {
                cell.avatarImage = #imageLiteral(resourceName: "AvatarSmallPlaceholder")
            }
            
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let chatMember = self.chatMember else {
            fatalError()
        }
        
        switch indexPath.section {
        case Constants.chatMembersTableSection:
            if indexPath.row == self.chatMembers.count - 1 {
                let lastChatMember = self.chatMembers[indexPath.row]
                
                if let chat = lastChatMember.chat, !lastChatMember.isDeanonymized {
                    self.performSegue(withIdentifier: Segues.editChatReveal, sender: chat)
                }
            }
            
        case Constants.optionsTableSection:
            switch indexPath.row {
            case Constants.editNameTableCellRow:
                if let chatMask = self.chatMember?.chat?.chatMask {
                    self.performSegue(withIdentifier: Segues.editChatMaskName, sender: chatMask)
                }
                
            case Constants.reportTableCellRow:
                self.reportChatMask()
                
            case Constants.blockTableCellRow:
                if chatMember.mask?.isBlocked ?? false {
                    self.unblockChatMask()
                } else {
                    self.blockChatMask()
                }
                
            default:
                break
            }
            
        default:
            fatalError()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case Constants.optionsTableSection:
            switch indexPath.row {
            case Constants.optionsTableSectionHeaderRow:
                return 56.0
                
            default:
                break
            }
            
        default:
            break
        }
        
        return 44.0
    }
}

// MARK: - DictionaryReceiver

extension ChatMaskViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let chatMember = dictionary["chatMember"] as? ChatMember else {
            return
        }
        
        self.apply(chatMember: chatMember)
    }
}
