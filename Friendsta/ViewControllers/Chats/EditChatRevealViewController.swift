//
//  EditChatRevealViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class EditChatRevealViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let finishChatRevealEditing = "FinishChatRevealEditing"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: -
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let notReadyTableCellRowIdentifier = "NotReadyTableCell"
        static let readyTableCellRowIdentifier = "ReadyTableCell"
        
        static let notReadyTableCellRow = 0
        static let readyTableCellRow = 1
    }
    
    // MARK: - Instance Propeties
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    // MARK: -
    
    fileprivate(set) var chat: Chat?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods

    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func deanonymize() {
        Log.high("deanonymize()", from: self)
        
        guard let chat = self.chat else {
            return
        }
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            firstly {
                Services.chatsService.deanonymize(for: chat)
            }.done {
                loadingViewController.dismiss(animated: true, completion: {
                    self.performSegue(withIdentifier: Segues.finishChatRevealEditing, sender: self)
                })
            }.catch { error in
                loadingViewController.dismiss(animated: true, completion: {
                    self.handle(actionError: error, okHandler: {
                        self.performSegue(withIdentifier: Segues.finishChatRevealEditing, sender: self)
                    })
                })
            }
        })
    }
    
    // MARK: -
    
    func apply(chat: Chat) {
        Log.high("apply(chat: \(chat.uid))", from: self)
        
        self.chat = chat
        
        if self.isViewLoaded {
            self.tableView.reloadData()
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let chat = self.chat, !self.hasAppliedData {
            self.apply(chat: chat)
        }
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
}

// MARK: - UITableViewDataSource

extension EditChatRevealViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch indexPath.row {
        case Constants.notReadyTableCellRow:
            return tableView.dequeueReusableCell(withIdentifier: Constants.notReadyTableCellRowIdentifier, for: indexPath)
            
        case Constants.readyTableCellRow:
            return tableView.dequeueReusableCell(withIdentifier: Constants.readyTableCellRowIdentifier, for: indexPath)
            
        default:
            fatalError()
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension EditChatRevealViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch indexPath.row {
        case Constants.notReadyTableCellRow:
            tableView.cellForRow(at: IndexPath(row: Constants.readyTableCellRow, section: 0))?.accessoryType = .none
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            
            self.performSegue(withIdentifier: Segues.finishChatRevealEditing, sender: self)
            
        case Constants.readyTableCellRow:
            tableView.cellForRow(at: IndexPath(row: Constants.notReadyTableCellRow, section: 0))?.accessoryType = .none
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            
            self.deanonymize()
            
        default:
            fatalError()
        }
    }
}

// MARK: - DictionaryReceiver

extension EditChatRevealViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let chat = dictionary["chat"] as? Chat else {
            return
        }
        
        self.apply(chat: chat)
    }
}
