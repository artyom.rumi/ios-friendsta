//
//  EditChatMaskNameViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 02.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class EditChatMaskNameViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let finishChatMaskNameEditing = "FinishChatMaskNameEditing"
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var messageLabel: UILabel!
    @IBOutlet fileprivate weak var nameTextField: UITextField!
    @IBOutlet fileprivate weak var saveButton: PrimaryButton!
    
    @IBOutlet fileprivate weak var bottomSpacerViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: -
    
    fileprivate(set) var chatMask: ChatMask?
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onSaveButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onSaveButtonTouchUpInside()", from: self)
    
        self.view.endEditing(true)
        
        self.updateChatMask()
    }
    
    // MARK: -
    
    fileprivate func showAlert(title: String?, message: String?, handler: (() -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.view.tintColor = Colors.primary
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .cancel, handler: { action in
            handler?()
        }))
        
        self.present(alertController, animated: true)
    }
    
    fileprivate func showMessage(withError error: Error, handler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.connection), .some(.timeOut):
            self.showAlert(title: "No Internet Connection".localized(),
                           message: "Check your wi-fi or mobile data connection.".localized(),
                           handler: handler)
            
        default:
            self.showAlert(title: "Something went wrong".localized(),
                           message: "Please let us know what went wrong or try again later.".localized(),
                           handler: handler)
        }
    }
    
    // MARK: -
    
    fileprivate func updateTextField(with text: String?) {
        self.nameTextField.text = text
        
        self.saveButton.isEnabled = !(text?.isEmpty ?? true)
    }
    
    // MARK: -
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func updateChatMask() {
        Log.high("updateChatMask()", from: self)
        
        guard let chatMask = self.chatMask else {
            return
        }
        
        chatMask.name = self.nameTextField.text
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            firstly {
                Services.chatMasksService.update(chatMask: chatMask)
            }.done { chatMask in
                loadingViewController.dismiss(animated: true, completion: {
                    self.performSegue(withIdentifier: Segues.finishChatMaskNameEditing, sender: nil)
                })
            }.catch { error in
                loadingViewController.dismiss(animated: true, completion: {
                    self.handle(actionError: error)
                })
            }
        })
    }
    
    // MARK: -
    
    func apply(chatMask: ChatMask) {
        Log.high("apply(chatMask: \(String(describing: chatMask.name)))", from: self)
        
        self.chatMask = chatMask
        
        if self.isViewLoaded {
            self.nameTextField.placeholder = chatMask.name ?? "Name".localized()
            
            self.updateTextField(with: nil)
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: -
    
    fileprivate func setupFont() {
        self.titleLabel.font = Fonts.heavy(ofSize: 22.0)
        self.messageLabel.font = Fonts.regular(ofSize: 17.0)
        self.nameTextField.font = Fonts.regular(ofSize: 22.0)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupFont()
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let chatMask = self.chatMask, !self.hasAppliedData {
            self.apply(chatMask: chatMask)
        }
        
        self.subscribeToKeyboardNotifications()
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.nameTextField.becomeFirstResponder()
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.view.endEditing(true)
        
        self.unsubscribeFromKeyboardNotifications()
    }
}

// MARK: - UITextFieldDelegate

extension EditChatMaskNameViewController: UITextFieldDelegate {
    
    // MARK: - Instance Methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.updateChatMask()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let replacedText: String
        
        if let text = self.nameTextField.text {
            replacedText = text.replacingCharacters(in: Range(range, in: text)!, with: string)
        } else {
            replacedText = string
        }
        
        let startPosition = textField.beginningOfDocument
        let cursorOffset = range.location + string.count
        
        self.updateTextField(with: replacedText)
        
        if let cursorPosition = textField.position(from: startPosition, offset: cursorOffset) {
            textField.selectedTextRange = textField.textRange(from: cursorPosition, to: cursorPosition)
        }
        
        return false
    }
}

// MARK: - DictionaryReceiver

extension EditChatMaskNameViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let chatMask = dictionary["chatMask"] as? ChatMask else {
            return
        }
        
        self.apply(chatMask: chatMask)
    }
}

// MARK: - KeyboardHandler

extension EditChatMaskNameViewController: KeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        self.bottomSpacerViewHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
}
