//
//  SendChatPhotoViewController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 24/10/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class SendChatPhotoViewController: MakePhotoViewController {
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let unauthorize = "Unauthorize"
        static let finishPhotoSending = "FinishPhotoSending"
    }
    
    // MARK: - Instance Properties
    
    fileprivate(set) var chat: Chat?
    
    private var feed: Feed?
    private var comment: Comment?
    
    private var isAnonymous: Bool = true
    
    fileprivate(set) var hasAppliedData = false
    
    // MARK: - Instance Methods
    
    fileprivate func handle(silentError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            break
        }
    }
    
    private func send(photoData: Data, isAnonymous: Bool) {
        if let chat = self.chat {
            firstly {
                Services.chatMessagesService.send(photoData: photoData, isAnonymous: isAnonymous, to: chat)
            }.done { chatPhotoMessage in
                self.performSegue(withIdentifier: Segues.finishPhotoSending, sender: self)
            }.catch { error in
                self.handle(silentError: error)
            }
        } else {
            if let feed = self.feed {
                firstly {
                    Services.chatsService.createChat(for: feed)
                }.done { chat in
                    self.feed?.chat = chat
                    self.apply(chat: chat)
                    self.send(photoData: photoData, isAnonymous: isAnonymous)
                }.catch { error in
                    self.handle(silentError: error)
                }
            }

            if let comment = self.comment {
                firstly {
                    Services.chatsService.createChat(for: comment)
                }.done { chat in
                    self.feed?.chat = chat
                    self.apply(chat: chat)
                    self.send(photoData: photoData, isAnonymous: isAnonymous)
                }.catch { error in
                    self.handle(silentError: error)
                }
            }
        }
    }
    
    private func apply(feed: Feed) {
        self.feed = feed
    }
    
    private func apply(comment: Comment) {
        self.comment = comment
    }
    
    private func apply(isAnonymous: Bool) {
        self.isAnonymous = isAnonymous
    }
    
    // MARK: -
    
    func apply(chat: Chat) {
        Log.high("apply(chat: \(String(describing: chat.uid)))", from: self)
        
        self.chat = chat
        
        if self.isViewLoaded {
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - MakePhotoViewController
    
    override func performAction(with photoImage: UIImage) {
        Log.high("performAction(withPhotoImage: \(photoImage.size))", from: self)
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true, completion: {
            DispatchQueue.global(qos: .userInitiated).async(execute: {
                let photoData: Data?
                
                if photoImage.size.width > Limits.photoImageMaxWidth {
                    if let photoImage = photoImage.scaled(to: Limits.photoImageMaxWidth) {
                        photoData = photoImage.jpegData(compressionQuality: Limits.photoImageQuality)
                    } else {
                        photoData = photoImage.jpegData(compressionQuality: Limits.photoImageQuality)
                    }
                } else {
                    photoData = photoImage.jpegData(compressionQuality: Limits.photoImageQuality)
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.25, execute: {
                    loadingViewController.dismiss(animated: true, completion: {
                        guard let photoData = photoData else {
                            return
                        }
                        
                        self.send(photoData: photoData, isAnonymous: self.isAnonymous)
                    })
                })
            })
        })
    }
    
    override func apply(dictionary: [String: Any]) {
        super.apply(dictionary: dictionary)
        
        if let feed = dictionary["feed"] as? Feed {
            self.apply(feed: feed)
        }
        
        if let comment = dictionary["comment"] as? Comment {
            self.apply(comment: comment)
        }
        
        if let isAnonymous = dictionary["isAnonymousMessage"] as? Bool {
            self.apply(isAnonymous: isAnonymous)
        }
    
        guard let chat = dictionary["chat"] as? Chat else {
            return
        }
        
        self.apply(chat: chat)
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let chat = self.chat, !self.hasAppliedData {
            self.apply(chat: chat)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
}
