//
//  ChatPhotoViewController.swift
//  Friendsta
//
//  Created by Elina Batyrova on 25.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class ChatPhotoViewController: LoggedViewController {
    
    // MARK: - Typealiases
    
    typealias ClosePhotoType = ((_ chatPhotoMessage: ChatPhotoMessage?) -> Void)
    
    // MARK: - Nested Types
    
    fileprivate enum Segues {
        
        // MARK: - Type Properties
        
        static let unauthorize = "Unauthorize"
    }
    
    // MARK: - Instance Properties

    @IBOutlet fileprivate weak var scrollView: UIScrollView!
    @IBOutlet fileprivate weak var imageView: UIImageView!
    
    @IBOutlet fileprivate weak var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: -
    
    fileprivate var touchStartPoint: CGPoint = CGPoint(x: 0, y: 0)
    
    // MARK: -
    
    fileprivate(set) var chatPhotoMessage: ChatPhotoMessage?
    
    fileprivate(set) var hasAppliedData = false
    
    var onPhotoClosed: ClosePhotoType?
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onViewTapped(_ sender: Any) {
        Log.high("onViewTapped()", from: self)
        
        self.dismiss(animated: true)
        self.onPhotoClosed?(self.chatPhotoMessage)
    }
    
    @IBAction fileprivate func onPanGestureRecognized(_ sender: UIPanGestureRecognizer) {
        Log.high("onPanGestureRecognized()", from: self)
        
        let touchPoint = sender.location(in: self.view?.window)
        
        switch sender.state {
        case .began:
            self.touchStartPoint = touchPoint
            
        case .changed:
            self.view.frame = CGRect(x: 0.0,
                                     y: touchPoint.y - self.touchStartPoint.y,
                                     width: self.view.frame.size.width,
                                     height: self.view.frame.size.height)
            
        case .ended, .cancelled:
            if (touchPoint.y - self.touchStartPoint.y > 100.0) || (touchPoint.y - self.touchStartPoint.y < -100.0) {
                self.dismiss(animated: true)
                self.onPhotoClosed?(self.chatPhotoMessage)
            } else {
                UIView.animate(withDuration: 0.25, animations: {
                    self.view.frame = CGRect(x: 0.0,
                                             y: 0.0,
                                             width: self.view.frame.size.width,
                                             height: self.view.frame.size.height)
                })
            }
            
        default:
            break
        }
    }
    
    // MARK: -
    
    fileprivate func handle(silentError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            break
        }
    }
    
    // MARK: -
    
    func apply(chatPhotoMessage: ChatPhotoMessage, onPhotoClosed: ((_ chatPhotoMessage: ChatPhotoMessage?) -> Void)?) {
        Log.high("apply(chatPhotoMessage: \(String(describing: chatPhotoMessage.photoURL)))", from: self)
        
        self.chatPhotoMessage = chatPhotoMessage
        self.onPhotoClosed = onPhotoClosed
        
        if self.isViewLoaded {
            if let imageURL = chatPhotoMessage.photoURL {
                self.activityIndicatorView.startAnimating()

                Services.imageLoader.loadImage(for: imageURL, in: self.imageView, completionHandler: { image in
                    if image != nil {
                        self.activityIndicatorView.stopAnimating()
                    }
                })
            } else {
                self.imageView.image = nil
            }
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let chatPhotoMessage = self.chatPhotoMessage, !self.hasAppliedData {
            self.apply(chatPhotoMessage: chatPhotoMessage, onPhotoClosed: self.onPhotoClosed)
        }

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
}

// MARK: - UIScrollViewDelegate

extension ChatPhotoViewController: UIScrollViewDelegate {
    
    // MARK: - Instance Methods
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        if scale < 1.0 {
            self.scrollView.setZoomScale(1.0, animated: true)
        } else if scale > 5.0 {
            self.scrollView.setZoomScale(5.0, animated: true)
        }
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        let offsetX = max((self.scrollView.bounds.size.width - self.scrollView.contentSize.width) * 0.5, 0.0)
        let offsetY = max((self.scrollView.bounds.size.height - self.scrollView.contentSize.height) * 0.5, 0.0)
        
        self.imageView.center = CGPoint(x: self.scrollView.contentSize.width * 0.5 + offsetX,
                                        y: self.scrollView.contentSize.height * 0.5 + offsetY)
    }
}

// MARK: - DictionaryReceiver

extension ChatPhotoViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let chatPhotoMessage = dictionary["chatPhotoMessage"] as? ChatPhotoMessage else {
            return
        }
        
        let onPhotoClosed = dictionary["onPhotoClosed"] as? ClosePhotoType
        
        self.apply(chatPhotoMessage: chatPhotoMessage, onPhotoClosed: onPhotoClosed)
    }
}
