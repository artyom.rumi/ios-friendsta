//
//  ChatMessageTableViewController.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 18/05/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.

// swiftlint:disable file_length
// swiftlint:disable type_body_length
// swiftlint:disable function_body_length
// swiftlint:disable vertical_whitespace
// swiftlint:disable multiple_closures_with_trailing_closure

// swiftlint:disable discouraged_object_literal
// swiftlint:disable  closure_parameter_position

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork
import SwiftKeychainWrapper

////////////////>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<
extension ChatMessageTableViewController: IncomingCommentMessageWithAvatarTableViewCellDelegate {
    
    func retrieveUserIdFromCommentMessageAvatarTableViewCell(userId: Int64) {
        
        presentUser(with: userId)
    }
}

extension ChatMessageTableViewController: IncomingPhotoMessageWithAvatarTableViewCellDelegate {
    
    func retrieveUserIdFromPhotoMessageAvatarTableViewCell(userId: Int64) {
        
        presentUser(with: userId)
    }
}

extension ChatMessageTableViewController: IncomingTextMessageWithAvatarTableViewCellDelegate {
    
    func retrieveUserIdFromTextMessageAvatarTableViewCell(userId: Int64) {
        
        presentUser(with: userId)
    }
}

extension ChatMessageTableViewController {
    
    private func presentUser(with userID: Int64) {
        guard let userViewController = UIStoryboard(name: "User", bundle: Bundle.main).instantiateInitialViewController() as? UserViewController else {
            fatalError("Can't init UserViewController")
        }

        userViewController.apply(userID: userID)
        
        
        if #available(iOS 13.0, *) {
            
            present(userViewController, animated: true, completion: nil)
        } else {
            
            userViewController.showCancelButton = true
            
            let navVC = UINavigationController(rootViewController: userViewController)
            present(navVC, animated: true, completion: nil)
        }
    }
}

class ChatMessageTableViewController: LoggedViewController, ErrorMessagePresenter {
    
    ///// ********* Lance's Code ********* /////
    
    ///////////////////////////////////////////////////////////////////////////////////////////-------------------------------------------------------------///////////////////////////////////////////////////////////////////////////////////////////
    fileprivate func configureNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(removeRevealButtonTimerLabelCheckBackInLabelSetKeychainWrapperChatSessionIdToNil),
                                               name: NSNotification.Name(rawValue: "removeRevealButtonInChatMessagesTVC"),
                                               object: nil)
    }
    
    // revealButton
    fileprivate lazy var revealButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Reveal Now", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = Colors.orangeGuideView
        button.addTarget(self, action: #selector(revealButtonPressed), for: .touchUpInside)
        
        //button.titleLabel?.textAlignment = .center
        button.layer.cornerRadius = 16
        button.layer.masksToBounds = true
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        
        //button.backgroundColor = .lightGray
        //button.isUserInteractionEnabled = false
        
        return button
    }()
    
    @objc fileprivate func revealButtonPressed() {
        
        let storyboard = UIStoryboard(name: "OutlineContacts", bundle: nil)
        let outlineContactsTVC = storyboard.instantiateViewController(withIdentifier: "OutlineContactsTableViewController") as! OutlineContactsTableViewController
        
        outlineContactsTVC.isComingFromChatTVC = true
        outlineContactsTVC.chatSessionId = chatSessionId
        outlineContactsTVC.timerHrs = timerHrs
        outlineContactsTVC.timerMins = timerMins
        outlineContactsTVC.timerSecs = timerSecs
        outlineContactsTVC.revealTimerText = revealTimerLabel.text
        
        if #available(iOS 13.0, *) {
            
            present(outlineContactsTVC, animated: true, completion: nil)
        } else {
            
            let navVC = UINavigationController(rootViewController: outlineContactsTVC)
            present(navVC, animated: true, completion: nil)
        }
        
        /*
        let storyboard = UIStoryboard(name: "Community", bundle: nil)
        let CommunityTVC = storyboard.instantiateViewController(withIdentifier: "CommunityTableViewController") as! CommunityTableViewController
        present(CommunityTVC, animated: true, completion: nil)
        */
    }
    
    fileprivate func disableUIElements() {
        revealButton.backgroundColor = .lightGray
        revealButton.isUserInteractionEnabled = false
    }
    
    fileprivate func enableUIElements() {
        revealButton.backgroundColor = Colors.orangeGuideView
        revealButton.isUserInteractionEnabled = true
    }
    
    fileprivate func showRevealButton() {
        // https://stackoverflow.com/questions/35284913/swift-countdown-timer-displays-days-hours-seconds-remaining
        // https://stackoverflow.com/questions/28571194/using-nsuserdefaults-to-add-a-24hour-countdown-timer-in-swift
        
        revealView.alpha = 0
        
        view.addSubview(revealButton)
        revealButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
        revealButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        revealButton.heightAnchor.constraint(equalToConstant: 32).isActive = true
    }
    
    fileprivate var didRevealAnimationRun  = false
    @objc fileprivate func removeRevealButtonTimerLabelCheckBackInLabelSetKeychainWrapperChatSessionIdToNil() {
        
        didRevealAnimationRun = true
        didMutualRevealHappen = true
        
        DispatchQueue.main.async { [weak self] in
            self?.revealButton.removeFromSuperview()
            self?.revealTimerLabel.removeFromSuperview()
            self?.staticCheckBackInLabel.removeFromSuperview()
        }
        
        if let chatSessionId = chatSessionId {
            KeychainWrapper.standard.removeObject(forKey: chatSessionId)
            
            KeychainWrapper.standard.set(true, forKey: "wasRevealed_\(chatSessionId)")
            
            //hideRevealView()
            
            DispatchQueue.main.async { [weak self] in
                
                guard let safeSelf = self else { return }
                
                safeSelf.didRemoveRevealButtonTimerFunctionFireWhileViewIsOnScreen = true
                
                if safeSelf.wasInvitedOrIsAlreadyAFriend {
                    
                    UIView.animate(withDuration: 0.33, animations: {
                        self?.revealView.alpha = 1
                        self?.revealView.set(state: .revealByBoth)
                    }) { (_) in
                        self?.tableView.reloadData()
                    }
                    
                } else {
                    
                    UIView.animate(withDuration: 0.33, animations: {
                        self?.revealView.alpha = 1
                        self?.revealView.set(state: .revealByBothAndShowAddFriend)
                    }) { (_) in
                        self?.tableView.reloadData()
                    }
                }
            }
            
            /*
            if safeSelf.wasInvitedOrIsAlreadyAFriend {
                
                self?.revealView.set(state: .revealByBothAnimation, completion: {
                    
                    UIView.animate(withDuration: 0.33, animations: {
                        self?.revealView.alpha = 1
                    }) { (_) in
                        self?.tableView.reloadData()
                    }
                })
            } else {
                
                self?.revealView.set(state: .revealByBothAndShowAddFriendAnimation, completion: {
                    
                    UIView.animate(withDuration: 0.33, animations: {
                        self?.revealView.alpha = 1
                    }) { (_) in
                        self?.tableView.reloadData()
                    }
                })
            }
            */
        }
    }
    
    // timerLabel + staticCheckBackInLabel
    fileprivate lazy var staticCheckBackInLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Check back in"
        label.font = UIFont.systemFont(ofSize: 17)
        label.textColor = UIColor.gray
        label.textAlignment = .left
        return label
    }()
    
    fileprivate lazy var revealTimerLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.monospacedDigitSystemFont(ofSize: 21, weight: UIFont.Weight.regular)
        label.textAlignment = .left
        return label
    }()
    
    fileprivate func addTimerLabelAndCheckBackInLabelAnchors() {
        
        view.addSubview(staticCheckBackInLabel)
        view.addSubview(revealTimerLabel)
        
        staticCheckBackInLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
        staticCheckBackInLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        
        revealTimerLabel.topAnchor.constraint(equalTo: staticCheckBackInLabel.bottomAnchor, constant: 0).isActive = true
        revealTimerLabel.leadingAnchor.constraint(equalTo: staticCheckBackInLabel.leadingAnchor, constant: 0).isActive = true
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////-------------------------------------------------------------///////////////////////////////////////////////////////////////////////////////////////////
    fileprivate var chatSessionId: String?
    fileprivate var revealTimer: Timer?
    fileprivate var timerHrs: Int?
    fileprivate var timerMins: Int?
    fileprivate var timerSecs: Int?
    
    fileprivate func configureRevealTimer(chatSessionId: String) {
        //print(chatSessionId)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let currentDate: TimeInterval = Date().timeIntervalSince1970
        let startDateAsDate: Date = Date(timeIntervalSince1970: currentDate)
        let revealDateAsDate: Date? = Calendar.current.date(byAdding: .hour, value: +10, to: startDateAsDate)
        
        let startDateFormatted = dateFormatter.string(from: startDateAsDate)
        //print("startDate: \(startDateFormatted)")
        
        guard let safeRevealDateAsDate = revealDateAsDate else { return }
        let revealDateFormattedAsStr = dateFormatter.string(from: safeRevealDateAsDate)
        //print("revealDate: \(revealDateFormattedAsStr)")
        
        let revealDate = safeRevealDateAsDate.timeIntervalSince1970
        //print(chatSessionId)
        KeychainWrapper.standard.set(revealDate, forKey: chatSessionId)
        //let chatSessionId = KeychainWrapper.standard.double(forKey: chatSessionId)
        
        self.chatSessionId = chatSessionId
        
        startRevealTimer()
    }
    
    fileprivate func startRevealTimer() {
        revealTimer?.invalidate()
        revealTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDownDate), userInfo: nil, repeats: true)
        if let revealTimer = revealTimer {
            RunLoop.current.add(revealTimer, forMode: RunLoop.Mode.common)
        }
    }
    
    fileprivate func invalidateRevealTimer() {
        revealTimer?.invalidate()
        revealTimer = nil
    }
    
    @objc fileprivate func countDownDate() {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        guard let chatSessionId = chatSessionId,
            let revealDateAsDouble = KeychainWrapper.standard.double(forKey: chatSessionId) else {
            invalidateRevealTimer()
            return
        }
        
        if !revealButton.isUserInteractionEnabled {
            enableUIElements()
        }
        
        let revealDate: Date = Date(timeIntervalSince1970: revealDateAsDouble)
        
        let currentDate = Date()
        let calendar = Calendar.current
        let diffDateComponents = calendar.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: revealDate)
        
        let countdown = "Days \(diffDateComponents.day),  Hours: \(diffDateComponents.hour), Minutes: \(diffDateComponents.minute), Seconds: \(diffDateComponents.second)"
        //print(countdown)
        
        timerHrs = diffDateComponents.hour
        timerMins = diffDateComponents.minute
        timerSecs = diffDateComponents.second
        
        if let hours = diffDateComponents.hour, let minutes = diffDateComponents.minute, let seconds = diffDateComponents.second {
            
            let hrStr = String(hours)
            let minStr = String(minutes)
            let secStr = String(seconds)
            
            let hoursStr = hours > 9 ? "\(hrStr)" : "0\(hrStr)"
            let minutesStr = minutes > 9 ? "\(minStr)" : "0\(minStr)"
            let secondsStr = seconds > 9 ? "\(secStr)" : "0\(secStr)"
            
            revealTimerLabel.text = "\(hoursStr):\(minutesStr):\(secondsStr)"
        }
        
        if currentDate >= revealDate {
            
            invalidateRevealTimer()
            
            if !didRevealAnimationRun {
                removeRevealButtonTimerLabelCheckBackInLabelSetKeychainWrapperChatSessionIdToNil()
            }
            
            /*
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) { [weak self] in
                self?.removeRevealButtonTimerLabelCheckBackInLabelSetKeychainWrapperChatSessionIdToNil()
            }
            */
        } else {
            
            //print("reveal time hasn't happened yet")
        }
    }
    
    private var didStartRevealTimer = false {
        didSet {
            
            guard didStartRevealTimer else { return }
            guard let chatId = self.chat?.uid else { return }
            
            let chatUID = "\(chatId)"
            
            chatSessionId = chatUID
            
            let wasRevealed = KeychainWrapper.standard.bool(forKey: "wasRevealed_\(chatUID)") ?? false
            if !wasRevealed {
                
                let chatSessionId = KeychainWrapper.standard.double(forKey: chatUID)
                if chatSessionId == nil {
                    
                    if !revealButton.isDescendant(of: self.view) {
                        
                        showRevealButton()
                        addTimerLabelAndCheckBackInLabelAnchors()
                        configureRevealTimer(chatSessionId: chatUID)
                    }
                    
                    KeychainWrapper.standard.set(false, forKey: "wasRevealed_\(chatUID)")
                    
                } else {
                    
                    if !revealButton.isDescendant(of: self.view) {
                        
                        showRevealButton()
                        addTimerLabelAndCheckBackInLabelAnchors()
                        startRevealTimer()
                    }
                }
            } else {
                
                if isMovingToParent {
                } else {
                    // view is getting pushed on
                    let callDidSet = wasInvitedOrIsAlreadyAFriend
                    wasInvitedOrIsAlreadyAFriend = callDidSet
                }
            }
        }
    }
    
    fileprivate let emptyCellId = "EmptyCell"
    fileprivate var wasInvitedOrIsAlreadyAFriend = false {
        didSet {
            
            if let chatUID = chatSessionId {
                //let revealDateAsDouble = KeychainWrapper.standard.double(forKey: "\(chatUID)")
                let wasRevealed = KeychainWrapper.standard.bool(forKey: "wasRevealed_\(chatUID)") ?? false
                if wasRevealed && wasInvitedOrIsAlreadyAFriend {
                    
                    if !didRemoveRevealButtonTimerFunctionFireWhileViewIsOnScreen {
                        revealView.isHidden = true
                        revealViewHeightConstraint.constant = 0
                    }
                }
            }
        }
    }
    
    fileprivate var didRemoveRevealButtonTimerFunctionFireWhileViewIsOnScreen = false
    
    let dateFormatterChecker = DateFormatter()
    
    func isEqualImages(image1: UIImage, image2: UIImage) -> Bool {
        let data1: Data? = image1.pngData()
        let data2: Data? = image2.pngData()
        return data1 == data2
    }
    
    ///// ********* Lance's Code ********* /////
    
    // MARK: - Nested Types
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let outgoingTextMessageCellIdentifier = "OutgoingTextMessageCell"
        static let outgoingPhotoMessageCellIdentifier = "OutgoingPhotoMessageCell"
        
        static let incomingTextMessageCellIdentifier = "IncomingTextMessageCell"
        static let incomingPhotoMessageCellIdentifier = "IncomingPhotoMessageCell"
        static let incomingViewedPhotoMessageCellIdentifier = "IncomingViewedPhotoMessageCell"
        static let incomingCommentMessageCellIdentifier = "IncomingCommentMessageCell"

        static let incomingTextMessageWithAvatarCellIdentifier = "IncomingTextMessageWithAvatarCell"
        static let incomingPhotoMessageWithAvatarCellIdentifier = "IncomingPhotoMessageWithAvatarCell"
        static let incomingCommentMessageWithAvatarCellIdentifier = "IncomingCommentMessageWithAvatarCell"
        
        static let welcomeInfoMessageCellIdentifier = "WelcomeInfoMessageCell"
        static let matchInfoMessageCellIdentifier = "MatchInfoMessageCell"
        static let oneRevealInfoMessageCellIdentifier = "OneRevealInfoMessageCell"
        static let mutualRevealInfoMessageCellIdentifier = "MutualRevealInfoMessageCell"
        
        static let tableSectionHeaderIdentifier = "TableSectionHeader"

        static let revealViewHeight: CGFloat = 56
    }

    // MARK: - Instance Properties
    
    @IBOutlet private weak var revealView: RevealView!
    @IBOutlet private weak var tableView: UITableView!
    
    @IBOutlet private weak var inputTextView: UITextView!
    @IBOutlet private weak var inputTextViewContainer: UIView!
    @IBOutlet private weak var inputTextViewPlaceholder: UILabel!
    
    @IBOutlet private weak var sendTextButton: UIButton!
    @IBOutlet private weak var tableViewBottomConstraint: NSLayoutConstraint!

    @IBOutlet private weak var bottomSpacerHeightConstraint: NSLayoutConstraint!

    @IBOutlet private weak var revealViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var welcomeMessageView: UIView!
    
    // MARK: -
    
    private var chatMessagesTimer: Timer?
    
    // MARK: -
    
    private(set) var chat: Chat?
    private(set) var chatMessages: [[ChatMessage]] = []
    private var feed: Feed?
    private var comment: Comment?
    private var potentialChatMember: User?
    
    private(set) var isRefreshingData = false
    private(set) var hasAppliedData = false
    
    var createdChat: ((_ chat: Chat) -> Void)?
    var sendChatPhoto: (() -> Void)?
    var showChatPhoto: ((_ photoMessage: ChatPhotoMessage) -> Void)?
    var showChatMask: ((_ chatMember: ChatMember) -> Void)?
    var unauthorize: (() -> Void)?
    var showUser: ((_ user: User) -> Void)?
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Initializers
    
    deinit {
        unsubscribeFromChatMessagesEvents()
        unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Target Actions - Instance Methods
    
    @IBAction private func onViewTapped(_ sender: Any) {
        Log.high("onViewTapped()", from: self)
        
        view.endEditing(true)
    }
    
    @IBAction private func onSendPhotoButtonTouchUpInside(_ sender: Any) {
        Log.high("onSendPhotoButtonTouchUpInside()", from: self)
        
        sendChatPhoto?()
    }
    
    @IBAction private func onSendTextButtonTouchUpInside(_ sender: Any) {
        Log.high("onSendTextButtonTouchUpInside()", from: self)
        
        if !inputTextView.text.isEmpty {
            sendChatMessage(with: inputTextView.text)
        }
    }
    
    @IBAction private func onUnblockButtonTouchUpInside(_ sender: Any) {
        Log.high("onUnblockButtonTouchUpInside()", from: self)
        
        unblockChatMember()
    }
    
    @IBAction private func onChatPhotoSendingFinished(segue: UIStoryboardSegue) {
        Log.high("onChatPhotoSendingFinished(withSegue: \(String(describing: segue.identifier)))", from: self)
    }
    
    @objc private func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        refreshChatMessages()
    }
    
    // MARK: -
    
    private func showUnblockButton() {
        tableViewBottomConstraint.priority = UILayoutPriority(rawValue: 250)
        
        inputTextViewContainer.isHidden = true
    }
    
    private func hideUnblockButton() {
        tableViewBottomConstraint.priority = UILayoutPriority(rawValue: 750)
        
        inputTextViewContainer.isHidden = false
    }

    // MARK: -
    
    private func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            unauthorize?()
            
        default:
            showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    private func handle(silentError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            unauthorize?()
            
        default:
            break
        }
    }
    
    private func sendChatMessage(with text: String) {
        Log.high("sendChatMessage(withText: \(text))", from: self)
        
        if let chat = self.chat {
            firstly {
                Services.chatMessagesService.send(text: text, isAnonymous: chat.isIncognito, to: chat)
                
            }.done { [weak self](chat) in
                if let chat = self?.chat {
                    
                    //self?.apply(chatMessages: Services.cacheViewContext.chatMessagesManager.fetch(withChatUID: chat.uid))
                    //self?.apply(chat: chat)
                }
            }.catch { [weak self](error) in
                self?.handle(silentError: error)
            }
        } else {
            
            if let feed = self.feed {
                firstly {
                    Services.chatsService.createChat(for: feed)
                }.done { [weak self](chat) in
                    self?.feed?.chat = chat
                    self?.apply(chat: chat)
                    self?.sendChatMessage(with: text)
                    self?.createdChat?(chat)
                }.catch { [weak self](error) in
                    self?.handle(silentError: error)
                }
            }

            if let comment = self.comment {
                firstly {
                    Services.chatsService.createChat(for: comment)
                }.done { [weak self](chat) in
                    self?.feed?.chat = chat
                    self?.apply(chat: chat)
                    self?.sendChatMessage(with: text)
                    self?.createdChat?(chat)
                }.catch { [weak self](error) in
                    self?.handle(silentError: error)
                }
            }
        }
        
        inputTextView.text = nil
        sendTextButton.isEnabled = false
        
        if !tableView.visibleCells.isEmpty {
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
        }
    }
    
    private func markAsViewed(chatMessage: ChatMessage) {
        Log.high("markAsViewed(chatMessage: \(String(describing: chatMessage.uid)))", from: self)
        
        firstly {
            Services.chatMessagesService.markAsViewed(chatMessage: chatMessage)
        }.catch { [weak self](error) in
            self?.handle(silentError: error)
        }
    }
    
    private func unblockChatMember() {
        Log.high("unblockChatMember()", from: self)
        
        guard let accountUserUID = Services.accountAccess?.userUID, let chat = self.chat else {
            return
        }
        
        guard let chatMember = chat.members?.first(where: { chatMember in
            return ((chatMember as! ChatMember).user?.uid != accountUserUID)
        }) as? ChatMember else {
            return
        }
        
        // TODO: Check it
        
        let unblockPromise: Promise<Void>
        
        if chat.isIncognito {
            guard let chatMask = chat.chatMask else {
                return
            }
            
            unblockPromise = Services.chatMasksService.unblock(chatMask: chatMask)
        } else {
            guard let user = chatMember.user else {
                return
            }
            
            unblockPromise = Services.usersService.unblock(user: user)
        }
        
        let loadingViewController = LoadingViewController()

        present(loadingViewController, animated: true, completion: {
            firstly {
                unblockPromise
            }.done {
                loadingViewController.dismiss(animated: true, completion: { [weak self] in
                    if let chat = self?.chat {
                        self?.apply(chat: chat)
                    }
                })
            }.catch { [weak self](error) in
                loadingViewController.dismiss(animated: true, completion: { [weak self] in
                    self?.handle(actionError: error)
                })
            }
        })
    }
    
    private func refreshChatMessages() {
        Log.high("refreshChatMessages()", from: self)
        
        guard let chat = self.chat else {
            return
        }
        
        isRefreshingData = true
        
        firstly {
            Services.chatsService.refresh(chat: chat)
        }.ensure { [weak self] in
            self?.isRefreshingData = false
        }.done { [weak self](chat) in
            self?.apply(chat: chat)
        }.catch { [weak self](error) in
            self?.handle(silentError: error)
        }
    }
    
    private func resetChatMessagesTimer() {
        chatMessagesTimer?.invalidate()
        chatMessagesTimer = nil
    }
    
    private func restartChatMessagesTimer() {
        resetChatMessagesTimer()
        
        guard !isRefreshingData else {
            return
        }
        
        chatMessagesTimer = Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false, block: { [weak self] timer in
            if let viewControleller = self, let chat = viewControleller.chat {
                viewControleller.apply(chatMessages: Services.cacheViewContext.chatMessagesManager.fetch(withChatUID: chat.uid))
            }
        })
        
        if let chatMessagesTimer = chatMessagesTimer {
            RunLoop.current.add(chatMessagesTimer, forMode: RunLoop.Mode.common)
        }
    }
    
    private func restartChatMessagesTimer(with chatMessages: [ChatMessage]) {
        restartChatMessagesTimer()
    }
    
    private func subscribeToChatMessagesEvents() {
        unsubscribeFromChatMessagesEvents()
        
        let chatMessagesManager = Services.cacheViewContext.chatMessagesManager
        
        chatMessagesManager.objectsChangedEvent.connect(self, handler: { [weak self] (chatMessages) in
            self?.restartChatMessagesTimer(with: chatMessages)
        })
        
        chatMessagesManager.startObserving()
    }
    
    private func unsubscribeFromChatMessagesEvents() {
        Services.cacheViewContext.chatMessagesManager.objectsChangedEvent.disconnect(self)
    }

    private func onMyRevealStateChanged(to value: Bool) {
        if let chat = self.chat {
            changeRevealState(in: chat, to: value)
        } else {
            if let feed = self.feed {
                firstly {
                    Services.chatsService.createChat(for: feed)
                }.done { [weak self](chat) in
                    self?.feed?.chat = chat
                    self?.apply(chat: chat)
                    self?.createdChat?(chat)
                    self?.changeRevealState(in: chat, to: value)
                }.catch { [weak self](error) in
                    self?.handle(silentError: error)
                }
            }

            if let comment = self.comment {
                firstly {
                    Services.chatsService.createChat(for: comment)
                }.done { [weak self](chat) in
                    self?.feed?.chat = chat
                    self?.apply(chat: chat)
                    self?.createdChat?(chat)
                    self?.changeRevealState(in: chat, to: value)
                }.catch { [weak self](error) in
                    self?.handle(silentError: error)
                }
            }
        }
    }
    
    private func changeRevealState(in chat: Chat, to value: Bool) {
        guard let accountUserUID = Services.accountAccess?.userUID else {
            return
        }

        let opponentChatMember = chat.members?.first(where: { chatMember in
            return ((chatMember as! ChatMember).user?.uid != accountUserUID)
        }) as? ChatMember

        let promise = value ? Services.chatsService.deanonymize(for: chat) : Services.chatsService.anonymize(for: chat)

        firstly {
            promise
        }.done { [weak self] in
            
            guard let safeSelf = self else { return }
            
            if opponentChatMember?.isDeanonymized ?? false {
                if opponentChatMember?.user?.friendshipStatus == .uninvited {
                    if value {
                        
                        if !safeSelf.didMutualRevealHappen {
                            safeSelf.revealView.set(state: .revealByBothAndShowAddFriendAnimation)
                        }
                        
                        if !safeSelf.didMutualRevealHappen {
                            safeSelf.didMutualRevealHappen = true
                        }
                        
                    } else {
                        safeSelf.revealView.set(state: .revealByOpponent, animate: true)
                    }
                } else {
                    if value {
                        //  safeSelf.revealView.set(state: .revealByBothAnimation, completion: {
                        //  safeSelf.hideRevealView()
                        //})
                        safeSelf.revealView.alpha = 0
                    } else {
                        safeSelf.revealView.set(state: .revealByOpponent, animate: true)
                    }
                }
            } else {
                if value {
                    safeSelf.revealView.set(state: .revealByMe, animate: true)
                } else {
                    safeSelf.revealView.set(state: .none, animate: true)
                }
            }

            safeSelf.chat = Services.chatsService.chat(for: chat.uid)

            safeSelf.revealView.isUserInteractionEnabled = true
        }.catch { [weak self](error) in
            
            guard let safeSelf = self else { return }
            
            safeSelf.handle(actionError: error)

            if opponentChatMember?.isDeanonymized ?? false {
                if value {
                    safeSelf.revealView.set(state: .revealByOpponent, animate: true)
                } else {
                    
                    if !safeSelf.didMutualRevealHappen {
                        safeSelf.revealView.set(state: .revealByBoth, animate: true)
                    }
                    
                    if !safeSelf.didMutualRevealHappen {
                        safeSelf.didMutualRevealHappen = true
                    }
                }
            } else {
                if value {
                    safeSelf.revealView.set(state: .none)
                } else {
                    safeSelf.revealView.set(state: .revealByMe)
                }
            }

            safeSelf.revealView.isUserInteractionEnabled = true
        }
    }

    private func addChatOpponentToFriends() {
        guard let chat = self.chat else {
            return
        }

        guard let accountUserUID = Services.accountAccess?.userUID else {
            return
        }

        let opponentChatMember = chat.members?.first(where: { chatMember in
            return ((chatMember as! ChatMember).user?.uid != accountUserUID)
        }) as? ChatMember

        guard let opponentUser = opponentChatMember?.user else {
            return
        }

        firstly {
            Services.usersService.inviteUser(user: opponentUser)
        } .done { _ in
            //self.hideRevealView()
            //print("done")
        } .catch { [weak self](error) in
            print(error.localizedDescription)
            
            if let error = error as? WebError, let statusCode = error.statusCode {
                print("addFriend statusCode error: ", statusCode)
                if statusCode == 422 {
                    print("User already sent friend intention to this user")
                    self?.alertUserAlreadySentAnInvite()
                    return
                }
            }
            
            self?.handle(actionError: error)
        }
    }
    
    fileprivate func alertUserAlreadySentAnInvite() {
        
        let title: String? = nil
        let message = "You have already invited this user.\n\nWe are awaiting their response."
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (action) in })
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func hideRevealView(animated: Bool = true) {
        /*
        if animated {
            UIView.animate(withDuration: 0.25, animations: {
                self.revealView.alpha = 0
                self.revealViewHeightConstraint.constant = 0

                self.view.layoutIfNeeded()
            }, completion: { _ in
                self.revealView.isHidden = true
            })
        } else {
            revealView.alpha = 0
            revealViewHeightConstraint.constant = 0
            revealView.isHidden = true

            view.layoutIfNeeded()
        }
        */
        
        /*
        if let chatUID = chatSessionId {
            let revealDateAsDouble = KeychainWrapper.standard.double(forKey: "\(chatUID)")
            let wasRevealed = KeychainWrapper.standard.bool(forKey: "wasRevealed_\(chatUID)") ?? false
            if wasRevealed {
                //revealView.isHidden = true
                //revealViewHeightConstraint.constant = 0
            }
            if revealDateAsDouble == nil && !wasRevealed {
                
            } else {
                
            }
        }
        */
    }
    
    // MARK: - Lance's Properties
    public var feedsFromFeedVC = [Feed]()
    fileprivate var copyOfFeed: Feed?
    fileprivate var didMutualRevealHappen = false
    fileprivate var didOneRevealHappen = false
    fileprivate var tempIdIfNoUID = "tempIdIfNoUID"
    fileprivate var originalPostId: String?
    fileprivate var linkUrlImage: UIImage?
    fileprivate func getLinkUrlImage(from feed: Feed, arrOfChatMessages: [ChatMessage]) -> ChatMessage? {
        
        var chatMessages = arrOfChatMessages
        
        chatMessages.sort(by: { $0.createdDate?.timeIntervalSince1970 ?? 0 < $1.createdDate?.timeIntervalSince1970 ?? 0 })
        
        if chatMessages.isEmpty {
            return nil
        }
        
        var messageToReturn: ChatMessage?
        
        if let link = feed.link, let linkURL = URL(string: link) {
            //print(link)
            //print(linkURL)
            
            let chatPhotoMessage: ChatPhotoMessage = Models.cache.viewContext.chatPhotoMessagesManager.append()
            
            let photo = Photo(imageURL: nil,
                              smallImageURL: nil,
                              mediumImageURL: linkURL,
                              largeImageURL: nil)
            
            chatPhotoMessage.photo = photo
            
            //chatFeedMessage.createdDate = Date()
            
            if let firstResponseCreationDate = chatMessages[0].createdDate {
                
                let setNewDateAsDate: Date? = Calendar.current.date(byAdding: .second,
                                                                    value: -1,
                                                                    to: firstResponseCreationDate)
                
                chatPhotoMessage.createdDate = setNewDateAsDate
                
               // if let setNewDateAsDate = setNewDateAsDate {
                    
                    //print("#*# ...**...getLinkUrlImage(feed: -TimeStamp: ", setNewDateAsDate)
                    
                    //let checkerDate = dateFormatterChecker.string(from: setNewDateAsDate)
                    //print("#*# ...**...getLinkUrlImage(feed: -createdDate: ", checkerDate)
                //}
            }
            
            chatPhotoMessage.userUID = 0000000
            
            messageToReturn = chatPhotoMessage
        }
        
        return messageToReturn
    }
        
    fileprivate func tryLinkUrlImage(feed: Feed, arrOfChatMessages: [ChatMessage]) -> ChatMessage {
        
        let chatMessages = arrOfChatMessages
        
        let chatFeedMessage = Models.cache.viewContext.chatPhotoMessagesManager.append()
        
        for feed in feedsFromFeedVC {
            
            if let feedLink = feed.link, let feedLinkURL = URL(string: feedLink) {
                
                for message in chatMessages {
                    
                    if let targetChatMessage = message as? ChatPhotoMessage {
                        
                        if feed.sourceUID == targetChatMessage.chatUID {
                            
                            let photo = Photo(imageURL: nil,
                                              smallImageURL: nil,
                                              mediumImageURL: feedLinkURL,
                                              largeImageURL: nil)
                            
                            chatFeedMessage.photo = photo
                            
                            if let firstResponseCreationDate = chatMessages[0].createdDate {
                                
                                let setNewDateAsDate: Date? = Calendar.current.date(byAdding: .second,
                                                                                    value: -1,
                                                                                    to: firstResponseCreationDate)
                                
                                chatFeedMessage.createdDate = setNewDateAsDate
                                
                                //if let setNewDateAsDate = setNewDateAsDate {
                                //    print("#*# ...**...tryLinkUrlImage(feed: -TimeStamp: ", setNewDateAsDate)
                                    
                                //    let checkerDate = dateFormatterChecker.string(from: setNewDateAsDate)
                                //    print("#*# ...**...tryLinkUrlImage(feed: -createdDate: ", checkerDate)
                                //}
                            }
                            
                            chatFeedMessage.userUID = 0
                            
                        }
                    }
                }
            }
        }
        
        return chatFeedMessage
    }
    
    func apply(feed: Feed, potentialChatMember: User) {
        self.feed = feed
        self.potentialChatMember = potentialChatMember
        
        copyOfFeed = feed
        
        revealView.set(state: .none)

        let chatFeedMessage = Models.cache.viewContext.chatPhotoMessagesManager.append()
        
        if feed.link == nil {
            chatFeedMessage.photo = feed.photo
        } else {
            
            if let link = feed.link, let linkURL = URL(string: link) {
               // print(link)
                //print(linkURL)
                let photo = Photo(imageURL: nil,
                                  smallImageURL: nil,
                                  mediumImageURL: linkURL,
                                  largeImageURL: nil)
                
                chatFeedMessage.photo = photo
            }
        }
        
        chatFeedMessage.createdDate = Date()
        
        //if let chatFeedMessageCreatedDate = chatFeedMessage.createdDate {
            
            //print("#*# ...**...apply(feed: -TimeStamp: ", chatFeedMessageCreatedDate)
            
            //let checkerDate = dateFormatterChecker.string(from: chatFeedMessageCreatedDate)
            //print("#*# ...**...apply(feed: -createdDate: ", checkerDate)
        //}
        
        chatFeedMessage.userUID = 0
        
        apply(chatMessages: [chatFeedMessage])
    }

    func apply(comment: Comment, for feed: Feed, potentialChatMember: User) {
        self.comment = comment
        self.potentialChatMember = potentialChatMember
        
        revealView.set(state: .none)
        
        let chatCommentMessage = Models.cache.viewContext.chatPhotoMessagesManager.append()
        chatCommentMessage.text = comment.text
        chatCommentMessage.photo = feed.photo
        chatCommentMessage.createdDate = Date()
        chatCommentMessage.userUID = 0
        apply(chatMessages: [chatCommentMessage])
    }
    
    private func apply(chatMessages: [ChatMessage]) {
        
        var arrOfChatMessages = chatMessages
        
        //print(arrOfChatMessages.count)
        
        Log.high("apply(chatMessages: \(arrOfChatMessages.count))", from: self)
        
        //print(potentialChatMember)
        //print(copyOfFeed)
        
        for message in arrOfChatMessages {
            if let messageId = message.uid {
                //print("+> messageId: ", messageId)
            } else {
                message.uid = tempIdIfNoUID
            }
            
            //print("+> userId: ", message.userUID)
                
            if let createdDate = message.createdDate {
                //print("+> createdDate: ", createdDate)
            }
                    
            if let viewedDate = message.viewedDate {
                //print("+> viewedDate: ", viewedDate)
            }
            
            if let modifiedDate = message.modifiedDate {
                //print("+> modifiedDate: \(modifiedDate)\n")
            }
        }
        
        if let feed = copyOfFeed {
            
            if let linkURLChatMessage = getLinkUrlImage(from: feed, arrOfChatMessages: arrOfChatMessages) {
                
                //print(linkURLChatMessage.uid as Any)
                //print(linkURLChatMessage.createdDate as Any)
                
                if !arrOfChatMessages.contains(where: { $0.createdDate == linkURLChatMessage.createdDate }) {
                    
                    if let firstChatMessage = chatMessages.first {
                        
                        if firstChatMessage.uid != tempIdIfNoUID {
                            arrOfChatMessages.append(linkURLChatMessage)
                        }
                    }
                }
            }
        }
        
        if feed == nil && copyOfFeed == nil {
            if let accountUserUID = Services.accountAccess?.userUID {
                if arrOfChatMessages.count > 1 {
                    
                    if arrOfChatMessages.indices.contains(1) {
                        
                        if originalPostId == nil {
                            //vic commented out 3 lines
                            let rearrangedMessages = findAndReturnInitialPostPhoto(arrOfChatMessages: arrOfChatMessages)
                            
                            arrOfChatMessages.removeAll()
                            arrOfChatMessages.append(contentsOf: rearrangedMessages)
                            
                            /*
                            arrOfChatMessages.sort(by: { $0.createdDate?.timeIntervalSince1970 ?? 0 < $1.createdDate?.timeIntervalSince1970 ?? 0 })
                            chatMessages.sort(by: { $0.createdDate?.timeIntervalSince1970 ?? 0 < $1.createdDate?.timeIntervalSince1970 ?? 0 })
                            let targetChatMessage = arrOfChatMessages[1]
                            
                            switch targetChatMessage {
                            case is ChatTextMessage:
                                break
                            case let photoMessage as ChatPhotoMessage:
                                
                                resetOrigianlPostDateFor(accountUserUID: accountUserUID,
                                                         arrOfChatMessages: arrOfChatMessages,
                                                         targetChatMessage: targetChatMessage,
                                                         photoMessage: photoMessage)
                                
                            case is ChatInfoMessage:
                                break
                            default:
                                break
                            }
                            */
                        }
                    }
                }
            }
        }
        
        Log.high("apply(chatMessages: \(arrOfChatMessages.count))", from: self)
        
        let filteredChatMessages = arrOfChatMessages.filter { chatMessage -> Bool in
            if let chatInfoMessage = chatMessage as? ChatInfoMessage {
                if chatInfoMessage.type == .some(.mutualReveal) || chatInfoMessage.type == .some(.oneReveal) {
                    return true
                } else {
                    return false
                }
            } else {
                return true
            }
        }

        guard let accountUserUID = Services.accountAccess?.userUID else {
            return
        }

        if let lastInfoMessage = chatMessages.first as? ChatInfoMessage, lastInfoMessage.userUID != accountUserUID {
            switch lastInfoMessage.type {
            case .match:
                
                if !didMutualRevealHappen {
                    
                    revealView.set(state: .revealByBothAnimation) { [weak self] in
                        self?.hideRevealView()
                        if let chatId = self?.chat?.uid, let chat = Services.chatsService.chat(for: chatId) {
                            self?.apply(chat: chat, updateChatMessages: false)
                        }
                    }
                } else {
                    if let chatId = self.chat?.uid, let chat = Services.chatsService.chat(for: chatId) {
                        apply(chat: chat, updateChatMessages: false)
                    }
                }
                
                if !didMutualRevealHappen {
                    didMutualRevealHappen = true
                }
                
            case .oneReveal:
                revealView.set(state: .revealByOpponent, animate: true)

            case .mutualReveal:
                
                if !didMutualRevealHappen {
                    
                    revealView.set(state: .revealByBothAnimation) { [weak self] in
                        self?.hideRevealView()
                        if let chatId = self?.chat?.uid, let chat = Services.chatsService.chat(for: chatId) {
                            self?.apply(chat: chat, updateChatMessages: false)
                        }
                    }
                    
                } else {
                    
                    if let chatId = self.chat?.uid, let chat = Services.chatsService.chat(for: chatId) {
                        apply(chat: chat, updateChatMessages: false)
                    }
                }
                
                if !didMutualRevealHappen {
                    didMutualRevealHappen = true
                }
                
            case .cancelReveal:
                revealView.set(state: .none, animate: true)

            case .none:
                revealView.set(state: .none, animate: true)
            }
        }
        
        self.chatMessages = filteredChatMessages.split(by: { firstChatMessage, secondChatMessage in
            return Calendar.current.isDate(firstChatMessage.createdDate ?? Date(),
                                           inSameDayAs: secondChatMessage.createdDate ?? Date())
        }).map({ chatMessages in
            return Array(chatMessages)
        })
        
        if isViewLoaded {
            tableView.reloadData()
        }
        
        resetChatMessagesTimer()
    }
    
    func apply(chat: Chat, updateChatMessages: Bool = true, feed: Feed? = nil) {
        Log.high("apply(chat: \(chat.uid))", from: self)
        
        self.chat = chat
        
        if feed != nil {
            copyOfFeed = feed
        }
        
        if let accountUserUID = Services.accountAccess?.userUID {
            let opponentChatMember = chat.members?.first(where: { chatMember in
                return ((chatMember as! ChatMember).user?.uid != accountUserUID)
            }) as? ChatMember
            if let potentialChatMember = opponentChatMember?.user {
                self.potentialChatMember = potentialChatMember
            }
        }
        
        if isViewLoaded {
            if (updateChatMessages) {
                apply(chatMessages: Services.cacheViewContext.chatMessagesManager.fetch(withChatUID: chat.uid))
            }

            guard let accountUserUID = Services.accountAccess?.userUID else {
                return
            }
            
            let opponentChatMember = chat.members?.first(where: { chatMember in
                return ((chatMember as! ChatMember).user?.uid != accountUserUID)
            }) as? ChatMember

            let currentUserChatMember = chat.members?.first(where: { currentUserChatMember in
                return ((currentUserChatMember as! ChatMember).user?.uid == accountUserUID)
            }) as? ChatMember
            
            if chat.isIncognito {
                revealView.isHidden = false
                revealViewHeightConstraint.constant = Constants.revealViewHeight
                view.layoutIfNeeded()
                
                if opponentChatMember?.isDeanonymized ?? false && currentUserChatMember?.isDeanonymized == false {
                    revealView.set(state: .revealByOpponent)
                    
                } else if opponentChatMember?.isDeanonymized == false && currentUserChatMember?.isDeanonymized ?? false {
                    revealView.set(state: .revealByMe)
                    
                } else if opponentChatMember?.isDeanonymized ?? false && currentUserChatMember?.isDeanonymized ?? false {
                    
                    if opponentChatMember?.user?.friendshipStatus == .uninvited {
                        
                        if !didMutualRevealHappen {
                            revealView.set(state: .revealByBothAndShowAddFriend)
                        }
                        
                        if !didMutualRevealHappen {
                            didMutualRevealHappen = true
                        }
                    }
                    
                } else {
                    
                    revealView.set(state: .none)
                }
                /////
                /*
                if let status = opponentChatMember?.user?.friendshipStatus {
                    switch status {
                    case .uninvited:
                        //print("\(opponentChatMember?.user?.uid as Any) -uninvited")
                    case .invited:
                        //print("\(opponentChatMember?.user?.uid as Any) -invited")
                    case .friend:
                        //print("\(opponentChatMember?.user?.uid as Any) -friend")
                    case .affirmed:
                        //print("\(opponentChatMember?.user?.uid as Any) -affirmed")
                    }
                }
                */
                if opponentChatMember?.user?.friendshipStatus == .uninvited {
                } else {
                    wasInvitedOrIsAlreadyAFriend = true
                }
                /////
                if chat.chatMask?.isBlocked ?? false {
                    showUnblockButton()
                } else {
                    hideUnblockButton()
                }
            } else {
                
                let wasRevealead = KeychainWrapper.standard.bool(forKey: "wasRevealed_\(chat.uid)") ?? false
                if wasRevealead && wasInvitedOrIsAlreadyAFriend {
                    
                    if !didRemoveRevealButtonTimerFunctionFireWhileViewIsOnScreen {
                        revealView.isHidden = true
                        revealViewHeightConstraint.constant = 0
                        
                    }
                } else {
                    print("we will possibly add some code here")
                }
                
                if opponentChatMember?.user?.isBlocked ?? false {
                    showUnblockButton()
                } else {
                    hideUnblockButton()
                }
            }
            
            hasAppliedData = true
        } else {
            hasAppliedData = false
        }
    }
    
    fileprivate func findAndReturnInitialPostPhoto(arrOfChatMessages: [ChatMessage]) -> [ChatMessage] {
        
        var chatMessages = arrOfChatMessages
        
        chatMessages.sort(by: { $0.createdDate?.timeIntervalSince1970 ?? 0 < $1.createdDate?.timeIntervalSince1970 ?? 0 })
        
        /*
        for message in chatMessages {
            if let messageId = message.uid {
                print("O> messageId: ", messageId)
            }

            print("O> userId: ", message.userUID)

            if let createdDate = message.createdDate {
                print("O> createdDate: ", createdDate)
            }

            if let viewedDate = message.viewedDate {
                print("O> viewedDate: \n", viewedDate)
            }

            if let modifiedDate = message.modifiedDate {
                print("O> modifiedDate: \(modifiedDate)\n")
            }
        }
        */
        
        for message in chatMessages {
            
            if let targetChatMessage = message as? ChatPhotoMessage {
                
                for post in feedsFromFeedVC {
                    
                    if let postRawLargeImageURL = post.rawLargeImageURL, let url = URL(string: postRawLargeImageURL) {
                        if url == targetChatMessage.largePhotoURL {
                            
                            if let firstResponseCreationDate = chatMessages[0].createdDate {
                                
                                //print(firstResponseCreationDate)
                                
                                let setNewDateAsDate: Date? = Calendar.current.date(byAdding: .second,
                                                                                    value: -1,
                                                                                    to: firstResponseCreationDate)
                                //print(setNewDateAsDate as Any)
                                
                                targetChatMessage.createdDate = setNewDateAsDate
                                
                                originalPostId = targetChatMessage.uid
                            }
                            break
                        }
                    }
                }
            }
        }
        chatMessages.sort(by: { $0.createdDate?.timeIntervalSince1970 ?? 0 < $1.createdDate?.timeIntervalSince1970 ?? 0 })
        return chatMessages
    }
    
    fileprivate func resetOrigianlPostDateFor(accountUserUID: Int64, arrOfChatMessages: [ChatMessage],
                                              targetChatMessage: ChatMessage, photoMessage: ChatPhotoMessage) {
        
        if accountUserUID == targetChatMessage.userUID {
            
            if let originalPostImageURL = photoMessage.largePhotoURL {
                
                for post in feedsFromFeedVC {
                    
                    // let possiblyOriginalPhoto = post.originalImageURL
                    // Services.imageLoader.loadImage(for: originalPostImageURL) { (image) in }
                    
                    if let postRawLargeImageURL = post.rawLargeImageURL, let url = URL(string: postRawLargeImageURL) {
                        if url == originalPostImageURL {
                            //print("1")
                            changePostDate(arrOfChatMessages: arrOfChatMessages, targetChatMessage: targetChatMessage)
                            break
                        }
                    }
                    
                    if let postRawMediumImageURL = post.rawMediumImageURL, let url = URL(string: postRawMediumImageURL) {
                        if url == originalPostImageURL {
                            //print("2")
                            changePostDate(arrOfChatMessages: arrOfChatMessages, targetChatMessage: targetChatMessage)
                            break
                        }
                    }
                    
                    if let postRawOriginalImageURL = post.rawOriginalImageURL, let url = URL(string: postRawOriginalImageURL) {
                        if url == originalPostImageURL {
                            //print("3")
                            changePostDate(arrOfChatMessages: arrOfChatMessages, targetChatMessage: targetChatMessage)
                            break
                        }
                    }
                    
                    if let postRawSmallImageURL = post.rawLargeImageURL, let url = URL(string: postRawSmallImageURL) {
                        if url == originalPostImageURL {
                            //print("4")
                            changePostDate(arrOfChatMessages: arrOfChatMessages, targetChatMessage: targetChatMessage)
                            break
                        }
                    }
                    
                    if post.originalImageURL == originalPostImageURL {
                        //print("5")
                        changePostDate(arrOfChatMessages: arrOfChatMessages, targetChatMessage: targetChatMessage)
                        break
                    }
                }
            }
        }
    }
    
    fileprivate func changePostDate(arrOfChatMessages: [ChatMessage], targetChatMessage: ChatMessage) {
        
        if let firstResponseCreationDate = arrOfChatMessages[0].createdDate {
            
            let setNewDateAsDate: Date? = Calendar.current.date(byAdding: .second,
                                                                value: -1,
                                                                to: firstResponseCreationDate)
            
            targetChatMessage.createdDate = setNewDateAsDate
            
            originalPostId = targetChatMessage.uid
        }
    }
    
    // MARK: -
    
    private func setupFont() {
        inputTextView.font = Fonts.regular(ofSize: 17.0)
    }
    
    private func configureWelcomeMessage() {
        welcomeMessageView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
    }
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateFormatterChecker.dateFormat = "MMM-dd-yyyy • h:mm a"
        configureNotifications()
        
        setupFont()
        tableView.transform = CGAffineTransform(rotationAngle: -CGFloat.pi)
        
        tableView.register(EmptyCell.self, forCellReuseIdentifier: emptyCellId)
        
        inputTextView.textContainerInset = .zero
        inputTextView.textContainer.lineFragmentPadding = 0
        
        inputTextViewContainer.layer.cornerRadius = 12.0
        inputTextViewContainer.layer.shadowColor = UIColor.black.cgColor
        inputTextViewContainer.layer.shadowOffset = CGSize(width: 1, height: 1)
        inputTextViewContainer.layer.shadowOpacity = 0.5
        inputTextViewContainer.layer.masksToBounds = false
        inputTextViewContainer.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        configureWelcomeMessage()

        revealView.onRevealStateChanged = { [weak self](value) in
            self?.onMyRevealStateChanged(to: value)
        }

        revealView.onAddAsAFriendTapped = { [weak self] in
            self?.addChatOpponentToFriends()
        }

        hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        isRefreshingData = false
        
        if let chat = self.chat, !hasAppliedData {
            apply(chat: chat)
        }
                
        subscribeToChatMessagesEvents()
        subscribeToKeyboardNotifications()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onApplicationWillEnterForeground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        setNeedsStatusBarAppearanceUpdate()

        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
        
        unsubscribeFromChatMessagesEvents()
        unsubscribeFromKeyboardNotifications()
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
        hasAppliedData = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if isMovingFromParent {
        } else {
            // is getting pushed off the stack
            invalidateRevealTimer()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.scrollIndicatorInsets = UIEdgeInsets(top: 0.0,
                                                            left: 0.0,
                                                            bottom: 0.0,
                                                            right: tableView.bounds.size.width - 8)
        
        inputTextView.textContainerInset = .zero
        inputTextView.textContainer.lineFragmentPadding = 0
    }
}

// MARK: - UITableViewDataSource

extension ChatMessageTableViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if feed != nil || comment != nil {
            return chatMessages.count
        } else {
            return chatMessages.count + ((chat?.isIncognito ?? false) ? 0 : 1)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ((section < chatMessages.count) ? chatMessages[section].count : 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let accountUserUID = Services.accountAccess?.userUID else {
            fatalError()
        }
        
        let cell: UITableViewCell
        
        if indexPath.section < chatMessages.count {
            
            let arrOfChatMessages = chatMessages[indexPath.section][indexPath.row]
            
            switch arrOfChatMessages {
            case let infoMessage as ChatInfoMessage:
                switch infoMessage.type {
                case .some(let type):
                    switch type {
                    case .mutualReveal:
                        /*
                        cell = tableView.dequeueReusableCell(withIdentifier: Constants.mutualRevealInfoMessageCellIdentifier,
                                                             for: indexPath)

                        config(mutualRevealInfoCell: cell as! MutualRevealMessageTableViewCell)
                        */
                        if let chatUID = chat?.uid {
                            didStartRevealTimer = true
                            let wasRevealed = KeychainWrapper.standard.bool(forKey: "wasRevealed_\(chatUID)") ?? false
                            if wasRevealed {
                                cell = tableView.dequeueReusableCell(withIdentifier: Constants.mutualRevealInfoMessageCellIdentifier, for: indexPath)
                                //cell = tableView.dequeueReusableCell(withIdentifier: emptyCellId, for: indexPath) as! EmptyCell
                                config(mutualRevealInfoCell: cell as! MutualRevealMessageTableViewCell)
                                
                            } else {
                                
                                cell = tableView.dequeueReusableCell(withIdentifier: emptyCellId, for: indexPath) as! EmptyCell
                            }
                            
                        } else {
                            
                            cell = tableView.dequeueReusableCell(withIdentifier: emptyCellId, for: indexPath) as! EmptyCell
                            //cell = tableView.dequeueReusableCell(withIdentifier: Constants.oneRevealInfoMessageCellIdentifier, for: indexPath)
                            //config(oneRevealInfoCell: cell as! OneRevealMessageTableViewCell)
                        }
                        
                    case .oneReveal:
                        
                        cell = tableView.dequeueReusableCell(withIdentifier: Constants.oneRevealInfoMessageCellIdentifier,
                                                             for: indexPath)
                        
                        config(oneRevealInfoCell: cell as! OneRevealMessageTableViewCell)
                        
//                        if !didOneRevealHappen {
//                            didOneRevealHappen = true
//                            cell = tableView.dequeueReusableCell(withIdentifier: Constants.oneRevealInfoMessageCellIdentifier,
//                                                                 for: indexPath)
//
//                            config(oneRevealInfoCell: cell as! OneRevealMessageTableViewCell)
//
//                        } else {
//                            cell = tableView.dequeueReusableCell(withIdentifier: emptyCellId, for: indexPath) as! EmptyCell
//                        }
                        
                        if let chatUID = chat?.uid {
                            let revealDateAsDouble = KeychainWrapper.standard.double(forKey: "\(chatUID)")
                            let wasRevealed = KeychainWrapper.standard.bool(forKey: "wasRevealed_\(chatUID)") ?? false
                            if revealDateAsDouble == nil && !wasRevealed {
                                revealView.set(state: .revealByOpponent, animate: true)
                            } else {
                                
                            }
                        }
                    //revealView.set(state: .revealByOpponent, animate: true)
                    default:
                        fatalError()
                    }
                case .none:
                    fatalError()
                }

            case let textMessage as ChatTextMessage:
                if textMessage.userUID == accountUserUID {
                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.outgoingTextMessageCellIdentifier,
                                                         for: indexPath)

                    config(outgoingTextMessageCell: cell as! OutgoingTextMessageTableViewCell, with: textMessage)
                } else {

                    let deanonymizedMembers = chat?.members?.allObjects.filter({ member -> Bool in
                        return (member as? ChatMember)?.isDeanonymized ?? false
                    })

                    if deanonymizedMembers?.count == 2 {
                        
                        /*
                         cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingTextMessageWithAvatarCellIdentifier,
                                                              for: indexPath)

                         config(incomingTextMessageCell: cell as! IncomingTextMessageWithAvatarTableViewCell, with: textMessage)
                         */
                        
                        ///// ********* Lance's Code ********* /////
                        if let chatUID = chat?.uid {
                            
                            let wasRevealed = KeychainWrapper.standard.bool(forKey: "wasRevealed_\(chatUID)") ?? false
                            if wasRevealed {
                                
                                cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingTextMessageWithAvatarCellIdentifier,
                                                                     for: indexPath)
                                config(incomingTextMessageCell: cell as! IncomingTextMessageWithAvatarTableViewCell, with: textMessage)
                                
                            } else {
                                
                                cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingTextMessageCellIdentifier, for: indexPath)
                                config(incomingTextMessageCell: cell as! IncomingTextMessageTableViewCell, with: textMessage)
                            }
                        } else {
                            
                            cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingTextMessageCellIdentifier, for: indexPath)
                            config(incomingTextMessageCell: cell as! IncomingTextMessageTableViewCell, with: textMessage)
                        }
                        ///// ********* Lance's Code ********* /////
                        
                    } else {
                        cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingTextMessageCellIdentifier, for: indexPath)
                        config(incomingTextMessageCell: cell as! IncomingTextMessageTableViewCell, with: textMessage)
                    }

                }
                
            case let photoMessage as ChatPhotoMessage:
                if photoMessage.userUID == accountUserUID {
                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.outgoingPhotoMessageCellIdentifier,
                                                         for: indexPath)
                    
                    config(outgoingPhotoMessageCell: cell as! OutgoingPhotoMessageTableViewCell, with: photoMessage)
                } else {
                    if photoMessage.text != nil {

                        let deanonymizedMembers = chat?.members?.allObjects.filter({ member -> Bool in
                            return (member as? ChatMember)?.isDeanonymized ?? false
                        })

                        if deanonymizedMembers?.count == 2 {
                            
                            /*
                            cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingCommentMessageWithAvatarCellIdentifier,
                                                                 for: indexPath)

                            config(incomingPhotoMessageCell: cell as! IncomingCommentMessageWithAvatarTableViewCell, with: photoMessage)
                            */
                            
                            ///// ********* Lance's Code ********* /////
                            if let chatUID = chat?.uid {
                                
                                let wasRevealed = KeychainWrapper.standard.bool(forKey: "wasRevealed_\(chatUID)") ?? false
                                if wasRevealed {
                                    
                                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingCommentMessageWithAvatarCellIdentifier,
                                                                         for: indexPath)
                                    config(incomingPhotoMessageCell: cell as! IncomingCommentMessageWithAvatarTableViewCell, with: photoMessage)
                                } else {
                                    
                                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingCommentMessageCellIdentifier, for: indexPath)
                                    config(incomingPhotoMessageCell: cell as! IncomingCommentMessageTableViewCell, with: photoMessage)
                                }
                            } else {
                                
                                cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingCommentMessageCellIdentifier, for: indexPath)
                                config(incomingPhotoMessageCell: cell as! IncomingCommentMessageTableViewCell, with: photoMessage)
                            }
                            ///// ********* Lance's Code ********* /////
                            
                        } else {
                            cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingCommentMessageCellIdentifier,
                                                                 for: indexPath)

                            config(incomingPhotoMessageCell: cell as! IncomingCommentMessageTableViewCell, with: photoMessage)
                        }
                    } else {

                        let deanonymizedMembers = chat?.members?.allObjects.filter({ member -> Bool in
                            return (member as? ChatMember)?.isDeanonymized ?? false
                        })

                        if deanonymizedMembers?.count == 2 {
                            /*
                            cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingPhotoMessageWithAvatarCellIdentifier,
                                                                 for: indexPath)

                            config(incomingPhotoMessageCell: cell as! IncomingPhotoMessageWithAvatarTableViewCell, with: photoMessage)
                            */
                            
                            ///// ********* Lance's Code ********* /////
                            if let chatUID = chat?.uid {
                                
                                let wasRevealed = KeychainWrapper.standard.bool(forKey: "wasRevealed_\(chatUID)") ?? false
                                if wasRevealed {
                                    
                                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingPhotoMessageWithAvatarCellIdentifier,
                                                                         for: indexPath)
                                    config(incomingPhotoMessageCell: cell as! IncomingPhotoMessageWithAvatarTableViewCell, with: photoMessage)
                                } else {
                                    
                                    cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingPhotoMessageCellIdentifier, for: indexPath)
                                    config(incomingPhotoMessageCell: cell as! IncomingPhotoMessageTableViewCell, with: photoMessage)
                                }
                            } else {
                                
                                cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingPhotoMessageCellIdentifier, for: indexPath)
                                config(incomingPhotoMessageCell: cell as! IncomingPhotoMessageTableViewCell, with: photoMessage)
                            }
                            ///// ********* Lance's Code ********* /////
                            
                        } else {
                            cell = tableView.dequeueReusableCell(withIdentifier: Constants.incomingPhotoMessageCellIdentifier,
                                                                 for: indexPath)

                            config(incomingPhotoMessageCell: cell as! IncomingPhotoMessageTableViewCell, with: photoMessage)
                        }
                    }
                }
            default:
                fatalError()
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: emptyCellId, for: indexPath) as! EmptyCell
            //cell = tableView.dequeueReusableCell(withIdentifier: Constants.welcomeInfoMessageCellIdentifier, for: indexPath)
        }
        
        cell.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
        
        return cell
    }
}

// MARK: - UITableViewDataSource Helper

extension ChatMessageTableViewController {

    // MARK: - Helper Methods

    private func config(outgoingTextMessageCell cell: OutgoingTextMessageTableViewCell, with textMessage: ChatTextMessage) {
        cell.message = textMessage.text
        // 1446, 1588, 1689
        if let createdDate = textMessage.createdDate {
            
            //print("#*# ...**...Outgoing Text Message -TimeStamp: ", createdDate)
            
            //let checkerDate = dateFormatterChecker.string(from: createdDate)
            //print("#*# ...**...Outgoing Text Message -createdDate: ", checkerDate)
            
            cell.date = ChatMessageDateFormatter.shared.string(from: createdDate)
        } else {
            cell.date = nil
        }

        cell.isSent = textMessage.isSent
        cell.isFailed = textMessage.isFailed
        cell.isViewed = textMessage.isViewed

        cell.onErrorButtonClicked = { [unowned self, weak textMessage] in
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

            alertController.addAction(UIAlertAction(title: "Retry".localized(), style: .default, handler: { action in
                if let textMessage = textMessage, !textMessage.isSent {
                    firstly {
                        Services.chatMessagesService.resend(chatMessage: textMessage)
                    }.catch { [weak self](error) in
                        self?.handle(silentError: error)
                    }
                }
            }))

            alertController.addAction(UIAlertAction(title: "Delete".localized(), style: .destructive, handler: { action in
                if let textMessage = textMessage {
                    firstly {
                        Services.chatMessagesService.delete(chatMessage: textMessage)
                    }.catch { [weak self](error) in
                        self?.handle(silentError: error)
                    }
                }
            }))

            alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel))

            self.present(alertController, animated: true)
        }
    }

    private func config(outgoingPhotoMessageCell cell: OutgoingPhotoMessageTableViewCell, with photoMessage: ChatPhotoMessage) {
        if let createdDate = photoMessage.createdDate {
            
            //print("#*# ...**...Outgoing Photo Message -TimeStamp: ", createdDate)
            
            //let checkerDate = dateFormatterChecker.string(from: createdDate)
            //print("#*# ...**...Outgoing Photo Message -createdDate: ", checkerDate)
            
            cell.date = ChatMessageDateFormatter.shared.string(from: createdDate)
        } else {
            cell.date = nil
        }

        if photoMessage.isSent || photoMessage.isFailed {
            cell.hideLoadingState()
        } else {
            cell.showLoadingState()
        }

        cell.isSent = photoMessage.isSent
        cell.isFailed = photoMessage.isFailed
        cell.isViewed = photoMessage.isViewed

        cell.onErrorButtonClicked = { [unowned self, weak photoMessage] in
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

            alertController.addAction(UIAlertAction(title: "Retry".localized(), style: .default, handler: { action in
                if let photoMessage = photoMessage, !photoMessage.isSent {
                    firstly {
                        Services.chatMessagesService.resend(chatMessage: photoMessage)
                    }.catch { [weak self](error) in
                        self?.handle(silentError: error)
                    }
                }
            }))

            alertController.addAction(UIAlertAction(title: "Delete".localized(), style: .destructive, handler: { action in
                if let photoMessage = photoMessage {
                    firstly {
                        Services.chatMessagesService.delete(chatMessage: photoMessage)
                    }.catch { [weak self](error) in
                        self?.handle(silentError: error)
                    }
                }
            }))

            alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel))

            self.present(alertController, animated: true)
        }

        cell.photoImage = nil

        cell.onViewPhotoButtonClicked = { [unowned self, weak photoMessage] in
            if let photoMessage = photoMessage {
                self.showChatPhoto?(photoMessage)
            }
        }
    }

    private func config(incomingTextMessageCell cell: IncomingTextMessageTableViewCell, with textMessage: ChatTextMessage) {
        cell.message = textMessage.text
        
        let chatMember = chat?.members?.allObjects.first(where: { member -> Bool in
            return (member as? ChatMember)?.user?.uid == textMessage.userUID
        }) as? ChatMember
        
        cell.author = chatMember?.user?.relationshipStatus?.localized() ?? potentialChatMember?.relationshipStatus?.localized() ?? "Unknown".localized()
        
        if textMessage.isViewed {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                cell.isViewed = textMessage.isViewed
            })
        } else {
            cell.isViewed = textMessage.isViewed
        }

        if let createdDate = textMessage.createdDate {
            
            //print("#*# ...**...Incoming Text Message -TimeStamp: ", createdDate)
            
            //let checkerDate = dateFormatterChecker.string(from: createdDate)
            //print("#*# ...**...Incoming Text Message -createdDate: ", checkerDate)
            
            cell.date = ChatMessageDateFormatter.shared.string(from: createdDate)
        } else {
            cell.date = nil
        }
    }

    private func config(incomingTextMessageCell cell: IncomingTextMessageWithAvatarTableViewCell, with textMessage: ChatTextMessage) {
        config(incomingTextMessageCell: cell as IncomingTextMessageTableViewCell, with: textMessage)
    }

    private func config(incomingViewedPhotoMessageCell cell: IncomingViewedPhotoMessageTableViewCell, with photoMessage: ChatPhotoMessage) {
        if let createdDate = photoMessage.createdDate {
            cell.date = ChatMessageDateFormatter.shared.string(from: createdDate)
        } else {
            cell.date = nil
        }
    }

    private func config(incomingPhotoMessageCell cell: IncomingPhotoMessageTableViewCell, with photoMessage: ChatPhotoMessage) {
        let chatMember = chat?.members?.allObjects.first(where: { member -> Bool in
            return (member as? ChatMember)?.user?.uid == photoMessage.userUID
        }) as? ChatMember
        
        cell.author = chatMember?.user?.relationshipStatus?.localized() ?? potentialChatMember?.relationshipStatus?.localized() ?? "Unknown".localized()
        
        if let createdDate = photoMessage.createdDate {
            
            //print("#*# ...**...Incoming Photo Message -TimeStamp: ", createdDate)
            
            //let checkerDate = dateFormatterChecker.string(from: createdDate)
            //print("#*# ...**...Incoming Photo Message -createdDate: ", checkerDate)
            
            cell.date = ChatMessageDateFormatter.shared.string(from: createdDate)
        } else {
            cell.date = nil
        }
        
        cell.photoImage = nil

        cell.onViewPhotoButtonClicked = { [unowned self, weak photoMessage] in
            if let photoMessage = photoMessage {
                self.showChatPhoto?(photoMessage)
            }
        }
    }

    private func config(incomingPhotoMessageCell cell: IncomingPhotoMessageWithAvatarTableViewCell, with photoMessage: ChatPhotoMessage) {
        config(incomingPhotoMessageCell: cell as IncomingPhotoMessageTableViewCell, with: photoMessage)
    }

    private func config(incomingPhotoMessageCell cell: IncomingCommentMessageTableViewCell, with photoMessage: ChatPhotoMessage) {
        let chatMember = chat?.members?.allObjects.first(where: { member -> Bool in
            return (member as? ChatMember)?.user?.uid == photoMessage.userUID
        }) as? ChatMember
        
        cell.author = "\(chatMember?.user?.relationshipStatus ?? potentialChatMember?.relationshipStatus ?? "Unknown") commented".localized()
        
        if let createdDate = photoMessage.createdDate {
            cell.date = ChatMessageDateFormatter.shared.string(from: createdDate)
        } else {
            cell.date = nil
        }

        cell.photoImage = nil

        cell.messageText = photoMessage.text
        
        if photoMessage.isViewed {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                cell.isViewed = photoMessage.isViewed
            })
        } else {
            cell.isViewed = photoMessage.isViewed
        }

        cell.onViewPhotoButtonClicked = { [unowned self, weak photoMessage] in
            if let photoMessage = photoMessage {
                self.showChatPhoto?(photoMessage)
            }
        }
    }

    private func config(incomingPhotoMessageCell cell: IncomingCommentMessageWithAvatarTableViewCell, with photoMessage: ChatPhotoMessage) {
        config(incomingPhotoMessageCell: cell as IncomingCommentMessageTableViewCell, with: photoMessage)
    }

    private func config(oneRevealInfoCell cell: OneRevealMessageTableViewCell) {
        cell.chatMaskName = chat?.chatMask?.name
    }

    private func config(mutualRevealInfoCell cell: MutualRevealMessageTableViewCell) {
        guard let accountUserUID = Services.accountAccess?.userUID, let chat = self.chat else {
            return
        }

        cell.frontAvatarImage = nil
        cell.backAvatarImage = nil

        let chatMember = chat.members?.first(where: { chatMember in
            return ((chatMember as! ChatMember).user?.uid != accountUserUID)
        }) as? ChatMember

        if let user = chatMember?.user {
            cell.fullName = user.fullName

            let schoolTitle = user.schoolTitle ?? "unknown school".localized()

            if let schoolGradeNumber = Services.schoolGradesService.schoolGradeNumber(of: Int(user.classYear)) {
                cell.schoolInfo = String(format: "%dth grade %@".localized(), schoolGradeNumber, schoolTitle)
            } else {
                cell.schoolInfo = String(format: "Finished %@".localized(), schoolTitle)
            }

            cell.onFrontAvatarTapped = { [weak self] in
                //self?.showUser?(user)
                
                self?.presentUser(with: user.uid)
            }
        } else {
            cell.fullName = nil
            cell.schoolInfo = nil
        }
    }

    private func config(sectionHeaderView: ChatMessageTableSectionHeaderView, with chatMessage: ChatMessage) {
        
        let currentDate = Date()
        
        if let chatMessageCreatedDate = chatMessage.createdDate {
            
            //print("#*# ...**...Section X HeaderView -TimeStamp: ", chatMessageCreatedDate)
            
            //let checkerDate = dateFormatterChecker.string(from: chatMessageCreatedDate)
            //print("#*# ...**...Section X HeaderView -createdDate: ", checkerDate)
        } else {
            
            let checkerDate = dateFormatterChecker.string(from: currentDate)
            //print("#*# ...**...Section Y HeaderView -createdDate: ", checkerDate)
        }
        
        sectionHeaderView.title = ChatSectionDateFormatter.shared.string(from: chatMessage.createdDate ?? currentDate)
    }

    // MARK: -
    private func loadPhotoImage(for cell: IncomingPhotoMessageTableViewCell, with photoMessage: ChatPhotoMessage) {
        cell.photoImage = nil
        
        cell.showLoadingState()
        
        if let imageURL = photoMessage.largePhotoURL {
            Services.imageLoader.loadImage(for: imageURL, in: cell.photoImageViewTarget, completionHandler: { [weak cell] image in
                
                cell?.hideLoadingState()
            })
        } else {
            
            if let linkUrlImage = linkUrlImage {
                
                cell.hideLoadingState()
                cell.photoImage = linkUrlImage
                
            } else {
                
                if let linkURL = photoMessage.mediumPhotoURL {
                    
                    Managers.linkPreviewManager.getInformationFrom(link: linkURL, completion: {
                        (linkString, linkDescription, imageURLString, error) in
                        
                        if let imageURLString = imageURLString, let imageURL = URL(string: imageURLString) {
                            Services.imageLoader.loadImage(for: imageURL, completionHandler: { [weak self](image) in
                                
                                cell.hideLoadingState()
                                
                                if image == nil {
                                    
                                    cell.photoImage = UIImage(named: "TutorialEmoji3")
                                    self?.linkUrlImage = UIImage(named: "TutorialEmoji3")
                                    
                                } else {
                                    
                                    cell.photoImage = image
                                    self?.linkUrlImage = image
                                }
                            })
                        }
                    })
                } else {
                    cell.hideLoadingState()
                    cell.photoImage = UIImage(named: "TutorialEmoji3")
                }
            }
        }
    }

    private func loadPhotoImage(for cell: OutgoingPhotoMessageTableViewCell, with photoMessage: ChatPhotoMessage) {
        cell.photoImage = nil

        cell.showLoadingState()
        
        if let imageURL = photoMessage.largePhotoURL {
            Services.imageLoader.loadImage(for: imageURL, in: cell.photoImageViewTarget, completionHandler: { [weak cell] image in
                cell?.hideLoadingState()
            })
        }
    }

    private func loadAvatar(for cell: IncomingTextMessageWithAvatarTableViewCell, with chatMessage: ChatMessage) {
        cell.avatarImage = nil
        
        let chatMember = chat?.members?.allObjects.first(where: { member -> Bool in
            return (member as? ChatMember)?.user?.uid == chatMessage.userUID
        })
         
        if let chatMember = chatMember as? ChatMember {
            if let imageURL = chatMember.user?.smallAvatarURL {
                Services.imageLoader.loadImage(for: imageURL, in: cell.avatarImageViewTarget, placeholder: #imageLiteral(resourceName: "AvatarMediumPlaceholder"))
            }
            
            ////////////////>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<
            if let user = chatMember.user {
                cell.delegate = self
                cell.user = user
            }
        }
    }
    
    private func loadAvatar(for cell: IncomingPhotoMessageWithAvatarTableViewCell, with photoMessage: ChatPhotoMessage) {
        cell.avatarImage = nil

        let chatMember = chat?.members?.allObjects.first(where: { member -> Bool in
            return (member as? ChatMember)?.user?.uid == photoMessage.userUID
        })

        if let chatMember = chatMember as? ChatMember {
            if let imageURL = chatMember.user?.smallAvatarURL {
                Services.imageLoader.loadImage(for: imageURL, in: cell.avatarImageViewTarget, placeholder: #imageLiteral(resourceName: "AvatarMediumPlaceholder"))
            }
            
            ////////////////>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<
            if let user = chatMember.user {
                cell.delegate = self
                cell.user = user
            }
        }
    }

    private func loadAvatar(for cell: IncomingCommentMessageWithAvatarTableViewCell, with photoMessage: ChatPhotoMessage) {
        cell.avatarImage = nil

        let chatMember = chat?.members?.allObjects.first(where: { member -> Bool in
            return (member as? ChatMember)?.user?.uid == photoMessage.userUID
        })

        if let chatMember = chatMember as? ChatMember {
            if let imageURL = chatMember.user?.smallAvatarURL {
                Services.imageLoader.loadImage(for: imageURL, in: cell.avatarImageViewTarget, placeholder: #imageLiteral(resourceName: "AvatarMediumPlaceholder"))
            }
            
            ////////////////>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<
            if let user = chatMember.user {
                cell.delegate = self
                cell.user = user
            }
        }
    }

    private func loadFrontAvatarImage(for cell: MutualRevealMessageTableViewCell, from imageURL: URL) {
        cell.frontAvatarImage = #imageLiteral(resourceName: "AvatarMediumPlaceholder")

        Services.imageLoader.loadImage(for: imageURL, in: cell.frontAvatarImageViewTarget, placeholder: #imageLiteral(resourceName: "AvatarMediumPlaceholder"))
    }

    private func loadBackAvatarImage(for cell: MutualRevealMessageTableViewCell, from imageURL: URL) {
        cell.backAvatarImage = #imageLiteral(resourceName: "AvatarMediumPlaceholder")

        Services.imageLoader.loadImage(for: imageURL, in: cell.backAvatarImageViewTarget, placeholder: #imageLiteral(resourceName: "AvatarMediumPlaceholder"))
    }
}

// MARK: - UITableViewDelegate

extension ChatMessageTableViewController: UITableViewDelegate {
    
    // MARK: - Instance Methods
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section < chatMessages.count {
            let chatMessage = chatMessages[indexPath.section][indexPath.row]
            
            switch chatMessage {
            case is ChatTextMessage:
                markAsViewed(chatMessage: chatMessage)
                if let cell = cell as? IncomingTextMessageWithAvatarTableViewCell {
                    loadAvatar(for: cell, with: chatMessage)
                }
                
            case let photoMessage as ChatPhotoMessage:
                if let cell = cell as? IncomingPhotoMessageTableViewCell, cell.photoImage == nil {
                    loadPhotoImage(for: cell, with: photoMessage)
                }
                
                if let cell = cell as? OutgoingPhotoMessageTableViewCell, cell.photoImage == nil {
                    loadPhotoImage(for: cell, with: photoMessage)
                }
                
                if let cell = cell as? IncomingPhotoMessageWithAvatarTableViewCell {
                    loadAvatar(for: cell, with: photoMessage)
                }
                
                if let cell = cell as? IncomingCommentMessageWithAvatarTableViewCell {
                    loadAvatar(for: cell, with: photoMessage)
                }

                markAsViewed(chatMessage: photoMessage)

            case is ChatInfoMessage:
                markAsViewed(chatMessage: chatMessage)
                
                if let cell = cell as? MutualRevealMessageTableViewCell {
                    if let accountUserUID = Services.accountAccess?.userUID, let chat = self.chat {
                        let frontChatMember = chat.members?.first(where: { chatMember in
                            return ((chatMember as! ChatMember).user?.uid != accountUserUID)
                        }) as? ChatMember
                        
                        let backChatMember = chat.members?.first(where: { chatMember in
                            return ((chatMember as! ChatMember).user?.uid == accountUserUID)
                        }) as? ChatMember
                        
                        if let avatarURL = frontChatMember?.user?.mediumAvatarURL {
                            loadFrontAvatarImage(for: cell, from: avatarURL)
                        }
                        
                        if let avatarURL = backChatMember?.user?.mediumAvatarURL {
                            loadBackAvatarImage(for: cell, from: avatarURL)
                        }
                    }
                }
                
            default:
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section < chatMessages.count {
            if let chatMessage = chatMessages[section].first {
                let sectionHeaderView = tableView.dequeueReusableCell(withIdentifier: Constants.tableSectionHeaderIdentifier,
                                                                      for: IndexPath(row: 0, section: section))
                
                config(sectionHeaderView: sectionHeaderView as! ChatMessageTableSectionHeaderView, with: chatMessage)
                
                sectionHeaderView.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
                
                return sectionHeaderView
            }
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section < chatMessages.count {
            return 15.0
        } else {
            return 0.0
        }
    }
}

// MARK: - UITextViewDelegate

extension ChatMessageTableViewController: UITextViewDelegate {
    
    // MARK: - Instance Methods
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let replacedText: String
        
        inputTextViewPlaceholder.isHidden = !(text.isEmpty && textView.text.isEmpty)
        
        if !textView.text.isEmpty {
            replacedText = textView.text.replacingCharacters(in: Range(range, in: textView.text)!, with: text)
        } else {
            replacedText = text
        }
        
        sendTextButton.isEnabled = !replacedText.isEmpty
        
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        let color = textView.tintColor
        
        textView.tintColor = .clear
        textView.tintColor = color
    }
}

// MARK: - UIScrollViewDelegate

extension ChatMessageTableViewController: UIScrollViewDelegate {
    
    // MARK: - Instance Methods
    
    func scrollViewShouldScrollToTop(_ scrollView: UIScrollView) -> Bool {
        return false
    }
}

// MARK: - KeyboardHandler

extension ChatMessageTableViewController: ContainerKeyboardHandler {
    
    // MARK: - Instance Methods
    
    func handle(keyboardHeight: CGFloat, view: UIView) {
        bottomSpacerHeightConstraint.constant = keyboardHeight
        
        UIView.animate(withDuration: 0.25, animations: {
            self.view.layoutIfNeeded()
        })
    }
}

class EmptyCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: "EmptyCell")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
