//
//  ChatMessagesRoutingController.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 30.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//
// swiftlint:disable file_length
// swiftlint:disable type_body_length
// swiftlint:disable function_body_length
// swiftlint:disable vertical_whitespace
// swiftlint:disable multiple_closures_with_trailing_closure

// swiftlint:disable for_where
// swiftlint:disable redundant_discardable_let

import UIKit
import FriendstaTools
import PromiseKit

class ChatMessagesRoutingController: LoggedViewController {
    
    // MARK: - Nested Types
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let embedContent = "EmbedContent"
        static let sendChatPhoto = "SendChatPhoto"
        static let showChatPhoto = "ShowChatPhoto"
        static let showChatMask = "ShowChatMask"
        static let showChatUser = "ShowChatUser"
        static let unauthorize = "Unauthorize"
    }

    private enum Constants {

        // MARK: - Type Properties

        static let replyToPost = "Reply to post"
    }
    
    // MARK: - Instance Properties

    @IBOutlet private weak var avatarImageView: RectangularImageView!
    
    @IBOutlet private weak var avatarActivityIndicator: UIActivityIndicatorView!
    
    // MARK: -
    
    private var chatMessageTableViewController: ChatMessageTableViewController!
    
    // MARK: -
    
    public var feedsFromFeedVC = [Feed]()
    public var newFeed: Feed?
    
    private(set) var chat: Chat?
    private(set) var feed: Feed?
    private(set) var comment: Comment?
    
    private var potentialChatMember: User?
    
    private(set) var hasAppliedData = false
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onChatPhotoMakingFinished(segue: UIStoryboardSegue) {
        Log.high("onChatPhotoMakingFinished(withSegue: \(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction private func onChatPhotoMakingCancelled(segue: UIStoryboardSegue) {
        Log.high("onChatPhotoMakingCancelled(withSegue: \(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction private func onChatPhotoClosed(segue: UIStoryboardSegue) {
        Log.high("onChatPhotoClosed(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction private func onChatUserClosed(segue: UIStoryboardSegue) {
        Log.high("onChatUserClosed(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction private func onAvatarButtonTouchUpInside(_ sender: Any) {
        Log.high("onAvatarButtonTouchUpInside()", from: self)
        
        guard let chat = self.chat, let accountUserUID = Services.accountAccess?.userUID else {
            return
        }
        
        guard let chatMember = self.chat?.members?.first(where: { chatMember in
            return ((chatMember as! ChatMember).user?.uid != accountUserUID)
        }) as? ChatMember else {
            return
        }
        
        var isRevealed = true
        
        chat.members?.forEach({ (chatMember) in
            if let member = chatMember as? ChatMember, member.isDeanonymized == false {
                isRevealed = false
            }
        })
        
        if isRevealed {
            switch chatMember.user?.role {
            case .some(.ordinary), .some(.test):
                self.performSegue(withIdentifier: Segues.showChatUser, sender: chatMember.user)
                
            case .some(.bot), .none:
                break
            }
        } else {
            self.performSegue(withIdentifier: Segues.showChatMask, sender: chatMember)
        }
    }
    
    private func onEmbedContent(segue: UIStoryboardSegue, sender: Any?) {
        guard let chatMessageTableViewController = segue.destination as? ChatMessageTableViewController else {
            fatalError()
        }
        
        self.chatMessageTableViewController = chatMessageTableViewController
        
        self.chatMessageTableViewController.createdChat = { [unowned self] chat in
            self.chat = chat
        }
        
        self.chatMessageTableViewController.sendChatPhoto = { [unowned self] in
            self.performSegue(withIdentifier: Segues.sendChatPhoto, sender: (self.chat, self.feed, self.comment))
        }
        self.chatMessageTableViewController.showChatPhoto = { [unowned self] chatPhotoMessage in
            self.performSegue(withIdentifier: Segues.showChatPhoto, sender: chatPhotoMessage)
        }
        
        self.chatMessageTableViewController.showChatMask = { [unowned self] chatMember in
            self.performSegue(withIdentifier: Segues.showChatMask, sender: chatMember)
        }

        self.chatMessageTableViewController.showUser = { [unowned self] user in
            self.performSegue(withIdentifier: Segues.showChatUser, sender: user)
        }
        
        self.chatMessageTableViewController.unauthorize = { [unowned self] in
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
        }
    }
    
    private func onSendChatPhoto(segue: UIStoryboardSegue, sender: Any?) {
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }

        if let sender = sender as? (chat: Chat?, feed: Feed?, comment: Comment?) {
            dictionaryReceiver?.apply(dictionary: ["chat": sender.chat as Any,
                                                   "feed": sender.feed as Any,
                                                   "comment": sender.comment as Any,
                                                   "source": PhotoCreationSource.chat])
        }
    }
    
    private func onShowChatPhoto(segue: UIStoryboardSegue, sender: Any?) {
        guard let chatPhotoMessage = sender as? ChatPhotoMessage else {
            fatalError()
        }
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        dictionaryReceiver?.apply(dictionary: ["chatPhotoMessage": chatPhotoMessage])
    }
    
    private func onShowChatMask(segue: UIStoryboardSegue, sender: Any?) {
        guard let chatMember = sender as? ChatMember else {
            fatalError()
        }
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        dictionaryReceiver?.apply(dictionary: ["chatMember": chatMember])
    }
    
    private func onShowChatUser(segue: UIStoryboardSegue, sender: Any?) {
        guard let user = sender as? User else {
            fatalError()
        }
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        dictionaryReceiver?.apply(dictionary: ["user": user])
    }
    
    // MARK: -
    
    private func loadAvatarImage(from imageURL: URL) {
        self.avatarImageView.image = nil
        
        self.avatarActivityIndicator.startAnimating()

        Services.imageLoader.loadImage(for: imageURL, in: self.avatarImageView.imageTarget, completionHandler: { image in
            self.avatarActivityIndicator.stopAnimating()
        })
    }

    // MARK: -

    @objc private func dissmiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: -
    
    func apply(chat: Chat) {
        Log.high("apply(chat: \(String(describing: chat.uid)))", from: self)
        
        self.chat = chat
        
        if chatMessageTableViewController != nil {
            chatMessageTableViewController.feedsFromFeedVC = feedsFromFeedVC
        }
        
        if self.isViewLoaded {
            
            if let feed = newFeed {
                self.chatMessageTableViewController.apply(chat: chat, updateChatMessages: true, feed: feed)
            } else {
                self.chatMessageTableViewController.apply(chat: chat)
            }

            let firstMessage = Services.cacheViewContext.chatPhotoMessagesManager.last(withChatUID: chat.uid)

            self.title = Constants.replyToPost.localized()

            if let firstMessage = firstMessage, let photoURL = firstMessage.smallPhotoURL {
                if let imageData = firstMessage.smallPhotoData, let image = UIImage(data: imageData) {
                    self.avatarImageView.image = image
                } else {
                    self.loadAvatarImage(from: photoURL)
                }
            }

            self.title = "Reply to post".localized()
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }

    func apply(feed: Feed, potentialChatMember: User) {
        Log.high("apply(feed: \(String(describing: feed.uid)))", from: self)

        self.feed = feed
        self.potentialChatMember = potentialChatMember
        
        if chatMessageTableViewController != nil {
            chatMessageTableViewController.feedsFromFeedVC = feedsFromFeedVC
        }
        
        if self.isViewLoaded {
           if let chat = feed.chat {
            
            ///// ********* Lance's Code ********* /////
                self.chatMessageTableViewController.apply(chat: chat, updateChatMessages: true, feed: feed)
            ///// ********* Lance's Code ********* /////
            
            // self.chatMessageTableViewController.apply(chat: chat)
            
            } else {
                self.chatMessageTableViewController.apply(feed: feed, potentialChatMember: potentialChatMember)
            }
            self.title = "Reply to post".localized()
            if let imageUrl = self.feed?.photo?.smallImageURL {
                self.avatarImageView.isHidden = false
                self.loadAvatarImage(from: imageUrl)
            } else {
                self.avatarImageView.isHidden = true
            }
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }

    func apply(comment: Comment, for feed: Feed, potentialChatMember: User) {
        Log.high("apply(comment: \(String(describing: comment.uid)))", from: self)

        self.comment = comment
        self.feed = feed
        self.potentialChatMember = potentialChatMember
        
        if chatMessageTableViewController != nil {
            chatMessageTableViewController.feedsFromFeedVC = feedsFromFeedVC
        }
        
        if self.isViewLoaded {
            if let chat = comment.chat {
                self.chatMessageTableViewController.apply(chat: chat)
            } else {
                self.chatMessageTableViewController.apply(comment: comment, for: feed, potentialChatMember: potentialChatMember)
            }

            self.navigationItem.setRightBarButton(UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(dissmiss)), animated: false)

            self.title = "Reply to comment".localized()

            if let imageUrl = self.feed?.photo?.smallImageURL {
                self.avatarImageView.isHidden = false
                self.loadAvatarImage(from: imageUrl)
            } else {
                self.avatarImageView.isHidden = true
            }

            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }

    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if !self.hasAppliedData {
            if let chat = self.chat {
                self.apply(chat: chat)
            } else if let feed = self.feed, let chatMember = self.potentialChatMember, self.comment == nil {
                self.apply(feed: feed, potentialChatMember: chatMember)
            } else if let comment = self.comment, let chatMember = self.potentialChatMember, let feed = self.feed {
                self.apply(comment: comment, for: feed, potentialChatMember: chatMember)
            }
        }
        
        self.navigationController?.setNavigationBarHidden(false, animated: animated)

        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.hasAppliedData = false
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case Segues.embedContent:
            self.onEmbedContent(segue: segue, sender: sender)
        
        case Segues.sendChatPhoto:
            self.onSendChatPhoto(segue: segue, sender: sender)
            
        case Segues.showChatPhoto:
            self.onShowChatPhoto(segue: segue, sender: sender)
            
        case Segues.showChatMask:
            self.onShowChatMask(segue: segue, sender: sender)
            
        case Segues.showChatUser:
            self.onShowChatUser(segue: segue, sender: sender)
            
        default:
            break
        }
    }
}

// MARK: - DictionaryReceiver

extension ChatMessagesRoutingController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        if let chat = dictionary["chat"] as? Chat {
            
            //self.apply(chat: chat)
            
            if let accountUserUID = Services.accountAccess?.userUID {
                let chatMember = chat.members?.first(where: { chatMember in
                    return ((chatMember as! ChatMember).user?.uid != accountUserUID)
                }) as? ChatMember
                if let potentialChatMember = chatMember, let potentialChatMemberUser = potentialChatMember.user {
                    if potentialChatMemberUser.uid != accountUserUID {
                        self.potentialChatMember = potentialChatMemberUser
                    }
                    
                    print(accountUserUID )
                    print(potentialChatMemberUser.uid)
                }
                
                if let feedsFromFeedVC = dictionary["feedsFromFeedVC"] as? [Feed] {
                    
                    self.feedsFromFeedVC = feedsFromFeedVC
                    
                    for feed in feedsFromFeedVC {
                        
                        print("-----\(chat.uid)")
                        
                        print("+++++\(feed.uid)")
                        
                        print(".....\(feed.chat)")
                        
                        if feed.chat?.uid == chat.uid {
                            
                            newFeed = feed
                            apply(chat: chat)
                            return
                        }
                    }
                }
                
                let chatLastMessage: ChatMessage? = Services.cacheViewContext.chatMessagesManager.last(withChatUID: chat.uid)
                
                switch chatLastMessage {
                case _ as ChatTextMessage:
                    print("chatLastTextMessage")
                    
                    let feedListType: FeedListType = .all
                    let promise = (feedListType == .all) ? Services.feedService.fetchFeed() : Services.feedService.fetchDiscover()

                    firstly {
                        promise
                    }.done { [weak self](feedList) in
                        let feeds = feedList.allFeeds
                        
                        self?.feedsFromFeedVC = feeds
                        
                        for feed in feeds {
                            
                            print("-----\(chat.uid)")
                            
                            print("+++++\(feed.uid)")
                            
                            print(".....\(feed.chat)")
                            
                            if feed.chat?.uid == chat.uid {
                                
                                self?.newFeed = feed
                                break
                            }
                        }
                        
                        self?.apply(chat: chat)
                        
                    }.catch { error in
                        print(error.localizedDescription)
                    }
                    
                case is ChatPhotoMessage:
                    print("ChatPhotoMessage")
                    self.apply(chat: chat)
                case _ as ChatInfoMessage:
                    print("chatLastInfoMessage")
                    self.apply(chat: chat)
                default:
                    print("nothing")
                    self.apply(chat: chat)
                }
                
            } else {
                self.apply(chat: chat)
            }
        }
    }
}
