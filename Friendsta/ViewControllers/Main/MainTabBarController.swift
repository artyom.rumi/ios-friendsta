//
//  MainTabBarController.swift
//  Friendsta
//
//  Created by Marat Galeev on 12.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class MainTabBarController: UITabBarController {
    
     // MARK: - Type Properties
    
    static let storyboardIdentifier = "MainTabBarController"
    
    // MARK: - Nested Types
    
    private enum Images {
        
        // MARK: - Type Properties
        
        static let mainMenuTabBarIconImage = UIImage(named: "MainMenuTabBarIcon")
    }
    
    // MARK: -
    
    private enum Segues {
        
        // MARK: - Type Properties
        
        static let showNotifications = "ShowNotifications"
        static let unauthorize = "Unauthorize"
        static let showActionSheet = "ShowActionSheet"
    }

    // MARK: -
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let isUserHasSeenNotificationsScreenKey = "UserHasSeenNotificationsScreen"
        
        static let tabBarAvatarImageSize = CGSize(width: 36.0, height: 36.0)
        
        static let avatarMediumPlaceholderImage = UIImage(named: "AvatarMediumPlaceholder")?.scaled(to: Constants.tabBarAvatarImageSize)?.withRenderingMode(.alwaysOriginal)
    }
    
    // MARK: -
    
    enum Tab: Int {
        
        // MARK: - Enumeration Cases

        case feed
        case people
        case camera
        case notifications
        case profile
    }
    
    // MARK: - Instance Properties
    
    private var transitionAnimation: CustomSendPropTransition!
    
    private var badgesTimer: Timer?
    
    // MARK: -
    
    var onCameraTabSelected: (() -> Void)?
    var onFeedTabReselected: (() -> Void)?
    var onFeedTabDeselected: (() -> Void)?
    var onFeedTabSelected: (() -> Void)?

    // MARK: - UITabBarController

    override var selectedIndex: Int {
        didSet {
            guard let tab = Tab(rawValue: self.selectedIndex) else {
                return
            }

            switch tab {
            case .feed:
                self.onFeedTabSelected?()

            case .camera:
                self.onCameraTabSelected?()

            default:
                break
            }
        }
    }
    
    // MARK: - Initializers
    
    deinit {
        Services.apiChatClient.stopObserving()

        self.unsubscribeFromPropCardsEvents()
        self.unsubscribeFromChatMessagesEvents()

        self.unsubscribeFromAccessEvents()
        
        self.unsubscribeFromDeviceTokenEvents()
        self.unsubscribeFromNotificationsEvents()
    }
    
    // MARK: - Instance Methods
    
    @IBAction private func onAuthorizationFinished(segue: UIStoryboardSegue) {
        Log.high("onAuthorizationFinished(withSegue: \(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction private func onMainMenuClosed(segue: UIStoryboardSegue) {
        Log.high("onMainMenuClosed(withSegue: \(String(describing: segue.identifier)))", from: self)
        
        if self.selectedIndex == Tab.feed.rawValue {
            self.onFeedTabSelected?()
        }
    }
    
    @objc private func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.refreshPropCards(forced: false)
        self.refreshActivities(forced: false)
        self.refreshAllChatList(forced: true)
        self.registerForNotifications()
        self.checkContactListChanged()
    }
    
    // MARK: -

    private func updateNotificationsBadge(context: CacheModelContext) -> Int {
        var activitiesCount: Int = 0
        
        if Services.accountAccess?.isValid ?? false {
            let activities = context.activityManager.fetch()
            
            activitiesCount = activities.reduce(0, { result, activity in
                return result + (activity.isUnread ? 1 : 0)
            })
        } else {
            activitiesCount = 0
        }
        
        DispatchQueue.main.async(execute: {
            if activitiesCount > 0 {
                self.viewControllers?[Tab.notifications.rawValue].tabBarItem.badgeValue = String(activitiesCount)
            } else {
                self.viewControllers?[Tab.notifications.rawValue].tabBarItem.badgeValue = nil
            }
        })
        
        return activitiesCount
    }
    
    private func updateChatMessagesBadge(context: CacheModelContext) -> Int {
        var chatMessagesCount: Int = 0
        
        guard let accountUserUID = Services.accountUser?.uid else {
            return 0
        }
        
        if Services.accountAccess?.isValid ?? false {
            let chatMessages = context.chatMessagesManager.fetch()
            
            chatMessagesCount = chatMessages.reduce(0, { result, message in
                if accountUserUID != message.userUID && !message.isErased {
                    return result + (message.isViewed ? 0 : 1)
                } else {
                    return result
                }
            })
        } else {
            chatMessagesCount = 0
        }
        
        DispatchQueue.main.async {
            if let navigationViewController = self.viewControllers?[MainTabBarController.Tab.feed.rawValue] as? UINavigationController, let feedViewController = navigationViewController.viewControllers.first as? FeedViewController {
                feedViewController.updateChatMessagesBadge(number: chatMessagesCount)
            }
        }
        
        return chatMessagesCount
    }
    
    private func updateBadges() {
        let backgroundContext = Services.cacheViewContext.createPrivateQueueChildContext()
        
        backgroundContext.perform(block: {
            let notificationsCount = self.updateNotificationsBadge(context: backgroundContext)
            
            let chatMessagesCount = self.updateChatMessagesBadge(context: backgroundContext)

            let applicationBadgeNumber = notificationsCount + chatMessagesCount

            if let userDefaults = UserDefaults(suiteName: Keys.applicationGroupID) {
                userDefaults.set(applicationBadgeNumber, forKey: Keys.storageApplicationBadgeNumberKey)
            }
            
            DispatchQueue.main.async(execute: {
                UIApplication.shared.applicationIconBadgeNumber = applicationBadgeNumber
            })
        })
        
        self.resetBadgesTimer()
    }

    private func resetBadgesTimer() {
        self.badgesTimer?.invalidate()
        self.badgesTimer = nil
    }
    
    private func restartBadgesTimer() {
        self.resetBadgesTimer()
        
        self.badgesTimer = Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false, block: { [weak self] timer in
            self?.updateBadges()
        })
    }
    
    private func subscribeToPropCardsEvents() {
        self.unsubscribeFromPropCardsEvents()
        
        let propCardsManager = Services.cacheViewContext.propCardsManager
        
        propCardsManager.objectsChangedEvent.connect(self, handler: { [weak self] propCards in
            self?.restartBadgesTimer()
        })
        
        propCardsManager.startObserving()
    }
    
    private func unsubscribeFromPropCardsEvents() {
        Services.cacheViewContext.propCardsManager.objectsChangedEvent.disconnect(self)
    }
    
    private func subscribeToActivitiesEvents() {
        self.unsubscribeFromActivitiesEvents()
        
        let activityManager = Services.cacheViewContext.activityManager
        
        activityManager.objectsChangedEvent.connect(self, handler: { [weak self] activities in
            self?.restartBadgesTimer()
        })
        
        activityManager.startObserving()
    }
    
    private func unsubscribeFromActivitiesEvents() {
        Services.cacheViewContext.activityManager.objectsChangedEvent.disconnect(self)
    }

    private func subscribeToChatMessagesEvents() {
        self.unsubscribeFromChatMessagesEvents()

        let chatMessagesManager = Services.cacheViewContext.chatMessagesManager

        chatMessagesManager.objectsChangedEvent.connect(self, handler: { [weak self] сhatMessages in
            self?.restartBadgesTimer()
        })

        chatMessagesManager.startObserving()
    }

    private func unsubscribeFromChatMessagesEvents() {
        Services.cacheViewContext.chatMessagesManager.objectsChangedEvent.disconnect(self)
    }
    
    private func subscribeToAccessEvents() {
        self.unsubscribeFromAccessEvents()
        
        Services.accountProvider.model.accessManager.accessResetedEvent.connect(self, handler: { [weak self] in
            self?.unregisterForNotifications()
        })
    }
    
    private func unsubscribeFromAccessEvents() {
        Services.accountProvider.model.accessManager.accessResetedEvent.disconnect(self)
    }
    
    private func subscribeToDeviceTokenEvents() {
        self.unsubscribeFromDeviceTokenEvents()
        
        Services.accountProvider.model.deviceTokenManager.deviceTokenUpdatedEvent.connect(self, handler: { deviceToken in
            self.updateDeviceToken()
        })
        
        Services.accountProvider.model.deviceTokenManager.deviceTokenResetedEvent.connect(self, handler: {
            self.updateDeviceToken()
        })
    }
    
    private func unsubscribeFromDeviceTokenEvents() {
        Services.accountProvider.model.deviceTokenManager.deviceTokenUpdatedEvent.disconnect(self)
        Services.accountProvider.model.deviceTokenManager.deviceTokenResetedEvent.disconnect(self)
    }
    
    private func subscribeToNotificationsEvents() {
        Services.accountProvider.model.notificationsManager.notificationReceivedEvent.disconnect(self)
        
        Services.accountProvider.model.notificationsManager.notificationReceivedEvent.connect(self, handler: { notification in
            Services.accountProvider.model.notificationsManager.clearNotifications()
            
            switch notification.action {
            case .some(.open):
                switch notification.category {
                case .propCard, .pokeReply:
                    self.selectedIndex = Tab.notifications.rawValue

                case .newUser, .newFriend:
                    self.selectedIndex = Tab.profile.rawValue

                case .chatMessage(let chatUID, let senderUID, let receiverUID):
                    break
                }
                
            case .none:
                switch notification.category {
                case .newUser:
                    self.refreshCommunityList(forced: true)
                    
                case .newFriend:
                    self.refreshFriendList(forced: true)
                    
                case .propCard:
                    self.refreshPropCards(forced: true)

                case .pokeReply:
                    self.refreshPropReactions(forced: true)

                case .chatMessage:
                    self.refreshAllChatList(forced: true)
                }
            }
        })
    }

    private func unsubscribeFromNotificationsEvents() {
        Services.accountProvider.model.notificationsManager.notificationReceivedEvent.disconnect(self)
    }
    
    // MARK: -
    
    private func handle(silentError error: Error) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
            
        default:
            break
        }
    }
    
    private func refreshPropCards(forced: Bool) {
        Log.high("refreshPropCards()", from: self)
        
        if forced || (self.selectedIndex != Tab.notifications.rawValue) {
            firstly {
                Services.propCardsService.refreshPropCards()
            }.catch { error in
                self.handle(silentError: error)
            }
        }
    }
    
    private func refreshActivities(forced: Bool) {
        Log.high("refreshActivities(forced: \(forced))", from: self)
        
        if forced || (self.selectedIndex != Tab.notifications.rawValue) {
            firstly {
                Services.activityService.refreshAllActivities()
            }.catch { error in
                self.handle(silentError: error)
            }
        }
    }

    private func refreshAllChatList(forced: Bool) {
        Log.high("refreshAllChatList()", from: self)

        if forced {
            firstly {
                Services.chatMasksService.refreshChatMasks()
            }.then { chatMasks in
                Services.chatsService.refreshAllChatList()
            }.catch { error in
                self.handle(silentError: error)
            }
        }
    }

    private func refreshPropReactions(forced: Bool) {
        Log.high("refreshPropReactions()", from: self)

        if forced || (self.selectedIndex != Tab.notifications.rawValue) {
            firstly {
                Services.propCardsService.refreshPropCardsWithReactions()
            }.catch { error in
                self.handle(silentError: error)
            }
        }
    }
    
    private func refreshFriendList(forced: Bool) {
        Log.high("refreshFriendsList()", from: self)
        
        if forced || (self.selectedIndex != Tab.people.rawValue) {
            firstly {
                Services.usersService.refreshFriends()
            }.catch { error in
                self.handle(silentError: error)
            }
        }
    }
  
    private func checkContactAccess() {
        Log.high("checkContactAccess()", from: self)
        if Services.accountUser == nil {
            switch Services.contactsService.accessState {
            case .notDetermined:
                self.requestContactsAccess()
                
            case .authorized, .denied, .restricted:
                break
            }
        }
    }
    
    private func refreshCommunityList(forced: Bool) {
        Log.high("refreshCommunityList()", from: self)
        
        if forced || (self.selectedIndex != Tab.people.rawValue) {
            firstly {
                Services.usersService.refreshInvitationInlineContacts()
            }.then { userList in
                Services.usersService.refreshInvitationSchoolmates()
            }.then { userList in
                Services.outlineContactsService.refreshOutlineContacts()
            }.catch { error in
                self.handle(silentError: error)
            }
        }
    }
  
    private func requestContactsAccess() {
        Log.high("requestContactsAccess()", from: self)
        
        firstly {
            Services.contactsService.requestAccess()
        }.done { accessState in
            switch accessState {
            case .notDetermined:
                self.requestContactsAccess()
                
            case .authorized, .denied, .restricted:
                break
            }
        }
    }
    
    private func updateDeviceToken() {
        Log.high("updateDeviceToken()", from: self)
        
        firstly {
            Services.notificationsService.update(deviceToken: Services.accountDeviceToken)
        }.catch { error in
            self.handle(silentError: error)
        }
    }
    
    private func registerForNotifications() {
        Log.high("registerForNotifications()", from: self)
        
        firstly {
            Services.notificationsService.requestAccess()
        }.done { accessState in
            switch accessState {
            case .notDetermined, .denied:
                break
                
            case .authorized:
                if !Services.notificationsService.isRegisteredForNotifications {
                    Services.notificationsService.registerForNotifications()
                }
            }
        }
    }
    
    private func unregisterForNotifications() {
        Log.high("unregisterForNotifications()", from: self)
        
        if Services.notificationsService.isRegisteredForNotifications {
            Services.notificationsService.unregisterForNotifications()
        }
    }
    
    private func checkContactListChanged() {
        Log.high("checkContactsChanged()", from: self)
        
        let cashContactList = Services.cacheViewContext.contactsManager.fetch()
        var currentContactList: [ContactListItem] = []
        
        if Services.contactsService.accessState == .authorized {
            firstly {
                Services.contactsService.refreshAllContacts()
            }.done { contactList in
                if let list = contactList {
                    currentContactList = list
                }
                
                var cashContactPhoneNumberList = cashContactList.compactMap({ (contactItem) in
                    return contactItem.phoneNumber
                })
                
                let currentContacts = currentContactList.compactMap { (contactListItem) in
                    return contactListItem.contact
                }
        
                var currentContactPhoneNumberList = currentContacts.compactMap { (contact) in
                    return contact.phoneNumber
                }
                
                cashContactPhoneNumberList.sort(by: { (firstNumber, secondNumber) -> Bool in
                    return firstNumber > secondNumber
                })
                
                currentContactPhoneNumberList.sort(by: { (firstNumber, secondNumber) -> Bool in
                    return firstNumber > secondNumber
                })
                
                if cashContactPhoneNumberList != currentContactPhoneNumberList {
                    firstly {
                        Services.usersService.refreshContacts(with: currentContacts)
                    }.cauterize()
                }
            }
        }
    }

    @objc private func setupTabBar() {
        if let profileTabItem = self.tabBar.items?[Tab.profile.rawValue] {
            profileTabItem.image = Constants.avatarMediumPlaceholderImage
            profileTabItem.selectedImage = Constants.avatarMediumPlaceholderImage?.roundedImageWithBorder(width: 1.0, color: UIColor.black)

            if let accountUser = Services.accountUser {
                self.loadAvatar(accountUser: accountUser, into: profileTabItem)
            } else {
                firstly {
                    Services.accountUserService.refreshAccountUser()
                }.done { accountUser in
                    self.loadAvatar(accountUser: accountUser, into: profileTabItem)
                }.cauterize()
            }
        }

        if let cameraTabItem = self.tabBar.items?[Tab.camera.rawValue] {
            cameraTabItem.image = Images.mainMenuTabBarIconImage
            cameraTabItem.selectedImage = Images.mainMenuTabBarIconImage
            cameraTabItem.tag = Tab.camera.rawValue
        }
    }

    // MARK: -
    
    func loadAvatar(accountUser: AccountUser, into profileTab: UITabBarItem) {
        if let imageURL = accountUser.mediumAvatarURL ?? accountUser.mediumAvatarPlaceholderURL {
            Services.imageLoader.loadImage(for: imageURL, completionHandler: { image in
                let image = image?.scaled(to: Constants.tabBarAvatarImageSize)?.roundedImage?.withRenderingMode(.alwaysOriginal)

                self.tabBar.items?[Tab.profile.rawValue].image = image ?? Constants.avatarMediumPlaceholderImage
                self.tabBar.items?[Tab.profile.rawValue].selectedImage = image?.roundedImageWithBorder(width: 1.0, color: Colors.black) ?? Constants.avatarMediumPlaceholderImage?.roundedImageWithBorder(width: 1.0, color: Colors.black)
            })
        }
    }

    // MARK: -

    func showFeedLoadingState() {
        let feedTabBarItem = self.tabBar.items?[Tab.feed.rawValue]

        feedTabBarItem?.selectedImage = UIImage(named: "RefreshTabBarIcon")
        feedTabBarItem?.image = UIImage(named: "RefreshTabBarIcon")

        let feedItemView = self.tabBar.subviews[5]

        guard let feedItemImageView = feedItemView.subviews.first as? UIImageView else {
            return
        }

        feedItemImageView.transform = .identity

        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: [.repeat, .curveEaseOut], animations: {
            feedItemImageView.transform = CGAffineTransform(rotationAngle: .pi)
        })
    }

    func hideFeedLoadingState() {
        let feedItemView = self.tabBar.subviews[5]

        guard let feedItemImageView = feedItemView.subviews.first as? UIImageView else {
            return
        }

        feedItemImageView.layer.removeAllAnimations()

        let feedTabBarItem = self.tabBar.items?[Tab.feed.rawValue]

        feedTabBarItem?.selectedImage = UIImage(named: "FeedTabBarIcon")
        feedTabBarItem?.image = UIImage(named: "FeedTabBarIcon")
    }

    func scrollFeedToTop() {
        Log.high("scrollFeedToTop()", from: self)

        if let navigationViewController = self.viewControllers?[MainTabBarController.Tab.feed.rawValue] as? UINavigationController, let feedViewController = navigationViewController.viewControllers.first as? FeedViewController {
            feedViewController.scrollToTop()
        }
    }
    
    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Services.apiChatClient.startObserving()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupTabBar()
        
        if let accountAccess = Services.accountAccess, accountAccess.isValid && accountAccess.hasUser {
            self.refreshPropCards(forced: true)
            self.refreshActivities(forced: true)
            self.refreshPropReactions(forced: true)
            self.refreshAllChatList(forced: true)
            self.checkContactListChanged()
            
            self.restartBadgesTimer()
        }
        
        self.subscribeToPropCardsEvents()
        self.subscribeToChatMessagesEvents()
        self.subscribeToActivitiesEvents()
        
        self.subscribeToAccessEvents()
        
        self.subscribeToDeviceTokenEvents()
        self.subscribeToNotificationsEvents()
        
        self.delegate = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onApplicationWillEnterForeground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.setupTabBar),
                                               name: .profileAvatarUpdated,
                                               object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let accountAccess = Services.accountAccess, accountAccess.isValid && accountAccess.hasUser {
            self.checkContactAccess()
            self.registerForNotifications()
        } else {
            self.performSegue(withIdentifier: Segues.unauthorize, sender: self)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unsubscribeFromPropCardsEvents()
        self.unsubscribeFromChatMessagesEvents()
        self.unsubscribeFromActivitiesEvents()
        
        self.unsubscribeFromAccessEvents()
        
        self.unsubscribeFromDeviceTokenEvents()
        self.unsubscribeFromNotificationsEvents()
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .profileAvatarUpdated, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if !(Services.accountAccess?.isValid ?? false) {
            self.selectedIndex = Tab.feed.rawValue
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let dictionaryReceiver: DictionaryReceiver?
        
        if let navigationController = segue.destination as? UINavigationController {
            dictionaryReceiver = navigationController.viewControllers.first as? DictionaryReceiver
        } else {
            dictionaryReceiver = segue.destination as? DictionaryReceiver
        }
        
        switch segue.identifier {
        case Segues.showNotifications:
            guard let accountUser = sender as? AccountUser else {
                return
            }
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["accountUser": accountUser])
            }
            
        case Segues.showActionSheet:
            guard let destination = segue.destination as? ActionSheetViewController else {
                return
            }
            
            guard let senderData = sender as? (PropListItem, UIView, Bool) else {
                return
            }
            
            self.transitionAnimation = CustomSendPropTransition(animatedView: senderData.1, isNeedScale: true)
            
            destination.transitioningDelegate = self.transitionAnimation
            
            if let dictionaryReceiver = dictionaryReceiver {
                dictionaryReceiver.apply(dictionary: ["propListItem": senderData.0,
                                                      "isFriendOrSuperFriend": senderData.2])
            }
            
        default:
            break
        }
    }
    
    // MARK: - UITabBarDelegate

    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if self.selectedIndex == Tab.camera.rawValue && item.tag == Tab.camera.rawValue {
            self.onCameraTabSelected?()
        } else if selectedIndex == Tab.feed.rawValue && item.tag == Tab.feed.rawValue {
            self.onFeedTabReselected?()
        }

        if item.tag == Tab.feed.rawValue {
            self.onFeedTabSelected?()
        } else {
            self.onFeedTabDeselected?()
        }
    }
}

// MARK: - UITabBarControllerDelegate

extension MainTabBarController: UITabBarControllerDelegate {
    
    // MARK: - Instance Methods
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is MainMenuViewController {
            if let menuViewController = UIStoryboard(name: MainMenuViewController.storyboardName, bundle: .main).instantiateInitialViewController() {
                tabBarController.present(menuViewController, animated: true)
                                
                return false
            }
        }
    
        return true
    }
}
