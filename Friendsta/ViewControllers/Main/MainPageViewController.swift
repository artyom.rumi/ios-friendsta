//
//  MainPageViewController.swift
//  Friendsta
//
//  Created by Nikita Asabin on 8/21/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class MainPageViewController: UIPageViewController {

    // MARK: - Nested Types

    private enum Constants {

        // MARK: - Type Properties
        static let textFeedViewControllerIndex = 0
        static let takePhotoViewControllerIndex = 1
        static let mainTabBarIndex = 2
        static let chatsViewControllerIndex = 3
    }
    
    // MARK: -
    
    enum Controller {
        
        // MARK: - Type Properties
        case textFeedViewController
        case mainTabBarController
        case makePhotoViewController
        case chatsViewController
    }

    // MARK: - Instance Properties
    public var feedsFromFeedVC = [Feed]()
    
    private var currentViewControllerIndex = 1
    private var existingViewControllers: [UIViewController] = []
    
    private var mainTabBarController: MainTabBarController!
    private var makePhotoViewController: MakePhotoViewController!
    private var chatsViewController: ChatTableViewController!
    private var chatsNavigationController: UINavigationController!
    private var textFeedViewController: TextFeedViewController!

    // MARK: - Instance Methods
    
    private func setupViewControllers() {
        guard let mainTabBarController = self.storyboard?.instantiateViewController(withIdentifier: MainTabBarController.storyboardIdentifier) as? MainTabBarController else {
            return
        }
        
        guard let makePhotoViewController = self.storyboard?.instantiateViewController(withIdentifier: MakePhotoViewController.storyboardIdentifier) as? MakePhotoViewController else {
            return
        }
        
        guard let chatsViewController = UIStoryboard(name: "Chats", bundle: Bundle.main).instantiateInitialViewController() as? ChatTableViewController else {
            return
        }

        guard let textFeedViewController = UIStoryboard(name: "TextFeed", bundle: Bundle.main).instantiateInitialViewController() as? TextFeedViewController else {
            return
        }
        
        self.mainTabBarController = mainTabBarController
        self.makePhotoViewController = makePhotoViewController
        self.chatsViewController = chatsViewController
        self.chatsNavigationController = TransparentNavigationController(rootViewController: chatsViewController)
        self.textFeedViewController = textFeedViewController

        self.mainTabBarController.onFeedTabSelected = {
            self.setScroll(enable: true)
        }

        self.mainTabBarController.onFeedTabDeselected = {
            self.setScroll(enable: false)
        }

        self.textFeedViewController.onSendTextFeedFinished = {
            self.setViewControllers([self.mainTabBarController], direction: .forward, animated: true, completion: nil)
            self.currentViewControllerIndex = Constants.mainTabBarIndex
        }
        
        self.textFeedViewController.shouldShowKeyboardOnViewWillAppear = false

        self.makePhotoViewController.onBack = {
            self.setViewControllers([self.mainTabBarController], direction: .forward, animated: true, completion: nil)
            self.currentViewControllerIndex = Constants.mainTabBarIndex
        }

        self.makePhotoViewController.onWriteAPostTapped = {
            self.setViewControllers([self.textFeedViewController], direction: .reverse, animated: true, completion: { (completed) in
                if completed {
                    self.currentViewControllerIndex = Constants.textFeedViewControllerIndex
                }
            })
        }

        self.chatsViewController.onBackTapped = {
            self.feedsFromFeedVC = [Feed]()
            self.setViewControllers([self.mainTabBarController], direction: .reverse, animated: true, completion: nil)
            self.currentViewControllerIndex = Constants.mainTabBarIndex
        }

        self.chatsViewController.onChatOpened = {
            self.setScroll(enable: false)
        }

        self.chatsViewController.onChatClosed = {
            self.setScroll(enable: true)
        }
        self.chatsNavigationController.navigationBar.tintColor = .black

        self.setViewControllers([self.mainTabBarController], direction: .reverse, animated: false, completion: nil)
        self.existingViewControllers = [self.textFeedViewController, self.makePhotoViewController, self.mainTabBarController, self.chatsNavigationController]
        self.dataSource = self
        self.delegate = self
    }

    private func setupBounce() {
        for subview in self.view.subviews {
            if let scrollView = subview as? UIScrollView {
                scrollView.delegate = self
                break
            }
        }
    }

    private func setScroll(enable: Bool) {
        for subview in self.view.subviews {
            if let scrollView = subview as? UIScrollView {
                scrollView.isScrollEnabled = enable
                
                break
            }
        }
    }
    
    // MARK: -
    
    func show(controller: Controller) {
        switch controller {
        case .chatsViewController:
            chatsViewController.feedsFromFeedVC = feedsFromFeedVC
            self.setViewControllers([self.chatsNavigationController], direction: .forward, animated: true)
            
        case .makePhotoViewController:
            self.setViewControllers([self.makePhotoViewController], direction: .reverse, animated: true)
            
        default:
            break
        }
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViewControllers()
        self.setupBounce()
    }
}

// MARK: - UIPageViewControllerDataSource

extension MainPageViewController: UIPageViewControllerDataSource {

    // MARK: - Instance Methods

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let index = self.existingViewControllers.firstIndex(of: viewController)
        
        switch index {
        case Constants.textFeedViewControllerIndex:
            return self.existingViewControllers[Constants.takePhotoViewControllerIndex]

        case Constants.takePhotoViewControllerIndex:
            return self.existingViewControllers[Constants.mainTabBarIndex]
            
        case Constants.mainTabBarIndex:
            return self.existingViewControllers[Constants.chatsViewControllerIndex]
            
        case Constants.chatsViewControllerIndex:
            return nil
            
        default:
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let index = self.existingViewControllers.firstIndex(of: viewController)
        
        switch index {
        case Constants.textFeedViewControllerIndex:
            return nil

        case Constants.takePhotoViewControllerIndex:
            return self.existingViewControllers[Constants.textFeedViewControllerIndex]
            
        case Constants.mainTabBarIndex:
            return self.existingViewControllers[Constants.takePhotoViewControllerIndex]
            
        case Constants.chatsViewControllerIndex:
            return self.existingViewControllers[Constants.mainTabBarIndex]
            
        default:
            return nil
        }
    }
}

// MARK: - UIPageViewControllerDelegate

extension MainPageViewController: UIPageViewControllerDelegate {

    // MARK: - Instance Methods

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard completed else {
            return
        }

        if let currentViewController = pageViewController.viewControllers?.first {
            let index = self.existingViewControllers.firstIndex(of: currentViewController)
            
            self.currentViewControllerIndex = index ?? Constants.mainTabBarIndex
        }
    }
}

// MARK: - UIScrollViewDelegate

extension MainPageViewController: UIScrollViewDelegate {

    // MARK: - Instance Methods

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.currentViewControllerIndex == Constants.textFeedViewControllerIndex && scrollView.contentOffset.x < scrollView.bounds.size.width) {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if (self.currentViewControllerIndex == Constants.chatsViewControllerIndex && scrollView.contentOffset.x > scrollView.bounds.size.width) {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
        
        if (self.currentViewControllerIndex == Constants.textFeedViewControllerIndex && scrollView.contentOffset.x == self.textFeedViewController.view.frame.width) {
            self.textFeedViewController.showKeyboard()
        }
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if (self.currentViewControllerIndex == Constants.textFeedViewControllerIndex && scrollView.contentOffset.x <= scrollView.bounds.size.width) {
            targetContentOffset.pointee = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if (self.currentViewControllerIndex == Constants.chatsViewControllerIndex && scrollView.contentOffset.x >= scrollView.bounds.size.width) {
            targetContentOffset.pointee = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }
}
