//
//  PropTableViewController.swift
//  Friendsta
//
//  Created by Marat Galeev on 10.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit
import PromiseKit
import SVProgressHUD
import FriendstaTools
import FriendstaNetwork

class PropTableViewController: LoggedViewController, ErrorMessagePresenter {
    
    // MARK: - Nested Types
    
    enum FilterType {
        
        // MARK: - Enumeration Cases
        
        case none
        case search(text: String)
    }
    
    // MARK: -
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let customPropTableCellIdentifier = "CustomPropTableCell"
        static let propTableCellIdentifier = "PropTableCell"
        static let pullDownToShuffleLabelCellIdentifier = "PullDownToShuffleLabelCell"
        
        static let customPropTableSection = 0
        static let propsTableSection = 1
        static let pullDownToShuffleLabelTableSection = 2
        
        static let popularPropsSegmentIndex = 0
        static let recentPropsSegmentIndex = 1
        static let allPropsSegmentIndex = 2
    }
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate weak var searchTextField: SearchTextField!
    
    @IBOutlet fileprivate weak var emptyStateContainerView: UIView!
    @IBOutlet fileprivate weak var emptyStateView: EmptyStateView!
    
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    fileprivate weak var tableRefreshControl: UIRefreshControl!
    
    // MARK: -
    
    fileprivate var propListTimer: Timer?
    
    // MARK: -
    
    fileprivate(set) var propReceiver: PropReceiver?
    
    fileprivate(set) var propListType: PropListType = .all
    
    fileprivate(set) var filterType: FilterType = .none
    
    fileprivate(set) var propList: [PropListItem] = []
    fileprivate(set) var propFullList: [PropListItem] = []
    
    fileprivate(set) var selectedPropListItem: PropListItem?
    fileprivate(set) var selectedBoostType: BoostType?
    
    fileprivate(set) var isRefreshingData = false
    fileprivate(set) var hasAppliedData = false
    fileprivate(set) var shouldSendProp = false
    
    var sendCustomProp: (() -> Void)?
    var finishPropSending: (() -> Void)?
    var unauthorize: (() -> Void)?
    
    // MARK: - UIViewController
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Initializers
    
    deinit {
        self.unsubscribeFromPropListEvents()
        self.unsubscribeFromKeyboardNotifications()
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onActionSheetClosed(segue: UIStoryboardSegue) {
        Log.high("onActionSheetClosed(\(String(describing: segue.identifier)))", from: self)
    }
    
    @IBAction fileprivate func onViewTapped(_ sender: Any) {
        Log.high("onViewTapped()", from: self)
        
        self.view.endEditing(true)
    }
    
    @IBAction fileprivate func onSearchTextFieldChanged(_ sender: Any) {
        Log.high("onSearchTextFieldChanged()", from: self)
        
        if let searchText = self.searchTextField.text, !searchText.isEmpty {
            self.apply(filterType: .search(text: searchText), propList: self.propFullList)
        } else {
            self.apply(filterType: .none, propList: self.propFullList)
        }
    }
    
    @objc fileprivate func onTableRefreshControlRequested(_ sender: Any) {
        Log.high("onTableRefreshControlRequested()", from: self)
        
        self.refreshPropList()
    }
    
    @objc fileprivate func onApplicationWillEnterForeground(_ notification: NSNotification) {
        Log.high("onApplicationWillEnterForeground()", from: self)
        
        self.refreshPropList()
    }
    
    // MARK: -
    
    fileprivate func showEmptyState(image: UIImage? = nil, title: String, message: String, action: EmptyStateAction? = nil) {
        self.emptyStateView.hideActivityIndicator()
        
        self.emptyStateView.image = image
        self.emptyStateView.title = title
        self.emptyStateView.message = message
        self.emptyStateView.action = action
        
        self.emptyStateContainerView.isHidden = false
        
        self.tableView.isHidden = true
    }
    
    fileprivate func hideEmptyState() {
        self.emptyStateContainerView.isHidden = true
        
        self.tableView.isHidden = false
    }
    
    fileprivate func showNoDataState() {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: { [unowned self] in
            self.refreshPropList()
        })
        
        self.showEmptyState(title: "Boosts not found".localized(), message: "", action: action)
    }
    
    fileprivate func showLoadingState() {
        if self.emptyStateContainerView.isHidden {
            self.showEmptyState(title: "Searching boosts".localized(),
                                message: "We are loading list of boosts. Please wait a bit.".localized())
        }
        
        self.emptyStateView.showActivityIndicator()
    }
    
    // MARK: -
    
    fileprivate func config(cell: PropTableViewCell, at indexPath: IndexPath) {
        let propListItem = self.propList[indexPath.row]
        
        guard let prop = propListItem.prop else {
            return
        }
        
        cell.emoji = prop.emoji
        cell.message = prop.message
        
        cell.onPropButtonClicked = { [unowned self, weak propListItem] in
            if let propListItem = propListItem, self.tableView.allowsSelection {
                self.send(propListItem: propListItem)
            }
        }
    }
    
    fileprivate func config(cell: CustomPropTableViewCell) {
        cell.onCustomPropButtonClicked = { [unowned self] in
            self.sendCustomProp?()
        }
    }
    
    // MARK: -
    
    fileprivate func handle(stateError error: Error, retryHandler: (() -> Void)? = nil) {
        let action = EmptyStateAction(title: "Try again".localized(), isPrimary: false, onClicked: {
            retryHandler?()
        })
        
        switch error as? WebError {
        case .some(.unauthorized):
            self.unauthorize?()
            
        case .some(.connection), .some(.timeOut):
            if self.propFullList.isEmpty {
                self.showEmptyState(title: "No Internet Connection".localized(),
                                    message: "Check your wi-fi or mobile data connection.".localized(),
                                    action: action)
            }
            
        default:
            if self.propFullList.isEmpty {
                self.showEmptyState(title: "Something went wrong".localized(),
                                    message: "Please let us know what went wrong or try again later.".localized(),
                                    action: action)
            }
        }
    }
    
    fileprivate func handle(actionError error: Error, okHandler: (() -> Void)? = nil) {
        switch error as? WebError {
        case .some(.unauthorized):
            self.unauthorize?()
            
        default:
            self.showMessage(withError: error, okHandler: okHandler)
        }
    }
    
    fileprivate func send(propListItem: PropListItem) {
        Log.high("send(propListItem: \(String(describing: propListItem.prop?.uid)))", from: self)
        
        guard let propReceiver = self.propReceiver, let prop = propListItem.prop else {
            return
        }
        
        let loadingViewController = LoadingViewController()
        
        self.present(loadingViewController, animated: true) {
            firstly {
                Services.propReceiversService.send(prop: prop, to: propReceiver, boostType: .secret)
            }.done { status in
                SVProgressHUD.showSuccess(withStatus: "Sent!")
                loadingViewController.dismiss(animated: true, completion: {
                    SVProgressHUD.dismiss()
                    self.finishPropSending?()
                })
            }.catch { error in
                loadingViewController.dismiss(animated: true, completion: {
                    self.handle(actionError: error)
                })
            }
        }
    }
    
    fileprivate func refreshPropList() {
        Log.high("refreshPropList()", from: self)
        
        self.isRefreshingData = true
        
        if !self.tableRefreshControl.isRefreshing {
            if self.propList.isEmpty || (!self.emptyStateContainerView.isHidden) {
                self.showLoadingState()
            }
        }
        
        // TODO: Check it
        
        let propListPromise: Promise<[PropListItem]>
        propListPromise = Services.propsService.refreshAllProps()
        
        let propListType = self.propListType
        
        firstly {
            after(seconds: 0.25)
        }.then {
            propListPromise
        }.ensure {
            if self.tableRefreshControl.isRefreshing {
                self.tableRefreshControl.endRefreshing()
            }
            
            self.isRefreshingData = false
        }.done { propList in
            if self.propListType == propListType {
                self.apply(filterType: self.filterType, propList: propList)
            }
        }.catch { error in
            self.handle(stateError: error, retryHandler: { [weak self] in
                self?.refreshPropList()
            })
        }
    }
    
    fileprivate func resetPropListTimer() {
        self.propListTimer?.invalidate()
        self.propListTimer = nil
    }
    
    fileprivate func restartPropListTimer() {
        self.resetPropListTimer()
        
        guard !self.isRefreshingData else {
            return
        }
        
        self.propListTimer = Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false, block: { [weak self] timer in
            if let viewController = self {
                let propList = Services.cacheViewContext.propListsManager.fetch(with: viewController.propListType)
                
                viewController.apply(filterType: viewController.filterType, propList: propList)
            }
        })
    }
    
    fileprivate func restartPropListTimer(with propListItems: [PropListItem]) {
        guard propListItems.contains(where: { propListItem in
            switch propListItem.listType {
            case .some(self.propListType):
                return true
                
            default:
                return false
            }
        }) else {
            return
        }
        
        self.restartPropListTimer()
    }
    
    fileprivate func subscribeToPropListEvents() {
        self.unsubscribeFromPropListEvents()
        
        let propListsManager = Services.cacheViewContext.propListsManager
        
        propListsManager.objectsChangedEvent.connect(self, handler: { [weak self] propListItems in
            self?.restartPropListTimer(with: propListItems)
        })
        
        propListsManager.startObserving()
    }
    
    fileprivate func unsubscribeFromPropListEvents() {
        Services.cacheViewContext.propListsManager.objectsChangedEvent.disconnect(self)
    }
    
    fileprivate func apply(filterType: FilterType, propList: [PropListItem], canShowState: Bool = true) {
        Log.high("apply(propList: \(propList.count))", from: self)
        
        self.filterType = filterType
        
        var propFullList: [PropListItem]
        
        if let user = self.propReceiver?.user {
            propFullList = propList.filter({ propListItem in
                switch propListItem.prop?.gender {
                case .some(.nonBinary):
                    return true
                
                case .some(.male):
                    return (user.gender == .male)
                    
                case .some(.female):
                    return (user.gender == .female)
                    
                case .none:
                    return false
                }
            })
        } else {
            propFullList = []
        }
        
        propFullList.shuffle()
        self.propFullList = propFullList
        
        if self.isViewLoaded {
            switch self.filterType {
            case .none:
                self.propList = self.propFullList
                
            case .search(let text):
                let searchText = text.lowercased()
                
                self.propList = self.propFullList.filter({ propListItem in
                    return propListItem.prop?.message?.lowercased().contains(searchText) ?? false
                })
            }
            
            if self.propFullList.isEmpty && canShowState {
                self.showNoDataState()
            } else {
                self.hideEmptyState()
            }
            
            if self.isViewAppeared {
                self.tableView.beginUpdates()
                self.tableView.deleteSections(IndexSet(integersIn: 0..<self.tableView.numberOfSections), with: .fade)
                self.tableView.insertSections(IndexSet(integersIn: 0..<(self.propList.isEmpty ? 0 : 3)), with: .fade)
                self.tableView.endUpdates()
            } else {
                self.tableView.reloadData()
            }
        }
        
        self.resetPropListTimer()
    }
    
    fileprivate func apply(propListType: PropListType) {
        Log.high("apply(propListType: \(propListType))", from: self)
        
        self.propListType = propListType
        
        if self.isViewLoaded {
            var propList: [PropListItem]
            
            propList = Services.cacheViewContext.propListsManager.fetch(with: .all)
            self.apply(filterType: self.filterType, propList: propList, canShowState: false)
            
            if self.propFullList.isEmpty {
                self.refreshPropList()
            }
        }
    }
    
    // MARK: -
    
    func apply(propReceiver: PropReceiver) {
        Log.high("apply(propReceiver: \(String(describing: propReceiver.user?.fullName)))", from: self)
        
        self.propReceiver = propReceiver
        
        if self.isViewLoaded {
            self.apply(propListType: .popular)
            
            self.hasAppliedData = true
        } else {
            self.hasAppliedData = false
        }
    }
    
    func apply(propListItem: PropListItem, boostType: BoostType) {
        Log.high("selectedPropListItem()", from: self)
        
        self.selectedPropListItem = propListItem
        
        self.selectedBoostType = boostType
        
        self.shouldSendProp = true
    }
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tableRefreshControl = UIRefreshControl()
        
        tableRefreshControl.tintColor = Colors.lightActivityIndicator
        
        tableRefreshControl.addTarget(self,
                                      action: #selector(self.onTableRefreshControlRequested(_:)),
                                      for: .valueChanged)
        
        self.tableView.refreshControl = tableRefreshControl
        self.tableRefreshControl = tableRefreshControl
        
        self.hasAppliedData = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isRefreshingData = false
        
        if let propReceiver = self.propReceiver, !self.hasAppliedData {
            self.apply(propReceiver: propReceiver)
        }
        
        self.subscribeToPropListEvents()
        self.subscribeToKeyboardNotifications()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.onApplicationWillEnterForeground(_:)),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.unsubscribeFromPropListEvents()
        self.unsubscribeFromKeyboardNotifications()
        
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        
        self.hasAppliedData = false
    }
}

// MARK: - UITableViewDataSource

extension PropTableViewController: UITableViewDataSource {
    
    // MARK: - Instance Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.propList.isEmpty ? 0 : 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case Constants.customPropTableSection:
            return 1
            
        case Constants.propsTableSection:
            return 3
            
        case Constants.pullDownToShuffleLabelTableSection:
            return 1
            
        default:
            fatalError()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        
        switch indexPath.section {
        case Constants.customPropTableSection:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.customPropTableCellIdentifier,
                                                 for: indexPath)
            
            self.config(cell: cell as! CustomPropTableViewCell)
            
        case Constants.propsTableSection:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.propTableCellIdentifier,
                                                 for: indexPath)
            
            self.config(cell: cell as! PropTableViewCell, at: indexPath)
    
        case Constants.pullDownToShuffleLabelTableSection:
            cell = tableView.dequeueReusableCell(withIdentifier: Constants.pullDownToShuffleLabelCellIdentifier, for: indexPath)
            
        default:
            fatalError()
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate

extension PropTableViewController: UITableViewDelegate {

    // MARK: - Instance Methods

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let user = self.propReceiver?.user else {
            fatalError()
        }

        switch indexPath.section {
        case Constants.customPropTableSection:
            if !user.canSendCustomProp {
                return 0.0
            } else {
                return 55.0
            }

        case Constants.propsTableSection, Constants.pullDownToShuffleLabelTableSection:
            return UITableView.automaticDimension

        default:
            fatalError()
        }
    }
}

// MARK: - KeyboardScrollableHandler

extension PropTableViewController: KeyboardScrollableHandler {
    
    // MARK: - Instance Properties
    
    var scrollableView: UITableView {
        return self.tableView
    }
}

// MARK: - DictionaryReceiver

extension PropTableViewController: DictionaryReceiver {
    
    // MARK: - Instance Methods
    
    func apply(dictionary: [String: Any]) {
        guard let propListItem = dictionary["propListItem"] as? PropListItem else {
            return
        }
        
        guard let boostType = dictionary["boostType"] as? BoostType else {
            return
        }
        
        self.apply(propListItem: propListItem, boostType: boostType)
    }
}
