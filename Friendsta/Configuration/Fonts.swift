//
//  Fonts.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 13.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

enum Fonts {
    
    // MARK: - Type Methods

    static func extraBoldItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-ExtraBoldItalic", size: size)!
    }
    
    static func blackItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-BlackItalic", size: size)!
    }
    
    static func heavy(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-Heavy", size: size)!
    }
    
    static func bold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-Bold", size: size)!
    }
    
    static func extraBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-ExtraBold", size: size)!
    }
    
    static func regular(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-Regular", size: size)!
    }
    
    static func medium(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-Medium", size: size)!
    }
    
    static func regularItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-RegularItalic", size: size)!
    }
    
    static func black(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-Black", size: size)!
    }
    
    static func semiBold(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-SemiBold", size: size)!
    }
    
    static func light(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-Light", size: size)!
    }
    
    static func boldItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-BoldItalic", size: size)!
    }
    
    static func mediumItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-MediumItalic", size: size)!
    }
    
    static func heavyItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-HeavyItalic", size: size)!
    }
    
    static func lightItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-LightItalic", size: size)!
    }
    
    static func semiBoldItalic(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "Gilroy-SemiBoldItalic", size: size)!
    }
}
