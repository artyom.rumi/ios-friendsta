//
//  Managers.swift
//  Friendsta
//
//  Created by Elina Batyrova on 30/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper
import FriendstaTools

enum Managers {
    
    // MARK: - Type Properties
    
    static let userDefaultsManager: UserDefaultsManager = DefaultUserDefaultsManager(suiteName: Keys.applicationGroupID)
    static let keychainWrapper = KeychainWrapper(serviceName: Keys.applicationGroupID, accessGroup: Keys.applicationGroupID)
    static let linkPreviewManager: LinkPreviewManager = DefaultLinkPreviewManager()
    static let imageLoader: ImageLoader = DefaultImageLoader()
}
