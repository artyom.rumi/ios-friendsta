//
//  Models.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaNetwork

enum Models {
    
    // MARK: - Type Properties

    static let account: AccountModel = DefaultAccountModel(userDefaultsManager: Managers.userDefaultsManager.userDefaults,
                                                           keychainWrapperManager: Managers.keychainWrapper)
    static let cache: CacheModel = DefaultCacheModel.shared
}
