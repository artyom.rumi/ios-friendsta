//
//  Colors.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 06.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

enum Colors {
    
    // MARK: - Type Properties
    
    static let primary = UIColor(redByte: 118, greenByte: 31, blueByte: 214)
    static let secondary = UIColor(redByte: 255, greenByte: 211, blueByte: 0)
    static let warning = UIColor(redByte: 238, greenByte: 71, blueByte: 56)
    static let success = UIColor(redByte: 76, greenByte: 217, blueByte: 100)
    static let black = UIColor(whiteByte: 0)
    static let clear = UIColor(whiteByte: 0, alpha: 0)
    static let lightPink = UIColor(red: 1, green: 0.44, blue: 0.53, alpha: 1)
    static let white = UIColor(whiteByte: 1)
    static let lightGray = UIColor(redByte: 221, greenByte: 221, blueByte: 221)

    static let whiteText = UIColor(whiteByte: 255)
    static let grayText = UIColor(whiteByte: 153)
    static let blackText = UIColor(whiteByte: 0)
    static let redText = UIColor(redByte: 255, greenByte: 60, blueByte: 84)
    static let placeholderText = UIColor(whiteByte: 153)

    static let whiteBackground = UIColor(whiteByte: 255)
    static let lightBackground = UIColor(whiteByte: 248, alpha: 0.82)
    static let grayBackground = UIColor(whiteByte: 235)
    static let darkBackground = UIColor(whiteByte: 0, alpha: 0.5)
    static let blackBackground = UIColor(whiteByte: 0)

    static let lightActivityIndicator = UIColor(whiteByte: 255)
    static let darkActivityIndicator = UIColor(whiteByte: 128)

    static let lightPageIndicator = UIColor(whiteByte: 255)
    static let darkPageIndicator = UIColor(whiteByte: 0, alpha: 0.12)

    static let blueGuideView = UIColor(redByte: 112, greenByte: 148, blueByte: 245)
    static let lightBlueGuideView = UIColor(redByte: 95, greenByte: 120, blueByte: 225)
    static let orangeGuideView = UIColor(redByte: 255, greenByte: 148, blueByte: 112)

    static let blackShadow = UIColor(redByte: 0, greenByte: 0, blueByte: 0)
    
    static let anonymousView = UIColor(redByte: 255, greenByte: 228, blueByte: 98)
    static let revealedView = UIColor(redByte: 255, greenByte: 127, blueByte: 239)
    
    static let incomingAnonymousCloudView = UIColor(redByte: 238, greenByte: 238, blueByte: 238)
    static let incomingNotAnonymousCloudView = UIColor(redByte: 130, greenByte: 241, blueByte: 232)
    
    static let outcomingAnonymousCloudView = UIColor(redByte: 255, greenByte: 226, blueByte: 88)
    static let outcomingNotAnonymousCloudView = UIColor(redByte: 255, greenByte: 127, blueByte: 239)
    
    static let keepControl = UIColor(redByte: 253, greenByte: 153, blueByte: 120)
}
