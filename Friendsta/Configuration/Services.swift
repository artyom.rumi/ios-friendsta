//
//  Services.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 09.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaNetwork
import FriendstaTools

enum Services {
    
    // MARK: - Type Properties
    
    static let accountProvider: AccountProvider = DefaultAccountProvider(model: Models.account)
    static let cacheProvider: CacheProvider = DefaultCacheProvider(model: Models.cache)
    
    // MARK: -
    
    static let accountUserExtractor: AccountUserExtractor = DefaultAccountUserExtractor(cacheProvider: Services.cacheProvider,
                                                                                        accountUserCoder: Coders.accountUserCoder,
                                                                                        propCoder: Coders.propCoder)
    
    static let usersExtractor: UsersExtractor = DefaultUsersExtractor(cacheProvider: Services.cacheProvider,
                                                                      userCoder: Coders.userCoder,
                                                                      propCoder: Coders.propCoder,
                                                                      metaCoder: Coders.metaCoder)
    
    static let contactsExtractor: ContactsExtractor = DefaultContactsExtractor(cacheProvider: Services.cacheProvider)
    
    static let outlineContactsExtractor: OutlineContactsExtractor = DefaultOutlineContactsExtractor(cacheProvider: Services.cacheProvider,
                                                                                                    outlineContactCoder: Coders.outlineContactCoder,
                                                                                                    metaCoder: Coders.metaCoder)
    
    static let schoolsExtractor: SchoolsExtractor = DefaultSchoolsExtractor(cacheProvider: Services.cacheProvider,
                                                                            schoolCoder: Coders.schoolCoder)
    
    static let propsExtractor: PropsExtractor = DefaultPropsExtractor(cacheProvider: Services.cacheProvider,
                                                                      propCoder: Coders.propCoder)
    
    static let propRequestsExtractor: PropRequestsExtractor = DefaultPropRequestsExtractor(cacheProvider: Services.cacheProvider,
                                                                                           propRequestCoder: Coders.propRequestCoder)
    
    static let propReceiversExtractor: PropReceiversExtractor = DefaultPropReceiversExtractor(cacheProvider: Services.cacheProvider,
                                                                                              userCoder: Coders.userCoder)
    
    static let propCardsExtractor: PropCardsExtractor = DefaultPropCardsExtractor(cacheProvider: Services.cacheProvider,
                                                                                  propCardCoder: Coders.propCardCoder,
                                                                                  userCoder: Coders.userCoder)
    
    static let chatsExtractor: ChatsExtractor = DefaultChatsExtractor(cacheProvider: Services.cacheProvider,
                                                                      chatCoder: Coders.chatCoder)
    
    static let chatMasksExtractor: ChatMasksExtractor = DefaultChatMasksExtractor(cacheProvider: Services.cacheProvider,
                                                                                  chatMaskCoder: Coders.chatMaskCoder)

    static let feedExtractor: FeedExtractor = DefaultFeedExtractor(cacheProvider: Services.cacheProvider, feedCoder: Coders.feedCoder, chatExtractor: Services.chatsExtractor)

    static let activityExtractor: ActivityExtractor = DefaultActivityExtractor(cacheProvider: Services.cacheProvider, activityCoder: Coders.activityCoder)

    // MARK: -
    
    static let webRequestAdapter: WebRequestAdapter = DefaultAPIWebRequestAdapter(serverBaseURL: Keys.apiServerBaseURL,
                                                                                  accountProvider: Services.accountProvider)
    
    static let webService: WebService = AlamofireWebService(serverBaseURL: Keys.apiServerBaseURL,
                                                            requestAdapter: Services.webRequestAdapter,
                                                            activityIndicator: UIWebActivityIndicator.shared)
    
    static let chatClient: ChatClient = PubNubChatClient(eventCoder: Coders.chatClientEventCoder,
                                                         pushPayloadCoder: Coders.chatClientPushPayloadCoder,
                                                         publishKey: Keys.pubNubPublishKey,
                                                         subscribeKey: Keys.pubNubSubscribeKey)

    static let propReactionsExtractor: PropReactionsExtractor = DefaultPropReactionsExtractor(cacheProvider: Services.cacheProvider,
                                                                                              propReactionCoder: Coders.propReactionCoder)

    static let commentExtractor: CommentExtractor = DefaultCommentExtractor(cacheProvider: Services.cacheProvider,
                                                                            commentCoder: Coders.commentCoder,
                                                                            userCoder: Coders.userCoder,
                                                                            chatCoder: Coders.chatCoder,
                                                                            chatExtractor: Services.chatsExtractor)
    
    // MARK: -
    
    static let apiWebService: APIWebService = DefaultAPIWebService(accountProvider: Services.accountProvider,
                                                                   webService: Services.webService)
    
    static let apiChatClient: APIChatClient = DefaultAPIChatClient(accountProvider: Services.accountProvider,
                                                                   cacheProvider: Services.cacheProvider,
                                                                   chatClient: Services.chatClient,
                                                                   chatMessageCoder: Coders.chatMessageCoder)
    
    static let authorizationService: AuthorizationService = DefaultAuthorizationService(accountProvider: Services.accountProvider,
                                                                                        apiWebService: Services.apiWebService,
                                                                                        accessCoder: Coders.accessCoder,
                                                                                        accessSessionCoder: Coders.accessSessionCoder)
    
    static let accountUserService: AccountUserService = DefaultAccountUserService(accountProvider: Services.accountProvider,
                                                                                  cacheProvider: Services.cacheProvider,
                                                                                  apiWebService: Services.apiWebService,
                                                                                  accountUserExtractor: Services.accountUserExtractor,
                                                                                  accountUserBufferCoder: Coders.accountUserBufferCoder,
                                                                                  accountUserCoder: Coders.accountUserCoder)
    
    static let usersService: UsersService = DefaultUsersService(cacheProvider: Services.cacheProvider,
                                                                apiWebService: Services.apiWebService,
                                                                contactsService: Services.contactsService,
                                                                chatsService: Services.chatsService,
                                                                usersExtractor: Services.usersExtractor)
    
    static let outlineContactsService: OutlineContactsService = DefaultOutlineContactsService(cacheProvider: Services.cacheProvider,
                                                                                              apiWebService: Services.apiWebService,
                                                                                              outlineContactsExtractor: Services.outlineContactsExtractor)
    
    static let mediaService: MediaService = DefaultMediaService()
    static let locationService: LocationService = DefaultLocationService()
    static let contactsService: ContactsService = DefaultContactsService(contactsExtractor: Services.contactsExtractor)
    
    static let schoolGradesService: SchoolGradesService = DefaultSchoolGradesService()
    
    static let schoolsService: SchoolsService = DefaultSchoolsService(apiWebService: Services.apiWebService,
                                                                      schoolsExtractor: Services.schoolsExtractor,
                                                                      locationCoder: Coders.locationCoder)
    
    static let propsService: PropsService = DefaultPropsService(apiWebService: Services.apiWebService,
                                                                propsExtractor: Services.propsExtractor)
    
    static let propRequestsService: PropRequestsService = DefaultPropRequestsService(apiWebService: Services.apiWebService,
                                                                                     propRequestsExtractor: Services.propRequestsExtractor)

    static let propReceiversService: PropReceiversService = DefaultPropReceiversService(cacheProvider: Services.cacheProvider,
                                                                                        apiWebService: Services.apiWebService,
                                                                                        propRequestsService: Services.propRequestsService,
                                                                                        usersService: Services.usersService,
                                                                                        propReceiversExtractor: Services.propReceiversExtractor)
    
    static let propReactionsService: PropReactionsService = DefaultPropReactionsService(apiWebService: Services.apiWebService,
                                                                                        propReactionsExtractor: Services.propReactionsExtractor)
                                                                                        
    static let propCardsService: PropCardsService = DefaultPropCardsService(cacheProvider: Services.cacheProvider,
                                                                            apiWebService: Services.apiWebService,
                                                                            propCardsExtractor: Services.propCardsExtractor)
    
    static let feedbackService: FeedbackService = DefaultFeedbackService(apiWebService: Services.apiWebService)
    
    static let chatsService: ChatsService = DefaultChatsService(accountProvider: Services.accountProvider,
                                                                cacheProvider: Services.cacheProvider,
                                                                apiWebService: Services.apiWebService,
                                                                apiChatClient: Services.apiChatClient,
                                                                chatsExtractor: Services.chatsExtractor)
    
    static let chatMasksService: ChatMasksService = DefaultChatMasksService(cacheProvider: Services.cacheProvider,
                                                                            apiWebService: Services.apiWebService,
                                                                            chatsService: Services.chatsService,
                                                                            chatMasksExtractor: Services.chatMasksExtractor,
                                                                            chatMaskCoder: Coders.chatMaskCoder)
    
    static let chatMessagesService: ChatMessagesService = DefaultChatMessagesService(accountProvider: Services.accountProvider,
                                                                                     cacheProvider: Services.cacheProvider,
                                                                                     apiWebService: Services.apiWebService,
                                                                                     apiChatClient: Services.apiChatClient,
                                                                                     chatsService: Services.chatsService,
                                                                                     photoCoder: Coders.photoCoder,
                                                                                     chatMessageCoder: Coders.chatMessageCoder)
    
    static let notificationsService: NotificationsService = DefaultNotificationsService(accountProvider: Services.accountProvider,
                                                                                        apiWebService: Services.apiWebService,
                                                                                        notificationCategoryCoder: Coders.notificationCategoryCoder,
                                                                                        notificationActionCoder: Coders.notificationActionCoder)
    
    static let feedService: FeedService = DefaultFeedService(apiWebService: Services.apiWebService, feedExtractor: Services.feedExtractor)

    static let commentService: CommentService = DefaultCommentService(apiWebService: Services.apiWebService,
                                                                      commentExtractor: Services.commentExtractor,
                                                                      feedExtractor: Services.feedExtractor)

    // MARK: -

    static let imageLoader: ImageLoader = DefaultImageLoader()
    
    // MARK: -
    
    static var accountUser: AccountUser? {
        if let accountUserUID = Services.accountProvider.model.access?.userUID {
            return Services.cacheProvider.model.viewContext.accountUsersManager.first(with: accountUserUID)
        } else {
            return nil
        }
    }
    
    static var accountAccess: Access? {
        return Services.accountProvider.model.access
    }
    
    static var accountDeviceToken: String? {
        return Services.accountProvider.model.deviceToken
    }
    
    static var accountNotifications: [FriendstaNetwork.Notification] {
        return Services.accountProvider.model.notifications
    }
    
    static var accountSettings: Settings {
        return Services.accountProvider.model.settings
    }
    
    static var cacheViewContext: CacheModelContext {
        return Services.cacheProvider.model.viewContext
    }
    
    // MARK: -
    
    static var invitationService: InvitationService = DefaultInvitationService(apiWebService: Services.apiWebService)

    static let activityService: ActivityService = DefaultActivityService(apiWebService: Services.apiWebService, activityExtractor: Services.activityExtractor, cacheProvider: Services.cacheProvider)
}
