//
//  Images.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 18/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

enum Images {

    // MARK: - Type Properties

    static let hoursGlassIcon = UIImage(named: "HourGlassIcon")
    static let plusIcon = UIImage(named: "PlusIcon")

    static let avatarSmallPlaceholder = UIImage(named: "AvatarSmallPlaceholder")
    static let avatarMediumPlaceholder = UIImage(named: "AvatarMediumPlaceholder")
    static let avatarLargePlaceholder = UIImage(named: "AvatarLargePlaceholder")

    static let likeIcon = UIImage(named: "LikeIcon")
    static let likeFillIcon = UIImage(named: "LikeFillIcon")

    static let chatIcon = UIImage(named: "ChatIcon")
    
    static let commentWhiteIcon = UIImage(named: "CommentIcon")
    
    static let repostIcon = UIImage(named: "RepostIcon")
    
    static let inboxBackground = UIImage(named: "InboxBackground")
  
    static let galleryIcon = UIImage(named: "GalleryIcon")

    static let closeButton = UIImage(named: "CloseButton")
    static let whiteCloseButton = UIImage(named: "WhiteCloseButton")
    static let backArrowButton = UIImage(named: "BackArrowButtonIcon")
    
    static let editPencilIcon = UIImage(named: "EditPencilIcon")
    
    static let anonymousSegmentIcon = UIImage(named: "AnonymousSegmentIcon")
    
    static let threeDotsIcon = UIImage(named: "ThreeDots")
    
    static let groupChatAvatarPlaceholder = UIImage(named: "GroupChatAvatarPlaceholder")
}
