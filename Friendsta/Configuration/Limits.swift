//
//  Limits.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 28.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

enum Limits {
    
    // MARK: - Type Properties
    
    static let friendMinCount = 5
    static let outlineContactMinCount = 5
    
    static let avatarImageMaxWidth: CGFloat = 1280.0
    static let avatarImageQuality: CGFloat = 0.8
    
    static let photoImageMaxWidth: CGFloat = 1280.0
    static let photoImageQuality: CGFloat = 0.8

    static let maxFriendCount = 9
}
