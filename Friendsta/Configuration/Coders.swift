//
//  Coders.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum Coders {
    
    // MARK: - Type Properties
    
    static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        
        return dateFormatter
    }()
    
    // MARK: -
    
    static let chatClientMessageCoder: ChatClientMessageCoder = PubNubChatClientMessageCoder()
    static let chatClientMethodCoder: ChatClientMethodCoder = PubNubChatClientMethodCoder(messageCoder: Coders.chatClientMessageCoder)
    
    static let chatClientEventCoder: ChatClientEventCoder = PubNubChatClientEventCoder(methodCoder: Coders.chatClientMethodCoder)
    static let chatClientPushPayloadCoder: ChatClientPushPayloadCoder = PubNubChatClientPushPayloadCoder()
    
    // MARK: -
    
    static let accessSessionCoder: AccessSessionCoder = DefaultAccessSessionCoder(dateFormatter: Coders.dateFormatter)
    static let accessCoder: AccessCoder = DefaultAccessCoder(dateFormatter: Coders.dateFormatter)
    
    static let userRoleCoder: UserRoleCoder = DefaultUserRoleCoder()
    static let locationCoder: LocationCoder = DefaultLocationCoder()
    static let genderCoder: GenderCoder = DefaultGenderCoder()
    static let photoCoder: PhotoCoder = DefaultPhotoCoder()
    
    static let accountUserBufferCoder: AccountUserBufferCoder = DefaultAccountUserBufferCoder(genderCoder: Coders.genderCoder)
    
    static let accountUserCoder: AccountUserCoder = DefaultAccountUserCoder(genderCoder: Coders.genderCoder,
                                                                            photoCoder: Coders.photoCoder,
                                                                            propCoder: Coders.propCoder,
                                                                            dateFormatter: Coders.dateFormatter)
    
    static let userCoder: UserCoder = DefaultUserCoder(userRoleCoder: Coders.userRoleCoder,
                                                       genderCoder: Coders.genderCoder,
                                                       photoCoder: Coders.photoCoder,
                                                       propCoder: Coders.propCoder)
    
    static let outlineContactCoder: OutlineContactCoder = DefaultOutlineContactCoder()
    
    static let schoolCoder: SchoolCoder = DefaultSchoolCoder()
    static let propCoder: PropCoder = DefaultPropCoder(genderCoder: Coders.genderCoder)
    static let customPropCoder: CustomPropCoder = DefaultCustomPropCoder()
    
    static let propRequestCoder: PropRequestCoder = DefaultPropRequestCoder()
    static let propReactionCoder: PropReactionCoder = DefaultPropReactionCoder(dateFormatter: Coders.dateFormatter)
    
    static let propCardTypeCoder: PropCardTypeCoder = DefaultPropCardTypeCoder()
    
    static let propCardCoder: PropCardCoder = DefaultPropCardCoder(propCardTypeCoder: Coders.propCardTypeCoder,
                                                                   propCoder: Coders.propCoder,
                                                                   customPropCoder: Coders.customPropCoder,
                                                                   propReactionCoder: Coders.propReactionCoder,
                                                                   genderCoder: Coders.genderCoder,
                                                                   userCoder: Coders.userCoder,
                                                                   dateFormatter: Coders.dateFormatter)
    
    static let chatCoder: ChatCoder = DefaultChatCoder(userCoder: Coders.userCoder,
                                                       dateFormatter: Coders.dateFormatter)
    
    static let chatMaskCoder: ChatMaskCoder = DefaultChatMaskCoder(photoCoder: Coders.photoCoder)
    
    static let chatMessageTypeCoder: ChatMessageTypeCoder = DefaultChatMessageTypeCoder()
    
    static let chatTextMessageCoder: ChatTextMessageCoder = DefaultChatTextMessageCoder(chatMessageTypeCoder: Coders.chatMessageTypeCoder)
    static let chatPhotoMessageCoder: ChatPhotoMessageCoder = DefaultChatPhotoMessageCoder(chatMessageTypeCoder: Coders.chatMessageTypeCoder)
    
    static let chatInfoTypeCoder: ChatInfoTypeCoder = DefaultChatInfoTypeCoder()
    
    static let chatInfoMessageCoder: ChatInfoMessageCoder = DefaultChatInfoMessageCoder(chatMessageTypeCoder: Coders.chatMessageTypeCoder,
                                                                                        chatInfoTypeCoder: Coders.chatInfoTypeCoder)
    
    static let chatMessageCoder: ChatMessageCoder = DefaultChatMessageCoder(chatMessageTypeCoder: Coders.chatMessageTypeCoder,
                                                                            chatTextMessageCoder: Coders.chatTextMessageCoder,
                                                                            chatPhotoMessageCoder: Coders.chatPhotoMessageCoder,
                                                                            chatInfoMessageCoder: Coders.chatInfoMessageCoder)
    
    static let notificationCategoryCoder: NotificationCategoryCoder = DefaultNotificationCategoryCoder()
    static let notificationActionCoder: NotificationActionCoder = DefaultNotificationActionCoder()

    static let feedCoder: FeedCoder = DefaultFeedCoder(photoCoder: Coders.photoCoder, dateFormatter: Formatter.iso8601)

    static let metaCoder: MetaCoder = DefaultMetaCoder()
    static let commentCoder: CommentCoder = DefaultCommentCoder(dateFormatter: Coders.dateFormatter,
                                                                userCoder: Coders.userCoder,
                                                                chatCoder: Coders.chatCoder)

    static let activityCoder: ActivityCoder = DefaultActivityCoder(dateFormatter: Coders.dateFormatter)
}
