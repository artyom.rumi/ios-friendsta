//
//  Staging.swift
//  FriendstaStaging
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum Keys {
    
    // MARK: - Instance Properties
    
//    static let apiServerBaseURL = URL(string: "http://props-api-staging.nicelysocial.com/api")!
//    static let apiServerBaseURL = URL(string: "http://localhost:5000/api")!
    static let apiServerBaseURL = URL(string: "http://198.27.81.216:3000/api")!
    
    static let webURL = URL(string: "https://staging.nicelysocial.com/u/")!
    
    static let pubNubPublishKey = "pub-c-5365bd7a-9cf2-4163-9ad7-0289af5a3077"
    static let pubNubSubscribeKey = "sub-c-f47fdf46-2132-11e8-a183-761142583d66"
    
    static let applicationGroupID = "group.com.decisionaccelerator.props.staging"
    
    static let storageApplicationBadgeNumberKey = "ApplicationBadge"
}
