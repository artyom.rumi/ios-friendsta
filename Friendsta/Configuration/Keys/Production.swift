//
//  Production.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

enum Keys {
    
    // MARK: - Instance Properties
    
    static let apiServerBaseURL = URL(string: "https://props-api.nicelysocial.com/api")!
//    static let apiServerBaseURL = URL(string: "http://localhost:5000/api")!
//    static let apiServerBaseURL = URL(string: "http://198.27.81.216:3000/api")!
    
    static let webURL = URL(string: "http://nicelysocial.com/u/")!
    
    static let pubNubPublishKey = "pub-c-2a23efff-6382-4ab5-8b4a-fe0562f9bc0b"
    static let pubNubSubscribeKey = "sub-c-b50c6d8e-5927-11e8-8ebf-f686a6d93a6b"
    
    static let applicationGroupID = "group.com.decisionaccelerator.props"
    
    static let storageApplicationBadgeNumberKey = "ApplicationBadge"
}
