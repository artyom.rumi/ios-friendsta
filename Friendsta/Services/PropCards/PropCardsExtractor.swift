//
//  PropCardsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol PropCardsExtractor {
    
    // MARK: - Instance Methods
    
    func extractPropCard(from response: [String: Any]) -> Promise<PropCard>
    func extractPropCards(from response: [[String: Any]]) -> Promise<[PropCard]>
    func extractPropCards(from responses: [PropCardListType: [[String: Any]]]) -> Promise<[[PropCardListItem]]>
    
    func extractPropWithReactions(from response: [[String: Any]]) -> Promise<[PropCard]>
}
