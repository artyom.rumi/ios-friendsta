//
//  DefaultPropCardsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultPropCardsExtractor: PropCardsExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let propCardCoder: PropCardCoder
    let userCoder: UserCoder
    
    // MARK: - Instance Methods
    
    @discardableResult
    fileprivate func extractPropCard(from response: [String: Any], context: CacheModelContext) throws -> PropCard {
        guard let propCardUID = self.propCardCoder.propCardUID(from: response) else {
            throw WebError.badResponse
        }
        
        let propCardsManager = context.propCardsManager
        
        let propCard: PropCard
        
        switch self.propCardCoder.propCardType(from: response) {
        case .unknown:
            throw WebError.badResponse
            
        case .regular:
            propCard = propCardsManager.first(with: propCardUID) ?? propCardsManager.append()
            
            guard let propUID = self.propCardCoder.propUID(from: response) else {
                throw WebError.badResponse
            }
        
            let propsManager = context.propsManager
            
            propCard.prop = propsManager.first(with: propUID) ?? propsManager.append()
            
        case .custom:
            propCard = propCardsManager.first(with: propCardUID) ?? propCardsManager.append()
            
            guard let customPropUID = self.propCardCoder.customPropUID(from: response) else {
                throw WebError.badResponse
            }
            
            let customPropsManager = context.customPropsManager
            
            propCard.customProp = customPropsManager.first(with: customPropUID) ?? customPropsManager.append()
        }
        
        let propReactionsManager = context.propReactionsManager
        
        if let propReactionUID = self.propCardCoder.propReactionUID(from: response) {
            propCard.reaction = propReactionsManager.first(with: propReactionUID) ?? propReactionsManager.append()
        } else {
            propCard.reaction = nil
        }
        
        let usersManager = context.usersManager
        
        if let senderUID = self.userCoder.userUID(from: response) {
            propCard.reaction?.sender = usersManager.first(with: senderUID) ?? usersManager.append()
        } else {
            propCard.reaction?.sender = nil
        }
        
        guard self.propCardCoder.decode(propCard: propCard, from: response) else {
            throw WebError.badResponse
        }
        
        return propCard
    }
    
    @discardableResult
    fileprivate func extractPropCards(from response: [[String: Any]], context: CacheModelContext) throws -> [PropCard] {
        let propCards = response.compactMap({ response in
            return (try? self.extractPropCard(from: response, context: context)) ?? nil
        })
        
        let propCardsManager = context.propCardsManager
        
        for propCard in propCardsManager.fetch() {
            if !propCards.contains(object: propCard) {
                propCardsManager.remove(object: propCard)
            }
        }
        
        return propCards
    }

    @discardableResult
    fileprivate func extractPropWithReactions(from response: [[String: Any]], context: CacheModelContext) throws -> [PropCard] {
        let propCards = response.compactMap({ response in
            return (try? self.extractPropCard(from: response, context: context)) ?? nil
        })

        let propCardsListManager = context.propCardListManager

        for propCardListItem in propCardsListManager.fetch(with: .reaction) {
            if let propCard = propCardListItem.propCard {
                if !propCards.contains(object: propCard) {
                    propCardsListManager.remove(object: propCardListItem)
                }
            }
        }

        return propCards
    }

    @discardableResult
    fileprivate func extractPropCards(from response: [[String: Any]], listType: PropCardListType, context: CacheModelContext) throws -> [PropCardListItem] {

        let propCardListManager = context.propCardListManager

        propCardListManager.clear(with: listType)

        var propCardList: [PropCardListItem] = []

        for response in response {
            let propCard = try self.extractPropCard(from: response, context: context)

            let propCardListItem = propCardListManager.append()

            propCardListItem.propCard = propCard
            propCardListItem.listType = listType
            propCardListItem.sorting = Int64(propCardList.count)

            propCardList.append(propCardListItem)
        }

        return propCardList
    }
    
    // MARK: - PropCardsExtractor
    
    func extractPropCard(from response: [String: Any]) -> Promise<PropCard> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let propCardUID = try self.extractPropCard(from: response, context: backgroundContext).uid
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.propCardsManager.first(with: propCardUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractPropCards(from response: [[String: Any]]) -> Promise<[PropCard]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
            
                backgroundContext.perform(block: {
                    do {
                        try self.extractPropCards(from: response, context: backgroundContext)
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.propCardsManager.fetch())
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }

    func extractPropWithReactions(from response: [[String: Any]]) -> Promise<[PropCard]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        try self.extractPropWithReactions(from: response, context: backgroundContext)

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            let propCardsWithReactionsList = cacheSession.model.viewContext.propCardListManager.fetch(with: .reaction)
                            let propCardWithReactions = propCardsWithReactionsList.compactMap({ (item) -> PropCard? in
                                if let propCard = item.propCard {
                                    return propCard
                                } else {
                                    return nil
                                }
                            })
                            seal.fulfill(propCardWithReactions)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractPropCards(from responses: [PropCardListType: [[String: Any]]]) -> Promise<[[PropCardListItem]]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        for (listType, response) in responses {
                            try self.extractPropCards(from: response, listType: listType, context: backgroundContext)
                        }
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(responses.compactMap({ listType, response in
                                let propList = cacheSession.model.viewContext.propCardListManager.fetch(with: listType)
                                
                                return propList.isEmpty ? nil : propList
                            }))
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
