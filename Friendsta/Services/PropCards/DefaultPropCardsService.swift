//
//  DefaultPropCardsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 29.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultPropCardsService: PropCardsService {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let apiWebService: APIWebService
    let propCardsExtractor: PropCardsExtractor

    // MARK: - Instance Methods
    
    func refreshPropCards() -> Promise<[PropCard]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v2/received_props"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.propCardsExtractor.extractPropCards(from: response)
            }.done { propCards in
                seal.fulfill(propCards)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func refreshPropCardsLists() -> Promise<[[PropCardListItem]]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v2/received_props"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { propCardsResponse in
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/my/incoming_reactions"),
                                             queue: DispatchQueue.global(qos: .userInitiated)).map({ propReactionsResponse in
                                                return (propCardsResponse, propReactionsResponse)
                                             })
            }.then { propCardsResponse, propReactionsResponse in
                self.propCardsExtractor.extractPropCards(from: [.prop: propCardsResponse,
                                                                .reaction: propReactionsResponse])
            }.done { userLists in
                seal.fulfill(userLists)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func markAsViewed(propCard: PropCard) -> Promise<Void> {
        return Promise(resolver: { seal in
            if propCard.isViewed {
                seal.fulfill(Void())
            } else {
                let propCardUID = propCard.uid
                
                firstly {
                    self.apiWebService.json(with: WebRequest(method: .post,
                                                             path: "v1/prop_deliveries/\(propCardUID)/views"))
                }.then { response in
                    self.cacheProvider.captureModel()
                }.done { cacheSession in
                    let propCardsManager = cacheSession.model.viewContext.propCardsManager
                    
                    for propCard in propCardsManager.fetch(with: propCardUID) {
                        propCard.viewedDate = Date()
                    }
                    
                    cacheSession.model.viewContext.save()
                    
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            }
        })
    }
    
    func refreshPropCardsWithReactions() -> Promise<[PropCard]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/my/incoming_reactions"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.propCardsExtractor.extractPropWithReactions(from: response)
            }.done { propCards in
                seal.fulfill(propCards)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
}
