//
//  PropCardsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 29.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol PropCardsService {
    
    // MARK: - Instance Methods
    
    func refreshPropCards() -> Promise<[PropCard]>
    func refreshPropCardsWithReactions() -> Promise<[PropCard]>
    func refreshPropCardsLists() -> Promise<[[PropCardListItem]]>
 
    func markAsViewed(propCard: PropCard) -> Promise<Void>
}
