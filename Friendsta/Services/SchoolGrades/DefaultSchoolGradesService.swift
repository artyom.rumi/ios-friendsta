//
//  DefaultSchoolGradesService.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 15/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

class DefaultSchoolGradesService: SchoolGradesService {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
    
        static let schoolGradeStartMonth = 9
        static let schoolGradeMaxNumber = 12
    }
    
    // MARK: - Instance Methods
    
    func generateSchoolGrades(for schoolGradeLevel: SchoolGradeLevel) -> [SchoolGrade] {
        let date = Date()
        
        let dateMonth = Calendar.current.component(.month, from: date)
        let dateYear = Calendar.current.component(.year, from: date)
        
        let minClassYear = dateYear + ((dateMonth >= Constants.schoolGradeStartMonth) ? 1 : 0)
        let maxClassYear = minClassYear + Constants.schoolGradeMaxNumber
        
        let schoolGradeMinNumber: Int
        let schoolGradeMaxNumber: Int
        
        switch schoolGradeLevel {
        case .middle:
            schoolGradeMinNumber = 6
            schoolGradeMaxNumber = 8
            
        case .high:
            schoolGradeMinNumber = 9
            schoolGradeMaxNumber = 12
        }
        
        var schoolGrades: [SchoolGrade] = []
    
        for schoolGradeNumber in schoolGradeMinNumber...schoolGradeMaxNumber {
            schoolGrades.append(SchoolGrade(level: schoolGradeLevel,
                                            number: schoolGradeNumber,
                                            classYear: maxClassYear - schoolGradeNumber))
        }
        
        return schoolGrades
    }
    
    func schoolGradeNumber(of classYear: Int) -> Int? {
        let date = Date()
        
        let dateMonth = Calendar.current.component(.month, from: date)
        let dateYear = Calendar.current.component(.year, from: date)
        
        let currentClassYear = dateYear + ((dateMonth >= Constants.schoolGradeStartMonth) ? 1 : 0)
        
        if classYear < currentClassYear {
            return nil
        } else {
            return max(Constants.schoolGradeMaxNumber - (classYear - currentClassYear), 0)
        }
    }
    
    func alreadyGraduatedYears() -> [Int] {
        let date = Date()
        
        let dateMonth = Calendar.current.component(.month, from: date)
        let dateYear = Calendar.current.component(.year, from: date)
        
        let currentClassYear = dateYear + ((dateMonth >= Constants.schoolGradeStartMonth) ? 1 : 0)
        
        var classYears: [Int] = []
        
        for classYear in (currentClassYear - 50)..<currentClassYear {
            classYears.append(classYear)
        }
        
        return classYears
    }
}
