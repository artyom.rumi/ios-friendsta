//
//  SchoolGradesService.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 14/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol SchoolGradesService {
    
    // MARK: - Instance Methods
    
    func generateSchoolGrades(for schoolGradeLevel: SchoolGradeLevel) -> [SchoolGrade]
    
    func schoolGradeNumber(of classYear: Int) -> Int?
    func alreadyGraduatedYears() -> [Int]
}
