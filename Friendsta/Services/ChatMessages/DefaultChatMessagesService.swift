//
//  DefaultChatMessagesService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 30.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultChatMessagesService: ChatMessagesService {
    
    // MARK: - Instance Properties
    
    let accountProvider: AccountProvider
    let cacheProvider: CacheProvider
    let apiWebService: APIWebService
    let apiChatClient: APIChatClient
    let chatsService: ChatsService
    let photoCoder: PhotoCoder
    let chatMessageCoder: ChatMessageCoder
    
    // MARK: - Instance Methods

    fileprivate func pushPayload(for chatMessage: ChatMessage, chat: Chat) -> ChatClientPushPayload? {
        if chatMessage.isSent {
            return nil
        } else {
            let senderName: String?
            
            if chat.isIncognito {
                senderName = nil
            } else {
                let chatMember = chat.members?.first(where: { chatMember in
                    return ((chatMember as! ChatMember).user?.uid == chatMessage.userUID)
                }) as? ChatMember
                
                senderName = chatMember?.user?.firstName ?? "apns_chat_message_unknown_sender".localized()
            }
            
            if let deviceToken = self.accountProvider.model.deviceToken {
                return self.chatMessageCoder.pushPayload(for: chatMessage, senderName: senderName, exceptions: [deviceToken])
            } else {
                return self.chatMessageCoder.pushPayload(for: chatMessage, senderName: senderName, exceptions: [])
            }
        }
    }
    
    fileprivate func event(for chatMessage: ChatMessage, chatClientMessage: ChatClientMessage) -> ChatClientEvent {
        if chatMessage.isSent {
            let receiverUID: Int64?
            
            switch chatMessage {
            case is ChatInfoMessage:
                receiverUID = self.accountProvider.model.access?.userUID
                
            default:
                receiverUID = nil
            }
            
            return ChatClientEvent(method: .update(message: chatClientMessage),
                                   chatUID: chatMessage.chatUID,
                                   senderUID: chatMessage.userUID,
                                   receiverUID: receiverUID)
            
        } else {
            return ChatClientEvent(method: .publish(message: chatClientMessage),
                                   chatUID: chatMessage.chatUID,
                                   senderUID: chatMessage.userUID,
                                   receiverUID: nil)
        }
    }
    
    // MARK: -
    
    fileprivate func fail(chatMessages: [ChatMessage]) -> Guarantee<Void> {
        return Guarantee(resolver: { completion in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                for chatMessage in chatMessages {
                    if !chatMessage.isSent {
                        chatMessage.isFailed = true
                    }
                }
                
                cacheSession.model.viewContext.save()
                
                completion(Void())
            }
        })
    }
    
    fileprivate func store(photoData: Data) -> Promise<URL> {
        return Promise(resolver: { seal in
            DispatchQueue.global(qos: .userInitiated).async(execute: {
                do {
                    let photoPathURL = URL(fileURLWithPath: NSTemporaryDirectory())
                    let photoFileURL = photoPathURL.appendingPathComponent("\(UUID().uuidString).jpg")
                    
                    try photoData.write(to: photoFileURL)
                    
                    DispatchQueue.main.async(execute: {
                        seal.fulfill(photoFileURL)
                    })
                } catch let error {
                    DispatchQueue.main.async(execute: {
                        seal.reject(error)
                    })
                }
            })
        })
    }
    
    fileprivate func upload(photoData: Data, for chatPhotoMessages: [ChatPhotoMessage]) -> Promise<Void> {
        return Promise(resolver: { seal in
            let multiPartItem = WebMultiPartDataItem(data: photoData,
                                                     name: "image",
                                                     fileName: "photo.jpg",
                                                     mimeType: "image/jpg")
            
            firstly {
                self.apiWebService.jsonObject(uploadingMultiPart: [multiPartItem], to: "v1/pictures")
            }.done { response in
                do {
                    guard let photo = self.photoCoder.photo(from: response) else {
                        throw WebError.badResponse
                    }
                
                    firstly {
                        self.cacheProvider.captureModel()
                    }.done { cacheSession in
                        for chatPhotoMessage in chatPhotoMessages {
                            chatPhotoMessage.photo = photo
                            
                            if let photoLocalURL = chatPhotoMessage.photoLocalURL {
                                try? FileManager.default.removeItem(at: photoLocalURL)
                                
                                chatPhotoMessage.photoLocalURL = nil
                            }
                        }
                        
                        cacheSession.model.viewContext.save()
                    
                        seal.fulfill(Void())
                    }
                } catch let error {
                    firstly {
                        self.fail(chatMessages: chatPhotoMessages)
                    }.done {
                        seal.reject(error)
                    }
                }
            }.catch { error in
                firstly {
                    self.fail(chatMessages: chatPhotoMessages)
                }.done {
                    seal.reject(error)
                }
            }
        })
    }
    
    fileprivate func send(chatMessage: ChatMessage) -> Promise<Void> {
        return Promise(resolver: { seal in
            do {
                guard let chat = self.cacheProvider.model.viewContext.chatsManager.first(with: chatMessage.chatUID) else {
                    throw ChatClientError.badRequest
                }
                
                guard let chatClientMessage = self.chatMessageCoder.encode(chatMessage: chatMessage) else {
                    throw ChatClientError.badRequest
                }
                
                firstly {
                    self.apiChatClient.send(event: self.event(for: chatMessage, chatClientMessage: chatClientMessage),
                                            pushPayload: self.pushPayload(for: chatMessage, chat: chat),
                                            to: chat)
                }.done {
                    if !chatMessage.isSent {
                        self.chatsService.update(chat: chat).cauterize()
                    }
                    
                    firstly {
                        self.cacheProvider.captureModel()
                    }.done { cacheSession in
                        chatMessage.isSent = true
                        chatMessage.isFailed = false
                    
                        cacheSession.model.viewContext.save()
                    
                        seal.fulfill(Void())
                    }
                }.catch { error in
                    firstly {
                        self.fail(chatMessages: [chatMessage])
                    }.done {
                        seal.reject(error)
                    }
                }
            } catch let error {
                firstly {
                    self.fail(chatMessages: [chatMessage])
                }.done {
                    seal.reject(error)
                }
            }
        })
    }
    
    // MARK: - ChatMessagesService
    
    func resend(chatMessage: ChatMessage) -> Promise<Void> {
        chatMessage.isFailed = false
        
        switch chatMessage {
        case let chatPhotoMessage as ChatPhotoMessage:
            if let photoLocalURL = chatPhotoMessage.photoLocalURL {
                return Promise(resolver: { seal in
                    DispatchQueue.global(qos: .userInitiated).async(execute: {
                        do {
                            let photoData = try Data(contentsOf: photoLocalURL)
                            
                            DispatchQueue.main.async(execute: {
                                firstly {
                                    self.upload(photoData: photoData, for: [chatPhotoMessage])
                                }.then {
                                    self.send(chatMessage: chatPhotoMessage)
                                }.done {
                                    seal.fulfill(Void())
                                }.catch { error in
                                    seal.reject(error)
                                }
                            })
                        } catch let error {
                            DispatchQueue.main.async(execute: {
                                seal.reject(error)
                            })
                        }
                    })
                })
            } else {
                return self.send(chatMessage: chatMessage)
            }
            
        default:
            return self.send(chatMessage: chatMessage)
        }
    }
    
    func delete(chatMessage: ChatMessage) -> Promise<Void> {
        return Promise(resolver: { seal in
            do {
                guard let accountUserUID = self.accountProvider.model.access?.userUID else {
                    throw WebError.unauthorized
                }
                
                guard chatMessage.userUID == accountUserUID else {
                    throw WebError.access
                }
                
                if chatMessage.isSent {
                    if chatMessage.isErased {
                        seal.fulfill(Void())
                    } else {
                        chatMessage.erasedDate = Date()

                        firstly {
                            self.resend(chatMessage: chatMessage)
                        }.done {
                            seal.fulfill(Void())
                        }.catch { error in
                            seal.reject(error)
                        }
                    }
                } else {
                    firstly {
                        self.cacheProvider.captureModel()
                    }.done { cacheSession in
                        cacheSession.model.viewContext.chatMessagesManager.remove(object: chatMessage)
                        cacheSession.model.viewContext.save()

                        seal.fulfill(Void())
                    }
                }
            } catch let error {
                seal.reject(error)
            }
        })
    }
    
    func markAsViewed(chatMessage: ChatMessage) -> Promise<Void> {
        return Promise(resolver: { seal in
            do {
                guard let accountUserUID = self.accountProvider.model.access?.userUID else {
                    throw WebError.unauthorized
                }
                
                if chatMessage.isViewed || (chatMessage.userUID == accountUserUID) {
                    seal.fulfill(Void())
                } else {
                    chatMessage.viewedDate = Date()
                    
                    if let chatMessageViewedDate = chatMessage.viewedDate {
                        
                        //print("#*# ...**...DefaultChatMessageServices markAsViewed -TimeStamp: ", chatMessageViewedDate)
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
                        let createdDateFormatted = dateFormatter.string(from: chatMessageViewedDate)
                        //print("#*# ...**...DefaultChatMessageServices markAsViewed -date: \(createdDateFormatted)\n")
                    }
                    
                    firstly {
                        self.resend(chatMessage: chatMessage)
                    }.done {
                        seal.fulfill(Void())
                    }.catch { error in
                        seal.reject(error)
                    }
                }
            } catch let error {
                seal.reject(error)
            }
        })
    }
    
    func send(text: String, isAnonymous: Bool, to chats: [Chat]) -> Promise<[ChatTextMessage]> {
        return Promise(resolver: { seal in
            do {
                guard let accountUserUID = self.accountProvider.model.access?.userUID else {
                    throw WebError.unauthorized
                }
                
                firstly {
                    self.cacheProvider.captureModel()
                }.done { cacheSession in
                    let chatTextMessages: [ChatTextMessage] = chats.map({ chat in
                        let chatTextMessage = cacheSession.model.viewContext.chatTextMessagesManager.append()
                        
                        chatTextMessage.uid = UUID().uuidString
                        
                        chatTextMessage.chatUID = chat.uid
                        chatTextMessage.userUID = accountUserUID
                        
                        chatTextMessage.createdDate = Date()
                        
                        if let chatTextMessageCreatedDate = chatTextMessage.createdDate {
                            
                            //print("#*# ...**...DefaultChatMessageServices TextMessage -TimeStamp: ", chatTextMessageCreatedDate)
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
                            let createdDateFormatted = dateFormatter.string(from: chatTextMessageCreatedDate)
                            //print("#*# ...**...DefaultChatMessageServices TextMessage -createdDate: ", createdDateFormatted)
                        }
                        
                        chatTextMessage.text = text
                        chatTextMessage.isAnonymous = isAnonymous
                        
                        return chatTextMessage
                    })
                    
                    cacheSession.model.viewContext.save()
                    
                    DispatchQueue.main.async(execute: {
                        firstly {
                            when(fulfilled: chatTextMessages.map({ chatTextMessage in
                                self.resend(chatMessage: chatTextMessage)
                            }))
                        }.done {
                            seal.fulfill(chatTextMessages)
                        }.catch { error in
                            seal.reject(error)
                        }
                    })
                }
            } catch let error {
                seal.reject(error)
            }
        })
    }
    
    func send(text: String, isAnonymous: Bool, to chat: Chat) -> Promise<ChatTextMessage> {
        return self.send(text: text, isAnonymous: isAnonymous, to: [chat]).firstValue
    }
    
    func send(photoData: Data, isAnonymous: Bool, to chats: [Chat]) -> Promise<[ChatPhotoMessage]> {
        return Promise(resolver: { seal in
            do {
                guard let accountUserUID = self.accountProvider.model.access?.userUID else {
                    throw WebError.unauthorized
                }
                
                firstly {
                    self.store(photoData: photoData)
                }.done { photoLocalURL in
                    firstly {
                        self.cacheProvider.captureModel()
                    }.done { cacheSession in
                        let chatPhotoMessages: [ChatPhotoMessage] = chats.map({ chat in
                            let chatPhotoMessage = cacheSession.model.viewContext.chatPhotoMessagesManager.append()
                        
                            chatPhotoMessage.uid = UUID().uuidString
                            
                            chatPhotoMessage.chatUID = chat.uid
                            chatPhotoMessage.userUID = accountUserUID
                            
                            chatPhotoMessage.createdDate = Date()
                            
                            if let chatPhotoMessageCreatedDate = chatPhotoMessage.createdDate {
                                
                                //print("#*# ...**...DefaultChatMessageServices Photo -TimeStamp: ", chatPhotoMessageCreatedDate)
                                
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
                                let createdDateFormatted = dateFormatter.string(from: chatPhotoMessageCreatedDate)
                                //print("#*# ...**...DefaultChatMessageServices Photo -createdDate: ", createdDateFormatted)
                            }
                            
                            chatPhotoMessage.photoLocalURL = photoLocalURL
                            
                            chatPhotoMessage.isAnonymous = isAnonymous
                            
                            return chatPhotoMessage
                        })
                        
                        cacheSession.model.viewContext.save()
                        
                        DispatchQueue.main.async(execute: {
                            firstly {
                                self.upload(photoData: photoData, for: chatPhotoMessages)
                            }.then {
                                when(fulfilled: chatPhotoMessages.map({ chatPhotoMessage in
                                    self.resend(chatMessage: chatPhotoMessage)
                                }))
                            }.done {
                                seal.fulfill(chatPhotoMessages)
                            }.catch { error in
                                seal.reject(error)
                            }
                        })
                    }
                }.catch { error in
                    seal.reject(error)
                }
            } catch let error {
                seal.reject(error)
            }
        })
    }
    
    func send(photoData: Data, isAnonymous: Bool, to chat: Chat) -> Promise<ChatPhotoMessage> {
        return self.send(photoData: photoData, isAnonymous: isAnonymous, to: [chat]).firstValue
    }
}
