//
//  ChatMessagesService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 30.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol ChatMessagesService {
    
    // MARK: - Instance Methods
    
    func resend(chatMessage: ChatMessage) -> Promise<Void>
    func delete(chatMessage: ChatMessage) -> Promise<Void>
    
    func markAsViewed(chatMessage: ChatMessage) -> Promise<Void>
    
    func send(text: String, isAnonymous: Bool, to chats: [Chat]) -> Promise<[ChatTextMessage]>
    func send(text: String, isAnonymous: Bool, to chat: Chat) -> Promise<ChatTextMessage>
    
    func send(photoData: Data, isAnonymous: Bool, to chats: [Chat]) -> Promise<[ChatPhotoMessage]>
    func send(photoData: Data, isAnonymous: Bool, to chat: Chat) -> Promise<ChatPhotoMessage>
}
