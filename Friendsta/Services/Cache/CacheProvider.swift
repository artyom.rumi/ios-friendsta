//
//  CacheProvider.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol CacheProvider {
    
    // MARK: - Instance Properties
    
    var isModelCaptured: Bool { get }
    
    var model: CacheModel { get }
    
    // MARK: - Instance Methods
    
    func captureModel() -> Guarantee<CacheSession>
}
