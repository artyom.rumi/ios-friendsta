//
//  DefaultCacheProvider.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

class DefaultCacheProvider: CacheProvider {
    
    // MARK: - Instance Properties
    
    fileprivate var captureResolvers: [(CacheSession) -> Void] = []
    
    // MARK: - CacheProvider
    
    fileprivate(set) var isModelCaptured = false
    
    let model: CacheModel
    
    // MARK: - Initializers
    
    init(model: CacheModel) {
        self.model = model
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createSession() -> CacheSession {
        return DefaultCacheSession(model: self.model, releaseHandler: { [weak self] in
            self?.resolveNextCapture()
        })
    }
    
    fileprivate func resolveNextCapture() {
        if !self.captureResolvers.isEmpty {
            self.isModelCaptured = true
            
            self.captureResolvers.removeFirst()(self.createSession()) // FIXME: - CRASH HERE
        } else {
            self.isModelCaptured = false
        }
    }
    
    // MARK: - AccountModelService
    
    func captureModel() -> Guarantee<CacheSession> {
        return Guarantee(resolver: { completion in
            if self.isModelCaptured {
                self.captureResolvers.append(completion)
            } else {
                self.isModelCaptured = true
                
                completion(self.createSession())
            }
        })
    }
}
