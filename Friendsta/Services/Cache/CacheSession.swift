//
//  CacheSession.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol CacheSession: class {
    
    // MARK: - Instance Properties
    
    var model: CacheModel { get }
}
