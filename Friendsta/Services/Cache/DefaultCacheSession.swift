//
//  DefaultCacheSession.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

class DefaultCacheSession: CacheSession {
    
    // MARK: - Instance Properties
    
    let releaseHandler: () -> Void
    
    // MARK: - AccountModelSession
    
    fileprivate(set) var model: CacheModel
    
    // MARK: - Initializers
    
    init(model: CacheModel, releaseHandler: @escaping (() -> Void)) {
        self.releaseHandler = releaseHandler
        
        self.model = model
    }
    
    deinit {
        self.releaseHandler()
    }
}
