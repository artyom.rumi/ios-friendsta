//
//  PropRequestsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 18.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol PropRequestsService {
    
    // MARK: - Instance Methods
    
    func refreshPropRequests() -> Promise<[PropRequest]>
    
    func requestProps() -> Promise<Void>
}
