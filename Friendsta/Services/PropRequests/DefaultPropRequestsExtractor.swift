//
//  DefaultPropRequestsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultPropRequestsExtractor: PropRequestsExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let propRequestCoder: PropRequestCoder
    
    // MARK: - Instance Methods
    
    @discardableResult
    fileprivate func extractPropRequest(from response: [String: Any], context: CacheModelContext) throws -> PropRequest {
        guard let propRequestUID = self.propRequestCoder.propRequestUID(from: response) else {
            throw WebError.badResponse
        }
        
        let propRequestsManager = context.propRequestsManager
        
        let propRequest = propRequestsManager.first(with: propRequestUID) ?? propRequestsManager.append()
        
        guard self.propRequestCoder.decode(propRequest: propRequest, from: response) else {
            throw WebError.badResponse
        }
        
        return propRequest
    }
    
    @discardableResult
    fileprivate func extractPropRequests(from response: [[String: Any]], context: CacheModelContext) throws -> [PropRequest] {
        let propRequests = try response.map({ response in
            return try self.extractPropRequest(from: response, context: context)
        })
        
        let propRequestsManager = context.propRequestsManager
        
        for propRequest in propRequestsManager.fetch() {
            if !propRequests.contains(object: propRequest) {
                propRequestsManager.remove(object: propRequest)
            }
        }
        
        return propRequests
    }
    
    // MARK: - PropRequestsExtractor
    
    func extractPropRequest(from response: [String: Any]) -> Promise<PropRequest> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let propRequestUID = try self.extractPropRequest(from: response, context: backgroundContext).uid
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.propRequestsManager.first(with: propRequestUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractPropRequests(from response: [[String: Any]]) -> Promise<[PropRequest]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        try self.extractPropRequests(from: response, context: backgroundContext)
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.propRequestsManager.fetch())
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
