//
//  DefaultPropRequestsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 18.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultPropRequestsService: PropRequestsService {
    
    // MARK: - Instance Properties
    
    let apiWebService: APIWebService
    let propRequestsExtractor: PropRequestsExtractor
    
    // MARK: - Instance Methods
    
    func refreshPropRequests() -> Promise<[PropRequest]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/incoming_requests"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.propRequestsExtractor.extractPropRequests(from: response)
            }.done { propRequests in
                seal.fulfill(propRequests)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func requestProps() -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .post, path: "v1/requests"))
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                seal.reject(error)
            }
        })
    }
}
