//
//  PropRequestsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol PropRequestsExtractor {
    
    // MARK: - Instance Methods
    
    func extractPropRequest(from response: [String: Any]) -> Promise<PropRequest>
    func extractPropRequests(from response: [[String: Any]]) -> Promise<[PropRequest]>
}
