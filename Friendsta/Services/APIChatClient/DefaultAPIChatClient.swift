//
//  APIChatClient.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 27.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//  swiftlint:disable function_body_length

import Foundation
import PromiseKit
import FriendstaTools
import FriendstaNetwork

class DefaultAPIChatClient: APIChatClient {
    
    // MARK: - Instance Properties
    
    let accountProvider: AccountProvider
    let cacheProvider: CacheProvider
    let chatClient: ChatClient
    let chatMessageCoder: ChatMessageCoder
    
    // MARK: - Initializers
    
    init(accountProvider: AccountProvider, cacheProvider: CacheProvider, chatClient: ChatClient, chatMessageCoder: ChatMessageCoder) {
        self.accountProvider = accountProvider
        self.cacheProvider = cacheProvider
        self.chatClient = chatClient
        self.chatMessageCoder = chatMessageCoder
    }
    
    // MARK: - Instance Methods
    
    fileprivate func extractPublishedChatTextMessage(from message: ChatClientMessage, context: CacheModelContext) -> ChatTextMessage? {
        guard let chatMessageUID = self.chatMessageCoder.chatMessageUID(from: message) else {
            return nil
        }
        
        let chatTextMessagesManager = context.chatTextMessagesManager
        
        let chatTextMessage = chatTextMessagesManager.first(with: chatMessageUID) ?? chatTextMessagesManager.append()
        
        if chatTextMessage.createdDate == nil {
            let currentDate: TimeInterval = Date().timeIntervalSince1970
            let currentDateAsDate: Date = Date(timeIntervalSince1970: currentDate)
            let setNewDateAsDate: Date? = Calendar.current.date(byAdding: .second, value: -1, to: currentDateAsDate)
            chatTextMessage.createdDate = setNewDateAsDate ?? Date()
        }
        
        if let chatTextMessageCreatedDate = chatTextMessage.createdDate {
            let doubleCheckDate = chatTextMessageCreatedDate
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
            let createdDateFormatted = dateFormatter.string(from: doubleCheckDate)
            //print("#*# @@__@@ -Z- DefaultAPIChatClient -chatTextMessageDate: \(createdDateFormatted)\n")
        }
        //print("#*# ??__?? Z DefaultAPIChatClient -chatTextMessageDate: \(chatTextMessage.createdDate as Any)\n")
        
        guard self.chatMessageCoder.decode(chatMessage: chatTextMessage, from: message) else {
            chatTextMessagesManager.remove(object: chatTextMessage)
            
            return nil
        }
        
        return chatTextMessage
    }
    
    fileprivate func extractPublishedChatPhotoMessage(from message: ChatClientMessage, context: CacheModelContext) -> ChatPhotoMessage? {
        guard let chatMessageUID = self.chatMessageCoder.chatMessageUID(from: message) else {
            return nil
        }
        
        let chatPhotoMessagesManager = context.chatPhotoMessagesManager
        
        let chatPhotoMessage = chatPhotoMessagesManager.first(with: chatMessageUID) ?? chatPhotoMessagesManager.append()
        
        if chatPhotoMessage.createdDate == nil {
            let currentDate: TimeInterval = Date().timeIntervalSince1970
            let currentDateAsDate: Date = Date(timeIntervalSince1970: currentDate)
            let setNewDateAsDate: Date? = Calendar.current.date(byAdding: .second, value: -1, to: currentDateAsDate)
            chatPhotoMessage.createdDate = setNewDateAsDate ?? Date()
        }
        
        if let chatPhotoMessageCreatedDate = chatPhotoMessage.createdDate {
            let doubleCheckDate = chatPhotoMessageCreatedDate
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
            let createdDateFormatted = dateFormatter.string(from: doubleCheckDate)
            //print("#*# @@__@@ -X- DefaultAPIChatClient -chatPhotoMessageDate: \(createdDateFormatted)\n")
        }
        //print("#*# ??__?? X DefaultAPIChatClient -chatPhotoMessageDate: \(chatPhotoMessage.createdDate as Any)\n")
        
        guard self.chatMessageCoder.decode(chatMessage: chatPhotoMessage, from: message) else {
            chatPhotoMessagesManager.remove(object: chatPhotoMessage)
            
            return nil
        }
        
        return chatPhotoMessage
    }
    
    fileprivate func extractPublishedChatInfoMessage(from message: ChatClientMessage, context: CacheModelContext) -> ChatInfoMessage? {
        guard let chatMessageUID = self.chatMessageCoder.chatMessageUID(from: message) else {
            return nil
        }
        
        let chatInfoMessagesManager = context.chatInfoMessagesManager
        
        let chatInfoMessage = chatInfoMessagesManager.first(with: chatMessageUID) ?? chatInfoMessagesManager.append()
        
        if chatInfoMessage.createdDate == nil {
            let currentDate: TimeInterval = Date().timeIntervalSince1970
            let currentDateAsDate: Date = Date(timeIntervalSince1970: currentDate)
            let setNewDateAsDate: Date? = Calendar.current.date(byAdding: .second, value: -1, to: currentDateAsDate)
            chatInfoMessage.createdDate = setNewDateAsDate ?? Date()
        }
        
        if let chatInfoMessageCreatedDate = chatInfoMessage.createdDate {
            let doubleCheckDate = chatInfoMessageCreatedDate
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
            let createdDateFormatted = dateFormatter.string(from: doubleCheckDate)
            //print("#*# @@__@@ Y DefaultAPIChatClient -chatInfoMessageDate: \(createdDateFormatted)\n")
        }
        //print("#*# ??__?? Y DefaultAPIChatClient -chatInfoMessageDate: \(chatInfoMessage.createdDate as Any)\n")
        
        guard self.chatMessageCoder.decode(chatMessage: chatInfoMessage, from: message) else {
            chatInfoMessagesManager.remove(object: chatInfoMessage)
            
            return nil
        }
        
        return chatInfoMessage
    }
    
    fileprivate func extractPublishedChatMessage(from message: ChatClientMessage, context: CacheModelContext) -> ChatMessage? {
        guard let chatMessageType = self.chatMessageCoder.chatMessageType(from: message) else {
            return nil
        }
        
        switch chatMessageType {
        case .text:
            return self.extractPublishedChatTextMessage(from: message, context: context)
            
        case .photo:
            return self.extractPublishedChatPhotoMessage(from: message, context: context)
            
        case .info:
            return self.extractPublishedChatInfoMessage(from: message, context: context)
        }
    }
    
    // MARK: -
    
    fileprivate func extractUpdatedChatTextMessage(from message: ChatClientMessage, context: CacheModelContext) -> ChatTextMessage? {
        guard let chatMessageUID = self.chatMessageCoder.chatMessageUID(from: message) else {
            return nil
        }
        
        let chatTextMessagesManager = context.chatTextMessagesManager
        
        guard let chatTextMessage = chatTextMessagesManager.first(with: chatMessageUID) else {
            return nil
        }
        
        guard self.chatMessageCoder.decode(chatMessage: chatTextMessage, from: message) else {
            return nil
        }
        
        return chatTextMessage
    }
    
    fileprivate func extractUpdatedChatPhotoMessage(from message: ChatClientMessage, context: CacheModelContext) -> ChatPhotoMessage? {
        guard let chatMessageUID = self.chatMessageCoder.chatMessageUID(from: message) else {
            return nil
        }
        
        let chatPhotoMessagesManager = context.chatPhotoMessagesManager
        
        guard let chatPhotoMessage = chatPhotoMessagesManager.first(with: chatMessageUID) else {
            return nil
        }
        
        guard self.chatMessageCoder.decode(chatMessage: chatPhotoMessage, from: message) else {
            return nil
        }
        
        return chatPhotoMessage
    }
    
    fileprivate func extractUpdatedChatInfoMessage(from message: ChatClientMessage, context: CacheModelContext) -> ChatInfoMessage? {
        guard let chatMessageUID = self.chatMessageCoder.chatMessageUID(from: message) else {
            return nil
        }
        
        let chatInfoMessagesManager = context.chatInfoMessagesManager
        
        guard let chatInfoMessage = chatInfoMessagesManager.first(with: chatMessageUID) else {
            return nil
        }
        
        guard self.chatMessageCoder.decode(chatMessage: chatInfoMessage, from: message) else {
            return nil
        }
        
        return chatInfoMessage
    }
    
    fileprivate func extractUpdatedChatMessage(from message: ChatClientMessage, context: CacheModelContext) -> ChatMessage? {
        guard let chatMessageType = self.chatMessageCoder.chatMessageType(from: message) else {
            return nil
        }
        
        switch chatMessageType {
        case .text:
            return self.extractUpdatedChatTextMessage(from: message, context: context)
            
        case .photo:
            return self.extractUpdatedChatPhotoMessage(from: message, context: context)
            
        case .info:
            return self.extractUpdatedChatInfoMessage(from: message, context: context)
        }
    }
    
    // MARK: -
    
    fileprivate func handle(publishedMessage: ChatClientMessage, event: ChatClientEvent, for userUID: Int64, chat: Chat, context: CacheModelContext) throws {
        if chat.isBlocked {
            if let chatMessageUID = self.chatMessageCoder.chatMessageUID(from: publishedMessage) {
                context.chatMessagesManager.clear(with: chatMessageUID)
            }
        } else {
            if let chatMessage = self.extractPublishedChatMessage(from: publishedMessage, context: context) {
                chatMessage.chatUID = event.chatUID
                chatMessage.userUID = event.senderUID ?? 0
                
                chatMessage.isSent = true
                chatMessage.isFailed = false
                
                if (userUID != event.senderUID) && (!chatMessage.isViewed) && (!chatMessage.isErased) {
                    chat.isViewed = false

                    if let chatInfoMessage = chatMessage as? ChatInfoMessage {
                        let opponentChatMember = chat.members?.first(where: { chatMember in
                            return ((chatMember as! ChatMember).user?.uid != userUID)
                        }) as? ChatMember

                        let currentUserChatMember = chat.members?.first(where: { chatMember in
                            return ((chatMember as! ChatMember).user?.uid == userUID)
                        }) as? ChatMember

                        switch chatInfoMessage.type {
                        case .cancelReveal:
                            opponentChatMember?.isDeanonymized = false

                        case .match:
                            opponentChatMember?.isDeanonymized = true
                            opponentChatMember?.isDeanonymized = true

                        case .mutualReveal:
                            currentUserChatMember?.isDeanonymized = true
                            opponentChatMember?.isDeanonymized = true

                        case .oneReveal:
                            opponentChatMember?.isDeanonymized = true

                        case .none:
                            opponentChatMember?.isDeanonymized = false
                            currentUserChatMember?.isDeanonymized = false
                        }
                    }
                }
                
                chat.isErased = false
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
                
                if let chatModifiedDate = chat.modifiedDate {
                    
                    //print("#*# ...**...DefaultAPIChatClient X chatModifiedDate -TimeStamp: ", chatModifiedDate)
                    let chatModifiedDateFormatted = dateFormatter.string(from: chatModifiedDate)
                    //print("#*# ...**...DefaultChatMessageServices X chatModifiedDate -date: \(chatModifiedDateFormatted)")
                    
                    if let chatMessageCreatedDate = chatMessage.createdDate, chatModifiedDate < chatMessageCreatedDate {
                        chat.modifiedDate = chatMessageCreatedDate
                        
                        //rint("#*# ...**...DefaultAPIChatClient Y chatMessageCreatedDate -TimeStamp: ", chatMessageCreatedDate)
                        let chatMessageCreatedDateFormatted = dateFormatter.string(from: chatMessageCreatedDate)
                        //print("#*# ...**...DefaultChatMessageServices Y chatMessageCreatedDate -date: \(chatMessageCreatedDateFormatted)")
                    }
                } else {
                    chat.modifiedDate = chatMessage.createdDate
                    
                    //print("#*# ...**...DefaultAPIChatClient Z chatModifiedDate -TimeStamp: ", chatMessage.createdDate as Any)
                    if let chatModifiedDateFormatted = chatMessage.createdDate {
                        dateFormatter.string(from: chatModifiedDateFormatted)
                        //print("#*# ...**...DefaultChatMessageServices Z chatModifiedDate -date: \(chatModifiedDateFormatted)\n")
                    }
                }
            }
        }
    }
    
    fileprivate func handle(updatedMessage: ChatClientMessage, event: ChatClientEvent, for userUID: Int64, chat: Chat, context: CacheModelContext) throws {
        if let chatMessage = self.extractUpdatedChatMessage(from: updatedMessage, context: context) {
            chatMessage.chatUID = event.chatUID
            chatMessage.userUID = event.senderUID ?? 0
            
            chatMessage.isSent = true
            chatMessage.isFailed = false
            
            if (userUID != event.senderUID) && (!chatMessage.isViewed) && (!chatMessage.isErased) {
                chat.isViewed = false
            }
        }
    }
    
    fileprivate func handle(event: ChatClientEvent, for userUID: Int64, chat: Chat, context: CacheModelContext) throws {
        switch event.method {
        case .publish(let message):
            try self.handle(publishedMessage: message, event: event, for: userUID, chat: chat, context: context)
            
        case .update(let message):
            try self.handle(updatedMessage: message, event: event, for: userUID, chat: chat, context: context)
            
        case .block:
            chat.isBlocked = true
            
        case .unblock:
            chat.isBlocked = false
            
        case .restore:
            chat.isErased = false
            
        case .delete:
            context.chatMessagesManager.clear(withChatUID: chat.uid)
            
            chat.isErased = true
            
        case .clear:
            context.chatMessagesManager.clear(withChatUID: chat.uid)
            
            chat.isErased = false
        }
    }
    
    fileprivate func handle(events: [ChatClientEvent], for userUID: Int64, chat: Chat, context: CacheModelContext) throws {
        if (context.chatMessagesManager.count(withChatUID: chat.uid) == 0) && (chat.members?.count == 2) {
            chat.isBlocked = chat.members?.contains(where: { chatMember in
                let chatMember = chatMember as! ChatMember
                
                if chat.isIncognito {
                    return (chatMember.mask?.isBlocked ?? false)
                } else {
                    return (chatMember.user?.isBlocked ?? false)
                }
            }) ?? false
        }
        
        for event in events {
            if event.chatUID == chat.uid {
                try self.handle(event: event, for: userUID, chat: chat, context: context)
            }
        }
        
        if !chat.isViewed {
            let chatMessages = context.chatMessagesManager.fetch(withChatUID: chat.uid)
            
            chat.isViewed = !chatMessages.contains(where: { chatMessage in
                if chatMessage.userUID != userUID {
                    return (!chatMessage.isViewed) && (!chatMessage.isErased)
                } else {
                    return false
                }
            })
        }
    }
    
    // MARK: -
    
    fileprivate func obtainHistory(from chatChannel: String, startTime: Int64, endTime: Int64) -> Promise<[ChatClientEvent]> {
        return Promise(resolver: { seal in
            do {
                guard let accountUserUID = self.accountProvider.model.access?.userUID else {
                    throw WebError.unauthorized
                }
                
                self.chatClient.history(on: chatChannel, startTime: startTime, endTime: endTime, completion: { history, error in
                    if let history = history {
                        let events: [ChatClientEvent] = history.events.compactMap({ event in
                            if (event.receiverUID == nil) || (event.receiverUID == accountUserUID) {
                                return event
                            } else {
                                return nil
                            }
                        })
                        
                        if (history.startTime < history.endTime) && (history.endTime > startTime) {
                            firstly {
                                self.obtainHistory(from: chatChannel, startTime: history.endTime, endTime: endTime)
                            }.done { otherEvents in
                                seal.fulfill(events + otherEvents)
                            }.catch { error in
                                seal.reject(error)
                            }
                        } else {
                            seal.fulfill(events)
                        }
                    } else {
                        seal.reject(error ?? ChatClientError.badResponse)
                    }
                })
            } catch let error {
                seal.reject(error)
            }
        })
    }
    
    fileprivate func deployHistory(events: [ChatClientEvent], for userUID: Int64, historyTime: Int64?) -> Promise<Void> {
        return Promise(resolver: { seal in
            if events.isEmpty {
                seal.fulfill(Void())
            } else {
                firstly {
                    self.cacheProvider.captureModel()
                }.done { cacheSession in
                    let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                    
                    backgroundContext.perform(block: {
                        do {
                            guard let chatUID = events.first?.chatUID else {
                                throw ChatClientError.badResponse
                            }
                            
                            guard let chat = backgroundContext.chatsManager.first(with: chatUID) else {
                                throw ChatClientError.badResponse
                            }
                            
                            try self.handle(events: events, for: userUID, chat: chat, context: backgroundContext)
                            
                            if let historyTime = historyTime {
                                chat.historyTime = historyTime
                            }
                            
                            backgroundContext.save()
                            
                            cacheSession.model.viewContext.performAndWait(block: {
                                cacheSession.model.viewContext.save()
                                
                                seal.fulfill(Void())
                            })
                        } catch let error {
                            DispatchQueue.main.async(execute: {
                                seal.reject(error)
                            })
                        }
                    })
                }
            }
        })
    }
    
    // MARK: - APIChatClient
    
    func send(event: ChatClientEvent, pushPayload: ChatClientPushPayload?, to chat: Chat) -> Promise<Void> {
        return Promise(resolver: { seal in
            do {
                guard let chatChannel = chat.channel else {
                    throw ChatClientError.badRequest
                }
                
                self.chatClient.send(event: event, pushPayload: pushPayload, to: chatChannel, completion: { error in
                    if let error = error {
                        seal.reject(error)
                    } else {
                        seal.fulfill(Void())
                    }
                })
            } catch let error {
                seal.reject(error)
            }
        })
    }
    
    func refreshHistory(of chat: Chat) -> Promise<[ChatMessage]> {
        return Promise(resolver: { seal in
            do {
                guard let accountUserUID = self.accountProvider.model.access?.userUID else {
                    throw WebError.unauthorized
                }
                
                guard let chatChannel = chat.channel else {
                    throw ChatClientError.badRequest
                }
                
                let nextHistoryTime: Int64 = Int64(Date().timeIntervalSinceReferenceDate) * 10000000
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
                
                let nextHistoryTimeDateFormatted = dateFormatter.string(from: Date(timeIntervalSince1970: Double(nextHistoryTime)))
                let chatHistoryTimeDateFormatted = dateFormatter.string(from: Date(timeIntervalSince1970: Double(chat.historyTime)))
                
                //print("#*# ...**...DefaultAPIChatClient nextHistoryTime -date: ", nextHistoryTimeDateFormatted)
                //print("#*# ...**...DefaultAPIChatClient chat.historyTime -date: \(chatHistoryTimeDateFormatted)\n")
                
                firstly {
                    self.obtainHistory(from: chatChannel, startTime: chat.historyTime, endTime: nextHistoryTime)
                }.then { events in
                    self.deployHistory(events: events, for: accountUserUID, historyTime: nextHistoryTime)
                }.done {
                    seal.fulfill(self.cacheProvider.model.viewContext.chatMessagesManager.fetch(withChatUID: chat.uid))
                }.catch { error in
                    seal.reject(error)
                }
            } catch let error {
                seal.reject(error)
            }
        })
    }
    
    func subscribe(to chats: [Chat]) {
        self.chatClient.subscribe(to: chats.compactMap({ chat in
            return chat.channel
        }))
    }
    
    func unsubscribe(from chats: [Chat]) {
        self.chatClient.unsubscribe(from: chats.compactMap({ chat in
            return chat.channel
        }))
    }
    
    func startObserving() {
        self.chatClient.addObserver(self)
    }
    
    func stopObserving() {
        self.chatClient.removeObserver(self)
    }
}

// MARK: - ChatClientObserver

extension DefaultAPIChatClient: ChatClientObserver {
    
    // MARK: - Instance Methods
    
    func chatClient(_ chatClient: ChatClient, didReceiveEvent event: ChatClientEvent) {
        Log.low("chatClientDidReceive(event: \(event.logDescription))", from: self)
        
        guard let accountUserUID = self.accountProvider.model.access?.userUID else {
            return
        }
        
        guard (event.receiverUID == nil) || (event.receiverUID == accountUserUID) else {
            return
        }
        
        self.deployHistory(events: [event], for: accountUserUID, historyTime: nil).cauterize()
    }
}
