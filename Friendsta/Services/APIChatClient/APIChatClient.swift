//
//  APIChatClient.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 28.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol APIChatClient {
    
    // MARK: - Instance Methods
    
    func send(event: ChatClientEvent, pushPayload: ChatClientPushPayload?, to chat: Chat) -> Promise<Void>
    
    func refreshHistory(of chat: Chat) -> Promise<[ChatMessage]>
    
    func subscribe(to chats: [Chat])
    func unsubscribe(from chats: [Chat])
    
    func startObserving()
    func stopObserving()
}
