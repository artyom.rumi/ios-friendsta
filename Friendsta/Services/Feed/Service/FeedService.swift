//
//  FeedService.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10.06.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol FeedService {
    
    // MARK: - Instance Methods
    
    func sendFeed(photo: UIImage, isIncognito: Bool, isPublic: Bool) -> Promise<Feed>
    func sendFeed(text: String?, link: URL) -> Promise<Void>
    
    func changeStatus(feed: Feed, isIncognito: Bool) -> Promise<Feed>
    
    func fetchFeed() -> Promise<FeedList>
    func fetchFeed(with feedID: Int64) -> Promise<Feed>
    
    func fetchDiscover() -> Promise<FeedList>
    
    func fetchTimelineFeeds() -> Promise<FeedList>
    func fetchAnonymousFeeds() -> Promise<FeedList>

    func fetchTimelineFeeds(for userUID: User.UID) -> Promise<FeedList>
    
    func delete(feed: Feed) -> Promise<Void>

    func putLike(for feedUID: Feed.UID) -> Promise<Feed>
    func removeLike(for feedUID: Feed.UID) -> Promise<Feed>

    func repost(feed: Feed) -> Promise<Feed>
    
    // added by lava on 0120
    func putRates(for feedUID: Feed.UID, rates: Int64) -> Promise<Feed>
    
}

// MARK: -

extension FeedService {
    
    // MARK: - Instance Methods
    
    func sendFeed(photo: UIImage, isIncognito: Bool = true) -> Promise<Feed> {
        self.sendFeed(photo: photo, isIncognito: isIncognito, isPublic: false)
    }
}
