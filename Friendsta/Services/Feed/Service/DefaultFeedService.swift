//
//  DefaultFeedService.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10.06.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import ImageIO
import MobileCoreServices
import PromiseKit
import FriendstaNetwork

struct DefaultFeedService: FeedService {
    
    // MARK: - Instance Properties
    
    let apiWebService: APIWebService

    let feedExtractor: FeedExtractor
    
    // MARK: - Instance Methods

    private func createProgressiveImage(from photo: UIImage) -> Data? {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first

        guard let imageURL = documentsURL?.appendingPathComponent("progressive.jpg") else {
            return nil
        }

        let targetURL = imageURL as CFURL

        guard let destination = CGImageDestinationCreateWithURL(targetURL, kUTTypeJPEG, 1, nil) else {
            return nil
        }

        guard let cfBooleanTrue = kCFBooleanTrue else {
            return nil
        }

        let jfifProperties = [kCGImagePropertyJFIFIsProgressive: cfBooleanTrue] as NSDictionary

        let properties = [
            kCGImageDestinationLossyCompressionQuality: Limits.photoImageQuality,
            kCGImagePropertyJFIFDictionary: jfifProperties
        ] as NSDictionary

        guard let cgImage = photo.cgImage else {
            return nil
        }

        CGImageDestinationAddImage(destination, cgImage, properties)
        CGImageDestinationFinalize(destination)

        return try? Data(contentsOf: imageURL)
    }

    // MARK: -
    
    func sendFeed(photo: UIImage, isIncognito: Bool, isPublic: Bool) -> Promise<Feed> {
        return Promise(resolver: { seal in
            guard let data = self.createProgressiveImage(from: photo) else {
                return seal.reject(WebError.aborted)
            }

            let multiPartItem = WebMultiPartDataItem(data: data,
                                                     name: "image",
                                                     fileName: "feed.jpg",
                                                     mimeType: "image/jpg")

            firstly {
                self.apiWebService.jsonObject(uploadingMultiPart: [multiPartItem],
                                              params: ["incognito": isIncognito, "ispublic": isPublic],
                                              to: "v1/my/feeds")
            }.then { response in
                self.feedExtractor.extractFeed(from: response)
            }.done { feed in
                seal.fulfill(feed)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func sendFeed(text: String?, link: URL) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                               path: "v1/my/feeds",
                                                               params: ["text": text ?? "", "link": link, "incognito": true]))
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func changeStatus(feed: Feed, isIncognito: Bool) -> Promise<Feed> {
        return Promise(resolver: { seal in
            let requestParameters = ["incognito": isIncognito]
            
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .put,
                                                               path: "v1/my/feeds/\(feed.uid)",
                                                               params: requestParameters))
            }.then { response in
                self.feedExtractor.extractFeed(from: response)
            }.done { feed in
                seal.fulfill(feed)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func fetchFeed() -> Promise<FeedList> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/my/feeds"))
            }.then {
                response in
                self.feedExtractor.extractFeedList(from: response, listType: .all)
            }.done { feedList in
                seal.fulfill(feedList)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func fetchDiscover() -> Promise<FeedList> {
        return Promise(resolver: { seal in
            let requestParameters: [String: Any] = ["incognito": false]
            
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get,
                                                              path: "v1/my/discoveries",
                                                              params: requestParameters))
            }.then { response in
                self.feedExtractor.extractFeedList(from: response, listType: .discover)
            }.done { feedList in
                seal.fulfill(feedList)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func fetchFeed(with feedID: Int64) -> Promise<Feed> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/my/feeds/\(feedID)"))
            }.then { response in
                self.feedExtractor.extractFeed(from: response)
            }.done { feed in
                seal.fulfill(feed)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func fetchTimelineFeeds() -> Promise<FeedList> {
        return Promise(resolver: { seal in
            let requestParameters: [String: Any] = ["incognito": false]
            
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get,
                                                              path: "v1/my/own_feeds",
                                                              params: requestParameters))
            }.then { response in
                self.feedExtractor.extractFeedList(from: response, listType: .timeline)
            }.done { feedList in
                seal.fulfill(feedList)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func fetchAnonymousFeeds() -> Promise<FeedList> {
        return Promise(resolver: { seal in
            let requestParameters: [String: Any] = ["incognito": true]
            
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get,
                                                              path: "v1/my/own_feeds",
                                                              params: requestParameters))
            }.then { response in
                self.feedExtractor.extractFeedList(from: response, listType: .anonymous)
            }.done { feedList in
                seal.fulfill(feedList)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func fetchTimelineFeeds(for userUID: User.UID) -> Promise<FeedList> {
        return Promise(resolver: { seal in
            let requestParameters: [String: Any] = ["user_id": userUID]

            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get,
                                                              path: "v1/users/\(userUID)/timeline",
                    params: requestParameters))
            }.then { response in
                self.feedExtractor.extractFeedList(from: response, listType: .timeline)
            }.done { feedList in
                seal.fulfill(feedList)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func delete(feed: Feed) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .delete, path: "v1/my/feeds/\(feed.uid)"))
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func putLike(for feedUID: Feed.UID) -> Promise<Feed> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post, path: "v1/feeds/\(feedUID)/likes"))
            }.then { response in
                self.feedExtractor.extractFeed(from: response)
            }.done { feed in
                seal.fulfill(feed)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func removeLike(for feedUID: Feed.UID) -> Promise<Feed> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .delete, path: "v1/feeds/\(feedUID)/likes"))
            }.then { response in
                self.feedExtractor.extractFeed(from: response)
            }.done { feed in
                seal.fulfill(feed)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func repost(feed: Feed) -> Promise<Feed> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post, path: "v1/feeds/\(feed.uid)/reposts"))
            }.then { response in
                self.feedExtractor.extractFeed(from: response)
            }.done { feed in
                seal.fulfill(feed)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    // added by lava on 0120
    func putRates(for feedUID: Feed.UID, rates: Int64) -> Promise<Feed> {

        return Promise(resolver: { seal in
            
            let requestParameters = ["rate": rates]

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(
                    method: .post,
                    path: "v1/feeds/\(feedUID)/rates",
                    params: requestParameters
                ))
            }.then { response in
                self.feedExtractor.extractFeed(from: response)
            }.done { feed in
                seal.fulfill(feed)
            }.catch { error in
                seal.reject(error)
            }
        })
    }}
    // ended by lava on 0120
