//
//  DefaultFeedExtractor.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//
// swiftlint:disable for_where

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultFeedExtractor: FeedExtractor {

    // MARK: - Instance Properties

    let cacheProvider: CacheProvider

    let feedCoder: FeedCoder
    let chatExtractor: ChatsExtractor

    // MARK: - Instance Methods
    fileprivate func printDictValues(from arr: [[String: Any]]) {
        
        arr.forEach { (dict) in
            
            for (key, value) in dict {
                
                if key == "chat" {
//                    print(key)
                    if let json = value as? [String: Any] {
                        
                        for (k, v) in json {
//                            print(k)
//                            print(v as Any)
                        }
                    }
                }
            }
        }
    }
    
    @discardableResult
    private func extractFeedList(from response: [[String: Any]], listType: FeedListType, context: CacheModelContext) throws -> FeedList {
        let feedList = context.feedListManager.firstOrNew(withListType: listType)
        
        printDictValues(from: response)
        
        feedList.clearFeeds()
        
//        print("0000000000000000000000000 this is the feedlist extract part 000000000000000000000000000")
//        print(response)

        try response.forEach { feedList.addToRawFeeds(try self.extractFeed(from: $0, context: context)) }

        return feedList
    }

    @discardableResult
    private func extractFeed(from response: [String: Any], context: CacheModelContext) throws -> Feed {
//        print("111111111111111111111111111 this is the feed extract  1111111111111111111111111111")
        guard let feedUID = self.feedCoder.uid(from: response) else {
            throw WebError.badResponse
        }

        let feed = context.feedManager.firstOrNew(withUID: feedUID)
        
        guard self.feedCoder.decode(feed: feed, from: response) else {
            throw WebError.badResponse
        }
        
        let user = context.accountUsersManager.first()
//        print("333333333333333333333333 this is the account user 33333333333333333333333333333")
//        print(user?.uid as Any)
        
        feed.isRated = false
        
        if feed.userUID == user?.uid {
            feed.isRated = true
        } else {
            for i in response["rates"] as! [[String: AnyObject]] {
//                print(i["user_id"] as Any)
                let val = i["user_id"] as? Int64
                if val == user?.uid {
                    feed.isRated = true
                    break
                }
            }
        }

        if let chatJson = response["chat"] as? [String: Any] {
            if let chat = try? self.chatExtractor.extractChat(from: chatJson, context: context) {
                feed.chat = chat
            }
        }
        
//        print("2222222222222222222222222 this is the feed extract  2222222222222222222222222")
//        print(feed)
        
        return feed
    }

    // MARK: - FeedExtractor

    func extractFeedList(from response: [[String: Any]], listType: FeedListType) -> Promise<FeedList> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        try self.extractFeedList(from: response, listType: listType, context: backgroundContext)

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            seal.fulfill(cacheSession.model.viewContext.feedListManager.first(with: listType)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }

    func extractFeed(from response: [String: Any]) -> Promise<Feed> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        let feedUID = try self.extractFeed(from: response, context: backgroundContext).uid

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            seal.fulfill(cacheSession.model.viewContext.feedManager.first(with: feedUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
