//
//  FeedExtractor.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol FeedExtractor {

    // MARK: - Instance Methods

    func extractFeedList(from response: [[String: Any]], listType: FeedListType) -> Promise<FeedList>
    func extractFeed(from response: [String: Any]) -> Promise<Feed>
}
