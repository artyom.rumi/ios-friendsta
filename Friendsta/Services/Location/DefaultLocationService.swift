//
//  DefaultLocationService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import CoreLocation
import PromiseKit
import FriendstaTools

fileprivate extension LocationAccessState {
    
    // MARK: - Initializers
    
    init(from accessState: CLAuthorizationStatus) {
        switch accessState {
        case .notDetermined:
            self = .notDetermined
            
        case .authorizedAlways, .authorizedWhenInUse:
            self = .authorized
            
        case .restricted:
            self = .restricted
            
        case .denied:
            self = .denied
        }
    }
}

// MARK: -

class DefaultLocationService: NSObject, LocationService {
    
    // MARK: - Instance Properties
    
    fileprivate var accessRequestCompletion: ((_ accessState: LocationAccessState) -> Void)?
    fileprivate var locationRequestCompletion: ((_ location: Location?) -> Void)?
    
    fileprivate let locationManager = CLLocationManager()
    
    // MARK: - LocationService
    
    var accessState: LocationAccessState {
        if CLLocationManager.locationServicesEnabled() {
            return LocationAccessState(from: CLLocationManager.authorizationStatus())
        } else {
            return .notAvailable
        }
    }
    
    // MARK: - Initializers
    
    override init() {
        super.init()
        
        self.locationManager.delegate = self
    }
    
    // MARK: - Instance Methods
    
    func requestAccess() -> Guarantee<LocationAccessState> {
        return Guarantee(resolver: { completion in
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined:
                    self.accessRequestCompletion = { accessState in
                        completion(accessState)
                    }
                    
                    self.locationManager.requestWhenInUseAuthorization()
                    
                case .authorizedAlways, .authorizedWhenInUse:
                    completion(.authorized)
                    
                case .restricted:
                    completion(.restricted)
                    
                case .denied:
                    completion(.denied)
                }
            } else {
                completion(.notAvailable)
            }
        })
    }
    
    func requestLocation() -> Guarantee<Location?> {
        return Guarantee(resolver: { completion in
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    completion(nil)
                    
                case .authorizedAlways, .authorizedWhenInUse:
                    self.locationRequestCompletion = { location in
                        completion(location)
                    }
                    
                    self.locationManager.requestLocation()
                }
            }
        })
    }
}

// MARK: - CLLocationManagerDelegate

extension DefaultLocationService: CLLocationManagerDelegate {
    
    // MARK: - Instance Methods
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        Log.low("locationManagerDidUpdateLocations(\(locations))", from: self)
        
        if let locationRequestCompletion = self.locationRequestCompletion {
            self.locationRequestCompletion = nil
            
            if let coordinate = locations.first?.coordinate {
                locationRequestCompletion(Location(latitude: Double(coordinate.latitude),
                                                   longitude: Double(coordinate.longitude)))
            } else {
                locationRequestCompletion(nil)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Log.low("locationManagerDidFailWithError(\(error))", from: self)
        
        if let locationRequestCompletion = self.locationRequestCompletion {
            self.locationRequestCompletion = nil
            
            locationRequestCompletion(nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        Log.low("locationManagerDidChangeAuthorization(\(status))", from: self)
        
        switch status {
        case .notDetermined:
            break
            
        default:
            if let accessRequestCompletion = self.accessRequestCompletion, status != .notDetermined {
                self.accessRequestCompletion = nil
                
                accessRequestCompletion(LocationAccessState(from: status))
            }
        }
    }
}
