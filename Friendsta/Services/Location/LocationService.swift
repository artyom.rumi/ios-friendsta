//
//  LocationService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol LocationService {
    
    // MARK: - Instance Methods
    
    var accessState: LocationAccessState { get }
    
    // MARK: - Instance Methods
    
    func requestAccess() -> Guarantee<LocationAccessState>
    func requestLocation() -> Guarantee<Location?>
}
