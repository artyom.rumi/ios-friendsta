//
//  PropReceiversExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol PropReceiversExtractor {
    
    // MARK: - Instance Methods
    
    func extractPropReceiver(from response: [String: Any], propReceiverUID: Int64, propRequestUID: Int64?) -> Promise<PropReceiver>
    func extractPropReceivers(from response: [[String: Any]], propRequestUID: Int64?) -> Promise<[PropReceiver]>
}
