//
//  DefaultPropReceiversService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultPropReceiversService: PropReceiversService {

    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let apiWebService: APIWebService
    let propRequestsService: PropRequestsService
    let usersService: UsersService
    let propReceiversExtractor: PropReceiversExtractor
    
    // MARK: - Instance Methods
    
    fileprivate func reduceRandomPropReceivers(minCount: Int, context: CacheModelContext) {
        let propReceiversManager = context.propReceiversManager
        
        var propReceivers = propReceiversManager.fetch()
        
        while (propReceivers.count > minCount) && (!propReceivers.last!.hasRequest) {
            propReceiversManager.remove(object: propReceivers.removeLast())
        }
    }
    
    fileprivate func reduceRandomPropReceivers(minCount: Int) -> Guarantee<[PropReceiver]> {
        return Guarantee(resolver: { completion in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    self.reduceRandomPropReceivers(minCount: minCount, context: backgroundContext)
                    
                    backgroundContext.save()
                    
                    cacheSession.model.viewContext.performAndWait(block: {
                        cacheSession.model.viewContext.save()
                        
                        completion(cacheSession.model.viewContext.propReceiversManager.fetch())
                    })
                })
            }
        })
    }
    
    fileprivate func remove(propReceiver propReceiverUID: Int64, context: CacheModelContext) {
        let propReceiversManager = context.propReceiversManager
        
        for propReceiver in propReceiversManager.fetch(with: propReceiverUID) {
            if let propRequest = propReceiver.request, (propRequest.receivers?.count ?? 0) <= 1 {
                context.propRequestsManager.remove(object: propRequest)
            }
            
            propReceiversManager.remove(object: propReceiver)
        }
    }
    
    fileprivate func remove(propReceiver: PropReceiver) -> Promise<Void> {
        return Promise(resolver: { seal in
            let propReceiverUID = propReceiver.uid
            
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
            
                backgroundContext.perform(block: {
                    self.remove(propReceiver: propReceiverUID, context: backgroundContext)
                    
                    backgroundContext.save()
                    
                    cacheSession.model.viewContext.performAndWait(block: {
                        cacheSession.model.viewContext.save()
                        
                        seal.fulfill(Void())
                    })
                })
            }
        })
    }
    
    fileprivate func refreshPropReceivers(with propRequests: [PropRequest], cachedPropReceivers: [PropReceiver]?) -> Promise<[PropReceiver]> {
        return Promise(resolver: { seal in
            var propRequests = propRequests
            
            if propRequests.isEmpty {
                seal.fulfill(cachedPropReceivers ?? self.cacheProvider.model.viewContext.propReceiversManager.fetch())
            } else {
                let propRequestUID = propRequests.removeFirst().uid
                
                firstly {
                    self.apiWebService.jsonArray(with: WebRequest(method: .get,
                                                                  path: "v1/requests/\(propRequestUID)/respondents"),
                                                 queue: DispatchQueue.global(qos: .userInitiated))
                }.then { response in
                    self.propReceiversExtractor.extractPropReceivers(from: response, propRequestUID: propRequestUID)
                }.then { propReceivers in
                    self.refreshPropReceivers(with: propRequests, cachedPropReceivers: propReceivers)
                }.done { propReceivers in
                    seal.fulfill(propReceivers)
                }.catch { error in
                    DispatchQueue.main.async(execute: {
                        seal.reject(error)
                    })
                }
            }
        })
    }
    
    fileprivate func refreshRandomPropReceivers(minCount: Int, cachedPropReceivers: [PropReceiver]?) -> Promise<[PropReceiver]> {
        return Promise(resolver: { seal in
            let propReceivers = cachedPropReceivers ?? self.cacheProvider.model.viewContext.propReceiversManager.fetch()
            
            if propReceivers.count < minCount {
                firstly {
                    self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/random_friends"),
                                                 queue: DispatchQueue.global(qos: .userInitiated))
                }.done { response in
                    if response.isEmpty {
                        DispatchQueue.main.async(execute: {
                            seal.fulfill(propReceivers)
                        })
                    } else {
                        firstly {
                            self.propReceiversExtractor.extractPropReceivers(from: response, propRequestUID: nil)
                        }.then { propReceivers in
                            self.refreshRandomPropReceivers(minCount: minCount, cachedPropReceivers: propReceivers)
                        }.done { propReceivers in
                            seal.fulfill(propReceivers)
                        }.catch { error in
                            DispatchQueue.main.async(execute: {
                                seal.reject(error)
                            })
                        }
                    }
                }.catch { error in
                    DispatchQueue.main.async(execute: {
                        seal.reject(error)
                    })
                }
            } else {
                seal.fulfill(propReceivers)
            }
        })
    }
    
    // MARK: - PropReceiversService
    
    func refreshPropReceivers(minCount: Int) -> Promise<[PropReceiver]> {
        return Promise(resolver: { seal in
            firstly {
                self.propRequestsService.refreshPropRequests()
            }.done { propRequests in
                let newPropRequests = propRequests.filter({ propRequest in
                    if propRequest.progress < propRequest.count {
                        return ((propRequest.receivers?.count ?? 0) == 0)
                    } else {
                        return false
                    }
                })
                
                if newPropRequests.isEmpty {
                    firstly {
                        self.refreshRandomPropReceivers(minCount: minCount, cachedPropReceivers: nil)
                    }.done { propReceivers in
                        seal.fulfill(propReceivers)
                    }.catch { error in
                        seal.reject(error)
                    }
                } else {
                    firstly {
                        self.reduceRandomPropReceivers(minCount: minCount)
                    }.then { propReceivers in
                        self.refreshPropReceivers(with: newPropRequests, cachedPropReceivers: propReceivers)
                    }.then { propReceivers in
                        self.refreshRandomPropReceivers(minCount: minCount, cachedPropReceivers: propReceivers)
                    }.done { propReceivers in
                        seal.fulfill(propReceivers)
                    }.catch { error in
                        seal.reject(error)
                    }
                }
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func send(prop: Prop, to propReceiver: PropReceiver, boostType: BoostType?) -> Promise<Void> {
        if let userUID = propReceiver.user?.uid {
            if let propRequestUID = propReceiver.request?.uid {
                return Promise(resolver: { seal in
                    firstly {
                        self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                                       path: "v1/requests/\(propRequestUID)/prop_deliveries",
                                                                       params: ["recipient_id": userUID, "prop_id": prop.uid, "boost_type": boostType?.rawValue ?? "secret"]))
                    }.then { response in
                        self.remove(propReceiver: propReceiver)
                    }.done {
                        seal.fulfill(Void())
                    }.catch { error in
                        seal.reject(error)
                    }
                })
            } else {
                return Promise(resolver: { seal in
                    firstly {
                        self.apiWebService.json(with: WebRequest(method: .post,
                                                                 path: "v1/prop_deliveries",
                                                                 params: ["recipient_id": userUID, "prop_id": prop.uid, "boost_type": boostType?.rawValue ?? "secret"]))
                    }.then { response in
                        self.usersService.refresh(userUID: userUID)
                    }.then { user in
                        self.remove(propReceiver: propReceiver)
                    }.done {
                        seal.fulfill(Void())
                    }.catch { error in
                        seal.reject(error)
                    }
                })
            }
        } else {
            return self.remove(propReceiver: propReceiver)
        }
    }
    
    func sendCustomProp(content: String, to propReceiver: PropReceiver) -> Promise<Void> {
        if let userUID = propReceiver.user?.uid {
            return Promise(resolver: { seal in
                firstly {
                    self.apiWebService.json(with: WebRequest(method: .post,
                                                             path: "v1/custom_prop_deliveries",
                                                             params: ["recipient_id": userUID, "content": content]))
                }.then { response in
                    self.usersService.refresh(userUID: userUID)
                }.then { user in
                    self.remove(propReceiver: propReceiver)
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            })
        } else {
            return self.remove(propReceiver: propReceiver)
        }
    }
    
    func skip(propReceiver: PropReceiver) -> Promise<Void> {
        if let propRequestUID = propReceiver.request?.uid {
            return Promise(resolver: { seal in
                firstly {
                    self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                                   path: "v1/requests/\(propRequestUID)/skip_deliveries"))
                }.then { response in
                    self.remove(propReceiver: propReceiver)
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            })
        } else {
            return self.remove(propReceiver: propReceiver)
        }
    }
}
