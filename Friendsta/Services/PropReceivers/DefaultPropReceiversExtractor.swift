//
//  DefaultPropReceiversExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultPropReceiversExtractor: PropReceiversExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let userCoder: UserCoder
    
    // MARK: - Instance Methods
    
    @discardableResult
    fileprivate func extractPropReceiver(from response: [String: Any], propReceiverUID: Int64, propRequest: PropRequest?, context: CacheModelContext) throws -> PropReceiver {
        guard let userUID = self.userCoder.userUID(from: response) else {
            throw WebError.badResponse
        }
        
        let usersManager = context.usersManager
        
        let user = usersManager.first(with: userUID) ?? usersManager.append()
        
        guard self.userCoder.decode(user: user, from: response) else {
            throw WebError.badResponse
        }
        
        let propReceiversManager = context.propReceiversManager
        
        let propReceiver = propReceiversManager.first(with: propReceiverUID) ?? propReceiversManager.append()
        
        propReceiver.uid = propReceiverUID
        propReceiver.user = user
        propReceiver.request = propRequest
        
        return propReceiver
    }
    
    @discardableResult
    fileprivate func extractPropReceivers(from response: [[String: Any]], propRequest: PropRequest?, context: CacheModelContext) throws -> [PropReceiver] {
        var propReceivers: [PropReceiver] = []
        
        for response in response {
            let propReceiverUID = ((propReceivers.last ?? context.propReceiversManager.last())?.uid ?? 0) + 1
            
            let propReceiver = try self.extractPropReceiver(from: response,
                                                            propReceiverUID: propReceiverUID,
                                                            propRequest: propRequest,
                                                            context: context)
            
            propReceivers.append(propReceiver)
        }
        
        return propReceivers
    }
    
    // MARK: - PropReceiversExtractor
    
    func extractPropReceiver(from response: [String: Any], propReceiverUID: Int64, propRequestUID: Int64?) -> Promise<PropReceiver> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let propRequest: PropRequest?
                        
                        if let propRequestUID = propRequestUID {
                            propRequest = backgroundContext.propRequestsManager.first(with: propRequestUID)
                        } else {
                            propRequest = nil
                        }
                        
                        let propReceiverUID = try self.extractPropReceiver(from: response,
                                                                           propReceiverUID: propReceiverUID,
                                                                           propRequest: propRequest,
                                                                           context: backgroundContext).uid
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.propReceiversManager.first(with: propReceiverUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractPropReceivers(from response: [[String: Any]], propRequestUID: Int64?) -> Promise<[PropReceiver]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let propRequest: PropRequest?
                        
                        if let propRequestUID = propRequestUID {
                            propRequest = backgroundContext.propRequestsManager.first(with: propRequestUID)
                        } else {
                            propRequest = nil
                        }
                        
                        try self.extractPropReceivers(from: response,
                                                      propRequest: propRequest,
                                                      context: backgroundContext)
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.propReceiversManager.fetch())
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
