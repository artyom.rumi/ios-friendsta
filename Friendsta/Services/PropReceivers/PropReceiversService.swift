//
//  PropReceiversService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

enum BoostType: String {
    case secret
    case friend
}

protocol PropReceiversService {
    
    // MARK: - Instance Methods
    
    func refreshPropReceivers(minCount: Int) -> Promise<[PropReceiver]>
    
    func send(prop: Prop, to propReceiver: PropReceiver, boostType: BoostType?) -> Promise<Void> 
    func sendCustomProp(content: String, to propReceiver: PropReceiver) -> Promise<Void>
    func skip(propReceiver: PropReceiver) -> Promise<Void>
}
