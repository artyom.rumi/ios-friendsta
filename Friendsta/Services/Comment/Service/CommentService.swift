//
//  CommentService.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol CommentService {

    // MARK: - Instance Methods

    func fetchComments(for feedUID: Int64) -> Promise<CommentList>
    func postComment(with text: String, for feedUID: Int64) -> Promise<CommentList>
    func editComment(with commentUID: Int64, for feedUID: Int64, newText: String) -> Promise<Comment>
    func deleteComment(with commentUID: Int64, for feedUID: Int64) -> Promise<CommentList>
    func markAsRead(commentUIDs: [Int64], for feedUID: Int64) -> Promise<Feed>
}
