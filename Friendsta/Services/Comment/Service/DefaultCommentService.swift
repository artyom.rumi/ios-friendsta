//
//  DefaultCommentService.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultCommentService: CommentService {

    // MARK: - Instance Properties

    let apiWebService: APIWebService

    let commentExtractor: CommentExtractor
    let feedExtractor: FeedExtractor

    // MARK: - Instance Methods

    func fetchComments(for feedUID: Int64) -> Promise<CommentList> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/feeds/\(feedUID)/comments"))
            }.then { response in
                self.commentExtractor.extractCommentList(from: response, listType: .feed(uid: feedUID))
            }.done { commentList in
                seal.fulfill(commentList)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func postComment(with text: String, for feedUID: Int64) -> Promise<CommentList> {
        return Promise(resolver: { seal in
            let requestParameters: [String: Any] = ["text": text]

            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .post,
                                                              path: "v1/feeds/\(feedUID)/comments",
                                                              params: requestParameters))
            }.then { response in
                self.commentExtractor.extractCommentList(from: response, listType: .feed(uid: feedUID))
            }.done { commentList in
                seal.fulfill(commentList)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func editComment(with commentUID: Int64, for feedUID: Int64, newText: String) -> Promise<Comment> {
        return Promise(resolver: { seal in
            let requestParameters: [String: Any] = ["text": newText]

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .patch,
                                                               path: "v1/feeds/\(feedUID)/comments/\(commentUID)",
                                                               params: requestParameters))
            }.then { response in
                self.commentExtractor.extractComment(from: response)
            }.done { comment in
                seal.fulfill(comment)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func deleteComment(with commentUID: Int64, for feedUID: Int64) -> Promise<CommentList> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .delete,
                                                              path: "v1/feeds/\(feedUID)/comments/\(commentUID)"))
            }.then { response in
                self.commentExtractor.extractCommentList(from: response, listType: .feed(uid: feedUID))
            }.done { commentList in
                seal.fulfill(commentList)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func markAsRead(commentUIDs: [Int64], for feedUID: Int64) -> Promise<Feed> {
        return Promise(resolver: { seal in
            let requestParameters: [String: Any] = ["commentIDs": commentUIDs]

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                               path: "v1/feeds/\(feedUID)/mark_comments",
                                                               params: requestParameters))
            }.then { markCommentsResponse in
                self.fetchComments(for: feedUID).map { _ in markCommentsResponse }
            }.then { markCommentsResponse in
                self.feedExtractor.extractFeed(from: markCommentsResponse)
            }.done { feed in
                seal.fulfill(feed)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
}
