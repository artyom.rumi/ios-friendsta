//
//  DefaultCommentExtractor.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultCommentExtractor: CommentExtractor {

    // MARK: - Instance Properties

    let cacheProvider: CacheProvider

    let commentCoder: CommentCoder
    let userCoder: UserCoder
    let chatCoder: ChatCoder

    let chatExtractor: ChatsExtractor

    // MARK: - Instance Methods

    @discardableResult
    private func extractComment(from response: [String: Any], context: CacheModelContext) throws -> Comment {
        guard let commentUID = self.commentCoder.uid(from: response) else {
            throw WebError.badResponse
        }
        
        guard let userUID = self.commentCoder.userUID(from: response) else {
            throw WebError.badResponse
        }
        
        let comment = context.commentManager.firstOrNew(withUID: commentUID)
        
        let sender = context.usersManager.firstOrNew(withUID: userUID)
                
        comment.sender = sender
        
        if let chatUID = self.commentCoder.chatUID(from: response) {
            let chat = context.chatsManager.firstOrNew(withUID: chatUID)
            
            comment.chat = chat
        }

        guard self.commentCoder.decode(comment: comment, from: response) else {
            throw WebError.badResponse
        }

        return comment
    }

    @discardableResult
    private func extractCommentList(from response: [[String: Any]], listType: CommentListType, context: CacheModelContext) throws -> CommentList {
        let commentList = context.commentListManager.firstOrNew(withListType: listType)

        commentList.clearComments()

        try response.forEach { commentList.addToRawComments(try self.extractComment(from: $0, context: context)) }

        return commentList
    }

    // MARK: - CommentExtractor

    func extractComment(from response: [String: Any]) -> Promise<Comment> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        let commentUID = try self.extractComment(from: response, context: backgroundContext).uid

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            seal.fulfill(cacheSession.model.viewContext.commentManager.first(with: commentUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }

    func extractCommentList(from response: [[String: Any]], listType: CommentListType) -> Promise<CommentList> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        try self.extractCommentList(from: response, listType: listType, context: backgroundContext)

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            seal.fulfill(cacheSession.model.viewContext.commentListManager.first(with: listType)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
