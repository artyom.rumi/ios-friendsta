//
//  CommentExtractor.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol CommentExtractor {

    // MARK: - Instance Methods

    func extractComment(from response: [String: Any]) -> Promise<Comment>
    func extractCommentList(from response: [[String: Any]], listType: CommentListType) -> Promise<CommentList>
}
