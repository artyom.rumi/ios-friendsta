//
//  ContactsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 21.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Contacts
import PromiseKit

protocol ContactsExtractor {
    
    // MARK: - Instance Methods
    
    func extractContacts(from cnContact: CNContact) -> Guarantee<[Contact]>
    func extractContacts(from cnContacts: [CNContact], listType: ContactListType) -> Guarantee<[ContactListItem]>
    func extractContacts(from cnContacts: [ContactListType: [CNContact]]) -> Guarantee<[[ContactListItem]]>
}
