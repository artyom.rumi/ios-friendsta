//
//  ContactsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol ContactsService {
    
    // MARK: - Instance Properties
    
    var accessState: ContactsAccessState { get }
    
    // MARK: - Instance Methods
    
    func requestAccess() -> Guarantee<ContactsAccessState>
    func requestPhoneNumbers() -> Guarantee<[String]?>
    
    func refreshAllContacts() -> Guarantee<[ContactListItem]?>
}
