//
//  DefaultContactsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 21.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Contacts
import PromiseKit

struct DefaultContactsExtractor: ContactsExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    
    // MARK: - Instance Methods
    
    @discardableResult
    fileprivate func extractContacts(from cnContact: CNContact, context: CacheModelContext) -> [Contact] {
        let contactsManager = context.contactsManager
        
        return cnContact.phoneNumbers.map({ phoneNumberLabel in
            let phoneNumber = phoneNumberLabel.value.stringValue
            
            let contactPhoneNumber: String
            
            if phoneNumber.starts(with: "+") {
                contactPhoneNumber = "+\(phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())"
            } else {
                contactPhoneNumber = "\(phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())"
            }
            
            let contact = contactsManager.first(with: contactPhoneNumber) ?? contactsManager.append()
            
            contact.phoneNumber = contactPhoneNumber
            
            contact.avatarURL = nil
            
            contact.firstName = cnContact.givenName
            contact.lastName = cnContact.familyName
            contact.company = cnContact.organizationName
            
            if let firstName = contact.firstName, !firstName.isEmpty {
                if let lastName = contact.lastName, !lastName.isEmpty {
                    contact.displayName = "\(firstName) \(lastName)"
                } else {
                    contact.displayName = firstName
                }
            } else {
                if let lastName = contact.lastName, !lastName.isEmpty {
                    contact.displayName = lastName
                } else {
                    if let company = contact.company, !company.isEmpty {
                        contact.displayName = company
                    } else {
                        contact.displayName = nil
                    }
                }
            }
            
            return contact
        })
    }
    
    @discardableResult
    fileprivate func extractContacts(from cnContacts: [CNContact], listType: ContactListType, context: CacheModelContext) -> [ContactListItem] {
        let contactListsManager = context.contactListsManager
        
        contactListsManager.clear(with: listType)
        
        var contactList: [ContactListItem] = []
        
        for cnContact in cnContacts {
            let contacts = self.extractContacts(from: cnContact, context: context)
            
            for contact in contacts {
                let contactListItem = contactListsManager.append()
                
                contactListItem.contact = contact
                contactListItem.listType = listType
                contactListItem.sorting = Int64(contactList.count)
                
                contactList.append(contactListItem)
            }
        }
        
        return contactList
    }
    
    @discardableResult
    fileprivate func extractContacts(from cnContacts: [ContactListType: [CNContact]], context: CacheModelContext) -> [[ContactListItem]] {
        return cnContacts.compactMap({ listType, cnContacts in
            let contactList = self.extractContacts(from: cnContacts, listType: listType, context: context)
            
            return contactList.isEmpty ? nil : contactList
        })
    }
    
    // MARK: - ContactsExtractor
    
    func extractContacts(from cnContact: CNContact) -> Guarantee<[Contact]> {
        return Guarantee(resolver: { completion in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    self.extractContacts(from: cnContact, context: backgroundContext)
                    
                    backgroundContext.save()
                    
                    cacheSession.model.viewContext.performAndWait(block: {
                        cacheSession.model.viewContext.save()
                        
                        completion(cacheSession.model.viewContext.contactsManager.fetch())
                    })
                })
            }
        })
    }
    
    func extractContacts(from cnContacts: [CNContact], listType: ContactListType) -> Guarantee<[ContactListItem]> {
        return Guarantee(resolver: { completion in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
            
                backgroundContext.perform(block: {
                    self.extractContacts(from: cnContacts, listType: listType, context: backgroundContext)
                    
                    backgroundContext.save()
                    
                    cacheSession.model.viewContext.performAndWait(block: {
                        cacheSession.model.viewContext.save()
                        
                        completion(cacheSession.model.viewContext.contactListsManager.fetch(with: listType))
                    })
                })
            }
        })
    }
    
    func extractContacts(from cnContacts: [ContactListType: [CNContact]]) -> Guarantee<[[ContactListItem]]> {
        return Guarantee(resolver: { completion in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
            
                backgroundContext.perform(block: {
                    for (listType, cnContacts) in cnContacts {
                        self.extractContacts(from: cnContacts, listType: listType, context: backgroundContext)
                    }
                    
                    backgroundContext.save()
                    
                    cacheSession.model.viewContext.performAndWait(block: {
                        cacheSession.model.viewContext.save()
                        
                        completion(cnContacts.compactMap({ listType, cnContacts in
                            let contactList = cacheSession.model.viewContext.contactListsManager.fetch(with: listType)
                            
                            return contactList.isEmpty ? nil : contactList
                        }))
                    })
                })
            }
        })
    }
}
