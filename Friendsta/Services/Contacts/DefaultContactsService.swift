//
//  DefaultContactsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Contacts
import PromiseKit

fileprivate extension ContactsAccessState {
    
    // MARK: - Initializers
    
    fileprivate init(from accessState: CNAuthorizationStatus) {
        switch accessState {
        case .notDetermined:
            self = .notDetermined
            
        case .authorized:
            self = .authorized
            
        case .restricted:
            self = .restricted
            
        case .denied:
            self = .denied
        }
    }
}

// MARK: -

struct DefaultContactsService: ContactsService {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let fieldKeys = [CNContactImageDataAvailableKey,
                                CNContactGivenNameKey,
                                CNContactFamilyNameKey,
                                CNContactOrganizationNameKey,
                                CNContactPhoneNumbersKey]
    }
    
    // MARK: - Instance Properties
    
    fileprivate let contactStore = CNContactStore()
    
    // MARK: -
    
    let contactsExtractor: ContactsExtractor
    
    var accessState: ContactsAccessState {
        return ContactsAccessState(from: CNContactStore.authorizationStatus(for: .contacts))
    }
    
    // MARK: - Instance Methods
    
    func requestAccess() -> Guarantee<ContactsAccessState> {
        return Guarantee(resolver: { completion in
            switch CNContactStore.authorizationStatus(for: .contacts) {
            case .notDetermined:
                self.contactStore.requestAccess(for: .contacts, completionHandler: { access, error in
                    completion(self.accessState)
                })
                
            case .authorized:
                completion(.authorized)
                
            case .restricted:
                completion(.restricted)
                
            case .denied:
                completion(.denied)
            }
        })
    }
    
    func requestPhoneNumbers() -> Guarantee<[String]?> {
        return Guarantee(resolver: { completion in
            switch CNContactStore.authorizationStatus(for: .contacts) {
            case .notDetermined, .restricted, .denied:
                completion(nil)
                
            case .authorized:
                DispatchQueue.global(qos: .userInitiated).async(execute: {
                    do {
                        let fetchRequest = CNContactFetchRequest(keysToFetch: Constants.fieldKeys as [CNKeyDescriptor])
                        
                        var phoneNumbers: [String] = []
                        
                        try self.contactStore.enumerateContacts(with: fetchRequest, usingBlock: { cnContact, stop in
                            phoneNumbers.append(contentsOf: cnContact.phoneNumbers.map({ phoneNumberLabel in
                                let phoneNumber = phoneNumberLabel.value.stringValue
                                
                                if phoneNumber.starts(with: "+") {
                                    return "+\(phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())"
                                } else {
                                    return "\(phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())"
                                }
                            }))
                        })
                        
                        DispatchQueue.main.async(execute: {
                            completion(phoneNumbers)
                        })
                    } catch {
                        DispatchQueue.main.async(execute: {
                            completion(nil)
                        })
                    }
                })
            }
        })
    }
    
    func refreshAllContacts() -> Guarantee<[ContactListItem]?> {
        return Guarantee(resolver: { completion in
            switch CNContactStore.authorizationStatus(for: .contacts) {
            case .notDetermined, .restricted, .denied:
                completion(nil)
                
            case .authorized:
                DispatchQueue.global(qos: .userInitiated).async(execute: {
                    do {
                        let fetchRequest = CNContactFetchRequest(keysToFetch: Constants.fieldKeys as [CNKeyDescriptor])
                        
                        fetchRequest.unifyResults = true
                        fetchRequest.sortOrder = .userDefault
                        
                        var cnContacts: [CNContact] = []
                        
                        try self.contactStore.enumerateContacts(with: fetchRequest, usingBlock: { cnContact, stop in
                            cnContacts.append(cnContact)
                        })
                        
                        DispatchQueue.main.async(execute: {
                            firstly {
                                self.contactsExtractor.extractContacts(from: cnContacts, listType: .all)
                            }.done { contactList in
                                completion(contactList)
                            }
                        })
                    } catch {
                        DispatchQueue.main.async(execute: {
                            completion(nil)
                        })
                    }
                })
            }
        })
    }
}
