//
//  ChatsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol ChatsService {
    
    // MARK: - Instance Methods
    
    func createChat(with users: [User], isIncognito: Bool?, name: String?) -> Promise<Chat>
    func createChat(for feed: Feed) -> Promise<Chat>
    func createChat(for comment: Comment) -> Promise<Chat>
    
    func refreshAllChatList() -> Promise<[ChatListItem]>
    func refresh(chat: Chat) -> Promise<Chat>
    
    func update(chat: Chat) -> Promise<Chat>
    
    func delete(chat: Chat) -> Promise<Void>
    func clear(chat: Chat) -> Promise<Void>
    
    func block(chatMask: ChatMask) -> Promise<Void>
    func unblock(chatMask: ChatMask) -> Promise<Void>
    
    func block(user: User) -> Promise<Void>
    func unblock(user: User) -> Promise<Void>
    
    func deanonymize(for chat: Chat) -> Promise<Void>
    func anonymize(for chat: Chat) -> Promise<Void>
    
    func chat(for uid: Int64) -> Chat?
}

// MARK: -

extension ChatsService {
    
    // MARK: - Instance Methods
    
    func createChat(with users: [User], isIncognito: Bool) -> Promise<Chat> {
        return self.createChat(with: users, isIncognito: isIncognito, name: nil)
    }
    
    func createChat(with users: [User], name: String) -> Promise<Chat> {
        return self.createChat(with: users, isIncognito: nil, name: name)
    }
}
