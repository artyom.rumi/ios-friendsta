//
//  DefaultChatsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultChatsService: ChatsService {
    
    // MARK: - Instance Properties
    
    let accountProvider: AccountProvider
    let cacheProvider: CacheProvider
    let apiWebService: APIWebService
    let apiChatClient: APIChatClient
    let chatsExtractor: ChatsExtractor
    
    // MARK: - Instance Methods
    
    fileprivate func send(accountUserMethod method: ChatClientMethod, to chat: Chat) -> Promise<Void> {
        return Promise(resolver: { seal in
            do {
                guard let accountUserUID = self.accountProvider.model.access?.userUID else {
                    throw WebError.unauthorized
                }
                
                let event = ChatClientEvent(method: method,
                                            chatUID: chat.uid,
                                            senderUID: accountUserUID,
                                            receiverUID: accountUserUID)
                
                firstly {
                    self.apiChatClient.send(event: event, pushPayload: nil, to: chat)
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            } catch let error {
                seal.reject(error)
            }
        })
    }
    
    // MARK: - ChatsService
    
    func createChat(with users: [User], isIncognito: Bool?, name: String?) -> Promise<Chat> {
        return Promise(resolver: { seal in
            let userUIDs = users.map({ user in
                return user.uid
            })
            
            var requestParameters: [String: Any] = ["user_ids": userUIDs]
            
            if isIncognito != nil {
                requestParameters["incognito"] = isIncognito
            }
            
            if name != nil {
                requestParameters["name"] = name
            }
            
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                               path: "v1/chats",
                                                               params: requestParameters),
                                                               queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.chatsExtractor.extractChat(from: response)
            }.done { chat in
                self.apiChatClient.subscribe(to: [chat])
                
                firstly {
                    self.apiChatClient.refreshHistory(of: chat)
                }.then { chatMessages in
                    self.send(accountUserMethod: .restore, to: chat)
                }.done {
                    if (users.count == 1) && users[0].isBlocked {
                        firstly {
                            self.send(accountUserMethod: .block, to: chat)
                        }.done {
                            seal.fulfill(chat)
                        }.catch { error in
                            seal.reject(error)
                        }
                    } else {
                        seal.fulfill(chat)
                    }
                }.catch { error in
                    seal.reject(error)
                }
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    func createChat(for feed: Feed) -> Promise<Chat> {
        return Promise(resolver: { seal in
            let feedID = (feed.sourceUID != 0) ? feed.sourceUID : feed.uid

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post, path: "v1/feeds/\(feedID)/chats"),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.chatsExtractor.extractChat(from: response)
            }.done { chat in
                self.apiChatClient.subscribe(to: [chat])

                firstly {
                    self.apiChatClient.refreshHistory(of: chat)
                }.then { chatMessages in
                    self.send(accountUserMethod: .restore, to: chat)
                }.done {
                    seal.fulfill(chat)
                }.catch { error in
                    seal.reject(error)
                }
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    func createChat(for comment: Comment) -> Promise<Chat> {
        return Promise(resolver: { seal in
            let commentID = comment.uid

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post, path: "v1/comments/\(commentID)/chats"),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.chatsExtractor.extractChat(from: response)
            }.done { chat in
                self.apiChatClient.subscribe(to: [chat])

                firstly {
                    self.apiChatClient.refreshHistory(of: chat)
                }.then { chatMessages in
                    self.send(accountUserMethod: .restore, to: chat)
                }.done {
                    seal.fulfill(chat)
                }.catch { error in
                    seal.reject(error)
                }
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func refreshAllChatList() -> Promise<[ChatListItem]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/chats"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.chatsExtractor.extractChats(from: response, listType: .all)
            }.done { chatList in
                let chats = chatList.compactMap({ chatListItem in
                    return chatListItem.chat
                })
                
                self.apiChatClient.subscribe(to: chats)
                
                firstly {
                    when(fulfilled: chats.map({ chat in
                        return self.apiChatClient.refreshHistory(of: chat)
                    }))
                }.done { chatMessages in
                    seal.fulfill(chatList)
                }.catch { error in
                    seal.reject(error)
                }
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func refresh(chat: Chat) -> Promise<Chat> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/chats/\(chat.uid)"),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.chatsExtractor.extractChat(from: response)
            }.done { chat in
                self.apiChatClient.subscribe(to: [chat])
                
                firstly {
                    self.apiChatClient.refreshHistory(of: chat)
                }.done { chatMessages in
                    seal.fulfill(chat)
                }.catch { error in
                    seal.reject(error)
                }
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    func update(chat: Chat) -> Promise<Chat> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post, path: "v1/chats/\(chat.uid)/activities"),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.chatsExtractor.extractChat(from: response)
            }.done { chat in
                seal.fulfill(chat)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    func delete(chat: Chat) -> Promise<Void> {
        return self.send(accountUserMethod: .delete, to: chat)
    }
    
    func clear(chat: Chat) -> Promise<Void> {
        return self.send(accountUserMethod: .clear, to: chat)
    }

    func block(chatMask: ChatMask) -> Promise<Void> {
        return Promise(resolver: { seal in
            let chats: [Chat]? = chatMask.members?.compactMap({ chatMember in
                if let chat = (chatMember as! ChatMember).chat, chat.isIncognito {
                    return (chat.members?.count == 2) ? chat : nil
                } else {
                    return nil
                }
            })
            
            if let chats = chats, !chats.isEmpty {
                firstly {
                    when(fulfilled: chats.map({ chat in
                        self.send(accountUserMethod: .block, to: chat)
                    }))
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            } else {
                seal.fulfill(Void())
            }
        })
    }
    
    func unblock(chatMask: ChatMask) -> Promise<Void> {
        return Promise(resolver: { seal in
            let chats: [Chat]? = chatMask.members?.compactMap({ chatMember in
                if let chat = (chatMember as! ChatMember).chat, chat.isIncognito {
                    return (chat.members?.count == 2) ? chat : nil
                } else {
                    return nil
                }
            })
            
            if let chats = chats, !chats.isEmpty {
                firstly {
                    when(fulfilled: chats.map({ chat in
                        self.send(accountUserMethod: .unblock, to: chat)
                    }))
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            } else {
                seal.fulfill(Void())
            }
        })
    }

    func block(user: User) -> Promise<Void> {
        return Promise(resolver: { seal in
            let chats: [Chat]? = user.chatMembers?.compactMap({ chatMember in
                if let chat = (chatMember as! ChatMember).chat, !chat.isIncognito {
                    return (chat.members?.count == 2) ? chat : nil
                } else {
                    return nil
                }
            })
            
            if let chats = chats, !chats.isEmpty {
                firstly {
                    when(fulfilled: chats.map({ chat in
                        self.send(accountUserMethod: .block, to: chat)
                    }))
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            } else {
                seal.fulfill(Void())
            }
        })
    }
    
    func unblock(user: User) -> Promise<Void> {
        return Promise(resolver: { seal in
            let chats: [Chat]? = user.chatMembers?.compactMap({ chatMember in
                if let chat = (chatMember as! ChatMember).chat, !chat.isIncognito {
                    return (chat.members?.count == 2) ? chat : nil
                } else {
                    return nil
                }
            })
            
            if let chats = chats, !chats.isEmpty {
                firstly {
                    when(fulfilled: chats.map({ chat in
                        self.send(accountUserMethod: .unblock, to: chat)
                    }))
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            } else {
                seal.fulfill(Void())
            }
        })
    }
    
    func deanonymize(for chat: Chat) -> Promise<Void> {
        return Promise(resolver: { seal in
            do {
                guard let accountUserUID = self.accountProvider.model.access?.userUID else {
                    throw WebError.unauthorized
                }

                firstly {
                    self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                                   path: "v1/chats/\(chat.uid)/deanonymizations"))
                }.then { response in
                    self.cacheProvider.captureModel()
                }.done { cacheSession in
                    chat.members?.forEach({ chatMember in
                        let chatMember = chatMember as! ChatMember
                        
                        if chatMember.user?.uid == accountUserUID {
                            chatMember.isDeanonymized = true
                        }
                    })

                    cacheSession.model.viewContext.save()

                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            } catch let error {
                seal.reject(error)
            }
        })
    }

    func anonymize(for chat: Chat) -> Promise<Void> {
        return Promise(resolver: { seal in
            do {
                guard let accountUserUID = self.accountProvider.model.access?.userUID else {
                    throw WebError.unauthorized
                }

                firstly {
                    self.apiWebService.json(with: WebRequest(method: .delete,
                                                             path: "v1/chats/\(chat.uid)/deanonymizations"))
                }.then { response in
                    self.cacheProvider.captureModel()
                }.done { cacheSession in
                    chat.members?.forEach({ chatMember in
                        let chatMember = chatMember as! ChatMember

                        if chatMember.user?.uid == accountUserUID {
                            chatMember.isDeanonymized = false
                        }
                    })

                    cacheSession.model.viewContext.save()

                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            } catch let error {
                seal.reject(error)
            }
        })
    }
    
    func chat(for uid: Int64) -> Chat? {
        guard let chat = self.cacheProvider.model.viewContext.chatsManager.first(with: uid) else {
            return nil
        }
        
        return chat
    }
}
