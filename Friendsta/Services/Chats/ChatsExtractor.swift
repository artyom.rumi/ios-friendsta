//
//  ChatsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol ChatsExtractor {
    
    // MARK: - Instance Methods
    
    func extractChat(from response: [String: Any]) -> Promise<Chat>
    func extractChats(from response: [[String: Any]], listType: ChatListType) -> Promise<[ChatListItem]>
    func extractChats(from responses: [ChatListType: [[String: Any]]]) -> Promise<[[ChatListItem]]>
    func extractChat(from response: [String: Any], context: CacheModelContext) throws -> Chat
}
