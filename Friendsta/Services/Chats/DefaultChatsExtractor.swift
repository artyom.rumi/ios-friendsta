//
//  DefaultChatsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultChatsExtractor: ChatsExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let chatCoder: ChatCoder
    
    // MARK: - Instance Methods
    
    @discardableResult
    func extractChat(from response: [String: Any], context: CacheModelContext) throws -> Chat {
        guard let chatUID = self.chatCoder.chatUID(from: response) else {
            throw WebError.badResponse
        }
        
        guard let userUIDs = self.chatCoder.userUIDs(from: response) else {
            throw WebError.badResponse
        }
        
        let chatsManager = context.chatsManager
        let chatMembersManager = context.chatMembersManager
        let chatMasksManager = context.chatMasksManager
        let usersManager = context.usersManager
        
        let chat = chatsManager.first(with: chatUID) ?? chatsManager.append()
        
        chat.owner = usersManager.append()
        
        for userUID in userUIDs {
            if let chatMember = chatMembersManager.first(withUserUID: userUID, chatUID: chatUID) {
                chatMember.mask = chatMasksManager.first(withUserUID: userUID)
                chatMember.chat = chat
            } else {
                let chatMember = chatMembersManager.append()
                let chatMask = chatMasksManager.first(withUserUID: userUID)
                let user = usersManager.first(with: userUID) ?? usersManager.append()
                    
                user.uid = userUID
                
                chatMember.chat = chat
                chatMember.mask = chatMask
                chatMember.user = user
            }
        }
        
        guard self.chatCoder.decode(chat: chat, from: response) else {
            throw WebError.badResponse
        }
        
        return chat
    }
    
    @discardableResult
    fileprivate func extractChats(from response: [[String: Any]], listType: ChatListType, context: CacheModelContext) throws -> [ChatListItem] {
        let chatListsManager = context.chatListsManager
        
        chatListsManager.clear(with: listType)
        
        var chatList: [ChatListItem] = []
        
        for response in response {
            let chat = try self.extractChat(from: response, context: context)
            
            let chatListItem = chatListsManager.append()
            
            chatListItem.chat = chat
            chatListItem.listType = listType
            chatListItem.sorting = Int64(chatList.count)
            
            chatList.append(chatListItem)
        }
        
        return chatList
    }
    
    @discardableResult
    fileprivate func extractChats(from responses: [ChatListType: [[String: Any]]], context: CacheModelContext) throws -> [[ChatListItem]] {
        return try responses.compactMap({ listType, response in
            let chatList = try self.extractChats(from: response, listType: listType, context: context)
            
            return chatList.isEmpty ? nil : chatList
        })
    }
    
    // MARK: - ChatsExtractor
    
    func extractChat(from response: [String: Any]) -> Promise<Chat> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let chatUID = try self.extractChat(from: response, context: backgroundContext).uid
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.chatsManager.first(with: chatUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractChats(from response: [[String: Any]], listType: ChatListType) -> Promise<[ChatListItem]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        try self.extractChats(from: response, listType: listType, context: backgroundContext)
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.chatListsManager.fetch(with: listType))
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractChats(from responses: [ChatListType: [[String: Any]]]) -> Promise<[[ChatListItem]]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
            
                backgroundContext.perform(block: {
                    do {
                        for (listType, response) in responses {
                            try self.extractChats(from: response, listType: listType, context: backgroundContext)
                        }
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(responses.compactMap({ listType, response in
                                let chatList = cacheSession.model.viewContext.chatListsManager.fetch(with: listType)
                                
                                return chatList.isEmpty ? nil : chatList
                            }))
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
