//
//  ChatMasksExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol ChatMasksExtractor {
    
    // MARK: - Instance Methods
    
    func extractChatMask(from response: [String: Any]) -> Promise<ChatMask>
    func extractChatMasks(from response: [[String: Any]]) -> Promise<[ChatMask]>
}
