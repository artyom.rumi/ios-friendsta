//
//  DefaultChatMasksService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 16.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultChatMasksService: ChatMasksService {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let apiWebService: APIWebService
    let chatsService: ChatsService
    let chatMasksExtractor: ChatMasksExtractor
    let chatMaskCoder: ChatMaskCoder
    
    // MARK: - Instance Methods
    
    func refreshChatMasks() -> Promise<[ChatMask]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/collocutor_masks"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.chatMasksExtractor.extractChatMasks(from: response)
            }.done { chatMasks in
                seal.fulfill(chatMasks)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func refresh(chatMask: ChatMask) -> Promise<ChatMask> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/collocutor_masks/\(chatMask.uid)"),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.chatMasksExtractor.extractChatMask(from: response)
            }.done { chatMask in
                seal.fulfill(chatMask)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func block(chatMask: ChatMask) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .post,
                                                         path: "v1/collocutor_masks/\(chatMask.uid)/blocking"))
            }.then { response in
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                chatMask.isBlocked = true
            
                cacheSession.model.viewContext.save()
            
                firstly {
                    self.chatsService.block(chatMask: chatMask)
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            }.catch { error in
                if chatMask.isBlocked {
                    firstly {
                        self.chatsService.block(chatMask: chatMask)
                    }.done {
                        seal.fulfill(Void())
                    }.catch { error in
                        seal.reject(error)
                    }
                } else {
                    seal.reject(error)
                }
            }
        })
    }
    
    func unblock(chatMask: ChatMask) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .delete,
                                                         path: "v1/collocutor_masks/\(chatMask.uid)/blocking"))
            }.then { response in
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                chatMask.isBlocked = false
                
                cacheSession.model.viewContext.save()
                
                firstly {
                    self.chatsService.unblock(chatMask: chatMask)
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            }.catch { error in
                if chatMask.isBlocked {
                    seal.reject(error)
                } else {
                    firstly {
                        self.chatsService.unblock(chatMask: chatMask)
                    }.done {
                        seal.fulfill(Void())
                    }.catch { error in
                        seal.reject(error)
                    }
                }
            }
        })
    }
    
    func update(chatMask: ChatMask) -> Promise<ChatMask> {
        return Promise(resolver: { seal in
            let requestParams = self.chatMaskCoder.encode(chatMask: chatMask)
            
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .patch,
                                                               path: "v1/collocutor_masks/\(chatMask.uid)",
                                                               params: requestParams),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.chatMasksExtractor.extractChatMask(from: response)
            }.done { chatMask in
                seal.fulfill(chatMask)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
}
