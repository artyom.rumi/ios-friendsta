//
//  DefaultChatMasksExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultChatMasksExtractor: ChatMasksExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let chatMaskCoder: ChatMaskCoder
    
    // MARK: - Instance Methods
    
    @discardableResult
    fileprivate func extractChatMask(from response: [String: Any], context: CacheModelContext) throws -> ChatMask {
        guard let chatMaskUID = self.chatMaskCoder.chatMaskUID(from: response) else {
            throw WebError.badResponse
        }
        
        let chatMasksManager = context.chatMasksManager
        
        let chatMask = chatMasksManager.first(with: chatMaskUID) ?? chatMasksManager.append()
        
        guard self.chatMaskCoder.decode(chatMask: chatMask, from: response) else {
            throw WebError.badResponse
        }
        
        return chatMask
    }
    
    @discardableResult
    fileprivate func extractChatMasks(from response: [[String: Any]], context: CacheModelContext) throws -> [ChatMask] {
        let chatMasks = try response.map({ response in
            return try self.extractChatMask(from: response, context: context)
        })
        
        let chatMasksManager = context.chatMasksManager
        
        for chatMask in chatMasksManager.fetch() {
            if !chatMasks.contains(object: chatMask) {
                chatMasksManager.remove(object: chatMask)
            }
        }
        
        return chatMasks
    }
    
    // MARK: - ChatMasksExtractor
    
    func extractChatMask(from response: [String: Any]) -> Promise<ChatMask> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let chatMaskUID = try self.extractChatMask(from: response, context: backgroundContext).uid
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.chatMasksManager.first(with: chatMaskUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractChatMasks(from response: [[String: Any]]) -> Promise<[ChatMask]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        try self.extractChatMasks(from: response, context: backgroundContext)
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.chatMasksManager.fetch())
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
