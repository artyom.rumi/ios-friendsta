//
//  ChatMasksService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 16.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol ChatMasksService {
    
    // MARK: - Instance Methods
    
    func refreshChatMasks() -> Promise<[ChatMask]>
    
    func refresh(chatMask: ChatMask) -> Promise<ChatMask>
    
    func block(chatMask: ChatMask) -> Promise<Void>
    func unblock(chatMask: ChatMask) -> Promise<Void>
    
    func update(chatMask: ChatMask) -> Promise<ChatMask>
}
