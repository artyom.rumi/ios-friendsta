//
//  AuthorizationService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 08.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

protocol AuthorizationService {
    
    // MARK: - Instance Methods
    
    func confirm(phoneNumber: String, with code: String) -> Promise<Access>
    
    func signIn(with phoneNumber: String) -> Promise<AccessSession>
    func signOut() -> Promise<Void>
}

// MARK: -

extension AuthorizationService {
    
    // MARK: - Instance Methods
    
    func confirm(accessSession: AccessSession, with code: String) -> Promise<Access> {
        return self.confirm(phoneNumber: accessSession.phoneNumber, with: code)
    }
}
