//
//  DefaultAuthorizationService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 08.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultAuthorizationService: AuthorizationService {
    
    // MARK: - Instance Properties
    
    let accountProvider: AccountProvider
    let apiWebService: APIWebService
    let accessCoder: AccessCoder
    let accessSessionCoder: AccessSessionCoder
    
    // MARK: - Instance Methods
    
    func confirm(phoneNumber: String, with code: String) -> Promise<Access> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                               path: "v1/auth_tokens",
                                                               params: ["phone_number": phoneNumber,
                                                                        "verification_code": code]))
            }.done { response in
                do {
                    guard let access = self.accessCoder.access(from: response) else {
                        throw WebError.badResponse
                    }
                
                    firstly {
                        self.accountProvider.captureModel()
                    }.done { accountSession in
                        accountSession.model.accessManager.update(access: access)
                        accountSession.model.accessManager.saveAccess()
                        
                        seal.fulfill(access)
                    }
                } catch let error {
                    seal.reject(error)
                }
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func signIn(with phoneNumber: String) -> Promise<AccessSession> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                               path: "v1/verification_codes",
                                                               params: ["phone_number": phoneNumber]))
            }.done { response in
                do {
                    guard let accessSession = self.accessSessionCoder.accessSession(from: response) else {
                        throw WebError.badResponse
                    }
                
                    seal.fulfill(accessSession)
                } catch let error {
                    seal.reject(error)
                }
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func signOut() -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .delete, path: "v1/logout"))
            }.ensure {
                firstly {
                    self.accountProvider.captureModel()
                }.done { accountSession in
                    accountSession.model.accessManager.resetAccess()
                    accountSession.model.accessManager.saveAccess()
                }
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                seal.reject(error)
            }
        })
    }
}
