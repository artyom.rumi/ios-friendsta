//
//  InvitationService.swift
//  Friendsta
//
//  Created by Elina Batyrova on 04.10.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol InvitationService {
    
    // MARK: - Instance Methods
    
    func sendInvite(to contacts: [OutlineContact]) -> Promise<Void>
}
