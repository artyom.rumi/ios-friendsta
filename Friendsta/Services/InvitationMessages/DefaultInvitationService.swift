//
//  DefaultInvitationService.swift
//  Friendsta
//
//  Created by Elina Batyrova on 04.10.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultInvitationService: InvitationService {
    
    // MARK: - Instance Properties

    let apiWebService: APIWebService
    
    // MARK: - Instance Methods
    
    func sendInvite(to contacts: [OutlineContact]) -> Promise<Void> {
        var requestParams = [[String: String]]()
        
        for contact in contacts {
            requestParams.append(["name": contact.fullName ?? contact.phoneNumber ?? "",
                                  "phone_number": contact.phoneNumber ?? ""])
        }
        
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .post,
                                                         path: "v1/invitation",
                                                         params: ["contacts": requestParams]))
            }.done { response in
                seal.fulfill(Void())
                ///// ********* Lance's Code ********* /////
                NotificationCenter.default.post(name: Notification.Name("removeRevealButtonInChatMessagesTVC"), object: nil)
                NotificationCenter.default.post(name: Notification.Name("inviteSentStopTimerDismissOutlineContactsTVC"), object: nil)
                ///// ********* Lance's Code ********* /////
            }.catch { error in
                seal.reject(error)

            }
        })
    }
}
