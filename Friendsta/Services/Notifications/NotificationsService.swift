//
//  NotificationsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

protocol NotificationsService {
    
    // MARK: - Instance Properties
    
    var notificationsBlockers: [NotificationsBlocker] { get }
    
    var isRegisteredForNotifications: Bool { get }
    
    // MARK: - Instance Methods
    
    func addNotificationsBlocker(_ notificationsBlocker: NotificationsBlocker)
    func removeNotificationsBlocker(_ notificationsBlocker: NotificationsBlocker)
    
    func registerForNotifications()
    func unregisterForNotifications()
    
    func requestAccess() -> Guarantee<NotificationsAccessState>
    
    func update(deviceToken: String?) -> Promise<Void>
}
