//
//  DefaultNotificationsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 19.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import UserNotifications
import PromiseKit
import FriendstaTools
import FriendstaNetwork

fileprivate extension NotificationsAccessState {
    
    // MARK: - Initializers
    
    fileprivate init(from accessState: UNAuthorizationStatus) {
        switch accessState {
        case .notDetermined:
            self = .notDetermined
            
        case .authorized, .provisional:
            self = .authorized
            
        case .denied:
            self = .denied
        }
    }
}

// MARK: -

class DefaultNotificationsService: NSObject, NotificationsService {
    
    // MARK: - Instance Properties
    
    let accountProvider: AccountProvider
    let apiWebService: APIWebService
    let notificationCategoryCoder: NotificationCategoryCoder
    let notificationActionCoder: NotificationActionCoder
    
    // MARK: -
    
    private var isAllChatsListOpened: Bool {
        return UIApplication.shared.topVisibleViewController is ChatTableViewController
    }
    
    private var openedChatUID: Int64? {
        if let chatMessagesRoutingController = UIApplication.shared.topVisibleViewController as? ChatMessagesRoutingController {
            return chatMessagesRoutingController.chat?.uid
        } else {
            return nil
        }
    }
    
    // MARK: - NotificationsService
    
    fileprivate(set) var notificationsBlockers: [NotificationsBlocker] = []
    
    var isRegisteredForNotifications: Bool {
        return UIApplication.shared.isRegisteredForRemoteNotifications
    }
    
    // MARK: - Initializers
    
    init(accountProvider: AccountProvider, apiWebService: APIWebService, notificationCategoryCoder: NotificationCategoryCoder, notificationActionCoder: NotificationActionCoder) {
        self.accountProvider = accountProvider
        self.apiWebService = apiWebService
        self.notificationCategoryCoder = notificationCategoryCoder
        self.notificationActionCoder = notificationActionCoder
        
        super.init()
        
        UNUserNotificationCenter.current().delegate = self
    }
    
    // MARK: - Instance Methods
    
    func addNotificationsBlocker(_ notificationsBlocker: NotificationsBlocker) {
        if !self.notificationsBlockers.contains(where: { $0 === notificationsBlocker }) {
            self.notificationsBlockers.append(notificationsBlocker)
        }
    }
    
    func removeNotificationsBlocker(_ notificationsBlocker: NotificationsBlocker) {
        if let index = self.notificationsBlockers.index(where: { $0 === notificationsBlocker }) {
            self.notificationsBlockers.remove(at: index)
        }
    }
    
    func registerForNotifications() {
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func unregisterForNotifications() {
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    func requestAccess() -> Guarantee<NotificationsAccessState> {
        return Guarantee(resolver: { completion in
            UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { settings in
                switch settings.authorizationStatus {
                case .notDetermined:
                    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge],
                                                                            completionHandler: { granted, error in
                        completion(granted ? .authorized : .denied)
                    })
                    
                case .authorized, .provisional:
                    completion(.authorized)
                    
                case .denied:
                    completion(.denied)
                }
            })
        })
    }

    func update(deviceToken: String?) -> Promise<Void> {
        return Promise(resolver: { seal in
            let requestParams: [String: Any]
            
            if let deviceToken = deviceToken {
                requestParams = ["push_token": deviceToken]
            } else {
                requestParams = [:]
            }
            
            firstly {
                self.apiWebService.json(with: WebRequest(method: .patch,
                                                         path: "v1/device",
                                                         params: requestParams))
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                seal.reject(error)
            }
        })
    }
}

// MARK: - UNUserNotificationCenterDelegate

extension DefaultNotificationsService: UNUserNotificationCenterDelegate {
    
    // MARK: - Instance Methods
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        Log.low("userNotificationCenterDidReceive(response: \(response.notification.request.content.categoryIdentifier))", from: self)
        
        defer {
            completionHandler()
        }
        
        guard let category = self.notificationCategoryCoder.notificationCategory(from: response.notification.request.content) else {
            return
        }
        
        guard let action = self.notificationActionCoder.notificationAction(from: response.actionIdentifier) else {
            return
        }
        
        firstly {
            self.accountProvider.captureModel()
        }.done { accountSession in
            accountSession.model.notificationsManager.push(notification: Notification(category: category, action: action))
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        Log.low("userNotificationCenterWillPresent(notification: \(notification.request.content.categoryIdentifier))", from: self)
        
        guard let category = self.notificationCategoryCoder.notificationCategory(from: notification.request.content) else {
            return completionHandler([])
        }
        
        guard let accountUserUID = self.accountProvider.model.access?.userUID else {
            return completionHandler([])
        }
        
        switch category {
        case .newUser, .newFriend:
            return completionHandler([.alert, .sound])
        
        case .propCard, .pokeReply:
            break
            
        case .chatMessage(let chatUID, let senderUID, let receiverUID):
            if let senderUID = senderUID, senderUID == accountUserUID {
                return completionHandler([])
            }
            
            if let receiverUID = receiverUID, receiverUID != accountUserUID {
                return completionHandler([])
            }
            
            if self.isAllChatsListOpened {
                return completionHandler([])
            } else {
                if let openedChatUID = self.openedChatUID, openedChatUID == chatUID {
                    return completionHandler([])
                } else {
                    completionHandler([.alert, .sound])
                }
            }
        }
        
        let notification = FriendstaNetwork.Notification(category: category)
        
        firstly {
            self.accountProvider.captureModel()
        }.done { accountSession in
            accountSession.model.notificationsManager.push(notification: notification)
        }
        
        for notificationsBlocker in self.notificationsBlockers {
            if notificationsBlocker.notificationsService(self, shouldBlock: notification) {
                return completionHandler([.sound])
            }
        }
        
        completionHandler([.alert, .sound])
    }
}
