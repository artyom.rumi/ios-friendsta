//
//  NotificationsBlocker.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 21.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaNetwork

protocol NotificationsBlocker: class {
    
    // MARK: - Instance Methods
    
    func notificationsService(_ notificationService: NotificationsService, shouldBlock notification: FriendstaNetwork.Notification) -> Bool
}
