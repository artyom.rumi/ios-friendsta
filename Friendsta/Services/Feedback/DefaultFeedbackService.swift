//
//  DefaultFeedbackService.swift
//  Friendsta
//
//  Created by Marat Galeev on 07.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultFeedbackService: FeedbackService {
    
    // MARK: - Instance Properties
    
    let apiWebService: APIWebService
    
    // MARK: - Instance Methods
    
    func sendFeedback(text: String) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .post,
                                                         path: "v1/feedbacks",
                                                         params: ["text": text]))
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                seal.reject(error)
            }
        })
    }
}
