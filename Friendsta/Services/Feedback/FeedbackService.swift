//
//  FeedbackService.swift
//  Friendsta
//
//  Created by Marat Galeev on 07.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol FeedbackService {
    
    // MARK: - Instance Methods
    
    func sendFeedback(text: String) -> Promise<Void>
}
