//
//  ActivityService.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/28/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol ActivityService {

    // MARK: - Instance Methods

    func refreshAllActivities() -> Promise<[Activity]>
    func markAsRead(activity: Activity) -> Promise<Void>
    func markAsRead(activities: [Activity]) -> Promise<Void>
}
