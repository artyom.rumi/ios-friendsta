//
//  DefaultActivityExtractor.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/28/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultActivityExtractor: ActivityExtractor {

    // MARK: - Instance Properties

    let cacheProvider: CacheProvider

    let activityCoder: ActivityCoder

    // MARK: - Instance Methods

    func extractActivity(from response: [String: Any]) -> Promise<Activity> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        let activityUID = try self.extractActivity(from: response, context: backgroundContext).uid

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            seal.fulfill(cacheSession.model.viewContext.activityManager.first(with: activityUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }

    func extractActivities(from response: [[String: Any]]) -> Promise<[Activity]> {
       return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.activityManager.clear()

                backgroundContext.perform(block: {
                    do {
                        try self.extractActivities(from: response, context: backgroundContext)

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            seal.fulfill(cacheSession.model.viewContext.activityManager.fetch())
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }

    // MARK: -

    @discardableResult
    private func extractActivity(from response: [String: Any], context: CacheModelContext) throws -> Activity {
        guard let activityUID = self.activityCoder.uid(from: response) else {
            throw WebError.badResponse
        }

        let activity = context.activityManager.firstOrNew(withUID: activityUID)

        guard self.activityCoder.decode(activity: activity, from: response) else {
            throw WebError.badResponse
        }

        return activity
    }

    @discardableResult
    private func extractActivities(from response: [[String: Any]], context: CacheModelContext) throws -> [Activity] {
        var activities: [Activity] = []

        try response.forEach { activities.append(try self.extractActivity(from: $0, context: context)) }

        return activities
    }

}
