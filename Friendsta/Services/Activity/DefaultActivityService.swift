//
//  DefaultActivityService.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/28/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultActivityService: ActivityService {

    // MARK: - Instance Properties

    let apiWebService: APIWebService
    let activityExtractor: ActivityExtractor
    let cacheProvider: CacheProvider

    // MARK: - Instance Methods

    func refreshAllActivities() -> Promise<[Activity]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/my/notifications/"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.activityExtractor.extractActivities(from: response)
            }.done { activities in
                seal.fulfill(activities)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    func markAsRead(activity: Activity) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .patch, path: "v1/my/notifications/\(activity.uid)"), queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let activitiesManager = cacheSession.model.viewContext.activityManager

                for activity in activitiesManager.fetch(with: activity.uid) {
                    activity.isUnread = false
                }

                cacheSession.model.viewContext.save()
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    func markAsRead(activities: [Activity]) -> Promise<Void> {
        let ids = activities.map { $0.uid }

        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .patch, path: "v1/my/notifications/", params: ["ids": ids]), queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let activitiesManager = cacheSession.model.viewContext.activityManager

                for id in ids {
                    for activity in activitiesManager.fetch(with: id) {
                        activity.isUnread = false
                    }
                }

                cacheSession.model.viewContext.save()

                seal.fulfill(Void())
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
}
