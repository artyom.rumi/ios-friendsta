//
//  ActivityExtractor.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/28/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol ActivityExtractor {

    // MARK: - Instance Methods

    func extractActivity(from response: [String: Any]) -> Promise<Activity>
    func extractActivities(from response: [[String: Any]]) -> Promise<[Activity]>
}
