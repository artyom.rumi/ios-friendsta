//
//  DefaultMediaService.swift
//  Friendsta
//
//  Created by Elina Batyrova on 23.10.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import AVFoundation
import PromiseKit
import Photos

// MARK: - CameraAccessState

fileprivate extension CameraAccessState {
    
    // MARK: - Initializers
    
    init(from accessState: AVAuthorizationStatus) {
        switch accessState {
        case .notDetermined:
            self = .notDetermined

        case .authorized:
            self = .authorized

        case .restricted:
            self = .restricted

        case .denied:
            self = .denied
        }
    }
}

// MARK: - LibraryAccessState

fileprivate extension LibraryAccessState {
    
    // MARK: - Initializers
    
    init(from accessState: PHAuthorizationStatus) {
        switch accessState {
        case .notDetermined:
            self = .notDetermined
            
        case .authorized:
            self = .authorized
            
        case .restricted:
            self = .restricted
            
        case .denied:
            self = .denied
        }
    }
}

// MARK: -

class DefaultMediaService: NSObject, MediaService {
    
    // MARK: - Instance Properties
    
    var cameraAccessState: CameraAccessState {
        return CameraAccessState(from: AVCaptureDevice.authorizationStatus(for: .video))
    }
    
    var libraryAccessState: LibraryAccessState {
        return LibraryAccessState(from: PHPhotoLibrary.authorizationStatus())
    }
    
    // MARK: - Initializers
    
    override init() {
        super.init()
    }
    
    // MARK: - Instance Methods
    
    func requestCameraAccess() -> Guarantee<CameraAccessState> {
        return Guarantee(resolver: { completion in
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                    completion(self.cameraAccessState)
                })
                
            case .authorized:
                completion(.authorized)
                
            case .restricted:
                completion(.restricted)
                
            case .denied:
                completion(.denied)
            }
        })
    }
    
    func requestLibraryAccess() -> Guarantee<LibraryAccessState> {
        return Guarantee(resolver: { completion in
            switch PHPhotoLibrary.authorizationStatus() {
            case .notDetermined:
                PHPhotoLibrary.requestAuthorization({ granted in
                    completion(self.libraryAccessState)
                })
                
            case .authorized:
                completion(.authorized)
                
            case .restricted:
                completion(.restricted)
                
            case .denied:
                completion(.denied)
            }
        })
    }
}
