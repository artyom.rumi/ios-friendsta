//
//  MediaService.swift
//  Friendsta
//
//  Created by Elina Batyrova on 23.10.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol MediaService {
    
    // MARK: - Instance Methods
    
    var cameraAccessState: CameraAccessState { get }
    var libraryAccessState: LibraryAccessState { get }
    
    // MARK: - Instance Methods
    
    func requestCameraAccess() -> Guarantee<CameraAccessState>
    func requestLibraryAccess() -> Guarantee<LibraryAccessState>
}
