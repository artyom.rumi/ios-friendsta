//
//  SchoolsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 17.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol SchoolsService {
    
    // MARK: - Instance Methods
    
    func refreshAllSchoolList(of classYear: Int, keywords: String?) -> Promise<SchoolList>
    func refreshNearbySchoolList(of classYear: Int, location: Location, keywords: String?) -> Promise<SchoolList>
    
    func loadMoreSchools(of schoolList: SchoolList) -> Promise<SchoolList>
}
