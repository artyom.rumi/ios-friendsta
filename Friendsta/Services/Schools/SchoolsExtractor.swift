//
//  SchoolsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 21.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol SchoolsExtractor {
    
    // MARK: - Instance Methods
    
    func extractSchool(from response: [String: Any]) -> Promise<School>
    func extractSchoolList(from response: [[String: Any]], listType: SchoolListType, location: Location?, keywords: String?, page: Int) -> Promise<SchoolList>
    func extractSchoolList(from response: [[String: Any]], schoolList: SchoolList, page: Int) -> Promise<SchoolList>
}
