//
//  DefaultSchoolsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 21.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultSchoolsExtractor: SchoolsExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let schoolCoder: SchoolCoder
    
    // MARK: - Instance Methods
    
    @discardableResult
    fileprivate func extractSchool(from response: [String: Any], context: CacheModelContext) throws -> School {
        guard let schoolUID = self.schoolCoder.schoolUID(from: response) else {
            throw WebError.badResponse
        }
        
        let schoolsManager = context.schoolsManager
        
        let school = schoolsManager.first(with: schoolUID) ?? schoolsManager.append()
        
        guard self.schoolCoder.decode(school: school, from: response) else {
            throw WebError.badResponse
        }
        
        return school
    }
    
    @discardableResult
    fileprivate func extractSchoolList(from response: [[String: Any]], listType: SchoolListType, location: Location?, keywords: String?, page: Int, context: CacheModelContext) throws -> SchoolList {
        let schoolListsManager = context.schoolListsManager

        let schoolList = schoolListsManager.first(with: listType) ?? schoolListsManager.append()

        if schoolList.nextPage != page {
            schoolList.clearSchools()
        }
        
        schoolList.listType = listType
        
        schoolList.location = location ?? Location.northPole
        schoolList.keywords = keywords
        schoolList.nextPage = Int32(page + 1)
        
        schoolList.cachedDate = Date()
        
        for response in response {
            schoolList.append(school: try self.extractSchool(from: response, context: context))
        }

        return schoolList
    }
    
    // MARK: - SchoolsExtractor
    
    func extractSchool(from response: [String: Any]) -> Promise<School> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        let schoolUID = try self.extractSchool(from: response, context: backgroundContext).uid

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            seal.fulfill(cacheSession.model.viewContext.schoolsManager.first(with: schoolUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractSchoolList(from response: [[String: Any]], listType: SchoolListType, location: Location?, keywords: String?, page: Int) -> Promise<SchoolList> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        try self.extractSchoolList(from: response,
                                                   listType: listType,
                                                   location: location,
                                                   keywords: keywords,
                                                   page: page,
                                                   context: backgroundContext)

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            seal.fulfill(cacheSession.model.viewContext.schoolListsManager.first(with: listType)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractSchoolList(from response: [[String: Any]], schoolList: SchoolList, page: Int) -> Promise<SchoolList> {
        return self.extractSchoolList(from: response,
                                      listType: schoolList.listType,
                                      location: schoolList.location,
                                      keywords: schoolList.keywords,
                                      page: page)
    }
}
