//
//  DefaultSchoolsService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 21.08.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultSchoolsService: SchoolsService {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let pageLimit = 40
    }
    
    // MARK: - Instance Properties
    
    let apiWebService: APIWebService
    let schoolsExtractor: SchoolsExtractor
    let locationCoder: LocationCoder
    
    // MARK: - Instance Methods
    
    fileprivate func loadAllSchoolList(of classYear: Int, keywords: String?, page: Int) -> Promise<SchoolList> {
        return Promise(resolver: { seal in
            var requestParams: [String: Any] = ["year_of_ending": classYear,
                                                "per_page": Constants.pageLimit,
                                                "page": page]
            
            if let keywords = keywords, !keywords.isEmpty {
                requestParams["keywords"] = keywords
            }
            
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get,
                                                              path: "v1/schools",
                                                              params: requestParams),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.schoolsExtractor.extractSchoolList(from: response,
                                                        listType: .all(classYear: classYear),
                                                        location: nil,
                                                        keywords: keywords,
                                                        page: page)
            }.done { schoolList in
                seal.fulfill(schoolList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    fileprivate func loadNearbySchoolList(of classYear: Int, location: Location, keywords: String?, page: Int) -> Promise<SchoolList> {
        return Promise(resolver: { seal in
            var requestParams = self.locationCoder.encode(location: location)
            
            requestParams["year_of_ending"] = classYear
            requestParams["per_page"] = Constants.pageLimit
            requestParams["page"] = page
            
            if let keywords = keywords {
                requestParams["keywords"] = keywords
            }
            
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get,
                                                              path: "v1/schools",
                                                              params: requestParams),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.schoolsExtractor.extractSchoolList(from: response,
                                                        listType: .nearby(classYear: classYear),
                                                        location: location,
                                                        keywords: keywords,
                                                        page: page)
            }.done { schoolList in
                seal.fulfill(schoolList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    // MARK: - SchoolsService
    
    func refreshAllSchoolList(of classYear: Int, keywords: String?) -> Promise<SchoolList> {
        return self.loadAllSchoolList(of: classYear, keywords: keywords, page: 1)
    }
    
    func refreshNearbySchoolList(of classYear: Int, location: Location, keywords: String?) -> Promise<SchoolList> {
        return self.loadNearbySchoolList(of: classYear, location: location, keywords: keywords, page: 1)
    }
    
    func loadMoreSchools(of schoolList: SchoolList) -> Promise<SchoolList> {
        switch schoolList.listType {
        case .unknown:
            fatalError()
        
        case .all(let classYear):
            return self.loadAllSchoolList(of: classYear,
                                          keywords: schoolList.keywords,
                                          page: Int(schoolList.nextPage))
            
        case .nearby(let classYear):
            return self.loadNearbySchoolList(of: classYear,
                                             location: schoolList.location,
                                             keywords: schoolList.keywords,
                                             page: Int(schoolList.nextPage))
        }
    }
}
