//
//  PropsService.swift
//  Friendsta
//
//  Created by Marat Galeev on 29.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol PropsService {

    // MARK: - Instance Methods
    
    func refreshAllProps() -> Promise<[PropListItem]>
    func refreshPopularProps() -> Promise<[PropListItem]>
    func refreshRecentProps() -> Promise<[PropListItem]>
    func refreshSpecialProps() -> Promise<[PropListItem]>
    
    func proposeProp(content: String) -> Promise<Void>
}
