//
//  DefaultPropsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 24.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultPropsExtractor: PropsExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let propCoder: PropCoder
    
    // MARK: - Instance Methods
    
    @discardableResult
    fileprivate func extractProp(from response: [String: Any], context: CacheModelContext) throws -> Prop {
        guard let propUID = self.propCoder.propUID(from: response) else {
            throw WebError.badResponse
        }
        
        let propsManager = context.propsManager
        
        let prop = propsManager.first(with: propUID) ?? propsManager.append()
        
        guard self.propCoder.decode(prop: prop, from: response) else {
            throw WebError.badResponse
        }
        
        return prop
    }
    
    @discardableResult
    fileprivate func extractProps(from response: [[String: Any]], listType: PropListType, context: CacheModelContext) throws -> [PropListItem] {
        let propListsManager = context.propListsManager
        
        propListsManager.clear(with: listType)
        
        var propList: [PropListItem] = []
        
        for response in response {
            let prop = try self.extractProp(from: response, context: context)
            
            let propListItem = propListsManager.append()
            
            propListItem.prop = prop
            propListItem.listType = listType
            propListItem.sorting = Int64(propList.count)
            
            propList.append(propListItem)
        }
        
        return propList
    }
    
    @discardableResult
    fileprivate func extractProps(from responses: [PropListType: [[String: Any]]], context: CacheModelContext) throws -> [[PropListItem]] {
        return try responses.compactMap({ listType, response in
            let propList = try self.extractProps(from: response, listType: listType, context: context)
            
            return propList.isEmpty ? nil : propList
        })
    }
    
    // MARK: - PropsExtractor
    
    func extractProp(from response: [String: Any]) -> Promise<Prop> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let propUID = try self.extractProp(from: response, context: backgroundContext).uid
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.propsManager.first(with: propUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractProps(from response: [[String: Any]], listType: PropListType) -> Promise<[PropListItem]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        try self.extractProps(from: response, listType: listType, context: backgroundContext)
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.propListsManager.fetch(with: listType))
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractProps(from responses: [PropListType: [[String: Any]]]) -> Promise<[[PropListItem]]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        for (listType, response) in responses {
                            try self.extractProps(from: response, listType: listType, context: backgroundContext)
                        }
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(responses.compactMap({ listType, response in
                                let propList = cacheSession.model.viewContext.propListsManager.fetch(with: listType)
                                
                                return propList.isEmpty ? nil : propList
                            }))
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
