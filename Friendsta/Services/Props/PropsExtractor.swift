//
//  PropsExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 24.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol PropsExtractor {
    
    // MARK: - Instance Methods
    
    func extractProp(from response: [String: Any]) -> Promise<Prop>
    func extractProps(from response: [[String: Any]], listType: PropListType) -> Promise<[PropListItem]>
    func extractProps(from responses: [PropListType: [[String: Any]]]) -> Promise<[[PropListItem]]>
}
