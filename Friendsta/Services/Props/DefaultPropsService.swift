//
//  DefaultPropsService.swift
//  Friendsta
//
//  Created by Marat Galeev on 29.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultPropsService: PropsService {
    
    // MARK: - Instance Properties
    
    let apiWebService: APIWebService
    let propsExtractor: PropsExtractor
    
    // MARK: - Instance Methods
    
    func refreshAllProps() -> Promise<[PropListItem]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/props"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.propsExtractor.extractProps(from: response, listType: .all)
            }.done { propList in
                seal.fulfill(propList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func refreshPopularProps() -> Promise<[PropListItem]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/popular_props"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.propsExtractor.extractProps(from: response, listType: .popular)
            }.done { propList in
                seal.fulfill(propList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func refreshRecentProps() -> Promise<[PropListItem]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/my/recently_used_props"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.propsExtractor.extractProps(from: response, listType: .recent)
            }.done { propList in
                seal.fulfill(propList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func refreshSpecialProps() -> Promise<[PropListItem]> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/special_props"),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.propsExtractor.extractProps(from: response, listType: .special)
            }.done { propList in
                seal.fulfill(propList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func proposeProp(content: String) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .post,
                                                         path: "v1/suggest_props",
                                                         params: ["content": content]))
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                seal.reject(error)
            }
        })
    }
}
