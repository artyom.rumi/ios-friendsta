//
//  AccountUserService.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 20/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol AccountUserService {
    
    // MARK: - Instance Methods
    
    func createAccountUser(from accountUserBuffer: AccountUserBuffer) -> Promise<AccountUser>
    func updateAccountUser(from accountUserBuffer: AccountUserBuffer, uid: Int64) -> Promise<AccountUser>
    
    func uploadAccountUserAvatar(with avatarData: Data) -> Promise<AccountUser>
    func deleteAccountUserAvatar() -> Promise<AccountUser?>
    
    func refreshAccountUser() -> Promise<AccountUser>
    
    func update(accountUser: AccountUser) -> Promise<AccountUser>
}
