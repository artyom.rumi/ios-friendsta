//
//  DefaultAccountUserExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultAccountUserExtractor: AccountUserExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let accountUserCoder: AccountUserCoder
    let propCoder: PropCoder
    
    // MARK: - Instance Methods
    
    fileprivate func extractAccountUser(from response: [String: Any], context: CacheModelContext) throws -> AccountUser {
        guard let accountUserUID = self.accountUserCoder.accountUserUID(from: response) else {
            throw WebError.badResponse
        }
        
        guard let pokesUIDs = self.accountUserCoder.pokesUIDs(from: response) else {
            throw WebError.badResponse
        }
        
        let pokesJSON = self.accountUserCoder.pokesArrayJSON(from: response)
        
        let accountUserManager = context.accountUsersManager
        let propsManager = context.propsManager
        
        let accountUser = accountUserManager.first(with: accountUserUID) ?? accountUserManager.append()
        
        for pokeUID in pokesUIDs {
            let poke = propsManager.first(with: pokeUID) ?? propsManager.append()

            poke.uid = pokeUID

            accountUser.addToTopPokes(poke)
        }
        
        for pokeJSON in pokesJSON {
            guard let pokeUID = self.propCoder.propUID(from: pokeJSON) else {
                throw WebError.badResponse
            }

            guard let poke = accountUser.topPokes?.allObjects.first(where: { poke in
                let poke = poke as? Prop
                
                return (poke?.uid == pokeUID)
            }) as? Prop else {
                throw WebError.badResponse
            }

            guard self.propCoder.decode(prop: poke, from: pokeJSON) else {
                throw WebError.badResponse
            }
        }
        
        guard self.accountUserCoder.decode(accountUser: accountUser, from: response) else {
            throw WebError.badResponse
        }
        
        return accountUser
    }

    // MARK: - AccountUserExtractor
    
    func extractAccountUser(from response: [String: Any]) -> Promise<AccountUser> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let accountUserUID = try self.extractAccountUser(from: response, context: backgroundContext).uid
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.accountUsersManager.first(with: accountUserUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
