//
//  AccountUserExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol AccountUserExtractor {
    
    // MARK: - Instance Methods
    
    func extractAccountUser(from response: [String: Any]) -> Promise<AccountUser>
}
