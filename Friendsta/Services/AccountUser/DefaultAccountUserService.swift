//
//  DefaultAccountUserService.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 20/03/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultAccountUserService: AccountUserService {
    
    // MARK: - Instance Properties
    
    let accountProvider: AccountProvider
    let cacheProvider: CacheProvider
    let apiWebService: APIWebService
    let accountUserExtractor: AccountUserExtractor
    let accountUserBufferCoder: AccountUserBufferCoder
    let accountUserCoder: AccountUserCoder
    
    // MARK: - Instance Methods
    
    func createAccountUser(from accountUserBuffer: AccountUserBuffer) -> Promise<AccountUser> {
        return Promise(resolver: { seal in
            let requestParams = self.accountUserBufferCoder.encode(accountUserBuffer: accountUserBuffer)
            
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                               path: "v1/users",
                                                               params: requestParams),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.accountUserExtractor.extractAccountUser(from: response)
            }.done { accountUser in
                firstly {
                    self.accountProvider.captureModel()
                }.done { accountSession in
                    if let access = accountSession.model.access {
                        accountSession.model.accessManager.update(access: Access(token: access.token,
                                                                                 expiryDate: access.expiryDate,
                                                                                 userUID: accountUser.uid))
                        
                        accountSession.model.accessManager.saveAccess()
                    }
                    
                    seal.fulfill(accountUser)
                }
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func updateAccountUser(from accountUserBuffer: AccountUserBuffer, uid: Int64) -> Promise<AccountUser> {
        return Promise(resolver: { seal in
            let requestParams = self.accountUserBufferCoder.encode(accountUserBuffer: accountUserBuffer, uid: uid)
            
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .patch,
                                                               path: "v1/users/\(uid)",
                                                               params: requestParams),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.accountUserExtractor.extractAccountUser(from: response)
            }.done { accountUser in
                seal.fulfill(accountUser)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    func uploadAccountUserAvatar(with avatarData: Data) -> Promise<AccountUser> {
        return Promise(resolver: { seal in
            let multiPartItem = WebMultiPartDataItem(data: avatarData,
                                                     name: "file",
                                                     fileName: "avatar.jpg",
                                                     mimeType: "image/jpg")
            
            firstly {
                self.apiWebService.jsonObject(uploadingMultiPart: [multiPartItem],
                                              to: "v1/avatar",
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.accountUserExtractor.extractAccountUser(from: response)
            }.done { accountUser in
                seal.fulfill(accountUser)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func deleteAccountUserAvatar() -> Promise<AccountUser?> {
        return Promise(resolver: { seal in
            do {
                guard let accountUserUID = self.accountProvider.model.access?.userUID else {
                    throw WebError.unauthorized
                }
                
                firstly {
                    self.apiWebService.json(with: WebRequest(method: .delete, path: "v1/avatar"))
                }.then { response in
                    self.cacheProvider.captureModel()
                }.done { cacheSession in
                    let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                    
                    backgroundContext.perform(block: {
                        let accountUsers = backgroundContext.accountUsersManager.fetch(with: accountUserUID)
                
                        for accountUser in accountUsers {
                            accountUser.avatarURL = nil
                            
                            accountUser.smallAvatarURL = nil
                            accountUser.mediumAvatarURL = nil
                            accountUser.largeAvatarURL = nil
                        }
                      
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(accountUsers.first)
                        })
                    })
                }.catch { error in
                    seal.reject(error)
                }
            } catch let error {
                seal.reject(error)
            }
        })
    }
    
    func refreshAccountUser() -> Promise<AccountUser> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/profile"),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.accountUserExtractor.extractAccountUser(from: response)
            }.done { user in
                seal.fulfill(user)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func update(accountUser: AccountUser) -> Promise<AccountUser> {
        return Promise(resolver: { seal in
            let requestParams = self.accountUserCoder.encode(accountUser: accountUser)
            
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .patch,
                                                               path: "v1/users/\(accountUser.uid)",
                                                               params: requestParams),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.accountUserExtractor.extractAccountUser(from: response)
            }.done { accountUser in
                seal.fulfill(accountUser)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
}
