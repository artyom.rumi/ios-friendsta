//
//  UsersService.swift
//  Friendsta
//
//  Created by Marat Galeev on 21.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol UsersService {
    
    // MARK: - Instance Methods
    
    func refresh(userUID: Int64) -> Promise<User> 
    
    func refreshContacts(with contacts: [Contact]) -> Promise<Void>

    func refreshInvitationSchoolmates() -> Promise<UserList>
    func refreshInvitationInlineContacts() -> Promise<UserList>

    func refreshPokeSchoolmates() -> Promise<UserList>
    func refreshPokeInlineContacts() -> Promise<UserList>

    func refreshFriends() -> Promise<UserList>
    func refreshInvitedFriends() -> Promise<UserList>
    func refreshExpiredFriends() -> Promise<UserList>

    func searchCommunitiesUsers(by keywords: String) -> Promise<[User]>
    func searchFriendsUsers(by keywords: String) -> Promise<[User]>

    func refreshAllUserList(of type: UserListType) -> Promise<UserList>

    func loadMoreUsers(of userList: UserList) -> Promise<UserList>

    func inviteUser(user: User) -> Promise<User>
    
    func report(user: User) -> Promise<Void>
    func block(user: User) -> Promise<Void>
    func unblock(user: User) -> Promise<Void>
    
    func keep(user: User) -> Promise<User>
    func keepUser(with id: Int64) -> Promise<Void>
}
