//
//  DefaultUsersExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 24.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultUsersExtractor: UsersExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider

    let userCoder: UserCoder
    let propCoder: PropCoder
    let metaCoder: MetaCoder
    
    // MARK: - Instance Methods
    
    @discardableResult
    private func extractUser(from response: [String: Any], context: CacheModelContext) throws -> User {
        guard let userUID = self.userCoder.userUID(from: response) else {
            throw WebError.badResponse
        }
        
        let pokesUIDs = self.userCoder.pokesUIDs(from: response) ?? []
        
        let pokesJSON = self.userCoder.pokesArrayJSON(from: response)
        
        let usersManager = context.usersManager
        let propsManager = context.propsManager
        
        let user = usersManager.first(with: userUID) ?? usersManager.append()
        
        for pokeUID in pokesUIDs {
            let poke = propsManager.first(with: pokeUID) ?? propsManager.append()
            
            poke.uid = pokeUID
            
            user.addToTopPokes(poke)
        }
        
        for pokeJSON in pokesJSON {
            guard let pokeUID = self.propCoder.propUID(from: pokeJSON) else {
                throw WebError.badResponse
            }
            
            guard let poke = user.topPokes?.allObjects.first(where: { poke in
                let poke = poke as? Prop
                
                return (poke?.uid == pokeUID)
            }) as? Prop else {
                throw WebError.badResponse
            }
            
            guard self.propCoder.decode(prop: poke, from: pokeJSON) else {
                throw WebError.badResponse
            }
        }
        
        guard self.userCoder.decode(user: user, from: response) else {
            throw WebError.badResponse
        }
        
        return user
    }

    @discardableResult
    private func extractUsers(from response: [[String: Any]], context: CacheModelContext) throws -> [User] {
        return try response.map { try self.extractUser(from: $0, context: context) }
    }

    @discardableResult
    private func extractUserList(from response: [String: Any], listType: UserListType, page: Int32, context: CacheModelContext) throws -> UserList {
        guard let usersJSON = self.userCoder.usersJSON(from: response) else {
            throw WebError.badResponse
        }

        guard let metaJSON = self.userCoder.metaJSON(from: response) else {
            throw WebError.badResponse
        }

        guard let meta = self.metaCoder.decode(from: metaJSON) else {
            throw WebError.badResponse
        }

        let userList = context.userListManager.firstOrNew(withListType: listType)

        if userList.nextPage != page {
            userList.clearUsers()
        }

        if usersJSON.isEmpty {
            userList.nextPage = 0
        } else {
            userList.nextPage = page + 1
        }

        userList.totalCount = meta.count

        try usersJSON.forEach { userList.addToRawUsers(try self.extractUser(from: $0, context: context)) }

        return userList
    }
    
    // MARK: - UsersExtractor
    
    func extractUser(from response: [String: Any]) -> Promise<User> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let userUID = try self.extractUser(from: response, context: backgroundContext).uid
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.usersManager.first(with: userUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }

    func extractUsers(from response: [[String: Any]]) -> Promise<[User]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        let userUIDs = try self.extractUsers(from: response, context: backgroundContext).map { $0.uid }

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            let users = userUIDs.compactMap { cacheSession.model.viewContext.usersManager.first(with: $0) }

                            seal.fulfill(users)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }

    func extractUserList(from response: [String: Any], listType: UserListType, page: Int32) -> Promise<UserList> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        try self.extractUserList(from: response, listType: listType, page: page, context: backgroundContext)

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            seal.fulfill(cacheSession.model.viewContext.userListManager.first(with: listType)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
