//
//  UsersExtractor.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 24.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol UsersExtractor {
    
    // MARK: - Instance Methods
    
    func extractUser(from response: [String: Any]) -> Promise<User>
    func extractUserList(from response: [String: Any], listType: UserListType, page: Int32) -> Promise<UserList>
    func extractUsers(from response: [[String: Any]]) -> Promise<[User]>
}
