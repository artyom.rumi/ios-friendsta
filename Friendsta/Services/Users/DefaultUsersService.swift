//
//  DefaultUsersService.swift
//  Friendsta
//
//  Created by Marat Galeev on 21.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultUsersService: UsersService {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let apiWebService: APIWebService
    let contactsService: ContactsService
    let chatsService: ChatsService
    let usersExtractor: UsersExtractor
    
    // MARK: - Instance Methods

    private func loadAllInvitationSchoolmates(page: Int32? = nil) -> Promise<UserList> {
        return Promise(resolver: { seal in
            var requestParameters: [String: Any] = [:]

            if let page = page {
                requestParameters = ["page": page, "per_page": Limits.friendMinCount]
            }

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/my/schoolmates", params: requestParameters),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response -> Promise<UserList> in
                if let page = page {
                    return self.usersExtractor.extractUserList(from: response, listType: .invitationSchoolmates, page: page)
                } else {
                    return self.usersExtractor.extractUserList(from: response, listType: .all(type: .invitationSchoolmates), page: 0)
                }
            }.done { userlist in
                seal.fulfill(userlist)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    private func loadAllInvitationInlineContacts(page: Int32? = nil) -> Promise<UserList> {
        return Promise(resolver: { seal in
            var requestParameters: [String: Any] = [:]

            if let page = page {
                requestParameters = ["page": page, "per_page": Limits.friendMinCount]
            }

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/my/inline_contacts", params: requestParameters),
                                         queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response -> Promise<UserList> in
                if let page = page {
                    return self.usersExtractor.extractUserList(from: response, listType: .invitationInlineContacts, page: page)
                } else {
                    return self.usersExtractor.extractUserList(from: response, listType: .all(type: .invitationInlineContacts), page: 0)
                }
            }.done { userList in
                seal.fulfill(userList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    private func loadAllPokeSchoolmates(page: Int32? = nil) -> Promise<UserList> {
        return Promise(resolver: { seal in
            var requestParameters: [String: Any] = [:]

            if let page = page {
                requestParameters = ["page": page, "per_page": Limits.friendMinCount]
            }

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/my/schoolmates", params: requestParameters),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response -> Promise<UserList> in
                if let page = page {
                    return self.usersExtractor.extractUserList(from: response, listType: .pokeSchoolmates, page: page)
                } else {
                    return self.usersExtractor.extractUserList(from: response, listType: .all(type: .pokeSchoolmates), page: 0)
                }
            }.done { userlist in
                seal.fulfill(userlist)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    private func loadAllPokeInlineContacts(page: Int32? = nil) -> Promise<UserList> {
        return Promise(resolver: { seal in
            var requestParameters: [String: Any] = [:]

            if let page = page {
                requestParameters = ["page": page, "per_page": Limits.friendMinCount]
            }

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/my/inline_contacts", params: requestParameters),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response -> Promise<UserList> in
                if let page = page {
                    return self.usersExtractor.extractUserList(from: response, listType: .pokeInlineContacts, page: page)
                } else {
                    return self.usersExtractor.extractUserList(from: response, listType: .all(type: .pokeInlineContacts), page: 0)
                }
            }.done { userList in
                seal.fulfill(userList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    private func loadAllFriends(page: Int32? = nil) -> Promise<UserList> {
        return Promise(resolver: { seal in
            var requestParameters: [String: Any] = [:]

            if let page = page {
                requestParameters = ["page": page, "per_page": Limits.friendMinCount]
            }

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/my/active_friends", params: requestParameters),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response -> Promise<UserList> in
                if let page = page {
                    return self.usersExtractor.extractUserList(from: response, listType: .friends, page: page)
                } else {
                    return self.usersExtractor.extractUserList(from: response, listType: .all(type: .friends), page: 0)
                }
            }.done { userList in
                seal.fulfill(userList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    private func loadAllExpiredFriends(page: Int32? = nil) -> Promise<UserList> {
        return Promise(resolver: { seal in
            var requestParameters: [String: Any] = [:]

            if let page = page {
                requestParameters = ["page": page, "per_page": Limits.friendMinCount]
            }

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/my/expired_friends", params: requestParameters),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response -> Promise<UserList> in
                if let page = page {
                    return self.usersExtractor.extractUserList(from: response, listType: .expired, page: page)
                } else {
                    return self.usersExtractor.extractUserList(from: response, listType: .all(type: .expired), page: 0)
                }
            }.done { userList in
                seal.fulfill(userList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    private func loadAllInvitedFriends(page: Int32? = nil) -> Promise<UserList> {
        return Promise(resolver: { seal in
            var requestParameters: [String: Any] = [:]

            if let page = page {
                requestParameters = ["page": page, "per_page": Limits.friendMinCount]
            }

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/my/invited_friends", params: requestParameters),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response -> Promise<UserList> in
                if let page = page {
                    return self.usersExtractor.extractUserList(from: response, listType: .invited, page: page)
                } else {
                    return self.usersExtractor.extractUserList(from: response, listType: .all(type: .invited), page: 0)
                }
            }.done { userList in
                seal.fulfill(userList)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    // MARK: -
    
    func refresh(userUID: Int64) -> Promise<User> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/users/\(userUID)"),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.usersExtractor.extractUser(from: response)
            }.done { user in
                seal.fulfill(user)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func refreshContacts(with contacts: [Contact]) -> Promise<Void> {
        var requestParams = [[String: String]]()
        
        for contact in contacts {
            requestParams.append(["name": contact.displayName ?? "",
                                  "phone_number": contact.phoneNumber ?? ""])
        }
        
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .post,
                                                         path: "v1/my/contacts",
                                                         params: ["phone_numbers": requestParams]),
                                        queue: DispatchQueue.global(qos: .userInitiated))
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }

    // MARK: -
    
    func refreshInvitationSchoolmates() -> Promise<UserList> {
        return self.loadAllInvitationSchoolmates(page: 1)
    }
    
    func refreshInvitationInlineContacts() -> Promise<UserList> {
        return self.loadAllInvitationInlineContacts(page: 1)
    }

    func refreshPokeSchoolmates() -> Promise<UserList> {
        return self.loadAllPokeSchoolmates(page: 1)
    }

    func refreshPokeInlineContacts() -> Promise<UserList> {
        return self.loadAllPokeInlineContacts(page: 1)
    }
    
    func refreshFriends() -> Promise<UserList> {
        return self.loadAllFriends(page: 1)
    }

    func refreshInvitedFriends() -> Promise<UserList> {
        return self.loadAllInvitedFriends(page: 1)
    }

    func refreshExpiredFriends() -> Promise<UserList> {
        return self.loadAllExpiredFriends(page: 1)
    }

    // MARK: -

    func refreshAllUserList(of type: UserListType) -> Promise<UserList> {
        switch type {
        case .expired:
            return self.loadAllExpiredFriends()

        case .friends:
            return self.loadAllFriends()

        case .invitationInlineContacts:
            return self.loadAllInvitationInlineContacts()

        case .invited:
            return self.loadAllInvitedFriends()

        case .invitationSchoolmates:
            return self.loadAllInvitationSchoolmates()

        case .pokeInlineContacts:
            return self.loadAllPokeInlineContacts()

        case .pokeSchoolmates:
            return self.loadAllPokeSchoolmates()

        case .all, .unknown:
            return Promise(error: WebError.badRequest)
        }
    }

    func inviteUser(user: User) -> Promise<User> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .post,
                                                         path: "v1/friend_intentions",
                                                         params: ["target_id": user.uid]))
            }.then { response in
                self.refresh(userUID: user.uid)
            }.done { user in
                seal.fulfill(user)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func report(user: User) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .post,
                                                         path: "v1/reports",
                                                         params: ["reported_user_id": user.uid]))
            }.done { response in
                seal.fulfill(Void())
            }.catch { error in
                seal.reject(error)
            }
        })
    }
    
    func block(user: User) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .post, path: "v1/users/\(user.uid)/blocking"))
            }.then { response in
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                user.isBlocked = true
                
                cacheSession.model.viewContext.save()
                
                firstly {
                    self.chatsService.block(user: user)
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            }.catch { error in
                if user.isBlocked {
                    firstly {
                        self.chatsService.block(user: user)
                    }.done {
                        seal.fulfill(Void())
                    }.catch { error in
                        seal.reject(error)
                    }
                } else {
                    seal.reject(error)
                }
            }
        })
    }
    
    func unblock(user: User) -> Promise<Void> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.json(with: WebRequest(method: .delete, path: "v1/users/\(user.uid)/blocking"))
            }.then { response in
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                user.isBlocked = false
                
                cacheSession.model.viewContext.save()
                
                firstly {
                    self.chatsService.unblock(user: user)
                }.done {
                    seal.fulfill(Void())
                }.catch { error in
                    seal.reject(error)
                }
            }.catch { error in
                if user.isBlocked {
                    seal.reject(error)
                } else {
                    firstly {
                        self.chatsService.unblock(user: user)
                    }.done {
                        seal.fulfill(Void())
                    }.catch { error in
                        seal.reject(error)
                    }
                }
            }
        })
    }
    
    func keep(user: User) -> Promise<User> {
        return Promise(resolver: { seal in
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                               path: "v1/my/keeps",
                                                               params: ["user_id": user.uid]))
            }.then { response in
                self.usersExtractor.extractUser(from: response)
            }.done { user in
                seal.fulfill(user)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func keepUser(with id: Int64) -> Promise<Void> {
           return Promise(resolver: { seal in
               firstly {
                   self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                                  path: "v1/my/keeps",
                                                                  params: ["user_id": id]))
               }.then { response in
                   self.usersExtractor.extractUser(from: response)
               }.done { user in
                   seal.fulfill(Void())
               }.catch { error in
                   seal.reject(error)
               }
           })
       }

    func loadMoreUsers(of userList: UserList) -> Promise<UserList> {
        if userList.hasNextPage {
            switch userList.listType {
            case .invited:
                return self.loadAllInvitedFriends(page: userList.nextPage)

            case .expired:
                return self.loadAllExpiredFriends(page: userList.nextPage)

            case .friends:
                return self.loadAllFriends(page: userList.nextPage)

            case .invitationInlineContacts:
                return self.loadAllInvitationInlineContacts(page: userList.nextPage)

            case .invitationSchoolmates:
                return self.loadAllInvitationSchoolmates(page: userList.nextPage)

            case .pokeInlineContacts:
                return self.loadAllPokeInlineContacts(page: userList.nextPage)

            case .pokeSchoolmates:
                return self.loadAllPokeSchoolmates(page: userList.nextPage)

            case .unknown, .all:
                return Promise(error: WebError.badRequest)
            }
        } else {
            return Promise(error: WebError.badRequest)
        }
    }

    // MARK: -

    func searchCommunitiesUsers(by keywords: String) -> Promise<[User]> {
        return Promise(resolver: { seal in
            let requestParameters: [String: Any] = ["keywords": keywords]

            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/communities", params: requestParameters),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.usersExtractor.extractUsers(from: response)
            }.done { users in
                seal.fulfill(users)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    func searchFriendsUsers(by keywords: String) -> Promise<[User]> {
        return Promise(resolver: { seal in
            let requestParameters: [String: Any] = ["keywords": keywords]

            firstly {
                self.apiWebService.jsonArray(with: WebRequest(method: .get, path: "v1/friends", params: requestParameters),
                                             queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.usersExtractor.extractUsers(from: response)
            }.done { users in
                seal.fulfill(users)
            }.catch { error in
                seal.reject(error)
            }
        })
    }
}
