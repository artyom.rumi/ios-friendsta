//
//  DefaultPropReactionsService.swift
//  Friendsta
//
//  Created by Marat Galeev on 12.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultPropReactionsService: PropReactionsService {
    
    // MARK: - Instance Properties
    
    let apiWebService: APIWebService
    let propReactionsExtractor: PropReactionsExtractor
    
    // MARK: - Instance Methods
    
    func sendPropReaction(for propCard: PropCard, with message: String) -> Promise<PropReaction> {
        return Promise(resolver: { seal in
            let propCardUID = propCard.uid
            
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .post,
                                                               path: "v1/prop_deliveries/\(propCardUID)/reaction",
                                                               params: ["content": message]),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.propReactionsExtractor.extractPropReaction(from: response, propCardUID: propCardUID)
            }.done { propReaction in
                seal.fulfill(propReaction)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
    
    func sendPropReaction(for propCard: PropCard, with photoData: Data) -> Promise<PropReaction> {
        return Promise(resolver: { seal in
            let propCardUID = propCard.uid
            let multiPartItem = WebMultiPartDataItem(data: photoData,
                                                     name: "photo",
                                                     fileName: "reply.jpg",
                                                     mimeType: "image/jpg")
            
            firstly {
                self.apiWebService.jsonObject(uploadingMultiPart: [multiPartItem],
                                        to: "v1/prop_deliveries/\(propCardUID)/reaction",
                                        queue: DispatchQueue.global(qos: .userInitiated))
                }.then { response in
                    self.propReactionsExtractor.extractPropReaction(from: response, propCardUID: propCardUID)
                }.done { propReaction in
                    seal.fulfill(propReaction)
                }.catch { error in
                    DispatchQueue.main.async(execute: {
                        seal.reject(error)
                    })
            }
        })
    }
    
    func markReplyAsViewed(propCard: PropCard) -> Promise<PropReaction> {
        return Promise(resolver: { seal in
            let propCardUID = propCard.uid
            
            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .patch,
                                                               path: "v1/prop_deliveries/\(propCardUID)/reaction",
                                                               params: [:]),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response in
                self.propReactionsExtractor.extractPropReaction(from: response, propCardUID: propCardUID)
            }.done { propReaction in
                seal.fulfill(propReaction)
            }.catch { error in
                DispatchQueue.main.async(execute: {
                    seal.reject(error)
                })
            }
        })
    }
}
