//
//  DefaultPropReactionsExtractor.swift
//  Friendsta
//
//  Created by Marat Galeev on 12.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultPropReactionsExtractor: PropReactionsExtractor {
    
    // MARK: - Instance Properties

    let cacheProvider: CacheProvider
    let propReactionCoder: PropReactionCoder
    
    // MARK: - Instance Methods
    
    @discardableResult
    fileprivate func extractPropReaction(from response: [String: Any], propCard: PropCard?, context: CacheModelContext) throws -> PropReaction {
        guard let propReactionUID = self.propReactionCoder.propReactionUID(from: response) else {
            throw WebError.badResponse
        }
        
        let propReactionsManager = context.propReactionsManager
        
        let propReaction = propReactionsManager.first(with: propReactionUID) ?? propReactionsManager.append()
        
        propReaction.card = propCard
        
        guard self.propReactionCoder.decode(propReaction: propReaction, from: response) else {
            throw WebError.badResponse
        }
        
        return propReaction
    }
    
    @discardableResult
    fileprivate func extractPropReactions(from response: [[String: Any]], context: CacheModelContext) throws -> [PropReaction] {
        let propReactions = response.compactMap({ response in
            return (try? self.extractPropReaction(from: response, propCard: nil, context: context)) ?? nil
        })
        
        let propReactionsManager = context.propReactionsManager
        
        for propReaction in propReactionsManager.fetch() {
            if !propReactions.contains(object: propReaction) {
                propReactionsManager.remove(object: propReaction)
            }
        }
        
        return propReactions
    }
    
    // MARK: - PropReactionsExtractor
    
    func extractPropReaction(from response: [String: Any], propCardUID: Int64?) -> Promise<PropReaction> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let propCard: PropCard?
                        
                        if let propCardUID = propCardUID {
                            propCard = backgroundContext.propCardsManager.first(with: propCardUID)
                        } else {
                            propCard = nil
                        }
                        
                        let propReactionUID = try self.extractPropReaction(from: response,
                                                                           propCard: propCard,
                                                                           context: backgroundContext).uid
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.propReactionsManager.first(with: propReactionUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
    
    func extractPropReactions(from response: [[String: Any]]) -> Promise<[PropReaction]> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        try self.extractPropReactions(from: response, context: backgroundContext)
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.propReactionsManager.fetch())
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
