//
//  PropReactionsService.swift
//  Friendsta
//
//  Created by Marat Galeev on 12.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol PropReactionsService {
    
    // MARK: - Instance Methods
    
    func sendPropReaction(for propCard: PropCard, with message: String) -> Promise<PropReaction>
    func sendPropReaction(for propCard: PropCard, with photoData: Data) -> Promise<PropReaction>
    
    func markReplyAsViewed(propCard: PropCard) -> Promise<PropReaction>
}
