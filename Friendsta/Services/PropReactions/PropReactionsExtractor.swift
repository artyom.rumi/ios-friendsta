//
//  PropReactionsExtractor.swift
//  Friendsta
//
//  Created by Marat Galeev on 12.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol PropReactionsExtractor {
    
    // MARK: - Instance Methods
    
    func extractPropReaction(from response: [String: Any], propCardUID: Int64?) -> Promise<PropReaction>
    func extractPropReactions(from response: [[String: Any]]) -> Promise<[PropReaction]>
}
