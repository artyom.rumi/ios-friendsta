//
//  DefaultOutlineContactsService.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultOutlineContactsService: OutlineContactsService {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider
    let apiWebService: APIWebService

    let outlineContactsExtractor: OutlineContactsExtractor
    
    // MARK: - Instance Methods

    private func loadAllOutlineContacts(page: Int32? = nil) -> Promise<OutlineContactList> {
        return Promise(resolver: { seal in
            var requestParameters: [String: Any] = [:]
            
            if let page = page {
                requestParameters = ["page": page,
                                     "per_page": Limits.outlineContactMinCount]
            }

            firstly {
                self.apiWebService.jsonObject(with: WebRequest(method: .get, path: "v1/my/outline_contacts", params: requestParameters),
                                              queue: DispatchQueue.global(qos: .userInitiated))
            }.then { response -> Promise<OutlineContactList> in
                if let page = page {
                    return self.outlineContactsExtractor.extractOutlineContactList(from: response, listType: .paginal, page: page)
                } else {
                    return self.outlineContactsExtractor.extractOutlineContactList(from: response, listType: .all, page: 0)
                }
            }.done { outlineContactList in
                seal.fulfill(outlineContactList)
            }.catch { error in
                seal.reject(error)
            }
        })
    }

    // MARK: - OutlineContactsService
    
    func refreshOutlineContacts() -> Promise<OutlineContactList> {
        return self.loadAllOutlineContacts(page: 1)
    }
    
    func refreshAllOutlineContacts() -> Promise<OutlineContactList> {
        return self.loadAllOutlineContacts()
    }

    func loadMoreOutlineContacts(of outlineContactList: OutlineContactList) -> Promise<OutlineContactList> {
        if outlineContactList.hasNextPage {
            switch outlineContactList.listType {
            case .paginal:
                return self.loadAllOutlineContacts(page: outlineContactList.nextPage)

            case .unknown, .all:
                return Promise(error: WebError.badRequest)
            }
        } else {
            return Promise(error: WebError.badRequest)
        }
    }
}
