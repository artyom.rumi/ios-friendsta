//
//  OutlineContactsService.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol OutlineContactsService {
    
    // MARK: - Instance Methods
    
    func refreshOutlineContacts() -> Promise<OutlineContactList>
    func refreshAllOutlineContacts() -> Promise<OutlineContactList>
    func loadMoreOutlineContacts(of outlineContactList: OutlineContactList) -> Promise<OutlineContactList>
}
