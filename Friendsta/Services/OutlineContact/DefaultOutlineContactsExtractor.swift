//
//  DefaultOutlineContactsExtractor.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

struct DefaultOutlineContactsExtractor: OutlineContactsExtractor {
    
    // MARK: - Instance Properties
    
    let cacheProvider: CacheProvider

    let outlineContactCoder: OutlineContactCoder
    let metaCoder: MetaCoder
    
    // MARK: - Instance Methods
    
    @discardableResult
    private func extractOutlineContact(from response: [String: Any], context: CacheModelContext) throws -> OutlineContact {
        guard let outlineContactUID = self.outlineContactCoder.outlineContactUID(from: response) else {
            throw WebError.badResponse
        }
        
        let outlineContactsManager = context.outlineContactsManager
        
        let outlineContact = outlineContactsManager.first(with: outlineContactUID) ?? outlineContactsManager.append()
        
        guard self.outlineContactCoder.decode(outlineContact: outlineContact, from: response) else {
            throw WebError.badResponse
        }
        
        return outlineContact
    }

    @discardableResult
    private func extractOutlineContactList(from response: [String: Any], listType: OutlineContactListType, page: Int32, context: CacheModelContext) throws -> OutlineContactList {
        guard let contactsJSON = self.outlineContactCoder.outlineContactsJSON(from: response) else {
            throw WebError.badResponse
        }

        guard let metaJSON = self.outlineContactCoder.metaJSON(from: response) else {
            throw WebError.badResponse
        }

        guard let meta = self.metaCoder.decode(from: metaJSON) else {
            throw WebError.badResponse
        }

        let outlineContactList = context.outlineContactListManager.firstOrNew(withListType: listType)

        if outlineContactList.nextPage != page {
            outlineContactList.clearOutlineContacts()
        }

        if contactsJSON.isEmpty {
            outlineContactList.nextPage = 0
        } else {
            outlineContactList.nextPage = page + 1
        }

        outlineContactList.totalCount = meta.count

        try contactsJSON.forEach { outlineContactList.addToRawOutlineContacts(try self.extractOutlineContact(from: $0, context: context)) }

        return outlineContactList
    }
    
    // MARK: - OutlineContactsExtractor
    
    func extractOutlineContact(from response: [String: Any]) -> Promise<OutlineContact> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()
                
                backgroundContext.perform(block: {
                    do {
                        let outlineContactUID = try self.extractOutlineContact(from: response, context: backgroundContext).uid
                        
                        backgroundContext.save()
                        
                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()
                            
                            seal.fulfill(cacheSession.model.viewContext.outlineContactsManager.first(with: outlineContactUID)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }

    func extractOutlineContactList(from response: [String: Any], listType: OutlineContactListType, page: Int32) -> Promise<OutlineContactList> {
        return Promise(resolver: { seal in
            firstly {
                self.cacheProvider.captureModel()
            }.done { cacheSession in
                let backgroundContext = cacheSession.model.viewContext.createPrivateQueueChildContext()

                backgroundContext.perform(block: {
                    do {
                        try self.extractOutlineContactList(from: response, listType: listType, page: page, context: backgroundContext)

                        backgroundContext.save()

                        cacheSession.model.viewContext.performAndWait(block: {
                            cacheSession.model.viewContext.save()

                            seal.fulfill(cacheSession.model.viewContext.outlineContactListManager.first(with: listType)!)
                        })
                    } catch let error {
                        DispatchQueue.main.async(execute: {
                            seal.reject(error)
                        })
                    }
                })
            }
        })
    }
}
