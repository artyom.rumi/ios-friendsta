//
//  OutlineContactsExtractor.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

protocol OutlineContactsExtractor {
    
    // MARK: - Instance Methods
    
    func extractOutlineContact(from response: [String: Any]) -> Promise<OutlineContact>
    func extractOutlineContactList(from response: [String: Any], listType: OutlineContactListType, page: Int32) -> Promise<OutlineContactList>
}
