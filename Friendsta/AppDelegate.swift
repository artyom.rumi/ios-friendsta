//
//  AppDelegate.swift
//  Friendsta
//
//  Created by Oleg Gorelov on 02/02/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import UIKit

import Fabric
import Crashlytics

import PromiseKit
import FriendstaTools

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Instance Properties
    
    var window: UIWindow?

    // MARK: - UIApplicationDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Log.initialize(withDateFormat: "HH:mm:ss.SSSS")
        Log.high("applicationDidFinishLaunchingWithOptions(\(launchOptions ?? [:]))", from: self)
        
        Fabric.with([Crashlytics.self])
        
        _ = Services.notificationsService
        
        UIBarButtonItem.appearance().setTitleTextAttributes([.font: Fonts.semiBold(ofSize: 17.0)], for: .normal)
        UIBarButtonItem.appearance().setTitleTextAttributes([.font: Fonts.semiBold(ofSize: 17.0)], for: .disabled)
        
        Managers.userDefaultsManager.isUserHasSeenFriendsScreen = false
        Managers.userDefaultsManager.isUserHasSeenCommunityScreen = false

        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        Log.high("applicationWillResignActive()", from: self)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Log.high("applicationDidEnterBackground()", from: self)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        Log.high("applicationWillEnterForeground()", from: self)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        Log.high("applicationDidBecomeActive()", from: self)
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        Log.high("applicationWillTerminate()", from: self)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Log.high("applicationDidRegisterForRemoteNotifications(withDeviceToken: \(deviceToken.hexEncoded))", from: self)
        
        firstly {
            Services.accountProvider.captureModel()
        }.done { accountSession in
            accountSession.model.deviceTokenManager.update(deviceToken: deviceToken.hexEncoded)
            accountSession.model.deviceTokenManager.saveDeviceToken()
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        Log.high("applicationDidFailToRegisterForRemoteNotifications(withError: \(error))", from: self)
        
        firstly {
            Services.accountProvider.captureModel()
        }.done { accountSession in
            accountSession.model.deviceTokenManager.resetDeviceToken()
            accountSession.model.deviceTokenManager.saveDeviceToken()
        }
    }
}
