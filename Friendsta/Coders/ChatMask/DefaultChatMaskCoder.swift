//
//  DefaultChatMaskCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultChatMaskCoder: ChatMaskCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        static let userUID = "collocutor_id"
        
        static let chatID = "chat_id"
        
        static let avatar = "avatar"
        static let avatarPlaceholder = "default_avatar"
        
        static let name = "name"
        
        static let isBlocked = "blocked"
    }
    
    // MARK: - Instance Properties
    
    let photoCoder: PhotoCoder
    
    // MARK: - Instance Methods
    
    func chatMaskUID(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }
    
    func decode(chatMask: ChatMask, from json: [String: Any]) -> Bool {
        guard let uid: Int64 = JSONKeys.uid <~~ json else {
            return false
        }
        
        guard let userUID: Int64 = JSONKeys.userUID <~~ json else {
            return false
        }
        
        guard let chatID: Int64 = JSONKeys.chatID <~~ json else {
            return false
        }
    
        let avatar: Photo?
        
        if let avatarJSON: [String: Any] = JSONKeys.avatar <~~ json {
            avatar = self.photoCoder.photo(from: avatarJSON)
        } else {
            avatar = nil
        }
        
        let avatarPlaceholder: Photo?
        
        if let avatarPlaceholderJSON: [String: Any] = JSONKeys.avatarPlaceholder <~~ json {
            avatarPlaceholder = self.photoCoder.photo(from: avatarPlaceholderJSON)
        } else {
            avatarPlaceholder = nil
        }
        
        guard let name: String = JSONKeys.name <~~ json else {
            return false
        }
        
        let isBlocked: Bool? = JSONKeys.isBlocked <~~ json
        
        chatMask.uid = uid
        chatMask.userUID = userUID
        chatMask.chatID = chatID
        
        chatMask.avatar = avatar ?? avatarPlaceholder
        chatMask.name = name
        
        chatMask.isBlocked = isBlocked ?? false
        
        return true
    }
    
    func encode(chatMask: ChatMask) -> [String: Any] {
        var json: [String: Any] = [:]
        
        json[JSONKeys.uid] = chatMask.uid
        
        if let name = chatMask.name {
            json[JSONKeys.name] = name
        }
        
        return json
    }
}
