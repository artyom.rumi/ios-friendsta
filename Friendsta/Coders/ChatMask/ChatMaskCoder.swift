//
//  ChatMaskCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 24.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ChatMaskCoder {
    
    // MARK: - Instance Methods
    
    func chatMaskUID(from json: [String: Any]) -> Int64?
    
    func decode(chatMask: ChatMask, from json: [String: Any]) -> Bool
    func encode(chatMask: ChatMask) -> [String: Any]
}
