//
//  LocationCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 08.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol LocationCoder {
    
    // MARK: - Instance Methods
    
    func encode(location: Location) -> [String: Any]
}
