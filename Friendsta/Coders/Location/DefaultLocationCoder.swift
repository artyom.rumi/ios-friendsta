//
//  DefaultLocationCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 08.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

struct DefaultLocationCoder: LocationCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let latitude = "latitude"
        static let longitude = "longitude"
    }
    
    // MARK: - Instance Methods
    
    func encode(location: Location) -> [String: Any] {
        var json: [String: Any] = [:]
        
        json[JSONKeys.latitude] = location.latitude
        json[JSONKeys.longitude] = location.longitude
        
        return json
    }
}
