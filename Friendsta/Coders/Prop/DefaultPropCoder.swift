//
//  DefaultPropCoder.swift
//  Friendsta
//
//  Created by Marat Galeev on 28.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultPropCoder: PropCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        static let emoji = "emoji"
        static let message = "content"
        static let gender = "gender"
    }
    
    // MARK: - Instance Properties
    
    let genderCoder: GenderCoder
    
    // MARK: - Instance Methods
    
    func propUID(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }
    
    func decode(prop: Prop, from json: [String: Any]) -> Bool {
        guard let uid: Int64 = JSONKeys.uid <~~ json else {
            return false
        }
        
        guard let emoji: String = JSONKeys.emoji <~~ json else {
            return false
        }
        
        guard let message: String = JSONKeys.message <~~ json else {
            return false
        }
        
        let gender: Gender
        
        if let rawGender: String = JSONKeys.gender <~~ json {
            gender = self.genderCoder.gender(from: rawGender) ?? .nonBinary
        } else {
            gender = .nonBinary
        }
        
        prop.uid = uid
        prop.emoji = emoji
        prop.message = message
        prop.gender = gender
        
        return true
    }
}
