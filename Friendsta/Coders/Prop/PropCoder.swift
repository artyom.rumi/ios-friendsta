//
//  PropCoder.swift
//  Friendsta
//
//  Created by Marat Galeev on 28.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol PropCoder {
    
    // MARK: - Instance Methods
    
    func propUID(from json: [String: Any]) -> Int64?
    
    func decode(prop: Prop, from json: [String: Any]) -> Bool
}
