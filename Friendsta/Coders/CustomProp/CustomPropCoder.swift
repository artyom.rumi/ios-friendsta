//
//  CustomPropCoder.swift
//  Friendsta
//
//  Created by Marat Galeev on 28.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol CustomPropCoder {
    
    // MARK: - Instance Methods
    
    func decode(customProp: CustomProp, uid: Int64, from json: [String: Any]) -> Bool
}
