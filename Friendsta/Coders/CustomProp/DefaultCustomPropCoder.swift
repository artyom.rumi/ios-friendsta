//
//  DefaultCustomPropCoder.swift
//  Friendsta
//
//  Created by Marat Galeev on 28.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultCustomPropCoder: CustomPropCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let emoji = "emoji"
        static let message = "content"
    }
    
    // MARK: - Instance Methods
    
    func decode(customProp: CustomProp, uid: Int64, from json: [String: Any]) -> Bool {
        guard let emoji: String = JSONKeys.emoji <~~ json else {
            return false
        }
        
        guard let message: String = JSONKeys.message <~~ json else {
            return false
        }
    
        customProp.uid = uid
        customProp.emoji = emoji
        customProp.message = message
        
        return true
    }
}
