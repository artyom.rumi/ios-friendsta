//
//  DefaultPhotoCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultPhotoCoder: PhotoCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let image = "image"
        
        static let imageURL = "original"
        static let smallImageURL = "small"
        static let mediumImageURL = "medium"
        static let largeImageURL = "large"
    }
    
    // MARK: - Instance Methods
    
    func photo(from json: [String: Any]) -> Photo? {
        if let imageJSON: [String: Any] = JSONKeys.image <~~ json {
            return self.photo(from: imageJSON)
        } else {
            guard let imageURL: URL = JSONKeys.imageURL <~~ json else {
                return nil
            }
            
            guard let smallImageURL: URL = JSONKeys.smallImageURL <~~ json else {
                return nil
            }
            
            guard let mediumImageURL: URL = JSONKeys.mediumImageURL <~~ json else {
                return nil
            }
           
            guard let largeImageURL: URL = JSONKeys.largeImageURL <~~ json else {
                return nil
            }
            
            return Photo(imageURL: imageURL,
                         smallImageURL: smallImageURL,
                         mediumImageURL: mediumImageURL,
                         largeImageURL: largeImageURL)
        }
    }
}
