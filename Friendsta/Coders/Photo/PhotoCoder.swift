//
//  PhotoCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol PhotoCoder {

    // MARK: - Instance Methods
    
    func photo(from json: [String: Any]) -> Photo?
}
