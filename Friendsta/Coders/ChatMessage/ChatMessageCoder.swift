//
//  ChatMessageCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ChatMessageCoder {
    
    // MARK: - Instance Methods
    
    func chatMessageUID(from chatClientMessage: ChatClientMessage) -> String?
    func chatMessageType(from chatClientMessage: ChatClientMessage) -> ChatMessageType?
    
    func pushPayload(for chatMessage: ChatMessage, senderName: String?, exceptions: [String]) -> ChatClientPushPayload?
    
    func decode(chatMessage: ChatMessage, from chatClientMessage: ChatClientMessage) -> Bool
    func encode(chatMessage: ChatMessage) -> ChatClientMessage?
}

// MARK: -

extension ChatMessageCoder {
    
    // MARK: - Instance Methods
    
    func pushPayload(for chatMessage: ChatMessage, senderName: String?) -> ChatClientPushPayload? {
        return self.pushPayload(for: chatMessage, senderName: senderName, exceptions: [])
    }
}
