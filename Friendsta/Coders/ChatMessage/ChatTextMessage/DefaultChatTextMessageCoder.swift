//
//  DefaultChatTextMessageCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultChatTextMessageCoder: ChatTextMessageCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum ContentKeys {
        
        // MARK: - Type Properties
        
        static let type = "type"
        static let text = "text"
    }
    
    // MARK: - Instance Properties
    
    let chatMessageTypeCoder: ChatMessageTypeCoder
    
    // MARK: - Instance Methods
    
    func pushPayload(for chatMessage: ChatTextMessage, senderName: String?, exceptions: [String]) -> ChatClientPushPayload? {
        var alert: [String: Any] = ["title-loc-key": "apns_chat_message_title"]
        
        if let senderName = senderName {
            alert["loc-key"] = "apns_chat_text_message_description"
            alert["loc-args"] = [senderName, chatMessage.text ?? ""]
        } else {
            alert["loc-key"] = "apns_chat_incognito_text_message_description"
            alert["loc-args"] = [chatMessage.text ?? ""]
        }
        
        let apnsPushPayload: [String: Any] = ["aps": ["category": "chat_message",
                                                      "alert": alert,
                                                      "badge": 1,
                                                      "sound": "default",
                                                      "mutable-content": 1],
                                              "category_type": "chat_message",
                                              "chat_id": chatMessage.chatUID,
                                              "chat_sender_id": chatMessage.userUID]
        
        return ChatClientPushPayload(apns: apnsPushPayload, exceptions: exceptions)
    }
    
    func decode(chatMessage: ChatTextMessage, from chatClientMessage: ChatClientMessage) -> Bool {
        guard let rawType: String = ContentKeys.type <~~ chatClientMessage.content else {
            return false
        }
        
        switch self.chatMessageTypeCoder.chatMessageType(from: rawType) {
        case .some(.text):
            guard let text: String = ContentKeys.text <~~ chatClientMessage.content else {
                return false
            }
            
            chatMessage.uid = chatClientMessage.uid
            
            chatMessage.createdDate = chatClientMessage.createdDate
            chatMessage.modifiedDate = chatClientMessage.modifiedDate
            chatMessage.deliveredDate = chatClientMessage.deliveredDate
            chatMessage.viewedDate = chatClientMessage.viewedDate
            chatMessage.erasedDate = chatClientMessage.erasedDate
            
            chatMessage.isAnonymous = chatClientMessage.isAnonymous
            
            chatMessage.text = text
         
            return true
            
        case .some(.photo), .some(.info), .none:
            return false
        }
    }
    
    func encode(chatMessage: ChatTextMessage) -> ChatClientMessage? {
        guard let text = chatMessage.text else {
            return nil
        }
        
        let content = [ContentKeys.type: self.chatMessageTypeCoder.value(from: .text),
                       ContentKeys.text: text]
        
        return ChatClientMessage(uid: chatMessage.uid ?? "",
                                 createdDate: chatMessage.createdDate ?? Date(),
                                 modifiedDate: chatMessage.modifiedDate,
                                 deliveredDate: chatMessage.deliveredDate,
                                 viewedDate: chatMessage.viewedDate,
                                 erasedDate: chatMessage.erasedDate,
                                 isAnonymous: chatMessage.isAnonymous,
                                 content: content)
    }
}
