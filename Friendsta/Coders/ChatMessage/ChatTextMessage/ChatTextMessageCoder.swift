//
//  ChatTextMessageCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ChatTextMessageCoder {
    
    // MARK: - Instance Methods
 
    func pushPayload(for chatMessage: ChatTextMessage, senderName: String?, exceptions: [String]) -> ChatClientPushPayload?
    
    func decode(chatMessage: ChatTextMessage, from chatClientMessage: ChatClientMessage) -> Bool
    func encode(chatMessage: ChatTextMessage) -> ChatClientMessage?
}

// MARK: -

extension ChatTextMessageCoder {
    
    // MARK: - Instance Methods
    
    func pushPayload(for chatMessage: ChatTextMessage, senderName: String) -> ChatClientPushPayload? {
        return self.pushPayload(for: chatMessage, senderName: senderName, exceptions: [])
    }
}
