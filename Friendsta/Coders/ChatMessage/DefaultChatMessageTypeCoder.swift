//
//  DefaultChatMessageTypeCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

struct DefaultChatMessageTypeCoder: ChatMessageTypeCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum Values {
        
        // MARK: - Type Properties
        
        static let text = "text"
        static let photo = "photo"
        static let info = "info"
    }
    
    // MARK: - Instance Methods
    
    func chatMessageType(from value: Any) -> ChatMessageType? {
        switch (value as? String)?.lowercased() {
        case .some(Values.text):
            return .text
            
        case .some(Values.photo):
            return .photo
            
        case .some(Values.info):
            return .info
            
        default:
            return nil
        }
    }
    
    func value(from chatMessageType: ChatMessageType) -> Any {
        switch chatMessageType {
        case .text:
            return Values.text
            
        case .photo:
            return Values.photo
            
        case .info:
            return Values.info
        }
    }
}
