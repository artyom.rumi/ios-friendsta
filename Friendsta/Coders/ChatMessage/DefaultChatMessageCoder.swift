//
//  DefaultChatMessageCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultChatMessageCoder: ChatMessageCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum ContentKeys {
        
        // MARK: - Type Properties
        
        static let type = "type"
    }
    
    // MARK: - Instance Properties
    
    let chatMessageTypeCoder: ChatMessageTypeCoder
    
    let chatTextMessageCoder: ChatTextMessageCoder
    let chatPhotoMessageCoder: ChatPhotoMessageCoder
    let chatInfoMessageCoder: ChatInfoMessageCoder
    
    // MARK: - Instance Methods
    
    func chatMessageType(from chatClientMessage: ChatClientMessage) -> ChatMessageType? {
        guard let rawType: String = ContentKeys.type <~~ chatClientMessage.content else {
            return nil
        }
        
        return self.chatMessageTypeCoder.chatMessageType(from: rawType)
    }
    
    func chatMessageUID(from chatClientMessage: ChatClientMessage) -> String? {
        return chatClientMessage.uid
    }
    
    func pushPayload(for chatMessage: ChatMessage, senderName: String?, exceptions: [String]) -> ChatClientPushPayload? {
        switch chatMessage {
        case let chatTextMessage as ChatTextMessage:
            return self.chatTextMessageCoder.pushPayload(for: chatTextMessage,
                                                         senderName: senderName,
                                                         exceptions: exceptions)
            
        case let chatPhotoMessage as ChatPhotoMessage:
            return self.chatPhotoMessageCoder.pushPayload(for: chatPhotoMessage,
                                                          senderName: senderName,
                                                          exceptions: exceptions)
            
        case let chatInfoMessage as ChatInfoMessage:
            return self.chatInfoMessageCoder.pushPayload(for: chatInfoMessage,
                                                         exceptions: exceptions)
            
        default:
            return nil
        }
    }
    
    func decode(chatMessage: ChatMessage, from chatClientMessage: ChatClientMessage) -> Bool {
        switch chatMessage {
        case let chatTextMessage as ChatTextMessage:
            return self.chatTextMessageCoder.decode(chatMessage: chatTextMessage,
                                                    from: chatClientMessage)
            
        case let chatPhotoMessage as ChatPhotoMessage:
            
            if let doubleCheckDate = chatPhotoMessage.createdDate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
                let createdDateFormatted = dateFormatter.string(from: doubleCheckDate)
                //print("#*# @@__@@ -U- DefaultChatMessageCoder -chatPhotoMessage.createdDate: \(createdDateFormatted)\n")
            }
            
            //print("#*# ??__?? U DefaultChatMessageCoder -chatPhotoMessageDate: \(chatPhotoMessage.createdDate as Any)\n")
            
            return self.chatPhotoMessageCoder.decode(chatMessage: chatPhotoMessage,
                                                     from: chatClientMessage)
            
        case let chatInfoMessage as ChatInfoMessage:
            
            if let doubleCheckDate = chatInfoMessage.createdDate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
                let createdDateFormatted = dateFormatter.string(from: doubleCheckDate)
                //print("#*# @@__@@ -V- DefaultChatMessageCoder -chatInfoMessage.createdDate: \(createdDateFormatted)\n")
            }
            
            //print("#*# ??__?? V DefaultChatMessageCoder -chatInfoMessageDate: \(chatInfoMessage.createdDate as Any)\n")
            
            return self.chatInfoMessageCoder.decode(chatMessage: chatInfoMessage,
                                                    from: chatClientMessage)
            
        default:
            return false
        }
    }
    
    func encode(chatMessage: ChatMessage) -> ChatClientMessage? {
        switch chatMessage {
        case let chatTextMessage as ChatTextMessage:
            return self.chatTextMessageCoder.encode(chatMessage: chatTextMessage)
            
        case let chatPhotoMessage as ChatPhotoMessage:
            return self.chatPhotoMessageCoder.encode(chatMessage: chatPhotoMessage)
            
        case let chatInfoMessage as ChatInfoMessage:
            return self.chatInfoMessageCoder.encode(chatMessage: chatInfoMessage)
            
        default:
            return nil
        }
    }
}
