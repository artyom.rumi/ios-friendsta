//
//  ChatPhotoMessageCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ChatPhotoMessageCoder {
    
    // MARK: - Instance Methods
    
    func pushPayload(for chatMessage: ChatPhotoMessage, senderName: String?, exceptions: [String]) -> ChatClientPushPayload?
    
    func decode(chatMessage: ChatPhotoMessage, from chatClientMessage: ChatClientMessage) -> Bool
    func encode(chatMessage: ChatPhotoMessage) -> ChatClientMessage?
}

// MARK: -

extension ChatPhotoMessageCoder {
    
    // MARK: - Instance Methods
    
    func pushPayload(for chatMessage: ChatPhotoMessage, senderName: String) -> ChatClientPushPayload? {
        return self.pushPayload(for: chatMessage, senderName: senderName, exceptions: [])
    }
}
