//
//  DefaultChatPhotoMessageCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultChatPhotoMessageCoder: ChatPhotoMessageCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum ContentKeys {
        
        // MARK: - Type Properties
        
        static let type = "type"
        
        static let photoURL = "photo_url"
        
        static let smallPhotoURL = "small_photo_url"
        static let mediumPhotoURL = "medium_photo_url"
        static let largePhotoURL = "large_photo_url"

        static let text = "comment"
    }
    
    // MARK: - Instance Properties
    
    let chatMessageTypeCoder: ChatMessageTypeCoder
    
    // MARK: - Instance Methods
    
    func pushPayload(for chatMessage: ChatPhotoMessage, senderName: String?, exceptions: [String]) -> ChatClientPushPayload? {
        var alert: [String: Any] = ["title-loc-key": "apns_chat_message_title"]
        
        if let senderName = senderName {
            alert["loc-key"] = "apns_chat_photo_message_description"
            alert["loc-args"] = [senderName]
        } else {
            alert["loc-key"] = "apns_chat_incognito_photo_message_description"
        }
        
        let apnsPushPayload: [String: Any] = ["aps": ["category": "chat_message",
                                                      "alert": alert,
                                                      "badge": 1,
                                                      "sound": "default",
                                                      "mutable-content": 1],
                                              "category_type": "chat_message",
                                              "chat_id": chatMessage.chatUID,
                                              "chat_sender_id": chatMessage.userUID]
        
        return ChatClientPushPayload(apns: apnsPushPayload, exceptions: exceptions)
    }
    
    func decode(chatMessage: ChatPhotoMessage, from chatClientMessage: ChatClientMessage) -> Bool {
        guard let rawType: String = ContentKeys.type <~~ chatClientMessage.content else {
            return false
        }
        
        switch self.chatMessageTypeCoder.chatMessageType(from: rawType) {
        case .some(.photo):
            let photoURL: URL? = ContentKeys.photoURL <~~ chatClientMessage.content
            
            let smallPhotoURL: URL? = ContentKeys.smallPhotoURL <~~ chatClientMessage.content
            let mediumPhotoURL: URL? = ContentKeys.mediumPhotoURL <~~ chatClientMessage.content
            let largePhotoURL: URL? = ContentKeys.largePhotoURL <~~ chatClientMessage.content
            
            chatMessage.uid = chatClientMessage.uid
            
            //print("#*# ??__?? A DefaultChatPhotoMessageCoder -chatPhotoMessageDate: \(chatMessage.createdDate as Any)\n")
            
            let messageCreationDate = chatMessage.createdDate
            
            if let doubleCheckDate = messageCreationDate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
                let createdDateFormatted = dateFormatter.string(from: doubleCheckDate)
                //print("#*# ==++== A DefaultChatPhotoMessageCoder -date: \(createdDateFormatted)\n")
            }
            
            chatMessage.createdDate = messageCreationDate
            chatMessage.modifiedDate = chatClientMessage.modifiedDate
            chatMessage.deliveredDate = chatClientMessage.deliveredDate
            chatMessage.viewedDate = chatClientMessage.viewedDate
            chatMessage.erasedDate = chatClientMessage.erasedDate
            
            chatMessage.photoURL = photoURL
            
            chatMessage.smallPhotoURL = smallPhotoURL
            chatMessage.mediumPhotoURL = mediumPhotoURL
            chatMessage.largePhotoURL = largePhotoURL
            
            chatMessage.isAnonymous = chatClientMessage.isAnonymous

            chatMessage.text = ContentKeys.text <~~ chatClientMessage.content
            
            return true
            
        case .some(.text), .some(.info), .none:
            return false
        }
    }
    
    func encode(chatMessage: ChatPhotoMessage) -> ChatClientMessage? {
        guard let photoRawURL = chatMessage.photoRawURL else {
            return nil
        }
        
        var content = [ContentKeys.type: self.chatMessageTypeCoder.value(from: .photo),
                       ContentKeys.photoURL: photoRawURL,
                       ContentKeys.smallPhotoURL: chatMessage.smallPhotoRawURL ?? photoRawURL,
                       ContentKeys.mediumPhotoURL: chatMessage.mediumPhotoRawURL ?? photoRawURL,
                       ContentKeys.largePhotoURL: chatMessage.largePhotoRawURL ?? photoRawURL]

        if let text = chatMessage.text {
            content[ContentKeys.text] = text
        }
        
        return ChatClientMessage(uid: chatMessage.uid ?? "",
                                 createdDate: chatMessage.createdDate ?? Date(),
                                 modifiedDate: chatMessage.modifiedDate ?? Date(),
                                 deliveredDate: chatMessage.deliveredDate,
                                 viewedDate: chatMessage.viewedDate,
                                 erasedDate: chatMessage.erasedDate,
                                 isAnonymous: chatMessage.isAnonymous,
                                 content: content)
    }
}
