//
//  ChatInfoTypeCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ChatInfoTypeCoder {
    
    // MARK: - Instance Methods
    
    func chatInfoType(from value: Any) -> ChatInfoType?
    func value(from chatInfoType: ChatInfoType) -> Any
}
