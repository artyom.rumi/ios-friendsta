//
//  DefaultChatInfoMessageCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultChatInfoMessageCoder: ChatInfoMessageCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum ContentKeys {
        
        // MARK: - Type Properties
        
        static let type = "type"
        static let infoType = "info_type"
    }
    
    // MARK: - Instance Properties
    
    let chatMessageTypeCoder: ChatMessageTypeCoder
    
    let chatInfoTypeCoder: ChatInfoTypeCoder
    
    // MARK: - Instance Methods
    
    func pushPayload(for chatMessage: ChatInfoMessage, exceptions: [String]) -> ChatClientPushPayload? {
        let description: String
        
        switch chatMessage.type {
        case .some(.match):
            description = "apns_chat_match_message_description"
        
        case .some(.oneReveal):
            description = "apns_chat_one_reveal_description"
            
        case .some(.mutualReveal):
            description = "apns_chat_mutual_reveal_description"

        case .cancelReveal:
             description = "apns_chat_cancel_reveal_description"

        case .none:
            return nil
        }
        
        let alert: [String: Any] = ["title-loc-key": "apns_chat_message_title",
                                    "loc-key": description]
        
        let apnsPushPayload: [String: Any] = ["aps": ["category": "chat_message",
                                                      "alert": alert,
                                                      "badge": 1,
                                                      "sound": "default",
                                                      "mutable-content": 1],
                                              "category_type": "chat_message",
                                              "chat_id": chatMessage.chatUID,
                                              "chat_sender_id": chatMessage.userUID]
        
        return ChatClientPushPayload(apns: apnsPushPayload, exceptions: exceptions)
    }
    
    func decode(chatMessage: ChatInfoMessage, from chatClientMessage: ChatClientMessage) -> Bool {
        guard let rawType: String = ContentKeys.type <~~ chatClientMessage.content else {
            return false
        }
        
        switch self.chatMessageTypeCoder.chatMessageType(from: rawType) {
        case .some(.info):
            guard let infoRawType: String = ContentKeys.infoType <~~ chatClientMessage.content else {
                return false
            }
            
            guard let infoType = self.chatInfoTypeCoder.chatInfoType(from: infoRawType) else {
                return false
            }
            
            chatMessage.uid = chatClientMessage.uid
            
            let messageCreationDate = chatMessage.createdDate
            
            //print("#*# ??__?? B DefaultChatInfoMessageCoder -chatPhotoMessageDate: \(chatMessage.createdDate as Any)\n")
            
            if let doubleCheckDate = messageCreationDate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MMM-dd-yyyy • h:mm a"
                let createdDateFormatted = dateFormatter.string(from: doubleCheckDate)
                //print("#*# ==++== B DefaultChatInfoMessageCoder -date: \(createdDateFormatted)\n")
            }
            
            chatMessage.createdDate = messageCreationDate
            chatMessage.modifiedDate = chatClientMessage.modifiedDate
            chatMessage.deliveredDate = chatClientMessage.deliveredDate
            chatMessage.viewedDate = chatClientMessage.viewedDate
            chatMessage.erasedDate = chatClientMessage.erasedDate
            
            chatMessage.isAnonymous = chatClientMessage.isAnonymous
            
            chatMessage.type = infoType
            
            return true
            
        case .some(.text), .some(.photo), .none:
            return false
        }
    }
    
    func encode(chatMessage: ChatInfoMessage) -> ChatClientMessage? {
        guard let infoType = chatMessage.type else {
            return nil
        }
        
        let content = [ContentKeys.type: self.chatMessageTypeCoder.value(from: .info),
                       ContentKeys.infoType: self.chatInfoTypeCoder.value(from: infoType)]
        
        return ChatClientMessage(uid: chatMessage.uid ?? "",
                                 createdDate: chatMessage.createdDate ?? Date(),
                                 modifiedDate: chatMessage.modifiedDate ?? Date(),
                                 deliveredDate: chatMessage.deliveredDate ?? Date(),
                                 viewedDate: chatMessage.viewedDate,
                                 erasedDate: chatMessage.erasedDate,
                                 isAnonymous: chatMessage.isAnonymous,
                                 content: content)
    }
}
