//
//  DefaultChatInfoTypeCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

struct DefaultChatInfoTypeCoder: ChatInfoTypeCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum Values {
        
        // MARK: - Type Properties
        
        static let match = "match"
        static let oneReveal = "one_reveal"
        static let mutualReveal = "mutual_reveal"
        static let cancelReveal = "cancel_reveal"
    }
    
    // MARK: - Instance Methods
    
    func chatInfoType(from value: Any) -> ChatInfoType? {
        switch (value as? String)?.lowercased() {
        case .some(Values.match):
            return .match
            
        case .some(Values.oneReveal):
            return .oneReveal
            
        case .some(Values.mutualReveal):
            return .mutualReveal

        case .some(Values.cancelReveal):
            return .cancelReveal
            
        default:
            return nil
        }
    }
    
    func value(from chatInfoType: ChatInfoType) -> Any {
        switch chatInfoType {
        case .match:
            return Values.match
            
        case .oneReveal:
            return Values.oneReveal
            
        case .mutualReveal:
            return Values.mutualReveal

        case .cancelReveal:
            return Values.cancelReveal
        }
    }
}
