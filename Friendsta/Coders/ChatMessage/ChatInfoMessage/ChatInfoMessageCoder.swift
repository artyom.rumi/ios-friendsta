//
//  ChatInfoMessageCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ChatInfoMessageCoder {
    
    // MARK: - Instance Methods
    
    func pushPayload(for chatMessage: ChatInfoMessage, exceptions: [String]) -> ChatClientPushPayload?
    
    func decode(chatMessage: ChatInfoMessage, from chatClientMessage: ChatClientMessage) -> Bool
    func encode(chatMessage: ChatInfoMessage) -> ChatClientMessage?
}

// MARK: -

extension ChatInfoMessageCoder {
    
    // MARK: - Instance Methods
    
    func pushPayload(for chatMessage: ChatInfoMessage) -> ChatClientPushPayload? {
        return self.pushPayload(for: chatMessage, exceptions: [])
    }
}
