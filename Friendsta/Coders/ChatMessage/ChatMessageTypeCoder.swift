//
//  ChatMessageTypeCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ChatMessageTypeCoder {
    
    // MARK: - Instance Methods
    
    func chatMessageType(from value: Any) -> ChatMessageType?
    func value(from chatMessageType: ChatMessageType) -> Any
}
