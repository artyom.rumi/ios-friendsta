//
//  DefaultPropRequestCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 27.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

class DefaultPropRequestCoder: PropRequestCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        
        static let progress = "progress"
        static let count = "respondents_amount"
    }
    
    // MARK: - Instance Methods
    
    func propRequestUID(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }
    
    func decode(propRequest: PropRequest, from json: [String: Any]) -> Bool {
        guard let uid: Int64 = JSONKeys.uid <~~ json else {
            return false
        }
        
        guard let progress: Int16 = JSONKeys.progress <~~ json else {
            return false
        }
        
        guard let count: Int16 = JSONKeys.count <~~ json else {
            return false
        }
        
        propRequest.uid = uid
        propRequest.progress = progress
        propRequest.count = count
        
        return true
    }
}
