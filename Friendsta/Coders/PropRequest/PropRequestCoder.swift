//
//  PropRequestCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 27.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol PropRequestCoder {
    
    // MARK: - Instance Metdos
    
    func propRequestUID(from json: [String: Any]) -> Int64?
    
    func decode(propRequest: PropRequest, from json: [String: Any]) -> Bool
}
