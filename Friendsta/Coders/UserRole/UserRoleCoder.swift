//
//  UserRoleCoder.swift
//  Friendsta
//
//  Created by Marat Galeev on 19.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol UserRoleCoder {
    
    // MARK: - Instance Methods
    
    func userRole(from value: Any) -> UserRole?
    func value(from userRole: UserRole) -> Any
}
