//
//  DefaultUserRoleCoder.swift
//  Friendsta
//
//  Created by Marat Galeev on 19.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

struct DefaultUserRoleCoder: UserRoleCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum Values {
        
        // MARK: - Type Properties
        
        static let ordinary = "ordinary"
        static let test = "test"
        static let bot = "bot"
    }
    
    // MARK: - Instance Methods
    
    func userRole(from value: Any) -> UserRole? {
        switch (value as? String)?.lowercased() {
        case .some(Values.ordinary):
            return .ordinary
            
        case .some(Values.test):
            return .test
            
        case .some(Values.bot):
            return .bot
            
        default:
            return nil
        }
    }
    
    func value(from userRole: UserRole) -> Any {
        switch userRole {
        case .ordinary:
            return Values.ordinary
            
        case .test:
            return Values.test
            
        case .bot:
            return Values.bot
        }
    }
}
