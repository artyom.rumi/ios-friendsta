//
//  DefaultUserCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultUserCoder: UserCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        static let role = "role"
        
        static let avatar = "avatar"
        static let avatarPlaceholder = "default_avatar"
        
        static let firstName = "first_name"
        static let lastName = "last_name"
        
        static let classYear = "grade"
        static let schoolTitle = "school_name"
        
        static let gender = "gender"
        static let bio = "bio"
        
        static let friendsCount = "friends_count"
        static let pokesCount = "pokes_count"
        
        static let topPokes = "top_pokes"
        
        static let canCreateChat = "chat_available"
        static let canSendProp = "followee"
        static let canSendCustomProp = "custom_prop_available"
        
        static let isCommunity = "followee"
        static let isBlocked = "blocked"

        static let friendshipStatus = "friendship_status"
        static let daysToExpiredFriendship = "days_to_expired_friendship"
        
        static let relationshipStatus = "relationship_status"

        static let users = "users"
        static let meta = "meta"
    }
    
    // MARK: - Instance Properties
    
    let userRoleCoder: UserRoleCoder
    let genderCoder: GenderCoder
    let photoCoder: PhotoCoder
    let propCoder: PropCoder
    
    // MARK: - Instance Methods
    
    func userUID(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }
    
    func pokesUIDs(from json: [String: Any]) -> [Int64]? {
        guard let pokesJSONs: [[String: Any]] = JSONKeys.topPokes <~~ json else {
            return nil
        }
        
        var pokesUIDs: [Int64] = []
        
        for pokeJSON in pokesJSONs {
            guard let pokeUID = self.propCoder.propUID(from: pokeJSON) else {
                return nil
            }
            
            pokesUIDs.append(pokeUID)
        }
        
        return pokesUIDs
    }
    
    func pokesArrayJSON(from json: [String: Any]) -> [[String: Any]] {
        guard let topPokesArrayJSON: [[String: Any]] = JSONKeys.topPokes <~~ json else {
            return []
        }
        
        return topPokesArrayJSON
    }

    func usersJSON(from json: [String: Any]) -> [[String: Any]]? {
        return JSONKeys.users <~~ json
    }

    func metaJSON(from json: [String: Any]) -> [String: Any]? {
        return JSONKeys.meta <~~ json
    }
    
    func decode(user: User, from json: [String: Any]) -> Bool {
        guard let uid: Int64 = JSONKeys.uid <~~ json else {
            return false
        }
        
        let role: UserRole?
        
        if let rawRole: String = JSONKeys.role <~~ json {
            role = self.userRoleCoder.userRole(from: rawRole)
        } else {
            role = .ordinary
        }
        
        let avatar: Photo?
        
        if let avatarJSON: [String: Any] = JSONKeys.avatar <~~ json {
            avatar = self.photoCoder.photo(from: avatarJSON)
        } else {
            avatar = nil
        }
        
        let avatarPlaceholder: Photo?
        
        if let avatarPlaceholderJSON: [String: Any] = JSONKeys.avatarPlaceholder <~~ json {
            avatarPlaceholder = self.photoCoder.photo(from: avatarPlaceholderJSON)
        } else {
            avatarPlaceholder = nil
        }
        
        guard let firstName: String = JSONKeys.firstName <~~ json else {
            return false
        }
        
        guard let lastName: String = JSONKeys.lastName <~~ json else {
            return false
        }

        var fullName = firstName

        if !firstName.isEmpty {
            if !lastName.isEmpty {
                 fullName = "\(firstName) \(lastName)"
            }
        } else {
            fullName = lastName
        }
        
        guard let classYear: Int16 = JSONKeys.classYear <~~ json else {
            return false
        }
        
        guard let schoolTitle: String = JSONKeys.schoolTitle <~~ json else {
            return false
        }
        
        guard let rawGender: String = JSONKeys.gender <~~ json else {
            return false
        }
        
        guard let gender = self.genderCoder.gender(from: rawGender) else {
            return false
        }

        if let friendshipStatus: String = JSONKeys.friendshipStatus <~~ json {
            user.friendshipStatus = FriendshipStatusType(rawValue: friendshipStatus)
        } else {
            user.friendshipStatus = nil
        }
        
        let bio: String? = JSONKeys.bio <~~ json
        
        let canCreateChat: Bool? = JSONKeys.canCreateChat <~~ json
        let canSendProp: Bool? = JSONKeys.canSendProp <~~ json
        let canSendCustomProp: Bool? = JSONKeys.canSendCustomProp <~~ json
        
        let isCommunity: Bool? = JSONKeys.isCommunity <~~ json
        let isBlocked: Bool? = JSONKeys.isBlocked <~~ json
        
        let friendsCount: Int16 = JSONKeys.friendsCount <~~ json ?? 0
        let pokesCount: Int16 = JSONKeys.pokesCount <~~ json ?? 0
        
        let relationshipStatus: String? = JSONKeys.relationshipStatus <~~ json
        
        user.uid = uid
        user.role = role ?? .ordinary
        
        user.avatar = avatar ?? avatarPlaceholder
        
        user.firstName = firstName
        user.lastName = lastName
        user.fullName = fullName
        
        user.classYear = classYear
        user.schoolTitle = schoolTitle
        
        user.gender = gender
        user.bio = bio
        
        user.friendsCount = friendsCount
        user.pokesCount = pokesCount
        
        user.canCreateChat = canCreateChat ?? false
        user.canSendProp = canSendProp ?? false
        user.canSendCustomProp = canSendCustomProp ?? false
        
        user.isCommunity = isCommunity ?? false
        user.isBlocked = isBlocked ?? false
        
        user.relationshipStatus = relationshipStatus

        if let daysToExpiredFriendship: Int64 = JSONKeys.daysToExpiredFriendship <~~ json {
            user.daysToExpiredFriendship = daysToExpiredFriendship
        }
        
        return true
    }
}
