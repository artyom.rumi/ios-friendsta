//
//  UserCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol UserCoder {
    
    // MARK: - Instance Methods
    
    func userUID(from json: [String: Any]) -> Int64?
    
    func pokesUIDs(from json: [String: Any]) -> [Int64]?
    func pokesArrayJSON(from json: [String: Any]) -> [[String: Any]]
    func usersJSON(from json: [String: Any]) -> [[String: Any]]?
    func metaJSON(from json: [String: Any]) -> [String: Any]?
    
    func decode(user: User, from json: [String: Any]) -> Bool
}
