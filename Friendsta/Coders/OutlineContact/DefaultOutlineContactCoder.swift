//
//  DefaultOutlineContactCoder.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultOutlineContactCoder: OutlineContactCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        
        static let fullName = "name"
        static let phoneNumber = "phone_number"
        static let isInvited = "invited"

        static let users = "users"
        static let meta = "meta"
    }
    
    // MARK: - Instance Methods
    
    func outlineContactUID(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }

    func outlineContactsJSON(from json: [String: Any]) -> [[String: Any]]? {
        return JSONKeys.users <~~ json
    }

    func metaJSON(from json: [String: Any]) -> [String: Any]? {
        return JSONKeys.meta <~~ json
    }
    
    func decode(outlineContact: OutlineContact, from json: [String: Any]) -> Bool {
        guard let uid: Int64 = JSONKeys.uid <~~ json else {
            return false
        }
        
        guard let fullName: String = JSONKeys.fullName <~~ json else {
            return false
        }
        
        guard let phoneNumber: String = JSONKeys.phoneNumber <~~ json else {
            return false
        }
        
        guard let isInvited: Bool = JSONKeys.isInvited <~~ json else {
            return false
        }
        
        outlineContact.uid = uid
        
        outlineContact.fullName = fullName
        outlineContact.phoneNumber = phoneNumber
        outlineContact.isInvited = isInvited
        
        return true
    }
}
