//
//  OutlineContactCoder.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

protocol OutlineContactCoder {
    
    // MARK: - Instance Methods
    
    func outlineContactUID(from json: [String: Any]) -> Int64?

    func outlineContactsJSON(from json: [String: Any]) -> [[String: Any]]?
    func metaJSON(from json: [String: Any]) -> [String: Any]?
    
    func decode(outlineContact: OutlineContact, from json: [String: Any]) -> Bool
}
