//
//  DefaultActivityCoder.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/27/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultActivityCoder: ActivityCoder {

    // MARK: - Nested Types

    private enum JSONKeys {

        // MARK: - Type Properties

        static let uid = "id"
        static let type = "type"
        static let message = "message"
        static let image = "image"
        static let smallImageURL = "small"
        static let createdAt = "created_at"
        static let isUnread = "is_unread"
        static let payload = "payload"
        static let isGrouped = "grouped"
        static let friendID = "friend_id"
        static let subtitle = "sender"
        static let feedID = "feed_id"
    }

    // MARK: - Instance Properties

    let dateFormatter: DateFormatter

    // MARK: - Instance Methods

    func decode(activity: Activity, from json: [String: Any]) -> Bool {

        guard let rawType: String = JSONKeys.type <~~ json else {
            return false
        }

        guard let createdDate = Gloss.Decoder.decode(dateForKey: JSONKeys.createdAt, dateFormatter: self.dateFormatter)(json) else {
            
            return false
        }

        activity.uid = JSONKeys.uid <~~ json ?? Int64(createdDate.timeIntervalSinceReferenceDate)
        activity.rawType = rawType
        activity.createdAt = createdDate

        activity.message = JSONKeys.message <~~ json

        if let image: [String: Any] = JSONKeys.image <~~ json {
            activity.imageURL = JSONKeys.smallImageURL <~~ image
        }

        activity.isUnread = JSONKeys.isUnread <~~ json ?? false

        if let payload: [String: Any] = JSONKeys.payload <~~ json {
            activity.isGrouped = JSONKeys.isGrouped <~~ payload ?? false
            activity.friendID = JSONKeys.friendID <~~ payload ?? 0
            activity.subtitle = JSONKeys.subtitle <~~ payload ?? ""
            activity.feedID = JSONKeys.feedID <~~ payload ?? 0
        }
        return true
    }

    func uid(from json: [String: Any]) -> Int64? {
        guard let createdDate = Gloss.Decoder.decode(dateForKey: JSONKeys.createdAt, dateFormatter: self.dateFormatter)(json) else {

            return nil
        }

        return JSONKeys.uid <~~ json ?? Int64(createdDate.timeIntervalSinceReferenceDate)
    }
}
