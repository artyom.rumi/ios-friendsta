//
//  ActivityCoder.swift
//  Friendsta
//
//  Created by Nikita Asabin on 11/27/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ActivityCoder: Coder {

    // MARK: - Instance Methods

    func decode(activity: Activity, from json: [String: Any]) -> Bool
}
