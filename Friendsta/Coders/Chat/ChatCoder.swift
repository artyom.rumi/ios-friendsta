//
//  ChatCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol ChatCoder {
    
    // MARK: - Instance Methods
    
    func chatUID(from json: [String: Any]) -> Int64?
    func userUIDs(from json: [String: Any]) -> [Int64]?
    
    func decode(chat: Chat, from json: [String: Any]) -> Bool
}
