//
//  DefaultChatCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 15.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultChatCoder: ChatCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        static let channel = "pubnub_channel_name"
        
        static let name = "name"
        
        static let isIncognito = "incognito"
        
        static let users = "users"
        
        static let owner = "owner"
        
        static let deanonymizations = "deanonymizations"
        static let deanonymizationUserUID = "user_id"
        
        static let isGroupChat = "is_group"
    }
    
    // MARK: - Instance Properties
    
    let userCoder: UserCoder
    
    let dateFormatter: DateFormatter
    
    // MARK: - Instance Methods
    
    func chatUID(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }
    
    func userUIDs(from json: [String: Any]) -> [Int64]? {
        guard let userJSONs: [[String: Any]] = JSONKeys.users <~~ json else {
            return nil
        }
        
        var userUIDs: [Int64] = []
        
        for userJSON in userJSONs {
            guard let userUID = self.userCoder.userUID(from: userJSON) else {
                return nil
            }
            
            userUIDs.append(userUID)
        }
        
        return userUIDs
    }
    
    func decode(chat: Chat, from json: [String: Any]) -> Bool {
        guard let uid: Int64 = JSONKeys.uid <~~ json else {
            return false
        }
        
        guard let channel: String = JSONKeys.channel <~~ json else {
            return false
        }
        
        guard let isIncognito: Bool = JSONKeys.isIncognito <~~ json else {
            return false
        }
        
        guard let ownerJSON: [String: Any] = JSONKeys.owner <~~ json else {
            return false
        }
        
        if let owner = chat.owner {
            guard self.userCoder.decode(user: owner, from: ownerJSON) else {
                return false
            }
        }
        
        if let chatMembers = chat.members?.allObjects as? [ChatMember], !chatMembers.isEmpty {
            guard let userJSONs: [[String: Any]] = JSONKeys.users <~~ json else {
                return false
            }
            
            let users = chatMembers.compactMap({ chatMember in
                return chatMember.user
            })
            
            guard userJSONs.count == users.count else {
                return false
            }
            
            for userJSON in userJSONs {
                guard let userUID = self.userCoder.userUID(from: userJSON) else {
                    return false
                }
                
                guard let user = users.first(where: { user in
                    return (user.uid == userUID)
                }) else {
                    return false
                }
                
                guard self.userCoder.decode(user: user, from: userJSON) else {
                    return false
                }
            }
            
            for chatMember in chatMembers {
                chatMember.isDeanonymized = false
            }
            
            if let deanonymizationJSONs: [[String: Any]] = JSONKeys.deanonymizations <~~ json {
                for deanonymizationJSON in deanonymizationJSONs {
                    guard let deanonymizationUserUID: Int64 = JSONKeys.deanonymizationUserUID <~~ deanonymizationJSON else {
                        return false
                    }
                    
                    guard let chatMember = chatMembers.first(where: { chatMember in
                        return (chatMember.user?.uid == deanonymizationUserUID)
                    }) else {
                        return false
                    }
                    
                    chatMember.isDeanonymized = true
                }
            }
        }
        
        chat.name = JSONKeys.name <~~ json
        
        chat.isGroupChat = (JSONKeys.isGroupChat <~~ json ?? false)
        
        chat.uid = uid
        chat.channel = channel
        
        chat.isIncognito = isIncognito
        
        return true
    }
}
