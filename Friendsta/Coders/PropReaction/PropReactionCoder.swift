//
//  PropReactionCoder.swift
//  Friendsta
//
//  Created by Marat Galeev on 12.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol PropReactionCoder {
    
    // MARK: - Instance Metdos
    
    func propReactionUID(from json: [String: Any]) -> Int64?
    
    func decode(propReaction: PropReaction, from json: [String: Any]) -> Bool
}
