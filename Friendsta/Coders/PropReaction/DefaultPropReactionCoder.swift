//
//  DefaultPropReactionCoder.swift
//  Friendsta
//
//  Created by Marat Galeev on 12.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultPropReactionCoder: PropReactionCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        static let message = "content"
        static let readAtDate = "read_at"
        static let createdAtDate = "created_at"
        
        static let photo = "photo"
        
        static let largePhotoURL = "large"
        static let originalPhotoURL = "original"
        static let mediumPhotoURL = "medium"
        static let smallPhotoURL = "small"
    }
    
    // MARK: - Instance Properties
    
    let dateFormatter: DateFormatter
    
    // MARK: - Instance Methods
    
    func propReactionUID(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }
    
    func decode(propReaction: PropReaction, from json: [String: Any]) -> Bool {
        guard let uid: Int64 = JSONKeys.uid <~~ json else {
            return false
        }
        
        guard let message: String = JSONKeys.message <~~ json else {
            return false
        }
        
        let readAtDate: Date? = Gloss.Decoder.decode(dateForKey: JSONKeys.readAtDate,
                                                     dateFormatter: self.dateFormatter)(json)
        
        let createdAtDate: Date? = Gloss.Decoder.decode(dateForKey: JSONKeys.createdAtDate,
                                                     dateFormatter: self.dateFormatter)(json)
        
        if let photoJSON: [String: Any] = JSONKeys.photo <~~ json {
            guard let originalPhotoURL: String = JSONKeys.originalPhotoURL <~~ photoJSON else {
                return false
            }
            
            guard let smallPhotoURL: String = JSONKeys.smallPhotoURL <~~ photoJSON else {
                return false
            }
            
            guard let mediumPhotoURL: String = JSONKeys.mediumPhotoURL <~~ photoJSON else {
                return false
            }
            
            guard let largePhotoURL: String = JSONKeys.largePhotoURL <~~ photoJSON else {
                return false
            }
            
            propReaction.originalPhotoURL = originalPhotoURL
            propReaction.smallPhotoURL = smallPhotoURL
            propReaction.mediumPhotoURL = mediumPhotoURL
            propReaction.largePhotoURL = largePhotoURL
        }
        
        propReaction.uid = uid
        propReaction.message = message
        propReaction.readAtDate = readAtDate
        propReaction.createdAtDate = createdAtDate
        
        return true
    }
}
