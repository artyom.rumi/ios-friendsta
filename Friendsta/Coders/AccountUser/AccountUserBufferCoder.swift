//
//  AccountUserBufferCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol AccountUserBufferCoder {

    // MARK: - Instance Methods
    
    func encode(accountUserBuffer: AccountUserBuffer, uid: Int64?) -> [String: Any]
}

// MARK: -

extension AccountUserBufferCoder {
    
    // MARK: - Instance Methods
    
    func encode(accountUserBuffer: AccountUserBuffer) -> [String: Any] {
        return self.encode(accountUserBuffer: accountUserBuffer, uid: nil)
    }
}
