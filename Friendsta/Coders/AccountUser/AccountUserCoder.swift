//
//  AccountUserCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol AccountUserCoder {
    
    // MARK: - Instance Methods
    
    func accountUserUID(from json: [String: Any]) -> Int64?
    
    func pokesUIDs(from json: [String: Any]) -> [Int64]?
    func pokesArrayJSON(from json: [String: Any]) -> [[String: Any]]
    
    func decode(accountUser: AccountUser, from json: [String: Any]) -> Bool
    func encode(accountUser: AccountUser) -> [String: Any]
}
