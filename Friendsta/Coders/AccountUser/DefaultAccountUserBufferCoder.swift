//
//  DefaultAccountUserBufferCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

struct DefaultAccountUserBufferCoder: AccountUserBufferCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "uid"
        
        static let firstName = "first_name"
        static let lastName = "last_name"
        
        static let age = "age"
        static let bio = "bio"
        
        static let classYear = "grade"
        static let schoolUID = "school_id"
        
        static let gender = "gender"
        
        static let phoneNumbers = "phone_numbers"
        
        static let hideTopPokes = "hide_top_pokes"
    }
    
    // MARK: - Instance Properties
    
    let genderCoder: GenderCoder
    
    // MARK: - Instance Methods
    
    func encode(accountUserBuffer: AccountUserBuffer, uid: Int64?) -> [String: Any] {
        var json: [String: Any] = [:]
        
        if let uid = uid {
            json[JSONKeys.uid] = uid
        }
        
        if let firstName = accountUserBuffer.firstName {
            json[JSONKeys.firstName] = firstName
        }
        
        if let lastName = accountUserBuffer.lastName {
            json[JSONKeys.lastName] = lastName
        }
        
        if let age = accountUserBuffer.age {
            json[JSONKeys.age] = age
        }
        
        if let bio = accountUserBuffer.bio {
            json[JSONKeys.bio] = bio
        }
        
        if let classYear = accountUserBuffer.classYear {
            json[JSONKeys.classYear] = classYear
        }
        
        if let schoolUID = accountUserBuffer.schoolUID {
            json[JSONKeys.schoolUID] = schoolUID
        }
        
        if let gender = accountUserBuffer.gender {
            json[JSONKeys.gender] = self.genderCoder.value(from: gender)
        }
        
        json[JSONKeys.phoneNumbers] = accountUserBuffer.phoneNumbers
        
        json[JSONKeys.hideTopPokes] = accountUserBuffer.hideTopPokesList
        
        return json
    }
}
