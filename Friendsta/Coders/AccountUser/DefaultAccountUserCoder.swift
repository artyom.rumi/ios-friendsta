//
//  DefaultAccountUserCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultAccountUserCoder: AccountUserCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        
        static let avatar = "avatar"
        static let avatarPlaceholder = "default_avatar"
        
        static let firstName = "first_name"
        static let lastName = "last_name"
        
        static let age = "age"
        static let bio = "bio"
        
        static let classYear = "grade"
        
        static let schoolUID = "school.id"
        static let schoolTitle = "school.name"
        static let schoolLocation = "school.address"
        
        static let gender = "gender"
        
        static let createdDate = "created_at"
        static let phoneNumber = "phone_number"
        
        static let friendsCount = "friends_count"
        static let pokesCount = "pokes_count"
        
        static let topPokes = "top_pokes"
        
        static let isReceivedPropNotificationsEnabled = "receiving_prop_notification_enabled"
        static let isPropRequestNotificationsEnabled = "prop_request_notification_enabled"
        static let isChatMessageNotificationEnabled = "chat_messages_notification_enabled"
        static let isNewUserNotificationsEnabled = "new_users_notification_enabled"
        static let isNewFriendNotificationsEnabled = "new_friends_notification_enabled"
        
        static let isHideTopPokesEnabled = "hide_top_pokes"
        
        static let isMatchingEnabled = "matching_enabled"
    }
    
    // MARK: - Instance Properties
    
    let genderCoder: GenderCoder
    let photoCoder: PhotoCoder
    let propCoder: PropCoder
    
    let dateFormatter: DateFormatter
    
    // MARK: - Instance Methods
    
    func accountUserUID(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }
    
    func pokesUIDs(from json: [String: Any]) -> [Int64]? {
        guard let pokesJSONs: [[String: Any]] = JSONKeys.topPokes <~~ json else {
            return nil
        }
        
        var pokesUIDs: [Int64] = []
        
        for pokeJSON in pokesJSONs {
            guard let pokeUID = self.propCoder.propUID(from: pokeJSON) else {
                return nil
            }
            
            pokesUIDs.append(pokeUID)
        }
        
        return pokesUIDs
    }
    
    func pokesArrayJSON(from json: [String: Any]) -> [[String: Any]] {
        guard let topPokesArrayJSON: [[String: Any]] = JSONKeys.topPokes <~~ json else {
            return []
        }
        
        return topPokesArrayJSON
    }
    
    func decode(accountUser: AccountUser, from json: [String: Any]) -> Bool {
        guard let uid: Int64 = JSONKeys.uid <~~ json else {
            return false
        }
        
        let avatar: Photo?
        
        if let avatarJSON: [String: Any] = JSONKeys.avatar <~~ json {
            avatar = self.photoCoder.photo(from: avatarJSON)
        } else {
            avatar = nil
        }
        
        let avatarPlaceholder: Photo?
        
        if let avatarPlaceholderJSON: [String: Any] = JSONKeys.avatarPlaceholder <~~ json {
            avatarPlaceholder = self.photoCoder.photo(from: avatarPlaceholderJSON)
        } else {
            avatarPlaceholder = nil
        }
        
        guard let firstName: String = JSONKeys.firstName <~~ json else {
            return false
        }
        
        guard let lastName: String = JSONKeys.lastName <~~ json else {
            return false
        }
        
        guard let age: Int16 = JSONKeys.age <~~ json else {
            return false
        }
        
        let bio: String? = JSONKeys.bio <~~ json
        
        guard let classYear: Int16 = JSONKeys.classYear <~~ json else {
            return false
        }
        
        guard let schoolUID: Int64 = JSONKeys.schoolUID <~~ json else {
            return false
        }
        
        guard let schoolTitle: String = JSONKeys.schoolTitle <~~ json else {
            return false
        }
        
        guard let schoolLocation: String = JSONKeys.schoolLocation <~~ json else {
            return false
        }
        
        guard let rawGender: String = JSONKeys.gender <~~ json else {
            return false
        }
        
        guard let gender = self.genderCoder.gender(from: rawGender) else {
            return false
        }
        
        let createdDate: Date? = Gloss.Decoder.decode(dateForKey: JSONKeys.createdDate,
                                                      dateFormatter: self.dateFormatter)(json)
        
        let phoneNumber: String? = JSONKeys.phoneNumber <~~ json
        
        let friendsCount: Int16 = JSONKeys.friendsCount <~~ json ?? 0
        let pokesCount: Int16 = JSONKeys.pokesCount <~~ json ?? 0
        
        let isReceivedPropNotificationsEnabled: Bool? = JSONKeys.isReceivedPropNotificationsEnabled <~~ json
        let isPropRequestNotificationsEnabled: Bool? = JSONKeys.isPropRequestNotificationsEnabled <~~ json
        let isChatMessageNotificationEnabled: Bool? = JSONKeys.isChatMessageNotificationEnabled <~~ json
        let isNewUserNotificationsEnabled: Bool? = JSONKeys.isNewUserNotificationsEnabled <~~ json
        let isNewFriendNotificationsEnabled: Bool? = JSONKeys.isNewFriendNotificationsEnabled <~~ json
        
        let isHideTopPokesEnabled: Bool? = JSONKeys.isHideTopPokesEnabled <~~ json
        
        guard let isMatchingEnabled: Bool = JSONKeys.isMatchingEnabled <~~ json else {
            return false
        }
        
        accountUser.uid = uid
        
        accountUser.avatar = avatar
        accountUser.avatarPlaceholder = avatarPlaceholder
        
        accountUser.firstName = firstName
        accountUser.lastName = lastName
        
        accountUser.age = age
        accountUser.bio = bio
        
        accountUser.classYear = classYear
        
        accountUser.schoolUID = schoolUID
        accountUser.schoolTitle = schoolTitle
        accountUser.schoolLocation = schoolLocation
        
        accountUser.gender = gender
        
        accountUser.createdDate = createdDate
        accountUser.phoneNumber = phoneNumber
        
        accountUser.friendsCount = friendsCount
        accountUser.pokesCount = pokesCount
        
        accountUser.isReceivedPropNotificationsEnabled = isReceivedPropNotificationsEnabled ?? false
        accountUser.isPropRequestNotificationsEnabled = isPropRequestNotificationsEnabled ?? false
        accountUser.isChatMessageNotificationEnabled = isChatMessageNotificationEnabled ?? false
        accountUser.isNewUserNotificationsEnabled = isNewUserNotificationsEnabled ?? false
        accountUser.isNewFriendNotificationsEnabled = isNewFriendNotificationsEnabled ?? false
        
        accountUser.isHideTopPokesEnabled = isHideTopPokesEnabled ?? false
        
        accountUser.isMatchingEnabled = isMatchingEnabled
        
        return true
    }
    
    func encode(accountUser: AccountUser) -> [String: Any] {
        var json: [String: Any] = [:]
        
        json[JSONKeys.uid] = accountUser.uid
        
        if let firstName = accountUser.firstName {
            json[JSONKeys.firstName] = firstName
        }
        
        if let lastName = accountUser.lastName {
            json[JSONKeys.lastName] = lastName
        }
        
        json[JSONKeys.age] = accountUser.age
        json[JSONKeys.bio] = accountUser.bio
        
        json[JSONKeys.classYear] = accountUser.classYear
        json[JSONKeys.schoolUID] = accountUser.schoolUID
        
        json[JSONKeys.gender] = self.genderCoder.value(from: accountUser.gender)
        
        json[JSONKeys.isReceivedPropNotificationsEnabled] = accountUser.isReceivedPropNotificationsEnabled
        json[JSONKeys.isPropRequestNotificationsEnabled] = accountUser.isPropRequestNotificationsEnabled
        json[JSONKeys.isChatMessageNotificationEnabled] = accountUser.isChatMessageNotificationEnabled
        json[JSONKeys.isNewUserNotificationsEnabled] = accountUser.isNewUserNotificationsEnabled
        json[JSONKeys.isNewFriendNotificationsEnabled] = accountUser.isNewFriendNotificationsEnabled
        
        json[JSONKeys.isMatchingEnabled] = accountUser.isMatchingEnabled
        
        return json
    }
}
