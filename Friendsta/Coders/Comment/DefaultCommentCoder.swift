//
//  DefaultCommentCoder.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultCommentCoder: CommentCoder {

    // MARK: - Nested Types

    private enum JSONKeys {

        // MARK: - Type Properties

        static let text = "text"
        static let status = "status"
        static let createdAt = "created_at"
        static let sender = "user"
        static let chat = "chat"
    }

    // MARK: - Instance Properties

    let dateFormatter: DateFormatter
    
    let userCoder: UserCoder
    let chatCoder: ChatCoder

    // MARK: - Instance Methods
    
    func userUID(from json: [String: Any]) -> Int64? {
        guard let userJSON: [String: Any] = JSONKeys.sender <~~ json else {
            return nil
        }
        
        guard let userUID = self.userCoder.userUID(from: userJSON) else {
            return nil
        }
        
        return userUID
    }
    
    func chatUID(from json: [String: Any]) -> Int64? {
        guard let chatJSON: [String: Any] = JSONKeys.chat <~~ json else {
            return nil
        }
        
        guard let chatUID = self.chatCoder.chatUID(from: chatJSON) else {
            return nil
        }
        
        return chatUID
    }

    func decode(comment: Comment, from json: [String: Any]) -> Bool {
        guard let uid = self.uid(from: json) else {
            return false
        }

        guard let text: String = JSONKeys.text <~~ json else {
            return false
        }

        guard let rawStatus: String = JSONKeys.status <~~ json else {
            return false
        }

        guard let status = CommentStatus(rawValue: rawStatus) else {
            return false
        }

        guard let createdDate = Gloss.Decoder.decode(dateForKey: JSONKeys.createdAt, dateFormatter: self.dateFormatter)(json) else {
            return false
        }
        
        guard let senderJSON: [String: Any] = JSONKeys.sender <~~ json else {
            return false
        }
        
        guard let sender = comment.sender else {
            return false
        }
        
        if self.userCoder.decode(user: sender, from: senderJSON) {
            comment.sender = sender
        }
        
        if let chatJSON: [String: Any] = JSONKeys.chat <~~ json {
            if let chat = comment.chat, self.chatCoder.decode(chat: chat, from: chatJSON) {
                comment.chat = chat
            }
        }
        
        comment.uid = uid

        comment.text = text
        comment.status = status
        comment.createdDate = createdDate
        
        return true
    }
}
