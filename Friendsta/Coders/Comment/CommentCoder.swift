//
//  CommentCoder.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 23/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

protocol CommentCoder: Coder {

    // MARK: - Instance Methods

    func userUID(from json: [String: Any]) -> Int64?
    func chatUID(from json: [String: Any]) -> Int64?
    
    func decode(comment: Comment, from json: [String: Any]) -> Bool
}
