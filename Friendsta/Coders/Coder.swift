//
//  Coder.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

private enum JSONKeys {

    // MARK: - Type Properties

    static let uid = "id"
}

// MARK: -

protocol Coder {

    // MARK: - Instance Methods

    func uid(from: [String: Any]) -> Int64?
}

// MARK: -

extension Coder {

    // MARK: - Instance Methods

    func uid(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }
}
