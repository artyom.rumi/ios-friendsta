//
//  DefaultMetaCoder.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 30/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultMetaCoder: MetaCoder {

    // MARK: - Nested Types

    private enum JSONKeys {

        // MARK: - Type Properties

        static let usersCount = "users_count"
    }

    // MARK: - Instance Methods

    func decode(from json: [String: Any]) -> Meta? {
        if let userCount: Int64 = JSONKeys.usersCount <~~ json {
            return Meta(count: userCount)
        } else {
            return nil
        }
    }
}
