//
//  MetaCoder.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 30/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

protocol MetaCoder {

    // MARK: - Instance Methods

    func decode(from json: [String: Any]) -> Meta?
}
