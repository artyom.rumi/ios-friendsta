//
//  DefaultPropCardTypeCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 12/10/2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

struct DefaultPropCardTypeCoder: PropCardTypeCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum Values {
        
        // MARK: - Type Properties
        
        static let regular = "ordinary"
        static let custom = "custom"
    }
    
    // MARK: - Instance Methods
    
    func propCardType(from value: Any) -> PropCardType {
        switch (value as? String)?.lowercased() {
        case .some(Values.regular):
            return .regular
            
        case .some(Values.custom):
            return .custom
            
        default:
            return .unknown
        }
    }
    
    func value(from propCardType: PropCardType) -> Any? {
        switch propCardType {
        case .unknown:
            return nil
            
        case .regular:
            return Values.regular
            
        case .custom:
            return Values.custom
        }
    }
}
