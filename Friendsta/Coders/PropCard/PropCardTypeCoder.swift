//
//  PropCardTypeCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol PropCardTypeCoder {
    
    // MARK: - Instance Methods
    
    func propCardType(from value: Any) -> PropCardType
    func value(from propCardType: PropCardType) -> Any?
}
