//
//  DefaultPropCardCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 12.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultPropCardCoder: PropCardCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        static let type = "type"
        
        static let senderGender = "sender_gender"
        static let createdDate = "created_at"
        static let viewedDate = "viewed_at"
        
        static let content = "prop"
        static let propReaction = "reaction"
        
        static let reactionSender = "recipient"
    }
    
    // MARK: - Instance Properties
    
    let propCardTypeCoder: PropCardTypeCoder
    let propCoder: PropCoder
    let customPropCoder: CustomPropCoder
    let propReactionCoder: PropReactionCoder
    let genderCoder: GenderCoder
    let userCoder: UserCoder
    
    let dateFormatter: DateFormatter
    
    // MARK: - Instance Methods
    
    func propCardUID(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }
    
    func propCardType(from json: [String: Any]) -> PropCardType {
        guard let rawType: String = JSONKeys.type <~~ json else {
            return .unknown
        }
        
        return self.propCardTypeCoder.propCardType(from: rawType)
    }
    
    func customPropUID(from json: [String: Any]) -> Int64? {
        switch self.propCardType(from: json) {
        case .unknown, .regular:
            return nil
        
        case .custom:
            return JSONKeys.uid <~~ json
        }
    }
    
    func propUID(from json: [String: Any]) -> Int64? {
        switch self.propCardType(from: json) {
        case .unknown, .custom:
            return nil
            
        case .regular:
            guard let propJSON: [String: Any] = JSONKeys.content <~~ json else {
                return nil
            }
            
            return self.propCoder.propUID(from: propJSON)
        }
    }
    
    func propReactionUID(from json: [String: Any]) -> Int64? {
        guard let propReactionJSON: [String: Any] = JSONKeys.propReaction <~~ json else {
            return nil
        }
        
        return self.propReactionCoder.propReactionUID(from: propReactionJSON)
    }
    
    func decode(propCard: PropCard, from json: [String: Any]) -> Bool {
        guard let uid: Int64 = JSONKeys.uid <~~ json else {
            return false
        }
        
        guard let rawType: String = JSONKeys.type <~~ json else {
            return false
        }
        
        let type = self.propCardTypeCoder.propCardType(from: rawType)
        
        switch type {
        case .unknown:
            return false
            
        case .regular:
            guard let prop = propCard.prop else {
                return false
            }
            
            guard let contentJSON: [String: Any] = JSONKeys.content <~~ json else {
                return false
            }
        
            guard self.propCoder.decode(prop: prop, from: contentJSON) else {
                return false
            }
        
        case .custom:
            guard let customProp = propCard.customProp else {
                return false
            }
            
            guard let contentJSON: [String: Any] = JSONKeys.content <~~ json else {
                return false
            }
            
            guard self.customPropCoder.decode(customProp: customProp, uid: uid, from: contentJSON) else {
                return false
            }
        }
        
        guard let senderRawGender: String = JSONKeys.senderGender <~~ json else {
            return false
        }
        
        guard let senderGender = self.genderCoder.gender(from: senderRawGender) else {
            return false
        }
        
        if let createdDate: Date = Gloss.Decoder.decode(dateForKey: JSONKeys.createdDate,
                                                        dateFormatter: self.dateFormatter)(json) {
            propCard.createdDate = createdDate
        }
        
        let viewedDate: Date? = Gloss.Decoder.decode(dateForKey: JSONKeys.viewedDate,
                                                     dateFormatter: self.dateFormatter)(json)
        
        if let propReaction = propCard.reaction {
            guard let propReactionJSON: [String: Any] = JSONKeys.propReaction <~~ json else {
                return false
            }
            
            guard self.propReactionCoder.decode(propReaction: propReaction, from: propReactionJSON) else {
                return false
            }
            
            if let reactionSender = propReaction.sender, let reactionSenderJSON: [String: Any] = JSONKeys.reactionSender <~~ json {
                guard self.userCoder.decode(user: reactionSender, from: reactionSenderJSON) else {
                    return false
                }
                                
                propReaction.sender = reactionSender
            } else {
                propReaction.sender = nil
            }
            
            propCard.reaction = propReaction
        } else {
            propCard.reaction = nil
        }
        
        propCard.uid = uid
        propCard.type = type
        
        propCard.senderGender = senderGender
        propCard.viewedDate = viewedDate
        
        return true
    }
}
