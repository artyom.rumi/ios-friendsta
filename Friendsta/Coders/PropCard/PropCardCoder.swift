//
//  PropCardCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 12.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol PropCardCoder {
    
    // MARK: - Instance Methods
    
    func propCardUID(from json: [String: Any]) -> Int64?
    func propCardType(from json: [String: Any]) -> PropCardType
    
    func propUID(from json: [String: Any]) -> Int64?
    func customPropUID(from json: [String: Any]) -> Int64?
    
    func propReactionUID(from json: [String: Any]) -> Int64?
    
    func decode(propCard: PropCard, from json: [String: Any]) -> Bool
}
