//
//  GenderCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol GenderCoder {
    
    // MARK: - Instance Methods
    
    func gender(from value: Any) -> Gender?
    func value(from gender: Gender) -> Any
}
