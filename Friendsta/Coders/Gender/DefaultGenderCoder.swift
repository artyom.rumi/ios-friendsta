//
//  DefaultGenderCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

struct DefaultGenderCoder: GenderCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum Values {
        
        // MARK: - Type Properties
        
        static let nonBinary = "non_binary"
        static let male = "male"
        static let female = "female"
    }
    
    // MARK: - Instance Methods
    
    func gender(from value: Any) -> Gender? {
        switch (value as? String)?.lowercased() {
        case .some(Values.nonBinary):
            return .nonBinary
            
        case .some(Values.male):
            return .male
            
        case .some(Values.female):
            return .female
            
        default:
            return nil
        }
    }
    
    func value(from gender: Gender) -> Any {
        switch gender {
        case .nonBinary:
            return Values.nonBinary
            
        case .male:
            return Values.male
            
        case .female:
            return Values.female
        }
    }
}
