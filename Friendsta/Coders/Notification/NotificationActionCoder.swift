//
//  NotificationActionCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaNetwork

protocol NotificationActionCoder {
    
    // MARK: - Instance Methods
    
    func notificationAction(from value: Any) -> NotificationAction?
}
