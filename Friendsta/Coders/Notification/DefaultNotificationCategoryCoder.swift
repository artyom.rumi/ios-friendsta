//
//  DefaultNotificationCategoryCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 19.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import UserNotifications
import Gloss
import FriendstaNetwork

struct DefaultNotificationCategoryCoder: NotificationCategoryCoder {
    
    // MARK: - Nested Types

    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let propRequestUID = "prop_request_id"
        
        static let chatUID = "chat_id"
        
        static let chatSenderUID = "chat_sender_id"
        static let chatReceiverUID = "chat_receiver_id"
    }
    
    fileprivate enum TypeValues {
        
        // MARK: - Type Properties
        
        static let newUser = "new_user"
        static let newFriend = "new_friend"
        static let propCard = "received_prop"
        static let pokeReply = "poke_reply"
        static let chatMessage = "chat_message"
    }
    
    // MARK: - Instance Methods
    
    func notificationCategory(from notificationContent: UNNotificationContent) -> NotificationCategory? {
        guard let userInfo = notificationContent.userInfo as? [String: Any] else {
            return nil
        }
        
        switch notificationContent.categoryIdentifier {
        case TypeValues.newUser:
            return .newUser
           
        case TypeValues.newFriend:
            return .newFriend
            
        case TypeValues.propCard:
            return .propCard

        case TypeValues.pokeReply:
            return .pokeReply

        case TypeValues.chatMessage:
            guard let chatUID: Int64 = JSONKeys.chatUID <~~ userInfo else {
                return nil
            }

            let chatSenderUID: Int64? = JSONKeys.chatSenderUID <~~ userInfo
            let chatReceiverUID: Int64? = JSONKeys.chatReceiverUID <~~ userInfo

            return .chatMessage(chatUID: chatUID, senderUID: chatSenderUID, receiverUID: chatReceiverUID)
            
        default:
            return nil
        }
    }
}
