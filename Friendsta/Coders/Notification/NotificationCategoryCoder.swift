//
//  NotificationCategoryCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 19.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import UserNotifications
import FriendstaNetwork

protocol NotificationCategoryCoder {
    
    // MARK: - Instance Methods
    
    func notificationCategory(from notificationContent: UNNotificationContent) -> NotificationCategory?
}
