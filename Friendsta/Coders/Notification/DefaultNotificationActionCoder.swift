//
//  DefaultNotificationActionCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import UserNotifications
import FriendstaNetwork

struct DefaultNotificationActionCoder: NotificationActionCoder {
    
    // MARK: - Instance Methods
    
    func notificationAction(from value: Any) -> NotificationAction? {
        switch value as? String {
        case .some(UNNotificationDefaultActionIdentifier):
            return .open
            
        default:
            return nil
        }
    }
}
