//
//  DefaultAccessSessionCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss
import FriendstaNetwork

struct DefaultAccessSessionCoder: AccessSessionCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let phoneNumber = "phone_number"
        static let userUID = "user_id"
        static let attemptMaxCount = "attempts_count"
        static let expiryDate = "expires_at"
    }
    
    // MARK: - Instance Properties
    
    let dateFormatter: DateFormatter
    
    // MARK: - Instance Methods
    
    func accessSession(from json: [String: Any]) -> AccessSession? {
        guard let phoneNumber: String = JSONKeys.phoneNumber <~~ json else {
            return nil
        }
        
        let userUID: Int64? = JSONKeys.userUID <~~ json
        
        let attemptMaxCount: Int? = JSONKeys.attemptMaxCount <~~ json
        
        let expiryDate: Date? = Gloss.Decoder.decode(dateForKey: JSONKeys.expiryDate,
                                                     dateFormatter: self.dateFormatter)(json)
        
        return AccessSession(phoneNumber: phoneNumber,
                             userUID: userUID,
                             attemptMaxCount: attemptMaxCount,
                             expiryDate: expiryDate)
    }
}
