//
//  AccessCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaNetwork

protocol AccessCoder {
    
    // MARK: - Instance Methods
    
    func access(from json: [String: Any]) -> Access?
}
