//
//  DefaultAccessCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss
import FriendstaNetwork

struct DefaultAccessCoder: AccessCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let token = "value"
        static let userUID = "user_id"
        static let expiryDate = "expires_at"
    }
    
    // MARK: - Instance Properties
    
    let dateFormatter: DateFormatter
    
    // MARK: - Instance Methods
    
    func access(from json: [String: Any]) -> Access? {
        guard let token: String = JSONKeys.token <~~ json else {
            return nil
        }
        
        guard let expiryDate: Date = Gloss.Decoder.decode(dateForKey: JSONKeys.expiryDate,
                                                          dateFormatter: self.dateFormatter)(json) else {
            return nil
        }
        
        let userUID: Int64? = JSONKeys.userUID <~~ json
        
        return Access(token: token,
                      expiryDate: expiryDate,
                      userUID: userUID)
    }
}
