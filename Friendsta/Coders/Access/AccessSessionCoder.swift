//
//  AccessSessionCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaNetwork

protocol AccessSessionCoder {
    
    // MARK: - Instance Methods
    
    func accessSession(from json: [String: Any]) -> AccessSession?
}
