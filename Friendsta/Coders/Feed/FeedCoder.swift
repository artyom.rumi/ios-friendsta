//
//  FeedCoder.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

protocol FeedCoder: Coder {

    // MARK: - Instance Methods

    func decode(feed: Feed, from json: [String: Any]) -> Bool 
}
