//
//  DefaultFeedCoder.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultFeedCoder: FeedCoder {

    // MARK: - Nested Types

    private enum JSONKeys {

        // MARK: - Type Properties

        static let image = "image"

        static let userID = "user_id"
        static let sourceID = "source_feed_id"
        static let likesCount = "likes_count"
        static let liked = "liked"
        static let createdAt = "created_at"
        static let unreadCommentCount = "unread_comments_count"
        static let text = "text"
        static let link = "link"
        static let commentCount = "comments_count"
        static let engagedFriendsCount = "count_of_engaged_friends"
        static let reposted = "reposted"
        static let isIncognito = "incognito"
        // added by lava on 0120
        static let rate = "rate"
        static let rateCount = "rate_count"
    }

    // MARK: - Instance Properties

    let photoCoder: PhotoCoder
    let dateFormatter: ISO8601DateFormatter

    // MARK: - Instance Methods

    func decode(feed: Feed, from json: [String: Any]) -> Bool {
        guard let uid = self.uid(from: json) else {
            return false
        }

        guard let userUID: Int64 = JSONKeys.userID <~~ json else {
            return false
        }

        guard let likesCount: Int64 = JSONKeys.likesCount <~~ json else {
            return false
        }

        guard let liked: Bool = JSONKeys.liked <~~ json else {
            return false
        }

        guard let createdDate = Gloss.Decoder.decode(dateForKey: JSONKeys.createdAt, dateFormatter: self.dateFormatter)(json) else {
            return false
        }

        guard let unreadCommentCount: Int64 = JSONKeys.unreadCommentCount <~~ json else {
            return false
        }

        guard let commentCount: Int64 = JSONKeys.commentCount <~~ json else {
            return false
        }
        
        // added by lava on 0120
        guard let rate: Double = JSONKeys.rate <~~ json else {
            return false
        }
                
        guard let rateCount: Int64 = JSONKeys.rateCount <~~ json else {
            return false
        }
        
        if let imageJSON: [String: Any] = JSONKeys.image <~~ json, let photo = self.photoCoder.photo(from: imageJSON) {
            feed.photo = photo
        }
        
        if let text: String = JSONKeys.text <~~ json {
            feed.text = text
        }
        
        if let link: String = JSONKeys.link <~~ json {
            feed.link = link
        }
        
        if let sourceUID: Int64 = JSONKeys.sourceID <~~ json {
            feed.sourceUID = sourceUID
        }
        
        if let isIncognito: Bool = JSONKeys.isIncognito <~~ json {
            feed.isIncognito = isIncognito
        }
    
        feed.uid = uid

        feed.userUID = userUID
        feed.likeCount = likesCount
        feed.isLiked = liked
        feed.createdDate = createdDate
        feed.unreadCommentCount = unreadCommentCount
        feed.commentCount = commentCount

        feed.engagedFriendsCount = JSONKeys.engagedFriendsCount <~~ json ?? 0
        feed.isRepost = JSONKeys.reposted <~~ json ?? false
        
        // added by lava on 0120
        feed.rate = rate
        feed.rateCount = rateCount

        return true
    }
}
