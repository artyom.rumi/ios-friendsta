//
//  DefaultSchoolCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

struct DefaultSchoolCoder: SchoolCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        
        static let title = "name"
        static let location = "address"
        
        static let memberCount = "members_amount"
    }
    
    // MARK: - Instance Methods
    
    func schoolUID(from json: [String: Any]) -> Int64? {
        return JSONKeys.uid <~~ json
    }
    
    func decode(school: School, from json: [String: Any]) -> Bool {
        guard let uid: Int64 = JSONKeys.uid <~~ json else {
            return false
        }
        
        guard let title: String? = JSONKeys.title <~~ json else {
            return false
        }
        
        let location: String? = JSONKeys.location <~~ json
        
        guard let memberCount: Int32 = JSONKeys.memberCount <~~ json else {
            return false
        }
        
        school.uid = uid
        
        school.title = title
        school.location = location
        
        school.memberCount = memberCount
        
        return true
    }
}
