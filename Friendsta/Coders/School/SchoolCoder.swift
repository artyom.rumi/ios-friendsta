//
//  SchoolCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

protocol SchoolCoder {
    
    // MARK: - Instance Methods
    
    func schoolUID(from json: [String: Any]) -> Int64?
    
    func decode(school: School, from json: [String: Any]) -> Bool
}
