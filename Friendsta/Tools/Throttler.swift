//
//  Throttler.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 01/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

class Throttler {

    // MARK: - Instance Properties

    private var workItem = DispatchWorkItem(block: { })
    private var previousRun = Date.distantPast

    private let queue: DispatchQueue
    private let minimumDelay: TimeInterval

    // MARK: - Initializers

    init(minimumDelay: TimeInterval, queue: DispatchQueue = .main) {
        self.minimumDelay = minimumDelay
        self.queue = queue
    }

    // MARK: - Instance Methods

    func throttle(_ block: @escaping () -> Void) {
        self.workItem.cancel()

        self.workItem = DispatchWorkItem(block: { [weak self] in
            self?.previousRun = Date()

            block()
        })

        let delay = previousRun.timeIntervalSinceNow > self.minimumDelay ? 0 : minimumDelay

        self.queue.asyncAfter(deadline: .now() + delay, execute: self.workItem)
    }
}
