//
//  ModelContext.swift
//  Tools
//
//  Created by Almaz Ibragimov on 24.02.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public protocol ModelContext {
    
    // MARK: - Instance Properties
    
    var observers: [ModelContextObserver] { get }
    
    var hasChanges: Bool { get }
    
    // MARK: - Instance Methods
    
    func addObserver(_ observer: ModelContextObserver)
    func removeObserver(_ observer: ModelContextObserver)
    
    func createMainQueueChildContext() -> Self
    func createPrivateQueueChildContext() -> Self
    
    func performAndWait(block: @escaping () -> Void)
    func perform(block: @escaping () -> Void)
    
    func rollback()
    func save()
}
