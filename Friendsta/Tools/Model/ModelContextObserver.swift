//
//  ModelContextObserver.swift
//  Tools
//
//  Created by Almaz Ibragimov on 10.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public protocol ModelContextObserver: class {
    
    // MARK: - Instance Methods
    
    func modelContext(_ modelContext: ModelContext, didRemoveObjects objects: [ModelObject])
    func modelContext(_ modelContext: ModelContext, didAppendObjects objects: [ModelObject])
    func modelContext(_ modelContext: ModelContext, didUpdateObjects objects: [ModelObject])
    func modelContext(_ modelContext: ModelContext, didChangeObjects objects: [ModelObject])
}
