//
//  ModelDatabase.swift
//  Tools
//
//  Created by Almaz Ibragimov on 04.03.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public protocol ModelDatabase: class {
    
    // MARK: - Nested Types
    
    associatedtype Context: ModelContext
    
    // MARK: - Instance Properties
    
    var identifier: String { get }
    var viewContext: Context { get }
}
