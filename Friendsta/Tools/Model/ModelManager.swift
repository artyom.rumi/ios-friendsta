//
//  ModelManager.swift
//  Tools
//
//  Created by Almaz Ibragimov on 24.02.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public protocol ModelManager {
    
    // MARK: - Nested Types
    
    associatedtype Object: ModelObject
    associatedtype Context: ModelContext
    
    // MARK: - Instance Properties
    
    var context: Context { get }
    var entityName: String { get }
    
    // MARK: - Instance Methods
    
    func createDefaultSortDescriptor() -> NSSortDescriptor
    
    func fetch(with fetchRequest: ModelFetchRequest<Object>) -> [Object]
    func count(with fetchRequest: ModelFetchRequest<Object>) -> Int
    func clear(with fetchRequest: ModelFetchRequest<Object>)
    
    func remove(object: Object)
    func append() -> Object
}

// MARK: -

public extension ModelManager {
    
    // MARK: - Instance Properties
    
    var entityName: String {
        return String(describing: Self.Object.self)
    }
    
    // MARK: - Instance Methods
    
    func firstOrNew() -> Object {
        return self.first() ?? self.append()
    }

    func lastOrNew() -> Object {
        return self.last() ?? self.append()
    }

    func first(with fetchRequest: ModelFetchRequest<Object>) -> Object? {
        var fetchRequest = fetchRequest
        
        fetchRequest.fetchLimit = 1
        fetchRequest.fetchBatchSize = 1
        
        return self.fetch(with: fetchRequest).first
    }
    
    func first(with predicate: NSPredicate?) -> Object? {
        var fetchRequest = ModelFetchRequest<Object>(predicate: predicate, sortDescriptors: [self.createDefaultSortDescriptor()])
        
        fetchRequest.fetchLimit = 1
        fetchRequest.fetchBatchSize = 1
        
        return self.fetch(with: fetchRequest).first
    }

    func first() -> Object? {
        return self.first(with: nil)
    }

    func last(with fetchRequest: ModelFetchRequest<Object>) -> Object? {
        let sortDescriptors = fetchRequest.sortDescriptors.map({ sortDescriptor in
            return sortDescriptor.reversedSortDescriptor as! NSSortDescriptor
        })
        
        var fetchRequest = fetchRequest
        
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.fetchLimit = 1
        fetchRequest.fetchBatchSize = 1
        
        return self.fetch(with: fetchRequest).first
    }
    
    func last(with predicate: NSPredicate?) -> Object? {
        let sortDescriptor = self.createDefaultSortDescriptor().reversedSortDescriptor as! NSSortDescriptor
        
        var fetchRequest = ModelFetchRequest<Object>(predicate: predicate, sortDescriptors: [sortDescriptor])
        
        fetchRequest.fetchLimit = 1
        fetchRequest.fetchBatchSize = 1
        
        return self.fetch(with: fetchRequest).first
    }

    func last() -> Object? {
        return self.last(with: nil)
    }
    
    func fetch(with predicate: NSPredicate?) -> [Object] {
        return self.fetch(with: ModelFetchRequest(predicate: predicate, sortDescriptors: [self.createDefaultSortDescriptor()]))
    }
    
    func fetch() -> [Object] {
        return self.fetch(with: nil)
    }

    func count(with predicate: NSPredicate?) -> Int {
        return self.count(with: ModelFetchRequest(predicate: predicate, sortDescriptors: [self.createDefaultSortDescriptor()]))
    }
    
    func count() -> Int {
        return self.count(with: nil)
    }

    func clear(with predicate: NSPredicate?) {
        self.clear(with: ModelFetchRequest(predicate: predicate, sortDescriptors: [self.createDefaultSortDescriptor()]))
    }
    
    func clear() {
        self.clear(with: nil)
    }
}
