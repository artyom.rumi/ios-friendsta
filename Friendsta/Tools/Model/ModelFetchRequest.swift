//
//  ModelFetchRequest.swift
//  Tools
//
//  Created by Almaz Ibragimov on 24.02.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public struct ModelFetchRequest<Object: ModelObject> {
    
    // MARK: - Instance Properties
    
    public var predicate: NSPredicate?
    public var sortDescriptors: [NSSortDescriptor]
    
    // MARK: -
    
    public var returnsObjectsAsFaults: Bool = true
    
    public var includesSubentities: Bool = true
    public var includesPendingChanges: Bool = true
    public var includesPropertyValues: Bool = true
    
    public var shouldRefreshRefetchedObjects: Bool = false
    
    public var fetchOffset: Int = 0
    public var fetchLimit: Int = 0
    public var fetchBatchSize: Int = 0
    
    public var relationshipsForPrefetching: [String]?
    
    // MARK: - Initializers
    
    public init(predicate: NSPredicate?, sortDescriptors: [NSSortDescriptor]) {
        self.predicate = predicate
        self.sortDescriptors = sortDescriptors
    }
}
