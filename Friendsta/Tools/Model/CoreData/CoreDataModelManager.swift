//
//  CoreDataModelManager.swift
//  Tools
//
//  Created by Almaz Ibragimov on 25.02.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObject: ModelObject { }

// MARK: -

fileprivate extension ModelFetchRequest where Object: NSManagedObject {
    
    // MARK: - Instance Methods
    
    func fetchRequestForEntity(_ entityName: String) -> NSFetchRequest<Object> {
        let fetchRequest = NSFetchRequest<Object>(entityName: entityName)
        
        fetchRequest.predicate = self.predicate
        fetchRequest.sortDescriptors = self.sortDescriptors
        
        fetchRequest.returnsObjectsAsFaults = self.returnsObjectsAsFaults
        
        fetchRequest.includesSubentities = self.includesSubentities
        fetchRequest.includesPendingChanges = self.includesPendingChanges
        fetchRequest.includesPropertyValues = self.includesPropertyValues
        
        fetchRequest.shouldRefreshRefetchedObjects = self.shouldRefreshRefetchedObjects
        
        fetchRequest.fetchOffset = self.fetchOffset
        fetchRequest.fetchLimit = self.fetchLimit
        fetchRequest.fetchBatchSize = self.fetchBatchSize
        
        fetchRequest.relationshipKeyPathsForPrefetching = self.relationshipsForPrefetching
        
        return fetchRequest
    }
}

// MARK: -

public protocol CoreDataModelManager: ModelManager where Object: NSManagedObject, Context == CoreDataModelContext { }

// MARK: -

public extension CoreDataModelManager {
    
    // MARK: - Instance Methods
    
    func fetch(with fetchRequest: ModelFetchRequest<Object>) -> [Object] {
        do {
            return try self.context.managedObjectContext.fetch(fetchRequest.fetchRequestForEntity(self.entityName))
        } catch {
            fatalError("Can not fetch \(String(describing: Object.self)) objects in context")
        }
    }
    
    public func count(with fetchRequest: ModelFetchRequest<Object>) -> Int {
        do {
            return try self.context.managedObjectContext.count(for: fetchRequest.fetchRequestForEntity(self.entityName))
        } catch {
            fatalError("Can not count \(String(describing: Object.self)) object in context")
        }
    }
    
    func clear(with fetchRequest: ModelFetchRequest<Object>) {
        let objects: [Object]
        
        do {
            objects = try self.context.managedObjectContext.fetch(fetchRequest.fetchRequestForEntity(self.entityName))
        } catch {
            fatalError("Can not fetch \(String(describing: Object.self)) objects in context")
        }
        
        for object in objects {
            self.context.managedObjectContext.delete(object)
        }
    }
    
    func remove(object: Object) {
        if !object.isDeleted {
            self.context.managedObjectContext.delete(object)
        }
    }
    
    func append() -> Object {
        guard let entity = NSEntityDescription.entity(forEntityName: self.entityName,
                                                      in: self.context.managedObjectContext) else {
            fatalError("Can not append \(String(describing: Object.self)) object into context")
        }
        
        return Object(entity: entity, insertInto: self.context.managedObjectContext)
    }
}
