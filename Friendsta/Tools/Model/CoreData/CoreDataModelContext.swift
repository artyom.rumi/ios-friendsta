//
//  CoreDataModelContext.swift
//  Tools
//
//  Created by Almaz Ibragimov on 25.02.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation
import CoreData

public class CoreDataModelContext: ModelContext {
    
    // MARK: - Instance Properties

    public final let managedObjectContext: NSManagedObjectContext
    
    // MARK: - ModelContext
    
    public fileprivate(set) final var observers: [ModelContextObserver] = []
    
    public final var hasChanges: Bool {
        return self.managedObjectContext.hasChanges
    }
    
    // MARK: - Initializers

    public required init(managedObjectContext: NSManagedObjectContext) {
        self.managedObjectContext = managedObjectContext
    }

    // MARK: - Instance Methods

    @objc
    fileprivate func onManagedObjectContextObjectsDidChange(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else {
            return
        }
        
        var changedObjects: [ModelObject] = []
        
        if let objects = userInfo[NSDeletedObjectsKey] as? Set<NSManagedObject>, !objects.isEmpty {
            let removedObjects = Array(objects)
            
            for observer in self.observers {
                observer.modelContext(self, didRemoveObjects: removedObjects)
            }
            
            changedObjects.append(contentsOf: removedObjects)
        }
        
        if let objects = userInfo[NSInsertedObjectsKey] as? Set<NSManagedObject>, !objects.isEmpty {
            let appendedObjects = Array(objects)
            
            for observer in self.observers {
                observer.modelContext(self, didAppendObjects: appendedObjects)
            }
            
            changedObjects.append(contentsOf: appendedObjects)
        }
        
        if let objects = userInfo[NSUpdatedObjectsKey] as? Set<NSManagedObject>, !objects.isEmpty {
            let updatedObjects = Array(objects)
            
            for observer in self.observers {
                observer.modelContext(self, didUpdateObjects: updatedObjects)
            }
            
            changedObjects.append(contentsOf: updatedObjects)
        }
        
        if !changedObjects.isEmpty {
            for observer in self.observers {
                observer.modelContext(self, didChangeObjects: changedObjects)
            }
        }
    }
    
    fileprivate func createContext<Context: CoreDataModelContext>(concurrencyType: NSManagedObjectContextConcurrencyType) -> Context {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: concurrencyType)
        
        managedObjectContext.parent = self.managedObjectContext
        managedObjectContext.mergePolicy = NSMergePolicy(merge: .errorMergePolicyType)
        
        return Context(managedObjectContext: managedObjectContext)
    }
    
    // MARK: - ModelContext
    
    public final func addObserver(_ observer: ModelContextObserver) {
        if self.observers.isEmpty {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(self.onManagedObjectContextObjectsDidChange(_ :)),
                                                   name: .NSManagedObjectContextObjectsDidChange,
                                                   object: self.managedObjectContext)
        }
        
        if !self.observers.contains(where: { $0 === observer }) {
            self.observers.append(observer)
        }
    }
    
    public final func removeObserver(_ observer: ModelContextObserver) {
        if let index = self.observers.index(where: { $0 === observer }) {
            self.observers.remove(at: index)
        }
        
        if self.observers.isEmpty {
            NotificationCenter.default.removeObserver(self,
                                                      name: .NSManagedObjectContextObjectsDidChange,
                                                      object: self.managedObjectContext)
        }
    }
    
    public final func createMainQueueChildContext() -> Self {
        return self.createContext(concurrencyType: .mainQueueConcurrencyType)
    }
    
    public final func createPrivateQueueChildContext() -> Self {
        return self.createContext(concurrencyType: .privateQueueConcurrencyType)
    }
    
    public final func performAndWait(block: @escaping () -> Void) {
        self.managedObjectContext.performAndWait(block)
    }

    public final func perform(block: @escaping () -> Void) {
        self.managedObjectContext.perform(block)
    }

    public final func rollback() {
        self.managedObjectContext.rollback()
    }
    
    public final func save() {
        if self.managedObjectContext.hasChanges {
            do {
                try self.managedObjectContext.save()
            } catch {
                fatalError("Can not save context")
            }
        }
    }
}
