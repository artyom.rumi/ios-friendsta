//
//  ClosePreviousModalControllerSegue.swift
//  Friendsta
//
//  Created by Elina Batyrova on 03.01.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

public class CloseModalControllerSegue: UIStoryboardSegue {
    
    // MARK: - Instance Methods
    
    fileprivate func perform(animated: Bool) {
        self.source.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UIStoryboardSegue
    
    public override func perform() {
        self.perform(animated: false)
    }
}

// MARK: -

public class CloseModalControllerAnimatedSegue: CloseModalControllerSegue {
    
    // MARK: - Instance Methods
    
    public override func perform() {
        self.perform(animated: true)
    }
}
