//
//  ReplaceTwoControllersSegue.swift
//  Friendsta
//
//  Created by Elina Batyrova on 24.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

public class ReplaceTwoControllersSegue: UIStoryboardSegue {
    
    // MARK: - Nested Types
    
    private enum Constants {
        
        // MARK: - Type Properties
        
        static let viewControllersCount = 2
    }
    
    // MARK: - Instance Methods
    
    fileprivate func perform(animated: Bool) {
        guard let navigationController = self.source.navigationController else {
            return super.perform()
        }
        
        var viewControllers = navigationController.viewControllers
        
        viewControllers.removeLast(Constants.viewControllersCount)
        viewControllers.append(self.destination)
        
        navigationController.setViewControllers(viewControllers, animated: animated)
    }
    
    // MARK: - UIStoryboardSegue
    
    public override func perform() {
        self.perform(animated: false)
    }
}

// MARK: -

public class ReplaceTwoControllersAnimatedSegue: ReplaceTwoControllersSegue {
    
    // MARK: - Instance Methods
    
    public override func perform() {
        self.perform(animated: true)
    }
}
