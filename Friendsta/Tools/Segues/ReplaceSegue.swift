//
//  ReplaceSegue.swift
//  Tools
//
//  Created by Almaz Ibragimov on 21.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import UIKit

public class ReplaceSegue: UIStoryboardSegue {
    
    // MARK: - Instance Methods
    
    fileprivate func perform(animated: Bool) {
        guard let navigationController = self.source.navigationController else {
            return super.perform()
        }
        
        var viewControllers = navigationController.viewControllers
        
        guard let viewControllerIndex = viewControllers.index(of: self.source) else {
            return super.perform()
        }
        
        viewControllers.removeLast(viewControllers.count - viewControllerIndex)
        viewControllers.append(self.destination)
        
        navigationController.setViewControllers(viewControllers, animated: animated)
    }
    
    // MARK: - UIStoryboardSegue
    
    public override func perform() {
        self.perform(animated: false)
    }
}

// MARK: -

public class ReplaceAnimatedSegue: ReplaceSegue {
    
    // MARK: - Instance Methods
    
    public override func perform() {
        self.perform(animated: true)
    }
}
