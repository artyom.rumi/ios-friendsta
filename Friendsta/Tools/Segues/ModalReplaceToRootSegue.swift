//
//  ReplaceToRootSegue.swift
//  Friendsta
//
//  Created by Nikita Asabin on 4/2/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

public class ModalReplaceToRootSegue: UIStoryboardSegue {
    
     // MARK: - Instance Methods
    
    fileprivate func perform(animated: Bool) {
        guard let navigationController = self.source.navigationController else {
            return super.perform()
        }
        
        var viewControllers = navigationController.viewControllers
        
        guard let rootViewController = viewControllers.first else {
            return super.perform()
        }
        
        viewControllers.removeAll()
        viewControllers.append(rootViewController)
        
        rootViewController.present(self.destination, animated: animated) {
            navigationController.setViewControllers(viewControllers, animated: animated)
        }
    }
    
     // MARK: - UIStoryboardSegue
    
    public override func perform() {
        self.perform(animated: true)
    }
}
