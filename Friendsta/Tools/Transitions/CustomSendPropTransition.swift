//
//  CustomSendPropTransition.swift
//  Friendsta
//
//  Created by Elina Batyrova on 07.01.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class CustomSendPropTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let padding: CGFloat = 16.0
    }
    
    // MARK: - Instance Properties
    
    fileprivate var animatedView: UIView
    fileprivate var isNeedScale: Bool
    
    fileprivate var initialView: UIView?
    fileprivate var initialFrame: CGRect?
    
    fileprivate var isPresenting = false

    // MARK: - Initializers
    
    init(animatedView: UIView, isNeedScale: Bool = false) {
        self.animatedView = animatedView
        self.isNeedScale = isNeedScale
    }
    
    // MARK: - Instance Methods
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.25
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        
        if self.isPresenting {
            guard let destinationController = transitionContext.viewController(forKey: .to) as? ActionSheetViewController else {
                return
            }
            
            guard let destinationView = transitionContext.view(forKey: .to) else {
                return
            }
            
            let copyAnimatedView = self.animatedView.copy() as! UIView
            
            self.initialView = copyAnimatedView
            
            let initialFrame = animatedView.convert(copyAnimatedView.bounds, to: destinationView)
            
            let recountedInitialFrame = CGRect(x: copyAnimatedView.frame.origin.x,
                                               y: initialFrame.origin.y,
                                               width: initialFrame.width,
                                               height: initialFrame.height)

            self.initialFrame = recountedInitialFrame
            
            let bottomView = destinationController.bottomView
            
            containerView.addSubview(destinationView)
            containerView.addSubview(copyAnimatedView)
            
            copyAnimatedView.frame = recountedInitialFrame
            
            self.animatedView.isHidden = true
            
            destinationView.setNeedsLayout()
            destinationView.layoutIfNeeded()
            
            UIView.animate(withDuration: 0.25, animations: {
                let finalSize: CGSize!
                
                if self.isNeedScale {
                    finalSize = CGSize(width: copyAnimatedView.bounds.width * 1.2,
                                       height: copyAnimatedView.bounds.height * 1.2)
                } else {
                    finalSize = CGSize(width: copyAnimatedView.bounds.width,
                                       height: copyAnimatedView.bounds.height)
                }
            
                let finalFrame = CGRect(x: bottomView.frame.left,
                                        y: bottomView.frame.top - finalSize.height - Constants.padding,
                                        size: finalSize)
                
                copyAnimatedView.frame = finalFrame
                
                destinationView.backgroundColor = destinationView.backgroundColor?.withAlphaComponent(0.5)
            }, completion: { completed in
                transitionContext.completeTransition(completed)
            })
        } else {
            guard let initialFrame = self.initialFrame else {
                return
            }
            
            guard let initialView = self.initialView else {
                return
            }
            
            guard let sourceView = transitionContext.view(forKey: .from) else {
                return
            }
            
            guard let sourceController = transitionContext.viewController(forKey: .from) as? ActionSheetViewController else {
                return
            }
            
            UIView.animate(withDuration: 0.25, animations: {
                sourceController.bottomView.isHidden = true
                initialView.frame = initialFrame
                sourceView.backgroundColor = sourceView.backgroundColor?.withAlphaComponent(0.0)
            }, completion: { completed in
                guard completed else {
                    return
                }
                
                self.animatedView.isHidden = false
                
                initialView.removeFromSuperview()
                
                transitionContext.completeTransition(completed)
            })
        }
    }
}

// MARK: - UIViewControllerTransitioningDelegate

extension CustomSendPropTransition: UIViewControllerTransitioningDelegate {
    
    // MARK: - Instance Methods
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = true
        
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = false
        
        return self
    }
}
