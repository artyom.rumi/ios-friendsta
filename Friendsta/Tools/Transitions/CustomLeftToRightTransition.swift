//
//  CustomLeftToRightTransition.swift
//  Friendsta
//
//  Created by Elina Batyrova on 09.04.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class CustomLeftToRightTransition: NSObject, UIViewControllerAnimatedTransitioning {
    
    // MARK: - Instance Properties
    
    fileprivate var isPresenting = false
    
    // MARK: - Instance Methods
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.25
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        
        if  self.isPresenting {
            guard let destinationController = transitionContext.viewController(forKey: .to) as? TextReplyViewController else {
                return
            }
            
            guard let destinationView = transitionContext.view(forKey: .to) else {
                return
            }
            
            let leftView = destinationController.leftView
            let backgroundView = destinationController.backgroundView
            
            containerView.addSubview(destinationView)
            
            leftView.transform = CGAffineTransform(translationX: -destinationView.frame.size.width,
                                                   y: 0.0)
            backgroundView.transform = CGAffineTransform(translationX: -destinationView.frame.size.width,
                                                         y: 0.0)
            
            UIView.animate(withDuration: 0.25,
                           delay: 0.0,
                           options: .curveEaseInOut,
                           animations: {
                leftView.transform = CGAffineTransform(translationX: 0.0,
                                                       y: 0.0)
                            
                backgroundView.transform = CGAffineTransform(translationX: 0.0,
                                                             y: 0.0)
                backgroundView.alpha = 1.0
                            
                destinationView.backgroundColor = destinationView.backgroundColor?.withAlphaComponent(0.5)
            }, completion: { (completed) in
                guard completed else {
                    return
                }
                
                transitionContext.completeTransition(completed)
            })
        } else {
            guard let sourceController = transitionContext.viewController(forKey: .from) as? TextReplyViewController else {
                return
            }
            
            guard let sourceView = transitionContext.view(forKey: .from) else {
                return
            }
            
            let leftView = sourceController.leftView
            let backgroundView = sourceController.backgroundView
            
            UIView.animate(withDuration: 0.25,
                           delay: 0.0,
                           options: .curveEaseInOut,
                           animations: {
                leftView.transform = CGAffineTransform(translationX: -sourceView.frame.size.width,
                                                       y: 0.0)
                            
                backgroundView.transform = CGAffineTransform(translationX: -sourceView.frame.size.width,
                                                             y: 0.0)
                backgroundView.alpha = 0.5
                            
                sourceView.backgroundColor = sourceView.backgroundColor?.withAlphaComponent(0.0)                
            }) { (completed) in
                guard completed else {
                    return
                }
                
                leftView.removeFromSuperview()
                
                transitionContext.completeTransition(completed)
            }
        }
    }
}

// MARK: - UIViewControllerTransitioningDelegate

extension CustomLeftToRightTransition: UIViewControllerTransitioningDelegate {
    
    // MARK: - Instance Methods
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = true
        
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.isPresenting = false
        
        return self
    }
}
