//
//  PresentAnimation.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 27/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class PresentAnimation: NSObject {

    // MARK: - Nested Types

    private enum Constants {

        // MARK: - Type Properties

        static let duration: TimeInterval = 0.25
    }

    // MARK: - Instance Methods

    private func animator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
        guard let toView = transitionContext.view(forKey: .to) else {
            fatalError()
        }

        guard let toViewController = transitionContext.viewController(forKey: .to) else {
            fatalError()
        }

        let finalFrame = transitionContext.finalFrame(for: toViewController)

        toView.frame = finalFrame.offsetBy(dx: 0, dy: finalFrame.height)

        let animator = UIViewPropertyAnimator(duration: Constants.duration, curve: .easeOut, animations: {
            toView.frame = finalFrame
        })

        animator.addCompletion { position in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }

        return animator
    }
}

// MARK: - UIViewControllerAnimatedTransitioning

extension PresentAnimation: UIViewControllerAnimatedTransitioning {

    // MARK: - Instance Methods

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return Constants.duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let animator = self.animator(using: transitionContext)

        animator.startAnimation()
    }

    func interruptibleAnimator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
        return self.animator(using: transitionContext)
    }
}
