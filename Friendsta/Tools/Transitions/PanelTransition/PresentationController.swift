//
//  PresentationController.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 27/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class PresentationController: UIPresentationController {

    // MARK: - Instance Properties

    private let topPadding: CGFloat
    private let cornerRadius: CGFloat
    private let options: Set<PanelTransition.Options>
    private let driver: TransitionDriver

    // MARK: -

    private lazy var dimmingView: UIView = {
        let view = UIView()

        view.backgroundColor = Colors.black.withAlphaComponent(0.66)
        view.alpha = 0

        return view
    }()

    private lazy var closeButton: UIButton = {
        let button = UIButton()

        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(Images.whiteCloseButton, for: .normal)
        button.tintColor = Colors.white
        button.addTarget(self, action: #selector(self.onCloseButtonTouchUpInside(_:)), for: .touchUpInside)
        button.alpha = 0

        return button
    }()

    // MARK: - UIPresentationController

    override var frameOfPresentedViewInContainerView: CGRect {
        guard let containerView = self.containerView else {
            return .zero
        }

        let bounds = containerView.bounds

        return CGRect(x: 0,
                      y: self.topPadding,
                      width: bounds.width,
                      height: bounds.height - topPadding)
    }

    // MARK: - Initializers

    init(presentedViewController: UIViewController,
         presentingViewController: UIViewController?,
         topPadding: CGFloat,
         cornerRadius: CGFloat,
         options: Set<PanelTransition.Options>,
         driver: TransitionDriver) {
        self.topPadding = topPadding
        self.cornerRadius = cornerRadius
        self.options = options
        self.driver = driver

        super.init(presentedViewController: presentedViewController, presenting: presentedViewController)
    }

    // MARK: - Instance Methods

    @objc private func onCloseButtonTouchUpInside(_ sender: UIButton) {
        Log.high("onCloseButtonTouchUpInside()", from: self)

        self.presentingViewController.dismiss(animated: true)
    }

    // MARK: -

    private func animateAlongsideTransitionIfPossible(_ animationBlock: @escaping () -> Void) {
        guard let coordinator = self.presentedViewController.transitionCoordinator else {
            return animationBlock()
        }

        coordinator.animate(alongsideTransition: { transitionCoordinatorContext in
            animationBlock()
        })
    }

    // MARK: -

    private func presentAnimation() {
        self.dimmingView.alpha = 1
        self.closeButton.alpha = 1
    }

    private func dismissAnimation() {
        self.dimmingView.alpha = 0
        self.closeButton.alpha = 0
    }

    private func removeSubviews() {
        self.dimmingView.removeFromSuperview()
        self.closeButton.removeFromSuperview()
    }

    // MARK: -

    private func configurePresentedView() {
        let presentedView = self.presentedViewController.view

        presentedView?.layer.masksToBounds = true
        presentedView?.layer.cornerRadius = self.cornerRadius
    }

    private func configureCloseButtonConstraints() {
        guard let containerView = self.containerView else {
            return
        }

        NSLayoutConstraint.activate([
            self.closeButton.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 20),
            self.closeButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -20),
            self.closeButton.widthAnchor.constraint(equalToConstant: 23),
            self.closeButton.heightAnchor.constraint(equalToConstant: 23)
        ])
    }

    // MARK: - UIPresentationController

    override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()

        guard let presentedView = self.presentedView else {
            return
        }

        self.configurePresentedView()

        self.containerView?.addSubview(presentedView)

        if self.options.contains(.translucent) {
            self.containerView?.insertSubview(self.dimmingView, at: 0)
        }

        if options.contains(.closeButton) {
            self.containerView?.addSubview(self.closeButton)
            self.configureCloseButtonConstraints()
        }

        self.animateAlongsideTransitionIfPossible { [unowned self] in
            self.presentAnimation()
        }
    }

    override func containerViewDidLayoutSubviews() {
        super.containerViewDidLayoutSubviews()

        self.presentedView?.frame = self.frameOfPresentedViewInContainerView

        guard let containerView = self.containerView else {
            return
        }

        self.dimmingView.frame = containerView.frame
    }

    override func presentationTransitionDidEnd(_ completed: Bool) {
        super.presentationTransitionDidEnd(completed)

        if completed {
            self.driver.direction = .dismiss
        } else {
            self.removeSubviews()
        }
    }

    override func dismissalTransitionWillBegin() {
        super.dismissalTransitionWillBegin()

        self.animateAlongsideTransitionIfPossible { [unowned self] in
            self.dismissAnimation()
        }
    }

    override func dismissalTransitionDidEnd(_ completed: Bool) {
        super.dismissalTransitionDidEnd(completed)

        if completed {
            self.removeSubviews()
        }
    }
}
