//
//  DismissAnimation.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 27/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class DismissAnimation: NSObject {

    // MARK: - Nested Types

    private enum Constants {

        // MARK: - Type Properties

        static let duration: TimeInterval = 0.25
    }

    // MARK: - Instance Methods

    private func animator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
        guard let fromView = transitionContext.view(forKey: .from) else {
            fatalError()
        }

        guard let fromViewController = transitionContext.viewController(forKey: .from) else {
            fatalError()
        }

        let initialFrame = transitionContext.initialFrame(for: fromViewController)

        let animator = UIViewPropertyAnimator(duration: Constants.duration, curve: .easeOut, animations: {
            fromView.frame = initialFrame.offsetBy(dx: 0, dy: initialFrame.height)
        })

        animator.addCompletion { position in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }

        return animator
    }
}

// MARK: - UIViewControllerAnimatedTransitioning

extension DismissAnimation: UIViewControllerAnimatedTransitioning {

    // MARK: - Instance Methods

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return Constants.duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let animator = self.animator(using: transitionContext)

        animator.startAnimation()
    }

    func interruptibleAnimator(using transitionContext: UIViewControllerContextTransitioning) -> UIViewImplicitlyAnimating {
        return self.animator(using: transitionContext)
    }
}
