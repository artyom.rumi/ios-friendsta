//
//  TransitionDirection.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 05/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

enum TransitionDirection {

    // MARK: - Enumeration Cases

    case present
    case dismiss
}
