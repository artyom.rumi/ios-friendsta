//
//  PanelTransition.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 27/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class PanelTransition: NSObject, UIViewControllerTransitioningDelegate {

    // MARK: - Nested Types

    enum Options: Hashable {

        // MARK: - Enumeration Cases

        case closeButton
        case translucent
        case interactive
    }

    // MARK: - Type Properties

    static let userRouting = PanelTransition(topPadding: 20, options: [.interactive])

    // MARK: - Instance Properties

    private let topPadding: CGFloat
    private let cornerRadius: CGFloat
    private let options: Set<Options>

    private let driver = TransitionDriver()

    // MARK: - Initializers

    init(topPadding: CGFloat, cornerRadius: CGFloat = 0, options: Set<Options> = []) {
        self.topPadding = topPadding
        self.cornerRadius = cornerRadius
        self.options = options
    }

    // MARK: - UIViewControllerTransitioningDelegate

    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        if self.options.contains(.interactive) {
            self.driver.link(to: presented)
        }

        return PresentationController(presentedViewController: presented,
                                      presentingViewController: presenting ?? source,
                                      topPadding: self.topPadding,
                                      cornerRadius: self.cornerRadius,
                                      options: self.options,
                                      driver: self.driver)
    }

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PresentAnimation()
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissAnimation()
    }

    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if self.options.contains(.interactive) {
            return self.driver
        } else {
            return nil
        }
    }

    func interactionControllerForPresentation(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if self.options.contains(.interactive) {
            return self.driver
        } else {
            return nil
        }
    }
}
