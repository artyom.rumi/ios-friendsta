//
//  TransitionDriver.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 05/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import FriendstaTools

class TransitionDriver: UIPercentDrivenInteractiveTransition {

    // MARK: - Instance Properties

    private weak var presentedViewController: UIViewController?

    private var panGeatureRecognizer: UIPanGestureRecognizer?

    // MARK: -

    private var maxTranslation: CGFloat {
        return self.presentedViewController?.view.frame.height ?? 0
    }

    private var isRunning: Bool {
        return self.percentComplete != 0
    }

    // MARK: -

    var direction: TransitionDirection = .present

    // MARK: - UIPercentDrivenInteractiveTransition

    override var wantsInteractiveStart: Bool {
        get {
            switch direction {
            case .present:
                return false

            case .dismiss:
                let isGestureActive = (self.panGeatureRecognizer?.state == .some(.began))

                return isGestureActive
            }
        }

        set { }
    }

    // MARK: - Instance Methods

    @objc private func onPanGeatureRecognized(_ sender: UIPanGestureRecognizer) {
        Log.high("onPanGeatureRecognized()", from: self)

        switch self.direction {
        case .present:
            self.handlePresentation(recognizer: sender)

        case .dismiss:
            self.handleDismiss(recognizer: sender)
        }
    }

    // MARK: -

    private func handleDismiss(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            self.pause()

            if !self.isRunning {
                self.presentedViewController?.dismiss(animated: true)
            }

        case .changed:
            self.update(self.percentComplete + recognizer.incrementToBottom(maxTranslation: self.maxTranslation))

        case .ended, .cancelled:
            if recognizer.isProjectedToDownHalf(maxTranslation: self.maxTranslation) {
                self.finish()
            } else {
                self.cancel()
            }

        default:
            break
        }
    }

    private func handlePresentation(recognizer: UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            self.pause()

        case .changed:
            let increment = -recognizer.incrementToBottom(maxTranslation: self.maxTranslation)

            self.update(self.percentComplete + increment)

        case .ended, .cancelled:
            if recognizer.isProjectedToDownHalf(maxTranslation: self.maxTranslation) {
                self.cancel()
            } else {
                self.finish()
            }

        case .failed:
            self.cancel()

        default:
            break
        }
    }

    // MARK: -

    func link(to viewController: UIViewController) {
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.onPanGeatureRecognized(_:)))

        viewController.view.addGestureRecognizer(panGestureRecognizer)

        self.presentedViewController = viewController
        self.panGeatureRecognizer = panGestureRecognizer
    }
}
