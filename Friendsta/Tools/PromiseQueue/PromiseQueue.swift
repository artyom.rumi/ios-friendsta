//
//  PromiseQueue.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 26/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

class PromiseQueue {

    // MARK: - Instance Properties

    private let operationQueue: OperationQueue

    // MARK: - Initializers

    init(name: String? = nil, maxConcurrentOperationCount: Int = 1) {
        let operationQueue = OperationQueue()

        operationQueue.name = name
        operationQueue.maxConcurrentOperationCount = maxConcurrentOperationCount

        self.operationQueue = operationQueue
    }

    // MARK: - Instance Methods

    func add<T>(_ makePromise: @escaping () -> Promise<T>) -> Promise<T> {
        return Promise(resolver: { seal in
            self.operationQueue.addOperation {
                PromiseOperation(makePromise: makePromise).runAndWait().pipe(to: seal.resolve)
            }
        })
    }
}
