//
//  PromiseOperation.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 26/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit
import FriendstaNetwork

class PromiseOperation<T> {

    // MARK: - Instance Properties

    private let makePromise: () -> Promise<T>

    // MARK: - Initializers

    init(makePromise: @escaping () -> Promise<T>) {
        self.makePromise = makePromise
    }

    // MARK: - Instance Methods

    func run() -> Promise<T> {
        return self.makePromise()
    }

    func runAndWait() -> Promise<T> {
        let semaphore = DispatchSemaphore(value: 0)

        var promise: Promise<T>!

        DispatchQueue.main.async(execute: {
            promise = self.makePromise().ensure {
                semaphore.signal()
            }
        })

        let timeoutResult = semaphore.wait(timeout: .distantFuture)

        switch timeoutResult {
        case .success:
            return promise

        case .timedOut:
            return Promise(error: WebError.timeOut)
        }
    }
}
