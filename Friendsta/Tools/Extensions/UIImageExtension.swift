//
//  UIImageExtension.swift
//  Tools
//
//  Created by Almaz Ibragimov on 01.01.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import UIKit

public extension UIImage {
    
    // MARK: - Instanse Properties
    
    var roundedImage: UIImage? {
        let imageRect = CGRect(origin:CGPoint(x: 0, y: 0), size: self.size)
        
        UIGraphicsBeginImageContextWithOptions(self.size, false, 1)
        
        UIBezierPath(roundedRect: imageRect,
                     cornerRadius: min(self.size.width, self.size.height) * 0.5).addClip()
        
        self.draw(in: imageRect)
        
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    // MARK: - Instance Methods
    
    final func scaled(to size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        
        self.draw(in: CGRect(x: 0.0,
                             y: 0.0,
                             size: size))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    final func scaled(to width: CGFloat) -> UIImage? {
        let scaleFactor = width / self.size.width
        
        let scaledSize: CGSize = CGSize(width: self.size.width * scaleFactor, height: self.size.height * scaleFactor)
        
        UIGraphicsBeginImageContext(scaledSize)
        
        self.draw(in: CGRect(x: 0.0,
                             y: 0.0,
                             size: scaledSize))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    func roundedImageWithBorder(width: CGFloat, color: UIColor) -> UIImage? {
        let square = CGSize(width: min(size.width, size.height) + width * 2,
                            height: min(size.width, size.height) + width * 2)
        
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        
        imageView.contentMode = .center
        imageView.image = self
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.masksToBounds = true
        imageView.layer.borderWidth = width
        imageView.layer.borderColor = color.cgColor
        
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        imageView.layer.render(in: context)
        
        var result = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        result = result?.withRenderingMode(.alwaysOriginal)
        
        return result
    }
}
