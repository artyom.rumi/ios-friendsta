//
//  ISO8601DateFormatterExtension.swift
//  Friendsta
//
//  Created by Elina Batyrova on 11/10/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension ISO8601DateFormatter {
    
    // MARK: - Initializers
    
    convenience init(_ formatOptions: Options, timeZone: TimeZone = TimeZone(secondsFromGMT: 0)!) {
        self.init()
        
        self.formatOptions = formatOptions
        self.timeZone = timeZone
    }
}
