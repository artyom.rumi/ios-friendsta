//
//  FloatingPointExtension.swift
//  Tools
//
//  Created by Almaz Ibragimov on 18.04.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

extension FloatingPoint {
    
    // MARK: - Instance Properties
    
    var degreesToRadians: Self {
        return self * .pi / 180
    }
    
    var radiansToDegrees: Self {
        return self * 180 / .pi
    }

    // MARK: - Instance Methods

    func project(initialVelocity: Self, decelerationRate: Self) -> Self {
        if decelerationRate >= 1 {
            return self
        } else {
            return self + initialVelocity * decelerationRate / (1 - decelerationRate)
        }
    }
}
