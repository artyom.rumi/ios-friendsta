//
//  FormatterExtension.swift
//  Friendsta
//
//  Created by Elina Batyrova on 11/10/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension Formatter {
    
    // MARK: - Instance Properties
    
    static let iso8601 = ISO8601DateFormatter([.withInternetDateTime, .withFractionalSeconds])
}
