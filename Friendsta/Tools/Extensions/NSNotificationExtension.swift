//
//  NSNotificationExtension.swift
//  Friendsta
//
//  Created by Elina Batyrova on 22.04.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    
    // MARK: - Type Properties
    
    static let profileAvatarUpdated = NSNotification.Name("profileAvatarUpdated")
}
