//
//  StringProtocolExtension.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 17/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension StringProtocol {

    // MARK: - Instance Methods

    func nsRange(of string: Self, options: String.CompareOptions = [], range: Range<Index>? = nil, locale: Locale? = nil) -> NSRange? {
        guard let range = self.range(of: string, options: options, range: range ?? startIndex ..< endIndex, locale: locale ?? .current) else {
            return nil
        }

        return .init(range, in: self)
    }
}
