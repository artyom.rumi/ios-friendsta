//
//  UITableViewCellExtension.swift
//  Friendsta
//
//  Created by Elina Batyrova on 24.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

public extension UITableViewCell {
    
    // MARK: - Instance Properties
    
    var nib: UINib? {
        let bundle = Bundle(for: type(of: self))
        
        return UINib(nibName: String(describing: type(of: self)), bundle: bundle)
    }
}
