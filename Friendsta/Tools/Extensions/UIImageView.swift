//
//  UIImageView.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 20/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

extension UIImageView {

    // MARK: - UIImageView

    // Fix setup tint color for template images
    // Setting tint color from storyboard doesn't work
    // See: http://openradar.appspot.com/23759908
    open override func awakeFromNib() {
        super.awakeFromNib()

        self.tintColorDidChange()
    }
}
