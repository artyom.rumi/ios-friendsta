//
//  ArrayExtension.swift
//  Tools
//
//  Created by Almaz Ibragimov on 01.01.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public extension Array {

    // MARK: - Intance Methods

    mutating func removeFirst(where predicate: ((Element) -> Bool)) -> Element? {
        guard let index = self.index(where: predicate) else {
            return nil
        }
        
        return self.remove(at: index)
    }
    
    mutating func prepend(_ element: Element) {
        self.insert(element, at: 0)
    }
    
    func split() -> (left: [Element], right: [Element]) {
        let allCount = self.count
        let halfCount = allCount / 2
        
        let leftSplit = self[0..<halfCount]
        let rightSplit = self[halfCount..<allCount]
        
        return (left: Array(leftSplit), right: Array(rightSplit))
    }
}

// MARK: - Array<AnyObject>

public extension Array where Element: AnyObject {

    // MARK: - Instance Methods

    @discardableResult
    mutating func remove(object: Element) -> Element? {
        guard let index = self.firstIndex(where: { $0 === object }) else {
            return nil
        }

        return self.remove(at: index)
    }

    func contains(object: Element) -> Bool {
        return self.contains(where: { $0 === object })
    }
}

extension BidirectionalCollection where Iterator.Element: Equatable {
    typealias Element = Self.Iterator.Element

    func after(_ item: Element, loop: Bool = false) -> Element? {
        if let itemIndex = self.firstIndex(of: item) {
            let lastItem: Bool = (index(after: itemIndex) == endIndex)

            if loop && lastItem {
                return self.first
            } else if lastItem {
                return nil
            } else {
                return self[index(after:itemIndex)]
            }
        }

        return nil
    }

    func before(_ item: Element, loop: Bool = false) -> Element? {
        if let itemIndex = self.firstIndex(of: item) {
            let firstItem: Bool = (itemIndex == startIndex)

            if loop && firstItem {
                return self.last
            } else if firstItem {
                return nil
            } else {
                return self[index(before:itemIndex)]
            }
        }

        return nil
    }
}
