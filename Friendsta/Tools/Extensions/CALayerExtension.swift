//
//  CALayerExtension.swift
//  Tools
//
//  Created by Timur Shafigullin on 27/05/2019.
//  Copyright © 2019 Flatstack. All rights reserved.
//

import UIKit
import QuartzCore

extension CALayer {

    // MARK: - Instance Methods

    func applyShadow(color: UIColor = .black,
                     alpha: Float = 0.5,
                     x: CGFloat = 0,
                     y: CGFloat = 2,
                     blur: CGFloat = 4) {
        self.masksToBounds = false

        self.shadowColor = color.cgColor
        self.shadowOpacity = alpha
        self.shadowOffset = CGSize(width: x, height: y)
        self.shadowRadius = blur / 2.0
    }
}
