//
//  UIApplicationExtension.swift
//  Friendsta
//
//  Created by Elina Batyrova on 11.11.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

extension UIApplication {
    
    // MARK: - Instance Properties
    
    var topVisibleViewController: UIViewController? {
        if let rootViewController = self.delegate?.window??.rootViewController {
            return self.topViewController(currentViewController: rootViewController)
        } else {
            return nil
        }
    }
    
    // MARK: - Instance Methods
    
    private func topViewController(currentViewController: UIViewController)-> UIViewController {
        if let tabBarController = currentViewController as? UITabBarController, let selectedViewController = tabBarController.selectedViewController {
            return self.topViewController(currentViewController: selectedViewController)
        } else if let navigationController = currentViewController as? UINavigationController, let visibleViewController = navigationController.visibleViewController {
            return self.topViewController(currentViewController: visibleViewController)
        } else if let presentedViewController = currentViewController.presentedViewController {
            return self.topViewController(currentViewController: presentedViewController)
        } else if let pageViewController = currentViewController as? UIPageViewController, let viewController = pageViewController.viewControllers?.first {
            return self.topViewController(currentViewController: viewController)
        } else {
            return currentViewController
        }
    }
}
