//
//  ComparableExtension.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 22/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

extension Comparable {

    // MARK: - Instance Methods

    func clamped(to limits: ClosedRange<Self>) -> Self {
        return min(max(self, limits.lowerBound), limits.upperBound)
    }
}
