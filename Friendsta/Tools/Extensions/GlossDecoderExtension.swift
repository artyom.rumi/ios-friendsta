//
//  GlossDecoderExtension.swift
//  Friendsta
//
//  Created by Elina Batyrova on 11/10/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation
import Gloss

extension Decoder {
    
    // MARK: - Instance Methods
    
    public static func decode(dateForKey key: String, dateFormatter: ISO8601DateFormatter, keyPathDelimiter: String = GlossKeyPathDelimiter) -> (JSON) -> Date? {
        return {
            json in
            
            if let dateString = json.valueForKeyPath(keyPath: key, withDelimiter: keyPathDelimiter) as? String {
                return dateFormatter.date(from: dateString)
            }
            
            return nil
        }
    }
}
