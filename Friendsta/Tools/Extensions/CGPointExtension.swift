//
//  CGPointExtension.swift
//  Tools
//
//  Created by Almaz Ibragimov on 01.01.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation
import CoreGraphics

public extension CGPoint {

    // MARK: - Type Methods

    static func + (left: CGPoint, right: CGPoint) -> CGPoint {
        return CGPoint(x: left.x + right.x, y: left.y + right.y)
    }
    
    // MARK: - Instance Properties
    
    var adjusted: CGPoint {
        return CGPoint(x: Int(floor(self.x)),
                       y: Int(floor(self.y)))
    }

    // MARK: - Instance Methods

    func distance(to point: CGPoint) -> CGFloat {
        return sqrt(pow((point.x - x), 2) + pow((point.y - y), 2))
    }

    func project(initialVelocity: CGPoint, decelerationRate: CGPoint) -> CGPoint {
        let xProjection = x.project(initialVelocity: initialVelocity.x, decelerationRate: decelerationRate.x)
        let yProjection = y.project(initialVelocity: initialVelocity.y, decelerationRate: decelerationRate.y)

        return CGPoint(x: xProjection, y: yProjection)
    }

    func project(initialVelocity: CGPoint, decelerationRate: CGFloat) -> CGPoint {
        return self.project(initialVelocity: initialVelocity, decelerationRate: CGPoint(x: decelerationRate, y: decelerationRate))
    }
}
