//
//  UIPanGestureRecognizerExtension.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 05/09/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

extension UIPanGestureRecognizer {

    // MARK: - Instance Methods

    func incrementToBottom(maxTranslation: CGFloat) -> CGFloat {
        let translation = self.translation(in: self.view).y

        self.setTranslation(.zero, in: nil)

        let percentIncrement = translation / maxTranslation

        return percentIncrement
    }

    func isProjectedToDownHalf(maxTranslation: CGFloat) -> Bool {
        let endLocation = self.projectedLocation()
        let isPresentationCompleted = (endLocation.y > maxTranslation / 2)

        return isPresentationCompleted
    }

    func projectedLocation() -> CGPoint {
        let velocityOffset = self.velocity(in: self.view).project(initialVelocity: .zero, decelerationRate: UIScrollView.DecelerationRate.normal.rawValue)
        let projectedLocation = self.location(in: self.view) + velocityOffset

        return projectedLocation
    }
}
