//
//  UserDefaultsManager.swift
//  Friendsta
//
//  Created by Elina Batyrova on 24/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

protocol UserDefaultsManager: AnyObject {
    
    // MARK: - Instance Properties
    
    var userDefaults: UserDefaults! { get }
    
    var isUserHasSeenFriendRequestGuide: Bool { get set }
    var isUserHasSeenPokeGuide: Bool { get set }
    var isUserHasSeenFriendsScreen: Bool { get set }
    var isUserHasSeenCommunityScreen: Bool { get set }
    var isUserHasSeenWriteAPostMessage: Bool { get set }
    var isUserHasSeenAnonymousPostingGuide: Bool { get set }
    // added by lava on 0125
    var isUserHasSeenTenRateToRequestFriend: Bool  {get set }
}
