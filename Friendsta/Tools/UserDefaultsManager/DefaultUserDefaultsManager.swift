//
//  DefaultUserDefaultsManager.swift
//  Friendsta
//
//  Created by Elina Batyrova on 25/07/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import Foundation

class DefaultUserDefaultsManager: UserDefaultsManager {
    
    // MARK: - Constants
    
    private enum Keys {
        
        // MARK: - Type Properties
        
        static let isUserHasSeenFriendRequestGuideKey = "UserHasSeenFriendRequestGuide"
        static let isUserHasSeenPokeGuideKey = "UserHasSeenPokeGuide"
        static let isUserHasSeenFriendsScreen = "UserHasSeenFriendsScreen"
        static let isUserHasSeenCommunityScreen = "UserHasSeenCommunityScreen"
        static let isUserHasSeenWriteAPostMessage = "UserHasSeenWriteAPostMessage"
        static let isUserHasSeenAnonymousPostingGuide = "UserHasSeenAnonymousPostingGuide"
        
        // added by lava 0125
        static let isUserHasSeenTenRateToRequestFriend = "isUserHasSeenTenRateToRequestFriend"
    }
    
    // MARK: - Instance Properties
    
    var userDefaults: UserDefaults!
    
    // MARK: -
    
    var isUserHasSeenFriendRequestGuide: Bool {
        get {
            return self.userDefaults.bool(forKey: Keys.isUserHasSeenFriendRequestGuideKey)
        }
        
        set {
            self.userDefaults.set(newValue, forKey: Keys.isUserHasSeenFriendRequestGuideKey)
        }
    }
    
    var isUserHasSeenPokeGuide: Bool {
        get {
            return self.userDefaults.bool(forKey: Keys.isUserHasSeenPokeGuideKey)
        }
        
        set {
            self.userDefaults.set(newValue, forKey: Keys.isUserHasSeenPokeGuideKey)
        }
    }
    
    var isUserHasSeenFriendsScreen: Bool {
        get {
            return self.userDefaults.bool(forKey: Keys.isUserHasSeenFriendsScreen)
        }
        
        set {
            self.userDefaults.set(newValue, forKey: Keys.isUserHasSeenFriendsScreen)
        }
    }

    var isUserHasSeenCommunityScreen: Bool {
        get {
            return self.userDefaults.bool(forKey: Keys.isUserHasSeenCommunityScreen)
        }

        set {
            self.userDefaults.set(newValue, forKey: Keys.isUserHasSeenCommunityScreen)
        }
    }

    var isUserHasSeenWriteAPostMessage: Bool {
        get {
            return self.userDefaults.bool(forKey: Keys.isUserHasSeenWriteAPostMessage)
        }

        set {
            self.userDefaults.set(newValue, forKey: Keys.isUserHasSeenWriteAPostMessage)
        }
    }
    
    var isUserHasSeenAnonymousPostingGuide: Bool {
        get {
            return self.userDefaults.bool(forKey: Keys.isUserHasSeenAnonymousPostingGuide)
        }
        
        set {
            self.userDefaults.set(newValue, forKey: Keys.isUserHasSeenAnonymousPostingGuide)
        }
    }
    
    // added by lava on 0125
    var isUserHasSeenTenRateToRequestFriend : Bool {
        get  {
            return self.userDefaults.bool(forKey: Keys.isUserHasSeenTenRateToRequestFriend)
        }
        
        set {
            self.userDefaults.set(newValue, forKey: Keys.isUserHasSeenTenRateToRequestFriend)
        }
    }
    
    // MARK: - Initializers
    
    init(suiteName: String) {
        self.userDefaults = UserDefaults(suiteName: suiteName)
    }
}
