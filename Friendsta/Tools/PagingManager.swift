//
//  PagingManager.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 22/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit
import pop

final class PagingManager {

    // MARK: - Nested Types

    private enum Constants {

        // MARK: - Type Properties

        static let snappingAnimationKey = "PagingView.contentView.snappingAnimation"
        static let nextPageDeltaOffset: CGFloat = 60
    }

    // MARK: - Instance Properties

    private let scrollView: UIScrollView
    private let anchors: [CGPoint]
    private let topHeaderPoint: CGPoint

    // MARK: -

    var minAnchor: CGPoint {
        let x = -self.scrollView.adjustedContentInset.left
        let y = -self.scrollView.adjustedContentInset.top

        return CGPoint(x: x, y: y)
    }

    var maxAnchor: CGPoint {
        let x = self.scrollView.contentSize.width - self.scrollView.bounds.width + self.scrollView.adjustedContentInset.right
        let y = self.scrollView.contentSize.height - self.scrollView.bounds.height + self.scrollView.adjustedContentInset.bottom

        return CGPoint(x: x, y: y)
    }

    // MARK: - Initializers

    init(scrollView: UIScrollView, anchors: [CGPoint], topHeaderPoint: CGPoint = CGPoint(x: 0, y: 0)) {
        self.scrollView = scrollView
        self.anchors = anchors
        self.topHeaderPoint = topHeaderPoint
    }

    // MARK: - Instance Methods

    private func nearestAnchor(forContentOffset offset: CGPoint, atBottom: Bool) -> CGPoint? {
        guard var canidate = self.anchors.min(by: {
            return offset.distance(to: $0) < offset.distance(to: $1)
        }) else {
            return nil
        }

        guard abs(offset.y - canidate.y) > Constants.nextPageDeltaOffset || offset.y < self.topHeaderPoint.y else {
            return nil
        }

        if offset.y > self.topHeaderPoint.y {
            if atBottom && offset.y > canidate.y {
                canidate = self.anchors.after(canidate) ?? canidate
            } else if !atBottom && offset.y < canidate.y {
                canidate = self.anchors.before(canidate) ?? canidate
            }
        }

        let x = canidate.x.clamped(to: self.minAnchor.x ... self.maxAnchor.x)
        let y = canidate.y.clamped(to: self.minAnchor.y ... self.maxAnchor.y)

        return CGPoint(x: x, y: y)
    }

    // MARK: -

    private func snapAnimated(toContentOffset newOffset: CGPoint, velocity: CGPoint) {
        let animation = POPSpringAnimation(propertyNamed: kPOPScrollViewContentOffset)

        animation?.velocity = velocity
        animation?.toValue = newOffset
        animation?.fromValue = self.scrollView.contentOffset
        animation?.springBounciness = 0
        animation?.springSpeed = 12

        self.scrollView.pop_add(animation, forKey: Constants.snappingAnimationKey)
    }

    private func stopSnappingAnimation() {
        self.scrollView.pop_removeAnimation(forKey: Constants.snappingAnimationKey)
    }

    // MARK: -

    func contentViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee = scrollView.contentOffset

        let offsetProjection = scrollView.contentOffset.project(initialVelocity: velocity, decelerationRate: self.scrollView.decelerationRate.rawValue)

        if let target = self.nearestAnchor(forContentOffset: offsetProjection, atBottom: velocity.y >= 0.0) {
            self.snapAnimated(toContentOffset: target, velocity: velocity)
        }
    }

    func contentViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.stopSnappingAnimation()
    }
}
