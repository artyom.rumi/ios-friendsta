//
//  DashedView.swift
//  Slider
//
//  Created by Timur Shafigullin on 10/01/2019.
//  Copyright © 2019 Timur Shafigullin. All rights reserved.
//

import UIKit

@IBDesignable class DashedView: UIView {
    
    // MARK: - UIView
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let borderLayer = CAShapeLayer()
        
        borderLayer.strokeColor = UIColor(red: 183 / 255.0, green: 183 / 255.0, blue: 183 / 255.0, alpha: 1).cgColor
        borderLayer.lineDashPattern = [3, 3]
        borderLayer.frame = self.bounds
        borderLayer.fillColor = nil
        borderLayer.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.bounds.height / 2).cgPath
        
        self.layer.addSublayer(borderLayer)
    }
}
