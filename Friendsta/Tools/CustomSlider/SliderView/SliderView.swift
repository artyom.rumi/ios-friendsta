//
//  SliderView.swift
//  Slider
//
//  Created by Timur Shafigullin on 10/01/2019.
//  Copyright © 2019 Timur Shafigullin. All rights reserved.
//

import UIKit

@IBDesignable class SliderView: UIView {
    
    // MARK: - Instance Properties
    
    @IBOutlet fileprivate var contentView: UIView!
    
    @IBOutlet fileprivate weak var leftView: UIView!
    @IBOutlet fileprivate weak var leftLabel: UILabel!
    @IBOutlet fileprivate weak var leftViewLeftConstraint: NSLayoutConstraint!
    
    @IBOutlet fileprivate weak var rightView: DashedView!
    @IBOutlet fileprivate weak var rightLabel: UILabel!
    
    // MARK: -
    
    var didSliderCompleted: (() -> Void)?
    
    @IBInspectable var leftTitle: String? = nil {
        didSet {
            self.apply()
        }
    }
    
    @IBInspectable var rightTitle: String? = nil {
        didSet {
            self.apply()
        }
    }
    
    // MARK: - Instance Methods
    
    @IBAction fileprivate func onLeftViewDragged(_ sender: UIPanGestureRecognizer) {
        defer {
            sender.setTranslation(.zero, in: self.contentView)
        }
        
        if sender.state == .ended {
            if self.leftView.frame.midX >= self.rightView.frame.minX {
                self.didSliderCompleted?()
            } else {
                self.leftViewLeftConstraint.constant = 0
            }
        } else {
            let translation = sender.translation(in: self.contentView)
            
            let newConstant = self.leftViewLeftConstraint.constant + translation.x
            
            guard 0 ... self.contentView.bounds.maxX - self.leftView.bounds.width ~= newConstant else {
                return
            }
            
            self.leftViewLeftConstraint.constant = newConstant
        }
        
        UIView.animate(withDuration: 0.25) { [unowned self] in
            self.layoutIfNeeded()
        }
    }
    
    fileprivate func initializeXib() {
        Bundle.main.loadNibNamed("SliderView", owner: self, options: nil)
        
        self.addSubview(self.contentView)
        
        self.contentView.frame = self.bounds
        self.contentView.layer.cornerRadius = self.contentView.bounds.height / 2
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        
        self.apply()
    }
    
    fileprivate func apply() {
        self.leftLabel.text = self.leftTitle
        self.rightLabel.text = self.rightTitle
        
        self.leftView.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "SliderBackground.pdf"))
    }
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initializeXib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initializeXib()
    }
}
