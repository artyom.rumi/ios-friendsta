//
//  ChatClient.swift
//  Tools
//
//  Created by Almaz Ibragimov on 17.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public protocol ChatClient {
    
    // MARK: - Instance Properties
    
    var observers: [ChatClientObserver] { get }
    
    var channels: [String] { get }
        
    // MARK: - Instance Methods
    
    func isSubscribed(on channel: String) -> Bool
    
    func subscribe(to channels: [String])
    func unsubscribe(from channels: [String])
    func unsubscribeFromAll()
    
    func send(event: ChatClientEvent, pushPayload: ChatClientPushPayload?, to channel: String, completion: @escaping ((ChatClientError?) -> Void))
    
    func history(on channel: String, limit: Int, startTime: Int64?, endTime: Int64?, completion: @escaping ((ChatClientHistory?, ChatClientError?) -> Void))
    func history(on channel: String, startTime: Int64?, endTime: Int64?, completion: @escaping ((ChatClientHistory?, ChatClientError?) -> Void))
    
    func addObserver(_ observer: ChatClientObserver)
    func removeObserver(_ observer: ChatClientObserver)
}
