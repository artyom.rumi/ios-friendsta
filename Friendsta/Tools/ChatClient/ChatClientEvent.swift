//
//  ChatClientEvent.swift
//  Tools
//
//  Created by Almaz Ibragimov on 18.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public struct ChatClientEvent {
    
    // MARK: - Instance Properties
    
    public let method: ChatClientMethod
    
    public let chatUID: Int64
    
    public let senderUID: Int64?
    public let receiverUID: Int64?
    
    // MARK: -
    
    public var logDescription: String {
        return "Chat #\(self.chatUID): \(self.method.logDescription)"
    }
}
