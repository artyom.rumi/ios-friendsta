//
//  ChatClientMessageCoder.swift
//  Tools
//
//  Created by Almaz Ibragimov on 18.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public protocol ChatClientMessageCoder {
    
    // MARK: - Instance Methods
    
    func message(from json: [String: Any]) -> ChatClientMessage?
    func json(from message: ChatClientMessage) -> [String: Any]
}
