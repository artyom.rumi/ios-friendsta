//
//  ChatClientHistory.swift
//  Tools
//
//  Created by Almaz Ibragimov on 18.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public struct ChatClientHistory {
    
    // MARK: - Instance Properties
    
    public let events: [ChatClientEvent]
    
    public let startTime: Int64
    public let endTime: Int64
}
