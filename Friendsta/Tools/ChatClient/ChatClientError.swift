//
//  ChatClientError.swift
//  Tools
//
//  Created by Almaz Ibragimov on 17.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public class ChatClientError: Error {
    
    // MARK: - Nested Types
    
    public enum Code {
        
        // MARK: - Enumeration Cases
        
        case unknown
        case aborted
        
        case connection
        case timeOut
        
        case security
        
        case badRequest
        case badResponse
        
        case unauthorized
    }
    
    // MARK: - Type Properties
    
    public static let unknown = ChatClientError(code: .unknown)
    public static let aborted = ChatClientError(code: .aborted)
    
    public static let connection = ChatClientError(code: .connection)
    public static let timeOut = ChatClientError(code: .timeOut)
    
    public static let security = ChatClientError(code: .security)
    
    public static let badRequest = ChatClientError(code: .badRequest)
    public static let badResponse = ChatClientError(code: .badResponse)
    
    public static let unauthorized = ChatClientError(code: .unauthorized)
    
    // MARK: - Instance Properties
    
    public final let code: Code
    
    public final let base: ChatClientErrorBase?
    
    // MARK: -
    
    public var logDescription: String {
        var description: String
        
        switch self {
        case ChatClientError.aborted:
            description = "ChatClientError.aborted"
            
        case ChatClientError.connection:
            description = "ChatClientError.connection"
            
        case ChatClientError.timeOut:
            description = "ChatClientError.timeOut"
            
        case ChatClientError.security:
            description = "ChatClientError.security"
            
        case ChatClientError.badRequest:
            description = "ChatClientError.badRequest"
            
        case ChatClientError.badResponse:
            description = "ChatClientError.badResponse"
            
        case ChatClientError.unauthorized:
            description = "ChatClientError.unauthorized"
            
        default:
            description = "WebError.unknown"
        }
        
        if let base = self.base {
            description.append("(\(base.logDescription))")
        }
        
        return description
    }
    
    // MARK: - Initializers
    
    public init(code: Code, base: ChatClientErrorBase? = nil) {
        self.code = code

        self.base = base
    }
}

// MARK: - Equatable

extension ChatClientError: Equatable {
    
    // MARK: - Type Methods
    
    public static func == (left: ChatClientError, right: ChatClientError) -> Bool {
        return (left.code == right.code)
    }
    
    public static func != (left: ChatClientError, right: ChatClientError) -> Bool {
        return (left.code != right.code)
    }
    
    public static func ~= (left: ChatClientError, right: ChatClientError) -> Bool {
        return (left.code == right.code)
    }
}
