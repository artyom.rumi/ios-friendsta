//
//  ChatClientObserver.swift
//  Tools
//
//  Created by Almaz Ibragimov on 17.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public protocol ChatClientObserver: class {

    // MARK: - Instance Methods
    
    func chatClient(_ chatClient: ChatClient, didReceiveEvent event: ChatClientEvent)
}
