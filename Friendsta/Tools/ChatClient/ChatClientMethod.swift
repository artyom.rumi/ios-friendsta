//
//  ChatClientMethod.swift
//  Tools
//
//  Created by Almaz Ibragimov on 18.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public enum ChatClientMethod {
    
    // MARK: - Enumeration Cases
    
    case publish(message: ChatClientMessage)
    case update(message: ChatClientMessage)
    
    case block
    case unblock
    
    case restore
    case delete
    case clear
    
    // MARK: - Instance Properties
    
    public var logDescription: String {
        switch self {
        case .publish(let message):
            return "PUBLISH \(message.logDescription)"
        
        case .update(let message):
            return "UPDATE \(message.logDescription)"
            
        case .block:
            return "BLOCK"
            
        case .unblock:
            return "UNBLOCK"
            
        case .restore:
            return "RESTORE"
            
        case .delete:
            return "DELETE"
            
        case .clear:
            return "CLEAR"
        }
    }
}
