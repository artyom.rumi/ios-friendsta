//
//  ChatClientPushPayload.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public struct ChatClientPushPayload {
    
    // MARK: - Instance Properties
    
    public let apns: [String: Any]?
    public let gcm: [String: Any]?
    public let mpns: [String: Any]?
    
    public let exceptions: [String]
    
    // MARK: - Initializers
    
    public init(apns: [String: Any]? = nil, gcm: [String: Any]? = nil, mpns: [String: Any]? = nil, exceptions: [String] = []) {
        self.apns = apns
        self.gcm = gcm
        self.mpns = mpns
        
        self.exceptions = exceptions
    }
}
