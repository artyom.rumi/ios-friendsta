//
//  ChatClientErrorBase.swift
//  Tools
//
//  Created by Almaz Ibragimov on 17.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public protocol ChatClientErrorBase {
    
    // MARK: - Instance Properties
    
    var logDescription: String { get }
}
