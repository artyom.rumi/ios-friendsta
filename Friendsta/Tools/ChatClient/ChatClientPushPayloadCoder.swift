//
//  ChatClientPushPayloadCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public protocol ChatClientPushPayloadCoder {
    
    // MARK: - Instance Methods
    
    func pushPayload(from json: [String: Any]) -> ChatClientPushPayload?
    func json(from pushPayload: ChatClientPushPayload) -> [String: Any]
}
