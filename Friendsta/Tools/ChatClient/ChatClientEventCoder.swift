//
//  ChatClientEventCoder.swift
//  Tools
//
//  Created by Almaz Ibragimov on 18.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public protocol ChatClientEventCoder {
    
    // MARK: - Instance Methods
    
    func event(from json: [String: Any]) -> ChatClientEvent?
    func json(from event: ChatClientEvent) -> [String: Any]
}
