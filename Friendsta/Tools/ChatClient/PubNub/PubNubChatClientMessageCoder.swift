//
//  PubNubChatClientMessageCoder.swift
//  Tools
//
//  Created by Almaz Ibragimov on 18.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public struct PubNubChatClientMessageCoder: ChatClientMessageCoder {
    
    // MARK: -
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let uid = "id"
        
        static let createdDate = "created_at"
        static let modifiedDate = "modified_at"
        static let deliveredDate = "delivered_ad"
        static let viewedDate = "viewed_at"
        static let erasedDate = "erased_at"
        
        static let isAnonymous = "is_anonymous"
        
        static let content = "content"
    }
    
    // MARK: - Instance Methods
    
    public func message(from json: [String: Any]) -> ChatClientMessage? {
        guard let uid = json[JSONKeys.uid] as? String else {
            return nil
        }
        
        guard let createdDateInterval = json[JSONKeys.createdDate] as? Double else {
            return nil
        }
        
        let createdDate = Date(timeIntervalSinceReferenceDate: TimeInterval(createdDateInterval))
        
        let modifiedDate: Date?
        
        if let modifiedDateInterval = json[JSONKeys.modifiedDate] as? Double {
            modifiedDate = Date(timeIntervalSinceReferenceDate: TimeInterval(modifiedDateInterval))
        } else {
            modifiedDate = nil
        }
        
        let deliveredDate: Date?
        
        if let deliveredDateInterval = json[JSONKeys.deliveredDate] as? Double {
            deliveredDate = Date(timeIntervalSinceReferenceDate: TimeInterval(deliveredDateInterval))
        } else {
            deliveredDate = nil
        }
        
        let viewedDate: Date?
        
        if let viewedDateInterval = json[JSONKeys.viewedDate] as? Double {
            viewedDate = Date(timeIntervalSinceReferenceDate: TimeInterval(viewedDateInterval))
        } else {
            viewedDate = nil
        }
        
        let erasedDate: Date?
        
        if let erasedDateInterval = json[JSONKeys.erasedDate] as? Double {
            erasedDate = Date(timeIntervalSinceReferenceDate: TimeInterval(erasedDateInterval))
        } else {
            erasedDate = nil
        }
        
        guard let content = json[JSONKeys.content] as? [String: Any] else {
            return nil
        }
        
        let isAnonymous = json[JSONKeys.isAnonymous] as? Bool ?? true
        
        return ChatClientMessage(uid: uid,
                                 createdDate: createdDate,
                                 modifiedDate: modifiedDate,
                                 deliveredDate: deliveredDate,
                                 viewedDate: viewedDate,
                                 erasedDate: erasedDate,
                                 isAnonymous: isAnonymous,
                                 content: content)
    }
    
    public func json(from message: ChatClientMessage) -> [String: Any] {
        var json: [String: Any] = [:]
        
        json[JSONKeys.uid] = message.uid
        
        json[JSONKeys.createdDate] = Double(message.createdDate.timeIntervalSinceReferenceDate)
        
        if let modifiedDate = message.modifiedDate {
            json[JSONKeys.modifiedDate] = Double(modifiedDate.timeIntervalSinceReferenceDate)
        }
        
        if let deliveredDate = message.deliveredDate {
            json[JSONKeys.deliveredDate] = Double(deliveredDate.timeIntervalSinceReferenceDate)
        }
        
        if let viewedDate = message.viewedDate {
            json[JSONKeys.viewedDate] = Double(viewedDate.timeIntervalSinceReferenceDate)
        }
        
        json[JSONKeys.content] = message.content
        
        json[JSONKeys.isAnonymous] = message.isAnonymous
        
        return json
    }
}
