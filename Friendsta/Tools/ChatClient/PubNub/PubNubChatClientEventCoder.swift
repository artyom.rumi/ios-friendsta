//
//  PubNubChatClientEventCoder.swift
//  Tools
//
//  Created by Almaz Ibragimov on 19.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public struct PubNubChatClientEventCoder: ChatClientEventCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties

        static let method = "method"
        
        static let chatUID = "chat_id"
        
        static let senderUID = "sender_id"
        static let receiverUID = "receiver_id"
        
        static let version = "version"
    }
    
    // MARK: -
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let version = 10000
    }
    
    // MARK: - Instance Properties
    
    let methodCoder: ChatClientMethodCoder
    
    // MARK: - Instance Methods
    
    public func event(from json: [String: Any]) -> ChatClientEvent? {
        guard let version = json[JSONKeys.version] as? Int, version == Constants.version else {
            return nil
        }
        
        guard let methodJSON = json[JSONKeys.method] as? [String: Any] else {
            return nil
        }
        
        guard let method = self.methodCoder.method(from: methodJSON) else {
            return nil
        }
        
        guard let chatUID = json[JSONKeys.chatUID] as? Int64 else {
            return nil
        }
        
        let senderUID = json[JSONKeys.senderUID] as? Int64
        let receiverUID = json[JSONKeys.receiverUID] as? Int64
        
        return ChatClientEvent(method: method,
                               chatUID: chatUID,
                               senderUID: senderUID,
                               receiverUID: receiverUID)
    }
    
    public func json(from event: ChatClientEvent) -> [String: Any] {
        var json: [String: Any] = [:]
        
        json[JSONKeys.method] = self.methodCoder.json(from: event.method)
        
        json[JSONKeys.chatUID] = event.chatUID
        
        if let senderUID = event.senderUID {
            json[JSONKeys.senderUID] = senderUID
        }
        
        if let receiverUID = event.receiverUID {
            json[JSONKeys.receiverUID] = receiverUID
        }
        
        json[JSONKeys.version] = Constants.version
        
        return json
    }
}
