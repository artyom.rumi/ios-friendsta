//
//  PubNubChatClientError.swift
//  Tools
//
//  Created by Almaz Ibragimov on 17.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation
import PubNub

fileprivate extension PNOperationType {
    
    // MARK: - Instance Properties
    
    var logDescription: String {
        switch self {
        case .subscribeOperation:
            return "subscribe"
            
        case .unsubscribeOperation:
            return "unsubscribe"
            
        case .publishOperation:
            return "publish"
            
        case .historyOperation:
            return "history"
            
        case .historyForChannelsOperation:
            return "historyForChannels"
            
        case .deleteMessageOperation:
            return "deleteMessage"
            
        case .whereNowOperation:
            return "whereNow"
            
        case .hereNowGlobalOperation:
            return "hereNowGlobal"
            
        case .hereNowForChannelOperation:
            return "hereNowForChannel"
            
        case .hereNowForChannelGroupOperation:
            return "hereNowForChannelGroup"
            
        case .heartbeatOperation:
            return "heartbeat"
            
        case .setStateOperation:
            return "setState"
            
        case .stateForChannelOperation:
            return "stateForChannel"
            
        case .stateForChannelGroupOperation:
            return "stateForChannelGroup"
            
        case .addChannelsToGroupOperation:
            return "addChannelsToGroup"
            
        case .removeChannelsFromGroupOperation:
            return "removeChannelsFromGroup"
            
        case .channelGroupsOperation:
            return "channelGroups"
            
        case .removeGroupOperation:
            return "removeGroup"
            
        case .channelsForGroupOperation:
            return "channelsForGroup"
            
        case .pushNotificationEnabledChannelsOperation:
            return "pushNotificationEnabledChannels"
            
        case .addPushNotificationsOnChannelsOperation:
            return "addPushNotificationsOnChannels"
            
        case .removePushNotificationsFromChannelsOperation:
            return "removePushNotificationsFromChannels"
            
        case .removeAllPushNotificationsOperation:
            return "removeAllPushNotifications"
            
        case .timeOperation:
            return "time"

        case .messageCountOperation:
            return "messageCountOperation"

        case .getStateOperation:
            return "getStateOperation"

        @unknown default:
            fatalError()
        }
    }
}

// MARK: -

fileprivate extension PNStatusCategory {
    
    // MARK: - Instance Properties
    
    var logDescription: String {
        switch self {
        case .PNCancelledCategory:
            return "cancelled"
            
        case .PNNetworkIssuesCategory:
            return "networkIssues"
            
        case .PNUnexpectedDisconnectCategory:
            return "unexpectedDisconnect"
            
        case .PNTimeoutCategory:
            return "timeout"
            
        case .PNTLSConnectionFailedCategory:
            return "connectionFailed"
            
        case .PNTLSUntrustedCertificateCategory:
            return "untrustedCertificate"
            
        case .PNBadRequestCategory:
            return "badRequest"
            
        case .PNRequestURITooLongCategory:
            return "requestURITooLong"
            
        case .PNMalformedResponseCategory:
            return "malformedResponse"
            
        case .PNDecryptionErrorCategory:
            return "decryptionError"
            
        case .PNAccessDeniedCategory:
            return "accessDenied"
            
        case .PNUnknownCategory:
            return "unknown"
            
        case .PNAcknowledgmentCategory:
            return "acknowledgment"
            
        case .PNRequestMessageCountExceededCategory:
            return "requestMessageCountExceeded"
            
        case .PNConnectedCategory:
            return "connected"
            
        case .PNReconnectedCategory:
            return "reconnected"
            
        case .PNDisconnectedCategory:
            return "disconnected"
            
        case .PNMalformedFilterExpressionCategory:
            return "malformedFilterExpression"
        }
    }
}

// MARK: -

extension PNStatus: ChatClientErrorBase {
    
    // MARK: - Instance Properties
    
    public var logDescription: String {
        return "PNStatus(operation: \(self.operation.logDescription), category: \(self.category.logDescription))"
    }
}

// MARK: -

public final class PubNubChatClientError: ChatClientError {
    
    // MARK: - Initializers
    
    public convenience init?(fromPNStatus status: PNStatus) {
        guard status.isError else {
            return nil
        }
        
        switch status.category {
        case .PNCancelledCategory:
            self.init(code: .aborted, base: status)
            
        case .PNNetworkIssuesCategory,
             .PNUnexpectedDisconnectCategory:
            self.init(code: .connection, base: status)
            
        case .PNTimeoutCategory:
            self.init(code: .timeOut, base: status)
        
        case .PNTLSConnectionFailedCategory,
             .PNTLSUntrustedCertificateCategory:
            self.init(code: .security, base: status)
            
        case .PNBadRequestCategory,
             .PNRequestURITooLongCategory:
            self.init(code: .badRequest, base: status)
            
        case .PNMalformedResponseCategory,
             .PNDecryptionErrorCategory:
            self.init(code: .badResponse, base: status)
            
        case .PNAccessDeniedCategory:
            self.init(code: .unauthorized, base: status)
            
        case .PNUnknownCategory,
             .PNAcknowledgmentCategory,
             .PNRequestMessageCountExceededCategory,
             .PNConnectedCategory,
             .PNReconnectedCategory,
             .PNDisconnectedCategory,
             .PNMalformedFilterExpressionCategory:
            self.init(code: .unknown, base: status)
        }
    }
}
