//
//  PubNubChatClientPushPayloadCoder.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public struct PubNubChatClientPushPayloadCoder: ChatClientPushPayloadCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let apns = "pn_apns"
        static let gcm = "pn_gcm"
        static let mpns = "pn_mpns"
        
        static let exceptions = "pn_exceptions"
    }
    
    // MARK: - Instance Methods
    
    public func pushPayload(from json: [String: Any]) -> ChatClientPushPayload? {
        let apns = json[JSONKeys.apns] as? [String: Any]
        let gcm = json[JSONKeys.gcm] as? [String: Any]
        let mpns = json[JSONKeys.gcm] as? [String: Any]
        
        let exceptions = json[JSONKeys.exceptions] as? [String]
        
        return ChatClientPushPayload(apns: apns, gcm: gcm, mpns: mpns, exceptions: exceptions ?? [])
    }
    
    public func json(from pushPayload: ChatClientPushPayload) -> [String: Any] {
        var json: [String: Any] = [:]
        
        if var apns = pushPayload.apns {
            apns[JSONKeys.exceptions] = pushPayload.exceptions
            
            json[JSONKeys.apns] = apns
        }
        
        if let gcm = pushPayload.gcm {
            json[JSONKeys.gcm] = gcm
        }
        
        if let mpns = pushPayload.mpns {
            json[JSONKeys.mpns] = mpns
        }
        
        return json
    }
}
