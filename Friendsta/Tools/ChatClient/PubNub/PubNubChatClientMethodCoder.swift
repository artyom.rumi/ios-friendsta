//
//  PubNubChatClientMethodCoder.swift
//  Tools
//
//  Created by Almaz Ibragimov on 19.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public struct PubNubChatClientMethodCoder: ChatClientMethodCoder {
    
    // MARK: - Nested Types
    
    fileprivate enum JSONKeys {
        
        // MARK: - Type Properties
        
        static let type = "type"
        static let message = "message"
    }
    
    // MARK: -
    
    fileprivate enum JSONTypeValues {
        
        // MARK: - Type Properties
        
        static let publish = "publish"
        static let update = "update"
        
        static let block = "block"
        static let unblock = "unblock"
        
        static let restore = "restore"
        static let delete = "delete"
        static let clear = "clear"
    }
    
    // MARK: - Instance Properties
    
    public let messageCoder: ChatClientMessageCoder
    
    // MARK: - Instance Methods
    
    public func method(from json: [String: Any]) -> ChatClientMethod? {
        switch (json[JSONKeys.type] as? String)?.lowercased() {
        case .some(JSONTypeValues.publish):
            guard let messageJSON = json[JSONKeys.message] as? [String: Any] else {
                return nil
            }
            
            guard let message = self.messageCoder.message(from: messageJSON) else {
                return nil
            }
            
            return .publish(message: message)
            
        case .some(JSONTypeValues.update):
            guard let messageJSON = json[JSONKeys.message] as? [String: Any] else {
                return nil
            }
            
            guard let message = self.messageCoder.message(from: messageJSON) else {
                return nil
            }
            
            return .update(message: message)
        
        case .some(JSONTypeValues.block):
            return .block
            
        case .some(JSONTypeValues.unblock):
            return .unblock
            
        case .some(JSONTypeValues.restore):
            return .restore
            
        case .some(JSONTypeValues.delete):
            return .delete
            
        case .some(JSONTypeValues.clear):
            return .clear
            
        default:
            return nil
        }
    }
    
    public func json(from method: ChatClientMethod) -> [String: Any] {
        var json: [String: Any] = [:]
        
        switch method {
        case .publish(let message):
            json[JSONKeys.type] = JSONTypeValues.publish
            
            json[JSONKeys.message] = self.messageCoder.json(from: message)
        
        case .update(let message):
            json[JSONKeys.type] = JSONTypeValues.update
            
            json[JSONKeys.message] = self.messageCoder.json(from: message)
            
        case .block:
            json[JSONKeys.type] = JSONTypeValues.block
            
        case .unblock:
            json[JSONKeys.type] = JSONTypeValues.unblock
            
        case .restore:
            json[JSONKeys.type] = JSONTypeValues.restore
            
        case .delete:
            json[JSONKeys.type] = JSONTypeValues.delete
            
        case .clear:
            json[JSONKeys.type] = JSONTypeValues.clear
        }
        
        return json
    }
}
