//
//  PubNubChatClient.swift
//  Tools
//
//  Created by Almaz Ibragimov on 17.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation
import PubNub
import FriendstaTools

public class PubNubChatClient: NSObject, ChatClient {
    
    // MARK: - Instance Properties
    
    fileprivate let pubNubClient: PubNub
    
    // MARK: -
    
    public let eventCoder: ChatClientEventCoder
    public let pushPayloadCoder: ChatClientPushPayloadCoder
    
    public var publishKey: String {
        return self.pubNubClient.currentConfiguration().publishKey
    }
    
    public var subscribeKey: String {
        return self.pubNubClient.currentConfiguration().subscribeKey
    }
    
    // MARK: - ChatClient
    
    public fileprivate(set) var observers: [ChatClientObserver] = []
    
    public var channels: [String] {
        return self.pubNubClient.channels()
    }
    
    // MARK: - Initializers
    
    public init(eventCoder: ChatClientEventCoder, pushPayloadCoder: ChatClientPushPayloadCoder, publishKey: String, subscribeKey: String) {
        let configuration = PNConfiguration(publishKey: publishKey, subscribeKey: subscribeKey)
        
        configuration.completeRequestsBeforeSuspension = true
        configuration.stripMobilePayload = false
        
        self.pubNubClient = PubNub.clientWithConfiguration(configuration)
        
        self.eventCoder = eventCoder
        self.pushPayloadCoder = pushPayloadCoder

        super.init()
    }
    
    // MARK: - Instance Methods
    
    public func isSubscribed(on channel: String) -> Bool {
        return self.pubNubClient.isSubscribed(on: channel)
    }
    
    public func subscribe(to channels: [String]) {
        Log.low("subscribe(toChannels: \(channels))", from: self)
        
        self.pubNubClient.subscribeToChannels(channels, withPresence: false)
    }
    
    public func unsubscribe(from channels: [String]) {
        Log.low("unsubscribe(fromChannels: \(channels))", from: self)
        
        self.pubNubClient.unsubscribeFromChannels(channels, withPresence: false)
    }
    
    public func unsubscribeFromAll() {
        Log.low("unsubscribeFromAll()", from: self)
        
        self.pubNubClient.unsubscribeFromAll()
    }
    
    public func send(event: ChatClientEvent, pushPayload: ChatClientPushPayload?, to channel: String, completion: @escaping ((ChatClientError?) -> Void)) {
        Log.low("send(event: \(event.logDescription), toChannel: \(channel))", from: self)
        
        let pushPayloadJSON: [String: Any]?
        
        if let pushPayload = pushPayload {
            pushPayloadJSON = self.pushPayloadCoder.json(from: pushPayload)
        } else {
            pushPayloadJSON = nil
        }
        
        self.pubNubClient.publish(self.eventCoder.json(from: event),
                                  toChannel: channel,
                                  mobilePushPayload: pushPayloadJSON,
                                  storeInHistory: true,
                                  withCompletion: { status in
            if let error = PubNubChatClientError(fromPNStatus: status) {
                Log.low("send(event: \(event.logDescription), toChannel: \(channel)) --> Error: \(error.logDescription)", from: self)
                
                completion(error)
            } else {
                Log.low("send(event: \(event.logDescription), toChannel: \(channel)) --> Success", from: self)
                
                completion(nil)
            }
        })
    }
    
    public func history(on channel: String, limit: Int, startTime: Int64?, endTime: Int64?, completion: @escaping ((ChatClientHistory?, ChatClientError?) -> Void)) {
        Log.low("history(forChannel: \(channel), limit: \(limit))", from: self)
     
        let startTimeStamp: NSNumber?
        
        if let startTime = startTime {
            startTimeStamp = NSNumber(value: startTime)
        } else {
            startTimeStamp = nil
        }
        
        let endTimeStamp: NSNumber?
        
        if let endTime = endTime {
            endTimeStamp = NSNumber(value: endTime)
        } else {
            endTimeStamp = nil
        }
        
        self.pubNubClient.historyForChannel(channel,
                                            start: startTimeStamp,
                                            end: endTimeStamp,
                                            limit: UInt(limit),
                                            reverse: false,
                                            withCompletion: { result, status in
            do {
                if let status = status, let error = PubNubChatClientError(fromPNStatus: status) {
                    throw error
                }
                
                guard let result = result, let eventJSONs = result.data.messages as? [[String: Any]] else {
                    throw ChatClientError.badResponse
                }
                
                let events = eventJSONs.compactMap({ eventJSON in
                    return self.eventCoder.event(from: eventJSON)
                })
                
                let startTime = result.data.start.int64Value
                let endTime = result.data.end.int64Value
                
                Log.low("history(forChannel: \(channel), limit: \(limit)) --> Success", from: self)
                
                completion(ChatClientHistory(events: events, startTime: startTime, endTime: endTime), nil)
            } catch let error {
                let error = (error as? ChatClientError) ?? .unknown
                
                Log.low("history(forChannel: \(channel), limit: \(limit)) --> Error: \(error.logDescription)", from: self)
                
                completion(nil, error)
            }
        })
    }
    
    public func history(on channel: String, startTime: Int64?, endTime: Int64?, completion: @escaping ((ChatClientHistory?, ChatClientError?) -> Void)) {
        self.history(on: channel, limit: 100, startTime: startTime, endTime: endTime, completion: completion)
    }
    
    public func addObserver(_ observer: ChatClientObserver) {
        Log.low("addObserver()", from: self)
        
        if self.observers.isEmpty {
            self.pubNubClient.addListener(self)
        }
        
        if !self.observers.contains(where: { $0 === observer }) {
            self.observers.append(observer)
        }
    }
    
    public func removeObserver(_ observer: ChatClientObserver) {
        Log.low("removeObserver()", from: self)
        
        if let index = self.observers.index(where: { $0 === observer }) {
            self.observers.remove(at: index)
        }
        
        if self.observers.isEmpty {
            self.pubNubClient.removeListener(self)
        }
    }
}

// MARK: - PNObjectEventListener

extension PubNubChatClient: PNObjectEventListener {
    
    // MARK: - Instance Methods
    
    public func client(_ client: PubNub, didReceive status: PNStatus) {
        Log.low("clientDidReceiveStatus(\(status.logDescription))", from: self)
        
        switch status.operation {
        case .subscribeOperation, .unsubscribeOperation:
            if status.isError {
                status.retry()
            }
            
        default:
            break
        }
    }
    
    public func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        Log.low("clientDidReceiveMessage(\(String(describing: message.data.message)))", from: self)
        
        guard let eventJSON = message.data.message as? [String: Any] else {
            return
        }
        
        guard let event = self.eventCoder.event(from: eventJSON) else {
            return
        }
        
        for observer in self.observers {
            observer.chatClient(self, didReceiveEvent: event)
        }
    }
    
    public func client(_ client: PubNub, didReceivePresenceEvent event: PNPresenceEventResult) {
        Log.low("clientdidReceivePresenceEvent(\(event.data.presenceEvent))", from: self)
    }
}
