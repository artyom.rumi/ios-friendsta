//
//  ChatClientMethodCoder.swift
//  Tools
//
//  Created by Almaz Ibragimov on 19.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public protocol ChatClientMethodCoder {
    
    // MARK: - Instance Methods
    
    func method(from json: [String: Any]) -> ChatClientMethod?
    func json(from method: ChatClientMethod) -> [String: Any]
}
