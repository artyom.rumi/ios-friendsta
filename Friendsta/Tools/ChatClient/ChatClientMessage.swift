//
//  ChatClientMessage.swift
//  Tools
//
//  Created by Almaz Ibragimov on 18.05.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public struct ChatClientMessage {
    
    // MARK: - Instance Properties
    
    public let uid: String
    
    public let createdDate: Date
    public let modifiedDate: Date?
    public let deliveredDate: Date?
    public let viewedDate: Date?
    public let erasedDate: Date?
    
    public let isAnonymous: Bool
    
    public let content: [String: Any]
    
    // MARK: -
    
    public var logDescription: String {
        return self.uid
    }
}
