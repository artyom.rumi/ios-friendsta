//
//  PhoneNumber.swift
//  Tools
//
//  Created by Almaz Ibragimov on 31.03.2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import Foundation

public struct PhoneNumber {
    
    // MARK: - Instance Properties
    
    public let countryCode: String
    
    public let domesticNumber: String
    public let internationalNumber: String
    
    // MARK: -
    
    public var isEmpty: Bool {
        return (self.internationalNumber.count > 1)
    }
    
    // MARK: - Initializers
    
    public init(countryCode: String, domesticNumber: String) {
        let nonDigits = CharacterSet.decimalDigits.inverted
        
        self.countryCode = "+\(countryCode.components(separatedBy: nonDigits).joined())"
        self.domesticNumber = "\(domesticNumber.components(separatedBy: nonDigits).joined().suffix(10))"
        self.internationalNumber = String(format: "%@%@", self.countryCode, self.domesticNumber)
    }
    
    public init?(internationalNumber: String) {
        let nonDigits = CharacterSet.decimalDigits.inverted
        
        self.internationalNumber = "+\(internationalNumber.components(separatedBy: nonDigits).joined())"
        
        guard internationalNumber.count > 1 else {
            return nil
        }
        
        self.countryCode = internationalNumber.prefix(count: 2)
        self.domesticNumber = internationalNumber.suffix(from: 2)
    }
}

// MARK: - Equatable

extension PhoneNumber: Equatable {
    
    // MARK: - Type Methods
    
    public static func == (left: PhoneNumber, right: PhoneNumber) -> Bool {
        return (left.internationalNumber == right.internationalNumber)
    }
    
    public static func != (left: PhoneNumber, right: PhoneNumber) -> Bool {
        return !(left == right)
    }
    
    public static func ~= (left: PhoneNumber, right: PhoneNumber) -> Bool {
        return (left == right)
    }
}
