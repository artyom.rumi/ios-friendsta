//
//  TextView.swift
//  Tools
//
//  Created by Oleg Gorelov on 30/05/2018.
//  Copyright © 2018 Flatstack. All rights reserved.
//

import UIKit

@IBDesignable public class TextView: UITextView {
    
    // MARK: - Instance Properties

    private var isShowingPlaceholder = false {
        didSet {
            self.applyState()
        }
    }

    // MARK: -
    
    @IBInspectable public var bottomOutset: CGFloat {
        get {
            return self.touchEdgeOutset.bottom
        }
        
        set {
            self.touchEdgeOutset.bottom = newValue
        }
    }
    
    @IBInspectable public var leftOutset: CGFloat {
        get {
            return self.touchEdgeOutset.left
        }
        
        set {
            self.touchEdgeOutset.left = newValue
        }
    }
    
    @IBInspectable public var rightOutset: CGFloat {
        get {
            return self.touchEdgeOutset.right
        }
        
        set {
            self.touchEdgeOutset.right = newValue
        }
    }
    
    @IBInspectable public var topOutset: CGFloat {
        get {
            return self.touchEdgeOutset.top
        }
        
        set {
            self.touchEdgeOutset.top = newValue
        }
    }

    @IBInspectable public var placeholderText: String? {
        didSet {
            self.isShowingPlaceholder = !self.hasText
        }
    }

    @IBInspectable public var placeholderTextColor: UIColor = UIColor.gray
    @IBInspectable public var defaultTextColor: UIColor = UIColor.black

    // MARK: -
    
    public var touchEdgeOutset: UIEdgeInsets = UIEdgeInsets.zero

    public var placeholderFont: UIFont?
    public var defaultFont: UIFont?

    // MARK: - Initializers

    public override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)

        self.initialize()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.initialize()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    // MARK: - Instance Methods

    @objc private func textViewDidBeginEditing(_ notification: Foundation.Notification) {
        self.textColor = self.defaultTextColor

        if self.isShowingPlaceholder {
            self.text = nil
        }
    }

    @objc private func textViewDidEndEditing(_ notification: Foundation.Notification) {
        self.isShowingPlaceholder = !self.hasText
    }

    // MARK: -

    private func initialize() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.textViewDidBeginEditing(_:)),
                                               name: UITextView.textDidBeginEditingNotification, object: self)

        NotificationCenter.default.addObserver(self, selector: #selector(self.textViewDidEndEditing(_:)),
                                               name: UITextView.textDidEndEditingNotification, object: self)
    }

    private func applyState() {
        if self.isShowingPlaceholder {
            self.text = self.placeholderText
            self.textColor = self.placeholderTextColor
            self.font = self.placeholderFont ?? self.font
        } else {
            self.textColor = self.defaultTextColor
            self.font = self.defaultFont ?? self.font
        }
    }
    
    // MARK: - UIControl
    
    public override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = CGRect(x: self.bounds.origin.x - self.touchEdgeOutset.left,
                          y: self.bounds.origin.y - self.touchEdgeOutset.top,
                          width: self.bounds.width + self.touchEdgeOutset.left + self.touchEdgeOutset.right,
                          height: self.bounds.height + self.touchEdgeOutset.top + self.touchEdgeOutset.bottom)
        
        return rect.contains(point)
    }
}
