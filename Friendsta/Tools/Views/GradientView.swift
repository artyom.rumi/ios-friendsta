//
//  GradientView.swift
//  Tools
//
//  Created by Timur Shafigullin on 27/05/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable class GradientView: UIView {

    // MARK: - Instance Properties

    @IBInspectable var firstColor: UIColor? {
        didSet {
            self.applyState()
        }
    }

    @IBInspectable var secondColor: UIColor? {
        didSet {
            self.applyState()
        }
    }

    @IBInspectable var thirdColor: UIColor? {
        didSet {
            self.applyState()
        }
    }

    @IBInspectable var isVertical: Bool = true {
        didSet {
            self.applyState()
        }
    }

    // MARK: -

    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }

    // MARK: - Instance Methods

    private func applyState() {
        let layer = self.layer as! CAGradientLayer

        guard let firstColor = self.firstColor, let secondColor = self.secondColor else {
            return
        }

        if let thirdColor = self.thirdColor {
            layer.colors = [firstColor.cgColor, secondColor.cgColor, thirdColor.cgColor]
        } else {
            layer.colors = [firstColor.cgColor, secondColor.cgColor]
        }

        if self.isVertical {
            layer.startPoint = CGPoint(x: 0.5, y: 0)
            layer.endPoint = CGPoint(x: 0.5, y: 1)
        } else {
            layer.startPoint = CGPoint(x: 0, y: 0.5)
            layer.endPoint = CGPoint(x: 1, y: 0.5)
        }
    }
}
