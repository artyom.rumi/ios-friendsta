//
//  CustomSegmentedControl.swift
//  Friendsta
//
//  Created by Elina Batyrova on 03.06.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

class CustomSegmentedControl: UIView {
    
    // MARK: - Nested Types
    
    enum Constants {
        
        // MARK: - Type Properties
        
        static let bottomSelectorViewHeight: CGFloat = 2.0
        static let buttonImageSpacing: CGFloat = 16
    }
    
    // MARK: - Instance Properties
    
    @IBInspectable var segments: String = "" {
        didSet {
            self.buttonTitles = segments.components(separatedBy: "\n")
        }
    }
    
    @IBInspectable var deselectedContentColor: UIColor = .clear {
        didSet {
            self.updateView()
        }
    }
    
    @IBInspectable var selectedContentColor: UIColor = .clear {
        didSet {
            self.updateView()
        }
    }
    
    @IBInspectable var bottomSelectorViewColor: UIColor = .clear {
        didSet {
            self.updateView()
        }
    }

    var buttonTitles: [String] = [] {
        didSet {
            self.updateButtonTitles()
        }
    }
    
    var buttonImages: [UIImage?] = [] {
        didSet {
            self.updateButtonImages()
        }
    }

    // MARK: -

    private var buttons: [UIButton] = [UIButton]()
    private var bottomSelectorView: UIView = UIView()
    
    // MARK: -
    
    var onSegmentSelected: ((_ index: Int) -> Void)?
    var selectedSegmentIndex: Int = 0
    
    // MARK: - Initializers
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        self.updateView()
    }
    
    // MARK: - Instance Methods
    
    private func configStackView() {
        let stack = UIStackView(arrangedSubviews: self.buttons)
        
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fillEqually
        
        self.addSubview(stack)
        
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        stack.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        stack.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
    }
    
    private func configSelectorView() {
        let selectorWidth = self.frame.width / CGFloat(self.buttonTitles.count)
        
        self.bottomSelectorView = UIView(frame: CGRect(x: 0,
                                                       y: self.frame.height - Constants.bottomSelectorViewHeight,
                                                       width: selectorWidth,
                                                       height: Constants.bottomSelectorViewHeight))
        self.bottomSelectorView.backgroundColor = self.bottomSelectorViewColor
        
        self.addSubview(self.bottomSelectorView)
    }
    
    private func createButtons() {
        self.buttons.removeAll()
        
        self.subviews.forEach({
            $0.removeFromSuperview()
        })
        
        for buttonTitle in self.buttonTitles {
            let button = UIButton(type: .system)
            
            button.setTitle(buttonTitle, for: .normal)
            button.addTarget(self, action: #selector(CustomSegmentedControl.onButtonTouchUpInside(sender:)), for: .touchUpInside)
            button.setTitleColor(self.deselectedContentColor, for: .normal)
            button.tintColor = self.deselectedContentColor
            button.semanticContentAttribute = .forceRightToLeft
            button.imageEdgeInsets = UIEdgeInsets(top: 0, left: Constants.buttonImageSpacing, bottom: 0, right: 0)
            button.titleLabel?.font = Fonts.medium(ofSize: 16.0)
            
            self.buttons.append(button)
        }
    
        self.buttons[0].setTitleColor(self.selectedContentColor, for: .normal)
    }
    
    @objc private func onButtonTouchUpInside(sender: UIButton) {
        for (index, button) in self.buttons.enumerated() {
            button.setTitleColor(self.deselectedContentColor, for: .normal)
            button.tintColor = self.deselectedContentColor
            
            if button == sender {
                let selectorPosition = frame.width / CGFloat(self.buttonTitles.count) * CGFloat(index)
                
                UIView.animate(withDuration: 0.25) {
                    self.bottomSelectorView.frame.origin.x = selectorPosition
                }
                
                button.setTitleColor(self.selectedContentColor, for: .normal)
                button.tintColor = self.selectedContentColor

                self.selectedSegmentIndex = index

                self.onSegmentSelected?(index)
            }
        }
    }
    
    private func updateView() {
        self.createButtons()
        self.configSelectorView()
        self.configStackView()
        self.updateButtonTitles()
        self.updateButtonImages()
    }

    private func updateButtonTitles() {
        guard !self.buttons.isEmpty else {
            return
        }

        guard self.buttonTitles.count == self.buttons.count else {
            fatalError("Title count should be equal button count")
        }

        self.buttonTitles.enumerated().forEach { index, buttonTitle in
            self.buttons[index].setTitle(buttonTitle, for: .normal)
        }
    }
    
    private func updateButtonImages() {
        if self.buttons.count != self.buttonImages.count {
            return
        }
        
        self.buttonImages.enumerated().forEach { index, buttonImage in
            self.buttons[index].setImage(buttonImage, for: .normal)
        }
    }
}
