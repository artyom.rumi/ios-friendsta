//
//  RoundButton.swift
//  Friendsta
//
//  Created by Elina Batyrova on 10.01.2020.
//  Copyright © 2020 Decision Accelerator. All rights reserved.
//

import UIKit

class RoundButton: UIButton {

    // MARK: - Initializers

    public override init(frame: CGRect = CGRect.zero) {
        super.init(frame: frame)

        self.layer.masksToBounds = true
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.layer.masksToBounds = true
    }

    // MARK: - Instance Methods

    public override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.cornerRadius = min(self.frame.width, self.frame.height) * 0.5
    }
}
