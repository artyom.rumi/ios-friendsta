//
//  RoundShadowViewWithImage.swift
//  Friendsta
//
//  Created by Elina Batyrova on 26.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable public class RoundShadowViewWithImage: UIView {
    
    // MARK: - Instance Properties

    private var shadowView = UIView()
    private var imageView = UIImageView()
    
    // MARK: -
    
    @IBInspectable public var shadowColor: UIColor = .clear
    @IBInspectable public var shadowOffset: CGSize = .zero
    @IBInspectable public var shadowOpacity: Float = 0
    @IBInspectable public var shadowRadius: CGFloat = 0
        
    public var imageViewTarget: UIImageView {
        return self.imageView
    }
    
    @IBInspectable public var placeholderImage: UIImage? {
        didSet {
            self.imageView.image = self.placeholderImage
        }
    }
    
    // MARK: - UIView

    public override func layoutSubviews() {
        super.layoutSubviews()

        let cornerRadius = min(self.frame.width, self.frame.height) * 0.5
        
        self.shadowView.frame = self.bounds
        
        self.shadowView.layer.shadowColor = self.shadowColor.cgColor
        self.shadowView.layer.shadowOffset = self.shadowOffset
        self.shadowView.layer.shadowRadius = self.shadowRadius
        self.shadowView.layer.shadowOpacity = self.shadowOpacity
        
        self.imageView.layer.cornerRadius = cornerRadius
        self.imageView.layer.masksToBounds = true
        self.imageView.frame = self.bounds
        
        self.shadowView.addSubview(imageView)
        
        self.backgroundColor = UIColor.clear
                
        self.addSubview(self.shadowView)
    }
}
