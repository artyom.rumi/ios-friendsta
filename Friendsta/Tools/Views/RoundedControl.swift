//
//  RoundedControl.swift
//  Friendsta
//
//  Created by Timur Shafigullin on 19/08/2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable public class RoundedControl: UIControl {

    // MARK: - Instance Properties

    @IBInspectable public var cornerRadius: CGFloat = 0
    @IBInspectable public var borderColor: UIColor = .clear
    @IBInspectable public var borderWidth: CGFloat = 0

    // MARK: - UIView

    public override func layoutSubviews() {
        super.layoutSubviews()

        self.layer.cornerRadius = self.cornerRadius
        self.layer.borderColor = self.borderColor.cgColor
        self.layer.borderWidth = self.borderWidth
        self.layer.masksToBounds = (self.cornerRadius > 0)
    }

    public override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        self.setNeedsLayout()
    }
}
