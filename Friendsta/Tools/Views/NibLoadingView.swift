//
//  NibLoadingView.swift
//  Friendsta
//
//  Created by Nikita Asabin on 2/5/19.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable
class NibLoadingView: UIView {
    
    // MARK: - Instance Properties
    
    @IBOutlet weak var view: UIView!
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.nibSetup()
    }
    
    // MARK: - Instance methods
    
    private func nibSetup() {
        self.backgroundColor = .clear
        
        self.view = self.loadViewFromNib()
        self.view.frame = self.bounds
        self.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.translatesAutoresizingMaskIntoConstraints = true
        
        self.addSubview(self.view)
    }
    
    private func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
    }
}
