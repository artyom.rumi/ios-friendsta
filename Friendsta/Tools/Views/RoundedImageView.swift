//
//  RoundedImageView.swift
//  Friendsta
//
//  Created by Elina Batyrova on 18.12.2019.
//  Copyright © 2019 Decision Accelerator. All rights reserved.
//

import UIKit

@IBDesignable public class RoundedImageView: UIImageView {

    // MARK: - Instance Properties

    @IBInspectable public var cornerRadius: CGFloat = 0.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
            self.layer.masksToBounds = (self.cornerRadius > 0.0)
        }
    }
}
