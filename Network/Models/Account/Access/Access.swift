//
//  Access.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public struct Access {
    
    // MARK: - Instance Properties
    
    public var token: String
    public var expiryDate: Date?
    public var userUID: Int64?
    
    // MARK: -
    
    public var isValid: Bool {
        if self.token.isEmpty {
            return false
        } else if let expiryDate = self.expiryDate {
            return (expiryDate > Date())
        } else {
            return true
        }
    }
    
    public var hasUser: Bool {
        return (self.userUID != nil)
    }
    
    // MARK: - Initializers
    
    public init(token: String, expiryDate: Date? = nil, userUID: Int64? = nil) {
        self.token = token
        self.expiryDate = expiryDate
        self.userUID = userUID
    }
}

// MARK: - Equatable

extension Access: Equatable {
    
    // MARK: - Type Methods
    
    public static func == (left: Access, right: Access) -> Bool {
        if left.token != right.token {
            return false
        }
        
        if left.expiryDate != right.expiryDate {
            return false
        }
        
        if left.userUID != right.userUID {
            return false
        }
        
        return true
    }
    
    public static func != (left: Access, right: Access) -> Bool {
        return !(left == right)
    }
    
    public static func ~= (left: Access, right: Access) -> Bool {
        return (left == right)
    }
}
