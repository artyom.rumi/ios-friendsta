//
//  AccessManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 25.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

public protocol AccessManager {
    
    // MARK: - Instance Properties
    
    var access: Access? { get }
    
    var accessUpdatedEvent: Event<Access> { get }
    var accessResetedEvent: Event<Void> { get }
    
    // MARK: - Instance Methods
    
    func update(access: Access)
    
    func resetAccess()
    func restoreAccess()
    func saveAccess()
}
