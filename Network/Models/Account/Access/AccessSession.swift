//
//  AccessSession.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 07.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public struct AccessSession {
    
    // MARK: - Instance Properties
    
    public var phoneNumber: String
    public var userUID: Int64?
    
    public var attemptMaxCount: Int?
    public var expiryDate: Date?
    
    // MARK: -
    
    public var isValid: Bool {
        if self.phoneNumber.isEmpty {
            return false
        } else if let expiryDate = self.expiryDate {
            return (expiryDate > Date())
        } else {
            return true
        }
    }
    
    public var hasUser: Bool {
        return (self.userUID != nil)
    }
    
    // MARK: - Initializers
    
    public init(phoneNumber: String, userUID: Int64?, attemptMaxCount: Int?, expiryDate: Date?) {
        self.phoneNumber = phoneNumber
        self.userUID = userUID
        self.attemptMaxCount = attemptMaxCount
        self.expiryDate = expiryDate
    }
}
