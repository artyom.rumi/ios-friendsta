//
//  DefaultAccessManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper
import FriendstaTools

public class DefaultAccessManager: AccessManager {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties

        static let storageStateKeyFormat = "AccessState<%@>"
        
        static let storageTokenKeyFormat = "AccessToken<%@>"
        static let storageExpiryDateKeyFormat = "AccessExpiryDate<%@>"
        static let storageUserUIDKeyFormat = "AccessUserUID<%@>"
    }
        
    // MARK: - Instance Properties

    fileprivate let storageStateKey: String
    
    fileprivate let storageTokenKey: String
    fileprivate let storageExpiryDateKey: String
    fileprivate let storageUserUIDKey: String
    
    private let userDefaultsManager: UserDefaults!
    private let keychainWrapperManager: KeychainWrapper!
    
    // MARK: -
    
    public let identifier: String
    
    // MARK: - AccessManager
    
    public var access: Access?
    
    public lazy var accessUpdatedEvent = Event<Access>()
    public lazy var accessResetedEvent = Event<Void>()
    
    // MARK: - Initializers
    
    init(identifier: String, userDefaultsManager: UserDefaults, keychainWrapperManager: KeychainWrapper) {
        self.storageStateKey = String(format: Constants.storageStateKeyFormat, identifier)
        
        self.storageTokenKey = String(format: Constants.storageTokenKeyFormat, identifier)
        self.storageExpiryDateKey = String(format: Constants.storageExpiryDateKeyFormat, identifier)
        self.storageUserUIDKey = String(format: Constants.storageUserUIDKeyFormat, identifier)
        
        self.identifier = identifier
        self.userDefaultsManager = userDefaultsManager
        self.keychainWrapperManager = keychainWrapperManager
        
        self.restoreAccess()
    }
    
    // MARK: - Instance Methods
    
    public func update(access: Access) {
        if self.access != access {
            self.access = access
            
            self.accessUpdatedEvent.emit(data: access)
        }
    }
    
    public func resetAccess() {
        self.access = nil
        
        self.accessResetedEvent.emit()
    }
    
    public func restoreAccess() {
        self.access = nil
        
        guard self.userDefaultsManager.bool(forKey: self.storageStateKey) else {
            return
        }
        
        guard let token = self.keychainWrapperManager.string(forKey: self.storageTokenKey,
                                                          withAccessibility: .whenUnlockedThisDeviceOnly) else {
            return
        }
        
        let expiryDate: Date?
        
        if let expiryTimeInterval = self.keychainWrapperManager.double(forKey: self.storageExpiryDateKey,
                                                                    withAccessibility: .whenUnlockedThisDeviceOnly) {
            expiryDate = Date(timeIntervalSinceReferenceDate: expiryTimeInterval)
        } else {
            expiryDate = nil
        }
        
        let userUID: Int64?
        
        if let userRawUID = self.keychainWrapperManager.string(forKey: self.storageUserUIDKey,
                                                            withAccessibility: .whenUnlockedThisDeviceOnly) {
            userUID = Int64(userRawUID)
        } else {
            userUID = nil
        }
        
        self.access = Access(token: token, expiryDate: expiryDate, userUID: userUID)
    }
    
    public func saveAccess() {
        if let token = self.access?.token {
            self.keychainWrapperManager.set(token,
                                         forKey: self.storageTokenKey,
                                         withAccessibility: .whenUnlockedThisDeviceOnly)
        } else {
            self.keychainWrapperManager.removeObject(forKey: self.storageTokenKey)
        }
        
        if let expiryTimeInterval = self.access?.expiryDate?.timeIntervalSinceReferenceDate {
            self.keychainWrapperManager.set(expiryTimeInterval,
                                         forKey: self.storageExpiryDateKey,
                                         withAccessibility: .whenUnlockedThisDeviceOnly)
        } else {
            self.keychainWrapperManager.removeObject(forKey: self.storageExpiryDateKey)
        }
     
        if let userUID = self.access?.userUID {
            self.keychainWrapperManager.set(String(userUID),
                                         forKey: self.storageUserUIDKey,
                                         withAccessibility: .whenUnlockedThisDeviceOnly)
        } else {
            self.keychainWrapperManager.removeObject(forKey: self.storageUserUIDKey)
        }
        
        self.userDefaultsManager.set(true, forKey: self.storageStateKey)
    }
}
