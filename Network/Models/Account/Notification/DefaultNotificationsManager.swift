//
//  DefaultNotificationsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

public class DefaultNotificationsManager: NotificationsManager {
    
    // MARK: - Instance Properties
    
    public var notifications: [Notification] = []
    
    public lazy var notificationReceivedEvent = Event<Notification>()
    public lazy var notificationsClearedEvent = Event<Void>()
    
    // MARK: - Instance Methods
    
    public func push(notification: Notification) {
        self.notifications.append(notification)
        
        self.notificationReceivedEvent.emit(data: notification)
    }
    
    public func clearNotifications() {
        self.notifications = []
        
        self.notificationsClearedEvent.emit()
    }
}
