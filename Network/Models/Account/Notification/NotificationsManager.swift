//
//  NotificationsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

public protocol NotificationsManager {
    
    // MARK: - Instance Properties
    
    var notifications: [Notification] { get }
    
    var notificationReceivedEvent: Event<Notification> { get }
    var notificationsClearedEvent: Event<Void> { get }
    
    // MARK: - Instance Methods
    
    func push(notification: Notification)
    
    func clearNotifications()
}
