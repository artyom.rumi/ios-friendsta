//
//  Notification.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public struct Notification {
    
    // MARK: - Instance Properties
    
    public let indentifier = UUID()
    
    public var category: NotificationCategory
    public var action: NotificationAction?
    
    // MARK: - Initializers
    
    public init(category: NotificationCategory, action: NotificationAction? = nil) {
        self.category = category
        self.action = action
    }
}

// MARK: - Equatable

extension Notification: Equatable {
    
    // MARK: - Type Methods
    
    public static func == (left: Notification, right: Notification) -> Bool {
        return (left.indentifier == right.indentifier)
    }
    
    public static func != (left: Notification, right: Notification) -> Bool {
        return !(left == right)
    }
    
    public static func ~= (left: Notification, right: Notification) -> Bool {
        return (left == right)
    }
}
