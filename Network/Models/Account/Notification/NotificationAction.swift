//
//  NotificationAction.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public enum NotificationAction {
    
    // MARK: - Enumeration Cases
    
    case open
}
