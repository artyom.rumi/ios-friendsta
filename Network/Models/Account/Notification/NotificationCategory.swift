//
//  NotificationCategory.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public enum NotificationCategory {
    
    // MARK: - Enumeration Cases
    
    case newUser
    case newFriend
    case propCard
    case pokeReply
    case chatMessage(chatUID: Int64, senderUID: Int64?, receiverUID: Int64?)
}
