//
//  AccountModel.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public protocol AccountModel {
    
    // MARK: - Instance Properties
    
    var identifier: String { get }
    
    var accessManager: AccessManager { get }
    var deviceTokenManager: DeviceTokenManager { get }
    var notificationsManager: NotificationsManager { get }
    var settingsManager: SettingsManager { get }
}

// MARK: -

extension AccountModel {
    
    // MARK: - Instance Properties
    
    public var access: Access? {
        return self.accessManager.access
    }
    
    public var deviceToken: String? {
        return self.deviceTokenManager.deviceToken
    }
    
    public var notifications: [Notification] {
        return self.notificationsManager.notifications
    }
    
    public var settings: Settings {
        return self.settingsManager.settings
    }
}
