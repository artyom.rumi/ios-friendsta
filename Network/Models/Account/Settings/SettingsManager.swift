//
//  SettingsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

public protocol SettingsManager {
    
    // MARK: - Instance Properties
    
    var settings: Settings { get }
    
    var settingsUpdatedEvent: Event<Settings> { get }
    var settingsResetedEvent: Event<Void> { get }
    
    // MARK: - Instance Methods
    
    func update(settings: Settings)
    
    func resetSettings()
    func restoreSettings()
    func saveSettings()
}
