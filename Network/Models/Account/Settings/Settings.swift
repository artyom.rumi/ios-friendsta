//
//  Settings.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public struct Settings {
    
    // MARK: - Instance Properties
    
    public var firstTutorialCompleted = false
    public var secondTutorialCompleted = false
    public var thirdTutorialCompleted = false
}

// MARK: - Equatable

extension Settings: Equatable {
    
    // MARK: - Type Methods
    
    public static func == (left: Settings, right: Settings) -> Bool {
        if left.firstTutorialCompleted != right.firstTutorialCompleted {
            return false
        }
        
        if left.secondTutorialCompleted != right.secondTutorialCompleted {
            return false
        }
        
        if left.thirdTutorialCompleted != right.thirdTutorialCompleted {
            return false
        }
        
        return true
    }
    
    public static func != (left: Settings, right: Settings) -> Bool {
        return !(left == right)
    }
    
    public static func ~= (left: Settings, right: Settings) -> Bool {
        return (left == right)
    }
}
