//
//  DefaultSettingsManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

public class DefaultSettingsManager: SettingsManager {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let storageFirstTutorialCompletedKeyFormat = "SettingsFirstTutorialCompleted<%@>"
        static let storageSecondTutorialCompletedKeyFormat = "SettingsSecondTutorialCompleted<%@>"
        static let storageThirdTutorialCompletedKeyFormat = "SettingsThirdTutorialCompleted<%@>"
    }
    
    // MARK: - Instance Properties
    
    fileprivate let storageFirstTutorialCompletedKey: String
    fileprivate let storageSecondTutorialCompletedKey: String
    fileprivate let storageThirdTutorialCompletedKey: String
    
    // MARK: -
    
    let identifier: String
    
    // MARK: - AccessManager
    
    public var settings = Settings()
    
    public lazy var settingsUpdatedEvent = Event<Settings>()
    public lazy var settingsResetedEvent = Event<Void>()
    
    // MARK: - Initializers
    
    public init(identifier: String) {
        self.storageFirstTutorialCompletedKey = String(format: Constants.storageFirstTutorialCompletedKeyFormat, identifier)
        self.storageSecondTutorialCompletedKey = String(format: Constants.storageSecondTutorialCompletedKeyFormat, identifier)
        self.storageThirdTutorialCompletedKey = String(format: Constants.storageThirdTutorialCompletedKeyFormat, identifier)
        
        self.identifier = identifier
        
        self.restoreSettings()
    }
    
    // MARK: - Instance Methods
    
    public func update(settings: Settings) {
        if self.settings != settings {
            self.settings = settings
            
            self.settingsUpdatedEvent.emit(data: settings)
        }
    }
    
    public func resetSettings() {
        self.settings = Settings()
        
        self.settingsResetedEvent.emit()
    }
    
    public func restoreSettings() {
        self.settings = Settings()
        
        if let firstTutorialCompleted = UserDefaults.standard.object(forKey: self.storageFirstTutorialCompletedKey) as? Bool {
            self.settings.firstTutorialCompleted = firstTutorialCompleted
        }
        
        if let secondTutorialCompleted = UserDefaults.standard.object(forKey: self.storageSecondTutorialCompletedKey) as? Bool {
            self.settings.secondTutorialCompleted = secondTutorialCompleted
        }
        
        if let thirdTutorialCompleted = UserDefaults.standard.object(forKey: self.storageThirdTutorialCompletedKey) as? Bool {
            self.settings.thirdTutorialCompleted = thirdTutorialCompleted
        }
    }
    
    public func saveSettings() {
        UserDefaults.standard.set(self.settings.firstTutorialCompleted, forKey: self.storageFirstTutorialCompletedKey)
        UserDefaults.standard.set(self.settings.secondTutorialCompleted, forKey: self.storageSecondTutorialCompletedKey)
        UserDefaults.standard.set(self.settings.thirdTutorialCompleted, forKey: self.storageThirdTutorialCompletedKey)
    }
}
