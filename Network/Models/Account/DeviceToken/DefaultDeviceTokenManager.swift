//
//  DefaultDeviceTokenManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

public class DefaultDeviceTokenManager: DeviceTokenManager {
    
    // MARK: - Nested Types
    
    fileprivate enum Constants {
        
        // MARK: - Type Properties
        
        static let storageDeviceTokenKeyFormat = "DeviceToken<%@>"
    }
    
    // MARK: - Instance Properties
    
    fileprivate let storageDeviceTokenKey: String
    
    private let userDefaultsManager: UserDefaults!
    
    // MARK: -
    
    let identifier: String
    
    // MARK: - Instance Properties
    
    public var deviceToken: String?
    
    public lazy var deviceTokenUpdatedEvent = Event<String>()
    public lazy var deviceTokenResetedEvent = Event<Void>()
    
    // MARK: - Initializers
    
    public init(identifier: String, userDefaultsManager: UserDefaults) {
        self.storageDeviceTokenKey = String(format: Constants.storageDeviceTokenKeyFormat, identifier)
        
        self.identifier = identifier
        self.userDefaultsManager = userDefaultsManager
        
        self.restoreDeviceToken()
    }
    
    // MARK: - Instance Methods
    
    public func update(deviceToken: String) {
        if self.deviceToken != deviceToken {
            self.deviceToken = deviceToken
            
            self.deviceTokenUpdatedEvent.emit(data: deviceToken)
        }
    }
    
    public func resetDeviceToken() {
        self.deviceToken = nil
        
        self.deviceTokenResetedEvent.emit()
    }
    
    public func restoreDeviceToken() {
        self.deviceToken = self.userDefaultsManager.object(forKey: self.storageDeviceTokenKey) as? String
    }
    
    public func saveDeviceToken() {
        if let deviceToken = self.deviceToken {
            self.userDefaultsManager.set(deviceToken, forKey: self.storageDeviceTokenKey)
        } else {
            self.userDefaultsManager.removeObject(forKey: self.storageDeviceTokenKey)
        }
    }
}
