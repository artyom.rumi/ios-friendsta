//
//  DeviceTokenManager.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 20.05.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import FriendstaTools

public protocol DeviceTokenManager {
    
    // MARK: - Instance Properties
    
    var deviceToken: String? { get }
    
    var deviceTokenUpdatedEvent: Event<String> { get }
    var deviceTokenResetedEvent: Event<Void> { get }
    
    // MARK: - Instance Methods
    
    func update(deviceToken: String)
    
    func resetDeviceToken()
    func restoreDeviceToken()
    func saveDeviceToken()
}
