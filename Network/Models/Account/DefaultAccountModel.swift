//
//  DefaultAccountModel.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 26.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

public final class DefaultAccountModel: AccountModel {
    
    // MARK: - Type Properties
    
    private let userDefaultsManager: UserDefaults!
    private let keychainWrapperManager: KeychainWrapper!
    
    // MARK: - Instance Properties
    
    public let identifier: String
    
    // MARK: -
    
    public lazy var accessManager: AccessManager = {
        return DefaultAccessManager(identifier: self.identifier,
                                    userDefaultsManager: self.userDefaultsManager,
                                    keychainWrapperManager: self.keychainWrapperManager)
    }()
    
    public lazy var deviceTokenManager: DeviceTokenManager = {
        return DefaultDeviceTokenManager(identifier: self.identifier, userDefaultsManager: self.userDefaultsManager)
    }()
    
    public lazy var notificationsManager: NotificationsManager = {
        return DefaultNotificationsManager()
    }()
    
    public lazy var settingsManager: SettingsManager = {
        return DefaultSettingsManager(identifier: self.identifier)
    }()
    
    // MARK: - Initializers
    
    public init(userDefaultsManager: UserDefaults, keychainWrapperManager: KeychainWrapper, identifier: String = "Default") {
        self.userDefaultsManager = userDefaultsManager
        self.keychainWrapperManager = keychainWrapperManager
        self.identifier = identifier
    }
}
