//
//  APIService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 22.04.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

public protocol APIWebService {
    
    // MARK: - Instance Methods
    
    func jsonArray(with webRequest: WebRequest, queue: DispatchQueue?) -> Promise<[[String: Any]]>
    func jsonObject(with webRequest: WebRequest, queue: DispatchQueue?) -> Promise<[String: Any]>
    func json(with webRequest: WebRequest, queue: DispatchQueue?) -> Promise<Any?>
    
    func jsonArray(uploadingMultiPart: WebMultiPart, to path: String, queue: DispatchQueue?) -> Promise<[[String: Any]]>
    func jsonObject(uploadingMultiPart: WebMultiPart, to path: String, queue: DispatchQueue?) -> Promise<[String: Any]>
    func jsonObject(uploadingMultiPart: WebMultiPart, params: [String: Any], to path: String, queue: DispatchQueue?) -> Promise<[String: Any]>
    func json(uploadingMultiPart: WebMultiPart, to path: String, queue: DispatchQueue?) -> Promise<Any?>
}

// MARK: -

public extension APIWebService {
    
    // MARK: - Instance Methods
    
    func jsonArray(with webRequest: WebRequest) -> Promise<[[String: Any]]> {
        return self.jsonArray(with: webRequest, queue: nil)
    }
    
    func jsonObject(with webRequest: WebRequest) -> Promise<[String: Any]> {
        return self.jsonObject(with: webRequest, queue: nil)
    }
    
    func json(with webRequest: WebRequest) -> Promise<Any?> {
        return self.json(with: webRequest, queue: nil)
    }
    
    func jsonArray(uploadingMultiPart: WebMultiPart, to path: String) -> Promise<[[String: Any]]> {
        return self.jsonArray(uploadingMultiPart: uploadingMultiPart, to: path, queue: nil)
    }
    
    func jsonObject(uploadingMultiPart: WebMultiPart, to path: String) -> Promise<[String: Any]> {
        return self.jsonObject(uploadingMultiPart: uploadingMultiPart, to: path, queue: nil)
    }
    
    func jsonObject(uploadingMultiPart: WebMultiPart, params: [String: Any], to path: String) -> Promise<[String: Any]> {
        return self.jsonObject(uploadingMultiPart: uploadingMultiPart, params: params, to: path, queue: nil)
    }
    
    func json(uploadingMultiPart: WebMultiPart, to path: String) -> Promise<Any?> {
        return self.json(uploadingMultiPart: uploadingMultiPart, to: path, queue: nil)
    }
}
