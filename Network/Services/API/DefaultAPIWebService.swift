//
//  DefaultAPIWebService.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 16.06.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

public struct DefaultAPIWebService: APIWebService {
    
    // MARK: - Instance Properties
    
    let accountProvider: AccountProvider
    let webService: WebService
    
    // MARK: - Initializers
    
    public init(accountProvider: AccountProvider, webService: WebService) {
        self.accountProvider = accountProvider
        self.webService = webService
    }
    
    // MARK: - Instance Methods
    
    fileprivate func jsonResponse<Response>(with webHandler: WebHandler, queue: DispatchQueue?) -> Promise<Response> {
        return Promise(resolver: { seal in
            webHandler.responseJSON(queue: queue, completion: { response, error in
                webHandler.keepAlive()
                
                if let error = error {
                    switch error {
                    case .unauthorized:
                        DispatchQueue.main.async(execute: {
                            firstly {
                                self.accountProvider.captureModel()
                            }.done { accountSession in
                                accountSession.model.accessManager.resetAccess()
                                accountSession.model.accessManager.saveAccess()
                                
                                seal.reject(error)
                            }
                        })
                        
                    default:
                        seal.reject(error)
                    }
                } else {
                    if let response = response as? Response {
                        seal.fulfill(response)
                    } else {
                        seal.reject(WebError.badResponse)
                    }
                }
            })
        })
    }
    
    fileprivate func jsonResponse<Response>(with webRequest: WebRequest, queue: DispatchQueue?) -> Promise<Response> {
        return self.jsonResponse(with: self.webService.make(request: webRequest), queue: queue)
    }
    
    fileprivate func jsonResponse<Response>(uploadingMultiPart: WebMultiPart, to path: String, queue: DispatchQueue?) -> Promise<Response> {
        return Promise(resolver: { seal in
            self.webService.upload(multiPart: uploadingMultiPart, to: path, encodingCompletion: { webHandler, error in
                if let webHandler = webHandler {
                    firstly {
                        self.jsonResponse(with: webHandler, queue: queue)
                    }.done { response in
                        seal.fulfill(response)
                    }.catch { error in
                        seal.reject(error)
                    }
                } else {
                    seal.reject(error ?? WebError.badRequest)
                }
            })
        })
    }
    
    fileprivate func jsonResponse<Response>(uploadingMultiPart: WebMultiPart, params: [String: Any], to path: String, queue: DispatchQueue?) -> Promise<Response> {
        return Promise(resolver: { seal in
            self.webService.upload(multiPart: uploadingMultiPart, params: params, to: path, encodingCompletion: { webHandler, error in
                if let webHandler = webHandler {
                    firstly {
                        self.jsonResponse(with: webHandler, queue: queue)
                    }.done { response in
                        seal.fulfill(response)
                    }.catch { error in
                        seal.reject(error)
                    }
                } else {
                    seal.reject(error ?? WebError.badRequest)
                }
            })
        })
    }
    
    // MARK: - APIWebService
    
    public func jsonArray(with webRequest: WebRequest, queue: DispatchQueue? = nil) -> Promise<[[String: Any]]> {
        return self.jsonResponse(with: webRequest, queue: queue)
    }
    
    public func jsonObject(with webRequest: WebRequest, queue: DispatchQueue? = nil) -> Promise<[String: Any]> {
        return self.jsonResponse(with: webRequest, queue: queue)
    }
    
    public func json(with webRequest: WebRequest, queue: DispatchQueue? = nil) -> Promise<Any?> {
        return self.jsonResponse(with: webRequest, queue: queue)
    }
    
    public func jsonArray(uploadingMultiPart: WebMultiPart, to path: String, queue: DispatchQueue? = nil) -> Promise<[[String: Any]]> {
        return self.jsonResponse(uploadingMultiPart: uploadingMultiPart, to: path, queue: queue)
    }
    
    public func jsonObject(uploadingMultiPart: WebMultiPart, to path: String, queue: DispatchQueue? = nil) -> Promise<[String: Any]> {
        return self.jsonResponse(uploadingMultiPart: uploadingMultiPart, to: path, queue: queue)
    }
    
    public func jsonObject(uploadingMultiPart: WebMultiPart, params: [String: Any], to path: String, queue: DispatchQueue? = nil) -> Promise<[String: Any]> {
        return self.jsonResponse(uploadingMultiPart: uploadingMultiPart, params: params, to: path, queue: queue)
    }
    
    public func json(uploadingMultiPart: WebMultiPart, to path: String, queue: DispatchQueue? = nil) -> Promise<Any?> {
        return self.jsonResponse(uploadingMultiPart: uploadingMultiPart, to: path, queue: queue)
    }
}
