//
//  DefaultAPIWebRequestAdapter.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 09.03.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

public struct DefaultAPIWebRequestAdapter: WebRequestAdapter {
    
    // MARK: - Instance Properties
    
    public let serverBaseURL: URL
    
    public let accountProvider: AccountProvider
    
    // MARK: - Initializers
    
    public init(serverBaseURL: URL, accountProvider: AccountProvider) {
        self.serverBaseURL = serverBaseURL
        self.accountProvider = accountProvider
    }
    
    // MARK: - Instance Methods
    
    public func adapt(_ request: URLRequest) throws -> URLRequest {
        var adaptedRequest = request
        
        if request.url?.absoluteString.hasPrefix(self.serverBaseURL.absoluteString) ?? false {
            if let accessToken = self.accountProvider.model.access?.token, !accessToken.isEmpty {
                adaptedRequest.setValue(accessToken, forHTTPHeaderField: "X-Auth-Token")
            }
        }
        
        return adaptedRequest
    }
}
