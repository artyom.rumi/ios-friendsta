//
//  DefaultAccountSession.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation

class DefaultAccountSession: AccountSession {
    
    // MARK: - Instance Properties
    
    let releaseHandler: () -> Void
    
    // MARK: - AccountSession
    
    fileprivate(set) var model: AccountModel
    
    // MARK: - Initializers
    
    init(model: AccountModel, releaseHandler: @escaping (() -> Void)) {
        self.releaseHandler = releaseHandler
        
        self.model = model
    }
    
    deinit {
        self.releaseHandler()
    }
}
