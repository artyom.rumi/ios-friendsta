//
//  DefaultAccountProvider.swift
//  Friendsta
//
//  Created by Almaz Ibragimov on 23.07.2018.
//  Copyright © 2018 Decision Accelerator. All rights reserved.
//

import Foundation
import PromiseKit

public class DefaultAccountProvider: AccountProvider {
    
    // MARK: - Instance Properties
    
    fileprivate var captureResolvers: [(AccountSession) -> Void] = []
    
    // MARK: - AccountProvider
    
    public var isModelCaptured = false
    
    public let model: AccountModel
    
    // MARK: - Initializers
    
    public init(model: AccountModel) {
        self.model = model
    }
    
    // MARK: - Instance Methods
    
    fileprivate func createSession() -> AccountSession {
        return DefaultAccountSession(model: self.model, releaseHandler: { [weak self] in
            self?.resolveNextCapture()
        })
    }
    
    fileprivate func resolveNextCapture() {
        if !self.captureResolvers.isEmpty {
            self.isModelCaptured = true
            
            self.captureResolvers.removeFirst()(self.createSession())
        } else {
            self.isModelCaptured = false
        }
    }
    
    // MARK: - AccountService
    
    public func captureModel() -> Guarantee<AccountSession> {
        return Guarantee(resolver: { completion in
            if self.isModelCaptured {
                self.captureResolvers.append(completion)
            } else {
                self.isModelCaptured = true
                
                completion(self.createSession())
            }
        })
    }
}
